//
//  MDRadialProgressTheme.m
//  MDRadialProgress
//
//  Created by Marco Dinacci on 07/10/2013.
//  Copyright (c) 2013 Marco Dinacci. All rights reserved.
//

#import "MDRadialProgressTheme.h"


// The font size is automatically adapted but this is the maximum it will get
// unless overridden by the user.
//static const int kMaxFontSize = 64;

static const int kMaxFontSize = 30;

@implementation MDRadialProgressTheme

+ (id)themeWithName:(const NSString *)themeName
{
	return [[MDRadialProgressTheme alloc] init];
}

+ (id)standardTheme
{
	return [MDRadialProgressTheme themeWithName:STANDARD_THEME];
}

- (id)init
{
	self = [super init];
	if (self) {
		// View
		self.completedColor =  [UIColor colorWithRed:12.0/255.0f green:174.0/255.0f blue:180.0/255.0f alpha:1.0f];
		self.incompletedColor =[UIColor colorWithRed:0.8 green:0.8 blue:0.8 alpha:1.0];
		self.sliceDividerColor = [UIColor whiteColor];
		self.centerColor = [UIColor clearColor];
		self.thickness = 10;
       // self.thickness = 15;
		self.sliceDividerHidden = NO;
		self.sliceDividerThickness = 2;
		
		// Label
		self.labelColor = [UIColor colorWithRed:12.0/255.0f green:174.0/255.0f blue:180.0/255.0f alpha:1.0f];
		self.dropLabelShadow = YES;
		self.labelShadowColor =[UIColor clearColor];
        //[UIColor colorWithRed:247/255.0 green:247/255.0 blue:247/255.0 alpha:1.0];
		self.labelShadowOffset = CGSizeMake(1, 1);
		self.font = [UIFont fontWithName:@"Muli" size:15];
	}
	
	return self;
}

@end
