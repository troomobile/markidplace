//
//  FilterCell.h
//  MarkidPlace
//
//  Created by stellent_mac_05 on 04/07/14.
//  Copyright (c) 2014 stellentmac1. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FilterCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *filternamelbl;
@property (strong, nonatomic) IBOutlet UILabel *namelbl;
;
@property (strong, nonatomic) IBOutlet UIButton *dropdownbtn;
@property (strong, nonatomic) IBOutlet UITextField *OtherTF;

@end
