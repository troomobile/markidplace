//
//  SupportViewController.m
//  MarkidPlace
//
//  Created by Varma Bhupatiraju on 10/09/14.
//  Copyright (c) 2014 stellentmac1. All rights reserved.
//

#import "SupportViewController.h"
#import "SuggestionsViewController.h"
#import "Supportmodelobject.h"
#import "DeleteaccountViewController.h"
@interface SupportViewController ()

@end

@implementation SupportViewController
@synthesize navbar,firstarry,firsttableview,supportlbl,secondarrray;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.navbar.frame = CGRectMake(0, 0, 320, 64);
    [self.navbar setBackgroundImage:[UIImage imageNamed:@"navbar.png"] forBarMetrics:UIBarMetricsDefault];
  self.supportlbl.font = [UIFont fontWithName:@"Monroe" size:18];
    firstarry =[[NSMutableArray alloc]initWithObjects:@"Delete Your Account",@"Contact MarKIDplace Support", nil];
    secondarrray =[[NSMutableArray alloc]init];
    
    for (int i=0;i<[firstarry count];i++)
    {
        Supportmodelobject *obj;
        obj=[[Supportmodelobject alloc]init];
        obj.namestr=[firstarry objectAtIndex:i];
        NSLog(@"name passing....%@",obj.namestr);
        [secondarrray addObject:obj];
        // NSLog(@"secondarray is....%@",[secondarray description]);
        
    }
    NSLog(@"secondarray is....%@",[secondarrray description]);
    
    

    
    // Do any additional setup after loading the view from its nib.
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return  44;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return  secondarrray.count;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
      DeleteaccountViewController *deleteVC;
    if (indexPath.row==0)
    {
        
    if(myappDelegate.isiphone5==NO)
    {
        deleteVC=[[DeleteaccountViewController alloc]initWithNibName:@"DeleteaccountViewController_iphone4" bundle:nil];
    }
    else if([[UIScreen mainScreen] bounds].size.height == 568)
    {
        deleteVC=[[DeleteaccountViewController alloc]initWithNibName:@"DeleteaccountViewController" bundle:nil];
    }
    
    [self.navigationController pushViewController:deleteVC animated:YES];
    }
    else
    {
        MFMailComposeViewController *mailComposer = [[MFMailComposeViewController alloc] init];
        
        if(mailComposer!= nil)
        {
            NSMutableArray *toArray=[[NSMutableArray alloc]initWithObjects:ksupportemail, nil];
            mailComposer.mailComposeDelegate = self;
            [mailComposer setToRecipients:finalinvitearray];
            [mailComposer setSubject:@"marKIDplace support"];
            [mailComposer setToRecipients:toArray];
            [mailComposer setMessageBody:@"Hi. I'm having an issue. Let me describe it below" isHTML:YES];
            mailComposer.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
             mailComposer.navigationController.navigationBar.titleTextAttributes = [NSDictionary dictionaryWithObject:[UIColor blackColor] forKey:UITextAttributeTextColor];
            // mailComposer.navigationBar.tintColor=[UIColor clearColor];
            [self presentViewController:mailComposer animated:YES completion:nil];
        }

        
        
    }

    
}
-(void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    if ( result == MFMailComposeResultFailed )
    {
        // Sending failed - display an error message to the user.
        /*     NSString* message = [NSString stringWithFormat:@"Error sending email '%@'. Please try again, or cancel the operation./Users/varmabhupatiraju/Documents/Ajay/TabBarproject/TabBarproject/ThirdViewController.m", [error localizedDescription]];
         UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@"Error Sending Email" message:message delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
         [alertView show];*/
        
    }else
        [self dismissViewControllerAnimated:YES completion:^{     }];
    
}
-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellidentifier=@"cell";
    SupportcustomCell *cell=(SupportcustomCell *)[firsttableview dequeueReusableCellWithIdentifier:cellidentifier];
    if (cell==nil)
    {
        [[NSBundle mainBundle]loadNibNamed:@"SupportcustomCell" owner:self options:nil];
        cell=self.mySupportcustomCell;
        
    }
     cell.namelbl.font = [UIFont fontWithName:@"Muli" size:15];
    Supportmodelobject *bobj=[secondarrray objectAtIndex:indexPath.row];
    NSLog(@"bobj.namestr...%@",bobj.namestr);
     cell.namelbl.text=bobj.namestr;
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    return  cell;
}

-(IBAction)backAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
    
}

-(IBAction)homeBtnAction:(id)sender;
{
    
        NSArray *arr=self.navigationController.viewControllers;
        for (UIViewController *childvc in arr)
        {
            
            if([childvc isKindOfClass:[SuggestionsViewController class]])
            {
                SuggestionsViewController *subvc=(SuggestionsViewController *)childvc;
                subvc.ismyfeed=NO;
                [self.navigationController popToViewController:subvc animated:YES];
            }
        }
        
}
    

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)internetConnection
{
    UIAlertView *Internetalert=[[UIAlertView alloc]initWithTitle:appname message:@"Internet connection not available" delegate:self cancelButtonTitle:nil otherButtonTitles:@"Ok", nil];
    [Internetalert show];
    [appdelegate stopspinner:self.view];
    
}
@end
