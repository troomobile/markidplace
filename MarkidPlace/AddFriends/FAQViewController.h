//
//  FAQViewController.h
//  MarkidPlace
//
//  Created by stellent_mac_05 on 24/09/14.
//  Copyright (c) 2014 stellentmac1. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

@class AppDelegate;
@interface FAQViewController : UIViewController
{
    IBOutlet UIWebView *webview;
    IBOutlet UIActivityIndicatorView *spinner;
    AppDelegate *appdelegate;
    
}
@property(nonatomic,strong) NSString *htmlMainString;
@property(nonatomic,strong) NSString *titleString;


@property(nonatomic,strong) IBOutlet UINavigationBar *navbar;
@property(nonatomic,strong) IBOutlet UILabel *titlelbl;

@property(nonatomic,strong) NSString *isFAQ;

-(IBAction)backBtnPressed:(id)sender;



@end
