//
//  AddFriendsViewController.h
//  Gameizm
//
//  Created by stellent on 08/08/14.
//  Copyright (c) 2014 stellent. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AddressBookUI/AddressBookUI.h>
#import <Twitter/Twitter.h>
#import "STTwitter.h"
#import <FacebookSDK/FacebookSDK.h>
#import <Accounts/Accounts.h>
#import <Social/Social.h>
#import "AppDelegate.h"
#import "AddFriendsCell.h"

@interface AddFriendsViewController : UIViewController<ABNewPersonViewControllerDelegate,ABPeoplePickerNavigationControllerDelegate,ABPersonViewControllerDelegate,UINavigationControllerDelegate,UITableViewDataSource,UITableViewDelegate,FBFriendPickerDelegate>
{
    ABPeoplePickerNavigationController *peoplepicker;
   
    //twitter
    AppDelegate*appdelegate;
    STTwitterAPI *twitter;
    NSString *loggedinUserName;
    NSMutableArray *totalIDsArray;
    NSMutableArray *usersArray;
    NSMutableArray *userNamesArray;
    ACAccountStore *account;
    ACAccountType *accountType;
    
    int maximumCount;
    int minimumCount;
    int skipCount;
    int maincount;
    BOOL isLoadingRecords;
    IBOutlet UIActivityIndicatorView *spinner;
    
    NSString *isfrom;
}
@property (nonatomic,strong) NSMutableDictionary *followersDictionary;
@property (nonatomic,strong)NSMutableArray *keyArray;
@property (nonatomic,strong) IBOutlet UITableView *friendsTableView;
@property(nonatomic,strong) NSString *typeString;
//@property(nonatomic,strong) NSString *typeString;
@property(nonatomic,strong)IBOutlet AddFriendsCell *addfriendscell;

-(IBAction)back:(id)sender;

@property (nonatomic,strong) IBOutlet UINavigationBar *navbar;
@property (nonatomic,strong) IBOutlet UILabel *titlelbl;

//@property(nonatomic,strong) NSString *isfrom;
-(IBAction)backbtnAction:(id)sender;
-(IBAction)invitebtnAction:(id)sender;
-(IBAction)twitterBtnAction:(id)sender;

@end


