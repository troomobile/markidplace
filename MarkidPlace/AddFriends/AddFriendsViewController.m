//
//  AddFriendsViewController.m
//  Gameizm
//
//  Created by stellent on 08/08/14.
//  Copyright (c) 2014 stellent. All rights reserved.
//

#import "AddFriendsViewController.h"
#import "FollowersObject.h"
#import "STTwitterAPI.h"

@interface AddFriendsViewController ()

@end

@implementation AddFriendsViewController
@synthesize typeString;
@synthesize followersDictionary,keyArray;
@synthesize friendsTableView,navbar,titlelbl,addfriendscell;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    appdelegate=(AppDelegate *)[[UIApplication sharedApplication]delegate];
    
    self.navbar.frame = CGRectMake(0, 0, 320, 64);
    [self.navbar setBackgroundImage:[UIImage imageNamed:@"navbar.png"] forBarMetrics:UIBarMetricsDefault];
    self.titlelbl.font = [UIFont fontWithName:@"Monroe" size:18];
    [self twitterBtnAction :nil];
    
       // Do any additional setup after loading the view from its nib.
}
-(IBAction)twitterBtnAction:(id)sender
{
    [APPDELEGATE.Twitterfollowers removeAllObjects];
    if(!usersArray)
        usersArray=[[NSMutableArray alloc] init];
    if(!userNamesArray)
        userNamesArray=[[NSMutableArray alloc] init];
    
    followersDictionary = [[NSMutableDictionary alloc] init];
    
    maincount=0;
    twitter = [STTwitterAPI twitterAPIOSWithFirstAccount];
    
    //loggedinUserName = @"Trying to login with iOS...";
    loggedinUserName = @"";
    [spinner startAnimating];
    [appdelegate startspinner:self.view];
    [twitter verifyCredentialsWithSuccessBlock:^(NSString *username) {
        loggedinUserName = username;
        //  NSLog(@"twitter username is %@",loggedinUserName);
        [self getFollowersIdList];
       
    } errorBlock:^(NSError *error)
     {
         //loggedinUserName = [error localizedDescription];
         [spinner stopAnimating];
         
         [appdelegate stopspinner:self.view];
         
         [self showAlert:[error localizedDescription]];
         
     }];
}
//for status bar white color
-(UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

-(IBAction)back:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:^{
        
    }];
}

//getting followers id's
-(void)getFollowersIdList
{
   // NSLog(@"checking username ---- %@",loggedinUserName);
    [twitter getFollowersIDsForScreenName:loggedinUserName  successBlock:^(NSArray *followers)
     {
         maincount=(int)followers.count;
       
        
        totalIDsArray=[[NSMutableArray alloc] init];
         [APPDELEGATE.Twitterfollowers removeAllObjects];
        [totalIDsArray addObjectsFromArray:followers];
         [APPDELEGATE.Twitterfollowers addObjectsFromArray:followers];
         //NSLog(@"the APPDELEGATE.Twitterfollowers %@ ",APPDELEGATE.Twitterfollowers.description);
         if([self.typeString isEqualToString:@"invite"])
         {
               if(([totalIDsArray count]-[usersArray count])>100)
               {
            maximumCount+=100;
               }
       
        else
        {
            maximumCount+=[totalIDsArray count]-[usersArray count];
        }
         
        
        // maximumCount=numberOfTurns;
        
        [self getRecords];
         }
        
                
    } errorBlock:^(NSError *error)
    {
        [spinner stopAnimating];
        [appdelegate stopspinner:self.view];

      //  NSLog(@"Error is %@",[error localizedDescription]);
    }];
    
}
//getting followers details
-(void) getRecords
{
    
    NSLog(@"the  maximum count iss  %d",maximumCount);
    NSMutableArray *tmparr=[[NSMutableArray alloc] init];
    
    for (int i=minimumCount;i<maximumCount;i++)
    {
        
        [tmparr addObject:[totalIDsArray objectAtIndex:i]];
        
    }
    
    NSString *str= [tmparr componentsJoinedByString:@","];
    //NSLog(@"str----------Krish----- %@",str);
         [twitter getUsersLookupForScreenName:nil orUserID:str includeEntities:nil successBlock:^(NSArray *users) {
       // NSLog(@"the twitter username and emals description is %@+++=++=====++=+++==",users.description);
             if (users)
             {
                 skipCount+=[tmparr count]-[users count];
             }
        
        
        //[usersArray addObjectsFromArray:users];
        
        for (NSDictionary *status in users)
        {
            
            FollowersObject *follower=[[FollowersObject alloc] init];
            follower.followerid=[status valueForKey:@"id_str"];
            //[APPDELEGATE.Twitterfollowers addObject:follower];
            follower.screen_name=[status valueForKey:@"screen_name"];
           // NSLog(@"the follower screen name is %@",follower.screen_name);
           // follower.imageurl=[status valueForKey:@""];
            
           // erger
            
             //NSLog(@"follower.followerid %@",follower.followerid);
            //NSLog(@"follower.screen_name %@",follower.screen_name);
            
            [userNamesArray addObject:[status valueForKey:@"screen_name"]];
            [usersArray addObject:follower];
            
        }
        
        minimumCount+=[tmparr count];
        
        if(([totalIDsArray count]-[usersArray count]+skipCount)>100)
            maximumCount+=100;
        else
            maximumCount+=[totalIDsArray count]-([usersArray count]+skipCount);
        
        
        // NSLog(@"users array is %d",usersArray.count);
        
       
        
        if([self.followersDictionary count]>0)
            [self.followersDictionary removeAllObjects];
        
        self.followersDictionary =[self fillingDictionary:userNamesArray];
        
        isLoadingRecords=NO;
        
        [self.friendsTableView reloadData];
       [spinner stopAnimating];
        [appdelegate stopspinner:self.view];

        
    } errorBlock:^(NSError *error) {
        
        // NSLog(@"error is %@",[error localizedDescription]);
        [spinner stopAnimating];
        [appdelegate stopspinner:self.view];

        [self showAlert:[error localizedDescription]];
    }];
    
}
-(void) showAlert:(NSString *)message
{
    //NSLog(@"THE MESSAGE IS %@",message);
    if([message isEqualToString:@"Too many terms specified in query."]||[message isEqualToString:@"Over capacity"]||[message isEqualToString:@""])
    {
        
    }
    else
    {
    UIAlertView *alert=[[UIAlertView alloc] initWithTitle:appname message:message delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        if([typeString isEqualToString:@"invite"])
        {
         [alert show];
        }
    }
}

-(NSMutableDictionary *)fillingDictionary:(NSMutableArray *)ary
{
    
    // This method has the real magic of this sample
    
    if(!self.keyArray)
        self.keyArray=[[NSMutableArray alloc]init];
    
    if([self.keyArray count]>0)
        [self.keyArray removeAllObjects];
    
    NSMutableDictionary *dic=[[NSMutableDictionary alloc]init];
    
    // First sort the array
    
    [ary sortUsingSelector:@selector(compare:)];
    
    for(NSString *str in ary)
    {
        unichar charval=[[str capitalizedString] characterAtIndex:0];
        
        //NSLog(@"char val is %d",charval);
        
        NSString *charStr=[NSString stringWithFormat: @"%C", charval];
        
        //NSLog(@"char str is %@",charStr);
        
        if(![self.keyArray containsObject:charStr])
        {
            NSMutableArray *charArray=[[NSMutableArray alloc]init];
            [charArray addObject:str];
            [self.keyArray addObject:charStr];
            [dic setValue:charArray forKey:charStr];
        }
        else
        {
            NSMutableArray *prevArray=(NSMutableArray *)[dic valueForKey:charStr];
            [prevArray addObject:str];
            [dic setValue:prevArray forKey:charStr];
        }
        
    }
    return dic;
}

#pragma mark TableView Methods

-(NSInteger )numberOfSectionsInTableView:(UITableView *)tableView
{
    if(userNamesArray.count+skipCount<totalIDsArray.count)
        return  [self.keyArray count]+1;
    
    else
        return  [self.keyArray count];

}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(section==self.keyArray.count)
    {
        
        return 1;
    }
    else
    {
        
        NSArray *ary=[self.followersDictionary valueForKey:[self.keyArray objectAtIndex:section]];
        
        return [ary count];
    }
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"cell";
    
    static NSString *MyIdentifier= @"addfriends";
    AddFriendsCell *cell= (AddFriendsCell *)[tableView dequeueReusableCellWithIdentifier:MyIdentifier];
    
    if (cell==nil)
    {
        [[NSBundle mainBundle]loadNibNamed:@"AddFriendsCell" owner:self options:nil];
        
        cell=addfriendscell;
        cell.selectionStyle=UITableViewCellSelectionStyleNone;

    }

    cell.namelbl.font = [UIFont fontWithName:@"Muli" size:16];
    cell.invitebtn.tag=indexPath.row;
    if(indexPath.section==self.keyArray.count)
    {
        cell.namelbl.text=@"Loading more records...";
        cell.namelbl.textColor=[UIColor colorWithRed:25.0/250.0 green:170.0/250.0 blue:158.0/250.0 alpha:1.0];
        cell.invitebtn.hidden=YES;
       // cell.textLabel.font=[UIFont boldSystemFontOfSize:20];
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
        
        if(!isLoadingRecords)
        {
            isLoadingRecords=YES;
            [self getRecords];
        }
        
    }
    else
    {
         dispatch_async(dispatch_get_main_queue(), ^{
        NSString *key=[self.keyArray objectAtIndex:[indexPath section]];
        NSArray *array=(NSArray *)[self.followersDictionary valueForKey:key];
             cell.invitebtn.hidden=NO;

        NSString *cellTitle=[array objectAtIndex:[indexPath row]];
             
        //cell.accessoryType=UITableViewCellAccessoryDisclosureIndicator;
        [cell.namelbl setText:cellTitle];
         });
    }
    return cell;
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/////////////////////////


//This is for the invite the friends
-(void)followmethod
{
    if([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter])
    {
    ACAccountStore *account = [[ACAccountStore alloc] init];
    ACAccountType *accountType = [account accountTypeWithAccountTypeIdentifier:
                                  ACAccountTypeIdentifierTwitter];
    
    [account requestAccessToAccountsWithType:accountType options:nil
                                  completion:^(BOOL granted, NSError *error)
    {
        if (granted == YES)
        {
            NSArray *arrayOfAccounts = [account
                                        accountsWithAccountType:accountType];
            
            if ([arrayOfAccounts count] > 0)
            {
                ACAccount *twitterAccount =
                [arrayOfAccounts lastObject];
                
                NSDictionary *message = @{@"status": @"My First Twitter post"};
                
                NSURL *requestURL = [NSURL
                                     URLWithString:@"http://api.twitter.com/1/statuses/update.json"];
                
                SLRequest *postRequest = [SLRequest
                                          requestForServiceType:SLServiceTypeTwitter
                                          requestMethod:SLRequestMethodPOST
                                          URL:requestURL parameters:message];
            }
        }
    }];
         }
     }
-(IBAction)backbtnAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
-(IBAction)invitebtnAction:(id)sender
{
    if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter]) {
        
        SLComposeViewController *mySLComposerSheet = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
            [mySLComposerSheet setInitialText:kinvitaion];
               //  [mySLComposerSheet addURL:[NSURL URLWithString:@"http://stackoverflow.com/questions/12503287/tutorial-for-slcomposeviewcontroller-sharing"]];
        
        [mySLComposerSheet setCompletionHandler:^(SLComposeViewControllerResult result)
         {
             
             switch (result)
             {
                 case SLComposeViewControllerResultCancelled:
                     //   NSLog(@"Post Canceled");
                     
                     ///    [twitterButton setImage:[UIImage imageNamed:@"twitter_Img.png"] forState:UIControlStateNormal];
                     break;
                 case SLComposeViewControllerResultDone:
                     
                     break;
                     
                 default:
                     break;
             }
         }];
        
        [self presentViewController:mySLComposerSheet animated:YES completion:nil];
    }
    else
    {
        // [twitterButton setImage:[UIImage imageNamed:@"twitter_Img.png"] forState:UIControlStateNormal];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"No Twitter Accounts" message:@"For some reason, Twitter is not recognizing marKIDplace. To fix, please tap your phone's Settings icon, scroll down to Twitter, where you should see your Twitter login credentials (if you don't, you need to log in) and there you should see a marKIDplace icon also (if you don't, you need to refresh the permissions again). Toggle marKIDplace to the \"ON\" position and you should be connected." delegate:self                                                       cancelButtonTitle:@"OK"  otherButtonTitles:nil];
        alert.tag = 12;
        [alert show];
        
    }
}
-(void)internetConnection
{
    UIAlertView *Internetalert=[[UIAlertView alloc]initWithTitle:appname message:@"Internet connection not available" delegate:self cancelButtonTitle:nil otherButtonTitles:@"Ok", nil];
    [Internetalert show];
    [appdelegate stopspinner:self.view];
    
}
@end
