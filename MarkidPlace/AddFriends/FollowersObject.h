//
//  FollowersObject.h
//  OAuthTwitterDemo
//
//  Created by Varma Bhupatiraju on 30/05/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FollowersObject : NSObject
{
    NSString *followerid;
    NSString *screen_name;
}
@property(nonatomic, strong) NSString *followerid;
@property(nonatomic, strong) NSString *screen_name;
@property(nonatomic, strong) NSString *imageurl;
@property (nonatomic,readwrite)BOOL isstatus;

@end
