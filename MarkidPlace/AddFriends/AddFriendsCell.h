//
//  AddFriendsCell.h
//  MarkidPlace
//
//  Created by stellent_mac_05 on 24/09/14.
//  Copyright (c) 2014 stellentmac1. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AddFriendsCell : UITableViewCell

@property(nonatomic,strong) IBOutlet UILabel *namelbl;
@property(nonatomic,strong) IBOutlet UIButton *invitebtn;


@end
