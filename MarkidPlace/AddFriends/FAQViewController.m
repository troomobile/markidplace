//
//  FAQViewController.m
//  MarkidPlace
//
//  Created by stellent_mac_05 on 24/09/14.
//  Copyright (c) 2014 stellentmac1. All rights reserved.
//

#import "FAQViewController.h"

@interface FAQViewController ()

@end

@implementation FAQViewController
@synthesize navbar,titlelbl,htmlMainString,titleString,isFAQ;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    appdelegate=(AppDelegate *)[[UIApplication sharedApplication]delegate];
    
    self.navbar.frame = CGRectMake(0, 0, 320, 64);
    [self.navbar setBackgroundImage:[UIImage imageNamed:@"navbar.png"] forBarMetrics:UIBarMetricsDefault];
    self.titlelbl.font = [UIFont fontWithName:@"Monroe" size:18];
    if([isFAQ isEqualToString:@"Yes"])
     {
         self.titlelbl.text=@"FAQ";
    NSString *htmlFile = [[NSBundle mainBundle] pathForResource:htmlMainString  ofType:@"html" inDirectory:nil];
    NSString* htmlString = [NSString stringWithContentsOfFile:htmlFile encoding:NSUTF8StringEncoding error:nil];
    [webview loadHTMLString:htmlString baseURL:nil];
     }
    else if ([isFAQ isEqualToString:@"No"])
    {
        self.titlelbl.text=@"TRACK";
        NSURL *websiteUrl = [NSURL URLWithString:htmlMainString];
        NSURLRequest *urlRequest = [NSURLRequest requestWithURL:websiteUrl];
        [webview loadRequest:urlRequest];
    }
    

    // Do any additional setup after loading the view from its nib.
    
    

}
-(UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

-(IBAction)backBtnPressed:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)webViewDidStartLoad:(UIWebView *)webView
{
     [spinner startAnimating];
}
-(void)webViewDidFinishLoad:(UIWebView *)webView
{
      [spinner stopAnimating];
}

-(void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    
    // NSLog(@"Error for WEBVIEW: %@", [error description]);
    [spinner stopAnimating];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
