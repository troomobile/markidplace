//
//  SuggestionsViewController.m
//  MarkidPlace
//
//  Created by stellentmac1 on 5/21/14.
//  Copyright (c) 2014 stellentmac1. All rights reserved.
//

#import "MyWishlistViewController.h"
#import "ProfileViewController.h"

@interface MyWishlistViewController ()

@end

@implementation MyWishlistViewController
@synthesize navbar,itemLabel1,itemLabel2,itempriceLabel1,itempriceLabel2,oldpriceLabel1,oldpriceLabel2,myAccountLabel,myClosetLabel,myfeedLabel,mywishlistLabel,shopallLabel,sellLabel,cartcountLabel,notificationcountlabel,notificationview;
@synthesize menuBtn,menuview;
@synthesize itemscrollview,myfeedImageview,mySuggestionsImageview,filterImageview,animationimgv,menubtnview;


@synthesize dataloderstring,cartview;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
#pragma mark view methods
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    notificationview.hidden=YES;

  //  myappDelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    registrationRep=[[RegistrationRepository alloc ]init];
    registrationRep.delegate=self;
    [myappDelegate horseanimation:animationimgv];
    self.navbar.frame = CGRectMake(0, 0, 320, 64);
    [self.navbar setBackgroundImage:[UIImage imageNamed:@"navbar.png"] forBarMetrics:UIBarMetricsDefault];
    
    
    [self myWishMethod];
}

-(void)myWishMethod
{
    itemsarray =[NSMutableArray new];
    
    self.itemLabel1.font = [UIFont fontWithName:@"Muli" size:13];
    self.itemLabel2.font = [UIFont fontWithName:@"Muli" size:13];
    self.itempriceLabel1.font = [UIFont fontWithName:@"Muli" size:13];
    self.itempriceLabel1.font = [UIFont fontWithName:@"Muli" size:13];
    self.itempriceLabel2.font = [UIFont fontWithName:@"Muli" size:13];
    self.oldpriceLabel1.font = [UIFont fontWithName:@"Muli" size:13];
    self.oldpriceLabel2.font = [UIFont fontWithName:@"Muli" size:13];
    self.myAccountLabel.font = [UIFont fontWithName:@"Muli" size:15];
    self.myClosetLabel.font = [UIFont fontWithName:@"Muli" size:15];
    self.myfeedLabel.font = [UIFont fontWithName:@"Muli" size:15];
    self.mywishlistLabel.font = [UIFont fontWithName:@"Muli" size:15];
    self.shopallLabel.font = [UIFont fontWithName:@"Muli" size:15];
    self.sellLabel.font = [UIFont fontWithName:@"Muli" size:15];
    self.cartcountLabel.font = [UIFont fontWithName:@"Muli" size:11];
    
    
    refreshControl = [[ODRefreshControl alloc] initInScrollView:self.itemscrollview];
    
    refreshControl.tintColor=[UIColor colorWithRed:12.0/255.0 green:183.0/255.0  blue:170.0/255.0  alpha:1.0];
    [refreshControl addTarget:self action:@selector(dropViewDidBeginRefreshing:) forControlEvents:UIControlEventValueChanged];
    refreshControl.activityIndicatorViewColor=[UIColor colorWithRed:12.0/255.0 green:183.0/255.0  blue:170.0/255.0  alpha:1.0];
    
    
    
    screenSize = [[UIScreen mainScreen]bounds];
    
    if (myappDelegate.isiphone5)
    {
        menubtnview.frame=CGRectMake(131,470, 58, 59);
    }
    else
    {
        menubtnview.frame=CGRectMake(131,355, 58, 59);
    }
    
    
    self.view.userInteractionEnabled=YES;
    
    [self.view addSubview:menuview];
    [self.view addSubview:menubtnview];
    
    itemscrollview.contentSize=CGSizeMake(itemscrollview.frame.size.width,itemscrollview.frame.size.height+100);
    
    myappDelegate.useridstring=[myappDelegate getStringFromPlist:@"userid"];
    
    titlelbl.font =[AppDelegate navtitlefont];
}
//this animation is performs when the animation is loaded
-(void)animateview
{
    itemscrollview.frame=CGRectMake(0, 250, 320, 504);
    [UIView animateWithDuration:0.2 animations:^{
        CGRect height=itemscrollview.frame;
        height.origin.y=itemscrollview.frame.origin.y-195;
        itemscrollview.frame=height;
        [self zoomBtn];
    }];
    if (myappDelegate.isiphone5)
    {
        itemscrollview.frame=CGRectMake(0, 64, 320, 504);
    }
    else{
        itemscrollview.frame=CGRectMake(0, 64, 320, 416);
    }
}
//this method go to the only show the like images
-(void)zoomBtn
{
    CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"transform.scale"];
    // Set the initial and the final values
    [animation setFromValue:[NSNumber numberWithFloat:0.4f]];
    [animation setToValue:[NSNumber numberWithFloat:1.00f]];
    
    // Set duration
    [animation setDuration:0.4f];
    
    // Set animation to be consistent on completion
    [animation setRemovedOnCompletion:NO];
    [animation setFillMode:kCAFillModeForwards];
    
    // Add animation to the view's layer
    [[itemscrollview layer] addAnimation:animation forKey:@"scale"];
}


- (void)dropViewDidBeginRefreshing:(ODRefreshControl *)refresh
{
    [myappDelegate.mylikesGlobalarray removeAllObjects];
    [refreshControl beginRefreshing ];
    [self setMenuDown];
    [self viewWillAppear:NO];
}
-(void)setMenuDown
{
    CGRect menu=menuview.frame;
    CGRect menuButtonframe=menubtnview.frame;
    if (myappDelegate.isiphone5)
    {
        if(ismenu==YES)
        {
            ismenu=NO;
            // menuBtn.frame=CGRectMake(131,470, 58, 59);
            menuButtonframe=CGRectMake(131,470, 58, 59);
            menu=menuview.frame;
            menu.origin.y=menuview.frame.origin.y+600;
            // menuview.frame=menu;
            self.view.userInteractionEnabled=NO;
        }
        self.view.userInteractionEnabled=YES;
    }
    else
    {
        if(ismenu==YES)
        {
            
            
            ismenu=NO;
            
            // menuBtn.frame=CGRectMake(131,355, 58, 59);
            menuButtonframe=CGRectMake(131,355, 58, 59);
            menu=menuview.frame;
            menu.origin.y=menuview.frame.origin.y+482;
            // menuview.frame=menu;
            self.view.userInteractionEnabled=NO;
            
        }
    }
    [UIView animateWithDuration:0.5 animations:^{
        
        //        CGRect rect= usersTableview.frame;
        //        rect.origin.y=64;
        //        usersTableview.frame=rect;
        
        
        menuview.frame=menu;
        menubtnview.frame=menuButtonframe;
        
    } completion:^(BOOL finished) {
        
    }];
    
    self.view.userInteractionEnabled=YES;
    
}
-(void)viewWillAppear:(BOOL)animated
{
    
    int value =[[APPDELEGATE getStringFromPlist:@"notificationcount"] intValue];
    NSLog(@"value....%d",value);

    
    if (value>9)
    {
        notificationcountlabel.text=@"9+";
        notificationview.hidden=NO;

        
    }
    else if (value==0)
    {
        
        notificationview.hidden=YES;
        
    }
    else
    {
        
        notificationcountlabel.text=[APPDELEGATE getStringFromPlist:@"notificationcount"];
        notificationview.hidden=NO;

    }
    

    [myaccountImgView setImage:[UIImage imageNamed:@"myaccount_off.png"]];
    [myclosetImgView setImage:[UIImage imageNamed:@"mycloset_off.png"]];
    [myfeedImgView setImage:[UIImage imageNamed:@"myfeed_off.png"]];
    [mywishlistImgView setImage:[UIImage imageNamed:@"mywishlist_on.png"]];
    [shopAllImgView setImage:[UIImage imageNamed:@"shopall_off.png"]];
    [sellImgView setImage:[UIImage imageNamed:@"sell_off.png"]];
   
     //registrationRep=[[RegistrationRepository alloc ]init];
    //registrationRep.delegate=self;
    
    
    if (myappDelegate.cartGlobalarray.count>0)
    {
        cartcountLabel.text=[NSString stringWithFormat:@"%d",myappDelegate.cartGlobalarray.count];
        cartview.hidden=NO;
        
    }
    else
    {
        cartview.hidden=YES;
    }
    
    itemscrollview.hidden=YES;
    
    if (myappDelegate.mylikesGlobalarray.count<1||myappDelegate.suggestionviewcont.loaddatabool==YES)
    {
        [myappDelegate startspinner:self.view];
       
        if ([myappDelegate isnullorempty:dataloderstring])
        {
            [myappDelegate startspinner:self.view];
            [registrationRep getdata:nil :mywishlist :@"GET" withcount:@"0"];
        }
        else{
            [registrationRep getdata:nil :@"getallLikes" :@"GET" withcount:@"0"];
        }
        
        
        
    }
    else
    {
        [self createScrollView];
    }
    
    
}
#pragma mark Button actions


-(void)viewWillDisappear:(BOOL)animated
{
    /*
    for (ASIHTTPRequest *req in ASIHTTPRequest.sharedQueue.operations)
    {
        [req cancel];
        [req setDelegate:nil];
    }
*/
    NSLog(@" viewWillDisappear:(BOOL)animated");
    itemscrollview.hidden=YES;
}
//this action performs navigation actions
-(IBAction)backAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(IBAction)myFeedButtonTapped:(id)sender
{
    [self setMenuDown];
    itemscrollview.hidden=NO;
    myfeedImageview.image=[UIImage imageNamed:@"myfeedbtn_on.png"];
    mySuggestionsImageview.image=[UIImage imageNamed:@"mysuggestionbtn@2x"];
    filterImageview.image=[UIImage imageNamed:@"filterbtn_off@2x.png"];
    
    menuBtn.hidden=NO;
    itemscrollview.hidden=NO;
    menuview.hidden=NO;
}

-(IBAction)mySuggestionsButtonTapped:(id)sender
{
    [self setMenuDown];
    itemscrollview.hidden=NO;
    myfeedImageview.image=[UIImage imageNamed:@"myfeedbtn_off.png"];
    mySuggestionsImageview.image=[UIImage imageNamed:@"mysuggestions@2x"];
    filterImageview.image=[UIImage imageNamed:@"filterbtn_off@2x.png"];
    menuBtn.hidden=YES;
    itemscrollview.hidden=YES;
    menuview.hidden=YES;
}

//this button action performs the shows the listing of carts and go to filter view controller
-(IBAction)filterButtonTapped:(id)sender
{
    
    [self setMenuDown];
    
    if ([sender tag]==330)
    {
        if (myappDelegate.cartGlobalarray.count>0)
        {
            CartViewController *cartVC;
            if (myappDelegate.isiphone5)
            {
                cartVC = [[CartViewController alloc]initWithNibName:@"CartViewController" bundle:nil];
            }
            else
            {
                cartVC = [[CartViewController alloc]initWithNibName:@"CartViewController_iphone4" bundle:nil];
            }
            
            ItemsModelobject *sltobj=[myappDelegate.cartGlobalarray objectAtIndex:0];
            cartVC.itemsModelObj=sltobj;
            [self.navigationController pushViewController:cartVC animated:YES];
            
        }
        else{
            
            [AppDelegate alertmethod:appname :@"Your cart is empty. Please add listing to cart."];
        }
        
    }
    else{
        
        
        
        if (myappDelegate.isiphone5)
        {
            FilterViewController *filterVC=[[FilterViewController alloc] initWithNibName:@"FilterViewController" bundle:nil];
            [self.navigationController pushViewController:filterVC animated:YES];
        }
        else{
            FilterViewController *filterVC=[[FilterViewController alloc] initWithNibName:@"FilterViewController_iphone4" bundle:nil];
            [self.navigationController pushViewController:filterVC animated:YES];
        }
    }
    
}



//this button performs shows the menu's
-(IBAction)menuBtnAction:(id)sender
{
    //notificationcountlabel.text=[APPDELEGATE getStringFromPlist:@"notificationcount"];
   // NSLog(@"notificationcountlabel.text....%@",notificationcountlabel.text);
    int value =[[APPDELEGATE getStringFromPlist:@"notificationcount"] intValue];
    NSLog(@"value....%d",value);
    
    
    if (value>9)
    {
        notificationcountlabel.text=@"9+";
        notificationview.hidden=NO;

        
    }
    else if (value==0)
    {
        
        notificationview.hidden=YES;
        
    }
    else
    {
        
        notificationcountlabel.text=[APPDELEGATE getStringFromPlist:@"notificationcount"];
        notificationview.hidden=NO;

    }
    

    [animationimgv stopAnimating];
    
    animationimgv.image=[UIImage imageNamed:@"hours_0022_Layer-1.png"];
    
    [animationimgv startAnimating];
    
    
    
    CGRect menu=menuview.frame;
    CGRect menuButtonframe=menubtnview.frame;
    
    
    
    if (myappDelegate.isiphone5)
    {
        if(ismenu==YES)
        {
            ismenu=NO;
            
            menu.origin.y=600;
            menuButtonframe.origin.y=470;
            
        }
        else
        {
            //menuButtonframe=CGRectMake(131, 250, 58, 59);
            
            ismenu=YES;
            
            //menuButtonframe.origin.y=menubtnview.frame.origin.y-250;
            menuButtonframe.origin.y=250;
            menu.origin.y=280;
            
        }
        
        self.view.userInteractionEnabled=YES;
        
    }
    else
    {
        if(ismenu==YES)
        {
            
            ismenu=NO;
            
            menuButtonframe=CGRectMake(131,355, 58, 59);
            
            
            menu.origin.y=menuview.frame.origin.y+482;
            
        }
        else
        {
            menuButtonframe=CGRectMake(131, 160, 58, 59);
            
            ismenu=YES;
            menu=menuview.frame;
            menu.origin.y=190;
        }
    }
    
    [UIView animateWithDuration:0.5 animations:^
     {
         
         menuview.frame=menu;
         menubtnview.frame=menuButtonframe;
         
         // [self runSpinAnimationOnView:self.menuBtn duration:1.0 rotations:1.0 repeat:0];
         
     }
                     completion:^(BOOL finished){
                         
                         //[animationimgv stopAnimating];
                         
                         // animationimgv.image=[UIImage imageNamed:@"hours_0022_Layer-1.png"];
                     }];
    
    self.view.userInteractionEnabled=YES;
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 1.0 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
        [animationimgv stopAnimating];
        
        animationimgv.image=[UIImage imageNamed:@"hours_0022_Layer-1.png"];
    });
    
    
}
/*
-(IBAction)menuBtnAction:(id)sender
{
    
    CGRect menu;
    CGRect menuButtonframe;
    
    if (myappDelegate.isiphone5)
    {
        if(ismenu==YES)
        {
            ismenu=NO;
            
            menuButtonframe=CGRectMake(131,470, 58, 59);
            menu=menuview.frame;
            menu.origin.y=menuview.frame.origin.y+600;
            
            self.view.userInteractionEnabled=NO;
        }
        else
        {
            
            menuButtonframe=CGRectMake(131, 250, 58, 59);
            ismenu=YES;
            menu=menuview.frame;
            menu.origin.y=280;
            
        }
        self.view.userInteractionEnabled=YES;
        
    }
    else
    {
        if(ismenu==YES)
        {
            
            
            ismenu=NO;
            
            menuButtonframe=CGRectMake(131,355, 58, 59);
            menu=menuview.frame;
            menu.origin.y=menuview.frame.origin.y+482;
            
            self.view.userInteractionEnabled=NO;
            
        }
        else
        {
            
            menuButtonframe=CGRectMake(131, 160, 58, 59);
            ismenu=YES;
            
            menu=menuview.frame;
            menu.origin.y=190;
        }
    }
    
    [UIView animateWithDuration:0.5 animations:^{
        
        
        menuview.frame=menu;
        menuBtn.frame=menuButtonframe;
        
    } completion:^(BOOL finished) {
        
    }];
    
    self.view.userInteractionEnabled=YES;
    
}

*/

-(IBAction)myAccountAction:(id)sender
{
    [self setMenuDown];
    ProfileViewController *profileVC;
    if (myappDelegate.isiphone5)
    {
        profileVC = [[ProfileViewController alloc]initWithNibName:@"ProfileViewController" bundle:nil];
    }
    else
    {
        profileVC = [[ProfileViewController alloc]initWithNibName:@"ProfileViewController_iphone4" bundle:nil];
    }
    [self.navigationController pushViewController:profileVC animated:YES];
}

//this button shows the list details
-(IBAction)listDetailsAction:(id)sender
{
    [self setMenuDown];
    ListingDetailViewController *listingDetailVC;
    if (myappDelegate.isiphone5)
    {
        listingDetailVC = [[ListingDetailViewController alloc]initWithNibName:@"ListingDetailViewController" bundle:nil];
    }
    else
    {
        listingDetailVC = [[ListingDetailViewController alloc]initWithNibName:@"ListingDetailViewController_iphone4" bundle:nil];
    }
    
    listingDetailVC.listingmainarray=itemsarray;
    listingDetailVC.senderIndex=[sender tag];
    listingDetailVC.condstring=@"Wishlist";
    [self.navigationController pushViewController:listingDetailVC animated:YES];
}

-(IBAction)menuItemTapped:(id)sender
{
    [self setMenuDown];
    switch ([sender tag]) {
        case 1:
        {
            BOOL isVCExist=NO;
            NSArray *arr=self.navigationController.viewControllers;
            for (UIViewController *childvc in arr) {
                
                if([childvc isKindOfClass:[ProfileViewController class]])
                {
                    isVCExist=YES;
                    UIViewController *subvc=(ProfileViewController *)childvc;
                    [self.navigationController popToViewController:subvc animated:YES];
                }
            }
            
            if(!isVCExist)
            {
                ProfileViewController *profileVC;
                if (myappDelegate.isiphone5)
                {
                    profileVC = [[ProfileViewController alloc]initWithNibName:@"ProfileViewController" bundle:nil];
                }
                else
                {
                    profileVC = [[ProfileViewController alloc]initWithNibName:@"ProfileViewController_iphone4" bundle:nil];
                }
                [self.navigationController pushViewController:profileVC animated:YES];
            }
        }
            break;
        case 2:
        {
            BOOL isVCExist=NO;
            NSArray *arr=self.navigationController.viewControllers;
            for (UIViewController *childvc in arr) {
                
                if([childvc isKindOfClass:[MyClosetViewController class]])
                {
                    isVCExist=YES;
                    UIViewController *subvc=(MyClosetViewController *)childvc;
                    [self.navigationController popToViewController:subvc animated:YES];
                }
            }
            
            if(!isVCExist)
            {
                MyClosetViewController *myclosetVC;
                if (myappDelegate.isiphone5)
                {
                    myclosetVC = [[MyClosetViewController alloc]initWithNibName:@"MyClosetViewController" bundle:nil];
                }
                else
                {
                    myclosetVC = [[MyClosetViewController alloc]initWithNibName:@"MyClosetViewController_iphone4" bundle:nil];
                }
                [self.navigationController pushViewController:myclosetVC animated:YES];
            }
        }
            break;
            
        case 3:
        {
            BOOL isVCExist=NO;
            NSArray *arr=self.navigationController.viewControllers;
            for (UIViewController *childvc in arr) {
                
                if([childvc isKindOfClass:[MyFeedViewController class]])
                {
                    isVCExist=YES;
                    UIViewController *subvc=(MyFeedViewController *)childvc;
                    [self.navigationController popToViewController:subvc animated:YES];
                }
            }
            
            if(!isVCExist)
            {
                
                MyFeedViewController *myfeedVC;
                if (myappDelegate.isiphone5)
                {
                    myfeedVC = [[MyFeedViewController alloc]initWithNibName:@"MyFeedViewController" bundle:nil];
                }
                else
                {
                    myfeedVC = [[MyFeedViewController alloc]initWithNibName:@"MyFeedViewController_iphone4" bundle:nil];
                }
                [self.navigationController pushViewController:myfeedVC animated:YES];
            }
        }
            break;
        case 4:
        {
            [self menuBtnAction:self];
        }
            break;
            
        case 5:
        {
            BOOL isVCExist=NO;
            NSArray *arr=self.navigationController.viewControllers;
            for (UIViewController *childvc in arr) {
                
                if([childvc isKindOfClass:[SuggestionsViewController class]])
                {
                    isVCExist=YES;
                    UIViewController *subvc=(SuggestionsViewController *)childvc;
                    [self.navigationController popToViewController:subvc animated:YES];
                }
            }
            
            if(!isVCExist)
            {
                if ([AppDelegate isiphone5])
                {
                    myappDelegate.suggestionviewcont = [[SuggestionsViewController alloc]initWithNibName:@"SuggestionsViewController" bundle:nil];
                }
                else
                {
                    myappDelegate.suggestionviewcont = [[SuggestionsViewController alloc]initWithNibName:@"SuggestionsViewController_iphone4" bundle:nil];
                }
                [self.navigationController pushViewController:myappDelegate.suggestionviewcont animated:YES];
            }
        }
            break;
        case 6:
        {
            BOOL isVCExist=NO;
            NSArray *arr=self.navigationController.viewControllers;
            for (UIViewController *childvc in arr) {
                
                if([childvc isKindOfClass:[NewListingViewController class]])
                {
                    isVCExist=YES;
                    UIViewController *subvc=(NewListingViewController *)childvc;
                    [self.navigationController popToViewController:subvc animated:YES];
                }
            }
            
            if(!isVCExist)
            {
                
                if (![[APPDELEGATE getStringFromPlist:@"paypalstr"] isEqualToString:@""] && [APPDELEGATE getStringFromPlist:@"paypalstr"].length>3)
                {
                    
                    NewListingViewController *mywishlistVC;
                    if (myappDelegate.isiphone5)
                    {
                        mywishlistVC = [[NewListingViewController alloc]initWithNibName:@"NewListingViewController" bundle:nil];
                    }
                    else
                    {
                        mywishlistVC = [[NewListingViewController alloc]initWithNibName:@"NewListingViewController_iphone4" bundle:nil];
                    }
                    
                    [self.navigationController pushViewController:mywishlistVC animated:YES];
                    
                }
                else
                {
                    [AppDelegate alertmethod:@"Warning" :@"PayPal Email Address is required for Creating A Listing, Please enter your PayPal Email Address in your profile."] ;
                }
                

            }
        }
            break;
        default:
            break;
    }
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)successresponceListing :(id)respdict :(NSString *)paramname;
{
    //[myappDelegate.mylikesGlobalarray removeAllObjects];///azhar
    if ([paramname isEqualToString:@"LikeItem"])
    {
        NSLog(@"resp dict %@",respdict);
        
        if ([respdict isKindOfClass:[NSDictionary class]])
        {
            NSDictionary *mydict=(NSDictionary *)respdict;
            
            NSString *messagestr=[mydict valueForKey:@"message"];
            if ([messagestr isEqualToString:@"Wishlist Saved Successfully"] || [messagestr isEqualToString:@"Wishlist Delted Successfully"])
            {
                
                ItemsModelobject *itemobj=[itemsarray objectAtIndex:likeindex];
                
                if ([messagestr isEqualToString:@"Wishlist Saved Successfully"])
                {
                    [myappDelegate.allLikesGlobalarray addObject:[NSString stringWithFormat:@"%@",[mydict objectForKey:@"userListingID"] ]];
                    
                    itemobj.likedbool=YES;
                }
                else{
                    itemobj.likedbool=NO;
                    [myappDelegate.allLikesGlobalarray removeObject:[NSString stringWithFormat:@"%@",[mydict objectForKey:@"userListingID"] ]];
                }
                
                BOOL breakbool=NO;
                for (int i=0; i<itemscrollview.subviews.count; i++)
                {
                    UIView *myv=[itemscrollview.subviews objectAtIndex:i];
                    
                    if (myv.tag==likeindex)
                    {
                        if ([myv isKindOfClass:[UIView class]])
                        {
                            for (UIView *temv in myv.subviews)
                            {
                                if ([temv isKindOfClass:[UIButton class]])
                                {
                                    UIButton *likebtn=(UIButton *)temv;
                                    if (likebtn.frame.size.width==23)
                                    {
                                        if (itemobj.likedbool==YES)
                                        {
                                            
                                            
                                            [likebtn setImage:[UIImage imageNamed:@"like_on.png"] forState:UIControlStateNormal];
                                        }
                                        else{
                                            [likebtn setImage:[UIImage imageNamed:@"like_off.png"] forState:UIControlStateNormal];
                                            
                                            
                                        }
                                        
                                        breakbool=YES;
                                        break;
                                    }
                                }
                            }
                            
                            if (breakbool==YES)
                            {
                                break;
                            }
                        }
                        
                        
                    }
                }
                
            }
            
            
        }
        
        
        [myappDelegate startspinner:self.view];
        [registrationRep getdata:nil :mywishlist :@"GET" withcount:@"0"];
        
    }
    else if ([paramname isEqualToString:mywishlist])
    {
        [myappDelegate.mylikesGlobalarray removeAllObjects];
        [myappDelegate.allLikesGlobalarray removeAllObjects ];
        
        NSLog(@" check teh dictionay  %@ ",respdict);
        prevlikedarray =[[NSMutableArray alloc]init];
        
        
        if ([respdict isKindOfClass:[NSArray class]])
        {
            NSArray *resparray=(NSArray *)respdict;
            
            for (int r=0; r<resparray.count; r++)
            {
                NSDictionary *subdict=[resparray objectAtIndex:r];
                
                NSArray  *listingarray= [subdict objectForKey:@"listUserListing"];
                
                for (int j=0; j<listingarray.count; j++)
                {
                    NSDictionary *maindict=[listingarray objectAtIndex:j];
                    
                    ItemsModelobject *itemsmodobj=[[ItemsModelobject alloc]init];
                    itemsmodobj.itemNamestr=[maindict objectForKey:@"itemName"];
                    
                    itemsmodobj.itemidstr=[maindict objectForKey:@"unitPrice"];
                    itemsmodobj.listingcoststr=[maindict objectForKey:@"unitPrice"];
                    itemsmodobj.salecostcoststr=[maindict objectForKey:@"listPrice"];
                    if ([maindict objectForKey:@"status"])
                    {
                        itemsmodobj.itemStatus=[maindict objectForKey:@"status"];
                        
                    }
                    itemsmodobj.brandstr=[maindict objectForKey:@"brand"];
                    itemsmodobj.conditionstr=[maindict objectForKey:@"condition"];
                    itemsmodobj.catogrystr=[maindict objectForKey:@"categoryName"];
                    itemsmodobj.sizestr=[maindict objectForKey:@"size"];
                    itemsmodobj.descriptionstr=[maindict objectForKey:@"description"];
                    itemsmodobj.forsalebool = [[maindict valueForKey:@"forsale"] boolValue];
                    
                    itemsmodobj.usernamestring=[maindict objectForKey:@"userName"];
                    itemsmodobj.namestr=[maindict objectForKey:@"name"];
                    
                    
                    itemsmodobj.userlistingidstr=[NSString stringWithFormat:@"%@",[maindict valueForKey:@"userListingId"]];
                    
                    [myappDelegate.allLikesGlobalarray addObject:[NSString stringWithFormat:@"%@",[maindict valueForKey:@"userListingId"]] ];
                    
                    itemsmodobj.itemidstr=[maindict objectForKey:@"userItemId"];
                    itemsmodobj.useridstr=[maindict objectForKey:@"userID"];
                    
                    itemsmodobj.weightstring=[NSString stringWithFormat:@"%@",[maindict objectForKey:@"weight"]];
                    
                    itemsmodobj.genderstring=[maindict objectForKey:@"gender"];
                    
                    itemsmodobj.uspsbool=[[maindict valueForKey:@"usps"] boolValue];
                    
                    itemsmodobj.inpersonbool=[[maindict valueForKey:@"inPerson"] boolValue];
                    
                    
                    itemsmodobj.profilepicstr=[maindict objectForKey:@"profilepic"];
                    itemsmodobj.citystring=[maindict objectForKey:@"city"];
                    itemsmodobj.statestr=[maindict objectForKey:@"state"];
                    
                    if ([myappDelegate isnullorempty:itemsmodobj.citystring])
                    {
                        itemsmodobj.citystring=@"";
                    }
                    
                    if ([myappDelegate isnullorempty:itemsmodobj.statestr])
                    {
                        itemsmodobj.statestr=@"";
                    }
                    
                    
                    
                    
                    NSArray *itempickarray=[maindict objectForKey:@"itemPictures"];
                    itemsmodobj.picturearray=[NSMutableArray new];
                    
                    for (int j=0; j<itempickarray.count; j++)
                    {
                        NSDictionary *picdict=[itempickarray objectAtIndex:j];
                        
                        
                        NSString *urlsrt=[picdict valueForKey:@"pictureCdnUrl"];
                        urlsrt=[urlsrt stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
                        // %20 NSURL *picutl=[NSURL URLWithString:[picdict valueForKey:@"pictureCdnUrl"]];
                        
                        [itemsmodobj.picturearray addObject:urlsrt];
                    }
                    
                    itemsmodobj.likedbool=YES;
                    //                    for (int lk=0; lk<prevlikedarray.count; lk++)
                    //                    {
                    //                        NSString *prevlikedstr=[prevlikedarray objectAtIndex:lk];
                    //
                    //                        if ([prevlikedstr isEqualToString:itemsmodobj.userlistingidstr ])
                    //                        {
                    //                            itemsmodobj.likedbool=YES;
                    //                            break;
                    //                        }
                    //
                    //                    }
                    
                    [myappDelegate.mylikesGlobalarray addObject:itemsmodobj];
                    
                }
                
                
                
                
                
                
            }
        }
        
        // [itemsarray addObjectsFromArray:[AppDelegate parselistingitem:respdict :NO :nil :YES]];
        
        
        [myappDelegate stopspinner:self.view ];
        [self createScrollView];
        [refreshControl endRefreshing];
        //[registrationRep getdata:nil :@"getListing" :@"GET"];
    }
    
    /*
     else if ([paramname isEqualToString:@"getListing"])
     {
     [itemsarray removeAllObjects];
     if ([respdict isKindOfClass:[NSArray class]])
     {
     NSArray *resparry=(NSArray *)respdict;
     
     for (int i=0; i<resparry.count; i++)
     {
     
     NSDictionary *maindict=[resparry objectAtIndex:i];
     
     ItemsModelobject *itemsmodobj=[[ItemsModelobject alloc]init];
     itemsmodobj.itemNamestr=[maindict objectForKey:@"itemName"];
     
     itemsmodobj.itemidstr=[maindict objectForKey:@"unitPrice"];
     itemsmodobj.listingcoststr=[maindict objectForKey:@"unitPrice"];
     itemsmodobj.salecostcoststr=[maindict objectForKey:@"listPrice"];
     
     itemsmodobj.brandstr=[maindict objectForKey:@"brand"];
     itemsmodobj.conditionstr=[maindict objectForKey:@"condition"];
     itemsmodobj.catogrystr=[maindict objectForKey:@"categoryName"];
     itemsmodobj.sizestr=[maindict objectForKey:@"size"];
     itemsmodobj.descriptionstr=[maindict objectForKey:@"description"];
     
     
     
     
     itemsmodobj.userlistingidstr=[NSString stringWithFormat:@"%@",[maindict valueForKey:@"userListingId"]];
     
     itemsmodobj.itemidstr=[maindict objectForKey:@"userItemId"];
     
     
     NSArray *itempickarray=[maindict objectForKey:@"itemPictures"];
     itemsmodobj.picturearray=[NSMutableArray new];
     
     for (int j=0; j<itempickarray.count; j++)
     {
     NSDictionary *picdict=[itempickarray objectAtIndex:j];
     
     
     NSString *urlsrt=[picdict valueForKey:@"pictureCdnUrl"];
     urlsrt=[urlsrt stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
     // %20 NSURL *picutl=[NSURL URLWithString:[picdict valueForKey:@"pictureCdnUrl"]];
     
     [itemsmodobj.picturearray addObject:urlsrt];
     }
     
     for (int lk=0; lk<prevlikedarray.count; lk++)
     {
     NSString *prevlikedstr=[prevlikedarray objectAtIndex:lk];
     
     if ([prevlikedstr isEqualToString:itemsmodobj.userlistingidstr ])
     {
     itemsmodobj.likedbool=YES;
     break;
     }
     
     }
     
     
     [itemsarray addObject:itemsmodobj];
     /*itemsmodobj.itemNamestr=[maindict objectForKey:@""];
     itemsmodobj.itemNamestr=[maindict objectForKey:@""];
     itemsmodobj.itemNamestr=[maindict objectForKey:@""];
     itemsmodobj.itemNamestr=[maindict objectForKey:@""];
     itemsmodobj.itemNamestr=[maindict objectForKey:@""];
     itemsmodobj.itemNamestr=[maindict objectForKey:@""];
     itemsmodobj.itemNamestr=[maindict objectForKey:@""];
     itemsmodobj.itemNamestr=[maindict objectForKey:@""];
     * /
     
     }
     }
     
     [self createscrollview];
     [appdelegate stopspinner:self.view];
     } */
    
    
    
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

-(void)createScrollView
{
    
    
    for (UIView *selfv in itemscrollview.subviews)
    {
        if ([selfv isKindOfClass:[ODRefreshControl class]])
        {
            
        }
        else{
            [selfv removeFromSuperview];
        }
    }
    
    int x=15;
    int y=5;
    
    itemsarray=myappDelegate.mylikesGlobalarray;
    NSLog(@" items array count %d",itemsarray.count);
    
    [myappDelegate createmyscrollview:itemscrollview :itemsarray :x :y :self];
    
    itemscrollview.hidden=NO;
    
    // if (y+220<itemscrollview.frame.size.height)
    {
        //    y=itemscrollview.frame.size.height-219;
    }
    
    // [itemscrollview setContentSize:CGSizeMake(320, y+220)];
    [itemscrollview setContentOffset:CGPointMake(0, 0)];
     [myappDelegate animatemyview:itemscrollview ];
    ///[self animateview];
    
}
#pragma mark scrollView Delegate Methods
-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    
    //   CGFloat pageWidth = scrollView.frame.size.width;
    //  int page = floor((scrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
    
    
    // [AppDelegate assaignitemimages:CGRectMake(0, scrollView.contentOffset.y,scrollView.frame.size.width , scrollView.frame.size.height) :itemsarray :itemscrollview];
    
}

-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    [AppDelegate assaignitemimages:CGRectMake(0, scrollView.contentOffset.y,scrollView.frame.size.width , scrollView.frame.size.height) :itemsarray :itemscrollview];
    
}
//this button performs the likebutton
-(IBAction)likebuttonaction:(id)sender
{
    [self setMenuDown];
    // [appdelegate startspinner:self.view];
    likeindex=[sender tag];
    ItemsModelobject *itemobj=[itemsarray objectAtIndex:[sender tag]];
    NSString *userid=[APPDELEGATE getStringFromPlist:@"userid"];

    
    NSDictionary *likedict=[[NSDictionary alloc]initWithObjectsAndKeys:userid,@"UserID",itemobj.userlistingidstr,@"UserListingID", nil];
    
    NSLog(@"chekc the like dict tag of sender %d-=-=-=%@",likeindex, likedict);
    registrationRep.delegate=self;
    [registrationRep getdata:likedict :@"LikeItem" :@"POST" withcount:@"0"];
    
    [myappDelegate.mylikesGlobalarray removeAllObjects];
    // [appdelegate startspinner:self.view];
    // [registrationRep getdata:nil :mywishlist :@"GET"];
    
    [self performSelector:@selector(viewWillAppear:) withObject:[NSNumber numberWithBool:YES] afterDelay:0.2];
    
    
}
//here button is performs pop the sugession viewcontroller
-(IBAction)homeBtnAction:(id)sender
{
    NSArray *arr=self.navigationController.viewControllers;
    for (UIViewController *childvc in arr) {
        
        if([childvc isKindOfClass:[SuggestionsViewController class]])
        {
            SuggestionsViewController *subvc=(SuggestionsViewController *)childvc;
            subvc.ismyfeed=NO;
            [self.navigationController popToViewController:subvc animated:YES];
        }
    }
    
}



-(IBAction)notificationclicked:(id)sender
{
    NotificationViewController *notificationVC;
    
    if(myappDelegate.isiphone5==NO)
    {
        notificationVC=[[NotificationViewController alloc]initWithNibName:@"NotificationViewController_iphone4" bundle:nil];
    }
    else if([[UIScreen mainScreen] bounds].size.height == 568)
    {
        notificationVC=[[NotificationViewController alloc]initWithNibName:@"NotificationViewController" bundle:nil];
        
    }
    NSString *countstr=@"0";
    [APPDELEGATE insertStringInPlist:@"notificationcount" value:countstr];
    [self.navigationController pushViewController:notificationVC animated:YES];
}

-(void)internetConnection
{
    UIAlertView *Internetalert=[[UIAlertView alloc]initWithTitle:appname message:@"Internet connection not available" delegate:self cancelButtonTitle:nil otherButtonTitles:@"Ok", nil];
    [Internetalert show];
    [APPDELEGATE stopspinner:self.view];
    
}
@end
