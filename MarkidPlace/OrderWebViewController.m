//
//  OrderWebViewController.m
//  MarkidPlace
//
//  Created by Stellent_Krish on 2/9/15.
//  Copyright (c) 2015 stellentmac1. All rights reserved.
//

#import "OrderWebViewController.h"

@interface OrderWebViewController ()

@end

@implementation OrderWebViewController
@synthesize navbar,titlelbl,htmlMainString,orderID,webview,doneBtn;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.titlelbl.text=@"ORDER";
    
    self.navbar.frame = CGRectMake(0, 0, 320, 64);
    [self.navbar setBackgroundImage:[UIImage imageNamed:@"navbar.png"] forBarMetrics:UIBarMetricsDefault];
    self.doneBtn.titleLabel.text=@"Done";
    self.titlelbl.font = [UIFont fontWithName:@"Monroe" size:18];
    self.doneBtn.titleLabel.font=[UIFont fontWithName:@"Monroe" size:15];
    NSLog(@"htmlMainString-----------%@",htmlMainString);
    NSURL *websiteUrl = [NSURL URLWithString:htmlMainString];
    NSURLRequest *urlRequest = [NSURLRequest requestWithURL:websiteUrl];
    [webview loadRequest:urlRequest];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

-(IBAction)backBtnPressed:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)webViewDidStartLoad:(UIWebView *)webView
{
    [spinner startAnimating];
}
-(void)webViewDidFinishLoad:(UIWebView *)webView
{
    [spinner stopAnimating];
}

-(void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    
    // NSLog(@"Error for WEBVIEW: %@", [error description]);
    [spinner stopAnimating];
}

-(IBAction)doneBtnPressed:(id)sender
{
    regisRepository = [[RegistrationRepository alloc]init];
    regisRepository.delegate = self;
    [regisRepository verificationOfPayPalPayment:orderID];

}
-(void)successResponseforPaypalVerification:(NSDictionary *)responseDict
{
    NSLog(@"responseDict ====%@",responseDict);
    
    /*
     {
     message = failed;
     orderId = 76;
     }

    */
    dispatch_async( dispatch_get_main_queue(), ^{
        [myappDelegate stopspinner:self.view];
        if([[responseDict valueForKey:@"message"] isEqualToString:@"Success"])
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:appname message:@"Your transaction is successfully completed." delegate:self                                                       cancelButtonTitle:@"OK"  otherButtonTitles:nil];
            alert.tag = 11;
            [alert show];
        }
        else if ([[responseDict valueForKey:@"message"] isEqualToString:@"success"])
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:appname message:@"Your transaction is successfully completed." delegate:self                                                       cancelButtonTitle:@"OK"  otherButtonTitles:nil];
            alert.tag = 11;
            [alert show];
        }
        else if ([[responseDict valueForKey:@"message"] isEqualToString:@"failed"])
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:appname message:@"Sorry ! Your transaction is failed." delegate:self                                                       cancelButtonTitle:@"OK"  otherButtonTitles:nil];
            alert.tag = 22;
            [alert show];
        }
    });
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(alertView.tag==22)
    {
    if (buttonIndex == 0)
    {
        [self homeBtnAction:nil];
    }
    }
    else if(alertView.tag==11)
    {
        
         BuyandSellViewController*sell;
         if (myappDelegate.isiphone5)
         {
         sell=[[BuyandSellViewController alloc]initWithNibName:@"BuyandSellViewController" bundle:nil];
         }
         else
         {
         sell=[[BuyandSellViewController alloc]initWithNibName:@"BuyandSellViewController_iphone4" bundle:nil];
         }
         sell.isbuyer=YES;
         sell.buystring=@"Cart";
         [self.navigationController pushViewController:sell animated:YES];
        
    }
}
-(IBAction)homeBtnAction:(id)sender;
{
    NSArray *arr=self.navigationController.viewControllers;
    for (UIViewController *childvc in arr) {
        
        if([childvc isKindOfClass:[SuggestionsViewController class]])
        {
            SuggestionsViewController *subvc=(SuggestionsViewController *)childvc;
            subvc.ismyfeed=NO;
            [self.navigationController popToViewController:subvc animated:YES];
        }
    }
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
