   //
//  CartViewController.m
//  MarkidPlace
//
//  Created by stellentmac1 on 5/21/14.
//  Copyright (c) 2014 stellentmac1. All rights reserved.
//

#import "CartViewController.h"
#import "FollowViewController.h"

// Set the environment:
// - For live charges, use PayPalEnvironmentProduction (default).
// - To use the PayPal sandbox, use PayPalEnvironmentSandbox.
// - For testing, use PayPalEnvironmentNoNetwork.

//#define kPayPalEnvironment PayPalEnvironmentSandbox

#define kPayPalEnvironment PayPalEnvironmentProduction

@interface CartViewController ()

@end

@implementation CartViewController

@synthesize navbar,titlelbl,
cartScrollView,itemsdetaillbl,purchasetotallbl,shippinglbl,purchasecostlbl,markaslbl,paypalbgimg,paypalbtn,allpaymentslbl,detailitemcostlbl,detailitemimglbl,itemnamelbl;
@synthesize itemImageView;
@synthesize itemsModelObj;
@synthesize cartIdString;

///
@synthesize cartlistview,cartcell,itemsArray;

int ycord = 48;
int totalcost = 0;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
#pragma mark View method.

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    
    currenttextfield=[UITextField new];
 //Production paypal
  //[PayPalMobile preconnectWithEnvironment:PayPalEnvironmentProduction];
    
    
//paypal  sandbox
 // [PayPalMobile preconnectWithEnvironment:PayPalEnvironmentSandbox];
}
- (void)viewDidLoad
{
    [super viewDidLoad];
   // appdelegate=(AppDelegate *)[[UIApplication sharedApplication]delegate];
    // Do any additional setup after loading the view from its nib.
    screenSize = [[UIScreen mainScreen]bounds];
  
    ycord = 48;
    totalcost = 0;
   
    

    if ([self respondsToSelector:@selector(edgesForExtendedLayout)])
        self.edgesForExtendedLayout = UIRectEdgeNone;

       regisRepository = [[RegistrationRepository alloc]init];
    regisRepository.delegate = self;
    
    self.navbar.frame = CGRectMake(0, 0, 320, 64);
    [self.navbar setBackgroundImage:[UIImage imageNamed:@"navbar@2x.png"] forBarMetrics:UIBarMetricsDefault];
    self.titlelbl.font = [UIFont fontWithName:@"Monroe" size:18];
    itemsArray=[[NSMutableArray alloc]init];
   
  }

//

#pragma mark Button Action
// Perform the Action of switch
- (IBAction)switchAction:(id)sender
{
    [currenttextfield resignFirstResponder];
    
    ItemsModelobject *itemobj=[myappDelegate.cartGlobalarray objectAtIndex:[sender tag]];
    
    if (itemobj.alteraddressbool==YES)
    {
        itemobj.alteraddressbool=NO;
        itemobj.alternadeaddressdict=nil;
    }
    else
    {
        itemobj.alteraddressbool=YES;
       
    }
    [cartlistview reloadData];
}
/*
- (IBAction)deletebtnAction:(id)sender
{
    totalcost = totalcost-[[itempriceArray objectAtIndex:[sender tag]-1] intValue];
    purchasecostlbl.text = [NSString stringWithFormat:@"$%d",totalcost];
       int temptag = 0;
    
        for (UIView * view in cartScrollView.subviews)
        {
            if ([view isKindOfClass:[UIView class]])
            {
                if (view.tag == [sender tag])
                {
                    temptag = [sender tag];
                    [view removeFromSuperview];
                }
            }
        }
    
    for (UIView * subview in cartScrollView.subviews)
    {
        if ([subview isKindOfClass:[UIView class]])
        {
            if (subview.tag >= temptag || subview.frame.size.height == 210)
            {
                CGRect rect = subview.frame;
                rect.origin.y = rect.origin.y-65;
                subview.frame = rect;
                temptag = temptag+1;
            }
        }
    }
    
    ycord = ycord-65;
    
    [self.cartScrollView setContentSize:CGSizeMake(320, ycord+210)];
    
    [cartScrollView setNeedsDisplay];
    
}
 */

// paypall button action
/*
- (IBAction)paypalbtnAction:(id)sender
{
    FollowViewController *followVC;
    if (appdelegate.isiphone5)
    {
        followVC = [[FollowViewController alloc]initWithNibName:@"FollowViewController" bundle:nil];
    }
    else
    {
        followVC = [[FollowViewController alloc]initWithNibName:@"FollowViewController_iphone4" bundle:nil];
    }
    
    [self.navigationController pushViewController:followVC animated:YES];

}
 
 */
// navigationbar backbuttonAction
- (IBAction)backbtnAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark---Paypal Methods Starts here

-(void)sendBuyerDetails
{
    [myappDelegate startspinner:self.view];
    
    NSString *userid=[myappDelegate getStringFromPlist:@"userid"];
    NSMutableDictionary *orderDict = [[NSMutableDictionary alloc]init];
    [orderDict setValue:[NSString stringWithFormat:@"%@",userid]    forKey:@"BuyerUserID"];
    
    [orderDict setValue:@"1"forKey:@"Quantity"];
    
    
    //[orderDict setValue:@"Processing" forKey:@"OrderStatus"];
    //[orderDict setValue:self.paypalStatusString forKey:@"PaymentStatus"];
    //[orderDict setValue: self.paypalTransIDString forKey:@"PaymentProviderTransactionID"];
    //[orderDict setValue:self.paypalStatusString forKey:@"PaymentProviderAuthStatus"];
    //[orderDict setValue:@"notes" forKey:@"PaymentProviderNotes"];
    [orderDict setValue:[NSString stringWithFormat:@"%0.1f",costval] forKey:@"Price"];
      [orderDict setValue:[NSString stringWithFormat:@"%0.1f",shipcostval] forKey:@"ShippingPrice"];
     ItemsModelobject *sltitem=[myappDelegate.cartGlobalarray objectAtIndex:selectedindex];
    self.cartIdString=sltitem.userlistingidstr;
    [orderDict setValue:[NSString stringWithFormat:@"%@",self.cartIdString] forKey:@"UserListingID"];
    
    if (sltitem.alteraddressbool)
    {
        NSLog(@" send alternate address %@",sltitem.alternadeaddressdict);
        [orderDict setValue:[sltitem.alternadeaddressdict valueForKey:@"Address 1" ] forKey:@"Address1"];
        [orderDict setValue:[sltitem.alternadeaddressdict valueForKey:@"Address 2" ] forKey:@"Address2"];
        [orderDict setValue:[sltitem.alternadeaddressdict valueForKey:@"City" ] forKey:@"City"];
        [orderDict setValue:[sltitem.alternadeaddressdict valueForKey:@"State" ] forKey:@"State"];
        [orderDict setValue:[sltitem.alternadeaddressdict valueForKey:@"Zipcode" ] forKey:@"Zipcode"];
        
        
        [orderDict setValue:[NSNumber numberWithBool:YES]  forKey:@"IsAleternate"];
    }
    else{
        [orderDict setValue:@"" forKey:@"Address1"];
        [orderDict setValue:@"" forKey:@"Address2"];
        [orderDict setValue:@"" forKey:@"City"];
        [orderDict setValue:@"" forKey:@"State"];
        [orderDict setValue:@"" forKey:@"Zipcode"];
         [orderDict setValue:[NSNumber numberWithBool:NO]  forKey:@"IsAleternate"];
    }
    
    NSLog(@"the order dict is %@",orderDict);
    
    //Now i am calling test shell service here 
    [regisRepository postPaymentDetails:orderDict];
    /*
    BuyandSellViewController*sell;
        if (myappDelegate.isiphone5)
        {
            sell=[[BuyandSellViewController alloc]initWithNibName:@"BuyandSellViewController" bundle:nil];
        }
        else
        {
            sell=[[BuyandSellViewController alloc]initWithNibName:@"BuyandSellViewController_iphone4" bundle:nil];
        }
        sell.isbuyer=YES;
    sell.buystring=@"Cart";
        [self.navigationController pushViewController:sell animated:YES];
     */
        
}

#pragma mark---Paypal Methods Ended

-(void)successResponseforPaypal:(NSDictionary *)responseDict
{
      NSLog(@"responseDict ====%@",responseDict);
    /*
    {
        buyerUserID = 0;
        isAleternate = 0;
        message = "Order Save Successfully";
        orderID = 33;
        orderNumber = 0;
        price = 0;
        quantity = 0;
        shippingPrice = 0;
        userID = 0;
        userListingID = 0;
    }
     */
    
   // [self performSelectorOnMainThread:@selector(showmessage:) withObject:responseDict waitUntilDone:YES];
    dispatch_async( dispatch_get_main_queue(), ^{
    [myappDelegate stopspinner:self.view];
    //[myappDelegate stopspinner:self.view];
    if([[responseDict valueForKey:@"message"] isEqualToString:@"Order Save Successfully"])
    {
        
        NSString *orderid=[NSString stringWithFormat:@"%d",[[responseDict valueForKey:@"orderId"] intValue]];
        if([APPDELEGATE isnullorempty:orderid])
        {
            
        }
        else
        {
    OrderWebViewController *orderWebVC=[[OrderWebViewController alloc]initWithNibName:@"OrderWebViewController" bundle:nil];
            orderWebVC.htmlMainString=[NSString stringWithFormat:@"http://mkppaypall.testshell.net/Default.aspx?OrderId=%@",orderid];
            orderWebVC.orderID=orderid;
            
             [self.navigationController pushViewController:orderWebVC animated:YES];
        }
    }
        
    else if([[responseDict valueForKey:@"message"] isEqualToString:@"Saved not Successfully"])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:appname message:@"Sorry ! Unable to process your request, try again." delegate:nil                                                       cancelButtonTitle:@"OK"  otherButtonTitles:nil];
        alert.tag = 22;
        [alert show];
    }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:appname message:@"Sorry ! Unable to process your request, try again." delegate:nil                                                       cancelButtonTitle:@"OK"  otherButtonTitles:nil];
            alert.tag = 22;
            [alert show];
        }
    });
}


-(void)showmessage :(NSDictionary *)respdict
{
    NSLog(@"the respdict %@",respdict);
    NSString *successString=[respdict objectForKey:@"message"];
    
    if ([successString isEqualToString:@"Order Save Successfully"])
    {
        for (int i=0; i<myappDelegate.cartGlobalarray.count; i++)
        {
            ItemsModelobject *modobj=[myappDelegate.cartGlobalarray objectAtIndex:i];
            
            if ([[NSString stringWithFormat:@"%@",modobj.userlistingidstr] isEqualToString:[NSString stringWithFormat:@"%@",self.cartIdString]])
            {
                [myappDelegate.cartGlobalarray removeObjectAtIndex:i];
                break;
            }
        }
        
        [cartlistview reloadData];
        UIAlertView *alertView=[[UIAlertView alloc]initWithTitle:appname message:@"Thank you for placing order. We have notified Seller about your order." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        alertView.tag=1234;
        [alertView show];
        
        [myappDelegate.myfeedsGlobalarray removeAllObjects];
        [myappDelegate.suggestonsGlobalarray removeAllObjects];
    }
    else
    {
        UIAlertView *alertView=[[UIAlertView alloc]initWithTitle:appname message:@"Unfortunately your payment was not completed successfully. We cancelled your order." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        alertView.tag=123;
        [alertView show];
    }
    
    [myappDelegate stopspinner:self.view];
  }


-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag==123)
    {
        BOOL isVCExist=NO;
        NSArray *arr=self.navigationController.viewControllers;
        for (UIViewController *childvc in arr) {
            
            if([childvc isKindOfClass:[SuggestionsViewController class]])
            {
                isVCExist=YES;
                SuggestionsViewController *subvc=(SuggestionsViewController *)childvc;
                [self.navigationController popToViewController:subvc animated:YES];
                return;
            }
        }
        
        if(!isVCExist)
        {
            SuggestionsViewController *sugge;
            if ([AppDelegate isiphone5])
            {
                sugge = [[SuggestionsViewController alloc]initWithNibName:@"SuggestionsViewController" bundle:nil];
            }
            else
            {
                sugge = [[SuggestionsViewController alloc]initWithNibName:@"SuggestionsViewController_iphone4" bundle:nil];
            }
            [self.navigationController pushViewController:sugge animated:YES];
        }

        //[self.navigationController popToRootViewControllerAnimated:YES];

    }
}

#pragma mark - Receive Single Payment

// performs the Action  for payments


-(IBAction)checkoutbtnAction:(id)sender
{
    [currenttextfield resignFirstResponder];
   
    ItemsModelobject *sltitem=[myappDelegate.cartGlobalarray objectAtIndex:[sender tag]];
    
    selectedindex=[sender tag];
    
    if (sltitem.alteraddressbool==YES)
    {
        NSLog(@"now check the alternate %@  ",sltitem.alternadeaddressdict);
        NSString *checkthis=[sltitem.alternadeaddressdict valueForKey:@"Zipcode"];
                             if (checkthis.length>0)
                             {
                                 int checkint=[checkthis intValue];
                                }
            else if ([APPDELEGATE isnullorempty:checkthis])
                   {
                    [AppDelegate alertmethod:appname :@"Please fill Zipcode"];
                       return;
                    }

        NSString *address2=[sltitem.alternadeaddressdict valueForKey:@"Address2"];
        
        if (address2.length>0)
        {
            //int checkint=address2.length ;
        }
            if (address2.length<1)
            {
                [sltitem.alternadeaddressdict setObject:@"" forKey:@"Address2"];
            }
               if ([myappDelegate isnullorempty:sltitem.alternadeaddressdict] || sltitem.alternadeaddressdict.allKeys.count<5 || sltitem.alternadeaddressdict.allValues.count<5)
        {
            
            [AppDelegate alertmethod:appname :[NSString stringWithFormat:@"Please fill all the fields in alternate address"]];
            return;
        }
        else
        {
            BOOL returnbool=NO;
            for (int i=0;i<5;i++)
            {
             /*   NSLog(@"i is ----%d-----",i);
                if(i==1||i==4)
                {
                    NSString *values=[sltitem.alternadeaddressdict.allValues objectAtIndex:i];
                    if ([myappDelegate isnullorempty:values])
                    {
                        NSLog(@"Empty");
                    }

                }
                else
                {*/
                
                NSString *values=[sltitem.alternadeaddressdict.allValues objectAtIndex:i];
            
                if ([myappDelegate isnullorempty:values])
                {
                   NSString *checkthis=[sltitem.alternadeaddressdict valueForKey:@"Address2"];
                    if([APPDELEGATE isnullorempty:checkthis])
                    {
                        NSLog(@"now check the alternate %@  ",checkthis);
                    }
                else
                    {
                    NSLog(@"i value is %d",i);
                    returnbool=YES;
                    [AppDelegate alertmethod:appname :[NSString stringWithFormat:@"Please fill %@",[sltitem.alternadeaddressdict.allKeys objectAtIndex:i]]];
                    return;
                    }
                }
            }
              if (returnbool)
            {
                //return;
            }
        }
    }
    if (sltitem.alteraddressbool==YES)
    {
        NSLog(@"sltitem.userlistingidstr %@",sltitem.userlistingidstr);
        NSLog(@"sltitem.itemidstr %@",sltitem.itemidstr);
        NSLog(@"sltitem.itemNamestr %@",sltitem.itemNamestr);

        [self verificationofAddress];
        
    }
    else
    {
        NSLog(@"sltitem.userlistingidstr %@",sltitem.userlistingidstr);
        NSLog(@"sltitem.itemidstr %@",sltitem.itemidstr);
        NSLog(@"sltitem.itemNamestr %@",sltitem.itemNamestr);
        
        //[self paypaldisplay];
        [self sendBuyerDetails];
    }
    
    
}
/*
-(void)openPayPalOnValideAddress:( ItemsModelobject *)sltitem
{
    //Open comment by srinadh
    
    self.resultText = nil;
    NSLog(@"@@@@@Cart Id is:%@",sltitem.userlistingidstr);
    cartIdString=sltitem.userlistingidstr;
    
    // Note: For purposes of illustration, this example shows a payment that includes
    //       both payment details (subtotal, shipping, tax) and multiple items.
    //       You would only specify these if appropriate to your situation.
    //       Otherwise, you can leave payment.items and/or payment.paymentDetails nil,
    //       and simply set payment.amount to your total charge.
    
    // Optional: include multiple items
    
    NSLog(@"@@@@@@Total Price of payment is:%@",self.detailitemcostlbl.text);
    
    // NSString *priceString=[[NSString stringWithFormat:@"%f",costval] stringByReplacingOccurrencesOfString:@"$" withString:@""];
    priceString=[[NSString stringWithFormat:@"%@",[NSString stringWithFormat:@"$%@",[NSString stringWithFormat:@"%0.1f",costval]] ] stringByReplacingOccurrencesOfString:@"$" withString:@""];
    NSLog(@"@@@@@@Trimmed Price of payment is:%@",priceString);
    NSLog(@"%@",sltitem.itemnumstr);
    NSLog(@"%@",sltitem.itemidstr);
    
    NSLog(@"%@",sltitem.itemNamestr);
    
    NSString *sku= [NSString stringWithFormat:@"%@",sltitem.itemidstr ];
    
    PayPalItem *item1 = [PayPalItem itemWithName:sltitem.itemNamestr
                                    withQuantity:1
                                       withPrice:[NSDecimalNumber decimalNumberWithString:priceString]
                                    withCurrency:@"USD"
                                         withSku:sku];//itemnum
    
    
    
    NSArray *items = @[item1];
    NSDecimalNumber *subtotal = [PayPalItem totalPriceForItems:items];
    
    // Optional: include payment details
    
    NSDecimalNumber *shipping = [[NSDecimalNumber alloc] initWithString:@"00.00"];
    NSDecimalNumber *tax = [[NSDecimalNumber alloc] initWithString:@"00.00"];
    PayPalPaymentDetails *paymentDetails = [PayPalPaymentDetails paymentDetailsWithSubtotal:subtotal
                                                                               withShipping:shipping
                                                                                    withTax:tax];
    
    NSDecimalNumber *total = [[subtotal decimalNumberByAdding:shipping] decimalNumberByAdding:tax];
    
    PayPalPayment *payment = [[PayPalPayment alloc] init];
    payment.amount = total;
    payment.currencyCode = @"USD";
    payment.shortDescription = sltitem.itemNamestr; //Item Name
    payment.items = items;  // if not including multiple items, then leave payment.items as nil
    payment.paymentDetails = paymentDetails; // if not including payment details, then leave payment.paymentDetails as nil
    
    if (!payment.processable)
    {
        // This particular payment will always be processable. If, for
        // example, the amount was negative or the shortDescription was
        // empty, this payment wouldn't be processable, and you'd want
        // to handle that here.
    }
    
    self.payPalConfig.acceptCreditCards = YES;
    
    PayPalPaymentViewController *paymentViewController = [[PayPalPaymentViewController alloc] initWithPayment:payment
                                                                                                configuration:self.payPalConfig
                                                                                                     delegate:self];
    [self presentViewController:paymentViewController animated:YES completion:nil];
    
    //Open comment by srinadh
}
 */
/*
-(void)paypaldisplay
{
    dispatch_async( dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        dispatch_async( dispatch_get_main_queue(), ^{
            

    [currenttextfield resignFirstResponder];
    
    ItemsModelobject *sltitem=[myappDelegate.cartGlobalarray objectAtIndex:selectedindex];
    
    
    if (sltitem.alteraddressbool==YES)
    {
        NSLog(@"now check the alternate %@ ",sltitem.alternadeaddressdict);
        NSString *checkthis=[sltitem.alternadeaddressdict valueForKey:@"Zipcode"];
        
        
        if (checkthis.length>0)
        {
            int checkint=[checkthis intValue];
            
            
            if (checkint<1)
            {
                [sltitem.alternadeaddressdict setObject:@"" forKey:@"Zipcode"];
            }
        }
        NSString *address2=[sltitem.alternadeaddressdict valueForKey:@"Address2"];
        
        if (address2.length>0)
        {
            //int checkint=address2.length ;
        }
        
        if (address2.length<1)
        {
            [sltitem.alternadeaddressdict setObject:@"" forKey:@"Address2"];
        }
        
        
        
        
        
        if ([myappDelegate isnullorempty:sltitem.alternadeaddressdict] || sltitem.alternadeaddressdict.allKeys.count<5 || sltitem.alternadeaddressdict.allValues.count<5)
        {
            
            [AppDelegate alertmethod:appname :[NSString stringWithFormat:@"Please fill all the fields in alternate address"]];
            return;
        }
        else
        {
            BOOL returnbool=NO;
            for (int i=0;i<5;i++)
            {
                NSLog(@"i is ----%d-----",i);
                               NSString *values=[sltitem.alternadeaddressdict.allValues objectAtIndex:i];
                    if ([myappDelegate isnullorempty:values])
                    {
                        NSString *checkthis=[sltitem.alternadeaddressdict valueForKey:@"Address2"];
                        if([APPDELEGATE isnullorempty:checkthis])
                        {
                            NSLog(@"now check the alternate %@  ",checkthis);
                            
                        }
                        else
                        {
                        returnbool=YES;
                        
                        [AppDelegate alertmethod:appname :[NSString stringWithFormat:@"Please fill %@",[sltitem.alternadeaddressdict.allKeys objectAtIndex:i]]];
                        return;
                        }
                   // }
                }
                
                
                
            }
            
            if (returnbool)
            {
                //return;
            }
        }
        
        
    }
    self.resultText = nil;
    NSLog(@"@@@@@Cart Id is:%@",sltitem.userlistingidstr);
    cartIdString=sltitem.userlistingidstr;
    
    // Note: For purposes of illustration, this example shows a payment that includes
    //       both payment details (subtotal, shipping, tax) and multiple items.
    //       You would only specify these if appropriate to your situation.
    //       Otherwise, you can leave payment.items and/or payment.paymentDetails nil,
    //       and simply set payment.amount to your total charge.
    
    // Optional: include multiple items
    
    NSLog(@"@@@@@@Total Price of payment is:%@",self.detailitemcostlbl.text);
    
    // NSString *priceString=[[NSString stringWithFormat:@"%f",costval] stringByReplacingOccurrencesOfString:@"$" withString:@""];
    priceString=[[NSString stringWithFormat:@"%@",[NSString stringWithFormat:@"$%@",[NSString stringWithFormat:@"%0.1f",costval]] ] stringByReplacingOccurrencesOfString:@"$" withString:@""];
    
    
    
    
    NSLog(@"@@@@@@Trimmed Price of payment is:%@",priceString);
 
            NSLog(@"%@",sltitem.itemnumstr);
            NSLog(@"%@",sltitem.itemidstr);

            NSLog(@"%@",sltitem.itemNamestr);

            NSString *sku=[NSString stringWithFormat:@"%@",sltitem.itemidstr ];
    PayPalItem *item1 = [PayPalItem itemWithName:sltitem.itemNamestr
                                    withQuantity:1
                                       withPrice:[NSDecimalNumber decimalNumberWithString:priceString]
                                    withCurrency:@"USD"
                                         withSku:sku];
    NSArray *items = @[item1];
    NSDecimalNumber *subtotal = [PayPalItem totalPriceForItems:items];
    
    // Optional: include payment details
    
    NSDecimalNumber *shipping = [[NSDecimalNumber alloc] initWithString:@"00.00"];
    NSDecimalNumber *tax = [[NSDecimalNumber alloc] initWithString:@"00.00"];
    PayPalPaymentDetails *paymentDetails = [PayPalPaymentDetails paymentDetailsWithSubtotal:subtotal
                                                                               withShipping:shipping
                                                                                    withTax:tax];
    
    NSDecimalNumber *total = [[subtotal decimalNumberByAdding:shipping] decimalNumberByAdding:tax];
    
    PayPalPayment *payment = [[PayPalPayment alloc] init];
    payment.amount = total;
    payment.currencyCode = @"USD";
    payment.shortDescription = sltitem.itemNamestr; //Item Name
    payment.items = items;  // if not including multiple items, then leave payment.items as nil
    payment.paymentDetails = paymentDetails; // if not including payment details, then leave payment.paymentDetails as nil
    
    if (!payment.processable)
    {
        // This particular payment will always be processable. If, for
        // example, the amount was negative or the shortDescription was
        // empty, this payment wouldn't be processable, and you'd want
        // to handle that here.
    }
    
    self.payPalConfig.acceptCreditCards = YES;
    
    PayPalPaymentViewController *paymentViewController = [[PayPalPaymentViewController alloc] initWithPayment:payment
                                                                                                configuration:self.payPalConfig
                                                                                                     delegate:self];
    [self presentViewController:paymentViewController animated:YES completion:nil];
        });
         });
}
*/
#pragma mark PayPalPaymentDelegate methods
/*
- (void)payPalPaymentViewController:(PayPalPaymentViewController *)paymentViewController didCompletePayment:(PayPalPayment *)completedPayment {
    NSLog(@"PayPal Payment Success!");
    self.resultText = [completedPayment description];
    [self showSuccess];
    
    [self sendCompletedPaymentToServer:completedPayment]; // Payment was processed successfully; send to server for verification and fulfillment
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)payPalPaymentDidCancel:(PayPalPaymentViewController *)paymentViewController {
    NSLog(@"PayPal Payment Canceled");
    self.resultText = nil;
    self.successView.hidden = YES;
    
    UIAlertView *alertView=[[UIAlertView alloc]initWithTitle:appname message:@"Unfortunately your payment was not completed successfully. We cancelled your order." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    alertView.tag=123;
    [alertView show];
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark Proof of payment validation

- (void)sendCompletedPaymentToServer:(PayPalPayment *)completedPayment {
    // TODO: Send completedPayment.confirmation to server
    NSLog(@"Here is your proof of payment:\n\n%@\n\nSend this to your server for confirmation and fulfillment.", completedPayment.confirmation);
    
    NSDictionary *mainDict=[completedPayment.confirmation objectForKey:@"response"];
    
    self.paypalTransIDString=[mainDict objectForKey:@"id"];
    
    NSString *statusString=[mainDict objectForKey:@"state"];
    
    if ([statusString isEqualToString:@"approved"])
    {
        NSLog(@"@@@@@@@@Success@@@@@@");
        
        self.paypalStatusString=@"success";
    }
    else
    {
        self.paypalStatusString=@"failed";
    }
    
    NSLog(@"@@@@@@@@Transaction String is:%@",self.paypalTransIDString);
    
    [self sendBuyerDetails];
}

*/
#pragma mark - Authorize Future Payments
/*
- (IBAction)getUserAuthorization:(id)sender {
    
    PayPalFuturePaymentViewController *futurePaymentViewController = [[PayPalFuturePaymentViewController alloc] initWithConfiguration:self.payPalConfig delegate:self];
    [self presentViewController:futurePaymentViewController animated:YES completion:nil];
}
*/

#pragma mark PayPalFuturePaymentDelegate methods
/*
- (void)payPalFuturePaymentViewController:(PayPalFuturePaymentViewController *)futurePaymentViewController didAuthorizeFuturePayment:(NSDictionary *)futurePaymentAuthorization {
    NSLog(@"PayPal Future Payment Authorization Success!");
    self.resultText = futurePaymentAuthorization[@"code"];
    [self showSuccess];
    
    [self sendAuthorizationToServer:futurePaymentAuthorization];
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)payPalFuturePaymentDidCancel:(PayPalFuturePaymentViewController *)futurePaymentViewController {
    NSLog(@"PayPal Future Payment Authorization Canceled");
    self.successView.hidden = YES;
    [self dismissViewControllerAnimated:YES completion:nil];
}
 */

- (void)sendAuthorizationToServer:(NSDictionary *)authorization {
    // TODO: Send authorization to server
    NSLog(@"Here is your authorization:\n\n%@\n\nSend this to your server to complete future payment setup.", authorization);
}


#pragma mark - Helpers

- (void)showSuccess {
    self.successView.hidden = NO;
    self.successView.alpha = 1.0f;
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.5];
    [UIView setAnimationDelay:2.0];
    self.successView.alpha = 0.0f;
    [UIView commitAnimations];
}

#pragma mark - Flipside View Controller

- (void)flipsideViewControllerDidFinish:(ZZFlipsideViewController *)controller {
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        [self dismissViewControllerAnimated:YES completion:nil];
    } else {
        [self.flipsidePopoverController dismissPopoverAnimated:YES];
        self.flipsidePopoverController = nil;
    }
}

- (void)popoverControllerDidDismissPopover:(UIPopoverController *)popoverController {
    self.flipsidePopoverController = nil;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"pushSettings"]) {
        [[segue destinationViewController] setDelegate:self];
        
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
            UIPopoverController *popoverController = [(UIStoryboardPopoverSegue *)segue popoverController];
            self.flipsidePopoverController = popoverController;
            popoverController.delegate = self;
        }
    }
}

- (IBAction)togglePopover:(id)sender
{
    if (self.flipsidePopoverController) {
        [self.flipsidePopoverController dismissPopoverAnimated:YES];
        self.flipsidePopoverController = nil;
    } else {
        [self performSegueWithIdentifier:@"showAlternate" sender:sender];
    }
}

#pragma mark --End of paypal Methods

//////
#pragma mark TableviewdelegateMethods

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return myappDelegate.cartGlobalarray.count;
   
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    ItemsModelobject *itemobj=[myappDelegate.cartGlobalarray objectAtIndex:indexPath.row];
   
    
    if (itemobj.alteraddressbool==YES)
    {
        return 450;//320
    }
    else
    {
       return 255;//252
    }
    
}
-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier=@"acell";
    
        CartViewCell *tbcell =(CartViewCell*)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        if (tbcell==nil)
        {
            [[NSBundle mainBundle] loadNibNamed:@"CartViewCell" owner:self options:nil];
            tbcell=cartcell;
            tbcell.selectionStyle=UITableViewCellSelectionStyleNone;
        }
    // setting the fonts the label
    tbcell.orderfromlbl.font = [UIFont fontWithName:@"Muli" size:15];
    tbcell.usernamelbl.font = [UIFont fontWithName:@"Muli" size:15];
    tbcell.itemnamelbl.font = [UIFont fontWithName:@"Muli" size:13];
    tbcell.itemdetailslbl.font = [UIFont fontWithName:@"Muli" size:18];
    tbcell.itemcostlbl.font = [UIFont fontWithName:@"Muli" size:15];
    tbcell.sizelbl.font = [UIFont fontWithName:@"Muli" size:13];
    tbcell.sizennumlbl.font = [UIFont fontWithName:@"Muli" size:13];
    tbcell.purchaselbl.font = [UIFont fontWithName:@"Muli" size:18];
    tbcell.purchasecostlbl.font = [UIFont fontWithName:@"Muli" size:15];
    tbcell.chargeslbl.font = [UIFont fontWithName:@"Muli" size:11];
    tbcell.shipingoptionlbl.font = [UIFont fontWithName:@"Muli" size:11];
     tbcell.shippingcostlbl.font = [UIFont fontWithName:@"Muli" size:15];
     tbcell.shippingcostpricelbl.font = [UIFont fontWithName:@"Muli" size:15];
    tbcell.addresstextView.font = [UIFont fontWithName:@"Muli" size:14];
    tbcell.alternateadresslbl.font = [UIFont fontWithName:@"Muli" size:14];
    tbcell.checkoutlbl.font = [UIFont fontWithName:@"Muli" size:17];
    tbcell.checkoutbtn.tag=indexPath.row;
    tbcell.switchbtn.tag=indexPath.row;
    tbcell.cancelbtn.tag=indexPath.row;
    tbcell.addresstextView.tag=indexPath.row;
   
    ItemsModelobject *itemobj=[myappDelegate.cartGlobalarray objectAtIndex:indexPath.row];
    
    NSString *urlstr=[itemobj.picturearray objectAtIndex:0];
    
    NSURL *imgutl=[NSURL URLWithString:urlstr];
    
       [tbcell.itemimgview setImageWithURL:imgutl placeholderImage:[UIImage imageNamed:@"noimage.png"] options:SDWebImageCacheMemoryOnly];
    
    tbcell.usernamelbl.text=[NSString stringWithFormat:@"@%@",itemobj.usernamestring ];
    tbcell.itemdetailslbl.text=itemobj.itemNamestr;
    tbcell.itemnamelbl.text=itemobj.brandstr;
    NSLog(@"the seller id str is    %@",itemobj.useridstr);
    selleridstr=itemobj.useridstr;
    NSLog(@"the seller id str is     %@",itemobj.useridstr);
   
  
    tbcell.sizennumlbl.text=[NSString stringWithFormat:@"%@",itemobj.sizestr];
    CGFloat weightstr=0;
    weightstr=[itemobj.weightstring floatValue];
    
    NSLog(@"itemobj.inpersonbool ------------- %d",itemobj.inpersonbool);
   //itemobj.inpersonbool.intvalue
    
    //Krish
    if(itemobj.inpersonbool)
    {
        tbcell.shippingcostpricelbl.text =@"$0.00" ;
    }
    else
    {
        tbcell.shippingcostpricelbl.text =[NSString stringWithFormat:@"$%@",itemobj.shippingChargesStr];
    }
    
    if (weightstr<=1)
    {
        //tbcell.shippingcostpricelbl.text =@"$4.00" ;
    }
    else if (weightstr>1)
    {
        //tbcell.shippingcostpricelbl.text=@"$7.50";
    }
 
     NSArray *mainArray=[tbcell.shippingcostpricelbl.text componentsSeparatedByString:@"$"];
    itemcostval=0;
    itemcostval=[itemobj.salecostcoststr floatValue];
    
    NSString*str=[NSString stringWithFormat:@"%@",[mainArray objectAtIndex:1]];
    shipcostval=0;
    shipcostval=[str floatValue];
    NSLog(@" the total cost is %f",shipcostval);
     costval=0;
    costval=shipcostval+itemcostval;
     tbcell.itemcostlbl.text=[NSString stringWithFormat:@"$%0.2f",itemcostval];
     NSLog(@" the total cost is %f",costval);
    tbcell.purchasecostlbl.text=[NSString stringWithFormat:@"$%@",[NSString stringWithFormat:@"%0.2f",costval]];
    

  
   
    tbcell.add1Tf.tag=indexPath.row;
    tbcell.add2Tf.tag=indexPath.row;
    tbcell.cityTf.tag=indexPath.row;
    tbcell.stateTf.tag=indexPath.row;
    tbcell.zipcodeTf.tag=indexPath.row;
  
    
    
    // switch action performed
        if (itemobj.alteraddressbool == YES)
        {
            tbcell.switchImageView.image = [UIImage imageNamed:@"switch_on.png"];
            tbcell.addresstextView.userInteractionEnabled=NO;
            
           NSMutableDictionary *alternatedict=itemobj.alternadeaddressdict;
           
            if ([myappDelegate isnullorempty:alternatedict])
            {
                tbcell.add1Tf.text=@"";
                tbcell.add2Tf.text=@"";
                tbcell.cityTf.text=@"";
                tbcell.stateTf.text=@"";
                tbcell.zipcodeTf.text=@"";
            }
         }
        else
        {
             tbcell.addresstextView.userInteractionEnabled=YES;
        
             tbcell.switchImageView.image = [UIImage imageNamed:@"switch_off.png"];
            
        }
            return tbcell;
}
// This Action performs cancel action to remove the row in cart listview.
-(IBAction)cancelbtnAction:(id)sender
{
    [currenttextfield resignFirstResponder];
    ItemsModelobject *itemobj=[myappDelegate.cartGlobalarray objectAtIndex:[sender tag]];
    itemobj.alteraddressbool=NO;
    [myappDelegate.cartGlobalarray removeObjectAtIndex:[sender tag]];
    NSLog(@"the removed row is %ld",(long)[sender tag]);
   
    [cartlistview reloadData];
    
    
    
    if(myappDelegate.cartGlobalarray.count==0)
    {
        [self.navigationController popViewControllerAnimated:YES];
   }

}
- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

#pragma  mark textfield delegate

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    currenttextfield=textField;
    
    CGRect actualframe = [textField convertRect:[textField bounds] toView:self.view];
    if ((actualframe.origin.y+60) >= (self.view.frame.size.height-216))
    {
        CGRect scrollfrm = [textField convertRect:[textField bounds] toView:self.cartlistview];
        [cartlistview setContentOffset:CGPointMake(0, scrollfrm.origin.y-100) animated:NO];//60
        
    }
   
    
}
-(void)textFieldDidEndEditing:(UITextField *)textField
{
     [cartlistview setContentOffset:CGPointMake(0, 0) animated:NO];
    NSLog(@"check the tag %ld and place holder %@",(long)textField.tag,textField.placeholder);
    ItemsModelobject *sltitem=[myappDelegate.cartGlobalarray objectAtIndex:textField.tag];
    
    if ( [myappDelegate isnullorempty:sltitem.alternadeaddressdict])
    {
        sltitem.alternadeaddressdict=[NSMutableDictionary new];
    }
        [sltitem.alternadeaddressdict setValue:textField.text forKey:textField.placeholder];
    if([textField.placeholder isEqualToString:@"State"])
    {
        textField.text=[textField.text uppercaseString];
    }
      // [cartlistview reloadData];
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    

    if ([textField.placeholder isEqualToString:@"Zipcode"])
    {
        NSCharacterSet *cs = [[NSCharacterSet characterSetWithCharactersInString:Numeric] invertedSet];
        
        NSString *filtered = [[string componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
        
        if ([string isEqualToString:filtered]==YES)
        {
            if([string isEqualToString:@""])
            {
                return YES;
            }
            if (textField.text.length <= 5)
            {
                return YES;
            }
            else
            {
                return NO;
            }
        }
        else
        {
            return NO;
        }
        

    }
    CartViewCell *myaddresscell1=(CartViewCell *)[cartlistview cellForRowAtIndexPath:0];
   // NSLog(@"myaddress1....%@",myaddresscell1.add1Tf.text);
    
   
    if (textField ==myaddresscell1.add1Tf||textField ==myaddresscell1.add2Tf)
   {
       
        NSCharacterSet *cs = [[NSCharacterSet characterSetWithCharactersInString:Alpha1] invertedSet];
       
        NSString *filtered = [[string componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
        return [string isEqualToString:filtered];
    }
   
    else{
        
        NSCharacterSet *cs = [[NSCharacterSet characterSetWithCharactersInString:[NSString stringWithFormat:@"%@%@%@-/",AlphaBet,Numeric,Alpha1]] invertedSet];
        
        NSString *filtered = [[string componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
        return [filtered isEqualToString:string];
    }
    
    return YES;
}
-(void)internetConnection
{
    UIAlertView *Internetalert=[[UIAlertView alloc]initWithTitle:appname message:@"Internet connection not available" delegate:self cancelButtonTitle:nil otherButtonTitles:@"Ok", nil];
    [Internetalert show];
    [APPDELEGATE stopspinner:self.view];
}

//Verification of seller

-(void)verificationofAddress
{
    dispatch_async( dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        dispatch_async( dispatch_get_main_queue(), ^{

    [myappDelegate startspinner:self.view];

    ItemsModelobject *sltitem=[myappDelegate.cartGlobalarray objectAtIndex:selectedindex];
    NSMutableDictionary *orderDict = [[NSMutableDictionary alloc]init];
    NSString *userid=[myappDelegate getStringFromPlist:@"userid"];

    if (sltitem.alteraddressbool)
    {
        NSLog(@" send alternate address %@",sltitem.alternadeaddressdict);
        [orderDict setValue:[sltitem.alternadeaddressdict valueForKey:@"Address 1" ] forKey:@"Address1"];
        [orderDict setValue:[sltitem.alternadeaddressdict valueForKey:@"Address 2" ] forKey:@"Address2"];
        [orderDict setValue:[sltitem.alternadeaddressdict valueForKey:@"City" ] forKey:@"City"];
        [orderDict setValue:[[sltitem.alternadeaddressdict valueForKey:@"State" ] uppercaseString]forKey:@"State"];
        [orderDict setValue:[sltitem.alternadeaddressdict valueForKey:@"Zipcode" ] forKey:@"Zipcode"];
        [orderDict setValue:selleridstr forKey:@"SellerUserId"];
        [orderDict setValue:userid forKey:@"BuyerUserId"];
        
        [orderDict setValue:[NSNumber numberWithBool:YES]  forKey:@"IsAleternate"];
    }
    else{
        [orderDict setValue:@"" forKey:@"Address1"];
        [orderDict setValue:@"" forKey:@"Address2"];
        [orderDict setValue:@"" forKey:@"City"];
        [orderDict setValue:@"" forKey:@"State"];
        [orderDict setValue:@"" forKey:@"Zipcode"];
         [orderDict setValue:selleridstr forKey:@"SellerUserId"];
          [orderDict setValue:userid forKey:@"BuyerUserId"];
        [orderDict setValue:[NSNumber numberWithBool:NO]  forKey:@"IsAleternate"];
    }
            NSLog(@"the other dict is %@",orderDict);
    regisRepository.delegate=self;
    [regisRepository sellerandbuyerVerification:orderDict];
        });
        });
}
//successresponse
-(void)successResponseforDetailVerification:(NSMutableDictionary *)responsedict
{
    dispatch_async( dispatch_get_main_queue(), ^{

     [myappDelegate stopspinner:self.view];
    NSLog(@"responsedict----%@-------",responsedict);
    if([[responsedict valueForKey:@"message"]isEqualToString:@"success"])
    {
        ItemsModelobject *sltitem=[myappDelegate.cartGlobalarray objectAtIndex:selectedindex]; [myappDelegate.cartGlobalarray objectAtIndex:selectedindex];
        
        NSLog(@"sltitem.userlistingidstr %@",sltitem.userlistingidstr);
        NSLog(@"sltitem.itemidstr %@",sltitem.itemidstr);
        NSLog(@"sltitem.itemNamestr %@",sltitem.itemNamestr);
        
       // [self paypaldisplay];
        [self sendBuyerDetails];
    }
    else
        if([[responsedict valueForKey:@"message"]isEqualToString:@"Object reference not set to an instance of an object."])
    {
        ItemsModelobject *sltitem=[myappDelegate.cartGlobalarray objectAtIndex:selectedindex]; [myappDelegate.cartGlobalarray objectAtIndex:selectedindex];
        
        NSLog(@"sltitem.userlistingidstr %@",sltitem.userlistingidstr);
        NSLog(@"sltitem.itemidstr %@",sltitem.itemidstr);
        NSLog(@"sltitem.itemNamestr %@",sltitem.itemNamestr);

            NSLog(@"Object reference not set to an instance of an object.");
    }
    else
    {
        if([[responsedict valueForKey:@"message"]isEqualToString:@"success"])
        {
            
        }
        else
            [AppDelegate alertmethod:appname :[responsedict valueForKey:@"message"]];
    }
    });
    
    
}

@end
