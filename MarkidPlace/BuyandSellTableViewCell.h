//
//  BuyandSellTableViewCell.h
//  MarkidPlace
//
//  Created by Stellent Software on 5/31/14.
//  Copyright (c) 2014 stellentmac1. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BuyandSellTableViewCell : UITableViewCell


@property(nonatomic,strong)IBOutlet UILabel *costlbl;
@property(nonatomic,strong)IBOutlet UILabel *sizelbl;
@property(nonatomic,strong)IBOutlet UILabel *sellerlbl;
@property(nonatomic,strong)IBOutlet UILabel *itemtitlelbl;
@property(nonatomic,strong)IBOutlet UILabel *orderlbl;

@property(nonatomic,strong)IBOutlet UILabel *sellernamelbl;
@property(nonatomic,strong)IBOutlet UILabel *sizename;

@property(nonatomic,strong)IBOutlet UIImageView *itemimageview;

@property(nonatomic,strong)IBOutlet UILabel *statuslbl;
@property(nonatomic,strong)IBOutlet UILabel *statusnamelbl;

@property(nonatomic,strong)IBOutlet UIImageView *trackBGImgView;
@property(nonatomic,strong)IBOutlet UIButton *trackButton;

@end
