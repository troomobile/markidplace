//
//  ItemsModelobject.m
//  MarkidPlace
//
//  Created by Stellent Software on 5/28/14.
//  Copyright (c) 2014 stellentmac1. All rights reserved.
//

#import "ItemsModelobject.h"

@implementation ItemsModelobject

@synthesize itemNamestr,totalpricestr;
@synthesize listingcoststr,itemnumstr;
@synthesize salecostcoststr;
@synthesize sizestr;
@synthesize brandstr;
@synthesize catogrystr;
@synthesize conditionstr;
@synthesize descriptionstr;
@synthesize statusbool;
@synthesize itemidstr,sellnamestr;
@synthesize userlistingidstr,useridstr;
@synthesize  unitprisestr,picturearray;
@synthesize itemStatus;
@synthesize likedbool,sellernamestr,forsalebool,orderstatusstr,usernamestring,namestr,shippingChargesStr;

@synthesize orderDetailsobj,alteraddressbool;

@synthesize citystring;
@synthesize statestr;
@synthesize profilepicstr,catogryIdstr,weightstring,genderstring;

@synthesize inpersonbool,uspsbool,alternadeaddressdict;
@end
