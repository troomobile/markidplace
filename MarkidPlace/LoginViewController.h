//
//  RegisterViewController.h
//  MarkidPlace
//
//  Created by stellentmac1 on 5/21/14.
//  Copyright (c) 2014 stellentmac1. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RegistrationRepository.h"
#import "AppDelegate.h"
#import "HelperProtocol.h"
#import "Reachability.h"

#import "RegisterViewController.h"
#import <Social/Social.h>
#import <Accounts/Accounts.h>
#import "HomeViewController.h"
#import "ForgetPasswordViewController.h"

@class AppDelegate;
@class RegistrationRepository;
@interface LoginViewController : UIViewController<UITextFieldDelegate,HelperProtocol>
{
    CGRect screenSize;
    id <HelperProtocol> delegate;
    UITextField *currenttextfield;
    RegistrationRepository *registrationRep;
   // AppDelegate *appdelegate;
    NSString *acessToken;
    NSString *facebookID;
    NSMutableDictionary *facebookDict;
    NSMutableDictionary *twitterDict;
}
@property(nonatomic,strong)UIImage *capturedImg;
@property(nonatomic,strong) ACAccount *account;
@property (nonatomic,strong) ACAccountStore *accountStore;
@property (nonatomic,strong) IBOutlet UINavigationBar *navBar;
@property (nonatomic,strong) IBOutlet UITextField *emailTextField;
@property (nonatomic,strong) IBOutlet UITextField *passwordTextField;
@property(nonatomic,strong) IBOutlet UIActivityIndicatorView *spinner;
@property(nonatomic,strong) IBOutlet UILabel *loginlabel;
@property(nonatomic,strong) IBOutlet UILabel *Registerlabel;
@property(nonatomic,strong)NSData *capturedImgData;
-(IBAction)loginAction:(id)sender;
-(IBAction)backAction:(id)sender;
-(IBAction)registerAction:(id)sender;
-(IBAction)forgetaction:(id)sender;

-(IBAction)twitterAction:(id)sender;

-(IBAction)facebookaction:(id)sender;
@end
