//
//  LookupModelobject.h
//  MarkidPlace
//
//  Created by Stellent Software on 5/29/14.
//  Copyright (c) 2014 stellentmac1. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LookupModelobject : NSObject


@property (nonatomic,strong)NSString *loolupid;
@property (nonatomic,strong)NSString *loolupName;
@property (nonatomic,strong)NSString *loolupValue;
@property (nonatomic,strong)NSString *loolupNumber;
@property (nonatomic,strong)NSString *loolupStatus;

@property (nonatomic,strong)NSString *parentcatid;
@property (nonatomic,strong)NSString *catDescription;

@property(nonatomic,strong) NSMutableArray *categoryDataArray;

@property(nonatomic,readwrite)BOOL isCheckMark; //By Srinadh
@property(nonatomic,readwrite)BOOL isother;

/////////ajay
@property (nonatomic,strong)NSString *sizestr;
@property (nonatomic,strong)NSString *categoryid;
@property (nonatomic,strong)NSString *sizeid;
@end
