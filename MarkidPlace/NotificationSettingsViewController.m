//
//  NotificationSettingsViewController.m
//  MarkidPlace
//
//  Created by stellent_mac_05 on 17/09/14.
//  Copyright (c) 2014 stellentmac1. All rights reserved.
//

#import "NotificationSettingsViewController.h"

@interface NotificationSettingsViewController ()


@end

@implementation NotificationSettingsViewController
@synthesize navbar,notificationcell,titlelbl,notificationlistview,profilerepo;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    appdelegate= (AppDelegate *)[[UIApplication sharedApplication]delegate];
    
    
    //calling the repository
     dispatch_async( dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
    profilerepo=[[ProfileRepository alloc]init];
    profilerepo.delegate=self;
    NSString *userid=[myappDelegate getStringFromPlist:@"userid"];
    NSLog(@"the userid is %@",userid);
    [myappDelegate startspinner:self.view];
    [profilerepo getnotificationsettings:userid];
     });
    
    self.navbar.frame = CGRectMake(0, 0, 320, 64);
    [self.navbar setBackgroundImage:[UIImage imageNamed:@"navbar.png"] forBarMetrics:UIBarMetricsDefault];
    self.titlelbl.font = [UIFont fontWithName:@"Monroe" size:18];
    

}
#pragma mark--Button Actions
//this action perfrorms navigation back action
-(IBAction)backbtnAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
//this action perfrorms pop the suggestion view controller
-(IBAction)homebtnaction:(id)sender
{
    NSArray *arr=self.navigationController.viewControllers;
    for (UIViewController *childvc in arr) {
        
        if([childvc isKindOfClass:[SuggestionsViewController class]])
        {
            SuggestionsViewController *subvc=(SuggestionsViewController *)childvc;
            subvc.ismyfeed=NO;
            [self.navigationController popToViewController:subvc animated:YES];
        }
    }
    
}

//SwitchButtonAction
-(IBAction)switchBtnAction:(id)sender
{
    
    UIButton*switchbtn=(UIButton*)sender;
    NSString *statuskey;
    NSString *mainkey;
    
    if([sender tag]==0)
    {
        if(isFollow==YES)
        {
            isFollow=NO;
            isswitch=YES;
            statuskey=@"false";
        }
        else
        {
            isFollow=YES;
            isswitch=NO;
            statuskey=@"true";

        }
        mainkey=@"IsFollows";
    }
    if([sender tag]==1)
    {
        if(islike==YES)
        {
            islike=NO;
            isswitch=YES;
            statuskey=@"false";


        }
        else
        {
            islike=YES;
            isswitch=NO;
            statuskey=@"true";

        }
        mainkey=@"IsLikesorShares";

    }
    if([sender tag]==2)
    {
        if(ismessage==YES)
        {
            ismessage=NO;
            isswitch=YES;
            statuskey=@"false";

        }
        else
        {
            ismessage=YES;
            isswitch=NO;
            statuskey=@"true";

        }
        mainkey=@"IsMessages";

    }
    if([sender tag]==3)
    {
        if(islisting==YES)
        {
            islisting=NO;
            isswitch=YES;
            statuskey=@"false";

        }
        else
        {
            islisting=YES;
            isswitch=NO;
            statuskey=@"true";

        }
        mainkey=@"IsListingSold";

    }
    if([sender tag]==4)
    {
        if(isannouncements==YES)
        {
            isannouncements=NO;
            isswitch=YES;
            statuskey=@"false";

        }
        else
        {
            isannouncements=YES;
            isswitch=NO;
            statuskey=@"true";

        }
        mainkey=@"IsAnnouncements";
    }
    
    if (isswitch==YES)
    {
        isswitch=NO;
        [switchbtn setImage:[UIImage imageNamed:@"switch_off.png"] forState:UIControlStateNormal];
        NSLog(@"the sender tag<<<<<====%d =>>>> is off",[sender tag]);
    }
    else
    {
        isswitch=YES;
        [switchbtn setImage:[UIImage imageNamed:@"switch_on.png"] forState:UIControlStateNormal];
        NSLog(@"the sender tag<<<<<====%d =>>>> is on",[sender tag]);
    }
    profilerepo=[[ProfileRepository alloc]init];
    profilerepo.delegate=self;
    NSString *userid=[myappDelegate getStringFromPlist:@"userid"];
    NSLog(@"the userid is %@",userid);
    NSLog(@"the mainkey is %@",mainkey);
     NSLog(@"the statuskey is %@",statuskey);

    
    [profilerepo notificationsettingschanges:userid :mainkey :statuskey];
    
}


#pragma mark --TableviewdelegateMethods

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 5;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 70;
}
- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier=@"acell";
    
    NotificationCell *tbcell =(NotificationCell*)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (tbcell==nil)
    {
        [[NSBundle mainBundle] loadNibNamed:@"NotificationSettingsCell" owner:self options:nil];
        tbcell=notificationcell;
        tbcell.selectionStyle=UITableViewCellSelectionStyleNone;
    }
    tbcell.notificationlbl.font = [UIFont fontWithName:@"Muli" size:15];
    tbcell.notificationtimelbl.font = [UIFont fontWithName:@"Muli" size:11];
    tbcell.switchbtn.tag=indexPath.row;

    if(indexPath.row==0)
    {
        isswitch=isFollow;
        tbcell.notificationlbl.text=@"Follows";
        tbcell.notificationtimelbl.text=@"Notify when someone follows you.";
    }
    else if(indexPath.row==1)
    {
        isswitch=islike;
        tbcell.notificationlbl.text=@"Likes or Shares";
        tbcell.notificationtimelbl.text=@"Notify when someone likes or shares your listings.";
    }
    else if(indexPath.row==2)
    {

        isswitch=ismessage;
        tbcell.notificationlbl.text=@"Messages";
        tbcell.notificationtimelbl.text=@"Notify when someone sends you a message.";
    }
    else if(indexPath.row==3)
    {
        isswitch=islisting;

        tbcell.notificationlbl.text=@"Listing Sold";
        tbcell.notificationtimelbl.text=@"Notify when someone has purchased my listing.";
    }
    else if(indexPath.row==4)
    {
        isswitch=isannouncements;
        tbcell.notificationlbl.text=@"Announcements";
        tbcell.notificationtimelbl.text=@"Notify me when there are promotions or policy channges.";
    }
    
    if (isswitch==YES)
    {
        [tbcell.switchbtn setImage:[UIImage imageNamed:@"switch_on.png"] forState:UIControlStateNormal];
        NSLog(@"the indexpath.row<<<<<====%d =>>>> is on",indexPath.row);
    }
    else
    {
        [tbcell.switchbtn setImage:[UIImage imageNamed:@"switch_off.png"] forState:UIControlStateNormal];
    }
      // [tbcell.switchbtn addTarget:self action:@selector(switchBtnAction:) forControlEvents:UIControlEventTouchUpInside];
        NSLog(@"the indexpath.row<<<<<====%d =>>>> is off",indexPath.row);
        return tbcell;
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//Successresponse of the notificationsettings
-(void)successResponseforNotification:(NSDictionary *)responseDict
{
    NSLog(@"the dictionary is %@",responseDict);
 dispatch_async(dispatch_get_main_queue(), ^{
     [myappDelegate stopspinner:self.view];
    NSDictionary *mainDict=responseDict;
 //  NSString *followstr=[mainDict valueForKey:@"isFollows"];
    NSString *likestr=[mainDict valueForKey:@"isLikesorShares"];
    NSString *messagestr=[mainDict valueForKey:@"isMessages"];
    NSString *listingstr=[mainDict valueForKey:@"isListingSold"];
    NSString *annoucementsstr=[mainDict valueForKey:@"isAnnouncements"];
  //  NSLog(@"the followstr is %@",followstr);
     
     
    isFollow = [[mainDict valueForKey:@"isFollows"] boolValue];
    islike = [likestr boolValue];
    ismessage = [messagestr boolValue];
    islisting = [listingstr boolValue];
    isannouncements = [annoucementsstr boolValue];
    [notificationlistview reloadData];
 });
}
-(void)internetConnection
{
    UIAlertView *Internetalert=[[UIAlertView alloc]initWithTitle:appname message:@"Internet connection not available" delegate:self cancelButtonTitle:nil otherButtonTitles:@"Ok", nil];
    [Internetalert show];
    [appdelegate stopspinner:self.view];
    
}
@end
