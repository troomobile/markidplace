//
//  NotificationViewController.m
//  MarkidPlace
//
//  Created by stellent_mac_05 on 10/06/14.
//  Copyright (c) 2014 stellentmac1. All rights reserved.
//

#import "NotificationViewController.h"

@interface NotificationViewController ()

@end

@implementation NotificationViewController
@synthesize navbar,notificationlistview,notiicationcell,titlelbl,getlistarray;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

#pragma mark--view methods
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    appdelegate= (AppDelegate *)[[UIApplication sharedApplication]delegate];
    NSString*mainstr=@"0";
    [APPDELEGATE insertStringInPlist:@"notificationcount" value:[NSString stringWithFormat:@"%d",0]];
    self.navbar.frame = CGRectMake(0, 0, 320, 64);
    [self.navbar setBackgroundImage:[UIImage imageNamed:@"navbar.png"] forBarMetrics:UIBarMetricsDefault];
    self.titlelbl.font = [UIFont fontWithName:@"Monroe" size:18];
    
   /// [AppDelegate alertmethod:appname :@"This is a static screen. All the data displayed in this screen is static. We are working on it we will finish this soon. Until then, please check the design. Thankyou "];
    
    
    
    
    [myappDelegate startspinner:self.view];
    
    
    notificationarray=[NSMutableArray new];
    
    
     start =0;
    
    NSString *userid=[myappDelegate getStringFromPlist:@"userid"];
    
    
    registrationRep=[[RegistrationRepository alloc ]init];
    registrationRep.delegate=self;
   
    NSString *startcount = [NSString stringWithFormat:@"%d",start];
   [registrationRep getdata :nil :@"getNotifications" :@"GET" withcount:startcount];
    
    
    profilerep=[[ProfileRepository alloc ]init];
    profilerep.delegate=self;
    [profilerep notificationbadge:userid];
     


}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark--Button Actions
//this action perfrorms navigation back action
-(IBAction)backbtnAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

//this action perfrorms pop the suggestion view controller
-(IBAction)homebtnaction:(id)sender
{
    NSArray *arr=self.navigationController.viewControllers;
    for (UIViewController *childvc in arr) {
        
        if([childvc isKindOfClass:[SuggestionsViewController class]])
        {
            SuggestionsViewController *subvc=(SuggestionsViewController *)childvc;
            subvc.ismyfeed=NO;
            [self.navigationController popToViewController:subvc animated:YES];
        }
    }
    
}

#pragma mark --TableviewdelegateMethods

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(start+10 ==[notificationarray count])
    {
        return [notificationarray count]+1;
    }
    else
    {
        return [notificationarray count];
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 84;
}
- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *cellIdentifier=@"acell";
    
    NotificationCell *tbcell =(NotificationCell*)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (tbcell==nil)
    {
        [[NSBundle mainBundle] loadNibNamed:@"NotificationCell" owner:self options:nil];
        tbcell=notiicationcell;
        tbcell.selectionStyle=UITableViewCellSelectionStyleNone;
    }
    tbcell.notificationlbl.font = [UIFont fontWithName:@"Muli" size:13];
    tbcell.notificationtimelbl.font = [UIFont fontWithName:@"Muli" size:13];
   
    
    if(indexPath.row<[notificationarray count])
    {
        
        NotificationMiodelObject *notobj=[notificationarray objectAtIndex:indexPath.row];
        
        // if(indexPath.row==1)
        {
            tbcell.notificationlbl.text=notobj.pushmsgstr;
            
            NSString*timestr=[NSDate postedTimeAgoWithPostedDateString: notobj.timestr];
            NSString *tempstr=[timestr substringFromIndex: [timestr length] - 1];
            ////////NSLog(@"the tempstr is %@",tempstr);
            if([tempstr isEqualToString:@"y"])
            {
                NSArray *mainArray=[timestr componentsSeparatedByString:@"y"];
                ////////NSLog(@"the [mainArray objectAtIndex:0]] is %@",[mainArray objectAtIndex:0]);
                if( [[mainArray objectAtIndex:0] isEqualToString:@"1"])
                {
                    tbcell.notificationtimelbl.text=[NSString stringWithFormat:@"%@ year ago",[mainArray objectAtIndex:0]];
                }
                else
                {
                    tbcell.notificationtimelbl.text=[NSString stringWithFormat:@"%@ years ago",[mainArray objectAtIndex:0]];
                }
                
            }
            
            
            if([tempstr isEqualToString:@"M"])
            {
                NSArray *mainArray=[timestr componentsSeparatedByString:@"M"];
                ////////NSLog(@"the [mainArray objectAtIndex:0]] is %@",[mainArray objectAtIndex:0]);
                if( [[mainArray objectAtIndex:0] isEqualToString:@"1"])
                {
                    tbcell.notificationtimelbl.text=[NSString stringWithFormat:@"%@ month ago",[mainArray objectAtIndex:0]];
                }
                else
                {
                    
                    tbcell.notificationtimelbl.text=[NSString stringWithFormat:@"%@ months ago",[mainArray objectAtIndex:0]];
                }
            }
            
            if([tempstr isEqualToString:@"w"])
            {
                NSArray *mainArray=[timestr componentsSeparatedByString:@"w"];
                ////////NSLog(@"the [mainArray objectAtIndex:0]] is %@",[mainArray objectAtIndex:0]);
                if( [[mainArray objectAtIndex:0] isEqualToString:@"1"])
                {
                    tbcell.notificationtimelbl.text=[NSString stringWithFormat:@"%@ week ago",[mainArray objectAtIndex:0]];
                }
                else
                {
                    
                    tbcell.notificationtimelbl.text=[NSString stringWithFormat:@"%@ weeks ago",[mainArray objectAtIndex:0]];
                }
            }
            if([tempstr isEqualToString:@"d"])
            {
                NSArray *mainArray=[timestr componentsSeparatedByString:@"d"];
                //////NSLog(@"the [mainArray objectAtIndex:0]] is %@",[mainArray objectAtIndex:0]);
                if( [[mainArray objectAtIndex:0] isEqualToString:@"1"])
                {
                    tbcell.notificationtimelbl.text=[NSString stringWithFormat:@"%@ day ago",[mainArray objectAtIndex:0]];
                }
                else
                {
                    
                    tbcell.notificationtimelbl.text=[NSString stringWithFormat:@"%@ days ago",[mainArray objectAtIndex:0]];
                }
            }
            if([tempstr isEqualToString:@"h"])
            {
                NSArray *mainArray=[timestr componentsSeparatedByString:@"h"];
                //////NSLog(@"the [mainArray objectAtIndex:0]] is %@",[mainArray objectAtIndex:0]);
                if( [[mainArray objectAtIndex:0] isEqualToString:@"1"])
                {
                    tbcell.notificationtimelbl.text=[NSString stringWithFormat:@"%@ hour ago",[mainArray objectAtIndex:0]];
                }
                else
                {
                    
                    tbcell.notificationtimelbl.text=[NSString stringWithFormat:@"%@ hours ago",[mainArray objectAtIndex:0]];
                }
            }
            if([tempstr isEqualToString:@"m"])
            {
                NSArray *mainArray=[timestr componentsSeparatedByString:@"m"];
                //////NSLog(@"the [mainArray objectAtIndex:0]] is %@",[mainArray objectAtIndex:0]);
                if( [[mainArray objectAtIndex:0] isEqualToString:@"1"])
                {
                    tbcell.notificationtimelbl.text=[NSString stringWithFormat:@"%@ minute ago",[mainArray objectAtIndex:0]];
                }
                else
                {
                    tbcell.notificationtimelbl.text=[NSString stringWithFormat:@"%@ minutes ago",[mainArray objectAtIndex:0]];
                }
            }
            if([tempstr isEqualToString:@"s"])
            {
                NSArray *mainArray=[timestr componentsSeparatedByString:@"s"];
                //////NSLog(@"the [mainArray objectAtIndex:0]] is %@",[mainArray objectAtIndex:0]);
                tbcell.notificationtimelbl.text=[NSString stringWithFormat:@"%@ seconds ago",[mainArray objectAtIndex:0]];
            }
            
            
            
            // tbcell.shareimgview.image=[UIImage imageNamed:@"mail.png"];
            tbcell.shareimgview.frame=CGRectMake(270, 30, 27, 17);
        }
        // else if (indexPath.row==2)
        {
            //   tbcell.notificationlbl.text=@"@username sent you a message.";
            // tbcell.shareimgview.image=[UIImage imageNamed:@"mail.png"];
            // tbcell.shareimgview.frame=CGRectMake(270, 30, 27 , 17);
        }
        
    
        CALayer *layer2 = tbcell.userimgview.layer;
        [layer2 setCornerRadius:32.0];
        [layer2 setBorderWidth:0];
        [layer2 setMasksToBounds:YES];
        [tbcell.userimgview setImageWithURL:[NSURL URLWithString:notobj.imagestr ] placeholderImage:[UIImage imageNamed:@"userimg.png"] options:SDWebImageCacheMemoryOnly];
    
    }
    
    else
    {
     //[myappDelegate startspinner:self.view];
       [self loadMoreRecords];
    }
    
    
    return tbcell;
   
    
}



-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NotificationMiodelObject*modelobj=[notificationarray objectAtIndex:indexPath.row];
if([modelobj.notificationtype isEqualToString:@"likesorshares"])
{
    //[AppDelegate alertmethod:appname :@"MyLikes Under implementation"];
    ListingDetailViewController *MywishVC;
    if (myappDelegate.isiphone5)
    {
        MywishVC=[[ListingDetailViewController alloc]initWithNibName:@"ListingDetailViewController" bundle:nil];
    }
    else
    {
        MywishVC=[[ListingDetailViewController alloc]initWithNibName:@"ListingDetailViewController_iphone4" bundle:nil];
    }
    
    NSMutableArray *itemsarray=[[NSMutableArray alloc]init];
    
    ItemsModelobject *itemobj=[[ItemsModelobject alloc]init];
    
    
    itemobj.useridstr=APPDELEGATE.useridstring;
    
    itemobj.usernamestring=[APPDELEGATE getStringFromPlist:usernamekey];
   
    
    itemobj.picturearray=[NSMutableArray new];
    
    
    // [itemobj.picturearray addObjectsFromArray:photosarray];
    
    
    itemobj.itemidstr=modelobj.itemidstr;
    itemobj.userlistingidstr=modelobj.itemidstr;
    itemobj.citystring=@"";
    itemobj.statestr=@"";
    
    [itemsarray addObject:itemobj];
    
    MywishVC.listingmainarray=itemsarray;
    MywishVC.senderIndex=0;
    MywishVC.sltobj=itemobj;
    MywishVC.condstring=@"New";
    MywishVC.iscompare=YES;
    
    
    MywishVC.nameString = modelobj.nameStr;
    MywishVC.usernameString = modelobj.userNameStr;
    MywishVC.imageString = modelobj.profilepicStr;
    
    
    
    APPDELEGATE.suggestionviewcont.loaddatabool=YES;
    

    [self.navigationController pushViewController:MywishVC animated:YES];
   }
if([modelobj.notificationtype isEqualToString:@"follows"])
    {
            FollowViewController *followVC;
            if (myappDelegate.isiphone5)
            {
                followVC = [[FollowViewController alloc]initWithNibName:@"FollowViewController" bundle:nil];
            }
            else
            {
                followVC = [[FollowViewController alloc]initWithNibName:@"FollowViewController_iphone4" bundle:nil];
            }
             followVC.notifystr=@"Notification";
            followVC.useridstr=[NSString stringWithFormat:@"%@",modelobj.useridstr];
         NSLog(@"the followVC.useridstris %@",followVC.useridstr);
    
        
        
        followVC.nameString = modelobj.nameStr;
        followVC.usernameString = modelobj.userNameStr;
        followVC.imageString = modelobj.profilepicStr;
        followVC.compareStr = @"compare";
        followVC.imFronVC = @"NotificationViewController";
        
         followVC.listdataarray=myappDelegate.suggestonsGlobalarray;
        
            [self.navigationController pushViewController:followVC animated:YES];
            
    }
    if([modelobj.notificationtype isEqualToString:@"orders"])
          {
              BuyandSellViewController *sell;

        if (myappDelegate.isiphone5)
        {
            sell=[[BuyandSellViewController alloc]initWithNibName:@"BuyandSellViewController" bundle:nil];
        }
        else
        {
            sell=[[BuyandSellViewController alloc]initWithNibName:@"BuyandSellViewController_iphone4" bundle:nil];
        }
        sell.isbuyer=NO;
        [self.navigationController pushViewController:sell animated:YES];
          }
}
-(void)updatenotificationsuccessResponse:(NSDictionary *)responsedict
{
    //////NSLog(@"responsedict....%@",responsedict);
    if ([[responsedict objectForKey:@"message"] isEqualToString:@"NotificationLog Updated Successfully"])
    {
        NSLog(@"$$$$$$$$Response$$$$$$$$$%@",responsedict);
    }
    
    
}



-(void)successresponceListing :(id)respdict :(NSString *)paramname;
{
    NSLog(@"respdict....%@",respdict);
    dispatch_async(dispatch_get_main_queue(), ^{

    if ([paramname isEqualToString:@"getNotifications"])
    {
        //////NSLog(@"resp %@",respdict);
        if ([respdict isKindOfClass:[NSArray class]])
        {
            
            //[notificationarray removeAllObjects];
            NSArray *resparray=(NSArray *)respdict;
            
            for (int i=0; i<resparray.count; i++)
            {
                NSDictionary *subdict=[resparray objectAtIndex:i];
                NotificationMiodelObject *notmodobj=[[NotificationMiodelObject alloc]init];
                
                
                notmodobj.senderidstr=[NSString stringWithFormat:@"%@",[subdict valueForKey:@"senderId" ]];
                notmodobj.useridstr=[NSString stringWithFormat:@"%@",[subdict valueForKey:@"userId"]];
                
                notmodobj.nameStr=[NSString stringWithFormat:@"%@",[subdict valueForKey:@"name"]];
                
                notmodobj.userNameStr=[NSString stringWithFormat:@"%@",[subdict valueForKey:@"userName"]];
                
                notmodobj.profilepicStr=[NSString stringWithFormat:@"%@",[subdict valueForKey:@"image"]];
            
                notmodobj.notificationtype=[subdict valueForKey:@"notificationType"];
                notmodobj.pushmsgstr=[NSString stringWithFormat:@"%@",[subdict valueForKey:@"pushMessage" ]];
                notmodobj.imagestr=[NSString stringWithFormat:@"%@",[subdict valueForKey:@"image" ]];
                notmodobj.timestr=[subdict valueForKey:@"createdDate"];
               // createdDate
                notmodobj.itemidstr=[NSString stringWithFormat:@"%@",[subdict valueForKey:@"itemId" ]];
                 notmodobj.notificationid=[NSString stringWithFormat:@"%@",[subdict valueForKey:@"notificationLogId" ]];
                
                notmodobj.countstr=[NSString stringWithFormat:@"%@",[subdict valueForKey:@"count" ]];
               [APPDELEGATE insertStringInPlist:@"notificationcount" value:[NSString stringWithFormat:@"%@",notmodobj.countstr]];
                
                [notificationarray addObject:notmodobj];
                
                
                
            }
        }

        
    }
        
        NSLog(@"******Notification*****%lu",(unsigned long)notificationarray.count);
        NSLog(@"******Notification*****%@",notificationarray);
          [notificationlistview reloadData];
        [myappDelegate stopspinner:self.view];

});
  
}
-(void)internetConnection
{
    UIAlertView *Internetalert=[[UIAlertView alloc]initWithTitle:appname message:@"Internet connection not available" delegate:self cancelButtonTitle:nil otherButtonTitles:@"Ok", nil];
    [Internetalert show];
    [appdelegate stopspinner:self.view];
    
}


-(void)loadMoreRecords
{
    
    start=start+10;
    
    NSString *startcount = [NSString stringWithFormat:@"%d",start];
    [registrationRep getdata :nil :@"getNotifications" :@"GET" withcount:startcount];
    

    // Call service here to get more records
}

@end
