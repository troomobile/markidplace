//
//  FilterViewController.m
//  MarkidPlace
//
//  Created by stellentmac1 on 5/21/14.
//  Copyright (c) 2014 stellentmac1. All rights reserved.
//

#import "FilterViewController.h"

@interface FilterViewController ()

@end

@implementation FilterViewController
@synthesize filtercell,filterarray,genderlbl,gendercancelbtn;

@synthesize dropdownview,searchbar,request,responseData,popview,filterlistview,buttonview,poptitlelable,genderpopview,genderdropdownview,cancelbtn;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
#pragma mark view methods

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
     //  myappDelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];

    [searchbar setShowsCancelButton:NO animated:YES];
    searchbar.text=@"";
    filterlistview.scrollEnabled=NO;
    currentTextField=[[UITextField alloc]init];
    navbar.frame = CGRectMake(0, 0, 320, 64);
    [navbar setBackgroundImage:[UIImage imageNamed:@"navbar.png"] forBarMetrics:UIBarMetricsDefault];
//    genderlbl.font = [UIFont fontWithName:@"Muli" size:16];
  //  gendercancelbtn.titleLabel.font=[UIFont fontWithName:@"Muli" size:16];
    titlelbl.font = [AppDelegate navtitlefont];
    poptitlelable.font=[AppDelegate navtitlefont];
    cancelbtn.titleLabel.font = [UIFont fontWithName:@"Muli" size:14];
   genderlbl.font=[AppDelegate navtitlefont];
    gendercancelbtn.titleLabel.font = [UIFont fontWithName:@"Muli" size:14];
    statestr=@"";
    latitudestr=@"";
    longitudestr=@"";
    
    [self.view addSubview:popview];
    CGRect popfrm=popview.frame;
    popfrm.origin.x=0;
    popfrm.origin.y=595;
    popview.frame=popfrm;
    

     //  dropdownview.hidden=YES;
     [self.view addSubview:genderpopview];
     CGRect genderpopfrm=genderpopview.frame;
    genderpopfrm.origin.x=0;
    genderpopfrm.origin.y=595;
    genderpopview.frame=genderpopfrm;
    [self filtermethod];
    

   }
-(void)viewWillAppear:(BOOL)animated
{
    
    [searchbar resignFirstResponder];
    filterlistview.tableFooterView=buttonview;
    [filterlistview reloadData];
    CGRect tablefrm=filterlistview.frame;
    tablefrm.size.height=filterlistview.contentSize.height;
    filterlistview.frame=tablefrm;
    
    locationManager = [[CLLocationManager alloc] init];
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0"))
    {
   //     [locationManager requestWhenInUseAuthorization];
        [locationManager requestWhenInUseAuthorization];
    }
  
    locationManager.delegate = self;
    locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters;
    [locationManager startUpdatingLocation];

    
    if (searchbar.text.length>0)
    {
            [searchbar becomeFirstResponder];
    }
    

    
   // filterlistview.scrollEnabled=NO;
}
//get the current location

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    CLGeocoder *geocoder = [[CLGeocoder alloc] init];
    
    CLLocation *currentLocation = newLocation;
    
    //CLLocation *location = [locationManager location];
    
    
    CLLocationCoordinate2D coordinate = [currentLocation coordinate];
    
   latitudestr = [NSString stringWithFormat:@"%f", coordinate.latitude];
    longitudestr = [NSString stringWithFormat:@"%f", coordinate.longitude];
    
    NSLog(@"MY HOME :%@", latitudestr);
    NSLog(@"MY HOME: %@ ", longitudestr);
    
    
    if (currentLocation != nil)
        NSLog(@"longitude = %.8f\nlatitude = %.8f", currentLocation.coordinate.longitude,currentLocation.coordinate.latitude);
    
    // stop updating location in order to save battery power
    [locationManager stopUpdatingLocation];
    
    
    [geocoder reverseGeocodeLocation:currentLocation completionHandler:^(NSArray *placemarks, NSError *error)
     {
         NSLog(@"Found placemarks: %@, error: %@", placemarks, error);
         if (error == nil && [placemarks count] > 0)
         {
             CLPlacemark *placemark = [placemarks lastObject];
             
             // strAdd -> take bydefault value nil
             NSString *strAdd = nil;
             
             if ([placemark.subThoroughfare length] != 0)
                 strAdd = placemark.subThoroughfare;
             
             if ([placemark.thoroughfare length] != 0)
             {
                 // strAdd -> store value of current location
                 if ([strAdd length] != 0)
                     strAdd = [NSString stringWithFormat:@"%@, %@",strAdd,[placemark thoroughfare]];
                 else
                 {
                     // strAdd -> store only this value,which is not null
                     strAdd = placemark.thoroughfare;
                 }
             }
             
             if ([placemark.postalCode length] != 0)
             {
                 if ([strAdd length] != 0)
                     strAdd = [NSString stringWithFormat:@"%@, %@",strAdd,[placemark postalCode]];
                 else
                     strAdd = placemark.postalCode;
             }
             
             if ([placemark.locality length] != 0)
             {
                 if ([strAdd length] != 0)
                     strAdd = [NSString stringWithFormat:@"%@, %@",strAdd,[placemark locality]];
                 else
                     strAdd = placemark.locality;
                
             }
             
             if ([placemark.administrativeArea length] != 0)
             {
                 if ([strAdd length] != 0)
                     strAdd = [NSString stringWithFormat:@"%@, %@",strAdd,[placemark administrativeArea]];
                 else
                     strAdd = placemark.administrativeArea;
                 NSLog(@"placemark.administrativeArea is %@,,,,%@",strAdd,placemark.administrativeArea);
                 
                 NSLog(@"placemark.administrativeArea is %@,,,,%@",strAdd,placemark.administrativeArea);
                 statestr=placemark.administrativeArea;
             }
             
             if ([placemark.country length] != 0)
             {
                 if ([strAdd length] != 0)
                     strAdd = [NSString stringWithFormat:@"%@, %@",strAdd,[placemark country]];
                 else
                     strAdd = placemark.country;
                 
             }
             
             
         }
     }];
}
-(void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    NSLog(@"Location error %@",error.description);
    longitudestr=@"";
    latitudestr=@"";
}
// This method is for assigning the fonts and allocating the fonts and array.

-(void)filtermethod
{
   // self.sortLabel.font = [UIFont fontWithName:@"Muli" size:15];
    
    pricearray=[[NSMutableArray alloc]initWithObjects:@"$1 - $20",@"$21 - $30",@"$31 - $50",@"$51 - $70",@"$71 - $100",@"$101 - $150", nil];
  //  genderarray=[[NSMutableArray alloc]initWithObjects:@"Boys",@"Girls",@"Unisex",@"Women", nil];
    
    
  //  self.sortNameLabel.font = [UIFont fontWithName:@"Muli" size:17];
       dropdownview.layer.borderWidth = 1;
    dropdownview.layer.cornerRadius=1.0f;
    dropdownview.layer.borderColor = [[UIColor colorWithRed:12.0/255.0 green:174.0/255.0 blue:180.0/255.0 alpha:1.0] CGColor];
    
    genderdropdownview.layer.borderWidth = 1;
    genderdropdownview.layer.cornerRadius=1.0f;
    genderdropdownview.layer.borderColor = [[UIColor colorWithRed:12.0/255.0 green:174.0/255.0 blue:180.0/255.0 alpha:1.0] CGColor];
    

    registrationRep=[[RegistrationRepository alloc ]init];
    
    registrationRep.delegate=self;
    [registrationRep getdistance:@"getdistance"];

    // This is used for checking the data in the dropdown view.
    
    if (myappDelegate.catogoryGlobalarray.count<1 || myappDelegate.conditionGlobalarray.count<1 || myappDelegate.sizeGlobalarray.count<1 || myappDelegate.brandGlobalarray.count<1)
    {
        [myappDelegate startspinner:self.view];
        [ registrationRep getdata:nil :@"getcatogery" :@"GET" withcount:@"0"];
    }
    
    sizestr=[NSString new];
    categorystr=[NSString new];
    pricestr=[NSString new];
    brandstr=[NSString new];
    genderstr=[NSString new];
    distancestr=[NSString new];
    dropdownview.layer.borderWidth = 1;
    dropdownview.layer.cornerRadius=1.0f;
    genderdropdownview.layer.borderWidth=1;
    genderdropdownview.layer.cornerRadius=1.0f;
    genderdropdownview.layer.borderColor=[[UIColor whiteColor]CGColor];
    dropdownview.layer.borderColor =[[UIColor whiteColor ] CGColor];
    isdropdown=NO;
    
    // This  for loop is used to adding the object in the array, loading in a tableview.
    
    filterarray=[NSMutableArray new];
    for (int i=0; i<6; i++)
    {
        FilterObject *obj=[[FilterObject alloc]init];
        if (i==0)
        {
            obj.isother=NO;
            obj.cellnamestr=@"Category";
            obj.cellvaluestr=@"";
            obj.otherstr=@"";
        }
        else if (i==1)
        {
            obj.isother=NO;
            obj.cellnamestr=@"Size";
            obj.cellvaluestr=@"";
            obj.otherstr=@"";
        }
        else if (i==2)
        {
            obj.isother=NO;
            obj.cellnamestr=@"Gender";
            obj.cellvaluestr=@"";
            obj.otherstr=@"";
        }
        else if (i==3)
        {
            obj.isother=NO;
            obj.cellnamestr=@"Price";
            obj.cellvaluestr=@"";
            obj.otherstr=@"";
        }
        
        else if (i==4)
        {
            obj.isother=NO;
            obj.cellnamestr=@"Brand";
            obj.cellvaluestr=@"";
            obj.otherstr=@"";
        }
        else if (i==5)
        {
            obj.isother=NO;
            obj.cellnamestr=@"Distance";
            obj.cellvaluestr=@"";
            obj.otherstr=@"";
        }
        
        [filterarray addObject:obj];
        NSLog(@"trhe number is%d",filterarray.count);
    }
    
    
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}
#pragma mark button Actions

-(IBAction)backAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
//Home Button ction
-(IBAction)homeBtnAction:(id)sender
{

    NSArray *arr=self.navigationController.viewControllers;
    for (UIViewController *childvc in arr) {

        if([childvc isKindOfClass:[SuggestionsViewController class]])
        {
            SuggestionsViewController *subvc=(SuggestionsViewController *)childvc;
            subvc.ismyfeed=NO;
            [self.navigationController popToViewController:subvc animated:YES];
        }
    }
    
}
//Filter Button ction
-(IBAction)filterBtnAction:(id)sender
{
    
    [currentTextField resignFirstResponder];
    
    
    {
        CGRect frame = self.view.frame;
        frame.origin.y=0;
        self.view.frame=frame;
        //    NSNumber *sizenumber;
        //    NSNumber *pricenumber;
        //    NSNumber *sortnumber;
        NSNumber *catnumber;
        //    NSNumber *brandnumber;
        
        [searchbar resignFirstResponder];
        searchbar.text=@"";
        
        UIButton *mybtn=(UIButton *)sender;
        dropdownview.tag=[sender tag];
        genderdropdownview.tag=[sender tag];
        if ([sender tag] == 777)
        {
            poptitlelable.text=@"Gender";
            if(isbrand==YES)
            {
                
                isbrand=NO;
            }
            else
            {
                
                isbrand=YES;
                //  dropdownview.hidden=NO;
            }
            iscategory=NO;
            isprice=NO;
            issize=NO;
            
            
            
            //[dropdownview reloadData];
            [genderdropdownview reloadData];
            genderdropdownview.contentOffset=CGPointMake(0, 0);
            
            //[self popviewmethod:YES];
            [self performSelector:@selector(genderpopviewmethod:) withObject:[NSNumber numberWithBool:YES] afterDelay:0.3];
        }
        else
        {
            if ([sender tag]==111)
            {
                if(parentid.length==0)
                {
                    UIAlertView *categoryalert=[[UIAlertView alloc]initWithTitle:appname message:@"Select category" delegate:self cancelButtonTitle:nil otherButtonTitles:@"Ok", nil];
                    [categoryalert show];
                    isdropdown=NO;
                    [filterlistview reloadData];
                }
                else
                {
                    poptitlelable.text=@"Size";
                    
                    if(issize==YES)
                    {
                        //dropdownview.frame=CGRectMake(127, mybtn.frame.origin.y+25, 184, 0);
                        issize=NO;
                        
                        // dropdownview.hidden=YES;
                        
                    }
                    else
                    {
                        
                        //  dropdownview.frame=CGRectMake(127, mybtn.frame.origin.y+25, 184, 250);
                        
                        issize=YES;
                        // dropdownview.hidden=NO;
                    }
                    
                    iscategory=NO;
                    isbrand=NO;
                    isprice=NO;
                }
            }
            if ([sender tag]==222)
            {
                poptitlelable.text=@"Price";
                issize=NO;
                isbrand=NO;
                isprice=YES;
                isdropdown=YES;
            }
            
            
            if ([sender tag]==333)
            {
                poptitlelable.text=@"Category";
                
                if(iscategory==YES)
                {
                    //  dropdownview.frame=CGRectMake(127, mybtn.frame.origin.y+25, 184, 0);
                    {
                        for (int i=0; i<myappDelegate.catogoryGlobalarray.count; i++)
                        {
                            LookupModelobject *lokupobj=[myappDelegate.catogoryGlobalarray objectAtIndex:i];
                            //  lokupobj.categoryDataArray
                            
                            for (int j=0;j<lokupobj.categoryDataArray.count;j++)
                            {
                                LookupModelobject *lokup=[lokupobj.categoryDataArray objectAtIndex:j];
                                if ([[NSString stringWithFormat:@"%@",categorystr] isEqualToString:[NSString stringWithFormat:@"%@",lokup.loolupValue]])
                                {
                                    catnumber =[NSNumber numberWithInt:[lokup.loolupid intValue] ];
                                    break;
                                }
                                
                            }
                            
                            
                            
                        }
                    }
                    
                    
                    iscategory =NO;
                    //  dropdownview.hidden=YES;
                }
                else
                {
                    
                    // dropdownview.frame=CGRectMake(127, mybtn.frame.origin.y+25, 184, 250);
                    
                    iscategory=YES;
                    // dropdownview.hidden=NO;
                }
                
                issize=NO;
                isbrand=NO;
                isprice=NO;
                isdropdown=YES;
                
            }
            if ([sender tag]==444)
            {
                poptitlelable.text=@"Brand";
                
                if(isbrand==YES)
                {
                    //   dropdownview.frame=CGRectMake(127, mybtn.frame.origin.y+25, 184, 0);
                    isbrand=NO;
                    //   dropdownview.hidden=YES;
                }
                else
                {
                    
                    
                    isbrand=YES;
                    //  dropdownview.hidden=NO;
                }
                iscategory=NO;
                isprice=NO;
                issize=NO;
                isdropdown=YES;
            }
            if ([sender tag]==777)
            {
                poptitlelable.text=@"Gender";
                if(isbrand==YES)
                {
                    // dropdownview.frame=CGRectMake(127, mybtn.frame.origin.y+25, 184, 0);
                    isbrand=NO;
                    //   dropdownview.hidden=YES;
                }
                else
                {
                    // dropdownview.frame=CGRectMake(127, mybtn.frame.origin.y+25, 184, 250);
                    
                    
                    isbrand=YES;
                    //  dropdownview.hidden=NO;
                }
                iscategory=NO;
                isprice=NO;
                issize=NO;
                isdropdown=YES;
                
                
                
                
            }
            if ([sender tag]==888)
            {
                poptitlelable.text=@"Distance";
                if(isdistance==YES)
                {
                    // dropdownview.frame=CGRectMake(127, mybtn.frame.origin.y+25, 184, 0);
                    isdistance=NO;
                    //   dropdownview.hidden=YES;
                }
                else
                {
                    // dropdownview.frame=CGRectMake(127, mybtn.frame.origin.y+25, 184, 250);
                    
                    
                    isdistance=YES;
                    //  dropdownview.hidden=NO;
                }
                iscategory=NO;
                isprice=NO;
                issize=NO;
                isbrand=NO;
                isdropdown=YES;
                
                //[AppDelegate alertmethod:appname :@"Under implementation."];
                //return;
            }
            
            //  [AppDelegate alertmethod:appname :@"Under implementation.Select Apply button "];
            
            [dropdownview reloadData];
            [genderdropdownview reloadData];
            dropdownview.contentOffset=CGPointMake(0, 0);
            
            //[self popviewmethod:YES];
            if(isdropdown)
            {
                [self performSelector:@selector(popviewmethod:) withObject:[NSNumber numberWithBool:YES] afterDelay:0.2];
            }
        }
    }
   
}

-(IBAction)applyBtnAction:(id)sender
{
    if ([sender tag]==110)
    {
        searchbar.text=@"";
        [searchbar resignFirstResponder];
        [searchbar setShowsCancelButton:NO animated:YES];
        
        
        // This is used to empty the Textfields.
        for (int i=0; i<filterarray.count; i++)
        {
            FilterObject *obj=[filterarray objectAtIndex:i];
            if (i==0)
            {
                obj.isother=NO;
                obj.cellnamestr=@"Category";
                obj.cellvaluestr=@"";
                obj.otherstr=@"";
            }
            else if (i==1)
            {
                obj.isother=NO;
                obj.cellnamestr=@"Size";
                obj.cellvaluestr=@"";
                obj.otherstr=@"";
            }
            else if (i==2)
            {
                obj.isother=NO;
                obj.cellnamestr=@"Gender";
                obj.cellvaluestr=@"";
                obj.otherstr=@"";
                genderstr=@"";
                
            }
            else if (i==3)
            {
                obj.isother=NO;
                obj.cellnamestr=@"Price";
                obj.cellvaluestr=@"";
                obj.otherstr=@"";
                
            }
            
            else if (i==4)
            {
                obj.isother=NO;
                obj.cellnamestr=@"Brand";
                obj.cellvaluestr=@"";
                obj.otherstr=@"";
            }
            else if (i==5)
            {
                obj.isother=NO;
                obj.cellnamestr=@"Distance";
                obj.cellvaluestr=@"";
                obj.otherstr=@"";
            }
            [filterlistview reloadData];
        }
       
        
    }
    else{
        //searchbar.text=@"";
        // [searchbar resignFirstResponder];
        //  [searchbar setShowsCancelButton:NO animated:YES];
      
        
        [UIView animateWithDuration:0.5 animations:^{
            
            CGRect frame = self.view.frame;
            frame.origin.y=0;
            self.view.frame=frame;
        }];
        
        
       if(([NSString stringWithFormat:@"%@",sizestr ].length==0)&&([NSString stringWithFormat:@"%@",pricestr ].length==0)&&([NSString stringWithFormat:@"%@",brandstr ].length==0)&&([NSString stringWithFormat:@"%@",genderstr ].length==0))
        {
            if ([NSString stringWithFormat:@"%@",categorystr ].length==0 && !iscategory)
            {
                [AppDelegate alertmethod:appname :@"Enter any one of the field"];
            }
            else{
                isApply=YES;
                isSearch=NO;
                
                
                [self postvalues];
            }
            
        }
        else
        {
            isApply=YES;
            isSearch=NO;
            
            
            [self postvalues];
        }
    }
}
-(IBAction)cancelBtnAction:(id)sender
{
    [self popviewmethod:NO];
    
    
}
-(IBAction)gendercancelbtnaction:(id)sender
{
    [self genderpopviewmethod:NO];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark table view


-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if(tableView==dropdownview)
    {
    if(dropdownview.tag==333)
    {
         return myappDelegate.catogoryGlobalarray.count;
    }
      else
      {
    return 1;
      }
    }
    else
    {
        return 1;
    }
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(tableView==dropdownview)
    {
        
            if (tableView.tag==333)
            {
                LookupModelobject *lookobj=[myappDelegate.catogoryGlobalarray objectAtIndex:section];
                
                return lookobj.categoryDataArray.count;
            }
            else if (tableView.tag==111)
            {
                return myappDelegate.sizeGlobalarray.count;
            }
            else if (tableView.tag==888)
            {
                return myappDelegate.distanceGlobalarray.count;
                NSLog(@"myappDelegate.distanceGlobalarray.count++++%d",myappDelegate.distanceGlobalarray.count);
            }
            else if (tableView.tag==222)
            {
                return pricearray.count;
            }
            else if (tableView.tag==444)
            {
                /*
                NSMutableDictionary *mydict= [myappDelegate.brandGlobalarray objectAtIndex:section];
                NSMutableArray *somearray=[mydict objectForKey:[mydict.allKeys objectAtIndex:0]] ;
                
                return somearray.count;
                 */
                return APPDELEGATE.brandGlobalarray.count;

            }
            else if (tableView.tag==777)
            {
                
                return myappDelegate.genderglobalaray.count;
            }
            else
            {
                return 1;
            }
        
    }
    else if(tableView == genderdropdownview)
        return myappDelegate.genderglobalaray.count;
    else
    {
        return filterarray.count;
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView==filterlistview)
    {
        FilterObject *myobj=[filterarray objectAtIndex:indexPath.row];
        if (myobj.isother==YES)
        {
            return 80;
        }
        else
        {
            return 42;
        }
    }
    else
    {
        return 42;
    }
    
    
}






-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{

    if (tableView==genderdropdownview)
    {
        
        UITableViewCell *tbcell=[tableView dequeueReusableCellWithIdentifier:@"cell"];
        if(tbcell==nil)
        {
            tbcell=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
        }
        tbcell.textLabel.font = [UIFont fontWithName:@"Muli" size:15];
        tbcell.textLabel.textColor=[UIColor whiteColor];
        //[UIColor colorWithRed:131.0/255.0 green:131.0/255.0 blue:131.0/255.0 alpha:1.0];
        tbcell.textLabel.backgroundColor=[UIColor clearColor];
        tbcell.backgroundColor=[UIColor clearColor];
        tbcell.contentView.backgroundColor=[UIColor clearColor];
        genderdropdownview.backgroundColor=[UIColor clearColor];
        if (genderdropdownview.tag==777)
        {
            NSLog(@"indexPath.row...%@",myappDelegate.genderglobalaray );
        
        LookupModelobject *lookobj=[myappDelegate.genderglobalaray objectAtIndex:indexPath.row];
            NSLog(@"lookobj.loolupValue....%@",lookobj.loolupValue);
        tbcell.textLabel.text=lookobj.loolupValue;
          return tbcell;
        }
        
        
    }
    
    if(tableView==dropdownview)
    {
    UITableViewCell *tbcell=[tableView dequeueReusableCellWithIdentifier:@"cell"];
    if(tbcell==nil)
    {
        tbcell=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    }
    tbcell.textLabel.font = [UIFont fontWithName:@"Muli" size:15];
    tbcell.textLabel.textColor=[UIColor whiteColor];
    //[UIColor colorWithRed:131.0/255.0 green:131.0/255.0 blue:131.0/255.0 alpha:1.0];
    tbcell.textLabel.backgroundColor=[UIColor clearColor];
    tbcell.backgroundColor=[UIColor clearColor];
    tbcell.contentView.backgroundColor=[UIColor clearColor];
    dropdownview.backgroundColor=[UIColor clearColor];
        
        
        
        
//This methods are used for loading the arrays in search criteria
        
        // category dropdown
        
        
        
    if (tableView.tag==333)
    {
       LookupModelobject *lookobj=[myappDelegate.catogoryGlobalarray objectAtIndex:indexPath.section];
            
            LookupModelobject *obj=[lookobj.categoryDataArray objectAtIndex:indexPath.row];
        NSLog(@"the data in the categaorydata object is %d",lookobj.categoryDataArray.count);
         tbcell.textLabel.text=obj.loolupValue;
                  return tbcell;
    }
        //size dropdownview
    else if (tableView.tag==111)
    {
        LookupModelobject *lookobj=[myappDelegate.sizeGlobalarray objectAtIndex: indexPath.row];
        NSLog(@"the data in the sizeGlobalarray object is %d",lookobj.categoryDataArray.count);

        
        tbcell.textLabel.text=lookobj.loolupValue;
        return tbcell;
    }
        //distance dropdownview
    else if (tableView.tag==888)
    {
        LookupModelobject *lookobj=[myappDelegate.distanceGlobalarray objectAtIndex: indexPath.row];
        NSLog(@"the data in the distanceGlobalarray object is %lu",(unsigned long)myappDelegate.distanceGlobalarray.count);
        
        tbcell.textLabel.text=lookobj.loolupValue;
        return tbcell;
    }

        //  price dropdownview
    else if (tableView.tag==222)
    {
       // LookupModelobject *lookobj=[appdelegate.conditionGlobalarray objectAtIndex: indexPath.row];
        
        tbcell.textLabel.text=[pricearray objectAtIndex:indexPath.row];
        return tbcell;
        
        
    }
        // brand dropdownview
    else if (tableView.tag==444)
    {
        LookupModelobject *lookobj=[APPDELEGATE.brandGlobalarray objectAtIndex: indexPath.row];
        
        tbcell.textLabel.text=lookobj.loolupValue;
        return tbcell;
        /*
            NSMutableDictionary *mydict= [myappDelegate.brandGlobalarray objectAtIndex:indexPath.section];
            NSMutableArray *somearray=[mydict objectForKey:[mydict.allKeys objectAtIndex:0]] ;
        
         LookupModelobject *lookobj=[somearray objectAtIndex: indexPath.row];
        
         tbcell.textLabel.text=lookobj.loolupValue;
         */
        
        
        return tbcell;
        
    }
        // gender dropdownview
    else if (tableView.tag==777)
    {
        LookupModelobject *lookobj=[myappDelegate.genderglobalaray objectAtIndex: indexPath.row];
        
        tbcell.textLabel.text=lookobj.loolupValue;
        
        return tbcell;
        
    }
        return tbcell;
  }
else
    {
        static NSString *MyIdentifier= @"MyIdentifiertripscell";
    FilterCell *maincell= (FilterCell *)[tableView dequeueReusableCellWithIdentifier:MyIdentifier];
    if (maincell==nil)
    {
        [[NSBundle mainBundle]loadNibNamed:@"FilterCell" owner:self options:nil];
        
        maincell=filtercell;
    }
        
        
        
        FilterObject *cellobj=[filterarray objectAtIndex:indexPath.row];
        maincell.filternamelbl.text=cellobj.cellnamestr;
        maincell.namelbl.text=cellobj.cellvaluestr;
        
        maincell.filternamelbl.font = [UIFont fontWithName:@"Muli" size:15];
        maincell.namelbl.font = [UIFont fontWithName:@"Muli" size:15];
        maincell.OtherTF.font = [UIFont fontWithName:@"Muli" size:15];
        maincell.OtherTF.tag=indexPath.row;
       
        if (cellobj.isother)
        {
            maincell.OtherTF.text=cellobj.otherstr;
        }
        
        NSLog(@"check ht eindex path  %d",indexPath.row);
        // Assigning the tags dropdownbutton in filterlistview
             if (indexPath.row==0)
        {
            maincell.dropdownbtn.tag=333;
            maincell.OtherTF.placeholder=@"Enter Your Category";
        }
        else  if (indexPath.row==1)
        {
            maincell.dropdownbtn.tag=111;
             maincell.OtherTF.placeholder=@"Enter  Size";
            
        }
        else  if (indexPath.row==2)
        {
            maincell.dropdownbtn.tag=777;
        }
        else  if (indexPath.row==3)
        {
            maincell.dropdownbtn.tag=222;
        }
        else  if (indexPath.row==4)
        {
            maincell.dropdownbtn.tag=444;
            maincell.OtherTF.placeholder=@"Enter Brand";

        }
        else  if (indexPath.row==5)
        {
            maincell.dropdownbtn.tag=888;
             maincell.OtherTF.placeholder=@"Enter Distance";
        }
        maincell.selectionStyle=UITableViewCellSelectionStyleNone;
                return maincell;
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
        if (tableView.tag==333)
    {
        return 30;
    }
    else
    {
        return 0;
    }
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if (tableView.tag==333)
    {
        UIView *sectionview=[[UIView alloc] initWithFrame:CGRectMake(0,0,320,30)];
        sectionview.backgroundColor=[UIColor whiteColor];
        
        UILabel *lbl=[[UILabel alloc] initWithFrame:CGRectMake(5,0,320,25)];
        
        [sectionview addSubview:lbl];
        lbl.font = [UIFont fontWithName:@"Muli" size:17];
        
        LookupModelobject *lookobj=[myappDelegate.catogoryGlobalarray objectAtIndex:section];
        lbl.text=lookobj.loolupValue;
        lbl.textColor=[UIColor colorWithRed:(12/255.f) green:(180/255.f) blue:(174/255.f) alpha:1.0];
        return sectionview;
    }
    return nil;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
  
    
    [currentTextField resignFirstResponder];

if(tableView==dropdownview)
{
    //category dropdownview
    if (tableView.tag==333)
    {
        
       
        LookupModelobject *lookobj=[myappDelegate.catogoryGlobalarray objectAtIndex:indexPath.section];
        
        LookupModelobject *obj=[lookobj.categoryDataArray objectAtIndex:indexPath.row];
        
        NSLog(@"check theval ==%@==",obj.loolupValue);
       
         NSLog(@"loolupid ==%@==",obj.loolupid);
        
    iscategory=NO;
        
        FilterObject *modobj=[filterarray objectAtIndex:0];
        
        if ([obj.loolupValue isEqualToString:@"Other"])
        {
            iscategory=YES;
            modobj.isother=YES;
            modobj.cellvaluestr=obj.loolupValue;
            categorystr=lookobj.loolupid;
            
        }
        else
        {
            modobj.isother=NO;
            modobj.cellvaluestr=obj.loolupValue;
            modobj.otherstr=@"";
            categorystr=obj.loolupid;
        }
        
        ProfileRepository *profilerepo=[[ProfileRepository alloc]init];
        profilerepo.delegate=self;
        parentid=[NSString stringWithFormat:@"%@",obj.parentcatid];//azhar
        NSLog(@"parentid.....%@",obj.parentcatid);
        
        if([parentid isEqualToString:@"17"]||[parentid isEqualToString:@"23"]||[parentid isEqualToString:@"59"])
        {
            LookupModelobject *lookobj=[myappDelegate.sizeGlobalarray objectAtIndex: 0];
             FilterObject *modobj=[filterarray objectAtIndex:1];
            lookobj.loolupValue=@"Not Applicable";
            lookobj.loolupid=@"";
            
            modobj.cellvaluestr=lookobj.loolupValue;
            modobj.isother=NO;
            
        }
        else
        {
            LookupModelobject *lookobj=[myappDelegate.sizeGlobalarray objectAtIndex: 0];
             FilterObject *modobj=[filterarray objectAtIndex:1];
            modobj.isother=NO;
            lookobj.loolupValue=@"";
            lookobj.loolupid=@"";
            
            modobj.cellvaluestr=lookobj.loolupValue;
            

        }////////////////
        
        
        
        
        /////////////////
        [profilerepo getsizelist:obj.parentcatid];
    }
        
    // size dropdownview
    else if (tableView.tag==111)
    {
        LookupModelobject *lookobj=[APPDELEGATE.sizeGlobalarray objectAtIndex: indexPath.row];
        
        
        
        NSLog(@"the id value is %@", lookobj.loolupid);
        issize=NO;
        
        FilterObject *modobj=[filterarray objectAtIndex:1];
        
        if ([lookobj.loolupValue isEqualToString:@"Other"])
        {
            iscategory=YES;
            modobj.isother=YES;
            modobj.cellvaluestr=lookobj.loolupValue;
            sizestr=@"";
        }
        else
        {
            
           /* NSLog(@"parentidnew.....%@",parentid);
            //azhar
            if([parentid isEqualToString:@"17"]||[parentid isEqualToString:@"23"]||[parentid isEqualToString:@"59"])
            {
                lookobj.loolupValue=@"Not Applicable";
                modobj.cellvaluestr=lookobj.loolupValue;
                
            }
            else
            {*/
            modobj.isother=NO;
            modobj.cellvaluestr=lookobj.loolupValue;
            sizestr=0;
            sizestr=lookobj.loolupid;
            NSLog(@"the sizestr is%@......",sizestr);
            modobj.otherstr=@"";
            //}
        }
      

     
    }
    else if (tableView.tag==888)
    {
        LookupModelobject *lookobj=[myappDelegate.distanceGlobalarray objectAtIndex: indexPath.row];
        
        
        NSLog(@"the id value is %@", lookobj.loolupid);
        issize=NO;
        
        FilterObject *modobj=[filterarray objectAtIndex:5];
        
        if ([lookobj.loolupValue isEqualToString:@"Other"])
        {
            iscategory=YES;
            modobj.isother=YES;
            modobj.cellvaluestr=lookobj.loolupValue;
            //sizestr=@"";
        }
        else
        {
            modobj.isother=NO;
            modobj.cellvaluestr=lookobj.loolupValue;
            //sizestr=lookobj.loolupid;
            modobj.otherstr=@"";
        }
        
        distancestr=lookobj.loolupValue;
    }
    // price dropdown
    else if (tableView.tag==222)
    {
        FilterObject *modobj=[filterarray objectAtIndex:3];

        pricestr=0;
       pricestr= [pricearray objectAtIndex:indexPath.row];

        modobj.cellvaluestr=pricestr;
      //  priceLabel.text=lookobj.loolupValue;
       // iscondition=NO;
        //priceLabel.text=lookobj.loolupValue;
    }
    //brand dropdownview
    else if (tableView.tag==444)
    {
       // LookupModelobject *lookobj=[myappDelegate.brandGlobalarray objectAtIndex: indexPath.row];
        
        
       // NSMutableDictionary *mydict= [myappDelegate.brandGlobalarray objectAtIndex:indexPath.section];
      //  NSMutableArray *somearray=[mydict objectForKey:[mydict.allKeys objectAtIndex:0]] ;
         LookupModelobject *lookobj=[APPDELEGATE.brandGlobalarray objectAtIndex: indexPath.row];
        
       // LookupModelobject *lookobj=[somearray objectAtIndex: indexPath.row];
        
       
        NSLog(@"the id value is %@", lookobj.loolupid);
      
        
        isbrand=NO;
        FilterObject *modobj=[filterarray objectAtIndex:4];
        
        if ([lookobj.loolupValue isEqualToString:@"Other"])
        {
            iscategory=YES;
            modobj.isother=YES;
            modobj.cellvaluestr=lookobj.loolupValue;
            brandstr= @"";
        }
        else
        {
            modobj.isother=NO;
            modobj.cellvaluestr=lookobj.loolupValue;
            brandstr= lookobj.loolupid;
            modobj.otherstr=@"";
        }
        NSLog(@"the selected obj is %@",brandstr);
    }
    //gender dropdownview
//    else if (tableView.tag==777)
//    {
//       LookupModelobject *lookobj=[myappDelegate.genderglobalaray objectAtIndex: indexPath.row];
//        
//        //  quantityTF.text=[genderarray objectAtIndex:indexPath.row];
//        
//        genderstr=lookobj.loolupid;
//        FilterObject *modobj=[filterarray objectAtIndex:2];
//      
//        if ([lookobj.loolupValue isEqualToString:@"Other"])
//        {
//            iscategory=YES;
//            modobj.isother=YES;
//            modobj.cellvaluestr=lookobj.loolupValue;
//            
//        }
//        else{
//            modobj.isother=NO;
//            modobj.cellvaluestr=lookobj.loolupValue;
//        }
//    }
    // distance dropdown
    else if (tableView.tag==888)
    {
        LookupModelobject *lookobj=[myappDelegate.distanceGlobalarray objectAtIndex: indexPath.row];
        
        
        NSLog(@"the id value is %@", lookobj.loolupid);
        issize=NO;
        
        FilterObject *modobj=[filterarray objectAtIndex:1];
        
        if ([lookobj.loolupValue isEqualToString:@"Other"])
        {
            iscategory=YES;
            modobj.isother=YES;
            modobj.cellvaluestr=lookobj.loolupValue;
            sizestr=@"";
        }
        else
        {
            modobj.isother=NO;
            modobj.cellvaluestr=lookobj.loolupValue;
            sizestr=lookobj.loolupid;
            modobj.otherstr=@"";
        }
        
    }
    
    dropdownview.contentOffset=CGPointMake(0, 0);
        [self popviewmethod:NO];
    
    if(filterlistview.frame.size.height>=282)
    {
        filterlistview.scrollEnabled=YES;
    }
    else
    {
        filterlistview.scrollEnabled=NO;
    }
    [filterlistview reloadData];
    
    CGRect tablefrm=filterlistview.frame;
    if (myappDelegate.isiphone5)
    {
         tablefrm.size.height=filterlistview.contentSize.height;
    }
    else{
        if (filterlistview.contentSize.height>350)
        {
            tablefrm.size.height=350;
        }
        else
        {
tablefrm.size.height=filterlistview.contentSize.height;
        }
    }
    filterlistview.frame=tablefrm;
    
    
   // dropdownview.frame=CGRectMake(127, 298, 184, 0);
}
else if(tableView==genderdropdownview)
{
    
        LookupModelobject *lookobj=[myappDelegate.genderglobalaray objectAtIndex: indexPath.row];
        
        //  quantityTF.text=[genderarray objectAtIndex:indexPath.row];
        
        genderstr=lookobj.loolupid;
        FilterObject *modobj=[filterarray objectAtIndex:2];
        
        if ([lookobj.loolupValue isEqualToString:@"Other"])
        {
            iscategory=YES;
            modobj.isother=YES;
            modobj.cellvaluestr=lookobj.loolupValue;
            
        }
        else{
            modobj.isother=NO;
            modobj.cellvaluestr=lookobj.loolupValue;
        }
    genderdropdownview.contentOffset=CGPointMake(0, 0);
    [self genderpopviewmethod:NO];
    if(filterlistview.frame.size.height>=282)
    {
        filterlistview.scrollEnabled=YES;
    }
    else
    {
        filterlistview.scrollEnabled=NO;
    }
    [filterlistview reloadData];
    
    CGRect tablefrm=filterlistview.frame;
    if (myappDelegate.isiphone5)
    {
        tablefrm.size.height=filterlistview.contentSize.height;
    }
    else{
        if (filterlistview.contentSize.height>350)
        {
            tablefrm.size.height=350;
        }
        else
        {
            tablefrm.size.height=filterlistview.contentSize.height;
        }
    }
    filterlistview.frame=tablefrm;
    

    
}
}

#pragma mark Textfielddelegatemethods

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    currentTextField=textField;
    [UIView animateWithDuration:0.1 animations:^{
        CGRect addempfrm=self.view.frame;
        addempfrm.origin.y=-70;
        self.view.frame=addempfrm;

    }];
   }

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [UIView animateWithDuration:0.1 animations:^{
    
    CGRect frame = self.view.frame;
    frame.origin.y=0;
    self.view.frame=frame;
        }];
    [textField resignFirstResponder];
    return YES;
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    
    FilterObject *cellobj=[filterarray objectAtIndex:textField.tag];
    
    cellobj.otherstr=textField.text;
    
    
    
}



#pragma mark Searchbardelegate methods

-(void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    [searchbar setShowsCancelButton:NO animated:YES];
    searchbar.text=@"";
    [searchBar resignFirstResponder];
}

-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [searchbar resignFirstResponder];
    [searchbar setShowsCancelButton:NO animated:YES];
    
    [searchBar resignFirstResponder];
    
    [myappDelegate startspinner:self.view];
    
    isSearch=YES;
    isApply=NO;
    
     NSLog(@"the text is %@---",searchbar.text);
    [self postvalues];
    
   // NSLog(@"the response is %@",responseData);
}
 
-(void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar
{
// This method is used to clear the data in the Textfields.
    parentid=@"";
    for (int i=0; i<filterarray.count; i++)
    {
        FilterObject *obj=[filterarray objectAtIndex:i];
        if (i==0)
        {
            obj.isother=NO;
            obj.cellnamestr=@"Category";
            obj.cellvaluestr=@"";
            obj.otherstr=@"";
        }
        else if (i==1)
        {
            obj.isother=NO;
            obj.cellnamestr=@"Size";
            obj.cellvaluestr=@"";
            obj.otherstr=@"";
        }
        else if (i==2)
        {
            obj.isother=NO;
            obj.cellnamestr=@"Gender";
            obj.cellvaluestr=@"";
            obj.otherstr=@"";
        }
        else if (i==3)
        {
            obj.isother=NO;
            obj.cellnamestr=@"Price";
            obj.cellvaluestr=@"";
            obj.otherstr=@"";
        }
        
        else if (i==4)
        {
            obj.isother=NO;
            obj.cellnamestr=@"Brand";
            obj.cellvaluestr=@"";
            obj.otherstr=@"";
        }
        else if (i==5)
        {
            obj.isother=NO;
            obj.cellnamestr=@"Distance";
            obj.cellvaluestr=@"";
            obj.otherstr=@"";
        }
        [filterlistview reloadData];
    }
   // dropdownview.hidden=YES;
    [searchbar setShowsCancelButton:YES animated:YES];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (scrollView==dropdownview && dropdownview.tag==333)
    {
        CGFloat sectionHeaderHeight = 30;
        if (scrollView.contentOffset.y<=sectionHeaderHeight&&scrollView.contentOffset.y>=0) {
            scrollView.contentInset = UIEdgeInsetsMake(-scrollView.contentOffset.y, 0, 0, 0);
        } else if (scrollView.contentOffset.y>=sectionHeaderHeight)
        {
            scrollView.contentInset = UIEdgeInsetsMake(-sectionHeaderHeight, 0, 0, 0);
        }
        
    }
    else
    {
        CGFloat sectionHeaderHeight = 0;
        if (scrollView.contentOffset.y<=sectionHeaderHeight&&scrollView.contentOffset.y>=0) {
            scrollView.contentInset = UIEdgeInsetsMake(-scrollView.contentOffset.y, 0, 0, 0);
        }
        else if (scrollView.contentOffset.y>=sectionHeaderHeight)
        {
            scrollView.contentInset = UIEdgeInsetsMake(-sectionHeaderHeight, 0, 0, 0);
        }
    }
}
// This method is used to implement the animation of dropdownview.
-(void)popviewmethod :(NSNumber *)isshow
{
    
    CGRect popfrm=popview.frame;
    popfrm.origin.x=0;
    if ([isshow isEqualToNumber:[NSNumber numberWithBool:YES] ])
    {
        // [self.view addSubview:popview];
        popfrm.origin.y=0;
        
        popview.hidden=NO;
        CATransition *transition = [CATransition animation];
        transition.duration = 0.3;
        transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
        transition.type = kCATransitionPush;
        transition.subtype =kCATransitionFromTop;
        transition.delegate = self;
        [popview.layer addAnimation:transition forKey:nil];
    }
    else
    {
        popfrm.origin.y=605;
        popview.hidden=NO;
        CATransition *transition = [CATransition animation];
        transition.duration = 0.3;
        transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
        transition.type = kCATransitionPush;
        transition.subtype =kCATransitionFromBottom;
        transition.delegate = self;
        [popview.layer addAnimation:transition forKey:nil];
    }
    
    popview.frame=popfrm;
    [self.view bringSubviewToFront:popview];
    
    
    
}
-(void)genderpopviewmethod :(NSNumber *)isshow
{
    CGRect popfrm=genderpopview.frame;
    popfrm.origin.x=0;
    if ([isshow isEqualToNumber:[NSNumber numberWithBool:YES] ])
    {
        // [self.view addSubview:popview];
        popfrm.origin.y=0;
        
        genderpopview.hidden=NO;
        CATransition *transition = [CATransition animation];
        transition.duration = 0.3;
        transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
        transition.type = kCATransitionPush;
        transition.subtype =kCATransitionFromTop;
        transition.delegate = self;
        [genderpopview.layer addAnimation:transition forKey:nil];
    }
    else
    {
        popfrm.origin.y=605;
        genderpopview.hidden=NO;
        CATransition *transition = [CATransition animation];
        transition.duration = 0.3;
        transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
        transition.type = kCATransitionPush;
        transition.subtype =kCATransitionFromBottom;
        transition.delegate = self;
        [genderpopview.layer addAnimation:transition forKey:nil];
    }
    
    genderpopview.frame=popfrm;
    [self.view bringSubviewToFront:genderpopview];
    
    
    
}


 // This method is for post thefilter details
-(void)callService
{
    
    if(isSearch==YES)
    {
        
        NSString *urlstr=[NSString stringWithFormat:@"%@Filter?SizeId=&BrandId=&CategoryId=&PriceFrom=&PriceTo=&OtherCategory=&OtherSizeId=&OtherBrandId=&Gender=&Distance=&State=&latitude=&longitude=&Searchtext=%@&format=json",MainUrl,searchbar.text];
        NSString* urlTextEscaped = [urlstr stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSURL *url = [NSURL URLWithString: urlTextEscaped];
        
        NSLog(@"@@@@@@@@URL at get Profile is:%@",url);
        
        request = [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:3600.0];
        
        [request setHTTPMethod:@"GET"];
    }
    
    else if (isApply==YES)
    {
        [myappDelegate startspinner:self.view];
        // NSString *urlstr=[NSString stringWithFormat:@"http://markidplace.testshell.net/api/Filter?SizeId=%@&BrandId=%@&CategoryId=%@&PriceFrom=%@&PriceTo=%@&format=json",sizeLabel.text,brandLabel.text,categoryLabel.text,priceLabel.text];
        
        NSString *pricestartval;
        NSString *priceendval;
        if ([myappDelegate isnullorempty: pricestr])
        {
            pricestartval=@"0";
            priceendval=@"0";
        }
        else
        {
            
            NSString *temstr=[pricestr stringByReplacingOccurrencesOfString:@"$" withString:@""];
            
            NSArray *restarr=[temstr componentsSeparatedByString:@" - "];
            
            if (restarr.count>=2)
            {
                pricestartval=[restarr objectAtIndex:0];
                priceendval=[restarr objectAtIndex:1];
            }
            else{
                pricestartval=@"0";
                priceendval=@"100";
            }
        }
        
        NSString *otehrcatstr;
        
        FilterObject *catobj=[filterarray objectAtIndex:0];
        if (iscategory)
        {
            
            otehrcatstr=catobj.otherstr;
        }
        else{
            otehrcatstr=@"";
        }
        
        
        
        NSString *sizeotherstr;
        
        FilterObject *sizeobj=[filterarray objectAtIndex:1];
        if (sizeobj.isother)
        {
            sizestr=@"0";
            sizeotherstr=sizeobj.otherstr;
        }
        else
        {
            sizeotherstr=@"";
        }
        
        NSString *brandotherstr;
        
        FilterObject *brandobj=[filterarray objectAtIndex:4];
        if (brandobj.isother)
        {
            brandstr=@"0";
            brandotherstr=brandobj.otherstr;
        }
        else if ([brandobj.cellvaluestr isEqualToString:@""])
        {
            brandstr=@"0";
            brandotherstr=@"";
        }
        else
        {
            brandotherstr=@"";
        }
        // NSString *distancestring;
        
        FilterObject *distanceobj=[filterarray objectAtIndex:5];
        NSString *urlstr;
        if ([distancestr isEqualToString:@"Any Distance"])
        {
            distancestr=@"0";
            NSLog(@"any distance is searched");
            urlstr=[NSString stringWithFormat:@"%@Filter?SizeId=%@&BrandId=%@&CategoryId=%@&PriceFrom=%@&PriceTo=%@&OtherCategory=%@&OtherSizeId=%@&OtherBrandId=%@&Gender=%@&Distance=%d&State=&latitude=&longitude=&Searchtext=&format=json",MainUrl, [NSNumber numberWithInt:[sizestr intValue]],[NSNumber numberWithInt:[brandstr intValue]],[NSNumber numberWithInt:[categorystr intValue]],pricestartval,priceendval,otehrcatstr,sizeotherstr,brandotherstr,genderstr,[distancestr intValue]];
            
            NSLog(@"urlstr.....%@",urlstr);
        }
        else if ([distancestr isEqualToString:@""])
        {
            NSLog(@"any distance is searched");
            NSString*str1=@"";
            NSString*str2=@"";
            NSString*str3=@"";
            urlstr=[NSString stringWithFormat:@"%@Filter?SizeId=%@&BrandId=%@&CategoryId=%@&PriceFrom=%@&PriceTo=%@&OtherCategory=%@&OtherSizeId=%@&OtherBrandId=%@&Gender=%@&Distance=%@&State=%@&latitude=%@&longitude=%@&Searchtext=&format=json",MainUrl, [NSNumber numberWithInt:[sizestr intValue]],[NSNumber numberWithInt:[brandstr intValue]],[NSNumber numberWithInt:[categorystr intValue]],pricestartval,priceendval,otehrcatstr,sizeotherstr,brandotherstr,genderstr,distancestr,str1,str2,str3];
            NSLog(@"urlstr.....%@",urlstr);
        }
        
        else
        {
            NSLog(@"distances are searched is searched");
            
            urlstr=[NSString stringWithFormat:@"%@Filter?SizeId=%@&BrandId=%@&CategoryId=%@&PriceFrom=%@&PriceTo=%@&OtherCategory=%@&OtherSizeId=%@&OtherBrandId=%@&Gender=%@&Distance=%@&State=%@&latitude=%@&longitude=%@&Searchtext=&format=json",MainUrl, [NSNumber numberWithInt:[sizestr intValue]],[NSNumber numberWithInt:[brandstr intValue]],[NSNumber numberWithInt:[categorystr intValue]],pricestartval,priceendval,otehrcatstr,sizeotherstr,brandotherstr,genderstr,distancestr,statestr,latitudestr,longitudestr];
            NSLog(@"urlstr.....%@",urlstr);
            
        }
        
        
        
        NSString* urlTextEscaped = [urlstr stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSURL *url = [NSURL URLWithString: urlTextEscaped];
        
        NSLog(@"@@@@@@@@URL at get Profile is:%@",url);
        
        request = [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:3600.0];
        
        [request setHTTPMethod:@"GET"];
        
    }
    
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    
    [NSURLConnection sendAsynchronousRequest:request queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         
         if (error)
         {
             
             NSLog(@"error is %@",error);
             //  [appdelegate stopspinner:self.view];
             
             [self dismissactivindic:NO];
         }
         else
         {
             NSError *jsonParsingError = nil;
             
             id respdict = [NSJSONSerialization JSONObjectWithData:data options:0 error:&jsonParsingError];
             
             NSLog(@"the main dict is %@",respdict);
             
             
             
             if (jsonParsingError)
             {
                 NSLog(@"--------------JSON ERROR: %@", [jsonParsingError localizedDescription])
                 ;
                 [self dismissactivindic:YES];
                 // [delegate errorMessage:@"message"];
             }
             else
             {
                 //[delegate successResponseofinterests:mainDict];
                 [self performSelectorOnMainThread:@selector(parsedata:) withObject:respdict waitUntilDone:YES];
                 
                 
             }
             
             // [appdelegate stopspinner:self.view];
         }
         
         [self dismissactivindic:NO];
         
         
     }];
}
-(void)postvalues
{
    if ([latitudestr isEqualToString:@"" ] && [longitudestr isEqualToString:@""] && ![distancestr isEqualToString:@"Any Distance"])
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Location Warning" message:@"Your location is importent to us, to find the nearest products, Please Turn On your Location Services, Goto Settings -> Privacy -> Location Services -> marKIDPlace -> Select  While Using the App " delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
    }
    else
    {
        [self callService];
    }
}

#pragma mark Success Response methods
// This is the success response for filter.
-(void)parsedata :(id)respdict
{
    if ([respdict isKindOfClass:[NSDictionary class]])
    {
         [myappDelegate stopspinner:self.view];
        [self dismissactivindic:YES];
    }
    else if ([respdict isKindOfClass:[NSArray class]])
    {
        NSMutableArray *itemsarry=[[NSMutableArray alloc]init];
        
        [itemsarry addObjectsFromArray:[myappDelegate parselistingitem:respdict :YES :myappDelegate.allLikesGlobalarray :NO]];
        
        NSLog(@"check the data of array %lu",(unsigned long)itemsarry.count);
        if (itemsarry.count<1)
        {
             [myappDelegate stopspinner:self.view];
            [self dismissactivindic:YES];
        }
        else
        {
            HomeViewController *homeviewcont;
            if (myappDelegate.isiphone5)
            {
                homeviewcont = [[HomeViewController alloc]initWithNibName:@"HomeViewController" bundle:nil];
            }
            else
            {
                homeviewcont = [[HomeViewController alloc]initWithNibName:@"HomeViewController_iphone4" bundle:nil];
            }
            
            homeviewcont.itemsarray=[NSMutableArray new];
            
            homeviewcont.itemsarray=itemsarry;
            
            [self.navigationController pushViewController:homeviewcont animated:YES];
        }
         [myappDelegate stopspinner:self.view];
        [self dismissactivindic:NO];
    }
    else
    {
        
         [myappDelegate stopspinner:self.view];
        [self dismissactivindic:YES];
    }
}

// This method is used to implement the parsing for getting the data in the dropdownview.
-(void)successresponceListing :(id)respdict :(NSString *)paramname
{
    NSLog(@"the response dict is %@",respdict);
    if([paramname isEqualToString:@"getcatogery"])
    {
        [myappDelegate.catogoryGlobalarray removeAllObjects];
        
       // [myappDelegate parsedataforCategory:respdict];
        
        [myappDelegate stopspinner:self.view];
    }
    /*
    else if([paramname isEqualToString:@"getsize"])
    {
        //  NSLog(@"check the size daa==== %@",respdict);
        
        
        [myappDelegate.sizeGlobalarray removeAllObjects];
        //  NSLog(@"check the catogory %@",respdict);
        if ([respdict isKindOfClass:[NSArray class]]) {
            NSArray *myar=(NSArray *)respdict;
            for (int i=0; i<myar.count; i++)
            {
                NSDictionary *mydict=[myar objectAtIndex:i];
                
                LookupModelobject *lookobj=[[LookupModelobject alloc]init];
                
                lookobj.loolupName=@"lookupName";
                lookobj.loolupid=[mydict valueForKey:@"lookupID"];
                lookobj.loolupValue=[mydict valueForKey:@"lookupValue"];
                lookobj.loolupNumber=[mydict valueForKey:@"sequenceNumber"];
                lookobj.loolupStatus=[mydict valueForKey:@"status"];
                
                
                [myappDelegate.sizeGlobalarray addObject:lookobj];
            }
            
        }
         
        // get conditions
        myappDelegate.sizeGlobalarray=[myappDelegate sortmyarray:@"loolupValue" :myappDelegate.sizeGlobalarray];
        
        [ registrationRep getdata:nil :@"getcondition" :@"GET" ];
    }*/
   
    else if([paramname isEqualToString:@"getcondition"])
    {
        [myappDelegate.conditionGlobalarray removeAllObjects];
        if ([respdict isKindOfClass:[NSArray class]]) {
            NSArray *myar=(NSArray *)respdict;
            for (int i=0; i<myar.count; i++)
            {
                NSDictionary *mydict=[myar objectAtIndex:i];
                
                LookupModelobject *lookobj=[[LookupModelobject alloc]init];
                                lookobj.loolupName=@"lookupName";
                lookobj.loolupid=[mydict valueForKey:@"lookupID"];
                lookobj.loolupValue=[mydict valueForKey:@"lookupValue"];
                lookobj.loolupNumber=[mydict valueForKey:@"sequenceNumber"];
                lookobj.loolupStatus=[mydict valueForKey:@"status"];
                [myappDelegate.conditionGlobalarray addObject:lookobj];
            }
        }
        myappDelegate.conditionGlobalarray=[myappDelegate sortmyarray:@"loolupValue" :myappDelegate.conditionGlobalarray];
        /// get brand
        [ registrationRep getdata:nil :@"getbrand" :@"GET" withcount:@"0"];
    }
    else if([paramname isEqualToString:@"getbrand"])
    {
        //    NSLog(@"check the check the brand data %@",respdict);
        [myappDelegate.brandGlobalarray removeAllObjects];
        //   NSLog(@"check the catogory %@",respdict);
        if ([respdict isKindOfClass:[NSArray class]]) {
            NSArray *myar=(NSArray *)respdict;
            for (int i=0; i<myar.count; i++)
            {
                NSDictionary *mydict=[myar objectAtIndex:i];
                
                LookupModelobject *lookobj=[[LookupModelobject alloc]init];
                
                lookobj.loolupName=@"lookupName";
                lookobj.loolupid=[mydict valueForKey:@"lookupID"];
                lookobj.loolupValue=[mydict valueForKey:@"lookupValue"];
                lookobj.loolupNumber=[mydict valueForKey:@"sequenceNumber"];
                lookobj.loolupStatus=[mydict valueForKey:@"status"];
                
                [myappDelegate.brandGlobalarray addObject:lookobj];
            }
           
        }
        myappDelegate.brandGlobalarray=[myappDelegate sortmyarray:@"loolupValue" :myappDelegate.brandGlobalarray];
        LookupModelobject *lookobj33=[[LookupModelobject alloc]init];
        
        lookobj33.loolupName=@"lookupName";
        lookobj33.loolupid=@"130";
        lookobj33.loolupValue=@"Other";
        lookobj33.loolupNumber=@"0";
        lookobj33.loolupStatus=@"0";
        lookobj33.isother=YES;
        [myappDelegate.brandGlobalarray addObject:lookobj33];

        
    }
    /*
    for (int k=0; k<myappDelegate.filterbrandGlobalarray.count; k++)
    {
        LookupModelobject *lookobj=[myappDelegate.filterbrandGlobalarray objectAtIndex:k];
        if([lookobj.loolupValue isEqualToString:@"Other"])
        {
            [myappDelegate.filterbrandGlobalarray removeObjectAtIndex:k];
            break;
        }
        [dropdownview reloadData];
    }
*/
        [myappDelegate stopspinner:self.view];
    
}

-(void)dismissactivindic :(BOOL)showalert
{
    
    [myappDelegate stopspinner:self.view];
    [myappDelegate stopspinner:self.view];
    if (showalert)
    {
        [AppDelegate alertmethod:appname :@"No data to display" ];
    }
}
-(void)internetConnection
{
    UIAlertView *Internetalert=[[UIAlertView alloc]initWithTitle:appname message:@"Internet connection not available" delegate:self cancelButtonTitle:nil otherButtonTitles:@"Ok", nil];
    [Internetalert show];
    [APPDELEGATE stopspinner:self.view];
    
}

-(void)successResponseforsizelist:(NSMutableArray *)responseArray
{
    [APPDELEGATE.sizeGlobalarray removeAllObjects];
    dispatch_async(dispatch_get_main_queue(), ^{
    
    for(int i=0;i<responseArray.count;i++)
    {
        NSMutableDictionary *mydict=[responseArray objectAtIndex:i];
        LookupModelobject *lookobj=[[LookupModelobject alloc]init];
        
        lookobj.loolupName=@"lookupName";
        lookobj.categoryid=[mydict valueForKey:@"categoryId"];
        lookobj.loolupValue=[mydict valueForKey:@"size"];
        lookobj.loolupid=[mydict valueForKey:@"sizeId"];
        
        [APPDELEGATE.sizeGlobalarray addObject:lookobj];
        
    }
        
        isdropdown=YES;
        dropdownview.tag=111;
    [dropdownview reloadData];
         });
    //  [APPDELEGATE.sizeGlobalarray removeAllObjects];
    [ registrationRep getdata:nil :@"getcondition" :@"GET" withcount:@"0"];
}

-(void)successResponseforDistance:(NSMutableArray *)responseArray
{
    NSLog(@"the response array is %@",responseArray);
    //  NSLog(@"check the size daa==== %@",respdict);
        [myappDelegate.distanceGlobalarray removeAllObjects];
        //  NSLog(@"check the catogory %@",respdict);
            for (int i=0; i<responseArray.count; i++)
            {
                NSDictionary *mydict=[responseArray objectAtIndex:i];
                
                LookupModelobject *lookobj=[[LookupModelobject alloc]init];
                
                lookobj.loolupName=@"lookupName";
                lookobj.loolupid=[mydict valueForKey:@"lookupID"];
                lookobj.loolupValue=[mydict valueForKey:@"lookupValue"];
                lookobj.loolupNumber=[mydict valueForKey:@"sequenceNumber"];
                lookobj.loolupStatus=[mydict valueForKey:@"status"];
                
        [myappDelegate.distanceGlobalarray addObject:lookobj];
            }
    [dropdownview reloadData];
            
}

@end
