//
//  InviteFriendViewController.m
//  MarkidPlace
//
//  Created by Varma Bhupatiraju on 17/09/14.
//  Copyright (c) 2014 stellentmac1. All rights reserved.
//

#import "InviteFriendViewController.h"
#import "InviteModelobject.h"
#import "AddFriendsViewController.h"
@interface InviteFriendViewController ()

@end

@implementation InviteFriendViewController
@synthesize myinvitecustomcell,myfirstarray,myimagearray,Basicarray;
@synthesize account,accountStore,acessToken;
@synthesize searchbar,navbar,friendslistview,friendscell,contacts,currentarray,friendsArray,defaultfriendsarray,titlelbl,contactcell,contactslistview,contactview,tempArray,collectionArray,smsbutton,Facebookbutton,emailbutton,sharebutton;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [super viewDidLoad];
    registrationRep = [[RegistrationRepository alloc]init];
    registrationRep.delegate = self;
    self.navbar.frame = CGRectMake(0, 0, 320, 64);
    self.invitelbl.font = [UIFont fontWithName:@"Monroe" size:18];
    
    [self.navbar setBackgroundImage:[UIImage imageNamed:@"navbar.png"] forBarMetrics:UIBarMetricsDefault];
    
   // @"Facebook",@"fb_icon.png",
    myfirstarray=[[NSMutableArray alloc]initWithObjects:@"Twitter",@"Text",@"Email", nil];
    myimagearray=[[NSMutableArray alloc]initWithObjects:@"twitter_icon.png",@"text_icon.png",@"email_icon.png", nil];
    
    Basicarray=[[NSMutableArray alloc]init];
    for (int i=0;i<[myfirstarray count]; i++)
    {
        InviteModelobject *obj=[[InviteModelobject alloc]init];
        obj.namestring=[myfirstarray objectAtIndex:i];
        obj.imgstring=[myimagearray objectAtIndex:i];
        //NSLog(@"obj.namesString !!!! %@",obj.namestring);
        //NSLog(@"imageString !!!! %@", obj.imgstring);
        
        [Basicarray addObject:obj];
        NSLog(@"Basicarray.... %@", [Basicarray description]);
    }
    
    
    // Do any additional setup after loading the view from its nib.
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return  50;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return Basicarray.count;
}
- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellidentifer=@"Cell";
    InviteCustomCell *cell=[friendslistview dequeueReusableCellWithIdentifier:cellidentifer];
    if (cell==Nil)
    {
        [[NSBundle mainBundle] loadNibNamed:@"InviteCustomCell" owner:self options:nil];
        cell = self.myinvitecustomcell;
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
    }
    InviteModelobject*obj1;
    obj1=[Basicarray objectAtIndex:indexPath.row];
    NSLog(@"basicArray objectAtIndex:indexPath.row !!!! %@",obj1);
    NSLog(@"namestringis......%@",obj1.namestring);
    cell.invitelbl.text=obj1.namestring;
    NSLog(@"imagestringis......%@",obj1.imgstring);
    cell.inviteimgview.image=[UIImage imageNamed:obj1.imgstring];
    
    return  cell;
    
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    if (indexPath.row==0)
   /* {
     sepstr=@"Facebook";
     
     
     {
     dispatch_async(dispatch_get_main_queue(), ^{
     
     // [self.spinner startAnimating];
     [myappDelegate startspinner:self.view];
     self.view.userInteractionEnabled = NO;
     
     
     
     NSArray *permissions =
     [NSArray arrayWithObjects:@"email",@"user_photos",@"read_stream", nil];
     NSDictionary * dict=[NSDictionary dictionaryWithObjectsAndKeys:@"770236303030542",ACFacebookAppIdKey,permissions,ACFacebookPermissionsKey,ACFacebookAudienceEveryone,ACFacebookAudienceKey, nil];
     
     accountStore = [[ACAccountStore alloc]init];
     ACAccountType *accountType = [accountStore accountTypeWithAccountTypeIdentifier:ACAccountTypeIdentifierFacebook];
     
     
     
     [accountStore requestAccessToAccountsWithType:accountType options:dict completion:^(BOOL granted, NSError *error)
     {
     if (granted == YES)
     {
     //  [self performSelector:@selector(startSpinner) withObject:nil afterDelay:0.1];
     
     NSLog(@"Grantedddddddddddd");
     NSArray *accountsArray = [accountStore accountsWithAccountType:accountType];
     
     ////NSLog(@"accountsArray =====%@",accountsArray);
     
     if ([accountsArray count] > 0)
     {
     account = [accountsArray objectAtIndex:0];
     
     //  ////NSLog(@"aaaaaaaaaaaa%@",Account);
     
     ACAccountCredential *fbCredential = [account credential];
     
     acessToken = [NSString stringWithFormat:@"%@",fbCredential.oauthToken];
     
     // ////NSLog(@"acessToken   %@",acessToken);
     
     NSDictionary *parameters = @{@"access_token": acessToken};
     
     NSURL *requestURL = [NSURL URLWithString:@"https://graph.facebook.com/me"];
     
     SLRequest *postRequest = [SLRequest requestForServiceType:SLServiceTypeFacebook requestMethod:SLRequestMethodGET URL:requestURL parameters:parameters];
     
     postRequest.account = account;
     
     [postRequest performRequestWithHandler:
     ^(NSData *responseData, NSHTTPURLResponse
     *urlResponse, NSError *error)
     {
     
     ////NSLog(@"response ======%@",urlResponse);
     
     NSDictionary *dataSourceDict= [NSJSONSerialization JSONObjectWithData:responseData options:NSJSONReadingMutableLeaves  error:&error];
     
     ////NSLog(@"dataSourceDict =====%@",dataSourceDict);
     
     if([dataSourceDict objectForKey:@"error"]!=nil)
     {
     [self attemptRenewCredentials];
     }
     else
     {
     
     facebookID = dataSourceDict[@"id"];
     
     [self getFBFriends];
     }
     
     
     }];
     }
     else{
     [myappDelegate stopspinner:self.view];
     }
     }
     else
     {
     
     NSLog(@"error in FB========> %@",error);
     [myappDelegate stopspinner:self.view];
     //[self.spinner stopAnimating];
     self.view.userInteractionEnabled = YES;
     
     UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"No Facebook Accounts" message:@"For some reason, Facebook is not recognizing marKIDplace. To fix, please tap your phone's Settings icon, scroll down to Facebook, where you should see your Facebook login credentials (if you don't, you need to log in) and there you should see a marKIDplace icon also (if you don't, you need to refresh the permissions again). Toggle marKIDplace to the \"ON\" position and you should be connected." delegate:self cancelButtonTitle:@"OK"  otherButtonTitles:nil];
     alert.tag = 11;
     [alert show];
     }
     }];
     });
     
     }
     
     if([sepstr isEqualToString:@"Facebook"])
     {
     NSLog(@"the facebook array count is %d",facebookarray.count);
     facebookobj =[facebookarray objectAtIndex:indexPath.row];
     NSLog(@"the facebook array count is %d",facebookarray.count);
     NSLog(@"the facebook obj count is %@",facebookobj);
     
     
     NSString *friendId = [NSString stringWithFormat:@"%@",facebookobj.uidstr];
     
     
     NSMutableDictionary *params1;
     
     
     params1= [NSMutableDictionary dictionaryWithObjectsAndKeys: @"Hey! Check out marKIDplace", @"name", @"770236303030542", @"app_id", friendId, @"to", acessToken, @"access_token",nil];
     
     [FBWebDialogs presentFeedDialogModallyWithSession:nil parameters:params1 handler:
     ^(FBWebDialogResult result, NSURL *resultURL, NSError *error)
     {
     NSLog(@"result ======%@",resultURL);
     if (error)
     {
     NSLog(@"error ======%@",error);
     }
     else
     {
     if (result == FBWebDialogResultDialogNotCompleted)
     {
     NSLog(@"User canceled story publishing.");
     }
     else
     {
     NSDictionary *urlParams = [self parseURLParams:[resultURL query]];
     if (![urlParams valueForKey:@"post_id"])
     {
     NSLog(@"User canceled story publishing.");
     }
     else
     {
     double delayInSeconds = 0.5;
     dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
     dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
     
     
     UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:appname message:[NSString stringWithFormat:@"Successfully invited"] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
     
     [alertView show];
     
     
     });
     
     
     }
     }
     }
     }];
     
     // });
     
     }
     
     [self sendRequest];
     
     //        InviteFriendsViewController *InviteVC;
     //        if (myappDelegate.isiphone5)
     //        {
     //            InviteVC = [[InviteFriendsViewController alloc]initWithNibName:@"InviteFriendsViewController" bundle:nil];
     //        }
     //        else
     //        {
     //            InviteVC = [[InviteFriendsViewController alloc]initWithNibName:@"InviteFriendsViewController_4s" bundle:nil];
     //        }
     //
     //        InviteVC.comparestr=@"Facebook";
     //        [self.navigationController pushViewController:InviteVC animated:YES];
     //
        }*/
    // else if (indexPath.row==1)
    {
        
        AddFriendsViewController *  addfriendVC = [[AddFriendsViewController alloc] initWithNibName:@"AddFriendsViewController" bundle:nil];
        addfriendVC.typeString=@"invite";
        [self.navigationController pushViewController:addfriendVC animated:YES];
        
    }
    else if (indexPath.row==1)
    {
        MFMessageComposeViewController *controller = [[MFMessageComposeViewController alloc] init];
        if([MFMessageComposeViewController canSendText])
        {
            controller.body = kinvitaion;
            //controller.recipients = [NSArray arrayWithObjects:@"", nil];
            controller.messageComposeDelegate = self;
            [self presentModalViewController:controller animated:YES];
        }
    }
    else if (indexPath.row==2)
    {
        MFMailComposeViewController *mailComposer = [[MFMailComposeViewController alloc] init];
        
        if(mailComposer!= nil)
        {
            mailComposer.mailComposeDelegate = self;
            [mailComposer setToRecipients:finalinvitearray];
            [mailComposer setSubject:appname];
            
            mailComposer.navigationController.navigationBar.titleTextAttributes = [NSDictionary dictionaryWithObject:[UIColor blackColor] forKey:UITextAttributeTextColor];
            [mailComposer setMessageBody:kinvitaion isHTML:YES];
            mailComposer.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
             mailComposer.navigationController.navigationBar.titleTextAttributes = [NSDictionary dictionaryWithObject:[UIColor blackColor] forKey:UITextAttributeTextColor];
            // mailComposer.navigationBar.tintColor=[UIColor clearColor];
            [self presentViewController:mailComposer animated:YES completion:nil];
        }
    }
    
}
- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result{
    // NSString *num = [[NSUserDefaultsstandardUserDefaults] stringForKey:@"SBFormattedPhoneNumber"];
    // NSLog(@"Some result received: %u, %@", result, num);
    
    [controller dismissViewControllerAnimated:YES completion:nil];
}
-(IBAction)backAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
    
}

-(IBAction)homeBtnAction:(id)sender;
{
    
    NSArray *arr=self.navigationController.viewControllers;
    for (UIViewController *childvc in arr)
    {
        
        if([childvc isKindOfClass:[SuggestionsViewController class]])
        {
            SuggestionsViewController *subvc=(SuggestionsViewController *)childvc;
            subvc.ismyfeed=NO;
            [self.navigationController popToViewController:subvc animated:YES];
        }
    }
    
}
-(void)getFBFriends
{
    @try
    {
        
        NSString *urlStr =[NSString stringWithFormat:@"https://graph.facebook.com/%@/fql?q=SELECT uid, name FROM user WHERE uid IN (SELECT uid2 FROM friend WHERE uid1 = me()) ORDER BY name&access_token=%@&limit=10000&offset=0",facebookID,acessToken];
        
        
        urlStr = [urlStr stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        
        
        NSURL *url = [NSURL URLWithString:urlStr];
        
        NSLog(@"postbloopdata url====%@",url);
        
        formRequest = [ASIFormDataRequest requestWithURL:url];
        
        [formRequest setDelegate:self];
        [formRequest setTimeOutSeconds:5000];
        [formRequest startAsynchronous];
    }
    @catch (NSException *exception)
    {
        @throw exception;
    }
}

- (void)requestFailed:(ASIHTTPRequest *)request
{
    [request cancel];
    
    request=nil;
    // [delegate failedtoConnect];
}
-(void)requestFinished:(ASIHTTPRequest *)request
{
    NSError *error1 = [request error];
    if (!error1)
    {
        NSString *response = [request responseString];
        
        // NSLog(@"response =====%@",response);
        
        NSDictionary *mainDict = [response JSONValue];
        
        // NSLog(@"*** mainDict *** %@",mainDict);
        
        [self getResponse:mainDict];
        
    }
}
-(void)getResponse:(NSDictionary *)mainDict
{
    NSArray *mainArray = [[NSArray alloc] init];
    
    mainArray = mainDict[@"data"];
    
    NSLog(@"main array ====%@ count ===%lu",mainArray,(unsigned long)mainArray.count);
    
    NSMutableDictionary *subDict =[[NSMutableDictionary alloc] init];
    
    for (int i = 0; i<mainArray.count; i++)
    {
        facebookobj=[[Facebookobject alloc]init];
        subDict = [mainArray objectAtIndex:i];
        NSLog(@"the subDict contacs are %@",subDict);
        
        NSString *nameStr = subDict[@"name"];
        NSLog(@"the nameStr contacs are %@",nameStr);
        
        facebookobj.namestr=nameStr;
        //NSLog(@"the namestr contacs are %@",facebookobj.namestr);
        
        NSString *idStr = subDict[@"uid"];
        facebookobj.uidstr=idStr;
        NSLog(@"the idStr contacs are %@",facebookobj.uidstr);
        
        [facebookarray addObject:facebookobj];
        
    }
    
    [friendslistview reloadData];
    NSLog(@"the facebook array  count is %lu",(unsigned long)facebookarray.count);
    
    // [self.imagesArray addObject:idStr];
    
    /*
     [self.namesArray addObject:nameStr];
     
     NSString *idStr = subDict[@"uid"];
     [self.imagesArray addObject:idStr];
     }
     
     [self.fbnamesArray addObjectsFromArray:self.namesArray];
     [self.fbimagesArray addObjectsFromArray:self.imagesArray];
     
     [self.searchresultTableView reloadData];
     */
    [myappDelegate stopspinner:self.view];
    
}
- (void)sendRequest {
    // Display the requests dialog
    [FBWebDialogs
     presentRequestsDialogModallyWithSession:nil
     message:[NSString stringWithFormat:@"Learn how to make your iOS apps social."]
     title:@"Testing"
     parameters:nil
     handler:^(FBWebDialogResult result, NSURL *resultURL, NSError *error) {
         if (error) {
             // Error launching the dialog or sending the request.
             NSLog(@"Error sending request.");
         } else {
             if (result == FBWebDialogResultDialogNotCompleted) {
                 // User clicked the "x" icon
                 NSLog(@"User canceled request.");
             } else {
                 // Handle the send request callback
                 NSDictionary *urlParams = [self parseURLParams:[resultURL query]];
                 if (![urlParams valueForKey:@"request"]) {
                     // User clicked the Cancel button
                     NSLog(@"User canceled request.");
                 } else {
                     // User clicked the Send button
                     NSString *requestID = [urlParams valueForKey:@"request"];
                     NSLog(@"Request ID: %@", requestID);
                 }
             }
         }
     }];
}
-(void)attemptRenewCredentials
{
    [self.accountStore renewCredentialsForAccount:(ACAccount *)account completion:^(ACAccountCredentialRenewResult renewResult, NSError *error){
        if(!error)
        {
            switch (renewResult) {
                case ACAccountCredentialRenewResultRenewed:
                    ////NSLog(@"Good to go");
                    [self Facebookbutton:self];
                    break;
                case ACAccountCredentialRenewResultRejected:
                    ////NSLog(@"User declined permission");
                    [self attemptRenewCredentials];
                    break;
                case ACAccountCredentialRenewResultFailed:
                    ////NSLog(@"non-user-initiated cancel, you may attempt to retry");
                    [self attemptRenewCredentials];
                    break;
                default:
                    break;
            }
            
        }
        else{
            //handle error
            ////NSLog(@"error from renew credentials%@",error);
            [self Facebookbutton:self];
            // [self attemptRenewCredentials];
        }
    }];
}

- (NSDictionary*)parseURLParams:(NSString *)query {
    NSArray *pairs = [query componentsSeparatedByString:@"&"];
    NSMutableDictionary *params2 = [[NSMutableDictionary alloc] init];
    for (NSString *pair in pairs) {
        NSArray *kv = [pair componentsSeparatedByString:@"="];
        NSString *val =
        [kv[1] stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        params2[kv[0]] = val;
    }
    return params2;
}
-(void)alertViewMethod:(NSString *)messageStr
{
    [myappDelegate stopspinner:self.view];
    // [spinner stopAnimating];
    //   [self performSelector:@selector(hideIndicator) withObject:nil afterDelay:0.1];
    
    //[self performSelector:@selector(stopactivity) withObject:nil afterDelay:1];
    
    UIAlertView *alertView=[[UIAlertView alloc]initWithTitle:appname message:messageStr delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alertView show];
}
-(void)twitterdetails:(NSData *)responseData
{
    
    //[spinner startAnimating];
    //spinner.hidden=NO;
    // self.view.userInteractionEnabled=NO;
    
    NSError* error = nil;
    NSDictionary* json = [NSJSONSerialization
                          JSONObjectWithData:responseData //1
                          options:NSJSONReadingAllowFragments
                          error:&error];
    
    
    NSLog(@"Twitter JSon is %@",json);
    NSString *name = [json objectForKey:@"name"];
    NSLog(@"Name is %@",name);
    NSString *scrnm = [json objectForKey:@"screen_name"];
    NSLog(@"screen_name is %@",scrnm);
    NSString *idStrin=[json objectForKey:@"id"];
    NSLog(@"idString is %@",idStrin);
    NSString *profileimage=[json objectForKey:@"profile_image_url"];
    NSLog(@"profileimage is %@",profileimage);
    twitterDict = [[NSMutableDictionary alloc]init];
    
    [twitterDict setValue:scrnm forKey:@"Username"];
    [twitterDict setValue:name forKey:@"Name"];
    
    [twitterDict setValue:@"" forKey:@"Email"];
    
    [twitterDict setValue:profileimage forKey:@"Profilepic"];
    [twitterDict setValue:idStrin forKey:@"SocialId"];
    
    [twitterDict setValue:@"Twitter" forKey:@"Type"];
    
    NSMutableDictionary *devicedict = [[NSMutableDictionary alloc]init];
    
    BOOL  isDeviceIphone=([[UIDevice currentDevice] userInterfaceIdiom]== UIUserInterfaceIdiomPhone);
    
    if (isDeviceIphone)
    {
        [devicedict setValue:@"iphone" forKey:@"DeviceType"];
    }
    else
    {
        [devicedict setValue:@"ipad" forKey:@"DeviceType"];
    }
    
    if (myappDelegate.deviceTokenString.length > 0)
    {
        [devicedict setValue:myappDelegate.deviceTokenString forKey:@"DeviceToken"];
    }
    else
    {
        myappDelegate.deviceTokenString=@"";
        //appdelegate.deviceTokenString=@"ba8d2205ca21d3aab0c170c485494747e470220f1aa42a2fae5a2e9178678469";
        
        [devicedict setValue:myappDelegate.deviceTokenString forKey:@"DeviceToken"];
    }
    
    float version = [[[UIDevice currentDevice] systemVersion] floatValue];
    NSLog(@"*** %f ***",version);
    
    [devicedict setValue:[NSString stringWithFormat:@"%f",version] forKey:@"DeviceOS"];
    
    [devicedict setValue:@"1" forKey:@"DeviceID"];
    [devicedict setValue:@"nothing" forKey:@"DeviceKeychain"];
    
    [twitterDict setValue:devicedict forKey:@"Device"];
    NSLog(@"twitterDict....%@",twitterDict);
    [registrationRep postAllDetails:twitterDict :@"Login" :@"POST"];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark  Mail composer delegate method.

-(void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    if ( result == MFMailComposeResultFailed )
    {
        // Sending failed - display an error message to the user.
        /*     NSString* message = [NSString stringWithFormat:@"Error sending email '%@'. Please try again, or cancel the operation./Users/varmabhupatiraju/Documents/Ajay/TabBarproject/TabBarproject/ThirdViewController.m", [error localizedDescription]];
         UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@"Error Sending Email" message:message delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
         [alertView show];*/
        
    }else
        [self dismissViewControllerAnimated:YES completion:^{     }];
    
}
-(void)internetConnection
{
    UIAlertView *Internetalert=[[UIAlertView alloc]initWithTitle:appname message:@"Internet connection not available" delegate:self cancelButtonTitle:nil otherButtonTitles:@"Ok", nil];
    [Internetalert show];
    [APPDELEGATE stopspinner:self.view];
    
}
@end
