//
//  SuggestionlistViewController.h
//  MarkidPlace
//
//  Created by stellent_mac_05 on 26/09/14.
//  Copyright (c) 2014 stellentmac1. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FindFriendsTableViewCell.h"
#import "HelperProtocol.h"
#import "AppDelegate.h"
#import "SuggestionObject.h"
#import "ProfileRepository.h"
@class AppDelegate;
@interface SuggestionlistViewController : UIViewController<HelperProtocol>
{
        ProfileRepository *profilerepo;
    NSString*alertmsg;
    
}
@property (strong, nonatomic) IBOutlet UINavigationBar *navbar;
@property (strong, nonatomic) IBOutlet UILabel *titlelbl;
@property (strong, nonatomic)  NSMutableArray *firstsectionarray;
@property (strong, nonatomic)  NSMutableArray *
tablearray;
@property (strong, nonatomic)  NSMutableArray *
twitterArray;
@property (strong, nonatomic)  NSMutableArray *
duplicatearray;
@property (strong, nonatomic)  NSMutableArray *Basicarray;
@property (strong, nonatomic)  NSMutableArray *secondsectionarray;
@property (strong, nonatomic) IBOutlet UITableView *suggestionslistview;
@property (strong, nonatomic) IBOutlet FindFriendsTableViewCell *findfriendcell;
@property (strong, nonatomic)  NSMutableArray *nameimagearray;
@property (strong, nonatomic) IBOutlet UILabel *suggestiontbllbl;
@property (strong, nonatomic)  NSString*typestring;
@property (strong, nonatomic) IBOutlet UISearchBar *usersearchbar;
-(IBAction)backbuttonaction:(id)sender;
-(IBAction)homebuttonaction:(id)sender;
-(IBAction)followButtonAction:(id)sender;

@end
