//
//  BuyandSellViewController.m
//  MarkidPlace
//
//  Created by Stellent Software on 5/31/14.
//  Copyright (c) 2014 stellentmac1. All rights reserved.
//

#import "BuyandSellViewController.h"
#import "OrderViewController.h"
@interface BuyandSellViewController ()

@end

@implementation BuyandSellViewController

@synthesize buyandsellTbl,navbar,mysearchbar,duplicatearray;

@synthesize isbuyer,buycell,buystring;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
#pragma mark--view methods
- (void)viewDidLoad
{
    [super viewDidLoad];
    self.navigationController.navigationBarHidden=YES;
    self.navbar.frame = CGRectMake(0, 0, 320, 64);
    [self.navbar setBackgroundImage:[UIImage imageNamed:@"navbar.png"] forBarMetrics:UIBarMetricsDefault];
    
    tablearray=[NSMutableArray new];
    duplicatearray=[NSMutableArray new];
    
    titlelbl.font = [AppDelegate navtitlefont];
    [mysearchbar  resignFirstResponder];
    
    //aj
    registrationRep=[[RegistrationRepository alloc ]init];
    
    registrationRep.delegate=self;
    if (isbuyer)
    {
        titlelbl.text=@"Purchases";//Purchases
        
        [myappDelegate startspinner:self.view];
        
        [ registrationRep getdata:nil :@"getbuyer" :@"GET" withcount:@"0"];
    }
    else
    {
        
        titlelbl.text=@"Sales";
        
        [myappDelegate startspinner:self.view];
        
        [ registrationRep getdata:nil :@"getseller" :@"GET" withcount:@"0"];
    }
    //ajend

    // Do any additional setup after loading the view from its nib.
}
-(void)viewWillAppear:(BOOL)animated
{
}
#pragma mark--Button Actions
//this action performs navigation back action
-(IBAction)backbtnaction:(id)sender
{
    if([buystring isEqualToString:@"Cart"])
    {
        SuggestionsViewController *sugge;
        if ([AppDelegate isiphone5])
        {
            sugge = [[SuggestionsViewController alloc]initWithNibName:@"SuggestionsViewController" bundle:nil];
        }
        else
        {
            sugge = [[SuggestionsViewController alloc]initWithNibName:@"SuggestionsViewController_iphone4" bundle:nil];
        }
        [self.navigationController pushViewController:sugge animated:YES];


    }
    else if([APPDELEGATE isnullorempty:buystring])
    {
        [self.navigationController popViewControllerAnimated:YES];
    }
    else
    {
    [self.navigationController popViewControllerAnimated:YES];
    }
}
//this action performs pop SuggestionsViewController
-(IBAction)homebtnaction:(id)sender
{
     BOOL isVCExist=NO;
    NSArray *arr=self.navigationController.viewControllers;
    for (UIViewController *childvc in arr) {
        
        if([childvc isKindOfClass:[SuggestionsViewController class]])
        {
            isVCExist=YES;
            SuggestionsViewController *subvc=(SuggestionsViewController *)childvc;
            [self.navigationController popToViewController:subvc animated:YES];
        }
    }
   
    if(!isVCExist)
    {
        SuggestionsViewController *sugge;
        if ([AppDelegate isiphone5])
        {
            sugge = [[SuggestionsViewController alloc]initWithNibName:@"SuggestionsViewController" bundle:nil];
        }
        else
        {
            sugge = [[SuggestionsViewController alloc]initWithNibName:@"SuggestionsViewController_iphone4" bundle:nil];
        }
        [self.navigationController pushViewController:sugge animated:YES];
    }
    
}
//this action performs pop NotificationViewController
-(IBAction)notificationClicked:(id)sender
{
    NotificationViewController *notificationVC;
    
    if(myappDelegate.isiphone5==NO)
    {
        notificationVC=[[NotificationViewController alloc]initWithNibName:@"NotificationViewController_iphone4" bundle:nil];
    }
    else if([[UIScreen mainScreen] bounds].size.height == 568)
    {
        notificationVC=[[NotificationViewController alloc]initWithNibName:@"NotificationViewController" bundle:nil];
        
    }
    
    [self.navigationController pushViewController:notificationVC animated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark--tableviewDelegateMethods
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
       return tablearray.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 105;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *MyIdentifier= @"MyIdentifiertripscell";
    BuyandSellTableViewCell *tbcell= (BuyandSellTableViewCell *)[tableView dequeueReusableCellWithIdentifier:MyIdentifier];
  
    
    if (tbcell==nil)
    {
        [[NSBundle mainBundle]loadNibNamed:@"BuyandSellTableViewCell" owner:self options:nil];
        
        tbcell=buycell;
    }
    
    ItemsModelobject *itemobj=[tablearray objectAtIndex:indexPath.row];
    
    if ([myappDelegate isnullorempty:itemobj.salecostcoststr])
    {
        itemobj.salecostcoststr=@"0";
    }
    
    if ([myappDelegate isnullorempty:itemobj.sizestr])
    {
        itemobj.sizestr=@"--";
    }
    
    if ([myappDelegate isnullorempty:itemobj.itemNamestr])
    {
        itemobj.itemNamestr=@"--";
    }
    tbcell.costlbl.text=[NSString stringWithFormat:@"$%@",itemobj.totalpricestr];
  //  tbcell.sizelbl.text=itemobj.sizestr;
    
    tbcell.itemtitlelbl.text=itemobj.itemNamestr;
    if (itemobj.picturearray.count>0)
    {
        NSString *urlstr=[itemobj.picturearray objectAtIndex:0];
        NSURL *picurl=[NSURL URLWithString:urlstr];
        [tbcell.itemimageview setImageWithURL:picurl             placeholderImage:[UIImage imageNamed:@"noimage.png"] options:SDWebImageCacheMemoryOnly];
    }
    else{
        tbcell.itemimageview.image=[UIImage imageNamed:@"noimage.png"];
    }
    /*
    if (isbuyer==NO)
    {
        tbcell.sellerlbl.hidden=YES;
        tbcell.sellernamelbl.hidden=YES;
    }
     */
    if ([myappDelegate isnullorempty:itemobj.sellernamestr])
    {
        itemobj.sellernamestr=@"--";
    }
    
    if ([myappDelegate isnullorempty:itemobj.orderstatusstr])
    {
        itemobj.orderstatusstr=@"--";
    }
    tbcell.sellerlbl.text=[NSString stringWithFormat:@"%@, %@",itemobj.catogrystr,itemobj.sizestr] ;
    
tbcell.statuslbl.text=[NSString stringWithFormat:@"Status : %@",itemobj.orderstatusstr];
    if (isbuyer)
    {
    tbcell.trackButton.tag=indexPath.row;
    //Krish
    if([itemobj.orderstatusstr isEqualToString:@"Processing"])
    {
        tbcell.trackButton.hidden=NO;
        tbcell.trackBGImgView.hidden=NO;
       // tbcell.trackButton.titleLabel.textColor=[UIColor whiteColor];
       // tbcell.trackButton.titleLabel.text=@"Track";
       
        tbcell.trackButton.titleLabel.font = [UIFont fontWithName:@"Muli" size:14];
        
       [tbcell.trackButton addTarget:self action:@selector(trackButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    }
    else
    {
        tbcell.trackButton.hidden=YES;
        tbcell.trackBGImgView.hidden=YES;
    }
    }
    else
    {
        tbcell.trackButton.hidden=YES;
        tbcell.trackBGImgView.hidden=YES;
    }
    
    
    tbcell.statusnamelbl.font = [UIFont fontWithName:@"Muli" size:14];
    tbcell.statuslbl.font = [UIFont fontWithName:@"Muli" size:14];
    
    tbcell.sellerlbl.font = [UIFont fontWithName:@"Muli" size:13];
    tbcell.itemtitlelbl.font = [UIFont fontWithName:@"Muli" size:16];
    
    tbcell.orderlbl.font = [UIFont fontWithName:@"Muli" size:13];
    
    tbcell.costlbl.font = [UIFont fontWithName:@"Muli" size:12];
    tbcell.sizelbl.font = [UIFont fontWithName:@"Muli" size:13];
    tbcell.sizename.font = [UIFont fontWithName:@"Muli" size:15];
    tbcell.sellernamelbl.font = [UIFont fontWithName:@"Muli" size:15];
    
    tbcell.orderlbl.text=[NSString stringWithFormat:@"Order %@",itemobj.orderDetailsobj.ordernumberstr];
    
    
    
    tbcell.itemimageview.layer.borderWidth=1.0f;
    tbcell.itemimageview.layer.borderColor=[[UIColor clearColor] CGColor];
    tbcell.itemimageview.layer.cornerRadius=5.0f;
    tbcell.itemimageview.clipsToBounds=YES;
    
    
    tbcell.sellerlbl=[AppDelegate lablefitToWidth:tbcell.sellerlbl :70];
    
    tbcell.sizelbl=[AppDelegate lablefitToWidth:tbcell.sizelbl :70];
    
  //  CGRect lblrect=tbcell.sizelbl.frame;
    
    //lblrect.origin.x=(tbcell.sellerlbl.frame.origin.x+tbcell.sellerlbl.frame.size.width)+1;
    
    //tbcell.sizelbl.frame=lblrect;
    
    tbcell.contentView.backgroundColor=[UIColor clearColor];
    
    tbcell.selectionStyle=UITableViewCellSelectionStyleNone;
    return tbcell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    ItemsModelobject *itemobj=[tablearray objectAtIndex:indexPath.row];
      [mysearchbar resignFirstResponder];
    if (myappDelegate.isiphone5)
    {
        
        OrderViewController *ovc=[[OrderViewController alloc]initWithNibName:@"OrderViewController" bundle:Nil];
        
        ovc.itemsobjmodel=itemobj;
        [self.navigationController pushViewController:ovc animated:YES];
        
    }
    else
    {
        OrderViewController *ovc=[[OrderViewController alloc]initWithNibName:@"OrderViewController_iphone4" bundle:Nil];
        ovc.itemsobjmodel=itemobj;
        [self.navigationController pushViewController:ovc animated:YES];
    }
}
- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}
-(void)successresponceListing :(id)respdict :(NSString *)paramname;
{
    
    NSLog(@"check the dict Krish-----%@",respdict);
    [tablearray removeAllObjects];
    if ([respdict isKindOfClass:[NSArray class]])
    {
        NSArray *resparry=(NSArray *)respdict;
        
        for (int i=0; i<resparry.count; i++)
        {
            
            NSDictionary *maindict=[resparry objectAtIndex:i];
            
            ItemsModelobject *itemsmodobj=[[ItemsModelobject alloc]init];
            itemsmodobj.itemNamestr=[maindict objectForKey:@"itemName"];
            itemsmodobj.weightstring=[maindict objectForKey:@"weight"];
            
            itemsmodobj.itemidstr=[maindict objectForKey:@"unitPrice"];
            itemsmodobj.listingcoststr=[maindict objectForKey:@"unitPrice"];
            itemsmodobj.salecostcoststr=[NSString stringWithFormat:@"$%@",[maindict objectForKey:@"listPrice"]];
            if ([maindict objectForKey:@"status"])
            {
                itemsmodobj.itemStatus=[maindict objectForKey:@"status"];
                
            }            itemsmodobj.brandstr=[maindict objectForKey:@"brand"];
            itemsmodobj.conditionstr=[maindict objectForKey:@"condition"];
            itemsmodobj.catogrystr=[maindict objectForKey:@"categoryName"];
            itemsmodobj.sizestr=[maindict objectForKey:@"size"];
            itemsmodobj.descriptionstr=[maindict objectForKey:@"description"];
            
            itemsmodobj.userlistingidstr=[NSString stringWithFormat:@"%@",[maindict valueForKey:@"userListingId"]];
            
            itemsmodobj.itemidstr=[maindict objectForKey:@"userItemId"];
            
            
            NSArray *itempickarray=[maindict objectForKey:@"itemPictures"];
            itemsmodobj.picturearray=[NSMutableArray new];
            
            for (int j=0; j<itempickarray.count; j++)
            {
                NSDictionary *picdict=[itempickarray objectAtIndex:j];
                
                
                NSString *urlsrt=[picdict valueForKey:@"pictureCdnUrl"];
                urlsrt=[urlsrt stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
                
                [itemsmodobj.picturearray addObject:urlsrt];
            }
            
            {
                
                NSDictionary *ordlistdict=[maindict objectForKey:@"order"];
                itemsmodobj.totalpricestr=[ordlistdict objectForKey:@"price"];
                
                NSLog(@"check this order dict %@ ",ordlistdict);
                
                
                {
                 itemsmodobj.orderstatusstr=[ordlistdict valueForKey:@"orderStatus"];
                    
                    itemsmodobj.orderDetailsobj=[[OrderModelObject alloc]init];
                    if (isbuyer)
                    {
                        itemsmodobj.sellernamestr=[ordlistdict valueForKey:@"sellerName"];//@"sellerName"
                         itemsmodobj.orderDetailsobj.sellernamestr=[ordlistdict valueForKey:@"sellerName"];//@"sellerName"
                        
                    }
                    else
                    {
                        itemsmodobj.sellernamestr=[ordlistdict valueForKey:@"buyerName"];
                         itemsmodobj.orderDetailsobj.sellernamestr=[ordlistdict valueForKey:@"buyerName"];//@"sellerName"
                    }
                    itemsmodobj.orderDetailsobj.address1Str=[self converttostr:[ordlistdict valueForKey:@"address1"]];
                    itemsmodobj.orderDetailsobj.address2Str=[self converttostr:[ordlistdict valueForKey:@"address2"]];
                    
                    itemsmodobj.orderDetailsobj.buyerUseridstr=[self converttostr:[ordlistdict valueForKey:@"buyerUserID"]];
                    
                    itemsmodobj.orderDetailsobj.citystr=[self converttostr:[ordlistdict valueForKey:@"city"]];
                    
                    itemsmodobj.orderDetailsobj.orderdatestr=[self converttostr:[ordlistdict valueForKey:@"orderDate"]];
                    
                    itemsmodobj.orderDetailsobj.orderidstr=[self converttostr:[ordlistdict valueForKey:@"orderID"]];
                    itemsmodobj.orderDetailsobj.ordernumberstr=[self converttostr:[ordlistdict valueForKey:@"orderNumber"]];
                    
                    itemsmodobj.orderDetailsobj.orderstatusstr=[self converttostr:[ordlistdict valueForKey:@"orderStatus"]];
                    
                    itemsmodobj.orderDetailsobj.quantitystr=[self converttostr:[ordlistdict valueForKey:@"quantity"]];
                    
                    itemsmodobj.orderDetailsobj.statestr=[self converttostr:[ordlistdict valueForKey:@"state"]];
                    
                    itemsmodobj.orderDetailsobj.useridstr=[self converttostr:[ordlistdict valueForKey:@"userID"]];
                    itemsmodobj.orderDetailsobj.userlistingstr=[self converttostr:[ordlistdict valueForKey:@"userListingID"]];
                    itemsmodobj.orderDetailsobj.zipcodestr=[self converttostr:[ordlistdict valueForKey:@"zipcode"]];
                    itemsmodobj.orderDetailsobj.trackingNumberStr=[self converttostr:[ordlistdict valueForKey:@"trackingNumber"]];
                }
            }
            [tablearray addObject:itemsmodobj];
            
            [duplicatearray addObject:itemsmodobj];
        }
    }
    
    [buyandsellTbl reloadData];
    [myappDelegate stopspinner:self.view];
    
    
    NSLog(@" =-=-=-=-=-=-=-=-=-=- count%lu",(unsigned long)tablearray.count);
}

-(NSString *)converttostr :(id)string
{
    return [NSString stringWithFormat:@"%@",string];
    
}

#pragma mark searchbardelegate

-(void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar
{    if ([searchBar.text isEqualToString:@" "])
    {
        [tablearray removeAllObjects];
        [tablearray addObjectsFromArray:self.duplicatearray];
        [buyandsellTbl reloadData];
    }

}
-(void)searchBarTextDidEndEditing:(UISearchBar *)searchBar
{
    if ([searchBar.text isEqualToString:@" "])
    {
        searchBar.text = @"";
        [tablearray removeAllObjects];
        [tablearray addObjectsFromArray:self.duplicatearray];
        [buyandsellTbl reloadData];
    }

}
-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    [searchBar setShowsCancelButton:NO animated:YES];
    if(searchBar.text.length == 0)
    {
        [tablearray removeAllObjects];
        [tablearray addObjectsFromArray:self.duplicatearray];
        [buyandsellTbl reloadData];
    }
    else
    {
        [searchBar setShowsCancelButton:NO animated:YES];
        [tablearray removeAllObjects];
        NSLog(@"the description of the duplicate Array is %@",duplicatearray.description);
         NSLog(@"the count of the duplicate Array is %lum",(unsigned long)duplicatearray.count);
        
        for(int i=0;i<[self.duplicatearray count];i++)
        {
            ItemsModelobject*searchobj=[self.duplicatearray objectAtIndex:i];
            
       //     NSRange nameRange = [[[self.duplicatearray objectAtIndex:i] valueForKey:@"itemName"]  rangeOfString:searchBar.text options:(NSCaseInsensitiveSearch|NSDiacriticInsensitiveSearch)];
            
            NSRange nameRange = [searchobj.itemNamestr rangeOfString:searchBar.text options:(NSCaseInsensitiveSearch|NSDiacriticInsensitiveSearch)];
            
            if(nameRange.location != NSNotFound)
            {
                [tablearray addObject:[self.duplicatearray objectAtIndex:i]];
            }
        }
        [buyandsellTbl reloadData];
    }
    
if([searchText length] == 0)
{
    [searchBar performSelector: @selector(resignFirstResponder)
                    withObject: nil
                    afterDelay: 0.1];
    return;
}

}
- (BOOL)searchBar:(UISearchBar *)searchBar shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    if ([text isEqualToString:@" "])
    {
        return YES;
    }
    else
    {
        return YES;
    }
}

-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [searchBar resignFirstResponder];
    [tablearray removeAllObjects];
    for(int i=0;i<[self.duplicatearray count];i++)
    {
        ItemsModelobject*searchobj=[self.duplicatearray objectAtIndex:i];
        
        NSRange nameRange = [searchobj.itemNamestr rangeOfString:searchBar.text options:(NSCaseInsensitiveSearch|NSDiacriticInsensitiveSearch)];
        
        if(nameRange.location != NSNotFound)
        {
            [tablearray addObject:[self.duplicatearray objectAtIndex:i]];
        }
    }
    [buyandsellTbl reloadData];
}

-(void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    searchBar.text=@"";
    [searchBar resignFirstResponder];
    [tablearray addObjectsFromArray:duplicatearray];
    [buyandsellTbl reloadData];
    
}
-(void)internetConnection
{
    UIAlertView *Internetalert=[[UIAlertView alloc]initWithTitle:appname message:@"Internet connection not available" delegate:self cancelButtonTitle:nil otherButtonTitles:@"Ok", nil];
    [Internetalert show];
    [APPDELEGATE stopspinner:self.view];
    
}
-(IBAction)trackButtonClicked:(id)sender
{
     NSIndexPath *indexpath=[NSIndexPath indexPathForRow:[sender tag] inSection:0];
    BuyandSellTableViewCell *tblCell=(BuyandSellTableViewCell *)[buyandsellTbl cellForRowAtIndexPath:indexpath];
    ItemsModelobject *itemobj=[tablearray objectAtIndex:[sender tag]];
    
    FAQViewController *termsVC;
    
    CGRect screenbounds = [[UIScreen mainScreen] bounds];
    
    if(screenbounds.size.height==568)
    {
        termsVC = [[FAQViewController alloc] initWithNibName:@"FAQViewController" bundle:nil];
    }
    else if(screenbounds.size.height==480)
    {
        termsVC = [[FAQViewController alloc] initWithNibName:@"FAQViewController" bundle:nil];
    }
    termsVC.isFAQ=@"No";
    termsVC.htmlMainString=[NSString stringWithFormat:@"https://tools.usps.com/go/TrackConfirmAction.action?tRef=fullpage&tLc=1&text28777=&tLabels=%@",itemobj.orderDetailsobj.trackingNumberStr];
    [self.navigationController pushViewController:termsVC animated:YES];
    
}

@end
