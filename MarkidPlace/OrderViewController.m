//
//  OrderViewController.m
//  MarkidPlace
//
//  Created by Varma Bhupatiraju on 10/06/14.
//  Copyright (c) 2014 stellentmac1. All rights reserved.
//

#import "OrderViewController.h"

@interface OrderViewController ()

@end

@implementation OrderViewController
@synthesize orderdatelbl,itemsobjmodel,
orderfrmlbl,addresslbl,sizenumlbl,itemimgview,
termannlabl,
itemdetailslbl,
babyshoeslbl,
sizelbl,
dallerlbl,
dalleronelbl,
purchasecostlbl,
shippingchargeslbl,
shipingoptionlbl,
statuslabl,
shippedlbl,
shippedtolbl,
anystreetlbl,
anywherelbl,datelbl,titlelbl,shippingcostlbl;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    
  //  myappDelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    
    screenSize = [[UIScreen mainScreen]bounds];
    
   // itemsobjmodel=[[ItemsModelobject alloc]init];
    
    
    
    self.navbar.frame = CGRectMake(0, 0, 320, 64);
    [self.navbar setBackgroundImage:[UIImage imageNamed:@"navbar.png"] forBarMetrics:UIBarMetricsDefault];
    
    
    

    [self orderDetails];
}
// This is used  and Assigning the details to the labels.

-(void)orderDetails
{
 titlelbl.font = [UIFont fontWithName:@"Muli" size:17];
 orderdatelbl.font = [UIFont fontWithName:@"Muli" size:13];
 datelbl.font = [UIFont fontWithName:@"Muli" size:13];
 orderfrmlbl.font = [UIFont fontWithName:@"Muli" size:13];
 termannlabl.font = [UIFont fontWithName:@"Muli" size:13];
 itemdetailslbl.font = [UIFont fontWithName:@"Muli" size:18];
 babyshoeslbl.font = [UIFont fontWithName:@"Muli" size:12];
 sizelbl.font = [UIFont fontWithName:@"Muli" size:12];
 sizenumlbl.font = [UIFont fontWithName:@"Muli" size:12];
 dallerlbl.font = [UIFont fontWithName:@"Muli" size:13];
 dalleronelbl.font = [UIFont fontWithName:@"Muli" size:16];
 purchasecostlbl.font = [UIFont fontWithName:@"Muli" size:15];
shippingcostlbl.font = [UIFont fontWithName:@"Muli" size:13];

 shippingchargeslbl.font = [UIFont fontWithName:@"Muli" size:11];
 shipingoptionlbl.font = [UIFont fontWithName:@"Muli" size:11];
 statuslabl.font = [UIFont fontWithName:@"Muli" size:12];
 shippedlbl.font = [UIFont fontWithName:@"Muli" size:12];
 shippedtolbl.font = [UIFont fontWithName:@"Muli" size:13];
 anystreetlbl.font = [UIFont fontWithName:@"Muli" size:13];
 anywherelbl.font = [UIFont fontWithName:@"Muli" size:13];
 addresslbl.font = [UIFont fontWithName:@"Muli" size:13];
 NSLog(@"the before is is %@",itemsobjmodel.orderDetailsobj.orderdatestr);
 
 NSDateFormatter *df = [[NSDateFormatter alloc] init];
 [df setDateFormat:@"yyyy-MM-dd'T'hh:mm:ss.SSS"];
    NSLog(@"itemsobjmodel.orderDetailsobj.orderdatestr %@",itemsobjmodel.orderDetailsobj.orderdatestr);
 NSDate *myDate = [df dateFromString:itemsobjmodel.orderDetailsobj.orderdatestr];
 NSLog(@"the date is %@",myDate);
 
 NSDateFormatter *df2 = [[NSDateFormatter alloc] init];
 [df2 setDateFormat:@"M/dd/yyyy"];
 datelbl.text=[df2 stringFromDate:myDate];
 NSLog(@"the condatestr is %@",datelbl.text);
 
 //  datelbl.text=itemsobjmodel.orderDetailsobj.orderdatestr;
 
 //  datelbl.text=;
 itemdetailslbl.text=itemsobjmodel.itemNamestr;
    NSLog(@"itemsobjmodel.orderDetailsobj.sellernamestr.....%@",itemsobjmodel.orderDetailsobj.sellernamestr);
 termannlabl.text=[NSString stringWithFormat:@"@%@",itemsobjmodel.orderDetailsobj.sellernamestr];
    /////////
    
    CGFloat weightstr=0;
    weightstr=[itemsobjmodel.weightstring floatValue];
    
    if (weightstr<=1)
    {
        shippingcostlbl.text =@"$4.00" ;
    }
    else if (weightstr>1)
    {
        shippingcostlbl.text=@"$7.50";
    }
    
    NSArray *mainArray=[shippingcostlbl.text componentsSeparatedByString:@"$"];
    
     NSArray *mainArray1=[itemsobjmodel.salecostcoststr componentsSeparatedByString:@"$"];
    NSString*str2=[NSString stringWithFormat:@"%@",[mainArray1 objectAtIndex:1]];
    itemcostval=0;
    itemcostval=[str2 floatValue];
    NSLog(@" the total cost is %@",itemsobjmodel.salecostcoststr);
    NSLog(@" the total cost is %f",itemcostval);
    NSString*str=[NSString stringWithFormat:@"%@",[mainArray objectAtIndex:1]];
    shipcostval=0;
    shipcostval=[str floatValue];
    NSLog(@" the total cost is %f",shipcostval);
    costval=0;
    costval=shipcostval+itemcostval;
    NSLog(@" the total cost is %f",costval);
    dalleronelbl.text=[NSString stringWithFormat:@"$%@",[NSString stringWithFormat:@"%0.2f",costval]];
    
     dallerlbl.text=[NSString stringWithFormat:@"$%@",[NSString stringWithFormat:@"%0.2f",itemcostval]];
    //////////
    
    
 babyshoeslbl.text=itemsobjmodel.brandstr;
 //dallerlbl.text=[NSString stringWithFormat:@" %@",itemsobjmodel.salecostcoststr];
  //dalleronelbl.text=[NSString stringWithFormat:@" %@",itemsobjmodel.salecostcoststr];
 shippedlbl.text=[NSString stringWithFormat:@"Status : %@",itemsobjmodel.orderstatusstr];
 addresslbl.text=itemsobjmodel.orderDetailsobj.address2Str;
 titlelbl.text=[NSString stringWithFormat:@"Order #%@",itemsobjmodel.orderDetailsobj.ordernumberstr];
 sizenumlbl.text=itemsobjmodel.sizestr;
    
      //   if(ordermodelobj.)
 // Do any additional setup after loading the view from its nib.
 if([myappDelegate isnullorempty:itemsobjmodel.orderDetailsobj.address1Str])
 {
     if([myappDelegate isnullorempty:itemsobjmodel.orderDetailsobj.address2Str])
     {
         addresslbl.text=@"";
     }
     else
     {
         addresslbl.text=[NSString stringWithFormat:@"%@,%@",itemsobjmodel.orderDetailsobj.address1Str,itemsobjmodel.orderDetailsobj.address2Str];

         //addresslbl.text=itemsobjmodel.orderDetailsobj.address2Str;
     }
     
 }
 else
 {
     addresslbl.text=[NSString stringWithFormat:@"%@,%@",itemsobjmodel.orderDetailsobj.address1Str,itemsobjmodel.orderDetailsobj.address2Str];
 }
 
 NSString *citystr;
 if ([myappDelegate isnullorempty:itemsobjmodel.orderDetailsobj.citystr])
 {
     citystr=@"";
 }
 else
 {
     citystr=itemsobjmodel.orderDetailsobj.citystr;
 }
 
 anystreetlbl.text=citystr;
 
 
 NSString *statestr;
 
 if ([myappDelegate isnullorempty:itemsobjmodel.orderDetailsobj.statestr])
 {
     statestr=@"";
 }
 else{
     statestr=itemsobjmodel.orderDetailsobj.statestr;
 }
 
 NSString *zipcodestr;
 
 if ([myappDelegate isnullorempty:itemsobjmodel.orderDetailsobj.zipcodestr])
 {
     zipcodestr=@"";
 }
 else
 {
     zipcodestr=itemsobjmodel.orderDetailsobj.zipcodestr;
 }
 
 anywherelbl.text=[NSString stringWithFormat:@"%@ %@",statestr,zipcodestr];
  
 if (itemsobjmodel.picturearray.count>0)
 {
     NSString *urlstr=[itemsobjmodel.picturearray objectAtIndex:0];
     NSURL *picurl=[NSURL URLWithString:urlstr];
     [itemimgview setImageWithURL:picurl             placeholderImage:[UIImage imageNamed:@"noimage.png"] options:SDWebImageCacheMemoryOnly];
 }
 else{
     itemimgview.image=[UIImage imageNamed:@"noimage.png"];
 }
 }
// navigationbar back button Action.
-(IBAction)backAction:(id)sender;
{
    [self.navigationController popViewControllerAnimated:YES];
}

// This Action performs pop to  Suggestion ViewController.
-(IBAction)homeBtnAction:(id)sender;
{
    NSArray *arr=self.navigationController.viewControllers;
    for (UIViewController *childvc in arr) {
        
        if([childvc isKindOfClass:[SuggestionsViewController class]])
        {
            SuggestionsViewController *subvc=(SuggestionsViewController *)childvc;
            subvc.ismyfeed=NO;
            [self.navigationController popToViewController:subvc animated:YES];
        }
    }
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)internetConnection
{
    UIAlertView *Internetalert=[[UIAlertView alloc]initWithTitle:appname message:@"Internet connection not available" delegate:self cancelButtonTitle:nil otherButtonTitles:@"Ok", nil];
    [Internetalert show];
    [APPDELEGATE stopspinner:self.view];
    
}
@end
