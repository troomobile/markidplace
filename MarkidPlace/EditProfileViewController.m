//
//  EditProfileViewController.m
//  MarkidPlace
//
//  Created by stellentmac1 on 5/31/14.
//  Copyright (c) 2014 stellentmac1. All rights reserved.
//

#import "EditProfileViewController.h"
#import "SettingsViewController.h"

#import <MobileCoreServices/MobileCoreServices.h> //KISHORE SEP24

@interface EditProfileViewController ()

@end

@implementation EditProfileViewController


@synthesize userNameTxtFld,dropdowntblview;
@synthesize navbar;
@synthesize delegate,mybrandsarray;
@synthesize profileImgView;
@synthesize bgscrollv,fieldArray;
@synthesize paypalImgView,paypallbl,paypaltxtField;
@synthesize isnew,backbtn,address2TextField,address1Label,address1TextField,address2Label,userNameLabel,userTitleNameLabel,nameLabel,nameTextField,zipcodeLabel,zipcodeTextField,cityLabel,cityTextField,stateLabel,stateTextField,sizeandbrandlabel,popview,sizebtn,brandbtn;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
#pragma mark --view methods
- (void)viewDidLoad
{
    [super viewDidLoad];
    fieldArray = [NSArray arrayWithObjects: nameTextField, address1TextField, address2TextField,cityTextField,stateTextField,zipcodeTextField,paypaltxtField,nil];
   // appdelegate=(AppDelegate *)[[UIApplication sharedApplication]delegate];
    self.navbar.frame = CGRectMake(0, 0, 320, 64);
    [self.navbar setBackgroundImage:[UIImage imageNamed:@"navbar.png"] forBarMetrics:UIBarMetricsDefault];
        
    
       [self editProfileMethod];

     brandarray=[[NSMutableArray alloc]init];
    mybrandsarray=[[NSMutableArray alloc]init];
     settingsmodelobject *obj=[[settingsmodelobject alloc]init];
    dropdowntblview.layer.borderWidth = 1;
    dropdowntblview.layer.cornerRadius=1.0f;
    // obj.namestr=[[settingsmodelobject alloc]init];
    
    dropdowntblview.layer.borderColor =[[UIColor whiteColor ] CGColor];
   

     sepstr=@"hai";
    [bgscrollv setContentSize:CGSizeMake(320, bgscrollv.frame.size.height+100)];
    
    profileRepos=[[ProfileRepository alloc]init];
    profileRepos.delegate=self;
    NSString *userid=myappDelegate.useridstring;
    [profileRepos getProfileData:userid];
    textchangebool=NO;
    
}

-(void)editProfileMethod
{
    CALayer *layer2 = profileImgView.layer;
    [layer2 setCornerRadius:39.0];
    [layer2 setBorderWidth:0];
    [layer2 setMasksToBounds:YES];
    myappDelegate.useridstring=[myappDelegate getStringFromPlist:@"userid"];
    [myappDelegate startspinner:self.view];
   // NSString *userid=myappDelegate.useridstring;
   // [profileRepos getProfileData:userid];
    
    self.nameTextField.font = [UIFont fontWithName:@"Muli" size:15];
    
    self.address1TextField.font = [UIFont fontWithName:@"Muli" size:15];
    self.paypaltxtField.font = [UIFont fontWithName:@"Muli" size:15];
    self.address2TextField.font = [UIFont fontWithName:@"Muli" size:15];
    self.cityTextField.font = [UIFont fontWithName:@"Muli" size:15];
    self.stateTextField.font = [UIFont fontWithName:@"Muli" size:15];
    self.zipcodeTextField.font = [UIFont fontWithName:@"Muli" size:15];
    self.userNameLabel.font = [UIFont fontWithName:@"Muli" size:15];
    self.userNameTxtFld.font = [UIFont fontWithName:@"Muli" size:15];
    
    self.sizeandbrandlabel.font =[UIFont fontWithName:@"Muli" size:17];
    
    self.address1Label.font = [UIFont fontWithName:@"Muli" size:15];
    self.paypallbl.font = [UIFont fontWithName:@"Muli" size:15];
    
    self.address2Label.font = [UIFont fontWithName:@"Muli" size:15];
    self.cityLabel.font = [UIFont fontWithName:@"Muli" size:15];
    self.stateLabel.font = [UIFont fontWithName:@"Muli" size:15];
    self.zipcodeLabel.font = [UIFont fontWithName:@"Muli" size:15];
    self.nameLabel.font = [UIFont fontWithName:@"Muli" size:15];
    self.userTitleNameLabel.font = [UIFont fontWithName:@"Muli" size:15];
    
    NSString *usernamestring=[myappDelegate getStringFromPlist:usernamekey];
    
    NSLog(@"@@@@@@@@User Name String is:%@",usernamestring);
    
    self.profileImgView.layer.cornerRadius=self.profileImgView.frame.size.width/2;
    
    self.profileImgView.layer.borderColor=[[UIColor lightGrayColor]CGColor];
    self.profileImgView.layer.borderWidth=3;
    self.profileImgView.layer.masksToBounds=YES;
    
    
   // self.profileImgView.contentMode=UIViewContentModeCenter; Bykishore
   // self.profileImgView.backgroundColor=[UIColor whiteColor];
    
    UILabel *dollerlbl1=[[UILabel alloc]initWithFrame:CGRectMake(0, 0, 15, 20)];
    dollerlbl1.textColor=userNameTxtFld.textColor;
    dollerlbl1.font=userNameTxtFld.font;
    dollerlbl1.text=@"@";
    
    [userNameTxtFld setLeftViewMode:UITextFieldViewModeAlways];
    userNameTxtFld.leftView= dollerlbl1;
    
    self.viewtitlelbl.font = [UIFont fontWithName:@"Muli" size:20];
    if ([myappDelegate isnullorempty:usernamestring])
    {
        userNameTxtFld.text=@"";
        self.userNameLabel.text=@"";
        userNameTxtFld.userInteractionEnabled=YES;
    }
    else
    {
        self.userNameLabel.text=[NSString stringWithFormat:@"@%@",usernamestring];
        userNameTxtFld.text=[NSString stringWithFormat:@"%@",usernamestring];
        userNameTxtFld.userInteractionEnabled=YES;
    }
}

-(void)successResponseforProfile:(NSDictionary *)responseDict
{
     dispatch_async(dispatch_get_main_queue(), ^{
    if (isnew)
    {
        backbtn.hidden=YES;
    }
    [myappDelegate stopspinner:self.view];
    NSLog(@"@@@@@Response For Profile is:%@",responseDict);
    
    self.nameTextField.text=[responseDict valueForKey:@"name"];
    self.address1TextField.text=[responseDict valueForKey:@"address1"];
    self.address2TextField.text=[responseDict valueForKey:@"address2"];
    self.cityTextField.text=[responseDict valueForKey:@"city"];
    self.stateTextField.text=[responseDict valueForKey:@"state"];
    self.zipcodeTextField.text=[responseDict valueForKey:@"zipcode"];
    self.userNameTxtFld.text=[responseDict valueForKey:@"userName"];
    self.userNameLabel.text=[NSString stringWithFormat:@"@%@",userNameTxtFld.text];
    NSLog(@"%@",[responseDict valueForKey:@"paypalEmailAddress"]);
    if (![[responseDict valueForKey:@"paypalEmailAddress"] isEqualToString:@""])
    {
        
        self.paypaltxtField.text=[responseDict valueForKey:@"paypalEmailAddress"];

    }
    pic_str =[[responseDict valueForKey:@"profilepic"] stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
    [myappDelegate insertStringInPlist:@"profilePicture" value:pic_str];
    if([self Contains:@"twimg" on:pic_str])
    {
        self.profileImgView.contentMode=UIViewContentModeCenter;
        self.profileImgView.backgroundColor=[UIColor whiteColor];
    }
    [profileImgView setImageWithURL:[NSURL URLWithString:pic_str] placeholderImage:[UIImage imageNamed:@"noimage_friend.png"] options:SDWebImageHighPriority];
    
     });
}
-(BOOL)Contains:(NSString *)StrSearchTerm on:(NSString *)StrText
{
    return  [StrText rangeOfString:StrSearchTerm options:NSCaseInsensitiveSearch].location==NSNotFound?FALSE:TRUE;
}
-(void)updateprofilesuccessprofile:(NSDictionary *)responseDict
{
    NSLog(@"response.....%@",responseDict);
    NSString *successString = [responseDict valueForKey:@"message"];
    
    if ([successString isEqualToString:@"User updated successfully"])
    {
        [myappDelegate insertStringInPlist:@"userid" value:responseDict[@"userId"]];
        [myappDelegate insertStringInPlist:@"email" value:responseDict[@"email"]];
        [myappDelegate insertStringInPlist:@"realname" value:responseDict[@"name"]];
        NSString *teststr=responseDict[@"userName"];
         dispatch_async(dispatch_get_main_queue(), ^{
        if(isnew)
        {
            
            isChange=YES;
            isnew=NO;
          /*  if([sepstr isEqualToString:@"Brands"])
            {
                [self MyBrandsBtnAction:nil];
            }
            if([sepstr isEqualToString:@"Sizes"])
            {
                [self MySizeBtnAction:nil];
            }*/
            backbtn.hidden=YES;
        }
         });
        teststr=[teststr stringByReplacingOccurrencesOfString:@"@" withString:@""];
        
        [myappDelegate insertStringInPlist:usernamekey value:teststr];
        
        [myappDelegate insertStringInPlist:@"profilePicture" value:responseDict[@"profilePicture"]];
        if([otherstr isEqualToString:@"directstr"])
        {
            UIAlertView *alertView=[[UIAlertView alloc]initWithTitle:appname message:@"Saved!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            alertView.tag=123;
            [alertView show];
        }
        else
        {
            UIAlertView *alertView=[[UIAlertView alloc]initWithTitle:appname message:@"Saved!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            alertView.tag=9999;
            isOther=NO;
            otherstr=@"sdf0";
            [alertView show];
        }
}
    else if ([successString isEqualToString:@"User updated successfully"])
    {
        [AppDelegate alertmethod:appname :[NSString stringWithFormat:@"User with %@ username is already exist. Please try unique username.",userNameTxtFld.text]];
    }
    else if([successString isEqualToString:@"Please Enter Valid Address Details"])
    {
        
        [AppDelegate alertmethod:appname :@"Please Enter Valid Address"];
    }

    else if([successString isEqualToString:@"User already exists with this username."])
    {
        
        [AppDelegate alertmethod:appname :successString];
        
    }
    else
    {
        NSLog(@"success string..%@",successString);
    }
    [myappDelegate stopspinner:self.view];
}
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag==123)
    {
        isnew=isChange;
        if (isnew==YES)
        {
            /*
           if ([AppDelegate isiphone5])
            {
                myappDelegate.suggestionviewcont = [[SuggestionsViewController alloc]initWithNibName:@"SuggestionsViewController" bundle:nil];
                
            }
            else
            {
                myappDelegate.suggestionviewcont = [[SuggestionsViewController alloc]initWithNibName:@"SuggestionsViewController_iphone4" bundle:nil];
                
            }
              [self.navigationController pushViewController:myappDelegate.suggestionviewcont animated:YES];
             */
            FindFriendsViewController *findVC;
            if (myappDelegate.isiphone5)
            {
                findVC=[[FindFriendsViewController alloc]initWithNibName:@"FindFriendsViewController" bundle:nil];
            }
            else
            {
                findVC=[[FindFriendsViewController alloc]initWithNibName:@"FindFriendsViewController_4s" bundle:nil];
                
            }
            findVC.backButton.hidden=YES;
            

             [self.navigationController pushViewController:findVC animated:YES];
            
           
        }
        else
        {
            BOOL isVCExist=NO;
            NSArray *arr=self.navigationController.viewControllers;
            for (UIViewController *childvc in arr)
            {
                
                if([childvc isKindOfClass:[SuggestionsViewController class]])
                {
                    isVCExist=YES;
                    UIViewController *subvc=(SuggestionsViewController *)childvc;
                    [self.navigationController popToViewController:subvc animated:YES];
                }
            }
            
            if(!isVCExist)
            {
                
                
                ProfileViewController *profileVC;
                if (myappDelegate.isiphone5)
                {
                    profileVC = [[ProfileViewController alloc]initWithNibName:@"ProfileViewController" bundle:nil];
                }
                else
                {
                    profileVC = [[ProfileViewController alloc]initWithNibName:@"ProfileViewController_iphone4" bundle:nil];
                }
                [self.navigationController pushViewController:profileVC animated:YES];
            }
        }
    }
    if (alertView.tag==12345)
    {
        [self popViewMethod:NO];
    }
    if(alertView.tag==9999)
    {
        [self performSelector:@selector(buttonindex2) withObject:nil afterDelay:0];
    }
    if (alertView.tag==999)
    {
        if (buttonIndex==1)
        {
            
        [self doneAction:nil];
          
        }
        else
       /* {
            SettingsViewController *settingVC;
            if (myappDelegate.isiphone5)
            {
                settingVC=[[SettingsViewController alloc]initWithNibName:@"SettingsViewController" bundle:nil];
                
            }
            else
            {
                settingVC=[[SettingsViewController alloc]initWithNibName:@"SettingsViewController_iphone4" bundle:nil];
                
                
            }
            if(isnew==YES)
            {
                paypaltxtField.text=@"";
            }
            profileRepos=[[ProfileRepository alloc]init];
            profileRepos.delegate=self;
            NSString *userid=myappDelegate.useridstring;
            [profileRepos getProfileData:userid];
            textchangebool=NO;
            [self.navigationController pushViewController:settingVC animated:YES];
            
        }*/
        {
            profileRepos=[[ProfileRepository alloc]init];
            profileRepos.delegate=self;
            NSString *userid=myappDelegate.useridstring;
            [profileRepos getProfileData:userid];
            textchangebool=NO;
            [self performSelector:@selector(buttonindex2) withObject:nil afterDelay:1];

        }
        
    }
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}
#pragma mark --button actions

-(IBAction)backAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
    [currentTextField resignFirstResponder];
    
    [self moveViewUp:NO];
}
-(IBAction)doneAction:(id)sender
{
    dispatch_async( dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        dispatch_async( dispatch_get_main_queue(), ^{
      [currentTextField resignFirstResponder];
    if(isOther==YES)
    {
        if([sepstr isEqualToString:@"Brands"]||[sepstr isEqualToString:@"Sizes"])
        {
            otherstr=@"indirectstr";
        }
    }
    else
    {
        otherstr=@"directstr";
    }
        [self moveViewUp:NO];
    
    if (zipcodeTextField.text.length>0)
    {
        int checkint=[zipcodeTextField.text intValue];
        
        
        if (checkint<1)
        {
            zipcodeTextField.text=@"";
        }
    }
    
    if (userNameTxtFld.text.length<1 || self.nameTextField.text.length<1 || self.address1TextField.text.length <1 || self.cityTextField.text.length<1 || self.stateTextField.text.length <1 || self.zipcodeTextField.text.length <1)
    {
        [AppDelegate alertmethod:appname :@"All fields are required"];
        return;
    }
    if (paypaltxtField.text.length>0)
    {
        if ([self emailvalidate:paypaltxtField.text])
        {
            [myappDelegate startspinner:self.view];
            
            profileRepos.delegate=self;
            [self performSelector:@selector(addressVerification) withObject:nil afterDelay:0.1];
            return;
        }
        else
            return;
    }
    else
    {
        [myappDelegate startspinner:self.view];
        
        profileRepos.delegate=self;
        [self performSelector:@selector(addressVerification) withObject:nil afterDelay:0.1];
        return;
    }
   
    return;
        });
    });
}
-(IBAction)mySizesAndBrands:(id)sender
{
    if (textchangebool)
    {
        UIAlertView *alertview=[[UIAlertView alloc]initWithTitle:appname message:@"Would you like to save the changes?" delegate:self cancelButtonTitle:@"NO" otherButtonTitles:@"YES", nil];
        sepstr=@"Brands";
        alertview.tag=999;
        isOther=YES;
        [alertview show];
        
    }
    else
    {
        
        SettingsViewController *settingVC;
        if (myappDelegate.isiphone5)
        {
            settingVC=[[SettingsViewController alloc]initWithNibName:@"SettingsViewController" bundle:nil];
            
        }
        else
        {
            settingVC=[[SettingsViewController alloc]initWithNibName:@"SettingsViewController_iphone4" bundle:nil];
            
            
        }
        isOther=NO;

        [self.navigationController pushViewController:settingVC animated:YES];
        
    }

    
//    MySizesViewController *mysizesVC;
//    if (myappDelegate.isiphone5)
//    {
//        mysizesVC=[[MySizesViewController alloc]initWithNibName:@"MySizesViewController" bundle:nil];
//        
//    }
//    else{
//        mysizesVC=[[MySizesViewController alloc]initWithNibName:@"MySizesViewController_iphone4" bundle:nil];
//        
//        
//    }
//    [self.navigationController pushViewController:mysizesVC animated:YES];
    

    
}
/***********posting the user details*************/
-(void)postWithDelay
{
    
    dispatch_async( dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        dispatch_async( dispatch_get_main_queue(), ^{
            [APPDELEGATE startspinner:self.view];

    //[appdelegate  configureAmazoneservics];
    NSMutableArray *urlsarray=[myappDelegate postimagestoamazone:NO photosaray:nil :self.profileImgView.image];
    
    NSString *finalstr=  [[urlsarray valueForKey:@"description"] componentsJoinedByString:@","];
    
    NSString *userid=[myappDelegate getStringFromPlist:@"userid"];
    NSData   *imageData = UIImageJPEGRepresentation(self.profileImgView.image, 0.25f);
    
    
    NSMutableDictionary *orderDict = [[NSMutableDictionary alloc]init];
    [orderDict setValue:userid forKey:@"UserId"];
    NSString *usernamestr=[userNameTxtFld.text stringByReplacingOccurrencesOfString:@"@" withString:@""];
    
    [orderDict setValue:[usernamestr lowercaseString] forKey:@"userName"];
    //[orderDict setObject:imageData forKey:@"Profilepic"];
    [orderDict setValue:self.nameTextField.text forKey:@"Name"];
    [orderDict setValue:self.address1TextField.text forKey:@"Address1"];
    [orderDict setValue:self.address2TextField.text forKey:@"Address2"];
    [orderDict setValue:self.cityTextField.text forKey:@"City"];
    [orderDict setValue:self.stateTextField.text forKey:@"State"];
    [orderDict setValue:[NSString stringWithFormat:@"%@",self.zipcodeTextField.text] forKey:@"Zipcode"];
    if (paypaltxtField.text.length>0 && paypaltxtField.text !=nil)
    {
        
        [orderDict setObject:[NSString stringWithFormat:@"%@",self.paypaltxtField.text] forKey:@"PaypalEmailAddress"];
    }
    [APPDELEGATE insertStringInPlist:@"paypalstr" value:paypaltxtField.text];
    [orderDict setValue:finalstr forKey:@"Profilepic"];
            NSLog(@"post delay is %@",orderDict);
    [profileRepos postProfileDetails:orderDict :imageData];
    
        });
    });
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
//this action  performs choose the gallery or camera
-(IBAction)camraBtnAction:(id)sender
{
    [currentTextField resignFirstResponder];
    
    [self moveViewUp:NO];
    
    CGRect frame=self.view.frame;
    frame.origin.y=0;
    self.view.frame=frame;
    [UIView commitAnimations];
    
    UIActionSheet *action = [[UIActionSheet alloc]initWithTitle:@"Profile Image" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Camera",@"Gallery", nil];
    [action showInView:self.view];
    
}
//this action sheet performs pick the images
-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    UIImagePickerController *picker = [[UIImagePickerController alloc]init];
    if(buttonIndex==0)
    {
        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
        picker.delegate =self;
       // picker.wantsFullScreenLayout = YES;
        picker.allowsEditing = YES;
        [self presentViewController:picker animated:YES completion:nil];
    }
    else if(buttonIndex==1)
    {
        picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        picker.delegate = self;
       // picker.wantsFullScreenLayout = YES;
        picker.allowsEditing = YES;
        [self presentViewController:picker animated:YES completion:nil];
    }
    else
    {
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}
//KISHORE SEP24
/*
-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    
    NSLog(@"%s",__FUNCTION__);
    
    //------
 //   NSString *mediaType = info[UIImagePickerControllerMediaType];
    //UIImagePickerControllerOriginalImage
    //UIImagePickerControllerEditedImage
   // if ([mediaType isEqualToString:(NSString *)kUTTypeImage])
 //  {
        UIImage * uiImage=[info valueForKey:@"UIImagePickerControllerEditedImage"];
        NSLog(@"@@@@@@@@@@@@@@ uiImage.size.height-----%f",uiImage.size.height);
        if([self Contains:@"twimg" on:pic_str])
        {
            self.profileImgView.contentMode=UIViewContentModeScaleToFill;
        }
        NSData *data = UIImageJPEGRepresentation(uiImage,0.2);
        self.profileImgView.image=[UIImage imageWithData:data];
        [self cropImageViewControllerDidFinished:self.profileImgView.image];
        
        /*
         UIImage *editedImage;
         NSData   *imageData;
         if (uiImage.size.height<640)
         {
         NSLog(@"@@@@@@@@@@@@@@@@@@@ 66666644444000000 @@@@@@@@@@@@@@@@");
         editedImage=[self squareImageFromImage :uiImage scaledToSize:320.0];
         imageData = UIImageJPEGRepresentation(editedImage, 1.0);
         self.profileImgView.image=[UIImage imageWithData:imageData];
         }
         else
         {
         NSLog(@"################ 33333333322222222000000000 ################");
         uiImage = info[UIImagePickerControllerEditedImage];
         imageData = UIImageJPEGRepresentation(uiImage, 1.0);
         self.profileImgView.image=uiImage;
         }
         * /
        
    //}
    
    [picker dismissViewControllerAnimated:NO completion:nil];
    //-----End
}
*/
 
-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingImage:(UIImage *)image editingInfo:(NSDictionary *)editingInfo
{
    if([self Contains:@"twimg" on:pic_str])
    {
        self.profileImgView.contentMode=UIViewContentModeScaleToFill;
    }
    self.profileImgView.image=image;
    [self cropImageViewControllerDidFinished:image];
    [picker dismissModalViewControllerAnimated:YES];
}

//KISHORE END
/*
//here assign the  images
-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingImage:(UIImage *)image editingInfo:(NSDictionary *)editingInfo
{
    //self.profileImgView.image=image;
    
    
    [picker dismissViewControllerAnimated:NO completion:nil];
    [self cropImageViewControllerDidFinished:image];
    
    [ AppDelegate cropimage:self :image];
}

*/

-(void)cropImageViewControllerDidFinished:(UIImage *)image
{
   
  UIImage *generatedImage = nil;
    NSData *data = UIImageJPEGRepresentation(image,0.2);
    generatedImage = [[UIImage alloc] initWithData:data];
   // UIImageView.image = image;
    self.profileImgView.image=generatedImage;
}

#pragma mark-- UITextField Delegate Methods
-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    NSLog(@"didbegin --");
    currentTextField=textField;
    //[UIView beginAnimations:nil context:NULL];
    //[UIView setAnimationDuration:0.3];
    
   // CGRect rect = self.view.frame;
    
    CGRect actualframe = [textField convertRect:[textField bounds] toView:self.view];
    if ((actualframe.origin.y+85) >= (self.view.frame.size.height-216))
    {
        
        //int upval=(self.view.frame.size.height-216)-(actualframe.origin.y+45);
       // int upval=self.view.frame.origin.y-(actualframe.origin.y-85);
        
          CGRect scrollfrm = [textField convertRect:[textField bounds] toView:self.bgscrollv];
        [bgscrollv setContentOffset:CGPointMake(0, scrollfrm.origin.y-30) animated:NO];
        //rect.origin.y =upval;
    }
    
    
   // self.view.frame = rect;
    
    //[UIView commitAnimations];
    
    
}
-(void)textFieldDidEndEditing:(UITextField *)textField
{
    NSLog(@"chekc the editing %d",self.editing);
    
  [bgscrollv setContentOffset:CGPointMake(0, 0) animated:NO];
    
}/********Animation For TextField ***********/



-(void) moveViewUp:(BOOL)moveUP
{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.3];
    
    CGRect rect = self.view.frame;
    
    CGRect screenbounds=[[UIScreen mainScreen]bounds];
    
    if (moveUP)
    {
        if(screenbounds.size.height==568)
        {
            if (currentTextField==self.stateTextField)
            {
                rect.origin.y = -50;
            }
            else if (currentTextField==self.zipcodeTextField)
            {
                rect.origin.y = -50;
            }
            
        }
        else
        {
            
        }
    }
    else
    {
        rect.origin.y =0;
        
    }
    self.view.frame = rect;
    
    [UIView commitAnimations];
    
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    textchangebool=YES;
    
    if (userNameTxtFld==textField)
    {
        NSCharacterSet *cs = [[NSCharacterSet characterSetWithCharactersInString:AlphaBet] invertedSet];
        
        NSString *filtered = [[string componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
        return [string isEqualToString:filtered];
    }
    else if (address1TextField==textField||address2TextField==textField)
        {
            /*
             NSCharacterSet *cs = [[NSCharacterSet characterSetWithCharactersInString:AlphaNumeric_username] invertedSet];
             
             NSString *filtered = [[string componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
             
             return [string isEqualToString:filtered];

             */
            
            NSCharacterSet *cs = [[NSCharacterSet characterSetWithCharactersInString:AlphaNumeric_address] invertedSet];
            
            NSString *filtered = [[string componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
            return [string isEqualToString:filtered];
        }

    else if (zipcodeTextField==textField)
    {
        
        NSCharacterSet *cs = [[NSCharacterSet characterSetWithCharactersInString:Numeric] invertedSet];
        
        NSString *filtered = [[string componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
        
        if ([string isEqualToString:filtered]==YES)
        {
            if([string isEqualToString:@""])
            {
                return YES;
            }
            if (textField.text.length <= 5)
            {
                return YES;
            }
            else
            {
                return NO;
            }
        }
        else
        {
            return NO;
        }
        
        
    }
    else
    {
        if (textField.tag==7)
        {
            return YES;
            
        }
        else
        {
            NSCharacterSet *cs = [[NSCharacterSet characterSetWithCharactersInString:[NSString stringWithFormat:@"%@%@-/",AlphaBet,Numeric] ] invertedSet];
            
            NSString *filtered = [[string componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
            return [string isEqualToString:filtered];
        }
        
    }
    
    return YES;
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
//    [self moveViewUp:NO];
//
//    [textField resignFirstResponder];
//    [self setEditing:NO];
//    return YES;
    BOOL didResign = [textField resignFirstResponder];
    if (!didResign) return NO;
    
    NSUInteger index = [self.fieldArray indexOfObject:textField];
    if (index == NSNotFound || index + 1 == fieldArray.count) return NO;
    
    id nexField = [fieldArray objectAtIndex:index + 1];
    textField = nexField;
    [nexField becomeFirstResponder];
    
    return NO;

}

#pragma mark -- End Of UITextField Delegates
#pragma  mark Other 
- (BOOL) emailvalidate:(NSString *)emailaddress
{
    NSString *email;
   
    
    email=[emailaddress stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    
    NSString *emailRegEx = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
	NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegEx];
    
    
    if(email==nil||[email isEqualToString:@""] || email.length==0 || [email isEqualToString:@" "])
    {
        //   [spinner stopAnimating];
        UIAlertView *alertview=[[UIAlertView alloc]initWithTitle:appname message:@"Email should not be empty." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alertview show];
        return NO;
	}
    else if ([emailTest evaluateWithObject:email] != YES)
    {
        UIAlertView *loginalert = [[UIAlertView alloc] initWithTitle:appname message:@"Email field not in proper format." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:Nil, nil];
        [loginalert show];
        return NO;
    }
    else
    {
        return YES;
    }
}

-(IBAction)MySizeBtnAction:(id)sender
{
       MySizesViewController *MySizesVC;
    
    if (isnew)
    {
        isOther=YES;
        [self doneAction:nil];
        sepstr=@"Sizes";
    }
    else
    {
        isOther=NO;
        sepstr=@"";
    if (myappDelegate.isiphone5)
    {
        MySizesVC=[[MySizesViewController alloc]initWithNibName:@"MySizesViewController" bundle:nil];
    }
    else
    {
        MySizesVC=[[MySizesViewController alloc]initWithNibName:@"MySizesViewController_iphone4" bundle:nil];
    }
    [self.navigationController pushViewController:MySizesVC animated:YES];
    }
}
-(IBAction)MyBrandsBtnAction:(id)sender
{
    if (isnew)
    {
        isOther=YES;
        [self doneAction:nil];
         sepstr=@"Brands";
    }
    else
    {
        isOther=NO;
        sepstr=@"";
        NSString *userid=[myappDelegate getStringFromPlist:@"userid"];
        NSLog(@"the userid is %@",userid);
       profilerepo=[[ProfileRepository alloc]init];
        profilerepo.delegate=self;
        [profilerepo getbrandsettings:userid];
        [self popViewMethod:YES];
        [myappDelegate startspinner:self.view];
    }
        // [dropdowntblview reloadData];
        
}

-(void)popViewMethod :(BOOL)isshow
{
    CGRect popfrm=popview.frame;
    popfrm.origin.x=0;
    if (isshow)
    {
        // [self.view addSubview:popview];
        popfrm.origin.y=0;
        
        
        CATransition *transition = [CATransition animation];
        transition.duration = 0.3;
        transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
        transition.type = kCATransitionPush;
        transition.subtype =kCATransitionFromTop;
        transition.delegate = self;
        [popview.layer addAnimation:transition forKey:nil];
    }
    else
    {
        popfrm.origin.y=605;
        CATransition *transition = [CATransition animation];
        transition.duration = 0.3;
        transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
        transition.type = kCATransitionPush;
        transition.subtype =kCATransitionFromBottom;
        transition.delegate = self;
        [popview.layer addAnimation:transition forKey:nil];
    }
    
    //[UIView beginAnimations: @"anim" context: nil];
    //[UIView setAnimationBeginsFromCurrentState: YES];
    // [UIView setAnimationDuration:0.3];
    popview.frame=popfrm;
    // [UIView commitAnimations];
    [self.view bringSubviewToFront:popview];
    [self.view addSubview:popview];
    
}
-(IBAction)cancelBtnAction:(id)sender
{
    [self popViewMethod:NO];
}
-(IBAction)DoneAction:(id)sender
{
    
    [self popViewMethod:NO];
    
    NSString *userid=[myappDelegate getStringFromPlist:@"userid"];
    
    NSString *finalstr=[brandarray componentsJoinedByString:@","];
    
    NSLog(@"finalstr....%@",finalstr);
    NSMutableDictionary *dic=[[NSMutableDictionary alloc]init];
    
    [dic setObject:userid forKey:@"UserId"];
    [dic setObject:finalstr forKey:@"Brands"];
    // [dic setValue:userid forKey:@"UserId"];
    //[dic setValue:finalstr forKey:@"Brands"];
    //  appdelegate= (AppDelegate *)[[UIApplication sharedApplication]delegate];
    
    
    //calling the repository
    profilerepo=[[ProfileRepository alloc]init];
    profilerepo.delegate=self;
    [profilerepo updatebrand:dic];
    
}
#pragma mark tableviewdelegate


-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    //return 4;
    if (tableView==dropdowntblview)
    {
        return 1;
    }
    else
    {
        return 7;
    }
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
           return mybrandsarray.count;
        
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
        UITableViewCell *tbcell=[tableView dequeueReusableCellWithIdentifier:@"cell"];
        if(tbcell==nil)
        {
            tbcell=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
        }
        tbcell.textLabel.font = [UIFont fontWithName:@"Muli" size:15];
        
        tbcell.textLabel.textColor=[UIColor whiteColor];//
        
        tbcell.textLabel.backgroundColor=[UIColor clearColor];
        tbcell.selectionStyle=UITableViewCellSelectionStyleNone;
        tbcell.contentView.backgroundColor=[UIColor clearColor];
        tbcell.backgroundColor=[UIColor clearColor];
        
        settingsmodelobject *lookobj=[mybrandsarray objectAtIndex:indexPath.row];
        NSLog(@"lookobj.brandid....%@",lookobj.brandid);
        
        tbcell.textLabel.text=lookobj.brandname;
        NSLog(@"tbcell.textLabel.text....%@",tbcell.textLabel.text);
        
        //obj.namestr
        if (lookobj.isCheckMark)
        {
            tbcell.accessoryType=UITableViewCellAccessoryCheckmark;
            tbcell.tintColor = [UIColor whiteColor];
            NSString *brandid=lookobj.brandid;
            [brandarray addObject:brandid];
        }
        else
        {
             tbcell.accessoryType=UITableViewCellAccessoryNone;
        }
        NSLog(@"access type %d",tbcell.accessoryType);
        return tbcell;
    
}

-(void)successResponsegetbrandsettings:(NSMutableArray *)responsearray;
{
    
    dispatch_async(dispatch_get_main_queue(), ^{
        for (int i=0;i<[responsearray count];i++)
        {
            settingsmodelobject *obj=[[settingsmodelobject alloc]init];
            NSMutableDictionary *responsedic=[responsearray objectAtIndex:i];
            
            NSLog(@"responsedict1...%@",responsearray);
            
            obj.brandname=[responsedic valueForKey:@"lookupValue"];
            NSLog(@"obj.namestr....%@",obj.brandname);
            obj.brandid=[responsedic valueForKey:@"lookupID"];
            obj.isCheckMark=[[responsedic valueForKey:@"isSelect"] boolValue];
            NSLog(@"obj.brandid...%@",obj.brandid);
            [mybrandsarray addObject:obj];
            
        }
        [myappDelegate stopspinner:self.view];
        [dropdowntblview reloadData];
    });
    
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
        NSLog(@"index.......%@",indexPath);
        UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
        
        //dropdownview.separatorStyle=NO;
        cell.selectionStyle=NO;
        
        // if (tableView.tag==111)
        // {
        
        
        //LookupModelobject *lookobj=[mybrandsarray objectAtIndex: indexPath.row];
        settingsmodelobject *lookobj=[mybrandsarray objectAtIndex:indexPath.row];
        
        if (lookobj.isCheckMark)
        {
            lookobj.isCheckMark=NO;
            NSString *brandid=lookobj.brandid;
            [brandarray removeObject:brandid];
        }
        else
        {
            lookobj.isCheckMark=YES;
            NSString *brandid=lookobj.brandid;
            [brandarray addObject:brandid];
            //obj.namestr=[lookobj.isCheckMark];
            
        }
        
        
        
        
        [dropdowntblview reloadData];
        // }
        
        //else
        //{
        
        NSLog(@"cell.accessoryType...%ld",cell.accessoryType);
        NSLog(@"UITableViewCellAccessoryCheckmark...%ld",(long)UITableViewCellAccessoryCheckmark);
        if (cell.accessoryType == UITableViewCellAccessoryCheckmark)
        {
            cell.accessoryType = UITableViewCellAccessoryNone;
            cell.tintColor = [UIColor whiteColor];
        }
        else
        {
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
            cell.tintColor = [UIColor whiteColor];
        }
    }
-(void)updatebrandsuccessResponse:(NSDictionary *)responseDict
{
    NSLog(@"responseDict.....%@",responseDict);
    if ([[responseDict valueForKey:@"message"] isEqualToString:@"MyBrands Update Successfully"])
    {
        //[AppDelegate alertmethod:appname :@"Your account has been deleted successfully, Thanks For using Markid place"];
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.001 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            
            UIAlertView *myalert=[[UIAlertView alloc]initWithTitle:appname message:@"Saved!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            myalert.tag=234;
            [myalert show];
            
            
            //[myappDelegate stopspinner:self.view];
            
            
        });
        
    }
    
    
}
-(void)internetConnection
{
    dispatch_async(dispatch_get_main_queue(), ^{
        
        UIAlertView *Internetalert=[[UIAlertView alloc]initWithTitle:appname message:@"Internet connection not available" delegate:self cancelButtonTitle:nil otherButtonTitles:@"Ok", nil];
        [APPDELEGATE stopspinner:self.view];
        [APPDELEGATE stopspinner:popview];
      //  Internetalert.tag=12345;
        [Internetalert show];
    });
    
    
}

-(void)buttonindex2
{
    {
        SettingsViewController *settingVC;
        if (myappDelegate.isiphone5)
        {
            settingVC=[[SettingsViewController alloc]initWithNibName:@"SettingsViewController" bundle:nil];
            
        }
        else
        {
            settingVC=[[SettingsViewController alloc]initWithNibName:@"SettingsViewController_iphone4" bundle:nil];
        }
        if(isnew==YES)
        {
            //paypaltxtField.text=@"";
        }
        [self.navigationController pushViewController:settingVC animated:YES];
        
    }
}
//Verification of  address
-(void)addressVerification
{
    NSMutableDictionary *orderDict = [[NSMutableDictionary alloc]init];
    [orderDict setValue:self.address1TextField.text forKey:@"Address1"];
    [orderDict setValue:self.address2TextField.text forKey:@"Address2"];
    [orderDict setValue:self.cityTextField.text forKey:@"City"];
    [orderDict setValue:self.stateTextField.text forKey:@"State"];
    [orderDict setValue:[NSString stringWithFormat:@"%@",self.zipcodeTextField.text] forKey:@"Zipcode"];
    profileRepos.delegate=self;
    [profileRepos addressVerification:orderDict];
    
}
//Success response for address Verrification
-(void)successResponseforaddressVerification:(NSMutableDictionary *)responsedict
{
        dispatch_async( dispatch_get_main_queue(), ^{
    NSLog(@"responsedict----%@_____",responsedict);
    if([[responsedict valueForKey:@"message"]isEqualToString:@"success"])
    {
         [self performSelector:@selector(postWithDelay) withObject:nil afterDelay:0.1];
    }
    else if([[responsedict valueForKey:@"message"]isEqualToString:@"PO Box conatin address are not allowed"])
            {
                [APPDELEGATE stopspinner:self.view];

              [AppDelegate alertmethod:appname :@"PO Box conatin address are not allowed"];
            }
    else
    {
        [AppDelegate alertmethod:appname :@"Invalid address"];
    }
            [myappDelegate stopspinner:self.view];

        });
}
@end
