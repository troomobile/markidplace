//
//  MyBrandsViewController.h
//  MarkidPlace
//
//  Created by Varma Bhupatiraju on 15/09/14.
//  Copyright (c) 2014 stellentmac1. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MybrandsCustomCell.h"
@interface MyBrandsViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>

@property (nonatomic,strong) IBOutlet UINavigationBar *navbar;
@property (nonatomic,strong) IBOutlet  UILabel * mybrndstitlelbl ;
@property (nonatomic,strong) IBOutlet  UITableView * brandtblview;
@property (nonatomic,strong) IBOutlet  UITableView * dropdowntblview;
@property (nonatomic,strong) IBOutlet  UIView * popview;
@property (nonatomic,strong) IBOutlet  MybrandsCustomCell * brndcell;
@property (nonatomic,strong) IBOutlet  NSMutableArray * brandarray;
@property (nonatomic,strong) IBOutlet  UIButton * updatebtn;
-(IBAction)backAction:(id)sender;
-(IBAction)homeBtnAction:(id)sender;
-(IBAction)brandbtnAction:(id)sender;
-(IBAction)cancelBtnAction:(id)sender;
-(IBAction)doneBtnAction:(id)sender;
@end
