//
//  InviteFriendsViewController.m
//  MarkidPlace
//
//  Created by stellent_mac_05 on 27/06/14.
//  Copyright (c) 2014 stellentmac1. All rights reserved.
//

#import "InviteFriendsViewController.h"

@interface InviteFriendsViewController ()

@end

@implementation InviteFriendsViewController
@synthesize searchbar,navbar,friendslistview,friendscell,contacts,currentarray,friendsArray,defaultfriendsarray,titlelbl,contactcell,contactslistview,contactview,tempArray,collectionArray,smsbutton,Facebookbutton,emailbutton,sharebutton,comparestr;
@synthesize account,accountStore,acessToken;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

#pragma mark View Methods

- (void)viewDidLoad
{
    [super viewDidLoad];
    
   appdelegate=(AppDelegate *)[[UIApplication sharedApplication] delegate];
    // Do any additional setup after loading the view from its nib.

      self.currentarray=[[NSMutableArray alloc]init];
     searchresultarray=[[NSMutableArray alloc]init];
   
    collectionArray=[[NSMutableArray alloc]init];
    friendsArray=[[NSMutableArray alloc]init];
    tempArray=[[NSMutableArray alloc]init];
    finalinvitearray=[[NSMutableArray alloc]init];
    facebookarray=[[NSMutableArray alloc]init];
    imagesArray=[[NSMutableArray alloc]init];
    if ([comparestr isEqualToString:@"Email"])
    {
       [self emailbutton:nil];
    }
    else if([comparestr isEqualToString:@"Facebook"])
    {
        [self performSelector:@selector(Facebookbutton:)];
        
    }
    else
    {
        [self smsbutton:nil];
    }
    //[self setupIndexData];
    
    navbar.frame = CGRectMake(0, 0, 320, 64);
    [navbar setBackgroundImage:[UIImage imageNamed:@"navbar.png"] forBarMetrics:UIBarMetricsDefault];
    
    titlelbl.font =[UIFont fontWithName:@"Monroe" size:18];
    self.smsbutton.titleLabel.font = [UIFont fontWithName:@"Muli" size:15];
    self.emailbutton.titleLabel.font = [UIFont fontWithName:@"Muli" size:15];
    self.Facebookbutton.titleLabel.font = [UIFont fontWithName:@"Muli" size:15];
     self.sharebutton.titleLabel.font = [UIFont fontWithName:@"Muli" size:15];
    

    [searchbar setShowsCancelButton:NO animated:YES];
    friendslistview.hidden=NO;
    
   }
#pragma mark TableView delegate methods.

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
    
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(tableView==friendslistview)
    {
        
        if([sepstr isEqualToString:@"Email"]||[sepstr isEqualToString:@"SMS"])
        {
            if(!issearching)
            {
                if ([friendsArray count]==[currentarray count])
                {
                    return [currentarray count];
                }
                else
                {
                    return [currentarray count]+1;
                }
            }
            else
            {
                return [searchresultarray count];
            }
        }
        else if([sepstr isEqualToString:@"Facebook"])
        {
           return facebookarray.count;
            
        }
    }
    else
    {
        return tempArray.count;
    }
    return 0;
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
 
       Person *personObject;
    
    // Loading the friendslistview
    
    if(tableView==friendslistview)
    {
    static NSString *cellidentifier=@"cell";
    InvitefriendCell *tbcell=(InvitefriendCell *)[tableView dequeueReusableCellWithIdentifier:cellidentifier];
    if (tbcell==nil)
    {
        [[NSBundle mainBundle]loadNibNamed:@"InvitefriendCell" owner:self options:nil];
        tbcell=self.friendscell;
    }

        if([sepstr isEqualToString:@"Email"])
        {
            tbcell.contactimgview.hidden=YES;
            tbcell.namelbl.frame=CGRectMake(27, 3, 177, 44);
        }
        else if([sepstr isEqualToString:@"SMS"])
        {
            tbcell.contactimgview.hidden=YES;
            tbcell.namelbl.frame=CGRectMake(27, 3, 177, 44);
        }
        else if([sepstr isEqualToString:@"Facebook"])
        {
            tbcell.contactimgview.hidden=NO;
            tbcell.namelbl.frame=CGRectMake(63, 3, 177, 44);
            tbcell.checkImageView.hidden=YES;
            NSLog(@"the Array is in cell for img response is.%d...",facebookarray.count);
            
            facebookobj=[facebookarray objectAtIndex:indexPath.row];
            tbcell.namelbl.text=facebookobj.namestr;
            NSLog(@"the facebook uid count is in cell for row is  %d",facebookarray.count);
            
            NSURL *pictureURL = [NSURL URLWithString:[NSString stringWithFormat:@"https://graph.facebook.com/%@/picture?type=large",facebookobj.uidstr]];
            
             [tbcell.contactimgview setImageWithURL:pictureURL             placeholderImage:[UIImage imageNamed:@"noimage.png"] options:SDWebImageCacheMemoryOnly];
            
            NSLog(@"the picture url is %@",pictureURL);
          //  tbcell.checkImageView.image=[UIImage imageWithData:[NSData dataWithContentsOfURL:pictureURL]];
            
            
        }
    if(issearching==YES)
    {
    personObject=[searchresultarray objectAtIndex:indexPath.row];
    }
    else
    {
        if (currentarray.count>indexPath.row)
        {
            personObject=[currentarray objectAtIndex:indexPath.row];
        }
        else{
    
    tbcell.namelbl.text=@"Loading more...";
        
            [self loadother];
        return tbcell;
        }
        
    }
        if(personObject.invitenool==YES)
        {
           tbcell.checkImageView.image=[UIImage imageNamed:@""];
         
            NSLog(@"the collection array is %d",collectionArray.count);
        }
        else
        {

            tbcell.checkImageView.image=[UIImage imageNamed:@"tick.png"];
         }
        if([sepstr isEqualToString:@"Email"]||[sepstr isEqualToString:@"SMS"])
        {
    tbcell.namelbl.text=personObject.fullName;
    }
    tbcell.namelbl.font = [UIFont fontWithName:@"Muli" size:15];

         return tbcell;

    }
    // Loading the contactlistview
    else
    {
        static NSString *cellidentifier=@"cell";
        ContactsCell*subcell=(ContactsCell *)[tableView dequeueReusableCellWithIdentifier:cellidentifier];
        if (subcell==nil)
        {
            [[NSBundle mainBundle]loadNibNamed:@"ContactsCell" owner:self options:nil];
            subcell=self.contactcell;
        }
   //   if(isSMS==YES)
        {
            
        subcell.contactlbl.text=[tempArray objectAtIndex:indexPath.row];
            subcell.selectedImageView.tag=indexPath.row;
        subcell.contactlbl.font = [UIFont fontWithName:@"Muli" size:15];
            }
        
        if (issearching)
        {
            personObject=[searchresultarray objectAtIndex:tableView.tag];
        }
        else{
             personObject=[currentarray objectAtIndex:tableView.tag];
        }
        
       
        if ([myappDelegate isnullorempty:personObject.homeEmail] || ![[tempArray objectAtIndex:indexPath.row] isEqualToString:personObject.homeEmail])
        {
            subcell.selectedImageView.image=[UIImage imageNamed:@"circle@2x.png"];
        }
        else if (![myappDelegate isnullorempty:personObject.homeEmail] &&[[tempArray objectAtIndex:indexPath.row] isEqualToString:personObject.homeEmail] )
        {
            subcell.selectedImageView.image=[UIImage imageNamed:@"greencircle@2x.png"];
        }
          return subcell;
    }
   
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(tableView==friendslistview)
    {
        return 50.0;
    }
    else
    {
        return 45.0;
    }
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    Person *personObject;
    
    if(tableView==friendslistview)
    {
        // condition is used for loading  contacts in phone book
        if([sepstr isEqualToString:@"Email"]||[sepstr isEqualToString:@"SMS"])
        {
        if(!issearching)
        {
            personObject=[currentarray objectAtIndex:indexPath.row];
        }
        else
        {
            personObject=[searchresultarray objectAtIndex:indexPath.row];
        }
          if(isSMS==YES)
        {
              NSLog(@"the contacts count is-------%@",personObject.contactphonenoArray);
            
           if(personObject.contactphonenoArray.count>1)
            {
                tempArray=personObject.contactphonenoArray;
                contactslistview.tag=indexPath.row;
                [self.view addSubview:contactview];
            }
            else
            {
                if(personObject.invitenool==YES)
                {
                [finalinvitearray addObject:personObject.homeEmail];
                    personObject.invitenool=NO;
                }
                else
                {
                    personObject.invitenool=YES;
                }
            }
        }
        else
        {
        NSLog(@"the contact count is-%lu",(unsigned long)personObject.contactEmailsArray.count);
       
            if(personObject.contactEmailsArray.count>1)
            {
                //  personObject.invitenool=NO;
                tempArray=personObject.contactEmailsArray;
                contactslistview.tag=indexPath.row;

                [self.view addSubview:contactview];
            }
            else
            {
                if(personObject.invitenool==YES)
                {
                    {
                        NSLog(@"emails of person %@",personObject.homeEmail);
                    [finalinvitearray addObject:personObject.homeEmail];
                    personObject.invitenool=NO;
                    }
                }
                else
                {
                    {
                    [finalinvitearray removeObject:personObject.homeEmail];
                    
                    personObject.invitenool=YES;
                    }
                }
            }
        }
        }
        // This condition is used for loading the Facebook Contacts.
     else if([sepstr isEqualToString:@"Facebook"])
        {
            NSLog(@"the facebook array count is %d",facebookarray.count);
            facebookobj =[facebookarray objectAtIndex:indexPath.row];
            NSLog(@"the facebook array count is %d",facebookarray.count);
            NSLog(@"the facebook obj count is %@",facebookobj);

            
            NSString *friendId = [NSString stringWithFormat:@"%@",facebookobj.uidstr];
            
            
            NSMutableDictionary *params1;
            
            
            params1= [NSMutableDictionary dictionaryWithObjectsAndKeys: @"Hey! Check out marKIDplace", @"name", @"770236303030542", @"app_id", friendId, @"to", acessToken, @"access_token",nil];
      
                [FBWebDialogs presentFeedDialogModallyWithSession:nil parameters:params1 handler:
                 ^(FBWebDialogResult result, NSURL *resultURL, NSError *error)
                 {
                      NSLog(@"result ======%@",resultURL);
                     if (error)
                     {
                           NSLog(@"error ======%@",error);
                     }
                     else
                     {
                         if (result == FBWebDialogResultDialogNotCompleted)
                         {
                              NSLog(@"User canceled story publishing.");
                         }
                         else
                         {
                             NSDictionary *urlParams = [self parseURLParams:[resultURL query]];
                             if (![urlParams valueForKey:@"post_id"])
                             {
                                  NSLog(@"User canceled story publishing.");
                             }
                             else
                             {
                                 double delayInSeconds = 0.5;
                                 dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
                                 dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
                                     
                                     
                                     UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:appname message:[NSString stringWithFormat:@"Successfully invited"] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                                     
                                     [alertView show];
                                     
                                     
                                 });
                                 
                                 
                             }
                         }
                     }
                 }];
           
           // });
            
        }
    }
    else if(tableView==contactslistview)
    {
        
        
        if(!issearching)
        {
            personObject=[currentarray objectAtIndex:tableView.tag];
        
          
        }
        else
        {
            personObject=[searchresultarray objectAtIndex:tableView.tag];
        }
        
        if ([myappDelegate isnullorempty:personObject.homeEmail] || ![[tempArray objectAtIndex:indexPath.row] isEqualToString:personObject.homeEmail])
        {
            if ([myappDelegate isnullorempty:personObject.homeEmail])
            {
                [finalinvitearray addObject:[tempArray objectAtIndex:indexPath.row]];
            }
            else
            {
            [finalinvitearray removeObject:personObject.homeEmail];
                [finalinvitearray addObject:[tempArray objectAtIndex:indexPath.row]];
            }
             personObject.homeEmail=[tempArray objectAtIndex:indexPath.row];
            
            personObject.invitenool=NO;
        }
        else if (![myappDelegate isnullorempty:personObject.homeEmail] &&[[tempArray objectAtIndex:indexPath.row] isEqualToString:personObject.homeEmail] )
        {
            personObject.homeEmail=nil;
            personObject.invitenool=YES;
             [finalinvitearray removeObject:[tempArray objectAtIndex:indexPath.row]];
        }
        else
        {
            NSLog(@"some thing hapen ");
        }
        
        NSLog(@"the  savestr is --------------------------%@______",savestr);
   
        NSLog(@"the string homeemail is --------------------------%@______",[tempArray objectAtIndex:indexPath.row]);
         NSLog(@"the string personobj is --------------------------%@______", personObject.homeEmail);
        NSLog(@"the contact indexpath is in select row is ----------%d",indexPath.row);
        
        
        [contactview removeFromSuperview];
     
        }
    
    [friendslistview reloadData];
    [contactslistview reloadData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//This method is uesd to loading the contacts in the  tableview

-(void)adressbook
{
    __block BOOL accessGranted = NO;
    
    ABAddressBookRef m_addressbook = ABAddressBookCreate();
    
    if (ABAddressBookRequestAccessWithCompletion != NULL)
    {
        // we're on iOS 6
        dispatch_semaphore_t sema = dispatch_semaphore_create(0);
        ABAddressBookRequestAccessWithCompletion(m_addressbook, ^(bool granted, CFErrorRef error)
                                                 {
                                                     accessGranted = granted;
                                                     dispatch_semaphore_signal(sema);
                                                 });
        dispatch_semaphore_wait(sema, DISPATCH_TIME_FOREVER);
        //dispatch_release(sema);
    }
    else
    {
        // we're on iOS 5 or older
        accessGranted = YES;
        
    }
    
    if (accessGranted)
    {
        
        sharebutton.hidden=NO;
        allPeople = ABAddressBookCopyArrayOfAllPeople(m_addressbook);
        nPeople = ABAddressBookGetPersonCount(m_addressbook);
        
       // NSArray *allContacts = (__bridge_transfer NSArray *)ABAddressBookCopyArrayOfAllPeople(m_addressbook);
         NSMutableArray *allContacts = (__bridge_transfer NSMutableArray *)ABAddressBookCopyArrayOfAllPeople(m_addressbook);
        NSLog(@"the aray people is %@",allContacts.description);
        
        NSUInteger i = 0;
        
        for (i = 0; i < [allContacts count]; i++)
        {
            Person *person = [[Person alloc] init];
            
            ABRecordRef contactPerson = (__bridge ABRecordRef)allContacts[i];
            
            NSString *firstName = (__bridge_transfer NSString *)ABRecordCopyValue(contactPerson, kABPersonFirstNameProperty);
            
            NSString *lastName =  (__bridge_transfer NSString *)ABRecordCopyValue(contactPerson, kABPersonLastNameProperty);
            
            ABRecordID recordId = ABRecordGetRecordID(contactPerson); // get record id from address book record
            
            person.recordId=[NSString stringWithFormat:@"%d",recordId]; // get record id string from record id
            if ([myappDelegate isnullorempty:firstName])
            {
                person.firstName=@"";
            }
            else
            {
                person.firstName = firstName;
                
                NSLog(@"person.firstName....%@",person.firstName);
            }
            
            if ([myappDelegate isnullorempty:lastName])
            {
                person.lastName=@"";
            }
            else
            {
                person.lastName = lastName;
                NSLog(@"person.lastName....%@",person.lastName);
            }
            
            NSString *fullName = [NSString stringWithFormat:@"%@ %@", person.firstName, person.lastName];
            
            person.fullName = fullName;
            
            if ([person.fullName isEqualToString:@" "])
            {
                
                NSLog(@"empty");
            }
            
            // Get the e-mail addresses as a multi-value property.
            //NSLog(@"emailsRef  emailsRef  emailsRef");

            ABMultiValueRef emailsRef = ABRecordCopyValue(contactPerson, kABPersonEmailProperty);
             //NSLog(@"emailsRef12  emailsRef 12 emailsRef");
            for (int i=0; i<ABMultiValueGetCount(emailsRef); i++)
            {
                
                //NSLog(@"emailsRef32  emailsRef 45 emailsRef");

                CFStringRef currentEmailLabel = ABMultiValueCopyLabelAtIndex(emailsRef, i);
                CFStringRef currentEmailValue = ABMultiValueCopyValueAtIndex(emailsRef, i);
                 if (currentEmailLabel != nil)
                 {
                if (CFStringCompare(currentEmailLabel, kABHomeLabel, 0) == kCFCompareEqualTo)
                {
                   // [contactInfoDict setObject:(__bridge NSString *)currentEmailValue forKey:@"homeEmail"];
                    person.homeEmail=(__bridge NSString *)currentEmailValue;
                    //NSLog(@"person.homeEmail....%@",person.homeEmail);
                    //NSLog(@"person.homeEmail....%@",(__bridge NSString *)currentEmailValue);
                    
                }
                     CFRelease(currentEmailLabel);
                 }
                if (currentEmailLabel != nil)
                {
                if (CFStringCompare(currentEmailLabel, kABWorkLabel, 0) == kCFCompareEqualTo)
                {
                    //[contactInfoDict setObject:(__bridge NSString *)currentEmailValue forKey:@"workEmail"];
                     person.workEmail=(__bridge NSString *)currentEmailValue;
                   // NSLog(@"person.workEmail....%@",person.workEmail);
                   // NSLog(@"person.workEmail....%@",(__bridge NSString *)currentEmailValue);
                }
                    CFRelease(currentEmailLabel);
            }
                    
                CFRelease(currentEmailValue);
            }
            
            if([APPDELEGATE isnullorempty:person.homeEmail] && [APPDELEGATE isnullorempty:person.workEmail])
            {
                
            }
            else
            {
            [APPDELEGATE.emailArray addObject:person];
            }
        }
        
        /*
        [self.friendsArray removeAllObjects];
        [searchresultarray removeAllObjects];
        [currentarray removeAllObjects];
        [finalinvitearray removeAllObjects];
       
        // Here getting the contacts details
        
        for (i = 0; i < [allContacts count]; i++)
        {
            Person *person = [[Person alloc] init];
            
            ABRecordRef contactPerson = (__bridge ABRecordRef)allContacts[i];
            
            NSString *firstName = (__bridge_transfer NSString *)ABRecordCopyValue(contactPerson, kABPersonFirstNameProperty);
            
            NSString *lastName =  (__bridge_transfer NSString *)ABRecordCopyValue(contactPerson, kABPersonLastNameProperty);
            
            
            if ([myappDelegate isnullorempty:firstName])
            {
                person.firstName=@"";
            }
            else
            {
                person.firstName = firstName;
                
                NSLog(@"person.firstName....%@",person.firstName);
            }
            
            if ([myappDelegate isnullorempty:lastName])
            {
                person.lastName=@"";
            }
            else
            {
                person.lastName = lastName;
                 NSLog(@"person.lastName....%@",person.lastName);
                
            }
            
            NSString *fullName = [NSString stringWithFormat:@"%@ %@", person.firstName, person.lastName];
            
            person.fullName = fullName;
            
            if ([person.fullName isEqualToString:@" "])
            {
                break;
            }
            
        // here getting the Email details.
            ABMultiValueRef emails = ABRecordCopyValue(contactPerson, kABPersonEmailProperty);
            
            
                 NSUInteger j = 0;
            
            if(isSMS==NO)
            {
            person.contactEmailsArray=[[NSMutableArray alloc]init];
            for (j = 0; j < ABMultiValueGetCount(emails); j++)
            {
                NSString *email = (__bridge_transfer NSString *)ABMultiValueCopyValueAtIndex(emails, j);
                NSLog(@"the email array is %@",email);
                
                if (ABMultiValueGetCount(emails)>1)
                {
                    [person.contactEmailsArray addObject:email];
                    
                       NSLog(@"check the count here %d ",person.contactEmailsArray.count);
                    
                }
                
                if (j == 0 && ABMultiValueGetCount(emails)==1)
                {
                    
                    person.homeEmail = email;
                }
             }
            }
            else
            {
            ABMultiValueRef contactno = ABRecordCopyValue(contactPerson, kABPersonPhoneProperty);
            if(ABMultiValueGetCount(contactno) > 0)
            {
              //  person.contactEmailsArray=[[NSMutableArray  alloc]init];
                
                
                person.contactphonenoArray=[[NSMutableArray  alloc]init];
                for (CFIndex k=0; k < ABMultiValueGetCount(contactno); k++)
                {
                    CFStringRef contno = ABMultiValueCopyValueAtIndex(contactno, k);
                    
                    NSString* phoneLabel = (__bridge_transfer  NSString*)contno;
                    {
                        if ([myappDelegate isnullorempty:phoneLabel])
                        {
                            
                        }
                        else
                        {
                            
                            NSCharacterSet *notAllowedChars = [[NSCharacterSet characterSetWithCharactersInString:@"0123456789"] invertedSet];
                            NSString *tempphonestr= [[phoneLabel componentsSeparatedByCharactersInSet:notAllowedChars] componentsJoinedByString:@""];
                            
                              [person.contactphonenoArray addObject:phoneLabel];
                             NSLog(@"the adress contacts is ---------%@------------",tempphonestr);
                           // NSLog(@"the adress contacts is ---------%d------------",person.contactphonenoArray.count);
                    
                            if (k == 0&&ABMultiValueGetCount(contactno)== 1)
                            {
                                person.homeEmail = tempphonestr;
                            }
                            
                        }
                           }
                }
                
            }
            
            }
                      person.invitenool=YES;

            if (isSMS)
            {
                if (person.contactphonenoArray.count<1 && [myappDelegate isnullorempty:person.homeEmail])
                {
                    
                }
                
                else
                {
                    [self.friendsArray addObject:person];
                }
                 [appdelegate.contactsArray addObjectsFromArray:self.friendsArray];
            }
            else{
                if (person.contactEmailsArray.count<1 && [myappDelegate isnullorempty:person.homeEmail])
                {
                    
                }
                else
                {
                    [self.friendsArray addObject:person];
                    [appdelegate.emailArray addObject:person];
                    
                }
               

            }
            }
        
        [searchresultarray removeAllObjects];
        
        //********sorting alphabetically
        
                NSSortDescriptor *sorterrrr = [[NSSortDescriptor alloc]initWithKey:@"fullName"
                                                                 ascending:YES selector:@selector(localizedCaseInsensitiveCompare:)];
        
        [friendsArray sortUsingDescriptors:[NSArray arrayWithObject:sorterrrr]];
        
        
        
        [self loadother];
        
        if (friendsArray.count<1)
        {
          //  UIAlertView *eror=[[UIAlertView alloc]initWithTitle:appname message:@"No contacts found." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            //[eror show];
            NSLog(@"No contacts found.....");
            
            
        }
        
        [friendslistview reloadData];
        [myappDelegate stopspinner:self.view ];
        
           }
    else
    {
        
        UIAlertView *eror=[[UIAlertView alloc]initWithTitle:appname message:@"We are unable to access your contacts. Please go to Settings->Privacy->Contacts and make sure marKIDplace is set to ON. " delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [eror show];
        [myappDelegate stopspinner:self.view ];
        
        
    }
         */
    }
}
// This Method used for load the 100  contacts.
-(void)loadother
{
    NSLog(@"array count **********%lu",(unsigned long)friendsArray.count);
    if ([self.friendsArray count]>100)
    {
            if (startvalue==0)
        {
            startvalue=100;
            
            for (int i=0; i<100; i++) {
                [self.currentarray addObject:[friendsArray objectAtIndex:i]];
            }
           
        }
        else
        {
            if ([self.friendsArray count]>(startvalue+100)) {
                for (int i=startvalue; i<(startvalue+100); i++) {
                    [self.currentarray addObject:[friendsArray objectAtIndex:i]];
                }
                startvalue=startvalue+100;
             }
            else
            {
                for (int i=startvalue; i<[friendsArray count]; i++) {
                    [self.currentarray addObject:[friendsArray objectAtIndex:i]];
                }
                startvalue=friendsArray.count;
            }
        }
           }
    else
    {
        [self.currentarray addObjectsFromArray:self.friendsArray];
        }
    [self.friendslistview reloadData];
}
    
#pragma mark Search bar Methods

#pragma mark Serach Bar Delegate Methods Start
//------------SearchBar methods---------------------//

-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    if (searchBar.text.length<2)
    {
        UIAlertView *somealt=[[UIAlertView alloc]initWithTitle:@"Markidplace" message:@"Please enter atleast 2 characters to search." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [somealt show];
        
    }
    else
    {
        issearching=YES;
        [searchBar resignFirstResponder];
        [searchbar setShowsCancelButton:NO animated:YES];
        
        if(searchresultarray.count>0)
        {
            [searchresultarray removeAllObjects];
        }
              dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.2 * NSEC_PER_SEC), dispatch_get_current_queue(), ^{
            
            for(int i=0;i<[friendsArray count];i++)
            {
                NSString *username=[[friendsArray objectAtIndex:i] valueForKey:@"fullName"];
                NSString *nameStr=[username lowercaseString];
                
                //  NSLog(@"name str is....%@",nameStr);
                if([nameStr hasPrefix:[searchBar.text lowercaseString]] || [nameStr hasPrefix:[searchBar.text uppercaseString]])
                {
                    //NSLog(@" condition satisfied");
                    [searchresultarray addObject:[friendsArray objectAtIndex:i] ];
                }
            }
                       [friendslistview reloadData];
                   });
        [friendslistview reloadData];
    }
}
- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

-(void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    issearching=NO;
    searchBar.text=@"";
    [searchbar setShowsCancelButton:NO animated:YES];

    [searchBar resignFirstResponder];
    
    [friendslistview reloadData];
 }
-(void) searchBarTextDidBeginEditing:(UISearchBar *)searchBar
{
 
    [searchBar setShowsCancelButton:YES animated:YES];
}

#pragma mark ButtonActions
-(IBAction)donebutton:(id)sender
{
    [contactview removeFromSuperview];
}
// navigation back button Action
-(IBAction)backbutton:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
// Action performs the share the email or contacts.
-(IBAction)sharebuttonaction:(id)sender
{
    if(finalinvitearray.count>0)
    {
        // This condition for Emails.

    if(isSMS==NO)
    {
        
        MFMailComposeViewController *mailComposer = [[MFMailComposeViewController alloc] init];
        
        if(mailComposer!= nil)
        {
            mailComposer.mailComposeDelegate = self;
            [mailComposer setToRecipients:finalinvitearray];
            [mailComposer setSubject:appname];
            //[mailComposer setMessageBody:@"" isHTML:YES];
            mailComposer.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
            [mailComposer setMessageBody:kinvitaion isHTML:NO];
             mailComposer.navigationController.navigationBar.titleTextAttributes = [NSDictionary dictionaryWithObject:[UIColor blackColor] forKey:UITextAttributeTextColor];
            // mailComposer.navigationBar.tintColor=[UIColor clearColor];
            [self presentViewController:mailComposer animated:YES completion:nil];
        }
        
        
    }
       // This condition for contacts
    else if(isSMS==YES)
        {
#if target_iphone_device
            
            NSArray *recipients = finalinvitearray;
            
            NSString * message = kinvitaion;
            
            MFMessageComposeViewController *messageController = [[MFMessageComposeViewController alloc] init];
            messageController.messageComposeDelegate = self;
            [messageController setRecipients:recipients];
            [messageController setBody:message];
            
            [self presentViewController:messageController animated:YES completion:nil];
            
#endif
        }
    }
    else
    {
        [AppDelegate alertmethod:appname :@"Please select contact."];
    }
}
//  Performs Facebook button Action
-(IBAction)Facebookbutton:(id)sender
{
    [facebookarray removeAllObjects];
    sharebutton.hidden=YES;
   Facebookbutton.layer.borderWidth = 1;
   Facebookbutton.layer.cornerRadius=1.0f;
   Facebookbutton.layer.borderColor =[[UIColor whiteColor] CGColor];  emailbutton.layer.borderColor=[[UIColor clearColor] CGColor];
       smsbutton.layer.borderColor = [[UIColor clearColor] CGColor];
    
    sepstr=@"Facebook";

    
    ////////////
    {
       // dispatch_async(dispatch_get_main_queue(), ^{
            
            // [self.spinner startAnimating];
            [myappDelegate startspinner:self.view];
            //self.view.userInteractionEnabled = NO;
            
            
            
            NSArray *permissions =
            [NSArray arrayWithObjects:@"email",@"user_photos",@"read_stream", nil];
            NSDictionary * dict=[NSDictionary dictionaryWithObjectsAndKeys:@"770236303030542",ACFacebookAppIdKey,permissions,ACFacebookPermissionsKey,ACFacebookAudienceEveryone,ACFacebookAudienceKey, nil];
            
            accountStore = [[ACAccountStore alloc]init];
            ACAccountType *accountType = [accountStore accountTypeWithAccountTypeIdentifier:ACAccountTypeIdentifierFacebook];
            
            
            
            [accountStore requestAccessToAccountsWithType:accountType options:dict completion:^(BOOL granted, NSError *error)
             {
                 if (granted == YES)
                 {
                     //  [self performSelector:@selector(startSpinner) withObject:nil afterDelay:0.1];
                     
                     NSLog(@"Grantedddddddddddd");
                     NSArray *accountsArray = [accountStore accountsWithAccountType:accountType];
                     
                     ////NSLog(@"accountsArray =====%@",accountsArray);
                     
                     if ([accountsArray count] > 0)
                     {
                         account = [accountsArray objectAtIndex:0];
                         
                         //  ////NSLog(@"aaaaaaaaaaaa%@",Account);
                         
                         ACAccountCredential *fbCredential = [account credential];
                         
                         acessToken = [NSString stringWithFormat:@"%@",fbCredential.oauthToken];
                         
                         // ////NSLog(@"acessToken   %@",acessToken);
                         
                         NSDictionary *parameters = @{@"access_token": acessToken};
                         
                         NSURL *requestURL = [NSURL URLWithString:@"https://graph.facebook.com/me"];
                         
                         SLRequest *postRequest = [SLRequest requestForServiceType:SLServiceTypeFacebook requestMethod:SLRequestMethodGET URL:requestURL parameters:parameters];
                         
                         postRequest.account = account;
                         
                         [postRequest performRequestWithHandler:
                          ^(NSData *responseData, NSHTTPURLResponse
                            *urlResponse, NSError *error)
                          {
                              
                              ////NSLog(@"response ======%@",urlResponse);
                              
                              NSDictionary *dataSourceDict= [NSJSONSerialization JSONObjectWithData:responseData options:NSJSONReadingMutableLeaves  error:&error];
                              
                              ////NSLog(@"dataSourceDict =====%@",dataSourceDict);
                              
                              if([dataSourceDict objectForKey:@"error"]!=nil)
                              {
                                  [self attemptRenewCredentials];
                              }
                              else
                              {
                       
                                  facebookID = dataSourceDict[@"id"];
                      
                                   [self getFBFriends];
                              }
                              
                              
                          }];
                     }
                     else{
                         [myappDelegate stopspinner:self.view];
                     }
                     self.view.userInteractionEnabled = YES;
                 }
                 else
                 {
                     
                     NSLog(@"error in FB========> %@",error);
                     //[myappDelegate stopspinner:self.view];
                     //[self.spinner stopAnimating];
                     self.view.userInteractionEnabled = YES;
                     
                     UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"No Facebook Accounts" message:@"For some reason, Facebook is not recognizing marKIDplace. To fix, please tap your phone's Settings icon, scroll down to Facebook, where you should see your Facebook login credentials (if you don't, you need to log in) and there you should see a marKIDplace icon also (if you don't, you need to refresh the permissions again). Toggle marKIDplace to the \"ON\" position and you should be connected." delegate:self cancelButtonTitle:@"OK"  otherButtonTitles:nil];
                     alert.tag = 11;
                     [alert show];
                 }
             }];
        //});
        
    }
    
     [self sendRequest];
    //[AppDelegate alertmethod:appname :@"Under Implementation."];
   // [friendslistview reloadData];

}
- (void)sendRequest {
    // Display the requests dialog
    [FBWebDialogs
     presentRequestsDialogModallyWithSession:nil
     message:kinvitaion
     title:nil
     parameters:nil
     handler:^(FBWebDialogResult result, NSURL *resultURL, NSError *error) {
         if (error) {
             // Error launching the dialog or sending the request.
             NSLog(@"Error sending request.");
         } else {
             if (result == FBWebDialogResultDialogNotCompleted) {
                 // User clicked the "x" icon
                 NSLog(@"User canceled request.");
             } else {
                 // Handle the send request callback
                 NSDictionary *urlParams = [self parseURLParams:[resultURL query]];
                 if (![urlParams valueForKey:@"request"]) {
                     // User clicked the Cancel button
                     NSLog(@"User canceled request.");
                 } else {
                     // User clicked the Send button
                     NSString *requestID = [urlParams valueForKey:@"request"];
                     NSLog(@"Request ID: %@", requestID);
                 }
             }
         }
     }];
}
//Action Performs the  loading emails in the tableView.
-(IBAction)emailbutton:(id)sender
{
    [self emailbtn];
    friendslistview.hidden=NO;
    [finalinvitearray removeAllObjects];
    [APPDELEGATE.emailArray removeAllObjects];
   

    [myappDelegate startspinner:self.view];
    //[self performSelector:@selector(adressbook) withObject:nil afterDelay:0];
    [self adressbook];
    sepstr=@"Email";
    [friendslistview reloadData];

   }

//Action Performs the  loading contacts in the tableView.
-(IBAction)smsbutton:(id)sender
{
    smsbutton.layer.borderWidth = 1;
    smsbutton.layer.cornerRadius=1.0f;
    smsbutton.layer.borderColor =[[UIColor whiteColor] CGColor];
    emailbutton.layer.borderColor=[[UIColor clearColor] CGColor];
    Facebookbutton.layer.borderColor = [[UIColor clearColor] CGColor];
    isSMS=YES;
    friendslistview.hidden=NO;
    //[finalinvitearray removeAllObjects];

    [myappDelegate startspinner:self.view];
  [self performSelector:@selector(adressbook) withObject:nil afterDelay:0.1];
    sepstr=@"SMS";
    [friendslistview reloadData];
    
}

#pragma mark  Message composer delegate method.
- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult) result
{
    switch (result) {
        case MessageComposeResultCancelled:
            break;
            
        case MessageComposeResultFailed:
        {
            /*
            UIAlertView *warningAlert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Oups, error while sendind SMS!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [warningAlert show];
            break;
             */
        }
            
        case MessageComposeResultSent:
            break;
            
        default:
            break;
    }
    
    [self dismissViewControllerAnimated:YES completion:nil];
}
#pragma mark  Mail composer delegate method.

-(void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    if ( result == MFMailComposeResultFailed )
    {
        // Sending failed - display an error message to the user.
        /*     NSString* message = [NSString stringWithFormat:@"Error sending email '%@'. Please try again, or cancel the operation./Users/varmabhupatiraju/Documents/Ajay/TabBarproject/TabBarproject/ThirdViewController.m", [error localizedDescription]];
         UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@"Error Sending Email" message:message delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
         [alertView show];*/
        
    }else
        [self dismissViewControllerAnimated:YES completion:^{     }];
    
}
// This  Method is used for setting  border to the button
-(void)emailbtn
{
    emailbutton.layer.borderWidth = 1;
    emailbutton.layer.cornerRadius=1.0f;
    emailbutton.layer.borderColor =[[UIColor whiteColor] CGColor];
    Facebookbutton.layer.borderColor=[[UIColor clearColor] CGColor];
    smsbutton.layer.borderColor = [[UIColor clearColor] CGColor];
    isSMS=NO;
   
}

//  These Methods  is used for Getting the Facebook friends.
-(void)getFBFriends
{
    @try
    {
        
        NSString *urlStr =[NSString stringWithFormat:@"https://graph.facebook.com/%@/fql?q=SELECT uid, name FROM user WHERE uid IN (SELECT uid2 FROM friend WHERE uid1 = me()) ORDER BY name&access_token=%@&limit=10000&offset=0",facebookID,acessToken];
        
        
        urlStr = [urlStr stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        
        
        NSURL *url = [NSURL URLWithString:urlStr];
        
         NSLog(@"postbloopdata url====%@",url);
        
        formRequest = [ASIFormDataRequest requestWithURL:url];
        
        [formRequest setDelegate:self];
        [formRequest setTimeOutSeconds:5000];
        [formRequest startAsynchronous];
    }
    @catch (NSException *exception)
    {
        @throw exception;
    }
}
- (void)requestFailed:(ASIHTTPRequest *)request
{
    [request cancel];
    
    request=nil;
   // [delegate failedtoConnect];
}
-(void)requestFinished:(ASIHTTPRequest *)request
{
    NSError *error1 = [request error];
    if (!error1)
    {
        NSString *response = [request responseString];
        
        // NSLog(@"response =====%@",response);
        
        NSDictionary *mainDict = [response JSONValue];
        
        // NSLog(@"*** mainDict *** %@",mainDict);
        
        [self getResponse:mainDict];
        
    }
}
-(void)getResponse:(NSDictionary *)mainDict
{
    NSArray *mainArray = [[NSArray alloc] init];
    
    mainArray = mainDict[@"data"];
    
      NSLog(@"main array ====%@ count ===%d",mainArray,mainArray.count);
    
    NSMutableDictionary *subDict =[[NSMutableDictionary alloc] init];
    
    for (int i = 0; i<mainArray.count; i++)
    {
        facebookobj=[[Facebookobject alloc]init];
        subDict = [mainArray objectAtIndex:i];
        NSLog(@"the subDict contacs are %@",subDict);

        NSString *nameStr = subDict[@"name"];
        NSLog(@"the nameStr contacs are %@",nameStr);
        
       facebookobj.namestr=nameStr;
        //NSLog(@"the namestr contacs are %@",facebookobj.namestr);
        
           NSString *idStr = subDict[@"uid"];
              facebookobj.uidstr=idStr;
        NSLog(@"the idStr contacs are %@",facebookobj.uidstr);
        
        [facebookarray addObject:facebookobj];

    }
        [friendslistview reloadData];
    NSLog(@"the facebook array  count is %d",facebookarray.count);

       // [self.imagesArray addObject:idStr];
        
       /*
        [self.namesArray addObject:nameStr];
        
        NSString *idStr = subDict[@"uid"];
        [self.imagesArray addObject:idStr];
    }
    
    [self.fbnamesArray addObjectsFromArray:self.namesArray];
    [self.fbimagesArray addObjectsFromArray:self.imagesArray];
    
    [self.searchresultTableView reloadData];
    */
    [myappDelegate stopspinner:self.view];

}

-(void)attemptRenewCredentials
{
    [self.accountStore renewCredentialsForAccount:(ACAccount *)account completion:^(ACAccountCredentialRenewResult renewResult, NSError *error){
        if(!error)
        {
            switch (renewResult) {
                case ACAccountCredentialRenewResultRenewed:
                    ////NSLog(@"Good to go");
                    [self Facebookbutton:self];
                    break;
                case ACAccountCredentialRenewResultRejected:
                    ////NSLog(@"User declined permission");
                    [self attemptRenewCredentials];
                    break;
                case ACAccountCredentialRenewResultFailed:
                    ////NSLog(@"non-user-initiated cancel, you may attempt to retry");
                    [self attemptRenewCredentials];
                    break;
                default:
                    break;
            }
            
        }
        else{
            //handle error
            ////NSLog(@"error from renew credentials%@",error);
            [self Facebookbutton:self];
            // [self attemptRenewCredentials];
        }
    }];
}


- (NSDictionary*)parseURLParams:(NSString *)query {
    NSArray *pairs = [query componentsSeparatedByString:@"&"];
    NSMutableDictionary *params2 = [[NSMutableDictionary alloc] init];
    for (NSString *pair in pairs) {
        NSArray *kv = [pair componentsSeparatedByString:@"="];
        NSString *val =
        [kv[1] stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        params2[kv[0]] = val;
    }
    return params2;
}
-(void)internetConnection
{
    UIAlertView *Internetalert=[[UIAlertView alloc]initWithTitle:appname message:@"Internet connection not available" delegate:self cancelButtonTitle:nil otherButtonTitles:@"Ok", nil];
    [Internetalert show];
    [appdelegate stopspinner:self.view];
    
}
@end
