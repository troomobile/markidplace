//
//  myaddressmodelobject.h
//  MarkidPlace
//
//  Created by Varma Bhupatiraju on 18/09/14.
//  Copyright (c) 2014 stellentmac1. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface myaddressmodelobject : NSObject
@property(nonatomic,strong) NSString *address1str;
@property(nonatomic,strong) NSString *address2str;
@property(nonatomic,strong) NSString *citystr;
@property(nonatomic,strong) NSString *statestr;
@property(nonatomic,strong) NSString *zipstr;
@end
