//
//  SuggestionsViewController.m
//  MarkidPlace
//
//  Created by stellentmac1 on 5/21/14.
//  Copyright (c) 2014 stellentmac1. All rights reserved.
//
#import "HomeViewController.h"
#import "ProfileViewController.h"

@interface HomeViewController ()

@end

@implementation HomeViewController
@synthesize navbar,itemLabel1,itemLabel2,itempriceLabel1,itempriceLabel2,oldpriceLabel1,oldpriceLabel2,myAccountLabel,myClosetLabel,myfeedLabel,mywishlistLabel,shopallLabel,sellLabel,cartcountLabel;
@synthesize menuBtn,menuview;
@synthesize itemscrollview,myfeedImageview,mySuggestionsImageview,filterImageview;

@synthesize cartview,itemsarray;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
#pragma mark view methods
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
   // myappDelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    self.navbar.frame = CGRectMake(0, 0, 320, 64);
    [self.navbar setBackgroundImage:[UIImage imageNamed:@"navbar.png"] forBarMetrics:UIBarMetricsDefault];
    
    viewtitlelbl.font = [AppDelegate navtitlefont];
    // itemsarray=[NSMutableArray new];
    
    self.itemLabel1.font = [UIFont fontWithName:@"Muli" size:13];
    self.itemLabel2.font = [UIFont fontWithName:@"Muli" size:13];
    self.itempriceLabel1.font = [UIFont fontWithName:@"Muli" size:13];
    self.itempriceLabel1.font = [UIFont fontWithName:@"Muli" size:13];
    self.itempriceLabel2.font = [UIFont fontWithName:@"Muli" size:13];
    self.oldpriceLabel1.font = [UIFont fontWithName:@"Muli" size:13];
    self.oldpriceLabel2.font = [UIFont fontWithName:@"Muli" size:13];
    self.myAccountLabel.font = [UIFont fontWithName:@"Muli" size:15];
    self.myClosetLabel.font = [UIFont fontWithName:@"Muli" size:15];
    self.myfeedLabel.font = [UIFont fontWithName:@"Muli" size:15];
    self.mywishlistLabel.font = [UIFont fontWithName:@"Muli" size:15];
    self.shopallLabel.font = [UIFont fontWithName:@"Muli" size:15];
    self.sellLabel.font = [UIFont fontWithName:@"Muli" size:15];
    self.cartcountLabel.font = [UIFont fontWithName:@"Muli" size:11];
    
    
    
    
    screenSize = [[UIScreen mainScreen]bounds];
    
    if (myappDelegate.isiphone5)
    {
        menuBtn.frame=CGRectMake(131,470, 58, 59);
    }
    else
    {
        menuBtn.frame=CGRectMake(131,355, 58, 59);
    }
    
    
    self.view.userInteractionEnabled=YES;
    
    [self.view addSubview:menuview];
    [self.view addSubview:menuBtn];
    
    itemscrollview.contentSize=CGSizeMake(itemscrollview.frame.size.width,itemscrollview.frame.size.height+100);
    
    myappDelegate.useridstring=[myappDelegate getStringFromPlist:@"userid"];
    
    NSLog(@"check the home scree user id  %@ ",myappDelegate.useridstring);
    
}
- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}
-(void)viewWillAppear:(BOOL)animated
{
    
    [myaccountImgView setImage:[UIImage imageNamed:@"myaccount_off.png"]];
    [myclosetImgView setImage:[UIImage imageNamed:@"mycloset_off.png"]];
    [myfeedImgView setImage:[UIImage imageNamed:@"myfeed_off.png"]];
    [mywishlistImgView setImage:[UIImage imageNamed:@"mywishlist_off.png"]];
    [shopAllImgView setImage:[UIImage imageNamed:@"shopall_on.png"]];
    [sellImgView setImage:[UIImage imageNamed:@"sell_off.png"]];
    
    if (myappDelegate.cartGlobalarray.count>0)
    {
        cartcountLabel.text=[NSString stringWithFormat:@"%d",myappDelegate.cartGlobalarray.count];
        cartview.hidden=NO;
        
        
    }
    else{
        cartview.hidden=YES;
    }
    
    //  [appdelegate startspinner:self.view];
      registrationRep=[[RegistrationRepository alloc ]init];
    
     registrationRep.delegate=self;
    
    //  [registrationRep getdata:nil :@"getallLikes" :@"GET"];
    [self createscrollview];
    
}
-(void)viewDidDisappear:(BOOL)animated
{
    for (ASIHTTPRequest *req in ASIHTTPRequest.sharedQueue.operations)
    {
        [req cancel];
        [req setDelegate:nil];
    }

}
-(void)viewWillDisappear:(BOOL)animated
{
    for (ASIHTTPRequest *req in ASIHTTPRequest.sharedQueue.operations)
    {
        [req cancel];
        [req setDelegate:nil];
    }

}
#pragma mark Button Action
//this action performs navigation back buttton action
-(IBAction)backAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
//this action performs pop SuggestionsViewController
-(IBAction)homeBtnAction:(id)sender
{
    
    NSArray *arr=self.navigationController.viewControllers;
    for (UIViewController *childvc in arr) {
        
        if([childvc isKindOfClass:[SuggestionsViewController class]])
        {
            UIViewController *subvc=(SuggestionsViewController *)childvc;
            [self.navigationController popToViewController:subvc animated:YES];
        }
    }
    
}

//it action performance
-(IBAction)myFeedButtonTapped:(id)sender
{
    [self setMenuDown];
    itemscrollview.hidden=NO;
    myfeedImageview.image=[UIImage imageNamed:@"myfeedbtn_on.png"];
    mySuggestionsImageview.image=[UIImage imageNamed:@"mysuggestionbtn@2x"];
    filterImageview.image=[UIImage imageNamed:@"filterbtn_off@2x.png"];
    
    menuBtn.hidden=NO;
    itemscrollview.hidden=NO;
    menuview.hidden=NO;
}
//
-(IBAction)mySuggestionsButtonTapped:(id)sender
{
    [self setMenuDown];
    itemscrollview.hidden=NO;
    myfeedImageview.image=[UIImage imageNamed:@"myfeedbtn_off.png"];
    mySuggestionsImageview.image=[UIImage imageNamed:@"mysuggestions@2x"];
    filterImageview.image=[UIImage imageNamed:@"filterbtn_off@2x.png"];
    menuBtn.hidden=YES;
    itemscrollview.hidden=YES;
    menuview.hidden=YES;
    
}
// This Action performs to push the Cart viewController.
-(IBAction)filterButtonTapped:(id)sender
{
    [self setMenuDown];
    
    if ([sender tag]==330)
    {
        if (myappDelegate.cartGlobalarray.count>0)
        {
            // ff
            
            CartViewController *cartVC;
            if (myappDelegate.isiphone5)
            {
                cartVC = [[CartViewController alloc]initWithNibName:@"CartViewController" bundle:nil];
            }
            else
            {
                cartVC = [[CartViewController alloc]initWithNibName:@"CartViewController_iphone4" bundle:nil];
            }
            
            ItemsModelobject *sltobj=[myappDelegate.cartGlobalarray objectAtIndex:0];
            cartVC.itemsModelObj=sltobj;
            [self.navigationController pushViewController:cartVC animated:YES];
            
        }
        else{
            
            [AppDelegate alertmethod:appname :@"Your cart is empty. Please add listing to cart."];
        }
        
    }
    else{
        if (myappDelegate.isiphone5)
        {
            FilterViewController *filterVC=[[FilterViewController alloc] initWithNibName:@"FilterViewController" bundle:nil];
            [self.navigationController pushViewController:filterVC animated:YES];
        }
        else{
            FilterViewController *filterVC=[[FilterViewController alloc] initWithNibName:@"FilterViewController_iphone4" bundle:nil];
            [self.navigationController pushViewController:filterVC animated:YES];
        }
        
        
        
    }
    
}


//Action performs the Animation of the menu view
-(IBAction)menuBtnAction:(id)sender
{
    
    CGRect menu;
    CGRect menuButtonframe;
    
    if (myappDelegate.isiphone5)
    {
        if(ismenu==YES)
        {
            ismenu=NO;
            
            menuButtonframe=CGRectMake(131,470, 58, 59);
            menu=menuview.frame;
            menu.origin.y=menuview.frame.origin.y+600;
            
            self.view.userInteractionEnabled=NO;
        }
        else
        {
            
            menuButtonframe=CGRectMake(131, 250, 58, 59);
            ismenu=YES;
            menu=menuview.frame;
            menu.origin.y=280;
            
        }
        self.view.userInteractionEnabled=YES;
        
    }
    else
    {
        if(ismenu==YES)
        {
            
            
            ismenu=NO;
            
            
            menuButtonframe=CGRectMake(131,355, 58, 59);
            menu=menuview.frame;
            menu.origin.y=menuview.frame.origin.y+482;
            
            self.view.userInteractionEnabled=NO;
            
        }
        else
        {
            
            menuButtonframe=CGRectMake(131, 160, 58, 59);
            ismenu=YES;
            
            menu=menuview.frame;
            menu.origin.y=190;
            
        }
    }
    
    [UIView animateWithDuration:0.5 animations:^{
        
        menuview.frame=menu;
        menuBtn.frame=menuButtonframe;
        
    } completion:^(BOOL finished) {
        
    }];
    
    self.view.userInteractionEnabled=YES;
    
}

-(void)setMenuDown
{
    CGRect menu=menuview.frame;
    CGRect menuButtonframe=menuBtn.frame;
    if (myappDelegate.isiphone5)
    {
        if(ismenu==YES)
        {
            ismenu=NO;
            
            menuButtonframe=CGRectMake(131,470, 58, 59);
            menu=menuview.frame;
            menu.origin.y=menuview.frame.origin.y+600;
            
            self.view.userInteractionEnabled=NO;
        }
        self.view.userInteractionEnabled=YES;
    }
    else
    {
        if(ismenu==YES)
        {
            
            
            ismenu=NO;
            
            
            menuButtonframe=CGRectMake(131,355, 58, 59);
            menu=menuview.frame;
            menu.origin.y=menuview.frame.origin.y+482;
            
            self.view.userInteractionEnabled=NO;
            
        }
    }
    [UIView animateWithDuration:0.5 animations:^{
        
        
        
        menuview.frame=menu;
        menuBtn.frame=menuButtonframe;
        
    } completion:^(BOOL finished) {
        
    }];
    
    self.view.userInteractionEnabled=YES;
    
}
//this action performs pop ProfileViewController
-(IBAction)myAccountAction:(id)sender
{
    [self setMenuDown];
    ProfileViewController *profileVC;
    if (myappDelegate.isiphone5)
    {
        profileVC = [[ProfileViewController alloc]initWithNibName:@"ProfileViewController" bundle:nil];
    }
    else
    {
        profileVC = [[ProfileViewController alloc]initWithNibName:@"ProfileViewController_iphone4" bundle:nil];
    }
   
    [self.navigationController pushViewController:profileVC animated:YES];
}
//
-(IBAction)likebuttonaction:(id)sender
{
    [self setMenuDown];
    //  [appdelegate startspinner:self.view];
    likeindex=[sender tag];
    ItemsModelobject *itemobj=[itemsarray objectAtIndex:[sender tag]];
    
    
    
    if (itemobj.likedbool==YES)
    {
        itemobj.likedbool=NO;
        
        [myappDelegate.allLikesGlobalarray removeObject:[NSString stringWithFormat:@"%@",itemobj.userlistingidstr ]];
        
    }
    else
    {
        [myappDelegate.allLikesGlobalarray addObject:[NSString stringWithFormat:@"%@",itemobj.userlistingidstr ]];
        itemobj.likedbool=YES;
    }
    
    
    
    BOOL breakbool=NO;
    for (int i=0; i<itemscrollview.subviews.count; i++)
    {
        UIView *myv=[itemscrollview.subviews objectAtIndex:i];
        
        if (myv.tag==likeindex)
        {
            if ([myv isKindOfClass:[UIView class]])
            {
                for (UIView *temv in myv.subviews)
                {
                    if ([temv isKindOfClass:[UIButton class]])
                    {
                        UIButton *likebtn=(UIButton *)temv;
                        if (likebtn.frame.size.width==23)
                        {
                            if (itemobj.likedbool==YES)
                            {
                                
                                
                                [likebtn setImage:[UIImage imageNamed:@"like_on.png"] forState:UIControlStateNormal];
                            }
                            else{
                                [likebtn setImage:[UIImage imageNamed:@"like_off.png"] forState:UIControlStateNormal];
                                
                                
                            }
                            
                            breakbool=YES;
                            break;
                        }
                    }
                }
                
                if (breakbool==YES)
                {
                    break;
                }
            }
            
            
        }
    }
    
    
    [myappDelegate.mylikesGlobalarray removeAllObjects];
    NSString *userid=[APPDELEGATE getStringFromPlist:@"userid"];

    
    NSDictionary *likedict=[[NSDictionary alloc]initWithObjectsAndKeys:userid,@"UserID",itemobj.userlistingidstr,@"UserListingID", nil];
    
    NSLog(@"chekc the like dict tag of sender %d-=-=-=%@",likeindex, likedict);
    registrationRep.delegate=self;
    [registrationRep getdata:likedict :@"LikeItem" :@"POST" withcount:@"0"];
    
}
//this action performs  pop list details action
-(IBAction)listDetailsAction:(id)sender
{
    [self setMenuDown];
    ListingDetailViewController *listingDetailVC;
    if (myappDelegate.isiphone5)
    {
        listingDetailVC = [[ListingDetailViewController alloc]initWithNibName:@"ListingDetailViewController" bundle:nil];
    }
    else
    {
        listingDetailVC = [[ListingDetailViewController alloc]initWithNibName:@"ListingDetailViewController_iphone4" bundle:nil];
    }
    
    listingDetailVC.listingmainarray=itemsarray;
    listingDetailVC.senderIndex=[sender tag];
    listingDetailVC.condstring=@"filterresult";
    [self.navigationController pushViewController:listingDetailVC animated:YES];
    
}
//this view controller push the profile view controller
-(IBAction)menuItemTapped:(id)sender
{
    
    [self setMenuDown];
    switch ([sender tag]) {
        case 1:
        {
            BOOL isVCExist=NO;
            NSArray *arr=self.navigationController.viewControllers;
            for (UIViewController *childvc in arr) {
                
                if([childvc isKindOfClass:[ProfileViewController class]])
                {
                    isVCExist=YES;
                    UIViewController *subvc=(ProfileViewController *)childvc;
                    [self.navigationController popToViewController:subvc animated:YES];
                }
            }
            
            if(!isVCExist)
            {
                ProfileViewController *profileVC;
                if (myappDelegate.isiphone5)
                {
                    profileVC = [[ProfileViewController alloc]initWithNibName:@"ProfileViewController" bundle:nil];
                }
                else
                {
                    profileVC = [[ProfileViewController alloc]initWithNibName:@"ProfileViewController_iphone4" bundle:nil];
                }
               
                [self.navigationController pushViewController:profileVC animated:YES];
            }
        }
            break;
        case 2:
        {
            BOOL isVCExist=NO;
            NSArray *arr=self.navigationController.viewControllers;
            for (UIViewController *childvc in arr) {
                
                if([childvc isKindOfClass:[MyClosetViewController class]])
                {
                    isVCExist=YES;
                    UIViewController *subvc=(MyClosetViewController *)childvc;
                    [self.navigationController popToViewController:subvc animated:YES];
                }
            }
            
            if(!isVCExist)
            {
                MyClosetViewController *myclosetVC;
                if (myappDelegate.isiphone5)
                {
                    myclosetVC = [[MyClosetViewController alloc]initWithNibName:@"MyClosetViewController" bundle:nil];
                }
                else
                {
                    myclosetVC = [[MyClosetViewController alloc]initWithNibName:@"MyClosetViewController_iphone4" bundle:nil];
                }
               
                [self.navigationController pushViewController:myclosetVC animated:YES];
            }
        }
            break;
            
        case 3:
        {
            BOOL isVCExist=NO;
            NSArray *arr=self.navigationController.viewControllers;
            for (UIViewController *childvc in arr) {
                
                if([childvc isKindOfClass:[MyFeedViewController class]])
                {
                    isVCExist=YES;
                    UIViewController *subvc=(MyFeedViewController *)childvc;
                    [self.navigationController popToViewController:subvc animated:YES];
                }
            }
            
            if(!isVCExist)
            {
                
                MyFeedViewController *myfeedVC;
                if (myappDelegate.isiphone5)
                {
                    myfeedVC = [[MyFeedViewController alloc]initWithNibName:@"MyFeedViewController" bundle:nil];
                }
                else
                {
                    myfeedVC = [[MyFeedViewController alloc]initWithNibName:@"MyFeedViewController_iphone4" bundle:nil];
                }
                
                [self.navigationController pushViewController:myfeedVC animated:YES];
            }
        }
            break;
            
        case 4:
        {
            BOOL isVCExist=NO;
            NSArray *arr=self.navigationController.viewControllers;
            for (UIViewController *childvc in arr) {
                
                if([childvc isKindOfClass:[MyWishlistViewController class]])
                {
                    isVCExist=YES;
                    UIViewController *subvc=(MyWishlistViewController *)childvc;
                    [self.navigationController popToViewController:subvc animated:YES];
                }
            }
            
            if(!isVCExist)
            {
                
                MyWishlistViewController *mywishlistVC;
                if (myappDelegate.isiphone5)
                {
                    mywishlistVC = [[MyWishlistViewController alloc]initWithNibName:@"MyWishlistViewController" bundle:nil];
                }
                else
                {
                    mywishlistVC = [[MyWishlistViewController alloc]initWithNibName:@"MyWishlistViewController_iphone4" bundle:nil];
                }
                
                [self.navigationController pushViewController:mywishlistVC animated:YES];
            }
        }
            break;
            
            
        case 5:
        {
            [self menuBtnAction:self];
        }
            break;
        case 6:
        {
            BOOL isVCExist=NO;
            NSArray *arr=self.navigationController.viewControllers;
            for (UIViewController *childvc in arr) {
                
                if([childvc isKindOfClass:[NewListingViewController class]])
                {
                    isVCExist=YES;
                    UIViewController *subvc=(NewListingViewController *)childvc;
                    [self.navigationController popToViewController:subvc animated:YES];
                }
            }
            
            if(!isVCExist)
            {
                
                NewListingViewController *mywishlistVC;
                if (myappDelegate.isiphone5)
                {
                    mywishlistVC = [[NewListingViewController alloc]initWithNibName:@"NewListingViewController" bundle:nil];
                }
                else
                {
                    mywishlistVC = [[NewListingViewController alloc]initWithNibName:@"NewListingViewController_iphone4" bundle:nil];
                }
               
                [self.navigationController pushViewController:mywishlistVC animated:YES];
            }
        }
            break;
            
        default:
            break;
    }
}

#pragma mark scrollViewDelgates

-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    [AppDelegate assaignitemimages:CGRectMake(0, scrollView.contentOffset.y,scrollView.frame.size.width , scrollView.frame.size.height) :itemsarray :itemscrollview];
    
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)successresponceListing :(id)respdict :(NSString *)paramname;
{
    
    NSLog(@"90900909090 %@ ",respdict);
    
    if ([paramname isEqualToString:@"LikeItem"])
    {
        NSLog(@"resp dict %@",respdict);
        
        if ([respdict isKindOfClass:[NSDictionary class]])
        {
            NSDictionary *mydict=(NSDictionary *)respdict;
            
            NSString *messagestr=[mydict valueForKey:@"message"];
            if ([messagestr isEqualToString:@"Wishlist Saved Successfully"] || [messagestr isEqualToString:@"Wishlist Delted Successfully"])
            {
                
                ItemsModelobject *itemobj=[itemsarray objectAtIndex:likeindex];
                
                if ([messagestr isEqualToString:@"Wishlist Saved Successfully"])
                {
                    itemobj.likedbool=YES;
                }
                else{
                    itemobj.likedbool=NO;
                }
                
                BOOL breakbool=NO;
                for (int i=0; i<itemscrollview.subviews.count; i++)
                {
                    UIView *myv=[itemscrollview.subviews objectAtIndex:i];
                    
                    if (myv.tag==likeindex)
                    {
                        if ([myv isKindOfClass:[UIView class]])
                        {
                            for (UIView *temv in myv.subviews)
                            {
                                if ([temv isKindOfClass:[UIButton class]])
                                {
                                    UIButton *likebtn=(UIButton *)temv;
                                    if (likebtn.frame.size.width<20)
                                    {
                                        if (itemobj.likedbool==YES)
                                        {
                                            
                                            
                                            [likebtn setImage:[UIImage imageNamed:@"like_on.png"] forState:UIControlStateNormal];
                                        }
                                        else{
                                            [likebtn setImage:[UIImage imageNamed:@"like_off.png"] forState:UIControlStateNormal];
                                            
                                            
                                        }
                                        
                                        breakbool=YES;
                                        break;
                                    }
                                }
                            }
                            
                            if (breakbool==YES)
                            {
                                break;
                            }
                        }
                        
                        
                    }
                }
                
            }
            
            
        }
        
        
        [myappDelegate stopspinner:self.view];
    }
    else if ([paramname isEqualToString:@"getallLikes"])
    {
        NSLog(@" check teh getall121Likes  %@ ",respdict);
        prevlikedarray =[[NSMutableArray alloc]init];
        if ([respdict isKindOfClass:[NSArray class]])
        {
            NSArray *resparray=(NSArray *)respdict;
            
            for (int i=0; i<resparray.count; i++)
            {
                NSDictionary *subdict=[resparray objectAtIndex:i];
                
                NSString *likedid=[NSString stringWithFormat:@"%@", [subdict objectForKey:@"userListingID"]];
                
                NSLog(@"check the string %@",likedid);
                [prevlikedarray addObject:likedid];
            }
        }
        
        [registrationRep getdata:nil :@"getListing" :@"GET" withcount:@"0"];
    }
    else if ([paramname isEqualToString:@"getListing"])
    {
        NSLog(@"get212Listing dictionary %@ ",respdict);
        
        [itemsarray removeAllObjects];
        if ([respdict isKindOfClass:[NSArray class]])
        {
            NSArray *resparry=(NSArray *)respdict;
            
            for (int i=0; i<resparry.count; i++)
            {
                
                NSDictionary *maindict=[resparry objectAtIndex:i];
                
                ItemsModelobject *itemsmodobj=[[ItemsModelobject alloc]init];
                itemsmodobj.itemNamestr=[maindict objectForKey:@"itemName"];
                
                itemsmodobj.itemidstr=[maindict objectForKey:@"unitPrice"];
                itemsmodobj.listingcoststr=[maindict objectForKey:@"unitPrice"];
                itemsmodobj.salecostcoststr=[maindict objectForKey:@"listPrice"];
                if ([maindict objectForKey:@"status"])
                {
                    itemsmodobj.itemStatus=[maindict objectForKey:@"status"];
                    
                }
                itemsmodobj.brandstr=[maindict objectForKey:@"brand"];
                itemsmodobj.conditionstr=[maindict objectForKey:@"condition"];
                itemsmodobj.catogrystr=[maindict objectForKey:@"categoryName"];
                itemsmodobj.sizestr=[maindict objectForKey:@"size"];
                itemsmodobj.descriptionstr=[maindict objectForKey:@"description"];
                itemsmodobj.useridstr=[maindict objectForKey:@"userID"];
                
                
                
                itemsmodobj.userlistingidstr=[NSString stringWithFormat:@"%@",[maindict valueForKey:@"userListingId"]];
                
                itemsmodobj.itemidstr=[maindict objectForKey:@"userItemId"];
                
                
                NSArray *itempickarray=[maindict objectForKey:@"itemPictures"];
                itemsmodobj.picturearray=[NSMutableArray new];
                
                for (int j=0; j<itempickarray.count; j++)
                {
                    NSDictionary *picdict=[itempickarray objectAtIndex:j];
                    
                    
                    NSString *urlsrt=[picdict valueForKey:@"pictureCdnUrl"];
                    urlsrt=[urlsrt stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
                    // %20 NSURL *picutl=[NSURL URLWithString:[picdict valueForKey:@"pictureCdnUrl"]];
                    
                    [itemsmodobj.picturearray addObject:urlsrt];
                }
                
                
                
                for (int lk=0; lk<prevlikedarray.count; lk++)
                {
                    NSString *prevlikedstr=[prevlikedarray objectAtIndex:lk];
                    
                    if ([prevlikedstr isEqualToString:itemsmodobj.userlistingidstr ])
                    {
                        itemsmodobj.likedbool=YES;
                        break;
                    }
                    
                }
                
                itemsmodobj.forsalebool = [[maindict valueForKey:@"forsale"] boolValue];
                
                [itemsarray addObject:itemsmodobj];
                
            }
        }
        
        [self createscrollview];
        [myappDelegate stopspinner:self.view];
    }
    
    
    
}

-(void)createscrollview
{
    
    for (UIView *selfv in itemscrollview.subviews)
    {
        [selfv removeFromSuperview];
    }
    int x=15;
    int y=5;
    
    NSLog(@" items array count %d",itemsarray.count);
    
    [myappDelegate createmyscrollview:itemscrollview :itemsarray :x :y :self];
    [itemscrollview setContentOffset:CGPointMake(0 , 0) animated:YES];
    }

-(void)internetConnection
{
    UIAlertView *Internetalert=[[UIAlertView alloc]initWithTitle:appname message:@"Internet connection not available" delegate:self cancelButtonTitle:nil otherButtonTitles:@"Ok", nil];
    [Internetalert show];
    [APPDELEGATE stopspinner:self.view];
    
}
@end
