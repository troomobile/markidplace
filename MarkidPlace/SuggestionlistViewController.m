//
//  SuggestionlistViewController.m
//  MarkidPlace
//
//  Created by stellent_mac_05 on 26/09/14.
//  Copyright (c) 2014 stellentmac1. All rights reserved.
//

#import "SuggestionlistViewController.h"

@interface SuggestionlistViewController ()

@end

@implementation SuggestionlistViewController
@synthesize navbar,titlelbl,firstsectionarray,tablearray,secondsectionarray,Basicarray,suggestionslistview,findfriendcell,nameimagearray,duplicatearray,suggestiontbllbl,usersearchbar,typestring,twitterArray;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    

    
    self.navbar.frame = CGRectMake(0, 0, 320, 64);
    self.titlelbl.font = [UIFont fontWithName:@"Monroe" size:18];
    tablearray=[NSMutableArray new];
    duplicatearray=[NSMutableArray new];
    twitterArray=[NSMutableArray new];
  
    [self.navbar setBackgroundImage:[UIImage imageNamed:@"navbar.png"] forBarMetrics:UIBarMetricsDefault];
    self.suggestiontbllbl.font=[UIFont fontWithName:@"muli" size:16];
    for ( int i=0; i<APPDELEGATE.suggestionslistArray.count; i++)
    {
        SuggestionObject *suggobj=[APPDELEGATE.suggestionslistArray objectAtIndex:i];
        NSLog(@"suggobj name is %@  ....",suggobj.namestr);
    }
    //[APPDELEGATE.Twitterfollowers removeAllObjects];

}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    if([typestring isEqualToString:@"twitter"])
    {[APPDELEGATE startspinner:self.view];
        dispatch_async( dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
            //twittercontacts_filtering
            // [self performSelectorOnMainThread:@selector(gettwittersuggestions) withObject:nil waitUntilDone:YES];
            [self performSelectorOnMainThread:@selector(twittercontacts_filtering) withObject:nil waitUntilDone:YES];
        //[self twittercontacts_filtering];
           // [self performSelector:@selector(gettwittersuggestions) withObject:nil];
        });
        
    }
    else if([typestring isEqualToString:@"contacts"])
    {[APPDELEGATE startspinner:self.view];
        dispatch_async( dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
            //[self performSelectorOnMainThread:@selector(getcontactssuggestions) withObject:nil waitUntilDone:YES];
            [self performSelector:@selector(getcontactssuggestions) withObject:nil];
        });
    }
    else
    {
        //self.duplicatearray=APPDELEGATE.suggestionslistArray;
        [self.duplicatearray addObjectsFromArray:APPDELEGATE.suggestionslistArray];
    }

}

-(void)viewDidAppear:(BOOL)animated
{
    /*
    if([typestring isEqualToString:@"twitter"])
    {
        dispatch_async( dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
       
        // [self performSelectorOnMainThread:@selector(gettwittersuggestions) withObject:nil waitUntilDone:YES];
             });
       [self performSelector:@selector(gettwittersuggestions) withObject:nil];
    }
    else if([typestring isEqualToString:@"contacts"])
    {
       dispatch_async( dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        //[self performSelectorOnMainThread:@selector(getcontactssuggestions) withObject:nil waitUntilDone:YES];
           [self performSelector:@selector(getcontactssuggestions) withObject:nil];
       });
    }
     */

}
- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return  50;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return APPDELEGATE.suggestionslistArray.count;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(APPDELEGATE.suggestionslistArray.count==0)
    {
        UITableViewCell *tbcell=[tableView dequeueReusableCellWithIdentifier:@"cell"];
        if(tbcell==nil)
        {
            tbcell=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
        }
        return tbcell;
    }
else
{
        static NSString *cellIdentifier=@"acell";
        
        FindFriendsTableViewCell *cell =(FindFriendsTableViewCell*)[suggestionslistview dequeueReusableCellWithIdentifier:cellIdentifier];
        if (cell==nil)
        {
            [[NSBundle mainBundle] loadNibNamed:@"FindFriendsTableViewCell" owner:self options:nil];
            cell=findfriendcell;
            cell.selectionStyle=UITableViewCellSelectionStyleNone;
        }
        cell.accountlbl.font = [UIFont fontWithName:@"Muli" size:16];
        cell.suggestionlbl.font = [UIFont fontWithName:@"Muli" size:15];
        cell.followbtn.layer.borderWidth=1;
        cell.followbtn.layer.borderColor=[[UIColor colorWithRed:12.0/255.0 green:183.0/255.0 blue:174.0/255.0 alpha:1.0] CGColor];
       //  cell.followbtn.titleLabel=[UIFont fon];
        // [cell.followbtn.titleLabel setFont:[UIFont systemFontOfSize:14]];
        [cell.followbtn.titleLabel setFont:[UIFont fontWithName:@"Muli" size:14]];
           SuggestionObject *userObj=[APPDELEGATE.suggestionslistArray objectAtIndex:indexPath.row];
    if([APPDELEGATE isnullorempty:userObj.usernamestr])
    {
        cell.suggestionlbl.text=userObj.namestr;
    }
    else
    {
        cell.suggestionlbl.text=userObj.usernamestr;
    }
    
    
        cell.followbtn.tag=indexPath.row;
    
        [cell.nameimgview setImageWithURL:[NSURL URLWithString:userObj.profilepicstr] placeholderImage:[UIImage imageNamed:@"noimage_friend.png"] ];
        
        [cell.nameimgview.layer setCornerRadius:cell.nameimgview.frame.size.height/2];
        [cell.nameimgview.layer setBorderWidth:0];
        [cell.nameimgview.layer setMasksToBounds:YES];
        [cell.followbtn setTitle:@"Follow" forState:UIControlStateNormal];
          cell.accountlbl.hidden=YES;
        return cell;
    }
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}
-(IBAction)backbuttonaction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
    
}
-(IBAction)homebuttonaction:(id)sender
{
    BOOL isVCExist=NO;
    NSArray *arr=self.navigationController.viewControllers;
    for (UIViewController *childvc in arr)
    {
        
        if([childvc isKindOfClass:[SuggestionsViewController class]])
        {
           
            SuggestionsViewController *subvc=(SuggestionsViewController *)childvc;
            subvc.ismyfeed=NO;
            isVCExist=YES;
            [self.navigationController popToViewController:subvc animated:YES];
        }
    }
    
    
    if(!isVCExist)
    {
        SuggestionsViewController *sugge;
        if ([AppDelegate isiphone5])
        {
            sugge = [[SuggestionsViewController alloc]initWithNibName:@"SuggestionsViewController" bundle:nil];
        }
        else
        {
            sugge = [[SuggestionsViewController alloc]initWithNibName:@"SuggestionsViewController_iphone4" bundle:nil];
        }
        [self.navigationController pushViewController:sugge animated:YES];
    }

}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)followButtonAction:(id)sender
{
    NSMutableDictionary *dict=[[NSMutableDictionary alloc] init];
    NSString *userid=[myappDelegate getStringFromPlist:@"userid"];
    [dict setValue:userid forKey:@"UserId"];
    SuggestionObject *userObj=[APPDELEGATE.suggestionslistArray objectAtIndex:[sender tag]];
    
    [dict setValue:[NSString stringWithFormat:@"%@",userObj.useridstr] forKey:@"FollowerUserId"];
    profilerepo=[[ProfileRepository alloc]init];
    profilerepo.delegate=self;
    [profilerepo followUserswithDictionary:dict];
    [APPDELEGATE.suggestionslistArray removeObject:userObj];
    [suggestionslistview reloadData];
}
#pragma mark searchbardelegate

-(void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar
{
    //self.duplicatearray=APPDELEGATE.suggestionslistArray;
if ([searchBar.text isEqualToString:@" "])
{
    [APPDELEGATE.suggestionslistArray removeAllObjects];
    [APPDELEGATE.suggestionslistArray addObjectsFromArray:self.duplicatearray];
    [suggestionslistview reloadData];
}
    
}
-(void)searchBarTextDidEndEditing:(UISearchBar *)searchBar
{
    if ([searchBar.text isEqualToString:@" "])
    {
        searchBar.text = @"";
        [APPDELEGATE.suggestionslistArray removeAllObjects];
        [APPDELEGATE.suggestionslistArray addObjectsFromArray:self.duplicatearray];
        [suggestionslistview reloadData];
    }
    
}
-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    [searchBar setShowsCancelButton:NO animated:YES];
    if(searchBar.text.length == 0)
    {
        [APPDELEGATE.suggestionslistArray removeAllObjects];
        [APPDELEGATE.suggestionslistArray addObjectsFromArray:self.duplicatearray];
        [suggestionslistview reloadData];
    }
    else
    {
        [searchBar setShowsCancelButton:NO animated:YES];
        [APPDELEGATE.suggestionslistArray removeAllObjects];
        NSLog(@"the description of the duplicate Array is %@",duplicatearray.description);
        NSLog(@"the count of the duplicate Array is %lu",(unsigned long)duplicatearray.count);
        
        for(int i=0;i<[self.duplicatearray count];i++)
        {
            SuggestionObject*searchobj=[self.duplicatearray objectAtIndex:i];
            
            //     NSRange nameRange = [[[self.duplicatearray objectAtIndex:i] valueForKey:@"itemName"]  rangeOfString:searchBar.text options:(NSCaseInsensitiveSearch|NSDiacriticInsensitiveSearch)];
            NSRange nameRange = [searchobj.usernamestr rangeOfString:searchBar.text options:(NSCaseInsensitiveSearch|NSDiacriticInsensitiveSearch)];
            
            if(nameRange.location != NSNotFound)
            {
                [APPDELEGATE.suggestionslistArray addObject:[self.duplicatearray objectAtIndex:i]];
            }
        }
        [suggestionslistview reloadData];
    }
    
    if([searchText length] == 0)
    {
        [searchBar performSelector: @selector(resignFirstResponder)
                        withObject: nil
                        afterDelay: 0.1];
        return;
    }
    
}
- (BOOL)searchBar:(UISearchBar *)searchBar shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    if ([text isEqualToString:@" "])
    {
        return NO;
    }
    else
    {
        return YES;
    }
}

-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [searchBar resignFirstResponder];
    [APPDELEGATE.suggestionslistArray removeAllObjects];
    for(int i=0;i<[self.duplicatearray count];i++)
    {
        SuggestionObject*searchobj=[self.duplicatearray objectAtIndex:i];
        NSLog(@"searchobj.usernamestr %@",searchobj.usernamestr);
        NSRange nameRange = [searchobj.usernamestr rangeOfString:searchBar.text options:(NSCaseInsensitiveSearch|NSDiacriticInsensitiveSearch)];
        
        if(nameRange.location != NSNotFound)
        {
            [APPDELEGATE.suggestionslistArray addObject:[self.duplicatearray objectAtIndex:i]];
        }
    }
    [suggestionslistview reloadData];
}

-(void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    searchBar.text=@"";
    [searchBar resignFirstResponder];
    [APPDELEGATE.suggestionslistArray addObjectsFromArray:duplicatearray];
    [suggestionslistview reloadData];
    
}
-(void)internetConnection
{
    UIAlertView *Internetalert=[[UIAlertView alloc]initWithTitle:appname message:@"Internet connection not available" delegate:self cancelButtonTitle:nil otherButtonTitles:@"Ok", nil];
    [Internetalert show];
    [APPDELEGATE stopspinner:self.view];
    
}
-(void)getcontactssuggestions
{
    [APPDELEGATE.suggestionslistArray removeAllObjects];
    NSLog(@"APPDELEGATE.userlistArray %lu",(unsigned long)APPDELEGATE.userlistArray.count);
    //Krish
    [APPDELEGATE.userlistArray enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        __block SuggestionObject *sobj = (SuggestionObject *)obj;
        __block BOOL isExisting = NO;
        [APPDELEGATE.coreDataContactsAray enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            Person *coreDataObj = (Person *)obj;
            if([sobj.emailstr isEqualToString:coreDataObj.homeEmail]||[sobj.emailstr isEqualToString:coreDataObj.workEmail])
            {
                isExisting = YES;
            }
        }];
        if(isExisting)
        {
            [APPDELEGATE.suggestionslistArray addObject:sobj];
        }
        
    }];
    
    
    
    
    
    
    /*
    
    for (int i=0; i<APPDELEGATE.userlistArray.count; i++)
    {
        for (int k=0; k<APPDELEGATE.coreDataContactsAray.count; k++)
        {
            SuggestionObject *obj=[APPDELEGATE.userlistArray objectAtIndex:i];
            Person  *person=[APPDELEGATE.coreDataContactsAray objectAtIndex:k];
            if([obj.typestr isEqualToString:@"General"])
            {
                //NSLog(@"obj.emailstr----------- %@",obj.emailstr);
                //NSLog(@"person.homeEmail----------- %@",person.homeEmail);
                if([obj.emailstr isEqualToString:person.homeEmail]||[obj.emailstr isEqualToString:person.workEmail])
                {
                    //NSLog(@"the log of k value is %d",i);
                    
                    [APPDELEGATE.suggestionslistArray addObject:obj];
                    break;
                }
            }
        }
    }
     */
    NSLog(@"APPDELEGATE.suggestionslistArray%lu",(unsigned long)APPDELEGATE.suggestionslistArray.count);
    [self.duplicatearray removeAllObjects];
    [self.duplicatearray addObjectsFromArray:APPDELEGATE.suggestionslistArray];
    alertmsg=@"contacts";
    
  
    [suggestionslistview reloadData];
   dispatch_async( dispatch_get_main_queue(), ^{
    
     [self suggestionlistview];
   });
}
-(void)gettwittersuggestions
{
    //dispatch_async( dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        
        AddFriendsViewController *addfriendsVC=[[AddFriendsViewController alloc]init];
        addfriendsVC.typeString=@"settings";
        
        [addfriendsVC twitterBtnAction:nil];
        
   // [self performSelector:@selector(twittercontacts_filtering) withObject:nil afterDelay:5.0];


        
    //});
   // [self twittercontacts_filtering];
   // [self twittercontacts_filtering];
    //[self performSelector:@selector(twittercontacts_filtering) withObject:nil afterDelay:0.4];
}


-(void)twittercontacts_filtering
{
    [APPDELEGATE.suggestionslistArray removeAllObjects];
    NSLog(@"the count of the twitterfollowers is %lu",(unsigned long)APPDELEGATE.Twitterfollowers.count);
     NSLog(@"the twitterfollowers Array is %@",APPDELEGATE.Twitterfollowers.description);
    for (int i=0; i<APPDELEGATE.userlistArray.count; i++)
    {
            SuggestionObject *obj=[APPDELEGATE.userlistArray objectAtIndex:i];
        
            if([obj.typestr isEqualToString:@"Twitter"])
            {
                [twitterArray addObject:obj];
            }
    }
    // NSLog(@"APPDELEGATE.suggestionslistArray.count is %d",APPDELEGATE.suggestionslistArray.count);
    //alertmsg=@"twitter";
   // [self suggestionlistview];
    
    if (twitterArray.count>0)
    {
        [self comparetwittercontacts];

    }
    else
    {
        alertmsg=@"twitter";
         [self suggestionlistview];
    }
    }
-(void)comparetwittercontacts
{
    NSLog(@"APPDELEGATE.Twitterfollowers-------%@",APPDELEGATE.Twitterfollowers.description);
    for (int k=0; k<twitterArray.count; k++)
    {
        
        SuggestionObject *obj=[twitterArray objectAtIndex:k];
        
        BOOL isTheObjectThere = [APPDELEGATE.Twitterfollowers containsObject:[NSString stringWithFormat:@"%@",obj.socialidstr]];
        if(isTheObjectThere)
        {
            [APPDELEGATE.suggestionslistArray addObject:obj];
        }

        
    }
    if (APPDELEGATE.suggestionslistArray.count>0)
    {
        [self.duplicatearray removeAllObjects];
        [self.duplicatearray addObjectsFromArray:APPDELEGATE.suggestionslistArray];
        [suggestionslistview reloadData];
    }
    else
    {
        alertmsg=@"twitter";
        [self suggestionlistview];
    }
[APPDELEGATE stopspinner:self.view];
    
}
-(void)suggestionlistview
{
   dispatch_async( dispatch_get_main_queue(), ^{
       
       [APPDELEGATE stopspinner:self.view];
       
       if(APPDELEGATE.suggestionslistArray.count>0)
       {
           [suggestionslistview reloadData];
       }
       else
       {
           if([alertmsg isEqualToString:@"twitter"])
           {
               [AppDelegate alertmethod:appname :@"Ooops!  It appears that none of your friends are using marKIDplace. That makes us sad!  No worries, you can invite them by going to the “Invite Friends” section in Settings"];
           }
           else if([alertmsg isEqualToString:@"contacts"])
           {
               [AppDelegate alertmethod:appname :@"Ooops!  It appears that none of your friends are using marKIDplace. That makes us sad!  No worries, you can invite them by going to the “Invite Friends” section in Settings"];
           }
           else
           {
               [AppDelegate alertmethod:appname :@"Ooops!  It appears that none of your friends are using marKIDplace. That makes us sad!  No worries, you can invite them by going to the “Invite Friends” section in Settings"];
           }
       }

      });
}


@end
