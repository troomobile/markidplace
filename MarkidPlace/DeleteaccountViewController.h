//
//  DeleteaccountViewController.h
//  MarkidPlace
//
//  Created by Varma Bhupatiraju on 10/09/14.
//  Copyright (c) 2014 stellentmac1. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ProfileRepository.h"
@class AppDelegate;

@interface DeleteaccountViewController : UIViewController<HelperProtocol,UIAlertViewDelegate>
{
    AppDelegate*appdelegate;
     ProfileRepository *profileRepos;
    
}
@property (nonatomic,strong) IBOutlet UINavigationBar *navbar;
@property(nonatomic,strong)IBOutlet UILabel *deletetitlelbl;
@property(nonatomic,strong)IBOutlet UILabel *welbl;
@property(nonatomic,strong)IBOutlet UILabel *dropdownlbl;
@property(nonatomic,strong)IBOutlet UIButton *dropdownbtn;
@property(nonatomic,strong)IBOutlet UITableView *dropdowntblview;
@property(nonatomic,strong)IBOutlet NSMutableArray *firstarray;
@property(nonatomic,strong) id<HelperProtocol>delegate;
@property(nonatomic,strong)IBOutlet UIButton *deletebtn;
-(IBAction)backAction:(id)sender;
-(IBAction)homeBtnAction:(id)sender;
-(IBAction)dropdownAction:(id)sender;
-(IBAction)deletebtnAction:(id)sender;
@end
