//
//  NSDate+PostedTimeAgo.h
//  Date-Class
//
//  Created by stellent on 05/09/14.
//  Copyright (c) 2014 stellent. All rights reserved.
//

#import <Foundation/Foundation.h>

//**************    READE ME!   **************//
//*******************************************//
/*
  This is NSDate Category file, The use of catergory file is extende the fetures of existing class, Here we are extending the NSDate class features, meanse we can use the NSDate class defult features and also the some other features that is calculate the Posted Time Ago,
 
  Here we are using two methods 
  They are : 
             1 convertServerDateStringToNSDateType
 
             2 postedTimeAgoWithPostedDateString
 
 1:convertServerDateStringToNSDateType : 
 
        - This method is useful for convert the server posted data in the form of "yyyy-MM-dd'T'HH:mm:ss.SSS" to our Objective-C NSDate.
        - Make sure always confirm with your backend people about Posted date is in the form of UTC or not. So we can get accurate results.
 
        - Make sure API call send the date in the form of "yyyy-MM-dd'T'HH:mm:ss.SSS" string.
 
        - "convertServerDateStringToNSDateType" method i used for convert the server utc date to objective-c NSDate internally. So you dont worry about this methods. No need to call or implement this method in your project.
 
 2:postedTimeAgoWithPostedDateString :
      
        - This is  most importent method for getting How much time was posted ago.
 
        - This method returns the NSString time,  like below.
 
          1y - 1 year ago
          1M - 1 Month ago // this code was commented, most of the social netwrok sites shown like weeks but not Months, if client requires, remove comments in above method for Months.
          1w - 1 week ago
          1d - 1 day ago
          1h - 1 hour ago
          1m - 1 minute ago
          1s - 1 second ago
       
       
 How to use These categories:
 
   - Drag and drop the "NSDate+PostedTimeAgp.h" and  "NSDate+PostedTimeAgp.m" files into your project prepared folder.
  
   - Import "NSDate+PostedTimeAgo.h" file like "#import "NSDate+PostedTimeAgo.h" in your prepared viewcontroller.
 
   - "postedTimeAgoWithPostedDateString" Method is a static methods of NSDate, so we have to call this method with NSDate class like below
 
 example:
      [NSDate postedTimeAgoWithPostedDateString:postedDateString];
 
  - Here we pass one paramter that is postedDateString, PostedDatestring is NSString type so we have to pass the Server API call Posted Date string that is in the form like "yyyy-MM-dd'T'HH:mm:ss.SSS"
 
 example: 
 
      NSString *postedDateString=@"2014-09-05'T'13:30:20:005000";
 
     [NSDate postedTimeAgoWithPostedDateString:postedDateString];
 
  - Make sure API call posted string alsways in the form of "yyyy-MM-dd'T'HH:mm:ss.SSS"
 
  - Some times API call send 7 digit milli seconds like 
 
    "2014-09-05'T'13:30:20:1234567" so its not a problem. everything will be handled in the code.
 
 
 */
//*******************************************//

@interface NSDate (PostedTimeAgo)


-(NSDate *)convertServerDateStringToNSDateType:(NSString *)dateString;

+(NSString* ) postedTimeAgoWithPostedDateString:(NSString *) postedDateString;


@end
