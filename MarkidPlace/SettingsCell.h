//
//  SettingsCell.h
//  MarkidPlace
//
//  Created by stellent_mac_05 on 11/06/14.
//  Copyright (c) 2014 stellentmac1. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SettingsCell : UITableViewCell
@property (nonatomic ,strong)IBOutlet UILabel *namelbl;
@property (nonatomic ,strong)IBOutlet UIImageView *accessoryimgview;
@property (nonatomic ,strong)IBOutlet UIButton *logoutbtn;

@end
