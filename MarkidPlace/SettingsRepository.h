//
//  SettingsRepository.h
//  MarkidPlace
//
//  Created by Varma Bhupatiraju on 11/09/14.
//  Copyright (c) 2014 stellentmac1. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "HelperProtocol.h"
#import "Reachability.h"
#import "AppDelegate.h"
@class AppDelegate;
@interface SettingsRepository : NSObject<HelperProtocol>

@property(nonatomic,strong)id <HelperProtocol> delegate;


-(void)deleteUserAccountWithUserid:(NSString*)userid andReasonToDelete:(NSString*)reason;
-(void)getListUsers:(NSString *)userid;

@end
