//
//  MySizecustomCell.h
//  MarkidPlace
//
//  Created by Varma Bhupatiraju on 14/08/14.
//  Copyright (c) 2014 stellentmac1. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MySizecustomCell : UITableViewCell
@property(nonatomic,strong)IBOutlet UILabel *brandnamelabel;
@property(nonatomic,strong)IBOutlet UILabel *brandlabel;
@property(nonatomic,strong)IBOutlet UIButton *brandactiontag;

@end