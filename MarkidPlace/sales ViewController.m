//
//  sales ViewController.m
//  MarkidPlace
//
//  Created by Varma Bhupatiraju on 20/09/14.
//  Copyright (c) 2014 stellentmac1. All rights reserved.
//

#import "sales ViewController.h"

@interface sales_ViewController ()

@end

@implementation sales_ViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
