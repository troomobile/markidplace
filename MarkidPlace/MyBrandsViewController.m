//
//  MyBrandsViewController.m
//  MarkidPlace
//
//  Created by Varma Bhupatiraju on 15/09/14.
//  Copyright (c) 2014 stellentmac1. All rights reserved.
//

#import "MyBrandsViewController.h"
#import "SuggestionsViewController.h"
@interface MyBrandsViewController ()

@end

@implementation MyBrandsViewController
@synthesize navbar,mybrndstitlelbl,dropdowntblview,popview,brndcell,brandtblview,brandarray,updatebtn;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    dropdowntblview.layer.borderWidth = 1;
    dropdowntblview.layer.cornerRadius=1.0f;
    
    dropdowntblview.layer.borderColor =[[UIColor whiteColor ] CGColor];
     self.mybrndstitlelbl.font = [UIFont fontWithName:@"Monroe" size:18];
    brandtblview.scrollEnabled=NO;
    updatebtn.layer.borderWidth = 1.0;
    updatebtn.backgroundColor=[UIColor whiteColor];
    updatebtn.layer.borderColor=[[UIColor colorWithRed:12.0/255.0f green:183.0/255.0f blue:174.0/255.0f alpha:1.0f]CGColor];
    updatebtn.titleLabel.font=[UIFont fontWithName:@"Muli" size:12];

    
    self.navbar.frame = CGRectMake(0, 0, 320, 64);
    [self.navbar setBackgroundImage:[UIImage imageNamed:@"navbar.png"] forBarMetrics:UIBarMetricsDefault];
    
        brandarray=[NSMutableArray new];
   
        Newmodelobject *obj=[[Newmodelobject alloc]init];

            obj.isother=NO;
            obj.cellnamestr=@"Brand";
            obj.cellvaluestr=@"";
            obj.otherstr=@"";
    
        [brandarray addObject:obj];
        
    

    
    // Do any additional setup after loading the view from its nib.
}
-(IBAction)backAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
    
}
-(IBAction)homeBtnAction:(id)sender;
{
    
    NSArray *arr=self.navigationController.viewControllers;
    for (UIViewController *childvc in arr)
    {
        
        if([childvc isKindOfClass:[SuggestionsViewController class]])
        {
            SuggestionsViewController *subvc=(SuggestionsViewController *)childvc;
            subvc.ismyfeed=NO;
            [self.navigationController popToViewController:subvc animated:YES];
        }
    }
    
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 40.0;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView == dropdowntblview)
    {
        if (tableView.tag==111)
        {
            //lookobj.categoryDataArray
          
            return myappDelegate.brandGlobalarray.count;

        }
        else
        {
            return 1;
        }
    }
    else
    {
        return brandarray.count;
    }
    

    return 0;
    
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
        return 1;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
   
        
        return 0;
    
    
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView==brandtblview)
    {
        
        static NSString *cellidentifier=@"cell";
        MybrandsCustomCell *cell=(MybrandsCustomCell *)[tableView dequeueReusableCellWithIdentifier:cellidentifier];
        if (cell==nil)
        {
            [[NSBundle mainBundle]loadNibNamed:@"MybrandsCustomCell" owner:self options:nil];
            cell=self.brndcell;
            
        }
        Newmodelobject *cellobj=[brandarray objectAtIndex:indexPath.row];
        
        cell.brandNamelbl.font=[UIFont fontWithName:@"Muli" size:15];
        NSLog(@"cellobj.cellnamestr.....%@",cellobj.cellnamestr);
        //cell.brandnamelabel.text=cellobj.cellnamestr;
        cell.brandlbl.text=cellobj.cellnamestr;
        NSLog(@" cell.brandnamelabel.text....%@", cell.brandNamelbl.text);
        //cell.brandnamelabel.text=cellobj.cellvaluestr;//
        cell.backgroundColor=[UIColor clearColor];
        cell.contentView.backgroundColor=[UIColor clearColor];
        cell.brandlbl.font=[UIFont fontWithName:@"Muli" size:15];
        if (indexPath.row==0)
        {
            cell.brandbtn.tag=111;
            
        }
        
        return  cell;
    }
    else if(tableView ==dropdowntblview)
    {
        
        UITableViewCell *tbcell=[tableView dequeueReusableCellWithIdentifier:@"cell"];
        if(tbcell==nil)
        {
            tbcell=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
        }
        tbcell.textLabel.font = [UIFont fontWithName:@"Muli" size:15];
        
        tbcell.textLabel.textColor=[UIColor whiteColor];//
        
        tbcell.textLabel.backgroundColor=[UIColor clearColor];
        tbcell.selectionStyle=UITableViewCellSelectionStyleNone;
        tbcell.contentView.backgroundColor=[UIColor clearColor];
        tbcell.backgroundColor=[UIColor clearColor];
        
        if (tableView.tag==111)
        {
            LookupModelobject *lookobj=[myappDelegate.brandGlobalarray objectAtIndex: indexPath.row];
            
            tbcell.textLabel.text=lookobj.loolupValue;
            NSLog(@"tbcell.textLabel.text....%@",tbcell.textLabel.text);
            if (lookobj.isCheckMark)
            {
                tbcell.accessoryType=UITableViewCellAccessoryCheckmark;
                tbcell.tintColor = [UIColor whiteColor];
            }
            else
                tbcell.accessoryType=UITableViewCellAccessoryNone;
            
            
            NSLog(@"access type %d",tbcell.accessoryType);
            return tbcell;
            
            
        }
        
        
    }
    return nil;
    
}
-(IBAction)brandbtnAction:(id)sender
{
    
    dropdowntblview.tag=[sender tag];
     [self popViewMethod:YES];
    [dropdowntblview reloadData];
    
}
-(IBAction)cancelBtnAction:(id)sender
{
    [self popViewMethod:NO];
}
-(IBAction)doneBtnAction:(id)sender
{
    
}
-(void)popViewMethod :(BOOL)isshow
{
    CGRect popfrm=popview.frame;
    popfrm.origin.x=0;
    if (isshow)
    {
        // [self.view addSubview:popview];
        popfrm.origin.y=0;
        
        
        CATransition *transition = [CATransition animation];
        transition.duration = 0.3;
        transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
        transition.type = kCATransitionPush;
        transition.subtype =kCATransitionFromTop;
        transition.delegate = self;
        [popview.layer addAnimation:transition forKey:nil];
    }
    else
    {
        popfrm.origin.y=605;
        CATransition *transition = [CATransition animation];
        transition.duration = 0.3;
        transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
        transition.type = kCATransitionPush;
        transition.subtype =kCATransitionFromBottom;
        transition.delegate = self;
        [popview.layer addAnimation:transition forKey:nil];
    }
    
    //[UIView beginAnimations: @"anim" context: nil];
    //[UIView setAnimationBeginsFromCurrentState: YES];
    // [UIView setAnimationDuration:0.3];
    popview.frame=popfrm;
    // [UIView commitAnimations];
    [self.view bringSubviewToFront:popview];
    [self.view addSubview:popview];
    
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSLog(@"dropdownview.tag...%d",dropdowntblview.tag);
    NSLog(@"indexpath.section....%d",indexPath.section);
    NSLog(@"indexpath.row....%d",indexPath.row);
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    //dropdownview.separatorStyle=NO;
    cell.selectionStyle=NO;
    
    if (tableView.tag==111)
    {
        LookupModelobject *lookobj=[myappDelegate.brandGlobalarray objectAtIndex: indexPath.row];
        
        
        if (lookobj.isCheckMark)
        {
            lookobj.isCheckMark=NO;
        }
        else
        {
            lookobj.isCheckMark=YES;
            
        }
        [dropdowntblview reloadData];
    }
    
    else
    {
        NSLog(@"cell.accessoryType...%d",cell.accessoryType);
        NSLog(@"UITableViewCellAccessoryCheckmark...%d",UITableViewCellAccessoryCheckmark);
        if (cell.accessoryType == UITableViewCellAccessoryCheckmark)
        {
            cell.accessoryType = UITableViewCellAccessoryNone;
            cell.tintColor = [UIColor whiteColor];
        }
        else
        {
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
            cell.tintColor = [UIColor whiteColor];
        }
    }
    

}
-(void)internetConnection
{
    UIAlertView *Internetalert=[[UIAlertView alloc]initWithTitle:appname message:@"Internet connection not available" delegate:self cancelButtonTitle:nil otherButtonTitles:@"Ok", nil];
    [Internetalert show];
    [APPDELEGATE stopspinner:self.view];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
