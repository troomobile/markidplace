//
//  LoginViewController.h
//  MarkidPlace
//
//  Created by stellentmac1 on 5/21/14.
//  Copyright (c) 2014 stellentmac1. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ProfileViewController.h"
#import "LoginViewController.h"
#import "AppDelegate.h"
#import "HelperProtocol.h"
#import "RegistrationRepository.h"
#import "SuggestionObject.h"
#import "RKCropImageController.h"


@class AppDelegate;
@class RegistrationRepository;
@interface RegisterViewController : UIViewController<UITextFieldDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UIActionSheetDelegate,HelperProtocol,UIAlertViewDelegate,RKCropImageViewDelegate>
{
    CGRect screenSize;
     id <HelperProtocol> delegate;
    UITextField *currenttextfield;
     RegistrationRepository *registrationRep;
    AppDelegate*appdelegate;
   // AppDelegate *appdelegate;

}

//@property (nonatomic,strong) UIImagePickerController *imagePickerController;
@property (nonatomic,strong) IBOutlet UIImageView *cameraLogoImageView;
@property (nonatomic,strong) IBOutlet UILabel *yourPhotoLabel;
@property (nonatomic,strong)  NSString *emailstr;
@property (nonatomic,strong)  NSString *passwordstr;

@property (nonatomic,strong) IBOutlet UINavigationBar *navBar;
@property (nonatomic,strong) IBOutlet UITextField *emailTextField;
@property (nonatomic,strong) IBOutlet UITextField *passwordTextField;

@property (nonatomic,strong) IBOutlet UITextField *usernametf;
//@property (nonatomic,strong) IBOutlet UILabel *photoLabel;
@property (nonatomic,strong) IBOutlet UIImageView *profileImageView;
//@property (nonatomic,strong) IBOutlet UIImageView *cameraImageView;
@property(nonatomic,strong) IBOutlet UIActivityIndicatorView *spinner;

-(IBAction)cameraButtonAction:(id)sender;
-(IBAction)loginButtonAction:(id)sender;
-(IBAction)registerButtonAction:(id)sender;
-(IBAction)pinterestButtonAction:(id)sender;

-(IBAction)backButtonAction:(id)sender;



@end
