//
//  ChangePasswordViewController.m
//  MarkidPlace
//
//  Created by Varma Bhupatiraju on 11/09/14.
//  Copyright (c) 2014 stellentmac1. All rights reserved.
//

#import "ChangePasswordViewController.h"
#import "Supportmodelobject.h"
#import "SuggestionsViewController.h"
#import "ChangePasswordCustomCell.h"
@interface ChangePasswordViewController ()

@end

@implementation ChangePasswordViewController
@synthesize Changepasswordtitlelbl,firsttableview,myChangePasswordCustomCell,firstarray,secondarry,navbar,updatebtn;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    profileRepos=[[ProfileRepository alloc]init];
    profileRepos.delegate=self;
    appdelegate =[[UIApplication sharedApplication]delegate];
    self.navbar.frame = CGRectMake(0, 0, 320, 64);
    updatebtn.titleLabel.font=[UIFont fontWithName:@"Muli" size:16];
    updatebtn.layer.cornerRadius=5;
    updatebtn.layer.masksToBounds=YES;
    updatebtn.layer.borderWidth=1;
    updatebtn.layer.borderColor=[[UIColor colorWithRed:12.0/255.0 green:183.0/255.0 blue:174.0/255.0 alpha:1.0]CGColor];
    [updatebtn setTitleColor:[UIColor colorWithRed:12.0/255.0 green:183.0/255.0 blue:174.0/255.0 alpha:1.0] forState:UIControlStateNormal];
    [self.navbar setBackgroundImage:[UIImage imageNamed:@"navbar.png"] forBarMetrics:UIBarMetricsDefault];
    self.Changepasswordtitlelbl.font = [UIFont fontWithName:@"Monroe" size:18];
    updatebtn.layer.borderWidth = 1.0;
    //updatebtn.layer.cornerRadius=5.0f;
    updatebtn.backgroundColor=[UIColor whiteColor];
    //loginlabel.layer.borderColor =[[UIColor greenColor] CGColor];
    updatebtn.layer.borderColor=[[UIColor colorWithRed:12.0/255.0f green:183.0/255.0f blue:174.0/255.0f alpha:1.0f]CGColor];
    updatebtn.titleLabel.font=[UIFont fontWithName:@"Muli" size:12];
    firstarray =[[NSMutableArray alloc]initWithObjects:@"Current Password",@"New Password",@"Re-enter New Password",nil];
    secondarry =[[NSMutableArray alloc]init];
    
    for (int i=0;i<[firstarray count];i++)
    {
        Supportmodelobject *obj;
        obj=[[Supportmodelobject alloc]init];
        obj.namestr=[firstarray objectAtIndex:i];
        NSLog(@"name passing....%@",obj.namestr);
        [secondarry addObject:obj];
        // NSLog(@"secondarray is....%@",[secondarray description]);
        
    }
    NSLog(@"secondarray is....%@",[secondarry description]);
    
    
    

    
    // Do any additional setup after loading the view from its nib.
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return  44;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return  secondarry.count;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
}
-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellidentifier=@"cell";
    ChangePasswordCustomCell *cell=(ChangePasswordCustomCell *)[firsttableview dequeueReusableCellWithIdentifier:cellidentifier];
    if (cell==nil)
    {
        [[NSBundle mainBundle]loadNibNamed:@"ChangePasswordCustomCell" owner:self options:nil];
        cell=self.myChangePasswordCustomCell;
        
    }
   // cell.namelbl.font = [UIFont fontWithName:@"Muli" size:13];
    Supportmodelobject *bobj=[secondarry objectAtIndex:indexPath.row];
    
    NSLog(@"bobj.namestr...%@",bobj.namestr);
    cell.namelbl.text=bobj.namestr;
    cell.namelbl.font=[UIFont fontWithName:@"muli" size:15];
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    //cell.changetextfield=Currenttextfield;
    return  cell;
}

-(IBAction)backAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
    
}

-(IBAction)homeBtnAction:(id)sender;
{
    
    NSArray *arr=self.navigationController.viewControllers;
    for (UIViewController *childvc in arr)
    {
        
        if([childvc isKindOfClass:[SuggestionsViewController class]])
        {
            SuggestionsViewController *subvc=(SuggestionsViewController *)childvc;
            subvc.ismyfeed=NO;
            [self.navigationController popToViewController:subvc animated:YES];
        }
    }
    
}


-(IBAction)updatebtnAction:(id)sender
{
   // [myappDelegate startspinner:self.view];

   [Currenttextfield resignFirstResponder];
    NSString *userid=[myappDelegate getStringFromPlist:@"userid"];
    NSIndexPath *indexpath1=[NSIndexPath indexPathForRow:0 inSection:0];
    NSIndexPath *indexpath2=[NSIndexPath indexPathForRow:1 inSection:0];
    NSIndexPath *indexpath3=[NSIndexPath indexPathForRow:2 inSection:0];
    
    ChangePasswordCustomCell *changepasswordcell1=(ChangePasswordCustomCell *)[firsttableview cellForRowAtIndexPath:indexpath1];
    NSLog(@"changepasswordcell1.....%@",changepasswordcell1);
    ChangePasswordCustomCell *changepasswordcell2=(ChangePasswordCustomCell *)[firsttableview cellForRowAtIndexPath:indexpath2];
    
    ChangePasswordCustomCell *changepasswordcell3=(ChangePasswordCustomCell *)[firsttableview cellForRowAtIndexPath:indexpath3];
    
 
    
    NSLog(@"changepasswordcell2.changetextfield.text....%@",changepasswordcell2.changetextfield.text);
    if ([changepasswordcell2.changetextfield.text length ]==0||[changepasswordcell1.changetextfield.text length]==0||[changepasswordcell3.changetextfield.text length]==0)
    {
        UIAlertView *myalert=[[UIAlertView alloc]initWithTitle:appname message:@"all fields are required" delegate:nil cancelButtonTitle:@"ok" otherButtonTitles: nil];
        [myalert show];
    }
    else if  ([changepasswordcell2.changetextfield.text isEqualToString:changepasswordcell3.changetextfield.text ] == FALSE)
    {
        UIAlertView *myAlert = [[UIAlertView  alloc]initWithTitle:appname message:@"Password not matched" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        
        [myAlert show];
        
        NSLog(@"Password not matched!!!");
    }
    else if ([changepasswordcell2.changetextfield.text length]>1 &&[changepasswordcell2.changetextfield.text length]<=5)
    {
        
        
            UIAlertView *myalert = [[UIAlertView alloc] initWithTitle:appname message:@"Password is too short." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:Nil, nil];
            [myalert show];
        
    }
  else
  {
      [myappDelegate startspinner:self.view];
      [profileRepos changepasswordUserid:userid currentpassword:changepasswordcell1.changetextfield.text newpassword:changepasswordcell2.changetextfield.text];
  }
    
    

}
-(void)successResponseforProfile:(NSDictionary *)responseDict
{
   
    //[myappDelegate stopspinner:self.view];
    NSLog(@"responsedict.....%@",responseDict);
    
    if ([[responseDict valueForKey:@"message"] isEqualToString:@"Change Password Successfully"])
    {
        //[AppDelegate alertmethod:appname :@"Your account has been deleted successfully, Thanks For using Markid place"];
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.001 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [myappDelegate stopspinner:self.view];
            UIAlertView *myalert=[[UIAlertView alloc]initWithTitle:appname message:@"Your password has been successfully changed." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [myalert show];
            
            
                   });
        
    }
    else if([[responseDict valueForKey:@"message"] isEqualToString:@"Invalid Current Password"])
    {
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.001 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            
            UIAlertView *myalert=[[UIAlertView alloc]initWithTitle:appname message:@"Your current password is incorrect." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [myalert show];
                [myappDelegate stopspinner:self.view];
        
         });
        
    }
    else
    {
        [myappDelegate stopspinner:self.view];
        UIAlertView *myalert=[[UIAlertView alloc]initWithTitle:appname message:@"Please contact admin, Thanks For using Markid place" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [myalert show];
        
    }

    
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    NSLog(@"return");
    
    [textField resignFirstResponder];
    return YES;
}
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    Currenttextfield = textField;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    Currenttextfield = nil;
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)internetConnection
{
    UIAlertView *Internetalert=[[UIAlertView alloc]initWithTitle:appname message:@"Internet connection not available" delegate:self cancelButtonTitle:nil otherButtonTitles:@"Ok", nil];
    [Internetalert show];
    [appdelegate stopspinner:self.view];
    
}
@end
