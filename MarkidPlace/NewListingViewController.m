//
//  NewListingViewController.m
//  MarkidPlace
//
//  Created by stellentmac1 on 5/21/14.
//  Copyright (c) 2014 stellentmac1. All rights reserved.
//

#import "NewListingViewController.h"
#import "ASIHTTPRequest.h"
#import "JSON.h"
#import"QuickTipsViewController.h"
@interface NewListingViewController ()

@end

@implementation NewListingViewController
@synthesize photosLabel,detailsLabel,categorytxtfeld,categoryNameLabel,sizeLabel,sizeNameLabel,brandLabel,brandNameLabel,conditionLabel,conditionNameLabel,descrptionLabel,descrptionNameLabel,shippingLabel,meetingLabel,markidLabel,priceLabel,sellLabel,originalLabel,listpriceLabel,earningsLabel,quicktipsLabel,ListingLabel,popview,poptitlelable,weight,createimageview;

@synthesize listingScrollView,categoryBtn,sizeBtn,conditionBtn,upscBtn,upscimgview,mettingBtn,mettingimgview,dropdownview;

@synthesize suggestionsVC;
@synthesize listingfirstimgview;
@synthesize photosScrollview;
@synthesize originalpriceTF;


@synthesize descriptionTF,listpriceTF,earningpriceTF,brandTF;
@synthesize itemnameTF;

@synthesize quantityTF,startDate,endDate,startdateTF,enddateTF,actionSheet,datePicker;

@synthesize itemnameLabel;
@synthesize availableLabel;
@synthesize startdateLabel;
@synthesize enddateLabel,cancelbtn;

@synthesize selecteditemobj,isupdate;

@synthesize successarray,con,mdata,photosarray,categorytableview,earningview,getweightstr;

@synthesize nlcustomcell,selltablearray;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
#pragma mark view methods
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self checkConnection];
   profilerepo=[[ProfileRepository alloc]init];
    profilerepo.delegate=self;
    catogeryotherstr=[NSString new];
    itemidstr=[NSString new];
    lbsArray=[[NSArray alloc]initWithObjects:@"0",@"1",@"2", @"3",@"4",@"5",@"6",@"7",@"8",@"9",@"10",nil ];
    ozArray=[[NSArray alloc]initWithObjects:@"0",@"1",@"2", @"3",@"4",@"5",@"6",@"7",@"8",@"9",@"10",@"11",@"12",@"13",@"14",@"15",nil ];
    

   
    //  APPDELEGATE = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    registrationRep=[[RegistrationRepository alloc]init];
    self.navbar.frame = CGRectMake(0, 0, 320, 64);
    [self.navbar setBackgroundImage:[UIImage imageNamed:@"navbar.png"] forBarMetrics:UIBarMetricsDefault];
    
    [self newListingMethod];
    
}
// this for loop is used to adding the objects in the array,and also load the table view.
-(void)newListingMethod
{
    isupsc=YES;
    ismetting=NO;
    upscimgview.image=[UIImage imageNamed:@"switch_on.png"];
    mettingimgview.image=[UIImage imageNamed:@"switch_off.png"];
    selltablearray=[NSMutableArray new];
  
    for (int i=0; i<5; i++)
    {
        Newmodelobject *obj=[[Newmodelobject alloc]init];
        if (i==0)
        {
            obj.isother=NO;
            obj.cellnamestr=@"Category";
            obj.cellvaluestr=@"";
            obj.otherstr=@"";
        }
        else if (i==1)
        {
            obj.isother=NO;
            obj.cellnamestr=@"Size";
            obj.cellvaluestr=@"";
            obj.otherstr=@"";
        }
        else if (i==2)
        {
            obj.isother=NO;
            obj.cellnamestr=@"Gender";
            obj.cellvaluestr=@"";
            obj.otherstr=@"";
        }

        else if (i==3)
        {
            obj.isother=NO;
            obj.cellnamestr=@"Brand";
            obj.cellvaluestr=@"";
            obj.otherstr=@"";
        }
      
        else if (i==4)
        {
            obj.isother=NO;
            obj.cellnamestr=@"Condition";
            obj.cellvaluestr=@"";
            obj.otherstr=@"";
        }
       /*
              else if (i==5)
        {
            obj.isother=NO;
            obj.cellnamestr=@"Weight (lb)";
            obj.cellvaluestr=@"";
            obj.otherstr=@"";
        }
        */
        [selltablearray addObject:obj];
    }
    
    [categorytableview reloadData];
    screenSize = [[UIScreen mainScreen]bounds];
   // NewListingViewController*newlist;
   // newlist.ListingLabel =@"Edit listing";//
    
   // NSLog(@"newlist.ListingLabel...%@",newlist.ListingLabel.text);//
    self.ListingLabel.font = [UIFont fontWithName:@"Monroe" size:18];
    self.photosLabel.font = [UIFont fontWithName:@"Muli" size:17];
    self.detailsLabel.font = [UIFont fontWithName:@"Muli" size:17];
    self.categorytxtfeld.font = [UIFont fontWithName:@"Muli" size:15];
    self.sizeLabel.font = [UIFont fontWithName:@"Muli" size:15];
    self.brandLabel.font = [UIFont fontWithName:@"Muli" size:15];
    self.conditionLabel.font = [UIFont fontWithName:@"Muli" size:15];
    self.descrptionLabel.font = [UIFont fontWithName:@"Muli" size:15];
    
    self.categoryNameLabel.font = [UIFont fontWithName:@"Muli" size:15];
    self.sizeNameLabel.font = [UIFont fontWithName:@"Muli" size:15];
    self.conditionNameLabel.font = [UIFont fontWithName:@"Muli" size:15];
    self.shippingLabel.font = [UIFont fontWithName:@"Muli" size:17];
    self.meetingLabel.font = [UIFont fontWithName:@"Muli" size:13];
    
    self.markidLabel.font = [UIFont fontWithName:@"Muli" size:13];
 
    self.priceLabel.font = [UIFont fontWithName:@"Muli" size:17];
    self.sellLabel.font = [UIFont fontWithName:@"Muli" size:13];
    self.originalLabel.font = [UIFont fontWithName:@"Muli" size:15];
    self.listpriceLabel.font = [UIFont fontWithName:@"Muli" size:15];
    self.earningsLabel.font = [UIFont fontWithName:@"Muli" size:15];
    self.quicktipsLabel.font = [UIFont fontWithName:@"Muli" size:14];
    
    subCategoryTextField.font = [UIFont fontWithName:@"Muli" size:15];
    subCategory_StatLbl.font = [UIFont fontWithName:@"Muli" size:15];
    
    self.lbslbl.font = [UIFont fontWithName:@"Muli" size:15];
    self.lbsweightlbl.font = [UIFont fontWithName:@"Muli" size:15];
    self.ozlbl.font = [UIFont fontWithName:@"Muli" size:15];
    self.ozweightlbl.font = [UIFont fontWithName:@"Muli" size:15];
    self.weightlbl.font = [UIFont fontWithName:@"Muli" size:15];
    
    poptitlelable.font= [UIFont fontWithName:@"Muli" size:15];
    categorytableview.scrollEnabled=FALSE;
    
    itemnameLabel.font = [UIFont fontWithName:@"Muli" size:15];
    availableLabel.font = [UIFont fontWithName:@"Muli" size:15];
    startdateLabel.font = [UIFont fontWithName:@"Muli" size:15];
    enddateLabel.font = [UIFont fontWithName:@"Muli" size:15];
   
    
    dropdownview.layer.borderWidth = 1;
    dropdownview.layer.cornerRadius=1.0f;
    
    dropdownview.layer.borderColor =[[UIColor whiteColor ] CGColor];
    
    //[[UIColor colorWithRed:12.0/255.0 green:174.0/255.0 blue:180.0/255.0 alpha:1.0] CGColor];
    
    
    
    actiondatelabel=[[UILabel alloc]init];
    actiondatelabel.backgroundColor=[UIColor clearColor];
    actiondatelabel.textAlignment=NSTextAlignmentCenter;
    
    
    subCategoryImgView.hidden=YES;
    subCategoryTextField.hidden=YES;
    subCategory_StatLbl.hidden=YES;
    subCategoryBtn.hidden=YES;
    subCategoryTextField.placeholder=@"Enter Your Category";
    categorytxtfeld.placeholder=@"";
    
    genderarray=[[NSMutableArray alloc]initWithObjects:@"Girl",@"Boy",@"Unisex",@"Women", nil];
    weightArray=[[NSMutableArray alloc]initWithObjects:@"Girl",@"Boy",@"Unisex",@"Women", nil];
    /* categorydropdownview.layer.borderWidth = 1;
     categorydropdownview.layer.cornerRadius=1.0f;
     categorydropdownview.layer.borderColor = [[UIColor colorWithRed:12.0/255.0 green:174.0/255.0 blue:180.0/255.0 alpha:1.0] CGColor];
     sizedropdownview.layer.borderWidth = 1;
     sizedropdownview.layer.cornerRadius=1.0f;
     sizedropdownview.layer.borderColor = [[UIColor colorWithRed:12.0/255.0 green:174.0/255.0 blue:180.0/255.0 alpha:1.0] CGColor];
     conditiondropdownview.layer.borderWidth = 1;
     conditiondropdownview.layer.cornerRadius=1.0f;
     conditiondropdownview.layer.borderColor = [[UIColor colorWithRed:12.0/255.0 green:174.0/255.0 blue:180.0/255.0 alpha:1.0] CGColor];*/
    
    // if (screenSize.size.height == 568)
    {
        [listingScrollView setContentSize:CGSizeMake(64,earningview.frame.origin.y+earningview.frame.size.height+200)];
    }
    // else
    {
        //[listingScrollView setContentSize:CGSizeMake(64, y)];
    }
    
    [self.view addSubview:popview];
    CGRect popfrm=popview.frame;
    popfrm.origin.x=0;
    popfrm.origin.y=595;
    popview.frame=popfrm;
    //    sizeArray=[[NSArray alloc]initWithObjects:@"Small",@"Medium",@"Large",@"Extra Large", nil];
    //    conditionArray=[[NSArray alloc]initWithObjects: @"New",@"1 month old",@"3 months old",@"6 months old",nil];
    //
    //    categoryArray=[[NSArray alloc]initWithObjects:@"Girls Clothing",@"Boys Clothing",@"Accessories",@"Shoes", nil];
    
    //   dropdownview.hidden=YES;
    [currentTextfield resignFirstResponder];
    [descriptionTF resignFirstResponder];
    
    
    currentimageview=[[UIImageView alloc]init];
    if ([APPDELEGATE isnullorempty:photosarray])
    {
        photosarray=[NSMutableArray new];
    }
    
    descriptionTF.layer.borderWidth = 1;
    descriptionTF.layer.cornerRadius=0.5;
    descriptionTF.layer.borderColor =[[UIColor colorWithRed:12.0f/255.0f green:174.0f/255.0f blue:180.0f/255.0f alpha:1.0f] CGColor];
    
    dropdownview.backgroundColor=[UIColor clearColor];
    //  descriptionTF.backgroundColor=[UIColor greenColor];
    
    
    //  quantityTF.text=@"For Sale";
    //  isforsale=YES;
    
    //
    //    ismetting=NO;
    //isupsc=YES;
    //
    //
    //    if(isupsc==YES)
    //    {
    //        upscimgview.image=[UIImage imageNamed:@"switch_on.png"];
    //    }
    //    else
    //    {
    //        upscimgview.image=[UIImage imageNamed:@"switch_off.png"];
    //    }
    //
    //    if(ismetting==YES)
    //    {
    //        mettingimgview.image=[UIImage imageNamed:@"switch_on.png"];
    //    }
    //    else
    //    {
    //        mettingimgview.image=[UIImage imageNamed:@"switch_off.png"];
    //    }
    
    
    UILabel *dollerlbl1=[[UILabel alloc]initWithFrame:CGRectMake(0, 0, 10, 20)];
    dollerlbl1.textColor=originalpriceTF.textColor;
    dollerlbl1.font=originalpriceTF.font;
    dollerlbl1.text=@"$";
    
    [originalpriceTF setLeftViewMode:UITextFieldViewModeAlways];
    originalpriceTF.leftView= dollerlbl1;
    
    
    UILabel *dollerlbl2=[[UILabel alloc]initWithFrame:CGRectMake(0, 0, 10, 20)];
    dollerlbl2.textColor=originalpriceTF.textColor;
    dollerlbl2.font=originalpriceTF.font;
    dollerlbl2.text=@"$";
    [listpriceTF setLeftViewMode:UITextFieldViewModeAlways];
    
    listpriceTF.leftView= dollerlbl2;
    poptitlelable.font=[AppDelegate navtitlefont];
    cancelbtn.titleLabel.font = [UIFont fontWithName:@"Muli" size:14];

    [self performSelector:@selector(getDropDownData) withObject:nil afterDelay:0.1];
}

/************* this method is implemented for animating dropdown table view  ***************/
-(void)popViewMethod :(BOOL)isshow
{
    CGRect popfrm=popview.frame;
    popfrm.origin.x=0;
    if (isshow)
    {
        // [self.view addSubview:popview];
        popfrm.origin.y=0;
        
        
        CATransition *transition = [CATransition animation];
        transition.duration = 0.3;
        transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
        transition.type = kCATransitionPush;
        transition.subtype =kCATransitionFromTop;
        transition.delegate = self;
        [popview.layer addAnimation:transition forKey:nil];
    }
    else
    {
        popfrm.origin.y=605;
        CATransition *transition = [CATransition animation];
        transition.duration = 0.3;
        transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
        transition.type = kCATransitionPush;
        transition.subtype =kCATransitionFromBottom;
        transition.delegate = self;
        [popview.layer addAnimation:transition forKey:nil];
    }
    
    //[UIView beginAnimations: @"anim" context: nil];
    //[UIView setAnimationBeginsFromCurrentState: YES];
    // [UIView setAnimationDuration:0.3];
    popview.frame=popfrm;
    // [UIView commitAnimations];
    [self.view bringSubviewToFront:popview];
    
    
}

- (void)viewDidAppear:(BOOL)animated
{
    // amazone setup
    //********Amazon Webservice********//
    [categorytableview reloadData];
    CGRect frmtable=categorytableview.frame;
    frmtable.size.height=categorytableview.contentSize.height;
    categorytableview.frame=frmtable;
    
    
    CGRect erngviewfrm=earningview.frame;
    erngviewfrm.origin.y=frmtable.size.height+frmtable.origin.y+20;
    earningview.frame=erngviewfrm;
    
    
    [listingScrollView setContentSize:CGSizeMake(64,earningview.frame.origin.y+earningview.frame.size.height+200)];
}



- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}
//*********************this method is using
-(void)getDropDownData
{
    registrationRep=[[RegistrationRepository alloc ]init];
    
    registrationRep.delegate=self;
    
    if (APPDELEGATE.catogoryGlobalarray.count<1 || APPDELEGATE.conditionGlobalarray.count<1 || APPDELEGATE.sizeGlobalarray.count<1 || APPDELEGATE.brandGlobalarray.count<1)
    {
        [APPDELEGATE startspinner:self.view];
        [ registrationRep getdata:nil :@"getcatogery" :@"GET" withcount:@"0"];
    }
    
    
}

-(void)viewWillAppear:(BOOL)animated
{
    
    CGRect frmtable=categorytableview.frame;
    frmtable.size.height=categorytableview.contentSize.height;
    categorytableview.frame=frmtable;
    
    
    CGRect erngviewfrm=earningview.frame;
    erngviewfrm.origin.y=frmtable.size.height+frmtable.origin.y+20;
    earningview.frame=erngviewfrm;
    
    
    [listingScrollView setContentSize:CGSizeMake(64,earningview.frame.origin.y+earningview.frame.size.height+200)];
    
    if (isgalleryapear==NO)
    {
        
        Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
        NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
        if (networkStatus == NotReachable)
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                
                UIAlertView *Internetalert=[[UIAlertView alloc]initWithTitle:appname message:@"Internet connection not available" delegate:self cancelButtonTitle:nil otherButtonTitles:@"Ok", nil];
                [APPDELEGATE stopspinner:self.view];
                [Internetalert show];
            });
            
            ////NSLog(@"There IS NO internet connection");
        }
        else
        {

        if (isupdate)
        {
            createimageview.image=[UIImage imageNamed:@"Updatelist.png"];
            [APPDELEGATE startspinner:self.view];
             [photosarray removeAllObjects];
            self.ListingLabel.text=@"Edit Listing Details";
            
            double delayInSeconds =0.02;
            
            dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
            dispatch_after(popTime, dispatch_get_main_queue(), ^(void)
                           {
                               
                                    itemnameTF.text=selecteditemobj.itemNamestr;
                               categorytxtfeld.text=selecteditemobj.catogrystr;
                               subCategory_StatLbl.text=selecteditemobj.catogrystr;
                               sizeLabel.text=selecteditemobj.sizestr;
                               brandTF.text=selecteditemobj.brandstr;
                               conditionLabel.text=selecteditemobj.conditionstr;
                               
                               if (selecteditemobj.forsalebool == YES)
                               {
                                   //  quantityTF.text=@"For Sale";
                                   //  isforsale=YES;
                                   
                               }
                               else{
                                   // quantityTF.text=@"Not For Sale";
                                   //  isforsale=NO;
                               }
                               
                               isupsc=selecteditemobj.uspsbool;
                               ismetting=selecteditemobj.inpersonbool;
                               
                               if(isupsc==YES)
                               {
                                   
                                   upscimgview.image=[UIImage imageNamed:@"switch_on.png"];
                                   
                               }
                               else
                               {
                                   
                                   
                                   upscimgview.image=[UIImage imageNamed:@"switch_off.png"];
                                   
                               }
                               
                               if(ismetting==YES)
                               {
                                   
                                   mettingimgview.image=[UIImage imageNamed:@"switch_on.png"];
                               }
                               else
                               {
                                   
                                   
                                   mettingimgview.image=[UIImage imageNamed:@"switch_off.png"];
                                   
                               }
                               
                               
                               
                               descriptionTF.text=selecteditemobj.descriptionstr;
                               originalpriceTF.text=[NSString stringWithFormat:@"%@",selecteditemobj.listingcoststr];
                               listpriceTF.text=[NSString stringWithFormat:@"%@",selecteditemobj.salecostcoststr];
                               
                               [self earningmoneymethod:selecteditemobj.salecostcoststr];
                               
                               
                               
                               for (int i=0; i<selecteditemobj.picturearray.count; i++)
                               {
                                   
                                   id classid=[selecteditemobj.picturearray objectAtIndex:i];
                                   
                                   if ([classid isKindOfClass:[NSString class]])
                                   {
                                       NSString *urlstr=[selecteditemobj.picturearray objectAtIndex:i];
                                       NSURL *imgurl=[NSURL URLWithString:urlstr];
                                       
                                       
                                       UIImage *itemimage=[UIImage imageWithData:[NSData dataWithContentsOfURL:imgurl]];
                                       
                                       [photosarray addObject:itemimage];
                                   }
                                   else if ([classid isKindOfClass:[UIImage class]])
                                   {
                                       [photosarray addObject:[selecteditemobj.picturearray objectAtIndex:i]];
                                   }
                                   
                                   
                               }
                               
                               for (int i=0; i<5; i++)
                               {
                                   Newmodelobject *obj=[selltablearray objectAtIndex:i];
                                   
                                   if (i==0)
                                   {
                                       BOOL isother=YES;
                                       
                                       
                                       for (int j=0; j<APPDELEGATE.catogoryGlobalarray.count; j++)
                                       {
                                           
                                           LookupModelobject *lookobj=[APPDELEGATE.catogoryGlobalarray objectAtIndex:j];
                                           // NSLog(@" check the cat data =%@= %@",lookobj,appdelegate.catogoryGlobalarray);
                                           BOOL isbreak=NO;
                                           for (int k=0; k<lookobj.categoryDataArray.count; k++)
                                           {
                                               LookupModelobject *obj23=[lookobj.categoryDataArray objectAtIndex:k];
                                               
                                               if ([selecteditemobj.catogrystr isEqualToString:obj23.loolupValue])
                                               {
                                                   isother=NO;
                                                   obj.isother=NO;
                                                   obj.cellvaluestr=obj23.loolupValue;
                                                   catidstr=obj23.loolupid;
                                                   isbreak=YES;
                                                   

                                                   break;
                                               }
                                               
                                           }
                                           if (isbreak==YES)
                                           {
                                               break;
                                           }
                                           
                                           
                                       }
                                       
                                       if (isother==YES)
                                       {
                                           obj.isother=YES;
                                           obj.cellvaluestr=@"Other";
                                           obj.otherstr=selecteditemobj.catogrystr;
                                           catogeryotherstr=selecteditemobj.catogryIdstr;
                                           parentid=selecteditemobj.catogryIdstr;
                                           NSLog(@"the id ster is %@",catogeryotherstr);
                                             NSLog(@"the id ster is %@",selecteditemobj.catogryIdstr);
                                           isothercat=YES;
                                           
                                       }
                                       
                                       obj.cellnamestr=@"Category";
                                       
                                   }
                                   else if (i==1)
                                   {
                                       
                                       BOOL isother=YES;
                                       for (int k=0; k<APPDELEGATE.sizeGlobalarray.count; k++)
                                       {
                                           
                                           LookupModelobject *obj23=[APPDELEGATE.sizeGlobalarray objectAtIndex:k];
                                           
                                           if ([selecteditemobj.sizestr isEqualToString:obj23.loolupValue])
                                           {
                                               isother=NO;
                                               obj.isother=NO;
                                               obj.cellvaluestr=obj23.loolupValue;
                                               break;
                                           }
                                           
                                       }
                                       if (isother==YES)
                                       {
                                           isother=NO;
                                           obj.isother=NO;
                                         //  obj.cellvaluestr=@"Other";
                                           obj.cellvaluestr=selecteditemobj.sizestr;
                                         //  obj.otherstr=selecteditemobj.sizestr;
                                if ([obj.cellvaluestr isEqualToString:@"Not Applicable"])
                                           {
                                               isother=NO;
                                               obj.isother=NO;
                                           }
                                       }
                                       
                                       obj.cellnamestr=@"Size";
                                       
                                   }
                                   else if (i==3)
                                   {
                                       BOOL isother=YES;
                                       for (int k=0; k<APPDELEGATE.brandGlobalarray.count; k++)
                                       {
                                           LookupModelobject *obj23=[APPDELEGATE.brandGlobalarray objectAtIndex:k];
                                           
                                           if ([selecteditemobj.brandstr isEqualToString:obj23.loolupValue])
                                           {
                                               isother=NO;
                                               obj.isother=NO;
                                               
                                               obj.cellvaluestr=obj23.loolupValue;
                                               
                                               break;
                                           }
                                           
                                       }
                                       if (isother==YES)
                                       {
                                           obj.isother=YES;
                                           obj.cellvaluestr=@"Other";
                                           obj.otherstr=selecteditemobj.brandstr;
                                       }
                                       obj.cellnamestr=@"Brand";
                                       
                                   }
                                   else if (i==4)
                                   {
                                       
                                       BOOL isother=YES;
                                       for (int k=0; k<APPDELEGATE.conditionGlobalarray.count; k++)
                                       {
                                           LookupModelobject *obj23=[APPDELEGATE.conditionGlobalarray objectAtIndex:k];
                                           
                                           if ([selecteditemobj.conditionstr isEqualToString:obj23.loolupValue])
                                           {
                                               isother=NO;
                                               obj.isother=NO;
                                               obj.cellvaluestr=obj23.loolupValue;
                                               
                                               break;
                                           }
                                           
                                       }
                                       if (isother==YES)
                                       {
                                           obj.isother=YES;
                                           obj.cellvaluestr=@"Other";
                                           obj.otherstr=selecteditemobj.conditionstr;
                                       }
                                       obj.cellnamestr=@"Condition";
                                       
                                   }
                                   
                                   else if (i==2)
                                   {
                                       obj.isother=NO;
                                       obj.cellnamestr=@"Gender";
                                       obj.cellvaluestr=selecteditemobj.genderstring;
                                       obj.otherstr=@"";
                                   }
                                   else if (i==5)
                                   {
                                       obj.isother=NO;
                                       obj.cellnamestr=@"Weight (lb)";
                                       obj.cellvaluestr=@"";
                                     //  _lbsweightlbl.text=selecteditemobj.weightstring;
                                     //  _ozweightlbl.text=selecteditemobj.weightstring;
                                    
                                    
                                   }
                                   // [selltablearray addObject:obj];
                               }
                                NSLog(@"the weight is in the weight listing is %@",weight);
                               [registrationRep getweightnewlisting:weight];
                               CGFloat weightfloat=[weight floatValue];
                               weight=[NSString stringWithFormat:@"%0.02f",weightfloat];
                               NSArray *mainArray=[weight componentsSeparatedByString:@"."];
                               NSLog(@"the weight is in the weight listing is %@",weight);
                             
                                
                               
                               _lbsweightlbl.text=[mainArray objectAtIndex:0];
                               _ozweightlbl.text=[mainArray objectAtIndex:1];
                               
                 // [registrationRep getweightnewlisting:weight];
                               [self createphotosscrollview];
                               [APPDELEGATE stopspinner:self.view];
                               
                               
                               // [self performSelector:@selector(getdropdowndata) withObject:nil afterDelay:0.5];
                           });
            
        }
        else
        {
            //  [self getdropdowndata];
            [self createphotosscrollview];
        }
    }
    }
    
}

#pragma mark button actions

//navigation bar backbutton action
-(IBAction)backAction:(id)sender
{
    [currentTextfield resignFirstResponder];
    isgalleryapear=NO;
    [self.navigationController popViewControllerAnimated:YES];
}

//this action performs pops suggession view controller
-(IBAction)homeBtnAction:(id)sender
{
    [currentTextfield resignFirstResponder];
    isgalleryapear=NO;
    
    NSArray *arr=self.navigationController.viewControllers;
    for (UIViewController *childvc in arr) {
        
        if([childvc isKindOfClass:[SuggestionsViewController class]])
        {
            SuggestionsViewController *subvc=(SuggestionsViewController *)childvc;
            subvc.ismyfeed=NO;
            [self.navigationController popToViewController:subvc animated:YES];
        }
    }
    
}
-(IBAction)subCategoryBtnAction:(id)sender
{
    dropdownview.tag=[sender tag];
}
-(IBAction)quicktipsbtnunderaction:(id)sender
{
    
    QuickTipsViewController *quicktipsVc=[[QuickTipsViewController alloc]initWithNibName:@"QuickTipsViewController" bundle:nil];
    quicktipsVc.presentstr=@"present";
    quicktipsVc.htmlMainString=@"quicktips";
    quicktipsVc.titleString=@"quicktips";
    //[APPDELEGATE.naviCon presentViewController:quicktipsVc animated:YES completion:nil];
     [self.navigationController pushViewController:quicktipsVc animated:YES];
    
}

-(IBAction)quicktipsbtnaction:(id)sender
{
    //FeePolicyViewController *feepolicy=[[FeePolicyViewController alloc]initWithNibName:@"FeePolicyViewController" bundle:nil];
    QuickTipsViewController *quicktipsVc=[[QuickTipsViewController alloc]initWithNibName:@"QuickTipsViewController" bundle:nil];
    quicktipsVc.presentstr=@"present";
    quicktipsVc.htmlMainString=@"quicktips";
    quicktipsVc.titleString=@"shipping-weights";
   // quicktipsVc.homebtn.hidden=YES;
    //[APPDELEGATE.naviCon presentViewController:quicktipsVc animated:YES completion:nil];
    [self.navigationController pushViewController:quicktipsVc animated:YES];
}
//this button action performs assigining the title label to the dropdown view
-(IBAction)categoryBtnAction:(id)sender
{
    
    if([sender tag]==111)
    {
        poptitlelable.text=@"Category";
        dropdownview.frame=CGRectMake(16, 61, 289, 483);
        sizeBool=YES;

    }
    else if ([sender tag]==222)
    {
        poptitlelable.text=@"Size";
        dropdownview.frame=CGRectMake(16, 61, 289, 483);
    }
    
    else if ([sender tag]==444)
    {
        poptitlelable.text=@"Brand";
        dropdownview.frame=CGRectMake(16, 61, 289, 483);
         sizeBool=YES;
    }
    else  if ([sender tag]==333)
    {
        poptitlelable.text=@"Condition";
        dropdownview.frame=CGRectMake(16, 61, 289, 195);
         sizeBool=YES;
    }
    else if ([sender tag]==777)
    {
        poptitlelable.text=@"Gender";
        dropdownview.frame=CGRectMake(16, 61, 289, 159);
         sizeBool=YES;
    }
    
    else  if ([sender tag]==1234)
    {
        poptitlelable.text=@"lbs";
        dropdownview.frame=CGRectMake(16, 61, 289, 483);
         sizeBool=YES;
    }
    else if ([sender tag]==5678)
    {
        poptitlelable.text=@"oz";
        dropdownview.frame=CGRectMake(16, 61, 289, 483);
         sizeBool=YES;
    }
    /*
     if(_lbsweightlbl.text.length>0||_ozlbl.text.length>0)
     {
         NSString *weightstr=[NSString stringWithFormat:@"%@.%@",_lbsweightlbl.text,_ozlbl.text];
        4f NSLog(@"weight....%@",weightstr);
         [registrationRep getweightnewlisting:weightstr];
     }
     */
    if(sizeBool==YES)
    {
    [self popViewMethod:YES];
    [currentTextfield resignFirstResponder];
    [descriptionTF resignFirstResponder];
    dropdownview.tag=[sender tag];
    }
    
    if(iscategory==YES)
    {
        
        iscategory=NO;
    }
    else
    {
        
        float tableheight=300;
        
        if(categorytxtfeld.text.length>0)
        {
            
        }
        else
        {
            
        }
        
        
        iscategory=YES;
    }
    [dropdownview reloadData];
}

-(IBAction)sizeBtnAction:(id)sender
{
    
    [currentTextfield resignFirstResponder];
    [descriptionTF resignFirstResponder];
    
    [self popViewMethod:YES];
    
    dropdownview.tag=[sender tag];
    if ([sender tag]==333)
    {
        poptitlelable.text=@"Condition";
        float tableheight=APPDELEGATE.conditionGlobalarray.count*40;
        
        if (tableheight>300)
        {
            tableheight=300;
        }
        else if (tableheight<60)
        {
            tableheight=100;
        }
        
    }
    else if ([sender tag]==222)
    {
        poptitlelable.text=@"Size";
        
        float tableheight=APPDELEGATE.sizeGlobalarray.count*40;
        
        if (tableheight>300)
        {
            tableheight=300;
        }
        else if (tableheight<60)
        {
            tableheight=100;
        }
        
    }
    
    else if ([sender tag]==777)
    {
        poptitlelable.text=@"Gender";
    }
    else
    {
        poptitlelable.text=@"Brand";
        
        
    }
    [dropdownview reloadData];
    
}
-(IBAction)conditionBtnAction:(id)sender
{
    poptitlelable.text=@"Condition";
    if ([sender tag]==777)
    {
        poptitlelable.text=@"Gender";
    }
    
    [self popViewMethod:YES];
    [currentTextfield resignFirstResponder];
    [descriptionTF resignFirstResponder];
    
    dropdownview.tag=[sender tag];
    UIButton *mybtn=(UIButton *)sender;
    
    float tableheight=APPDELEGATE.conditionGlobalarray.count*40;
    
    if (tableheight>250)
    {
        tableheight=250;
    }
    else if (tableheight<60)
    {
        tableheight=100;
    }
    
    [dropdownview reloadData];
    
}
-(IBAction)upscAction:(id)sender
{
    [currentTextfield resignFirstResponder];
    [descriptionTF resignFirstResponder];
    
    if(isupsc==YES)
    {
        isupsc=NO;
        ismetting=YES;
        upscimgview.image=[UIImage imageNamed:@"switch_off.png"];
        
        mettingimgview.image=[UIImage imageNamed:@"switch_on.png"];
        
        
    }
    else
    {
        isupsc=YES;
        ismetting=NO;
        upscimgview.image=[UIImage imageNamed:@"switch_on.png"];
        mettingimgview.image=[UIImage imageNamed:@"switch_off.png"];
        
        
    }
    
    
    
}
//here action  performs the images on or off
-(IBAction)mettingBtnAction:(id)sender
{
    [currentTextfield resignFirstResponder];
    [descriptionTF resignFirstResponder];
    
    if(ismetting==YES)
    {
        ismetting=NO;
        
        isupsc=YES;
        mettingimgview.image=[UIImage imageNamed:@"switch_off.png"];
        upscimgview.image=[UIImage imageNamed:@"switch_on.png"];
    }
    else
    {
        ismetting=YES;
        isupsc=NO;
        mettingimgview.image=[UIImage imageNamed:@"switch_on.png"];
        upscimgview.image=[UIImage imageNamed:@"switch_off.png"];
        
    }
    
    
    
}


// this action sheet is using go to the gallery and camera
-(IBAction)camraBtnAction:(id)sender
{
    
    [currentTextfield resignFirstResponder];
    [descriptionTF resignFirstResponder];
    
    imgtag=[sender tag];
    CGRect frame=self.view.frame;
    frame.origin.y=0;
    self.view.frame=frame;
    [UIView commitAnimations];
    
    UIActionSheet *action = [[UIActionSheet alloc]initWithTitle:@"Listing Image" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Camera",@"Gallery", nil];
    [action showInView:self.view];
    
}

-(IBAction)ActionBtnAction:(id)sender
{
    UIActionSheet *pickersheet=[[UIActionSheet alloc]initWithTitle:nil delegate:self cancelButtonTitle:@"cancel" destructiveButtonTitle:nil otherButtonTitles:nil, nil];
    UIView *pickerview=[[UIView alloc]init];
    [pickerview addSubview:pickersheet];
    UIButton *donebtn=[[UIButton alloc]init];
    donebtn=[UIButton buttonWithType:UIButtonTypeCustom];
    //UIButton *donebtn=[[UIButton alloc]initWithFrame:CGRectMake(0, 10, 55, 30)];
    donebtn.frame=CGRectMake(0, 10, 55, 30);
    donebtn.titleLabel.text=@"Done";
    
}


//this action performing cancel the all dropdown actions
-(IBAction)cancelBtnAction:(id)sender
{
    if (dropdownview.tag==111)
    {
        categorytxtfeld.text=@"";
        subCategoryTextField.text=@"";
        
    }
    else if (dropdownview.tag==222)
    {
        sizeLabel.text=@"";
    }
    else if (dropdownview.tag==333)
    {
        conditionLabel.text=@"";
    }
    else if (dropdownview.tag==444)
    {
        brandTF.text=@"";
    }
    
    
    isothercat=NO;
    subCategoryTextField.text=@"";
    // catogeryotherstr=@"";
    
    subCategoryImgView.hidden=YES;
    subCategoryTextField.hidden=YES;
    subCategory_StatLbl.hidden=YES;
    subCategoryBtn.hidden=YES;
    
    photosLabel.frame=CGRectMake(13,20,138,21);
    photosScrollview.frame=CGRectMake(10,45,300,86);//86
    detailsLabel.frame=CGRectMake(13,141,147,21);
    
    itemnameLabel.frame=CGRectMake(13,177,98,30);
    
    itemnameImageView.frame=CGRectMake(123,177,182,30);
    
    itemnameTF.frame=CGRectMake(125,177,174,30);
    
    categoryNameLabel.frame=CGRectMake(13,212,98,30);
    
    categorytxtfeld.frame=CGRectMake(128,212,151,30);
    
    categoryBtn.frame=CGRectMake(123,215,184,30);
    
    categoryImageView.frame=CGRectMake(124,212,182,30);
    
    // [popview removeFromSuperview];
    [self popViewMethod:NO];
}

-(IBAction)categorybtncellaction:(id)sender
{
    [currentTextfield resignFirstResponder];
    if ([sender tag]==111 )
    {
        [self categoryBtnAction:sender];
    }
    else if ([sender tag] == 777 || [sender tag]== 333)
    {
        [self conditionBtnAction:sender];
    }
    else
    {
        
        // 222 // 444
        [self sizeBtnAction:sender];
    }
    
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark scrollView Delegate Methods
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (scrollView==dropdownview && dropdownview.tag==111)
    {
        CGFloat sectionHeaderHeight = 30;
        if (scrollView.contentOffset.y<=sectionHeaderHeight&&scrollView.contentOffset.y>=0) {
            scrollView.contentInset = UIEdgeInsetsMake(-scrollView.contentOffset.y, 0, 0, 0);
        }
        else if (scrollView.contentOffset.y>=sectionHeaderHeight)
        {
            scrollView.contentInset = UIEdgeInsetsMake(-sectionHeaderHeight, 0, 0, 0);
        }
    }
    else
    {
        CGFloat sectionHeaderHeight = 0;
        if (scrollView.contentOffset.y<=sectionHeaderHeight&&scrollView.contentOffset.y>=0) {
            scrollView.contentInset = UIEdgeInsetsMake(-scrollView.contentOffset.y, 0, 0, 0);
        }
        else if (scrollView.contentOffset.y>=sectionHeaderHeight)
        {
            scrollView.contentInset = UIEdgeInsetsMake(-sectionHeaderHeight, 0, 0, 0);
        }
        //       else if ((dropdownview.contentOffset.y <0))
        //        {
        //            [dropdownview setScrollEnabled:NO];
        //
        //        }
    }
}
#pragma mark tableView Delegate Methods
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (categorytableview==tableView)
    {
        
        Newmodelobject *myobj=[selltablearray objectAtIndex:indexPath.row];
        
        if (myobj.isother==YES)
        {
            return 85;
        }
        else{
            
            return 40;
            
        }
    }
    else
    {
        return 40;
    }
    
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if (tableView.tag==111)
    {
        return APPDELEGATE.catogoryGlobalarray.count;
    }
    else
        return 1;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (tableView.tag==111)
    {
        return 30;
    }
    else
    {
        
        
        return 0;
    }
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if (tableView.tag==111)
    {
        UIView *sectionview=[[UIView alloc] initWithFrame:CGRectMake(0,0,320,30)];
        UILabel *lbl=[[UILabel alloc] initWithFrame:CGRectMake(5,0,320,25)];
        
        [sectionview addSubview:lbl];
        lbl.font = [UIFont fontWithName:@"Muli" size:17];
        
        LookupModelobject *lookobj=[APPDELEGATE.catogoryGlobalarray objectAtIndex:section];
        lbl.text=lookobj.loolupValue;
        lbl.textColor=[UIColor colorWithRed:(12/255.f) green:(180/255.f) blue:(174/255.f) alpha:1.0];
        sectionview.backgroundColor=[UIColor whiteColor];
        return sectionview;
    }
    return nil;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    if (tableView == dropdownview)
    {
        if (tableView.tag==111)
        {
            //lookobj.categoryDataArray
            LookupModelobject *lookobj=[APPDELEGATE.catogoryGlobalarray objectAtIndex:section];
            return lookobj.categoryDataArray.count;
        }
        else if (tableView.tag==222)
        {
            return APPDELEGATE.sizeGlobalarray.count;
        }
        else if (tableView.tag==333)
        {
            return APPDELEGATE.conditionGlobalarray.count;
        }
        else if (tableView.tag==444)
        {
            return APPDELEGATE.brandGlobalarray.count;
        }
        else if (tableView.tag==777)
        {
            return genderarray.count;
        }
        else if (tableView.tag==1234)
        {
            return lbsArray.count;
        }
        else if (tableView.tag==5678)
        {
            return ozArray.count;
        }

        
        else
        {
            return 1;
        }
    }
    else
    {
        return selltablearray.count;
    }
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (tableView==dropdownview)
    {
        UITableViewCell *tbcell=[tableView dequeueReusableCellWithIdentifier:@"cell"];
        if(tbcell==nil)
        {
            tbcell=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
        }
        tbcell.textLabel.font = [UIFont fontWithName:@"Muli" size:15];
        
        tbcell.textLabel.textColor=[UIColor whiteColor];
        
        tbcell.textLabel.backgroundColor=[UIColor clearColor];
        tbcell.selectionStyle=UITableViewCellSelectionStyleNone;
        tbcell.contentView.backgroundColor=[UIColor clearColor];
        tbcell.backgroundColor=[UIColor clearColor];
        
        //this statements using drop down data to table view.
        if (tableView.tag==111)
        {
            
            
            LookupModelobject *lookobj=[APPDELEGATE.catogoryGlobalarray objectAtIndex:indexPath.section];
            
            LookupModelobject *obj=[lookobj.categoryDataArray objectAtIndex:indexPath.row];
            
            tbcell.textLabel.text=obj.loolupValue;
            
            return tbcell;
            
        }
        else if (tableView.tag==222)
        {
            LookupModelobject *lookobj=[APPDELEGATE.sizeGlobalarray objectAtIndex: indexPath.row];
            
            tbcell.textLabel.text=lookobj.loolupValue;
            
            
            // tbcell.textLabel.textColor=[UIColor colorWithRed:131.0/255.0 green:131.0/255.0 blue:131.0/255.0 alpha:1.0];
            return tbcell;
        }
        else if (tableView.tag==333)
        {
            LookupModelobject *lookobj=[APPDELEGATE.conditionGlobalarray objectAtIndex: indexPath.row];
            
            tbcell.textLabel.text=lookobj.loolupValue;
            return tbcell;
            
            
        }
        else if (tableView.tag==444)
        {
            LookupModelobject *lookobj=[APPDELEGATE.brandGlobalarray objectAtIndex: indexPath.row];
            
            tbcell.textLabel.text=lookobj.loolupValue;
            return tbcell;
            
            
        }
        
        else if (tableView.tag==777)
        {
            LookupModelobject *lookobj=[APPDELEGATE.genderglobalaray objectAtIndex: indexPath.row];
            
            tbcell.textLabel.text=lookobj.loolupValue;
            
            return tbcell;
        }
        else if (tableView.tag==1234)
        {
           tbcell.textLabel.text=[lbsArray objectAtIndex: indexPath.row];
             return tbcell;
        }
        else if (tableView.tag==5678)
        {
            tbcell.textLabel.text=[ozArray objectAtIndex: indexPath.row];
             return tbcell;
        }
          else
        {
            return tbcell;
        }
    }
    //here statements using category tableview loading
    else if (tableView==categorytableview)
    {
        static NSString *cellidentifier=@"cell222";
        NewListingCustomcell *cell=(NewListingCustomcell *)[tableView dequeueReusableCellWithIdentifier:cellidentifier];
        if (cell==nil)
        {
            [[NSBundle mainBundle]loadNibNamed:@"NewListingCustomcell" owner:self options:nil];
            cell=self.nlcustomcell;
            
        }
        
        Newmodelobject *cellobj=[selltablearray objectAtIndex:indexPath.row];
        cell.categorylbl.font = [UIFont fontWithName:@"Muli" size:15];
        cell.categorynamelbl.font=[UIFont fontWithName:@"Muli" size:15];
        cell.othertextfield.font=[UIFont fontWithName:@"Muli" size:15];
        
        cell.categorylbl.text=cellobj.cellnamestr;
        cell.categorynamelbl.text=cellobj.cellvaluestr;
        
        
        if (cellobj.isother)
        {
            cell.othertextfield.text=cellobj.otherstr;
        }
        
        NSLog(@"check ht eindex path  %ld",(long)indexPath.row);
        
        
        if (indexPath.row==0)
        {
            cell.catdropdownbutton.tag=111;
            cell.othertextfield.placeholder=@"Enter Category";
            
        }
        else  if (indexPath.row==1)
        {
            
            cell.catdropdownbutton.tag=222;
            cell.othertextfield.placeholder=@"Enter Size";
           
        }
        else  if (indexPath.row==2)
        {
            cell.catdropdownbutton.tag=777;
        }

        else  if (indexPath.row==3)
        {
            cell.catdropdownbutton.tag=444;
            cell.othertextfield.placeholder=@"Enter Brand";
        }
        else  if (indexPath.row==4)
        {
            cell.catdropdownbutton.tag=333;
            cell.othertextfield.placeholder=@"Enter Condition";
        }
              /* else  if (indexPath.row==5)
        {
            
            // cell.catdropdownbutton.tag=111;
            cell.catdropdownbutton.hidden=YES;
            
            CGRect textviewfrm=cell.othertextfield.frame;
            
            textviewfrm.origin.y=cell.catdropdownbutton.frame.origin.y;
            cell.othertextfield.frame=textviewfrm;
            
            cell.categorynamelbl.hidden=YES;
            
            cell.othertextfield.placeholder=@"Enter Weight";
            cell.othertextfield.text=cellobj.otherstr;
            cell.othertextfield.keyboardType=UIKeyboardTypeNumbersAndPunctuation;
            cell.catdropdownbutton.tag=888;
            
            cell.dropdownimgv.image=[UIImage imageNamed:@"textfield.png"];
            cell.othertextfield.backgroundColor=[UIColor clearColor];
        }
        */
        cell.othertextfield.tag=indexPath.row;
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
        return cell;
    }
    return nil;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // [popview removeFromSuperview];
    //here statements  are using dropdown tableview loading
      if (tableView==dropdownview)
    {
        
        if (tableView.tag==111)
        {
            LookupModelobject *lookobj=[APPDELEGATE.catogoryGlobalarray objectAtIndex:indexPath.section];
            
            LookupModelobject *obj=[lookobj.categoryDataArray objectAtIndex:indexPath.row];
            
            NSLog(@"check theval ==%@==",obj.loolupValue);
            
            categorytxtfeld.text=obj.loolupValue;
            categorytxtfeld.text=[categorytxtfeld.text stringByReplacingOccurrencesOfString:@" " withString:@""];
            NSLog(@"***** categorytxtfeld.text ****** =%@=",categorytxtfeld);
            iscategory=NO;
            
            Newmodelobject *modobj=[selltablearray objectAtIndex:0];
            
            if ([obj.loolupValue isEqualToString:@"Other"])
            {
                iscategory=YES;
                catogeryotherstr=obj.parentcatid;
                modobj.isother=YES;
                modobj.cellvaluestr=obj.loolupValue;
                catidstr=obj.categoryid;
                
                _ozweightlbl.text=@"0";
            }
            else
            {
                modobj.isother=NO;
                modobj.cellvaluestr=obj.loolupValue;
                catidstr=obj.loolupid;
                _ozweightlbl.text=@"0";
                NSLog(@"other tect parent cat %@ ",obj.loolupid);
                }
            parentid=[NSString stringWithFormat:@"%@",obj.parentcatid];
            NSLog(@"the parientid in the category is %@",parentid);
            
            if([parentid isEqualToString:@"17"]||[parentid isEqualToString:@"23"]||[parentid isEqualToString:@"59"])
            {
            LookupModelobject *lookobj=[APPDELEGATE.sizeGlobalarray objectAtIndex: 0];
                 Newmodelobject *modobj=[selltablearray objectAtIndex:1];
                lookobj.loolupValue=@"Not Applicable";
                modobj.cellvaluestr=lookobj.loolupValue;
                modobj.isother=NO;
            }
            else
            {
                LookupModelobject *lookobj=[APPDELEGATE.sizeGlobalarray objectAtIndex: 0];
                 Newmodelobject *modobj=[selltablearray objectAtIndex:1];
                modobj.isother=NO;
                lookobj.loolupValue=@"";
                modobj.cellvaluestr=lookobj.loolupValue;
            }
            if(![parentid isEqualToString:@"23"])
            {
                _ozweightlbl.text=@"15";
            }
            else
            {
                _ozweightlbl.text=@"0";
            }

            //[dropdownview reloadData];
                       [profilerepo getsizelist:obj.parentcatid];
            
        }
        else if (tableView.tag==222)
        {
            LookupModelobject *lookobj=[APPDELEGATE.sizeGlobalarray objectAtIndex: indexPath.row];
            sizeLabel.text=lookobj.loolupValue;
            ///  issize=NO;
            
            Newmodelobject *modobj=[selltablearray objectAtIndex:1];
            
            if ([lookobj.loolupValue isEqualToString:@"Other"])
            {
                
                iscategory=YES;
                modobj.isother=YES;
                //[categorytableview reloadData];
                modobj.cellvaluestr=lookobj.loolupValue;
               
            }
            else
            {
                modobj.isother=NO;
                modobj.cellvaluestr=lookobj.loolupValue;
                
//                if ([modobj.cellvaluestr isEqualToString:@"Not Applicable"])
//                {
//                    modobj.isother=NO;
//                    [categorytableview reloadData];
//                }
            }
            NSLog(@"lookobj.loolupValue %@",lookobj.loolupValue);
        }
        else if (tableView.tag==777)
        {
            LookupModelobject *lookobj=[APPDELEGATE.genderglobalaray objectAtIndex: indexPath.row];
            
            //  quantityTF.text=[genderarray objectAtIndex:indexPath.row];
            
            
            Newmodelobject *modobj=[selltablearray objectAtIndex:2];
            
            if ([lookobj.loolupValue isEqualToString:@"Other"])
            {
                iscategory=YES;
                modobj.isother=YES;
                modobj.cellvaluestr=lookobj.loolupValue;
                
            }
            else{
                modobj.isother=NO;
                modobj.cellvaluestr=lookobj.loolupValue;
            }
        }

        else if (tableView.tag==333)
        {
            LookupModelobject *lookobj=[APPDELEGATE.conditionGlobalarray objectAtIndex: indexPath.row];
            conditionLabel.text=lookobj.loolupValue;
            // iscondition=NO;
            
            Newmodelobject *modobj=[selltablearray objectAtIndex:4];
            
            if ([lookobj.loolupValue isEqualToString:@"Other"])
            {
                iscategory=YES;
                modobj.isother=YES;
                modobj.cellvaluestr=lookobj.loolupValue;
                
            }
            else{
                modobj.isother=NO;
                modobj.cellvaluestr=lookobj.loolupValue;
            }
        }
        else if (tableView.tag==444)
        {
            
            LookupModelobject *lookobj=[APPDELEGATE.brandGlobalarray objectAtIndex: indexPath.row];
            brandTF.text=lookobj.loolupValue;
            //  isbrand=NO;
            
            Newmodelobject *modobj=[selltablearray objectAtIndex:3];
            
            if ([lookobj.loolupValue isEqualToString:@"Other"])
            {
                iscategory=YES;
                modobj.isother=YES;
                modobj.cellvaluestr=lookobj.loolupValue;
                
            }
            else
            {
                modobj.isother=NO;
                modobj.cellvaluestr=lookobj.loolupValue;
            }
        }
        else if (tableView.tag==1234)
        {
            _lbsweightlbl.text=[lbsArray objectAtIndex:indexPath.row];
        }
       else if (tableView.tag==5678)
        {
            _ozweightlbl.text=[ozArray objectAtIndex:indexPath.row];
  
        }
        if(_lbsweightlbl.text.length>0||_ozweightlbl.text.length>0)
        {
            weight=[NSString stringWithFormat:@"%@.%@",_lbsweightlbl.text,_ozweightlbl.text];
            NSLog(@"weight....%@",weight);
            [registrationRep getweightnewlisting:weight];
        }

               dropdownview.contentOffset=CGPointMake(0, 0);
        
        [self popViewMethod:NO];
        
        [categorytableview reloadData];
        
    }
    
    CGRect frmtable=categorytableview.frame;
    frmtable.size.height=categorytableview.contentSize.height;
    categorytableview.frame=frmtable;
    
    
    CGRect erngviewfrm=earningview.frame;
    erngviewfrm.origin.y=frmtable.size.height+frmtable.origin.y+20;
    earningview.frame=erngviewfrm;
    
    
    [listingScrollView setContentSize:CGSizeMake(64,earningview.frame.origin.y+earningview.frame.size.height+200)];
    
    //   dropdownview.frame=CGRectMake(124, 298, 181, 0);
}
#pragma mark textView Delegate methods
-(void)textViewDidBeginEditing:(UITextView *)textView
{
    
    
    CGRect actualframe = [textView convertRect:[textView bounds] toView:self.view];
    
    if (actualframe.origin.y >= self.view.frame.size.height-216 )
    {
        CGRect scrollfrm = [textView convertRect:[textView bounds] toView:self.listingScrollView];
        [listingScrollView setContentOffset:CGPointMake(0, scrollfrm.origin.y-80) animated:NO];
        
        NSLog(@"now check the frame to scroll %@",NSStringFromCGRect(scrollfrm));
    }
    
    NSLog(@"check teh log %@ ",NSStringFromCGRect(actualframe));
    
    
    
    
}

-(BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    isgalleryapear=NO;
    
    if ([text isEqualToString:@"\n"])
    {
        [textView resignFirstResponder];
    }
    
    
    if (textView.text.length + text.length >100)
    {
        
        
        [ AppDelegate alertmethod:appname :@"Description limits to 100 characters only." ];
        [textView resignFirstResponder];
        return NO;
    }
    
    NSCharacterSet *cs = [[NSCharacterSet characterSetWithCharactersInString:AlphaNumeric_new] invertedSet];
    
    NSString *filtered = [[text componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
    return [text isEqualToString:filtered];
    
    //return YES;
}
#pragma mark textField Delegate methods
-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    currentTextfield=textField;
    
    CGRect actualframe = [textField convertRect:[textField bounds] toView:self.view];
    NSLog(@"frame %@  and checck the yy %f and the y %f",NSStringFromCGRect(actualframe),self.view.frame.size.height-216,(actualframe.origin.y+35));
    // if (appdelegate.isiphone5)
    {
        if ((actualframe.origin.y+60) >= (self.view.frame.size.height-216) && (textField != originalpriceTF && textField != listpriceTF && textField != earningpriceTF))
        {
            // [listingScrollView setContentOffset:CGPointMake(0, actualframe.origin.y-105) animated:NO];
            CGSize scrollmaxsize=listingScrollView.contentSize;
            
            
            CGRect scrollfrm = [textField convertRect:[textField bounds] toView:self.listingScrollView];
            [listingScrollView setContentOffset:CGPointMake(0, scrollfrm.origin.y-60) animated:NO];
        }
        else  if ((actualframe.origin.y+60) >= self.view.frame.size.height-216 && (textField == originalpriceTF|| textField== listpriceTF|| textField==earningpriceTF))
        {
            //  [listingScrollView setContentOffset:CGPointMake(0, 490) animated:NO];
            CGSize scrollmaxsize=listingScrollView.contentSize;
            
            [listingScrollView setContentOffset:CGPointMake(0,originalpriceTF.frame.origin.y-60) animated:NO];
            
            CGRect scrollfrm = [textField convertRect:[textField bounds] toView:self.listingScrollView];
            [listingScrollView setContentOffset:CGPointMake(0, scrollfrm.origin.y-60) animated:NO];
        }
    }
    
    
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    
    [textField resignFirstResponder];
    return YES;
}
-(void)textFieldDidEndEditing:(UITextField *)textField
{
    
    if (textField== listpriceTF)
    {
        NSString *liststr=[listpriceTF.text stringByReplacingOccurrencesOfString:@"$" withString:@""];
        [self  earningmoneymethod:[NSString stringWithFormat:@"%@",liststr]];
        listpriceTF.text=[NSString stringWithFormat:@"%@",liststr];
        
        if([liststr floatValue] < 1.00)
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:appname message:@"List Price should not be less than $1." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            textField.text=@"";
            [alert show];
            
        }
        else
        {
            
        }
    }
    else if(textField == originalpriceTF)
    {
        
        {
            // UIAlertView *alert = [[UIAlertView alloc] initWithTitle:appname message:@"Original Price Should not be less than 3" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            
            
            
        }
        
        {
            NSString *liststr=[originalpriceTF.text stringByReplacingOccurrencesOfString:@"$" withString:@""];
            originalpriceTF.text=[NSString stringWithFormat:@"%@",liststr];
        }
    }
    else if(textField != itemnameTF && textField != listpriceTF &&textField != originalpriceTF )
    {
        
        Newmodelobject *myobj=[selltablearray objectAtIndex:textField.tag];
        
        
        myobj.otherstr=textField.text;
        
    }
    if (textField.tag==5)
    {
        if (textField.text.length==0)
        {
            
             getweightstr=@"0";
            self.markidLabel.text=[NSString stringWithFormat:@"I'll use marKIDplace's $%@ USPS label.",getweightstr];
        }
        /*
        else
        {
        NSLog(@"textField.tag==5....%@",textField.text);
        weight=textField.text;
        NSLog(@"weight....%@",weight);
        [registrationRep getweightnewlisting:weight];
        }
         */
    }
}
-(void)successresponceweight:(NSDictionary *)responseDict
{
    
    dispatch_async(dispatch_get_main_queue(), ^{
    NSLog(@"response....%@",responseDict);
       getweightstr=[responseDict valueForKey:@"shippingPrice"];
        if( [_lbsweightlbl.text isEqual:@"0"]&&[_ozweightlbl.text isEqual:@"0"])
        {
            getweightstr=@"0";
             self.markidLabel.text=[NSString stringWithFormat:@"I'll use marKIDplace's $%@ USPS label.",getweightstr];
        }
        else
        {
    self.markidLabel.text=[NSString stringWithFormat:@"I'll use marKIDplace's $%@ USPS label.",getweightstr];
        }
    });
    
}
-(void)errorMessage:(NSString *)message
{
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.001 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        if ([message isEqualToString:@"message"])
        {
            
            
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:appname message:@"Something is wrong, Please contact Admin" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
            
            
        }
        [APPDELEGATE stopspinner:self.view];
    });
}
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSLog(@"check the text field tag %ld",(long)textField.tag);
        if (textField.tag==5)
    
       {
    
        NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
              
       NSString *expression = @"^([0-9]+)?(\\.([0-9]{1,2})?)?$";
              
              NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:expression
                                                                                     options:NSRegularExpressionCaseInsensitive
                                                                                       error:nil];
              NSUInteger numberOfMatches = [regex numberOfMatchesInString:newString
                                                                  options:0
                                                                    range:NSMakeRange(0, [newString length])];
              if (numberOfMatches == 0)
              {
                  return NO;
              }
          return YES;
             
        }
    
    if (textField == listpriceTF || textField == originalpriceTF || textField.tag==5)
    {
        NSCharacterSet *cs = [[NSCharacterSet characterSetWithCharactersInString:floatnumeric] invertedSet];
        
        NSString *filtered = [[string componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
        
        
        if ([string isEqualToString:filtered] && textField == listpriceTF)
        {
            // if(listpriceTF.text.length<1)
            {
                //    [self  earningmoneymethod:[NSString stringWithFormat:@"%@",string]];
            }
            // else
            {
                NSString *liststr=[listpriceTF.text stringByReplacingOccurrencesOfString:@"$" withString:@""];
                [self  earningmoneymethod:[NSString stringWithFormat:@"%@%@",liststr,string]];
                listpriceTF.text=[NSString stringWithFormat:@"%@",liststr];
            }
            
            
            return YES;
        }
        else if ([string isEqualToString:filtered] && textField.tag == 5)
        {
            return YES;
        }
        
        else if (([string isEqualToString:filtered] && textField == originalpriceTF) )
        {
            
            return YES;
        }
        else{
            return NO;
        }
        
    }
    else if(textField == itemnameTF)
    {
        
        if (textField.text.length==0)
        {
            string=[string uppercaseString];
        }
        
        NSCharacterSet *cs = [[NSCharacterSet characterSetWithCharactersInString:[NSString stringWithFormat:@"%@%@-/",AlphaBet,Numeric]] invertedSet];
        
        NSString *filtered = [[string componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
        
        
        return [string isEqualToString:filtered];
    }
    else
    {
        
        if (textField.text.length==0)
        {
            string=[string uppercaseString];
        }
        NSCharacterSet *cs = [[NSCharacterSet characterSetWithCharactersInString:[NSString stringWithFormat:@"%@%@-/",AlphaBet,Numeric]] invertedSet];
        
        NSString *filtered = [[string componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
        
        
        return [string isEqualToString:filtered];
    }
    
    return YES;
}


-(void)earningmoneymethod :(NSString *)listingpricestr
{
    
    float listpriceflt=[listingpricestr floatValue];
    if (listpriceflt<1 && listingpricestr.length>0)
    {
        // earningpriceTF.text=[NSString stringWithFormat:@"$%0.2f",(listpriceflt- 2.95)];
    }
    else if (listpriceflt<1 && listingpricestr.length<1)
    {
        earningpriceTF.text=@"";
    }
    else
    {
        float myearningflt=100;
        
        myearningflt = (10*listpriceflt)/100;
        
        earningpriceTF.text=[NSString stringWithFormat:@"$%0.2f",(listpriceflt- myearningflt)];
    }
    
    
}


//action sheet performs go to the gallery and camera
-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    UIImagePickerController *picker = [[UIImagePickerController alloc]init];
    if(buttonIndex==0)
    {
        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
        picker.delegate =self;
        picker.wantsFullScreenLayout = YES;
        picker.allowsEditing = YES;
        [self presentViewController:picker animated:YES completion:nil];
        
        isgalleryapear=YES;
    }
    else if(buttonIndex==1)
    {
        picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        picker.delegate = self;
        picker.wantsFullScreenLayout = YES;
        picker.allowsEditing = YES;
        [self presentViewController:picker animated:YES completion:nil];
        isgalleryapear=YES;
    }
    else
    {
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}
//*******this imagepicker using the pick the images from gallery or camera***
-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingImage:(UIImage *)image editingInfo:(NSDictionary *)editingInfo
{
    //self.userProfilePicImgVw.image = image;
    
    // imageLayer.cornerRadius=1.0f;
    // listingfirstimgview.image=image;
    
    NSLog(@"what i the tag %d",currentimageview.tag);
    if (imgtag<photosarray.count)
    {
        
        [photosarray replaceObjectAtIndex:imgtag withObject:image];
    }
    else
    {
        
        [photosarray addObject:image];
        
    }
    
    [self createphotosscrollview];
    
    CGFloat compressrate=0.25f;
    
    
    [picker dismissModalViewControllerAnimated:YES];
}



//**************this scrollview using add the images and delete the images******//
-(void)createphotosscrollview
{
    for (UIView *tempv in photosScrollview.subviews)
    {
        [tempv removeFromSuperview];
    }
    
    
    int x=7;
    
    int loopint=0;
    
    if (photosarray.count<3)
    {
        loopint=4-photosarray.count;
        
        if (loopint==photosarray.count)
        {
            loopint++;
        }
        
        
    }
    else
    {
        loopint=photosarray.count+1;
    }
    
    for (int i=0; i<loopint; i++)
    {
        if (i>4)
        {
            if (photosarray.count>4)
            {
                for (int tem=5; tem<photosarray.count; tem++)
                {
                    [photosarray removeObjectAtIndex:tem];
                }
            }
            break;
        }
        
        UIView *bgview=[[UIView alloc]initWithFrame:CGRectMake(x, 6, 85, 86)];
        
        bgview.backgroundColor=[UIColor colorWithRed:233.0/255.0f green:233.0/255.0f blue:233.0/255.0f alpha:1.0];
        UIImageView *camimgview=[[UIImageView alloc]initWithFrame:CGRectMake((bgview.frame.size.width/2)-10, (bgview.frame.size.height/2)-10, 25, 20)];//20,20
        
       
        
        UIImageView *imgview=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, bgview.frame.size.width, bgview.frame.size.width)];
        imgview.backgroundColor=[UIColor clearColor];
        
        UIButton *mybutton=[UIButton buttonWithType:UIButtonTypeCustom];
        
        mybutton.backgroundColor=[UIColor clearColor];
        
        mybutton.frame=imgview.frame;
        imgview.tag=i;
        mybutton.tag=i;
        
        UIButton *photoDeleteAction=[UIButton buttonWithType:UIButtonTypeCustom];
        photoDeleteAction.tag=i;
        [photosScrollview addSubview:bgview];
        if (i>=photosarray.count && i<6)
        {
            if (i==photosarray.count && i<2 )
            {
                
                camimgview.image=[UIImage imageNamed:@"camera_new@2x.png" ];
            }
            else
            {
                camimgview.image=[UIImage imageNamed:@"plus@2x.png" ];
               camimgview.frame=CGRectMake((bgview.frame.size.width/2)-13, (bgview.frame.size.height/2)-10, 25, 20);
            }
            
            if (i==1 && i!=photosarray.count)
            {
                camimgview.image=nil;
            }
        }
        else if(i<photosarray.count && i<5)
        {
            bgview.backgroundColor=[UIColor clearColor];
            
            [photoDeleteAction setImage:[UIImage imageNamed:@"cancel.png"] forState:UIControlStateNormal];
            
            
            photoDeleteAction.frame = CGRectMake(bgview.frame.origin.x-6, bgview.frame.origin.y-6, 18, 18);
            
            [photosScrollview addSubview:photoDeleteAction];
            
            camimgview.image=[UIImage imageNamed:@"camera_new.png"];
            //here image stores through photo array
            imgview.image=[photosarray objectAtIndex:i];
        }
        else
        {
            bgview.backgroundColor=[UIColor clearColor];
            break;
        }
        [photoDeleteAction addTarget:self
                              action:@selector(photoDeleteActionmethod:)
                    forControlEvents:UIControlEventTouchUpInside];
        
        [mybutton addTarget:self action:@selector(camraBtnAction:) forControlEvents:UIControlEventTouchUpInside];
        [bgview addSubview:camimgview];
        [bgview addSubview:imgview];
        [bgview addSubview:mybutton];
        bgview.clipsToBounds=YES;
        
        
        x=x+105;
    }
    
    [photosScrollview setContentSize:CGSizeMake(photosarray.count*130+20, photosScrollview.frame.size.height)];//(photosarray.count*130+20, photosScrollview.frame.size.height)
    
}

//this action  performing delete the photos in the scroll view
-(IBAction)photoDeleteActionmethod:(id)sender
{
    [currentTextfield resignFirstResponder];
    [descriptionTF resignFirstResponder];
    
    phototag=[sender tag];
    UIAlertView *alert=[[UIAlertView alloc]initWithTitle:appname message:@"Are you sure you want to delete" delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
    alert.tag=111;
    [alert show];
    
}


#pragma mark submit listing
-(IBAction)submitBtnAction:(id)sender
{
    
     [currentTextfield resignFirstResponder];
    [descriptionTF resignFirstResponder];
    [subCategoryTextField resignFirstResponder];
    
    NSString *realname=[APPDELEGATE getStringFromPlist:@"realname"];
    
    if ([APPDELEGATE isnullorempty:realname])
    {
        [AppDelegate alertmethod:appname :@"Please fill your profile first." ];
        
        return;
    }
    
    if (itemnameTF.text.length>0 &&  originalpriceTF.text.length>0 && listpriceTF.text.length>0 && earningpriceTF.text.length>0 && photosarray.count>0 && descriptionTF.text.length>0)
    {
        
        for (int i=0; i<selltablearray.count; i++)
        {
            Newmodelobject *nobj=[selltablearray objectAtIndex:i];
            if (nobj.isother==YES && [APPDELEGATE isnullorempty:nobj.otherstr])
            {
                [ AppDelegate alertmethod:appname :[NSString stringWithFormat:@"Please fill %@ other field.",nobj.cellnamestr] ];
                return;
            }
            else
            {
                
                if ([APPDELEGATE isnullorempty:nobj.cellvaluestr] && ![nobj.cellnamestr isEqualToString:@"Weight (lb)" ])
                {
                    [ AppDelegate alertmethod:appname :[NSString stringWithFormat:@"Please fill %@.",nobj.cellnamestr] ];
                    
                    return;
                }
                
                else if (([APPDELEGATE isnullorempty:nobj.otherstr] && [nobj.cellnamestr isEqualToString:@"Weight (lb)" ])  )
                {
                    [ AppDelegate alertmethod:appname :[NSString stringWithFormat:@"Please fill %@.",nobj.cellnamestr] ];
                    
                    return;
                }
                else if ([nobj.otherstr floatValue]==0 && [nobj.cellnamestr isEqualToString:@"Weight (lb)" ])
                {
                    [ AppDelegate alertmethod:appname :[NSString stringWithFormat:@"Please fill %@. It must not be %@.",nobj.cellnamestr,nobj.otherstr] ];
                    
                    return;
                }
            }
            
        }
        
        quantityTF.text=@"For Sale";
        
        [APPDELEGATE startspinner:self.view ];
        
        
        [self performSelector:@selector(delaypostmethod) withObject:nil afterDelay:0.3 ];
        
    }
    else{
        
        [ AppDelegate alertmethod:appname :@"Please fill all the details including photos." ];
    }
    
}


-(void)delaypostmethod
{
    
    NSMutableDictionary *postmutdictionary=[[NSMutableDictionary alloc]init];
    NSNumber *sizenumber;
    NSNumber *catnumber;
    NSNumber *conditionnumber;
    NSNumber *brandnumber;
    
    NSNumber *genderbumber;
    
    for (int i=0; i<APPDELEGATE.catogoryGlobalarray.count; i++)
    {
        LookupModelobject *lokupobj=[APPDELEGATE.catogoryGlobalarray objectAtIndex:i];
        NSLog(@"the count in the is  %d",i);

        
        for (int j=0;j<lokupobj.categoryDataArray.count;j++)
        {
            LookupModelobject *lokup=[lokupobj.categoryDataArray objectAtIndex:j];
            Newmodelobject *msizeobj=[selltablearray objectAtIndex:0];

            
            if (msizeobj.isother)
            {
                NSNumber *parentno=[NSNumber numberWithInt:[catogeryotherstr intValue]];
                NSLog(@"the parent idin the update is %@",catidstr);

                [postmutdictionary setObject:parentid forKey:@"CategoryId"];
                
                [ postmutdictionary setObject:msizeobj.otherstr forKey:@"OtherCategory"];
                break;
            }
            
            if ([msizeobj.cellvaluestr isEqualToString:lokup.loolupValue])
            {
                
                catnumber =[NSNumber numberWithInt:[lokup.loolupid intValue] ];
                
                //  catnumber =[NSNumber numberWithInt:[catidstr intValue] ];
                 NSLog(@"the parent idin the update is %@",catidstr);
                
        [postmutdictionary setObject:catidstr forKey:@"CategoryId" ];
               

                break;
            }
            
        }
        
        
        
    }
    
    NSLog(@"check thid -=%@-= ",[postmutdictionary valueForKey:@"CategoryId"]);
    for (int i=0; i<APPDELEGATE.sizeGlobalarray.count; i++)
    {
        
        Newmodelobject *msizeobj=[selltablearray objectAtIndex:1];
        
        LookupModelobject *lokupobj=[APPDELEGATE.sizeGlobalarray objectAtIndex:i];
        
        if (msizeobj.isother)
        {
            [ postmutdictionary setObject:[NSNumber numberWithInt:0] forKey:@"SizeId"];
            [ postmutdictionary setObject:msizeobj.otherstr forKey:@"OtherSizeID"];
            break;
        }
        if ([msizeobj.cellvaluestr isEqualToString:lokupobj.loolupValue])
        {
            sizenumber =[NSNumber numberWithInt:[lokupobj.loolupid intValue] ];
            [ postmutdictionary setObject:sizenumber forKey:@"SizeId"];
            [ postmutdictionary setObject:@"" forKey:@"OtherSizeID"];
            break;
        }
       if ([msizeobj.cellvaluestr isEqualToString:@"Not Applicable"])
        {
            lokupobj.loolupid=@"0";
             sizenumber =[NSNumber numberWithInt:[lokupobj.loolupid intValue] ];
            [ postmutdictionary setObject:@"0" forKey:@"SizeId"];
            [ postmutdictionary setObject:@"Not Applicable" forKey:@"OtherSizeID"];
            break;

        } 

    }
    for (int i=0; i<APPDELEGATE.brandGlobalarray.count; i++)
    {
        LookupModelobject *lokupobj=[APPDELEGATE.brandGlobalarray objectAtIndex:i];
        
        Newmodelobject *msizeobj=[selltablearray objectAtIndex:3];
        
        if (msizeobj.isother)
        {
            [ postmutdictionary setObject:[NSNumber numberWithInt:0] forKey:@"BrandId"];
            [ postmutdictionary setObject:msizeobj.otherstr forKey:@"OtherBrandID"];
            break;
        }
        if ([msizeobj.cellvaluestr isEqualToString:lokupobj.loolupValue])
        {
            brandnumber =[NSNumber numberWithInt:[lokupobj.loolupid intValue] ];
            [ postmutdictionary setObject:brandnumber forKey:@"BrandId"];
            [ postmutdictionary setObject:@"" forKey:@"OtherBrandID"];
            break;
        }
        
    }
    for (int i=0; i<APPDELEGATE.conditionGlobalarray.count; i++)
    {
        LookupModelobject *lokupobj=[APPDELEGATE.conditionGlobalarray objectAtIndex:i];
        
        Newmodelobject *msizeobj=[selltablearray objectAtIndex:4];
        
        if (msizeobj.isother)
        {
            [ postmutdictionary setObject:[NSNumber numberWithInt:0] forKey:@"ConditionId"];
            
            [ postmutdictionary setObject:msizeobj.otherstr forKey:@"OtherConditionID"];
            break;
        }
        if ([msizeobj.cellvaluestr isEqualToString:lokupobj.loolupValue])
        {
            conditionnumber =[NSNumber numberWithInt:[lokupobj.loolupid intValue] ];
            [ postmutdictionary setObject:conditionnumber forKey:@"ConditionId"];
            [ postmutdictionary setObject:@"" forKey:@"OtherConditionID"];
            break;
        }
        
    }
    
    
    
    for (int i=0; i<APPDELEGATE.genderglobalaray.count; i++)
    {
        
        Newmodelobject *msizeobj=[selltablearray objectAtIndex:2];
        
        LookupModelobject *lokupobj=[APPDELEGATE.genderglobalaray objectAtIndex:i];
        
        if ([msizeobj.cellvaluestr isEqualToString:lokupobj.loolupValue])
        {
            genderbumber =[NSNumber numberWithInt:[lokupobj.loolupid intValue] ];
            [ postmutdictionary setObject:genderbumber forKey:@"Gender"];
            break;
        }
        
    }
    
    
    //Newmodelobject *weightobj=[selltablearray objectAtIndex:5];
    [ postmutdictionary setObject:weight forKey:@"Weight"];
    
    NSString *orgstr=[originalpriceTF.text stringByReplacingOccurrencesOfString:@"$" withString:@""];
    NSString *liststr=[listpriceTF.text stringByReplacingOccurrencesOfString:@"$" withString:@""];
    
    
    [ postmutdictionary setObject:APPDELEGATE.useridstring forKey:@"UserId"];
    
    if (isupdate)
    {
        NSLog(@"update item id %@",selecteditemobj.itemidstr);
        
        [ postmutdictionary setObject:selecteditemobj.itemidstr forKey:@"UserItemId"];
    }
    
    
    [ postmutdictionary setObject:orgstr forKey:@"UnitPrice"];
    
    
    
    
    [ postmutdictionary setObject:descriptionTF.text forKey:@"Description"];
    [ postmutdictionary setObject:itemnameTF.text forKey:@"ItemName"];
    
    
    [ postmutdictionary setObject:[NSNumber numberWithBool:YES] forKey:@"Forsale"];
    
    
    
    [postmutdictionary setObject:@"1" forKey:@"AvailableQuantity" ];
    [postmutdictionary setObject:liststr forKey:@"ListPrice" ];
    [postmutdictionary setObject:@"Posted" forKey:@"Status" ];
    
    
    [postmutdictionary setObject:[NSNumber numberWithBool:isupsc] forKey:@"USPS" ];
    
    [postmutdictionary setObject:[NSNumber numberWithBool:ismetting] forKey:@"inPerson" ];
    
    
    NSLog(@"=-=-=-= chekc the int dict %@ ",postmutdictionary);
    
    
    NSMutableArray *urlsarray=[APPDELEGATE postimagestoamazone:YES photosaray:photosarray :nil];
    
    NSString *finalstr=  [[urlsarray valueForKey:@"description"] componentsJoinedByString:@","];
    
    [postmutdictionary setObject:finalstr forKey:@"Picture" ];
    
    NSLog(@" what i got here %@",finalstr);
    NSLog(@"postmutdictionary....%@",postmutdictionary);
    
    [self postotservice:postmutdictionary];
    
    
}

-(void)requestFinished:(ASIHTTPRequest *)request
{
    NSError *error = [request error];
    
    if(!error)
    {
        NSString *response = [request responseString];
        
        NSDictionary *dict = [response JSONValue];
        
        NSLog(@"dict ***** %@",dict);
        
        NSString *msgstring=[dict objectForKey:@"message"];
        
        if ([msgstring isEqualToString:@"User Item Added Successfully"] || [msgstring isEqualToString:@"User Item updated Successfully"])
        {
            msgstring=[msgstring stringByReplacingOccurrencesOfString:@"User " withString:@""];
            
            itemidstr=[dict valueForKey:@"userItemId"];
            if (isupdate)
            {
                msgstring=@"Listing updated successfully";
            }
            else
            {
                msgstring=@"Listing created successfully";
            }
            UIAlertView *myaltv=[[UIAlertView alloc]initWithTitle:appname message:msgstring delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            myaltv.tag=1010;
            [myaltv show];
        }
        else{
            [ AppDelegate alertmethod:appname :@"Unable to post your listing." ];
        }
    }
    
    [APPDELEGATE stopspinner:self.view];
    
}

-(void)successresponceListing :(id)respdict :(NSString *)paramname
{
    
    if ([paramname isEqualToString:@"SubmitListine"])
    {
        NSLog(@"ckkkkkk responce %@",respdict);
        
        if ([respdict isKindOfClass:[NSDictionary class]] )
        {
            NSDictionary *gettingdict= (NSDictionary *)respdict;
            
            NSString *msgstring=[gettingdict valueForKey:@"message"];
            if ([msgstring isEqualToString:@"User Item Added Successfully"] || [msgstring isEqualToString:@"User Item updated Successfully"])
            {
                msgstring=[msgstring stringByReplacingOccurrencesOfString:@"User " withString:@""];
                UIAlertView *myaltv=[[UIAlertView alloc]initWithTitle:appname message:msgstring delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                myaltv.tag=1010;
                [myaltv show];
            }
            else{
                [ AppDelegate alertmethod:appname :@"unable to post your listing"];
            }
        }
        
        [APPDELEGATE stopspinner:self.view];
    }
    else if([paramname isEqualToString:@"getListine"])
    {
        
    }
    
    else if([paramname isEqualToString:@"getcatogery"])
    {
        
        [APPDELEGATE.catogoryGlobalarray removeAllObjects];
        
        
        [APPDELEGATE parsedataforCategory:respdict ];
        
        [APPDELEGATE stopspinner:self.view ];
        NSLog(@"check the catogory %@",respdict);
        
    }
   
   /* else if([paramname isEqualToString:@"getsize"])
    {
        
        
        [APPDELEGATE.sizeGlobalarray removeAllObjects];
        
        if ([respdict isKindOfClass:[NSArray class]]) {
            NSArray *myar=(NSArray *)respdict;
            for (int i=0; i<myar.count; i++)
            {
                NSDictionary *mydict=[myar objectAtIndex:i];
                
                LookupModelobject *lookobj=[[LookupModelobject alloc]init];
                
                lookobj.loolupName=@"lookupName";
                lookobj.loolupid=[mydict valueForKey:@"lookupID"];
                lookobj.loolupValue=[mydict valueForKey:@"lookupValue"];
                lookobj.loolupNumber=[mydict valueForKey:@"sequenceNumber"];
                lookobj.loolupStatus=[mydict valueForKey:@"status"];
                
                
                [APPDELEGATE.sizeGlobalarray addObject:lookobj];
            }
        }
        APPDELEGATE.sizeGlobalarray=[APPDELEGATE sortmyarray:@"loolupValue" :APPDELEGATE.sizeGlobalarray];
        
        [ registrationRep getdata:nil :@"getcondition" :@"GET" ];
    }*/
    
    else if([paramname isEqualToString:@"getcondition"])
    {
        
        [APPDELEGATE.conditionGlobalarray removeAllObjects];
        if ([respdict isKindOfClass:[NSArray class]]) {
            NSArray *myar=(NSArray *)respdict;
            for (int i=0; i<myar.count; i++)
            {
                NSDictionary *mydict=[myar objectAtIndex:i];
                
                LookupModelobject *lookobj=[[LookupModelobject alloc]init];
                
                lookobj.loolupName=@"lookupName";
                lookobj.loolupid=[mydict valueForKey:@"lookupID"];
                lookobj.loolupValue=[mydict valueForKey:@"lookupValue"];
                lookobj.loolupNumber=[mydict valueForKey:@"sequenceNumber"];
                lookobj.loolupStatus=[mydict valueForKey:@"status"];
                [APPDELEGATE.conditionGlobalarray addObject:lookobj];
            }
            
        }
        
        APPDELEGATE.conditionGlobalarray=[APPDELEGATE sortmyarray:@"loolupValue" :APPDELEGATE.conditionGlobalarray];
        /// get brand
        [ registrationRep getdata:nil :@"getbrand" :@"GET" withcount:@"0"];
    }
    else if([paramname isEqualToString:@"getbrand"])
    {
        [APPDELEGATE.brandGlobalarray removeAllObjects];
        if ([respdict isKindOfClass:[NSArray class]]) {
            NSArray *myar=(NSArray *)respdict;
            for (int i=0; i<myar.count; i++)
            {
                NSDictionary *mydict=[myar objectAtIndex:i];
                
                LookupModelobject *lookobj=[[LookupModelobject alloc]init];
                
                lookobj.loolupName=@"lookupName";
                lookobj.loolupid=[mydict valueForKey:@"lookupID"];
                lookobj.loolupValue=[mydict valueForKey:@"lookupValue"];
                lookobj.loolupNumber=[mydict valueForKey:@"sequenceNumber"];
                lookobj.loolupStatus=[mydict valueForKey:@"status"];
                
                
                [APPDELEGATE.brandGlobalarray addObject:lookobj];
            }
            
        }
        
        //APPDELEGATE.brandGlobalarray=[APPDELEGATE sortmyarray:@"loolupValue" :APPDELEGATE.brandGlobalarray];
        
        LookupModelobject *lookobj33=[[LookupModelobject alloc]init];
        
        lookobj33.loolupName=@"lookupName";
        lookobj33.loolupid=@"130";
        lookobj33.loolupValue=@"Other";
        lookobj33.loolupNumber=@"0";
        lookobj33.loolupStatus=@"0";
        lookobj33.isother=YES;
        [APPDELEGATE.brandGlobalarray addObject:lookobj33];

        [APPDELEGATE stopspinner:self.view];
    }
}

-(IBAction)selectdateAction:(id)sender
{
    [currentTextfield resignFirstResponder];
    [descriptionTF resignFirstResponder];
    if ([sender tag]==9999)
    {
        
        
        CGRect pickerFrame = CGRectMake(0, 70, 0, 0);
        datePicker = [[UIDatePicker alloc] initWithFrame:pickerFrame];
        datePicker.datePickerMode=UIDatePickerModeDate;
        
        NSDate *tempdate=[NSDate date];
        [datePicker addTarget:self
                       action:@selector(LabelChange:)
             forControlEvents:UIControlEventValueChanged];
        
        datePicker.minimumDate=tempdate;
        
        if(self.startDate==nil || [self.startDate isKindOfClass:[NSNull class]])
        {
            datePicker.date=tempdate;
            
        }
        else
        {
            datePicker.date=self.startDate;
        }
        
        UISegmentedControl *cancelButton = [[UISegmentedControl alloc] initWithItems:[NSArray arrayWithObject:@"Cancel"]];
        cancelButton.momentary = YES;
        cancelButton.frame = CGRectMake(5, 4.0f, 50.0f, 30.0f);
        cancelButton.segmentedControlStyle = UISegmentedControlStyleBar;
        cancelButton.tintColor = [UIColor blackColor];
        [cancelButton addTarget:self action:@selector(actionsheetcancel) forControlEvents:UIControlEventValueChanged];
        
        
        UISegmentedControl *closeButton = [[UISegmentedControl alloc] initWithItems:[NSArray arrayWithObject:@"Done"]];
        closeButton.momentary = YES;
        closeButton.frame = CGRectMake(265, 4.0f, 50.0f, 30.0f);
        closeButton.segmentedControlStyle = UISegmentedControlStyleBar;
        closeButton.tintColor = [UIColor blackColor];
        [closeButton addTarget:self action:@selector(dismissActionSheet) forControlEvents:UIControlEventValueChanged];
        
        self.actionSheet = [[UIActionSheet alloc] initWithTitle:nil
                                                       delegate:nil
                                              cancelButtonTitle:nil
                                         destructiveButtonTitle:nil
                                              otherButtonTitles:nil];
        
        [self.actionSheet setActionSheetStyle:UIActionSheetStyleBlackTranslucent];
        UILabel *lbl=[[UILabel alloc] initWithFrame:CGRectMake(0,4,320,25)];
        actionSheet.tag=5;
        
        lbl.text=@"Start Date";
        lbl.font=[UIFont fontWithName:@"Helvetica" size:18];
        lbl.textColor=[UIColor whiteColor];
        
        float  version = [[[UIDevice currentDevice] systemVersion] floatValue];
        
        
        
        lbl.backgroundColor=[UIColor clearColor];
        lbl.textAlignment=NSTextAlignmentCenter;
        
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setDateFormat:@"EEE, MMM dd, yyyy"];
        NSString *dateString=[dateFormat stringFromDate:datePicker.date];
        actiondatelabel.text=dateString;
        [self.actionSheet addSubview:actiondatelabel];
        [self.actionSheet addSubview:lbl];
        [self.actionSheet addSubview:datePicker];
        [self.actionSheet addSubview:cancelButton];
        [self.actionSheet addSubview:closeButton];
        [actionSheet showInView:[[UIApplication sharedApplication] keyWindow]];
        if(version>=7.0)
        {
            actiondatelabel.frame=CGRectMake(0,45,320,30);
            lbl.textColor=[UIColor blackColor];
            actiondatelabel.textColor=[UIColor blackColor];
            [actionSheet setBounds:CGRectMake(0, 0, 320, 500)];
        }
        else{
            actiondatelabel.frame=CGRectMake(0,40,320,30);
            actiondatelabel.textColor=[UIColor whiteColor];
            [actionSheet setBounds:CGRectMake(0, 0, 320, 550)];
        }
    }
    else
    {
        
        
        if(startdateTF.text.length<1)
        {
            UIAlertView *alervv=[[UIAlertView alloc]initWithTitle:appname message:@"Select Start Date First" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alervv show];
        }
        else{
            
            datePicker.minimumDate=startDate;
            
            if(self.endDate==nil || [self.endDate isKindOfClass:[NSNull class]])
            {
                datePicker.date=self.startDate;
            }
            else
            {
                datePicker.date=self.endDate;
            }
            
            
            UISegmentedControl *cancelButton = [[UISegmentedControl alloc] initWithItems:[NSArray arrayWithObject:@"Cancel"]];
            cancelButton.momentary = YES;
            cancelButton.frame = CGRectMake(5, 4.0f, 50.0f, 30.0f);
            cancelButton.segmentedControlStyle = UISegmentedControlStyleBar;
            cancelButton.tintColor = [UIColor blackColor];
            [cancelButton addTarget:self action:@selector(actionsheetcancel) forControlEvents:UIControlEventValueChanged];
            
            
            UISegmentedControl *closeButton = [[UISegmentedControl alloc] initWithItems:[NSArray arrayWithObject:@"Done"]];
            closeButton.momentary = YES;
            closeButton.frame = CGRectMake(260, 4.0f, 50.0f, 30.0f);
            closeButton.segmentedControlStyle = UISegmentedControlStyleBar;
            closeButton.tintColor = [UIColor blackColor];
            [closeButton addTarget:self action:@selector(dismissActionSheet) forControlEvents:UIControlEventValueChanged];
            
            self.actionSheet = [[UIActionSheet alloc] initWithTitle:nil
                                                           delegate:nil
                                                  cancelButtonTitle:nil
                                             destructiveButtonTitle:nil
                                                  otherButtonTitles:nil];
            [self.actionSheet setActionSheetStyle:UIActionSheetStyleBlackTranslucent];
            UILabel *lbl=[[UILabel alloc] initWithFrame:CGRectMake(90,4,140,30)];
            actionSheet.tag=10;
            
            lbl.text=@"End Date";
            lbl.font=[UIFont fontWithName:@"Helvetica" size:18];
            lbl.textColor=[UIColor whiteColor];
            
            float  version = [[[UIDevice currentDevice] systemVersion] floatValue];
            
            lbl.backgroundColor=[UIColor clearColor];
            lbl.textAlignment=UITextAlignmentCenter;
            
            
            NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
            [dateFormat setDateFormat:@"EEE, MMM dd, yyyy"];
            NSString *dateString=[dateFormat stringFromDate:datePicker.date];
            actiondatelabel.text=dateString;
            [self.actionSheet addSubview:actiondatelabel];
            [self.actionSheet addSubview:lbl];
            [self.actionSheet addSubview:datePicker];
            [self.actionSheet addSubview:closeButton];
            [self.actionSheet addSubview:cancelButton ];
            [actionSheet showInView:[[UIApplication sharedApplication] keyWindow]];
            if(version>=7.0)
            {
                actiondatelabel.frame=CGRectMake(0,45,320,30);
                lbl.textColor=[UIColor blackColor];
                actiondatelabel.textColor=[UIColor blackColor];
                [actionSheet setBounds:CGRectMake(0, 0, 320, 500)];
            }
            else{
                actiondatelabel.frame=CGRectMake(0,40,320,30);
                actiondatelabel.textColor=[UIColor whiteColor];
                [actionSheet setBounds:CGRectMake(0, 0, 320, 550)];
            }
            
        }
        
    }
    
}


-(void)dismissActionSheet
{
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"EEE, MMM dd, yyyy"];
    NSString *dateString=[dateFormat stringFromDate:datePicker.date];
    
    if (actionSheet.tag !=10)
    {
        
        enddateTF.text=@"";
        if(self.startDate==nil || [self.startDate isKindOfClass:[NSNull class]])
        {
            startDate=datePicker.date;
            
            
            startdateTF.text=[NSString stringWithFormat:@"%@",startDate]  ;
            
        }
        else
        {
            startDate=datePicker.date;
            if(self.endDate==nil || [self.endDate isKindOfClass:[NSNull class]])
            {
                
            }
            else
            {
                NSComparisonResult result = [startDate compare:endDate];
                switch (result)
                {
                    case NSOrderedAscending:
                    {
                        
                        break;
                    }
                    case NSOrderedDescending:
                    {
                        endDate=startDate;
                        
                        break;
                    }
                    case NSOrderedSame:
                        
                        break;
                    default:
                        
                        break;
                }
            }
        }
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"EEE, MMM dd, yyyy"];
        
        startdateTF.text=[formatter stringFromDate:startDate ]  ;
    }
    else
    {
        endDate=datePicker.date;
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"EEE, MMM dd, yyyy"];
        enddateTF.text=[formatter stringFromDate:endDate ];
        
    }
    [datePicker removeFromSuperview];
    [actionSheet dismissWithClickedButtonIndex:0 animated:YES];
    
}

-(void)actionsheetcancel
{
    
    [datePicker removeFromSuperview];
    [actionSheet dismissWithClickedButtonIndex:0 animated:YES];
}
- (void)LabelChange:(id)sender

{
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"EEE, MMM dd, yyyy"];
    NSString *dateString=[dateFormat stringFromDate:datePicker.date];
    
    actiondatelabel.text=dateString;
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    if (alertView.tag==1010)
    {
        
        [APPDELEGATE.myclosetGlobalarray removeAllObjects];
        if (isupdate==YES)
        {
            BOOL popbool=YES;
            NSArray *arr=self.navigationController.viewControllers;
            
            for (UIViewController *childvc in arr)
            {
                
                if([childvc isKindOfClass:[ListingDetailViewController class]])
                {
                    popbool=NO;
                    
                    ListingDetailViewController *listingDetailVC=(ListingDetailViewController *)childvc;
                    NSMutableArray *itemsarray=[[NSMutableArray alloc]init];
                    
                    ItemsModelobject *itemobj=[[ItemsModelobject alloc]init];
                    
                    
                    itemobj.useridstr=APPDELEGATE.useridstring;
                    
                    
                    itemobj.itemNamestr=itemnameTF.text;
                    
                    itemobj.catogrystr=subCategory_StatLbl.text;
                    if ([APPDELEGATE isnullorempty:subCategory_StatLbl.text])
                    {
                        itemobj.catogrystr=categorytxtfeld.text;
                    }
                    
                    
                    itemobj.sizestr=sizeLabel.text;
                    itemobj.brandstr=brandTF.text;
                    itemobj.conditionstr=conditionLabel.text;
                    itemobj.descriptionstr=descriptionTF.text;
                    
                    itemobj.usernamestring=[APPDELEGATE getStringFromPlist:usernamekey];
                    
                    NSString *orgstr=[originalpriceTF.text stringByReplacingOccurrencesOfString:@"$" withString:@""];
                    NSString *liststr=[listpriceTF.text stringByReplacingOccurrencesOfString:@"$" withString:@""];
                    
                    itemobj.listingcoststr=orgstr;
                    itemobj.salecostcoststr=liststr;
                    
                    itemobj.picturearray=[NSMutableArray new];
                    
                    // [itemobj.picturearray addObjectsFromArray:photosarray];
                    itemobj.itemidstr=itemidstr;
                    itemobj.userlistingidstr=itemidstr;
                    itemobj.citystring=@"";
                    itemobj.statestr=@"";
                    
                    
                    
                    [itemsarray addObject:itemobj];
                    
                    listingDetailVC.listingmainarray=itemsarray;
                    listingDetailVC.senderIndex=0;
                    listingDetailVC.sltobj=itemobj;
                    listingDetailVC.condstring=@"New";
                    
                    
                    APPDELEGATE.suggestionviewcont.loaddatabool=YES;
                    
                    [self.navigationController popToViewController:listingDetailVC animated:YES];
                    break;
                    
                }
            }
            
            
            
            if (popbool)
            {
                ListingDetailViewController *listingDetailVC;
                if (APPDELEGATE.isiphone5)
                {
                    listingDetailVC = [[ListingDetailViewController alloc]initWithNibName:@"ListingDetailViewController" bundle:nil];
                }
                else
                {
                    listingDetailVC = [[ListingDetailViewController alloc]initWithNibName:@"ListingDetailViewController_iphone4" bundle:nil];
                }
                
                
                
                NSMutableArray *itemsarray=[[NSMutableArray alloc]init];
                
                ItemsModelobject *itemobj=[[ItemsModelobject alloc]init];
                
                
                itemobj.useridstr=APPDELEGATE.useridstring;
                
                
                itemobj.itemNamestr=itemnameTF.text;
                
                itemobj.catogrystr=subCategory_StatLbl.text;
                if ([APPDELEGATE isnullorempty:subCategory_StatLbl.text])
                {
                    itemobj.catogrystr=categorytxtfeld.text;
                }
                
                
                itemobj.sizestr=sizeLabel.text;
                itemobj.brandstr=brandTF.text;
                itemobj.conditionstr=conditionLabel.text;
                itemobj.descriptionstr=descriptionTF.text;
                
                itemobj.usernamestring=[APPDELEGATE getStringFromPlist:usernamekey];
                
                NSString *orgstr=[originalpriceTF.text stringByReplacingOccurrencesOfString:@"$" withString:@""];
                NSString *liststr=[listpriceTF.text stringByReplacingOccurrencesOfString:@"$" withString:@""];
                
                itemobj.listingcoststr=orgstr;
                itemobj.salecostcoststr=liststr;
                
                itemobj.picturearray=[NSMutableArray new];
                
                // [itemobj.picturearray addObjectsFromArray:photosarray];
                
                
                
                itemobj.itemidstr=itemidstr;
                itemobj.userlistingidstr=itemidstr;
                
                itemobj.citystring=@"";
                itemobj.statestr=@"";
                
                [itemsarray addObject:itemobj];
                
                listingDetailVC.listingmainarray=itemsarray;
                listingDetailVC.senderIndex=0;
                listingDetailVC.sltobj=itemobj;
                listingDetailVC.condstring=@"New";
                
                APPDELEGATE.suggestionviewcont.loaddatabool=YES;
                [self.navigationController pushViewController:listingDetailVC animated:YES];
                
            }
        }
        else
        {
            
            
            SuggestionsViewController *listingDetailVC;
            if (APPDELEGATE.isiphone5)
            {
                listingDetailVC = [[SuggestionsViewController alloc]initWithNibName:@"SuggestionsViewController" bundle:nil];
            }
            else
            {
                listingDetailVC = [[SuggestionsViewController alloc]initWithNibName:@"SuggestionsViewController_iphone4" bundle:nil];
                
            }
            
            /*
            
            NSMutableArray *itemsarray=[[NSMutableArray alloc]init];
            
            ItemsModelobject *itemobj=[[ItemsModelobject alloc]init];
            
            
            itemobj.useridstr=APPDELEGATE.useridstring;
            
            itemobj.usernamestring=[APPDELEGATE getStringFromPlist:usernamekey];
            itemobj.itemNamestr=itemnameTF.text;
            
            itemobj.catogrystr=subCategory_StatLbl.text;
            if ([APPDELEGATE isnullorempty:subCategory_StatLbl.text])
            {
                itemobj.catogrystr=categorytxtfeld.text;
            }
            
            
            itemobj.sizestr=sizeLabel.text;
            itemobj.brandstr=brandTF.text;
            itemobj.conditionstr=conditionLabel.text;
            itemobj.descriptionstr=descriptionTF.text;
            
            
            NSString *orgstr=[originalpriceTF.text stringByReplacingOccurrencesOfString:@"$" withString:@""];
            NSString *liststr=[listpriceTF.text stringByReplacingOccurrencesOfString:@"$" withString:@""];
            
            itemobj.listingcoststr=orgstr;
            itemobj.salecostcoststr=liststr;
            
            itemobj.picturearray=[NSMutableArray new];
            
            
            // [itemobj.picturearray addObjectsFromArray:photosarray];
            
            
            itemobj.itemidstr=itemidstr;
            itemobj.userlistingidstr=itemidstr;
            itemobj.citystring=@"";
            itemobj.statestr=@"";
            
            [itemsarray addObject:itemobj];
            
            listingDetailVC.listingmainarray=itemsarray;
            listingDetailVC.senderIndex=0;
            listingDetailVC.sltobj=itemobj;
            listingDetailVC.condstring=@"New";
            
            APPDELEGATE.suggestionviewcont.loaddatabool=YES;
             */
            [self.navigationController pushViewController:listingDetailVC animated:YES];
            
            
        }
        
    }
    else if(alertView.tag==111)
    {
        if(buttonIndex==1)
        {
            [photosarray removeObjectAtIndex:phototag];
            
            [self createphotosscrollview];
            
        }
    }
}

-(void)request:(AmazonServiceRequest *)request didCompleteWithResponse:(AmazonServiceResponse *)response
{
    
    NSLog(@"plz check  %@ ",response);
    
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
    
}

-(void)request:(AmazonServiceRequest *)request didFailWithError:(NSError *)error
{
    NSLog(@"Error: %@", error);
    
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
    
}


#pragma mark - Data transpering to W/S
- (void) connection:(NSURLConnection *)connection didReceiveData:(NSData *)data1
{
    
    if (!mdata)
    {
        mdata = [[NSMutableData alloc] initWithData:data1];
    }
    else
    {
        [mdata appendData:data1];
    }
    
}
- (void) connectionDidFinishLoading:(NSURLConnection *)connection
{
    
	NSString *dataString=[[NSString alloc]initWithData:mdata encoding:NSUTF8StringEncoding];
	
    
	mdata = nil;
	con = nil;
    
	NSMutableDictionary *setResponseDict = [[NSMutableDictionary alloc] init];
    
    setResponseDict = [dataString JSONValue];
    NSLog(@"setResponseDict--%@",setResponseDict);
    
    NSString *msgstring=[setResponseDict objectForKey:@"message"];
    
    if ([msgstring isEqualToString:@"User Item Added Successfully"] || [msgstring isEqualToString:@"User Item updated Successfully"])
    {
        msgstring=[msgstring stringByReplacingOccurrencesOfString:@"User " withString:@""];
        
        itemidstr=[setResponseDict valueForKey:@"userItemId"];
        if (isupdate)
        {
            msgstring=@"User item updated successfully";
        }
        else
        {
            // msgstring=@"Listing created successfully";
            msgstring=@"Thanks for posting with marKIDplace";
        }
        UIAlertView *myaltv=[[UIAlertView alloc]initWithTitle:appname message:msgstring delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        myaltv.tag=1010;
        [myaltv show];
    }
    else
    {
        [ AppDelegate alertmethod:appname :@"Unable to post your listing." ];
    }
    
    
    [APPDELEGATE stopspinner:self.view];
    
}

- (void) connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    
   	con = nil;
    NSString *stringval=@"error occured";
    
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Connection Error!" message:@"Coudn't process request." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
    
    [alertView show];
    
    [APPDELEGATE stopspinner:self.view];
}


//here post to services
-(void)postotservice :(NSMutableDictionary *)mainDict
{
    
    NSLog(@"check the dict %@",mainDict);
    
    NSError *jsonSerializationError = nil;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:mainDict options:NSJSONWritingPrettyPrinted error:&jsonSerializationError];
    
    if(!jsonSerializationError)
    {
        NSString *serJSON = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        NSLog(@"Serialized JSON: %@", serJSON);
    }
    else
    {
        NSLog(@"JSON Encoding Failed: %@", [jsonSerializationError localizedDescription]);
    }
    
    
    
    SBJsonWriter *jsonWriter = [[SBJsonWriter alloc] init];
    
    
    NSString *jsonRequest = [jsonWriter stringWithObject:mainDict];
    jsonRequest = [jsonRequest stringByReplacingOccurrencesOfString:@"<" withString:@""];
    
    NSURL *url;
    url = [NSURL URLWithString:[NSString stringWithFormat:@"%@UserItems?format=json",MainUrl]];
    
    NSLog(@"check teh url %@ ",url);
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:60.0];
    
    
    [request setHTTPMethod:@"POST"];
    
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    [request setValue:[NSString stringWithFormat:@"%d", [jsonData length]] forHTTPHeaderField:@"Content-Length"];
    
    // NSLog(@"request data %@",jsonData);
    [request setHTTPBody: jsonData];
    
    con = [NSURLConnection connectionWithRequest:request delegate:self];
    
    
    
}




- (void)simpleJsonParsingPostMetod :(NSMutableDictionary *)maindictt
{
    
    // #warning set webservice url and parse POST method in JSON
    //-- Temp Initialized variables
    NSString *first_name;
    NSString *image_name;
    NSData *imageData;
    
    //-- Convert string into URL
    NSString *urlString = [NSString stringWithFormat:@"demo.com/your_server_db_name/service/link"];
    NSURL *url;
    url = [NSURL URLWithString:[NSString stringWithFormat:@"%@UserItems?format=json",MainUrl]];
    
    
    NSMutableURLRequest *request =[[NSMutableURLRequest alloc] init];
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    
    NSString *boundary = @"14737809831466499882746641449";
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
    [request addValue:contentType forHTTPHeaderField: @"Content-Type"];
    
    //-- Append data into posr url using following method
    NSMutableData *body = [NSMutableData data];
    
    
    //-- For Sending text
    
    //-- "firstname" is keyword form service
    //-- "first_name" is the text which we have to send
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    //[body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n",@"firstname"] dataUsingEncoding:NSUTF8StringEncoding]];
    //[body appendData:[[NSString stringWithFormat:@"%@",first_name] dataUsingEncoding:NSUTF8StringEncoding]];
    
    
    //-- For sending image into service if needed (send image as imagedata)
    
    //-- "image_name" is file name of the image (we can set custom name)
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    
    NSMutableDictionary *imgsdict=[maindictt objectForKey:@"UserPictureItem"];
    
    NSMutableArray *imgsaray=[imgsdict objectForKey:@"PictureCdnUrl"];
    
    for (int i=0; i<imgsaray.count; i++)
    {
        
        NSData *imgpostdata=[imgsaray objectAtIndex:i];
        NSString *currentdatestring=[AppDelegate uniqueidstring];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition:form-data; name=\"file\"; filename=\"%@.jpg\"\r\n",currentdatestring] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"Content-Type: application/octet-stream\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[NSData dataWithData:imgpostdata]];
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    }
    
    //-- Sending data into server through URL
    [request setHTTPBody:body];
    
    //-- Getting response form server
    NSData *responseData = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil];
    
    //-- JSON Parsing with response data
    NSDictionary *result = [NSJSONSerialization JSONObjectWithData:responseData options:NSJSONReadingMutableContainers error:nil];
    NSLog(@"Result = %@",result);
    
    [APPDELEGATE stopspinner:self.view];
}
-(void)internetConnection
{
   // dispatch_async(dispatch_get_main_queue(), ^{

    UIAlertView *Internetalert=[[UIAlertView alloc]initWithTitle:appname message:@"Internet connection not available" delegate:self cancelButtonTitle:nil otherButtonTitles:@"Ok", nil];
         [APPDELEGATE stopspinner:self.view];
         [Internetalert show];
  //  });
}
-(void)checkConnection
{
    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    if (networkStatus == NotReachable)
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            
            UIAlertView *Internetalert=[[UIAlertView alloc]initWithTitle:appname message:@"Internet connection not available" delegate:self cancelButtonTitle:nil otherButtonTitles:@"Ok", nil];
            [APPDELEGATE stopspinner:self.view];
            [Internetalert show];
        });
        
        ////NSLog(@"There IS NO internet connection");
    }
    
}
-(IBAction)weightBtnAction:(id)sender
{
    
    if([sender tag]==1234)
    {
        poptitlelable.text=@"lbs";
        dropdownview.frame=CGRectMake(16, 61, 289, 483);
        
    }
    else if ([sender tag]==5678)
    {
        poptitlelable.text=@"oz";
        dropdownview.frame=CGRectMake(16, 61, 289, 483);
    }
    [self popViewMethod:YES];
    [currentTextfield resignFirstResponder];
    [descriptionTF resignFirstResponder];
    dropdownview.tag=[sender tag];
    
    if(iscategory==YES)
    {
        
        iscategory=NO;
    }
    else
    {
        
        float tableheight=300;
        
        if(categorytxtfeld.text.length>0)
        {
            
        }
        else
        {
            
        }
        
        
        iscategory=YES;
        
    }
    
    [dropdownview reloadData];
}

-(void)successResponseforsizelist:(NSMutableArray *)responseArray
{
    [APPDELEGATE.sizeGlobalarray removeAllObjects];
     dispatch_async(dispatch_get_main_queue(), ^{
    for(int i=0;i<responseArray.count;i++)
    {
        NSMutableDictionary *mydict=[responseArray objectAtIndex:i];
        LookupModelobject *lookobj=[[LookupModelobject alloc]init];
        
        lookobj.loolupName=@"lookupName";
        lookobj.categoryid=[mydict valueForKey:@"categoryId"];
        lookobj.loolupValue=[mydict valueForKey:@"size"];
        lookobj.loolupid=[mydict valueForKey:@"sizeId"];
        
        [APPDELEGATE.sizeGlobalarray addObject:lookobj];
        
    }
         sizeBool=YES;
         dropdownview.tag=222;
         [dropdownview reloadData];
       
     });
      //  [APPDELEGATE.sizeGlobalarray removeAllObjects];
            [ registrationRep getdata:nil :@"getcondition" :@"GET" withcount:@"0"];
}

@end


