//
//  ListPageControl.m
//  MarkidPlace
//
//  Created by stellentmac1 on 5/23/14.
//  Copyright (c) 2014 stellentmac1. All rights reserved.
//

#import "ListPageControl.h"

@implementation ListPageControl

-(id) initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    
    activeImage = [UIImage imageNamed:@"greencircle.png"];
    inactiveImage = [UIImage imageNamed:@"circle.png"];
    
    return self;
}

-(void)updateDots
{
    for (int i = 0; i < [self.subviews count]; i++)
    {
       // NSLog(@"i ===%d",i);
       // NSLog(@"currentPage ===%ld",(long)self.currentPage);
        
        UIView *sview = [self.subviews objectAtIndex:i];
        
        for (UIImageView *iview in sview.subviews)
        {
            [iview removeFromSuperview];
        }
        
        UIImageView *dot = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 12, 12)];
        
        if (i == self.currentPage)
        {
           // dot.image = [UIImage imageNamed:@""];
            dot.image = activeImage;
        }
        else
        {
           // dot.image = [UIImage imageNamed:@""];
            dot.image = inactiveImage;
        }
        
        [sview addSubview:dot];
    }
}

-(void)setCurrentPage:(NSInteger)page
{
    [super setCurrentPage:page];
    [self updateDots];
}

@end
