//
//  ChartModelObject.h
//  MarkidPlace
//
//  Created by Varma Bhupatiraju on 12/06/14.
//  Copyright (c) 2014 stellentmac1. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ChartModelObject : NSObject
@property(nonatomic,readwrite)BOOL isuser;

@property(nonatomic,readwrite)BOOL istext;
@property(nonatomic,strong)NSString *timevalueStr;
@property(nonatomic,strong)NSString *imageurl;
@property(nonatomic,strong)NSString *messageStr;

@property(nonatomic,strong)NSString *namestring;
@property(nonatomic,strong)NSString *createdatechartstr;
@property(nonatomic,strong)UIImage  *postedimage;

@property(nonatomic,strong)NSString *buyeridstr;
@property(nonatomic,strong)NSString *conversationuseridstr;
@property(nonatomic,strong)NSString *selleridstr;
@property(nonatomic,strong)NSString *profilepic;

@property(nonatomic,strong)NSDictionary *profilerespdict;


@end
