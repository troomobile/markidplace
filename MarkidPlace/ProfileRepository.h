//
//  ProfileRepository.h
//  MarkidPlace
//
//  Created by stellentmac1 on 5/31/14.
//  Copyright (c) 2014 stellentmac1. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Reachability.h"
#import "HelperProtocol.h"
#import "AppDelegate.h"
#import "ASIFormDataRequest.h"
#import "JSON.h"

@class AppDelegate;

@interface ProfileRepository : NSObject<HelperProtocol,ASIHTTPRequestDelegate>
{
   // AppDelegate *appdelegate;
    id <HelperProtocol> delegate;
    NSMutableData *respData;
    NSURLConnection *urlconnection;
    

}
@property(nonatomic,strong)id <HelperProtocol> delegate;
-(void)getProfileData:(NSString*)userId;
-(void)followOrUnfollow:(NSMutableDictionary *)dict;
-(void)postProfileDetails :(NSMutableDictionary *)allDetailsdict : (NSData *)imgData;
-(void)deleteUserAccountWithUserid:(NSString*)userid andReasonToDelete:(NSString*)reason;
-(void)changepasswordUserid:(NSString*)UserId currentpassword:(NSString*)currentpassword newpassword:(NSString*)newpassword;
-(void)myaddressgetuserid:(NSString*)userid;
-(void)getnotificationsettings:(NSString*)userid;
-(void)notificationsettingschanges:(NSString*)loginid:(NSString *)mainkey:(NSString*)statuskey;
-(void)updateaddress:(NSString*)userid:(NSMutableDictionary *)updatedic;
-(void)updatebrand:(NSDictionary*)dic;
-(void)getbrandsettings:(NSString*)userid;
-(void)postSizesListByUserid:(NSString*)userid withSelectSizesString:(NSString *)sizesString;
-(void)getSizesListByUserid:(NSString*)userid;
-(void)getSuggestionFriendsListByUserid:(NSString*)userid;
-(void)followUserswithDictionary:(NSMutableDictionary*)dict;
-(void)markassold:(NSString *)objectid;
-(void)getsizelist:(NSString *)categoryid;
-(void)notificationbadge:(NSString*)notificationid;
-(void)messageRead:(NSString*)userid:(NSString*)msguserid;
-(void)emailupdate:(NSMutableDictionary*)addressdict;

-(void)getfilterbrands:(NSString*)userid;
-(void)addressVerification:(NSMutableDictionary*)addressdict
;

@end
