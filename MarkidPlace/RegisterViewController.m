//
//  LoginViewController.m
//  MarkidPlace
//
//  Created by stellentmac1 on 5/21/14.
//  Copyright (c) 2014 stellentmac1. All rights reserved.
//

#import "RegisterViewController.h"
#import "SuggestionsViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "AppDelegate.h"

#import <MobileCoreServices/MobileCoreServices.h> //KISHORE SEP24
@interface RegisterViewController ()

@end

@implementation RegisterViewController
@synthesize navBar,emailTextField,passwordTextField,yourPhotoLabel,profileImageView,cameraLogoImageView,spinner;
@synthesize usernametf,emailstr,passwordstr;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
#pragma mark View Methods

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.

   // myappDelegate=(AppDelegate *)[[UIApplication sharedApplication]delegate];
    screenSize = [[UIScreen mainScreen]bounds];
    self.navigationController.navigationBarHidden = YES;
    currenttextfield=[[UITextField alloc]init];
    
    UITapGestureRecognizer *singletap =
    [[UITapGestureRecognizer alloc]
     initWithTarget:self
     action:@selector(tapDetected:)];
    singletap.numberOfTapsRequired = 1;
    [self.view addGestureRecognizer:singletap];
    
    self.navBar.frame = CGRectMake(0, 0, 320, 64);
    [self.navBar setBackgroundImage:[UIImage imageNamed:@"navbar.png"] forBarMetrics:UIBarMetricsDefault];
    
    self.yourPhotoLabel.font = [UIFont fontWithName:@"Muli" size:16];
    self.emailTextField.font = [UIFont fontWithName:@"Muli" size:18];
    self.passwordTextField.font = [UIFont fontWithName:@"Muli" size:18];
    self.usernametf.font = [UIFont fontWithName:@"Muli" size:18];
  //  NSArray *fonts = [UIFont familyNames];
    
  //  NSLog(@"fonts ===%@",fonts);
    
    [self.emailTextField setValue:[UIColor colorWithRed:179.0/255.0 green:179.0/255.0 blue:179.0/255.0 alpha:1.0] forKeyPath:@"_placeholderLabel.textColor"];
    [self.passwordTextField setValue:[UIColor colorWithRed:179.0/255.0 green:179.0/255.0 blue:179.0/255.0 alpha:1.0] forKeyPath:@"_placeholderLabel.textColor"];
      [self.usernametf setValue:[UIColor colorWithRed:179.0/255.0 green:179.0/255.0 blue:179.0/255.0 alpha:1.0] forKeyPath:@"_placeholderLabel.textColor"];

    
    UILabel *dollerlbl1=[[UILabel alloc]initWithFrame:CGRectMake(0, 0, 20, 20)];
    dollerlbl1.textColor=usernametf.textColor;
    dollerlbl1.font=usernametf.font;
    dollerlbl1.text=@"@";
    
    [usernametf setLeftViewMode:UITextFieldViewModeAlways];
    usernametf.leftView= dollerlbl1;

}
-(IBAction)tapDetected:(id)sender
{
     [self moveViewUp:NO];
    [currenttextfield resignFirstResponder];
}
-(void)viewWillDisappear:(BOOL)animated
{
    [currenttextfield resignFirstResponder];
}

//This Action performs the show the action sheet for select the picture.
-(IBAction)cameraButtonAction:(id)sender
{
    [currenttextfield resignFirstResponder];
    CGRect frame=self.view.frame;
    frame.origin.y=0;
    self.view.frame=frame;
    [UIView commitAnimations];
    
    UIActionSheet *action = [[UIActionSheet alloc]initWithTitle:@"Choose Image" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Camera",@"Gallery", nil];
    [action showInView:self.view];
 }


-(IBAction)loginButtonAction:(id)sender
{
    LoginViewController *loginVC;
    if (myappDelegate.isiphone5)
    {
        loginVC = [[LoginViewController alloc]initWithNibName:@"LoginViewController" bundle:nil];
    }
    else
    {
        loginVC = [[LoginViewController alloc]initWithNibName:@"LoginViewController_iphone4" bundle:nil];
    }
    [self.navigationController pushViewController:loginVC animated:YES];

}

- (BOOL) emailvalidate
{
    NSString *email;
    NSString *password;
    password=self.passwordTextField.text;
    
    email=[self.emailTextField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    
    NSString *emailRegEx = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
	NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegEx];
    
    
    if(email==nil||[email isEqualToString:@""] || email.length==0 || [email isEqualToString:@" "])
    {
        //   [spinner stopAnimating];
        UIAlertView *alertview=[[UIAlertView alloc]initWithTitle:appname message:@"Email should not be empty." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alertview show];
        return NO;
	}
    else if(password==nil||[password isEqualToString:@""] || password.length==0 || [password isEqualToString:@" "])
    {
        //  [spinner stopAnimating];
        UIAlertView *alertview=[[UIAlertView alloc]initWithTitle:appname message:@"Enter a valid password." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alertview show];
        return NO;
    }
    else if ([emailTest evaluateWithObject:email] != YES)
    {
        UIAlertView *loginalert = [[UIAlertView alloc] initWithTitle:appname message:@"Email field not in proper format." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:Nil, nil];
        [loginalert show];
        return NO;
    }
    else if(password.length>1 && password.length<=5)
    {
        UIAlertView *loginalert = [[UIAlertView alloc] initWithTitle:appname message:@"Password is too short." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:Nil, nil];
        [loginalert show];
        return NO;
    }
    else
    {
        return YES;
    }
}

// This Action performs the validation of Email and password and register the details.

-(IBAction)registerButtonAction:(id)sender
{
      [emailTextField resignFirstResponder];
        [passwordTextField resignFirstResponder];
        [usernametf resignFirstResponder];
    
    CGRect rect = self.view.frame;
    rect.origin.y =0;
    emailTextField.text=emailstr;
    passwordTextField.text=passwordstr;
    self.view.frame = rect;
    if (emailTextField.text.length>0 &&
        passwordTextField.text.length>=6 && usernametf.text.length>0 && !CGSizeEqualToSize(profileImageView.image.size, CGSizeZero) )
    {
        NSString *emailRegEx = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
        NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegEx];

        if ([emailTest evaluateWithObject:emailTextField.text] != YES)
        {
            UIAlertView *loginalert = [[UIAlertView alloc] initWithTitle:appname message:@"Email field not in proper format." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:Nil, nil];
            [loginalert show];
            return;
        }

        else
        {
            if ([AppDelegate imagecompare:[UIImage imageNamed:@"photobg.png"] isEqualTo:profileImageView.image])
                 {
                     [AppDelegate alertmethod:appname :@"Please take a photo" ];
                     return;
            }
            
            [myappDelegate startspinner :self.view];
            registrationRep = [[RegistrationRepository alloc]init];
            registrationRep.delegate=self;
            [self performSelector:@selector(postwithdelay) withObject:nil afterDelay:0.3];
        }
    }
    else
    {
        
        if (passwordTextField.text.length<6)
        {
            [AppDelegate alertmethod:appname :@"Password is too short"];
        }
        else{
        [AppDelegate alertmethod:appname :@"All fileds are required including photo."];
        }
    }
}
-(void)errorMessage:(NSString *)message
{
    NSLog(@"error msg %@",message);
    dispatch_sync(dispatch_get_main_queue(), ^{
        [myappDelegate stopspinner:self.view];
        [self.spinner stopAnimating];
        self.view.userInteractionEnabled = YES;
        
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:appname message:@"Registration failed" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        passwordTextField.text=@"";
        
    });
}

// posting the details in Registration Form.
-(void)postwithdelay
{
   
    NSMutableDictionary *regDict = [[NSMutableDictionary alloc]init];
    [regDict setValue:@"" forKey:@"Name"];
    [regDict setValue:[usernametf.text lowercaseString] forKey:@"UserName"];
    [regDict setValue:[emailTextField.text lowercaseString] forKey:@"Email"];
    
    // [regDict setValue:profileImageView.image forKey:@"Profilepic"];
    
    
    NSMutableArray *urlsarray=[myappDelegate postimagestoamazone:NO photosaray:nil :profileImageView.image];
    
    NSString *finalstr=  [[urlsarray valueForKey:@"description"] componentsJoinedByString:@","];

    
    [regDict setValue:finalstr forKey:@"Profilepic"];
    [regDict setValue:passwordTextField.text forKey:@"Password"];
    
    
    
    NSMutableDictionary *devicedict = [[NSMutableDictionary alloc]init];
    
    BOOL  isDeviceIphone=([[UIDevice currentDevice] userInterfaceIdiom]== UIUserInterfaceIdiomPhone);
    
    if (isDeviceIphone)
    {
        [devicedict setValue:@"iphone" forKey:@"DeviceType"];
    }
    else
    {
        [devicedict setValue:@"ipad" forKey:@"DeviceType"];
    }
    
    if (myappDelegate.deviceTokenString.length > 0)
    {
        [devicedict setValue:myappDelegate.deviceTokenString forKey:@"DeviceToken"];
    }
    else
    {
              [devicedict setValue:@"" forKey:@"DeviceToken"];
    }
    
    float version = [[[UIDevice currentDevice] systemVersion] floatValue];
    NSLog(@"*** %f ***",version);
    
    [devicedict setValue:[NSString stringWithFormat:@"%f",version] forKey:@"DeviceOs"];
    
    [devicedict setValue:@"1" forKey:@"DeviceId"];
    [devicedict setValue:@"nothing" forKey:@"DeviceKeychain"];
    
    [regDict setValue:devicedict forKey:@"Device"];
    
    [registrationRep postAllDetails:regDict :@"Registration" :@"POST"];
    [APPDELEGATE dealWithPhoneBookContacts];
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

         //success response for Regitration.
-(void)successResponseforRegistration:(NSDictionary *)responseDict
{
    NSLog(@"responseDict ====%@",responseDict);
    
    //[self.spinner stopAnimating];
    
    dispatch_sync(dispatch_get_main_queue(), ^{
        
         [myappDelegate stopspinner:self.view];
      
    if ([responseDict[@"message"] isEqualToString:@"User Already Existed"])
        {
            NSString *emailstr=[NSString stringWithFormat:@"%@",[responseDict valueForKey:@"message"]];
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:appname message:emailstr delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            alert.tag=12345;
            [alert show];
        }
        else if ([responseDict[@"message"] isEqualToString:@"Invalid UserName or Password.."])
        {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:appname message:@"Incorrect username or password" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
        }
        else if ([responseDict[@"message"] isEqualToString:@"Validate User"] || [responseDict[@"message"] isEqualToString:@"User added successfully"])
        {
            [myappDelegate insertStringInPlist:@"userid" value:responseDict[@"userId"]];
            [myappDelegate insertStringInPlist:@"email" value:responseDict[@"email"]];
            [myappDelegate insertStringInPlist:@"type" value:responseDict[@"type"]];
            if(APPDELEGATE.islistusers==YES)
            {
                APPDELEGATE.islistusers=NO;
                dispatch_async( dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                    [APPDELEGATE performSelector:@selector(getuserslist) withObject:nil afterDelay:0.001];
                });
            }
            NSString *userstr=[responseDict[@"userName"] stringByReplacingOccurrencesOfString:@"@" withString:@""];
            [myappDelegate insertStringInPlist:usernamekey value:userstr];
            
            myappDelegate.useridstring=responseDict[@"userId"];
            
            EditProfileViewController *editprofile;
            if (myappDelegate.isiphone5)
            {
                editprofile=[[EditProfileViewController alloc]initWithNibName:@"EditProfileViewController" bundle:nil];
            }
            else{
                editprofile=[[EditProfileViewController alloc]initWithNibName:@"EditProfileViewController_iphone4" bundle:nil];
            }
            editprofile.isnew=YES;
        
            [self.navigationController pushViewController:editprofile animated:YES];
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:appname message:@"Registration failed." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
        }
    });
}

#pragma mark - Alertview

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(alertView.tag==111||alertView.tag==12345)
    {
        dispatch_async(dispatch_get_main_queue(), ^{

            [self.navigationController popViewControllerAnimated:YES];
        });

    }
}

#pragma mark - Imagepicker

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    NSLog(@"%s",__FUNCTION__);
    
    //------
    NSString *mediaType = info[UIImagePickerControllerMediaType];
    //UIImagePickerControllerOriginalImage
    //UIImagePickerControllerEditedImage
    if ([mediaType isEqualToString:(NSString *)kUTTypeImage])
    {
        UIImage * uiImage=[info valueForKey:@"UIImagePickerControllerEditedImage"];
    
        profileImageView.image=uiImage;
        yourPhotoLabel.hidden=YES;
        cameraLogoImageView.hidden=YES;
        profileImageView.layer.cornerRadius=profileImageView.frame.size.width/2;
        profileImageView.layer.masksToBounds=YES;

        /*
         UIImage *editedImage;
         NSData   *imageData;
         if (uiImage.size.height<640)
         {
         NSLog(@"@@@@@@@@@@@@@@@@@@@ 66666644444000000 @@@@@@@@@@@@@@@@");
         editedImage=[self squareImageFromImage :uiImage scaledToSize:320.0];
         imageData = UIImageJPEGRepresentation(editedImage, 1.0);
         self.profileImgView.image=[UIImage imageWithData:imageData];
         }
         else
         {
         NSLog(@"################ 33333333322222222000000000 ################");
         uiImage = info[UIImagePickerControllerEditedImage];
         imageData = UIImageJPEGRepresentation(uiImage, 1.0);
         self.profileImgView.image=uiImage;
         }
         */
        
    }
    
    [picker dismissViewControllerAnimated:NO completion:nil];
    //-----End
}
#pragma mark - textfield

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    currenttextfield=textField;
    [self moveViewUp:YES];
    
}
-(void) moveViewUp:(BOOL)moveUP
{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.3];

    CGRect rect = self.view.frame;

    CGRect screenbounds=[[UIScreen mainScreen]bounds];

    if (moveUP)
    {
        if(screenbounds.size.height==568)
        {

            if(currenttextfield==self.emailTextField)
            {

                rect.origin.y = -90;
            }
            else if(currenttextfield==self.passwordTextField)
            {

                rect.origin.y = -110;
            }


        }
        else
        {

            if(currenttextfield==self.emailTextField)
            {

                rect.origin.y = -90;
            }
            else if(currenttextfield==self.passwordTextField)
            {

                rect.origin.y = -130;
            }

        }
    }
    else
    {
        rect.origin.y =0;

    }
    self.view.frame = rect;

    [UIView commitAnimations];

}


-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self moveViewUp:NO];
    [textField resignFirstResponder];
    return YES;
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (usernametf==textField)
    {
         NSCharacterSet *cs = [[NSCharacterSet characterSetWithCharactersInString:AlphaNumeric_username] invertedSet];
        
        NSString *filtered = [[string componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
        
        return [string isEqualToString:filtered];
    }
   
    else if (emailTextField==textField)
    {
        
        NSCharacterSet *cs = [[NSCharacterSet characterSetWithCharactersInString:Email] invertedSet];
        
        NSString *filtered = [[string componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
        
        
        return [string isEqualToString:filtered];
    }
    
    return YES;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
//navigstion Back button Action.
-(IBAction)backButtonAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    UIImagePickerController *picker = [[UIImagePickerController alloc]init];
    if(buttonIndex==0)
    {
        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
        picker.delegate =self;
       // picker.wantsFullScreenLayout = YES;
        picker.allowsEditing = YES;
    //    [picker setAllowsEditing:NO];
      //  picker.allowsImageEditing=NO;
        
        [self presentViewController:picker animated:YES completion:nil];
    }
    else if(buttonIndex==1)
    {
        picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        picker.delegate = self;
       // picker.wantsFullScreenLayout = YES;
        picker.allowsEditing = YES;
        [self presentViewController:picker animated:YES completion:nil];
    }
    else
    {
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}

-(void)cropImageViewControllerDidFinished:(UIImage *)image
{
    UIImage *generatedImage = nil;
    NSData *data = UIImageJPEGRepresentation(image,1.0);
    generatedImage = [[UIImage alloc] initWithData:data];
     // imageView.image = image;
    profileImageView.image=generatedImage;
    yourPhotoLabel.hidden=YES;
    cameraLogoImageView.hidden=YES;
    
    profileImageView.layer.cornerRadius=profileImageView.frame.size.width/2;
    
    profileImageView.layer.masksToBounds=YES;
    
}


-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    UITouch *tch = [touches anyObject];
    UIView *tchView = tch.view;
    
    
    if (tchView == self.emailTextField || tchView == self.passwordTextField || tchView == self.usernametf)
    {
    }
    else
    {
        [emailTextField resignFirstResponder];
        [passwordTextField resignFirstResponder];
        [usernametf resignFirstResponder];
    }
}
//successresponse ofuserslist
-(void)successResponseforlistusers:(NSDictionary *)responseDict
{
    NSLog(@"thelist main count is =======>>>>>>>>>><<<<<mainresponse dict is %@",responseDict);
    NSMutableArray *mainArray=[responseDict valueForKey:@"listusers"];
    [APPDELEGATE.userlistArray removeAllObjects];
    for (int i=0; i<mainArray.count; i++)
    {
        NSMutableDictionary *maindict=[mainArray objectAtIndex:i];
        SuggestionObject *suggestionobj=[[SuggestionObject alloc]init];
        suggestionobj.useridstr=[maindict valueForKey:@"userId"];
        suggestionobj.emailstr=[maindict valueForKey:@"email"];
        suggestionobj.followcount=[maindict valueForKey:@"followersCount"];
        suggestionobj.namestr=[maindict valueForKey:@"name"];
        suggestionobj.usernamestr=[maindict valueForKey:@"userName"];
        suggestionobj.profilepicstr=[maindict valueForKey:@"profilepic"];
        suggestionobj.typestr=[maindict valueForKey:@"type"];
        suggestionobj.socialidstr=[maindict valueForKey:@"socialId"];
        [APPDELEGATE.userlistArray addObject:suggestionobj];
    }
}
-(void)internetConnection
{
    UIAlertView *Internetalert=[[UIAlertView alloc]initWithTitle:appname message:@"Internet connection not available" delegate:self cancelButtonTitle:nil otherButtonTitles:@"Ok", nil];
    [Internetalert show];
    [APPDELEGATE stopspinner:self.view];
    
}

@end
