//
//  SettingsViewController.m
//  MarkidPlace
//
//  Created by stellentmac1 on 5/31/14.
//  Copyright (c) 2014 stellentmac1. All rights reserved.
//

#import "SettingsViewController.h"
#import "SupportViewController.h"
#import "ChangePasswordViewController.h"
#import "MyBrandsViewController.h"
#import "MySizesViewController.h"
#import "InviteFriendViewController.h"
#import "MyAdressViewController.h"
#import "QuickTipsViewController.h"
#import "MyAdressViewController.h"
#import "SharingViewController.h"
#import "settingsmodelobject.h"
#import "settingsmodelobject.h"
#import "FAQViewController.h"
#import "FindFriendsViewController.h"
#import "MyClosetViewController.h"
#import "MyWishlistViewController.h"
@interface SettingsViewController ()

@end

@implementation SettingsViewController
{
    SettingsRepository *settingrepo;
}

@synthesize settingsTableView,settingscell;
@synthesize optionsArray,viewtitlelbl,editArray,section3Array,zerosectionArray,firstsectionArray,secondsectionArray,thirdsectionArray,fourthsectionArray,fivethsectionArray,myscrollview,popview,dropdowntblview,brandarray,myaddresstextfiled,brandnamesArray,mybrandsarray;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    settingrepo=[[SettingsRepository alloc]init];
    settingrepo.delegate=self;
    profilerepo=[[ProfileRepository alloc]init];
    profilerepo.delegate=self;
    appdelegate =[[UIApplication sharedApplication]delegate];
    mybrandsarray=[[NSMutableArray alloc]init];
    settingsmodelobject *obj=[[settingsmodelobject alloc]init];
    dropdowntblview.layer.borderWidth = 1;
    dropdowntblview.layer.cornerRadius=1.0f;
   // obj.namestr=[[settingsmodelobject alloc]init];
    dropdowntblview.layer.borderColor =[[UIColor whiteColor ] CGColor];
    self.navbar.frame = CGRectMake(0, 0, 320, 64);
    [self.navbar setBackgroundImage:[UIImage imageNamed:@"navbar.png"] forBarMetrics:UIBarMetricsDefault];
    brandarray=[[NSMutableArray alloc]init];
    
    
    
    //Krish
    [APPDELEGATE.Twitterfollowers removeAllObjects];
    dispatch_async( dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
    
    AddFriendsViewController *addfriendsVC=[[AddFriendsViewController alloc]init];
    addfriendsVC.typeString=@"settings";
    
    [addfriendsVC twitterBtnAction:nil];
    });
    
    // [self performSelector:@selector(twittercontacts_filtering) withObject:nil afterDelay:5.0];
    //Krish
    
    //brandnamesArray=[[NSMutableArray alloc]init];
    
    // myscrollview.contentSize = CGSizeMake(320,600);
    
    
/*
 self.editArray=[[NSMutableArray alloc]initWithObjects:@"Support",@"About marKIDplace" ,nil];
    
    self.optionsArray=[[NSMutableArray alloc]initWithObjects:@"Edit Profile",@"Invite Friends",@"My Account ",@"LogOut", nil];
    */
    //azhar
    self.zerosectionArray=[[NSMutableArray alloc]initWithObjects:@"Invite Friends",@"Find People", nil];
    self.firstsectionArray=[[NSMutableArray alloc]initWithObjects:@"My Sizes",@"My Brands ", nil];
    self.secondsectionArray=[[NSMutableArray alloc]initWithObjects:@"My Closet",@"My Likes",@"My Profile",@"My Address",@"Change Password", nil];
    self.thirdsectionArray=[[NSMutableArray alloc]initWithObjects:@"Sales",@"Purchases", nil];
    self.fourthsectionArray=[[NSMutableArray alloc]initWithObjects:@"Shared Settings",@"Notification Settings", nil];
    
    self.fivethsectionArray=[[NSMutableArray alloc]initWithObjects:@"Your Guide to markIDplace ",@"About MarkIDplace",@"Terms & Conditions",@"Privacy Policy",@"Support",@"Frequently Asked Questions",@"Quick Tips", nil];
    
    
    
  //  myappDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    screenSize = [[UIScreen mainScreen]bounds];
    viewtitlelbl.font = [UIFont fontWithName:@"Muli" size:20];

    [settingsTableView reloadData];
  }
-(IBAction)LogoutAction:(id)sender
{
    
    
    UIAlertView *log=[[UIAlertView alloc]initWithTitle:appname message:@"Are you sure you want to logout?" delegate:self cancelButtonTitle:@"NO" otherButtonTitles:@"YES", nil];
    log.tag=123;
    [log show];
    
}
-(void)viewWillAppear:(BOOL)animated
{
    
    [super viewWillAppear:YES];
    
    /*
    dispatch_async( dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        
         AddFriendsViewController *addfriendsVC=[[AddFriendsViewController alloc]init];
         addfriendsVC.typeString=@"settings";
         
         [addfriendsVC twitterBtnAction:nil];
         // [addfriendsVC  performSelector:@selector(twitterBtnAction:) withObject:nil afterDelay:0];
         
         InviteFriendsViewController *invitefriendsVC=[[InviteFriendsViewController alloc]init];
          [invitefriendsVC  performSelector:@selector(emailbutton:) withObject:nil afterDelay:0];
         //[invitefriendsVC performSelectorOnMainThread:@selector(emailbutton:) withObject:nil waitUntilDone:YES];
        
         
        
    });
*/
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    
    
    
}
- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}
#pragma mark--- UITableView Methods
/*
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return optionsArray.count;
}*/
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    //return 4;
    if (tableView==dropdowntblview)
    {
        return 1;
    }
    else
    {
    return 7;
    }
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView==dropdowntblview)
    {
    return mybrandsarray.count;

    }
    else
    {

        if (section==0)
        {
            return zerosectionArray.count;
        }
        else if (section==1)
        {
            return firstsectionArray.count;
        }
        else if (section==2)
        {
            return secondsectionArray.count;
        }
        else if (section==3)
        {
            return thirdsectionArray.count;
        }
        else if (section==4)
        {
            return fourthsectionArray.count;
        }
        else if (section==5)
        {
            return fivethsectionArray.count;
        }
        else
        {
            return 1;
        }

    }
   
    
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (tableView==dropdowntblview)
    {
        return 0;
    }
    else
    {
         if(section==0)
         {
      return 0;
        }
       else
        {
        return 20;
        }
    }
}



-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView==settingsTableView)
    {
        
        static NSString *cellIdentifier=@"acell";
        
        SettingsCell *tbcell =(SettingsCell*)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        if (tbcell==nil)
        {
            [[NSBundle mainBundle] loadNibNamed:@"SettingsCell" owner:self options:nil];
            tbcell=settingscell;
            tbcell.selectionStyle=UITableViewCellSelectionStyleNone;
        }
        tbcell.namelbl.font = [UIFont fontWithName:@"Muli" size:16];
        
        if (indexPath.section==0)
        {
            tbcell.namelbl.text=[self.zerosectionArray objectAtIndex:indexPath.row];
            
        }
        else if (indexPath.section==1)
        {
            tbcell.namelbl.text=[self.firstsectionArray objectAtIndex:indexPath.row];
            
        }
        else if (indexPath.section==2)
        {
            tbcell.namelbl.text=[self.secondsectionArray objectAtIndex:indexPath.row];
            
        }
        else if (indexPath.section==3)
        {
            tbcell.namelbl.text=[self.thirdsectionArray objectAtIndex:indexPath.row];
        }
        else if (indexPath.section==4)
        {
            tbcell.namelbl.text=[self.fourthsectionArray objectAtIndex:indexPath.row];
        }
        else if (indexPath.section==5)
        {
            tbcell.namelbl.text=[self.fivethsectionArray objectAtIndex:indexPath.row];
        }
        
        if (indexPath.section==6)
        {
            
            tbcell.logoutbtn.hidden=NO;
            tbcell.namelbl.hidden=YES;
            tbcell.accessoryimgview.hidden=YES;
            tbcell.backgroundColor=[UIColor clearColor];
            
        }
        return tbcell;
        
    }
    else if(tableView ==dropdowntblview)
    {
        
        UITableViewCell *tbcell=[tableView dequeueReusableCellWithIdentifier:@"cell"];
        if(tbcell==nil)
        {
            tbcell=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
        }
        tbcell.textLabel.font = [UIFont fontWithName:@"Muli" size:15];
        
        tbcell.textLabel.textColor=[UIColor whiteColor];//
        
        tbcell.textLabel.backgroundColor=[UIColor clearColor];
        tbcell.selectionStyle=UITableViewCellSelectionStyleNone;
        tbcell.contentView.backgroundColor=[UIColor clearColor];
        tbcell.backgroundColor=[UIColor clearColor];
        
        
           // LookupModelobject *lookobj=[mybrandsarray objectAtIndex:indexPath.row];
        
        settingsmodelobject *lookobj=[mybrandsarray objectAtIndex:indexPath.row];
        NSLog(@"lookobj.brandid....%@",lookobj.brandid);
        
                  tbcell.textLabel.text=lookobj.brandname;
            NSLog(@"tbcell.textLabel.text....%@",tbcell.textLabel.text);
       
        //obj.namestr
            if (lookobj.isCheckMark)
            {
                tbcell.accessoryType=UITableViewCellAccessoryCheckmark;
                tbcell.tintColor = [UIColor whiteColor];
                NSString *brandid=lookobj.brandid;
                [brandarray addObject:brandid];
            }
            else
                tbcell.accessoryType=UITableViewCellAccessoryNone;
        
            
            NSLog(@"access type %d",tbcell.accessoryType);
            return tbcell;
        }
    return nil;
    }
-(IBAction)backAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
-(IBAction)DoneAction:(id)sender
{
    
  // NSArray *noDuplicates = [[NSSet setWithArray: brandarray] allObjects];
    
    [appdelegate startspinner:self.view];
   NSString *userid=[myappDelegate getStringFromPlist:@"userid"];
    
    NSString *finalstr=[brandarray componentsJoinedByString:@","];
    
    NSLog(@"finalstr....%@",finalstr);
    NSMutableDictionary *dic=[[NSMutableDictionary alloc]init];
  
     [dic setObject:userid forKey:@"UserId"];
    [dic setObject:finalstr forKey:@"Brands"];
   // [dic setValue:userid forKey:@"UserId"];
    //[dic setValue:finalstr forKey:@"Brands"];
  //  appdelegate= (AppDelegate *)[[UIApplication sharedApplication]delegate];
    

    //calling the repository
    profilerepo=[[ProfileRepository alloc]init];
    profilerepo.delegate=self;
    [profilerepo updatebrand:dic];

    

}
-(void)updatebrandsuccessResponse:(NSDictionary *)responseDict
{
    NSLog(@"responseDict.....%@",responseDict);
    if ([[responseDict valueForKey:@"message"] isEqualToString:@"MyBrands Update Successfully"])
    {
        //[AppDelegate alertmethod:appname :@"Your account has been deleted successfully, Thanks For using Markid place"];
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.001 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [appdelegate stopspinner:self.view];
            UIAlertView *myalert=[[UIAlertView alloc]initWithTitle:appname message:@"Saved!" delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
            myalert.tag=3251;
            [myalert show];
            
            
            //[myappDelegate stopspinner:self.view];
            
            
        });
        
    }

    
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
   
    if (tableView==dropdowntblview)
    {
        NSLog(@"index.......%@",indexPath);
        UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
        
        //dropdownview.separatorStyle=NO;
        cell.selectionStyle=NO;
        
       // if (tableView.tag==111)
       // {
      
        
            //LookupModelobject *lookobj=[mybrandsarray objectAtIndex: indexPath.row];
        settingsmodelobject *lookobj=[mybrandsarray objectAtIndex:indexPath.row];
        
            if (lookobj.isCheckMark)
            {
                lookobj.isCheckMark=NO;
                 NSString *brandid=lookobj.brandid;
                 [brandarray removeObject:brandid];
            }
            else
            {
                lookobj.isCheckMark=YES;
                NSString *brandid=lookobj.brandid;
                [brandarray addObject:brandid];
                //obj.namestr=[lookobj.isCheckMark];
            }
                   [dropdowntblview reloadData];
       // }
        
        //else
        //{
        
           // NSLog(@"cell.accessoryType...%d",cell.accessoryType);
            //NSLog(@"UITableViewCellAccessoryCheckmark...%d",UITableViewCellAccessoryCheckmark);
            if (cell.accessoryType == UITableViewCellAccessoryCheckmark)
            {
                cell.accessoryType = UITableViewCellAccessoryNone;
                cell.tintColor = [UIColor whiteColor];
            }
            else
            {
                cell.accessoryType = UITableViewCellAccessoryCheckmark;
                cell.tintColor = [UIColor whiteColor];
            }
        //}
        
        

        
        
    }
    else
    {
    if (indexPath.section==0)
    {
        if (indexPath.row==0)
        {
            
//            InviteFriendsViewController *inviteVC;
//            if (myappDelegate.isiphone5)
//            {
//                inviteVC=[[InviteFriendsViewController alloc]initWithNibName:@"InviteFriendsViewController" bundle:nil];
//            }
//            else
//            {
//                inviteVC=[[InviteFriendsViewController alloc]initWithNibName:@"InviteFriendsViewController_4s" bundle:nil];viewdid
//                
//            }
//            [self.navigationController pushViewController:inviteVC animated:YES];
//
            InviteFriendViewController *inviteVC;//InviteFriendViewController *inviteVC
            if (myappDelegate.isiphone5)
            {
            inviteVC=[[InviteFriendViewController alloc]initWithNibName:@"InviteFriendViewController" bundle:nil];
            }
            else
            {
                inviteVC=[[InviteFriendViewController alloc]initWithNibName:@"InviteFriendViewController_4s" bundle:nil];
                
            }
            
            [self.navigationController pushViewController:inviteVC animated:YES];
        }
       if (indexPath.row==1)
       {
            FindFriendsViewController *findVC;
            if (myappDelegate.isiphone5)
            {
                findVC=[[FindFriendsViewController alloc]initWithNibName:@"FindFriendsViewController" bundle:nil];
            }
            else
            {
                findVC=[[FindFriendsViewController alloc]initWithNibName:@"FindFriendsViewController_4s" bundle:nil];
                
            }
         /*  dispatch_async( dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
               
               dispatch_async( dispatch_get_main_queue(), ^{
                   [self getuserslist];

               });
           });
           */
           findVC.imFronVC = @"SettingsViewController";
           
            [self.navigationController pushViewController:findVC animated:YES];
        }
        
    }
    else if (indexPath.section==1)
    {
        if (indexPath.row==0)
        {
            //[AppDelegate alertmethod:appname :@"Mysizes Under implementation"];
            
            MySizesViewController *MySizesVC;
            if (myappDelegate.isiphone5)
            {
                MySizesVC=[[MySizesViewController alloc]initWithNibName:@"MySizesViewController" bundle:nil];
            }
            else
            {
                MySizesVC=[[MySizesViewController alloc]initWithNibName:@"MySizesViewController_iphone4" bundle:nil];
                
            }
            [self.navigationController pushViewController:MySizesVC animated:YES];
            
            
        }
        
        else
        {

            NSString *userid=[myappDelegate getStringFromPlist:@"userid"];
            NSLog(@"the userid is %@",userid);
            profilerepo=[[ProfileRepository alloc]init];
            profilerepo.delegate=self;
            [profilerepo getbrandsettings:userid];
           
            [self popViewMethod:YES];
             [APPDELEGATE startspinner:self.view];
           // [dropdowntblview reloadData];
            
        }
    }
    else if (indexPath.section==2)
    {
        
        if (indexPath.row==0)
        {
            
            
           //  [AppDelegate alertmethod:appname :@"MyCloset Under implementation"];
            
            MyClosetViewController *MyclosetVC;
            if (myappDelegate.isiphone5)
            {
                MyclosetVC=[[MyClosetViewController alloc]initWithNibName:@"MyClosetViewController" bundle:nil];
            }
            else
            {
                MyclosetVC=[[MyClosetViewController alloc]initWithNibName:@"MyClosetViewController_iphone4" bundle:nil];
                
            }
            [self.navigationController pushViewController:MyclosetVC animated:YES];
            
            
        }
        
        else if (indexPath.row==1)
        {
             //[AppDelegate alertmethod:appname :@"MyLikes Under implementation"];
            MyWishlistViewController *MywishVC;
            if (myappDelegate.isiphone5)
            {
                MywishVC=[[MyWishlistViewController alloc]initWithNibName:@"MyWishlistViewController" bundle:nil];
            }
            else
            {
                MywishVC=[[MyWishlistViewController alloc]initWithNibName:@"MyWishlistViewController_iphone4" bundle:nil];
                
            }
            [self.navigationController pushViewController:MywishVC animated:YES];
            

            
            
            
            
        }
        else if(indexPath.row==2)
        {
            
            EditProfileViewController *editProfileVC;
            if (myappDelegate.isiphone5)
            {
                editProfileVC = [[EditProfileViewController alloc]initWithNibName:@"EditProfileViewController" bundle:nil];
            }
            else
            {
                editProfileVC = [[EditProfileViewController alloc]initWithNibName:@"EditProfileViewController_iphone4" bundle:nil];
            }
            [self.navigationController pushViewController:editProfileVC animated:YES];
            
        }
        else if (indexPath.row==3)
        {
            
            
            MyAdressViewController *inviteVC;
            if (myappDelegate.isiphone5)
            {
                inviteVC=[[MyAdressViewController alloc]initWithNibName:@"MyAdressViewController" bundle:nil];
            }
            else
            {
                inviteVC=[[MyAdressViewController alloc]initWithNibName:@"MyAdressViewController_4s" bundle:nil];
                
            }
            [self.navigationController pushViewController:inviteVC animated:YES];
            
            
        }
        /*
        else if(indexPath.row==4)
        {
            BalanceViewController *balanceVC;
            if (myappDelegate.isiphone5)
            {
                balanceVC = [[BalanceViewController alloc]initWithNibName:@"BalanceViewController" bundle:nil];
            }
            else
            {
                balanceVC = [[BalanceViewController alloc]initWithNibName:@"BalanceViewController__iphone4" bundle:nil];
            }
            [self.navigationController pushViewController:balanceVC animated:YES];
        }
        */
        else
        {
            
            ChangePasswordViewController *changeVC;
            if (myappDelegate.isiphone5)
            {
                changeVC=[[ChangePasswordViewController alloc]initWithNibName:@"ChangePasswordViewController" bundle:nil];
            }
            else
            {
                changeVC=[[ChangePasswordViewController alloc]initWithNibName:@"ChangePasswordViewController_iphone4" bundle:nil];
                
            }
            [self.navigationController pushViewController:changeVC animated:YES];
            
            
        }
        
    }
    else if (indexPath.section==3)
    {
        BuyandSellViewController *sell;
        if (indexPath.row==0)
        {
            if (myappDelegate.isiphone5)
            {
                sell=[[BuyandSellViewController alloc]initWithNibName:@"BuyandSellViewController" bundle:nil];
            }
            else
            {
                sell=[[BuyandSellViewController alloc]initWithNibName:@"BuyandSellViewController_iphone4" bundle:nil];
            }
            sell.isbuyer=NO;
            [self.navigationController pushViewController:sell animated:YES];
        }
        else
        {
            if (myappDelegate.isiphone5)
            {
                sell=[[BuyandSellViewController alloc]initWithNibName:@"BuyandSellViewController" bundle:nil];
            }
            else
            {
                sell=[[BuyandSellViewController alloc]initWithNibName:@"BuyandSellViewController_iphone4" bundle:nil];
            }
            sell.isbuyer=YES;
            [self.navigationController pushViewController:sell animated:YES];
            
        }
        
    }
    else if (indexPath.section==4)
    {
        if (indexPath.row==0)
        {
            // [AppDelegate alertmethod:appname :@"Shared setting under implementation"];
            SharingViewController *sharedVC;
            
            if(myappDelegate.isiphone5==NO)
            {
                sharedVC=[[SharingViewController alloc]initWithNibName:@"SharingViewController_4s" bundle:nil];
            }
            else if([[UIScreen mainScreen] bounds].size.height == 568)
            {
                sharedVC=[[SharingViewController alloc]initWithNibName:@"SharingViewController" bundle:nil];
            }
            
            [self.navigationController pushViewController:sharedVC animated:YES];
            

    
            
        }
        else
        {
            NotificationSettingsViewController *notificationVC;
            
            if(myappDelegate.isiphone5==NO)
            {
                notificationVC=[[NotificationSettingsViewController alloc]initWithNibName:@"NotificationSettingsViewController_4s" bundle:nil];
            }
            else if([[UIScreen mainScreen] bounds].size.height == 568)
            {
                notificationVC=[[NotificationSettingsViewController alloc]initWithNibName:@"NotificationSettingsViewController" bundle:nil];
            }
            
            [self.navigationController pushViewController:notificationVC animated:YES];
            
        }
    }
    else if (indexPath.section==5)
    {
        if (indexPath.row==0)
        {
            [AppDelegate alertmethod:appname :@"Your Guide to markIDplace under implementation"];
//            MyAdressViewController *inviteVC;
//            if (myappDelegate.isiphone5)
//            {
//                inviteVC=[[MyAdressViewController alloc]initWithNibName:@"MyAdressViewController" bundle:nil];
//            }
//            else
//            {
//                inviteVC=[[MyAdressViewController alloc]initWithNibName:@"MyAdressViewController_4s" bundle:nil];
//                
//            }
//            [self.navigationController pushViewController:inviteVC animated:YES];
//            
            
        
            
        }
        else if (indexPath.row==1)
        {
            [AppDelegate alertmethod:appname :@"About MarkIDplace under implementation"];
            
            
        }
        else if (indexPath.row==2)
        {
            [AppDelegate alertmethod:appname :@"Terms & Conditions"];
        }
        else if (indexPath.row==3)
        {
            [AppDelegate alertmethod:appname :@"PrivacyPolicy"];
        }
        else if (indexPath.row==4)
        {
            
            SupportViewController *supportVC;
            
            if(myappDelegate.isiphone5==NO)
            {
                supportVC=[[SupportViewController alloc]initWithNibName:@"SupportViewController_iphone4s" bundle:nil];
            }
            else if([[UIScreen mainScreen] bounds].size.height == 568)
            {
                supportVC=[[SupportViewController alloc]initWithNibName:@"SupportViewController" bundle:nil];
            }
            
            [self.navigationController pushViewController:supportVC animated:YES];
            
        }
        else if (indexPath.row==5)
        {
            //   [AppDelegate alertmethod:appname :@"Frequently Asked Questions under implentation "];
            FAQViewController *termsVC;
            
            CGRect screenbounds = [[UIScreen mainScreen] bounds];
            
            if(screenbounds.size.height==568)
            {
                termsVC = [[FAQViewController alloc] initWithNibName:@"FAQViewController" bundle:nil];
            }
            else if(screenbounds.size.height==480)
            {
                termsVC = [[FAQViewController alloc] initWithNibName:@"FAQViewController" bundle:nil];
            }
            termsVC.htmlMainString=@"MKPFAQrevised";
            termsVC.titleString=@"MKPFAQrevised";
            termsVC.isFAQ=@"Yes";
            [self.navigationController pushViewController:termsVC animated:YES];
        }
        else
        {
            //[AppDelegate alertmethod:appname :@"Quick Tips Under Implementation"];
            
           QuickTipsViewController  *quickVC;
            
            if (myappDelegate.isiphone5)
            {
                quickVC=[[QuickTipsViewController alloc]initWithNibName:@"QuickTipsViewController" bundle:nil];
            }
            else
            {
                quickVC=[[QuickTipsViewController alloc]initWithNibName:@"QuickTipsViewController" bundle:nil];
                
            }
           // quickVC.presentstr=@"abc";
            quickVC.htmlMainString=@"quicktips";
            quickVC.titleString=@"quicktips";
            [self.navigationController pushViewController:quickVC animated:YES];
        }
    }
    else if(indexPath.section==6)
        
    {
        //this is submit button no need to do anything.
    }
    
    }
}
-(IBAction)cancelBtnAction:(id)sender
{
    
    [self popViewMethod:NO];
}
-(void)successResponsegetbrandsettings:(NSMutableArray *)responsearray;
{
   //
    [mybrandsarray removeAllObjects];
    dispatch_async(dispatch_get_main_queue(), ^{
        
        [APPDELEGATE stopspinner:popview];
        [APPDELEGATE stopspinner:self.view];
    for (int i=0;i<[responsearray count];i++)
    {
        settingsmodelobject *obj=[[settingsmodelobject alloc]init];
        NSMutableDictionary *responsedic=[responsearray objectAtIndex:i];

        NSLog(@"responsedict1...%@",responsearray);
   
    obj.brandname=[responsedic valueForKey:@"lookupValue"];
    NSLog(@"obj.namestr....%@",obj.brandname);
    obj.brandid=[responsedic valueForKey:@"lookupID"];
    obj.isCheckMark=[[responsedic valueForKey:@"isSelect"] boolValue];
    
    NSLog(@"obj.brandid...%@",obj.brandid);
    [mybrandsarray addObject:obj];
    }
    [APPDELEGATE stopspinner:self.view];
    [dropdowntblview reloadData];
    });

}
-(void)popViewMethod :(BOOL)isshow
{
    CGRect popfrm=popview.frame;
    popfrm.origin.x=0;
    if (isshow)
    {
        // [self.view addSubview:popview];
        popfrm.origin.y=0;
        
        
        CATransition *transition = [CATransition animation];
        transition.duration = 0.3;
        transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
        transition.type = kCATransitionPush;
        transition.subtype =kCATransitionFromTop;
        transition.delegate = self;
        [popview.layer addAnimation:transition forKey:nil];
    }
    else
    {
        popfrm.origin.y=605;
        CATransition *transition = [CATransition animation];
        transition.duration = 0.3;
        transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
        transition.type = kCATransitionPush;
        transition.subtype =kCATransitionFromBottom;
        transition.delegate = self;
        [popview.layer addAnimation:transition forKey:nil];
    }
    
    //[UIView beginAnimations: @"anim" context: nil];
    //[UIView setAnimationBeginsFromCurrentState: YES];
    // [UIView setAnimationDuration:0.3];
    popview.frame=popfrm;
    // [UIView commitAnimations];
    [self.view bringSubviewToFront:popview];
    [self.view addSubview:popview];
    [APPDELEGATE stopspinner:self.view];
    
}
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag==123)
    {
        if (buttonIndex==1)
        {
            [APPDELEGATE insertStringInPlist:@"notificationcount" value:@""];
            LoginViewController *loginvc;
            if (myappDelegate.isiphone5)
            {
                //appdelegate.isiphone5=YES;
                loginvc = [[LoginViewController alloc]initWithNibName:@"LoginViewController" bundle:nil];
            }
            else
            {
                //appdelegate.isiphone5=NO;
                loginvc = [[LoginViewController alloc]initWithNibName:@"LoginViewController_iphone4" bundle:nil];
            }
            
            [myappDelegate insertStringInPlist:usernamekey value:@""];
            [myappDelegate insertStringInPlist:@"userid" value:@""];
             [myappDelegate insertStringInPlist:@"realname" value:@""];
            
            [myappDelegate.suggestonsGlobalarray removeAllObjects];
            [myappDelegate.myfeedsGlobalarray removeAllObjects];
            [myappDelegate.myclosetGlobalarray removeAllObjects];
            [myappDelegate.mylikesGlobalarray removeAllObjects];
            [myappDelegate.cartGlobalarray removeAllObjects];
            [myappDelegate.allLikesGlobalarray removeAllObjects];
            
            myappDelegate.naviCon = [[UINavigationController alloc]initWithRootViewController:loginvc];
            myappDelegate.naviCon.navigationBarHidden=YES;
            myappDelegate.window.rootViewController = myappDelegate.naviCon;

        }
    }
    if (alertView.tag==12345)
    {
        [self popViewMethod:NO];
    }
    if (alertView.tag==3251)
    {
        if (buttonIndex==0)
        {
             [self popViewMethod:NO];
        }
        
    }
}


- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    
    //if (scrollView==dropdownview && dropdownview.tag==333)
    {
        CGFloat sectionHeaderHeight = 30;
        if (scrollView.contentOffset.y<=sectionHeaderHeight&&scrollView.contentOffset.y>=0) {
            scrollView.contentInset = UIEdgeInsetsMake(-scrollView.contentOffset.y, 0, 0, 0);
        } else if (scrollView.contentOffset.y>=sectionHeaderHeight)
        {
            scrollView.contentInset = UIEdgeInsetsMake(-sectionHeaderHeight, 0, 0, 0);
        }
    }
  /*  else
    {
        CGFloat sectionHeaderHeight = 0;
        if (scrollView.contentOffset.y<=sectionHeaderHeight&&scrollView.contentOffset.y>=0) {
            scrollView.contentInset = UIEdgeInsetsMake(-scrollView.contentOffset.y, 0, 0, 0);
        }
        else if (scrollView.contentOffset.y>=sectionHeaderHeight)
        {
            scrollView.contentInset = UIEdgeInsetsMake(-sectionHeaderHeight, 0, 0, 0);
        }
    }*/
}
-(void)getuserslist
{
    
    dispatch_async( dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        
        dispatch_async( dispatch_get_main_queue(), ^{
            NSString *useridstring;
            useridstring=[APPDELEGATE getStringFromPlist:@"userid"];
            if([APPDELEGATE isnullorempty:useridstring])
            {
                APPDELEGATE.islistusers=YES;
            }
            else
            {
                //SettingsRepository *settingrepo=[[SettingsRepository alloc]init];
                //settingrepo.delegate=self;
                
                [settingrepo getListUsers:useridstring];
                              //[invitefriendsVC performSelectorInBackground:@selector(smsbutton:) withObject:nil];
                //  [invitefriendsVC performSelectorInBackground:@selector(emailbutton:) withObject:nil];
            }
        });
    });
}
-(void)internetConnection
{
    dispatch_async(dispatch_get_main_queue(), ^{

    UIAlertView *Internetalert=[[UIAlertView alloc]initWithTitle:appname message:@"Internet connection not available" delegate:self cancelButtonTitle:nil otherButtonTitles:@"Ok", nil];
   
    [APPDELEGATE stopspinner:self.view];
    
    self.view .userInteractionEnabled=YES;
    //Internetalert.tag=12345;
         [Internetalert show];
    });
  
    
}
@end
