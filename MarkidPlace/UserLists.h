//
//  UserLists.h
//  MarkidPlace
//
//  Created by Varma Bhupatiraju on 5/31/14.
//  Copyright (c) 2014 stellentmac1. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UserLists : NSObject
@property(nonatomic) NSString *userListingIdStr;
@property(nonatomic) NSString *userItemIdStr;
@property(nonatomic) NSString *categoryIdStr;
@property(nonatomic) NSString *listPriceStr;
@property(nonatomic) NSString *statusStr;
@property(nonatomic) NSString *inPersonStr;
@property(nonatomic) NSString *uspsStr;
@property(nonatomic) NSString *userIDStr;
@property(nonatomic) NSString *unitPriceStr;
@property(nonatomic) NSString *categoryNameStr;
@property(nonatomic) NSString *sizeIdStr;
@property(nonatomic) NSString *sizeStr;
@property(nonatomic) NSString *brandIdStr;
@property(nonatomic) NSString *brandStr;
@property(nonatomic) NSString *conditionIdStr;
@property(nonatomic) NSString *conditionStr;
@property(nonatomic) NSString *descriptionStr;
@property(nonatomic) NSString *itemNameStr;
@property(nonatomic) NSString *availableQuantityStr;
//@property(nonatomic) NSString *itemStatus;

@property(nonatomic) NSMutableArray *itemPicturesArray;
@property(nonatomic,readwrite)BOOL likestatus;



@end
