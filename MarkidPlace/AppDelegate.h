//
//  AppDelegate.h
//  MarkidPlace
//
//  Created by stellentmac1 on 5/21/14.
//  Copyright (c) 2014 stellentmac1. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RegisterViewController.h"
#import "LoginViewController.h"
#import "CustomActivity.h"
#import "SuggestionsViewController.h"
#import "SuggestionObject.h"
#import <SDWebImage/SDImageCache.h>
#import <SDWebImage/UIImageView+WebCache.h>
#import "HomeViewController.h"
#import "EditProfileViewController.h"
//********Amazon Server********//
#import <AWSS3/AWSS3.h>
//#import "Constants.h"
#import <AWSRuntime/AWSRuntime.h>
#import <ImageIO/ImageIO.h>
#import "InviteFriendsViewController.h"
#import "RegistrationRepository.h"



#import "MDRadialProgressView.h"
#import "MDRadialProgressTheme.h"
#import "MDRadialProgressLabel.h"
#import <BugSense-iOS/BugSenseController.h>
#import "SettingsRepository.h"
#import "HelperProtocol.h"
#import "AddFriendsViewController.h"
#import <CoreData/CoreData.h>
#import "PhoneContacts.h"

@class LoginViewController;
@class SuggestionObject;
@class SuggestionsViewController;
@class HomeViewController;
@class CustomActivity;
@class EditProfileViewController;
@class SettingsRepository;
@class InviteFriendsViewController;
@class AddFriendsViewController;

@interface AppDelegate : UIResponder <UIApplicationDelegate,UINavigationControllerDelegate,AmazonServiceRequestDelegate,HelperProtocol>
{
    CGRect screenSize;
    AppDelegate*appdelegate;
    NSString *superstr;
    // screen size
    NSTimer *timer;
    RegistrationRepository*registrationRep;
    PhoneContacts*phonecontactsobj;
}

+(BOOL)isiphone5;

//login userid
@property (strong, nonatomic) NSString *useridstring;

@property (strong, nonatomic) NSString *deviceTokenString;

// activity indicator
@property(nonatomic,strong) CustomActivity *customactivityObj;
@property(nonatomic,strong) SuggestionObject *suggestionobj;

@property(nonatomic,strong) SuggestionsViewController *suggestionviewcont;
@property(nonatomic,strong) HomeViewController *homeviewcont;
@property(nonatomic ,strong)EditProfileViewController *editprofilevc;
@property (nonatomic,strong)id<HelperProtocol>delegate;

//@property (nonatomic,readwrite)BOOL islikedbool;
//@property (nonatomic,readwrite)BOOL issymbolbool;
@property (nonatomic,readwrite)BOOL isiphone5;
@property (nonatomic,readwrite)BOOL islistusers;

@property (strong, nonatomic) UIWindow *window;
@property (nonatomic, strong) UINavigationController *naviCon;

@property(nonatomic,strong) LoginViewController *loginvc;

// insert in to plist
-(void)insertStringInPlist:(NSString *)key value:(NSString *)value;
// get from plist
-(NSString *)getStringFromPlist:(NSString *)key;

// check empty on null conditon for object
-(BOOL)isnullorempty:(id)sender;

// alert method
+(void)alertmethod :(NSString *)Titlestring :(NSString *)messagestring;

// activity ind start & act ind stop
-(void)startspinner :(UIView *)inview;
-(void)stopspinner :(UIView *)inview;
-(void)getuserslist;

// unique string for images posting on
+(NSString *)uniqueidstring;

// images compare method
+(BOOL)imagecompare:(UIImage *)image1 isEqualTo:(UIImage *)image2;

///// for listing and filter

@property (strong, nonatomic) NSMutableArray *catogoryGlobalarray;
@property (strong, nonatomic) NSMutableArray *sizeGlobalarray;
@property (strong, nonatomic) NSMutableArray *conditionGlobalarray;
@property (strong, nonatomic) NSMutableArray *distanceGlobalarray;
@property (strong, nonatomic) NSMutableArray *brandGlobalarray;
@property (strong, nonatomic) NSMutableArray *filterbrandGlobalarray;
@property (strong, nonatomic) NSMutableArray *genderglobalaray;
@property (strong, nonatomic) NSMutableArray *emailArray;
@property (strong, nonatomic) NSMutableArray *contactsArray;
@property (strong, nonatomic) NSMutableArray *cartGlobalarray;
@property (strong, nonatomic) NSMutableArray *userlistArray;
@property (strong, nonatomic) NSMutableArray *suggestionslistArray;
@property (strong, nonatomic) NSMutableArray *Twitterfollowers;


@property (strong, nonatomic) NSMutableArray *suggestonsGlobalarray;
@property (strong, nonatomic) NSMutableArray *mylikesGlobalarray;
@property (strong, nonatomic) NSMutableArray *myfeedsGlobalarray;
@property (strong, nonatomic) NSMutableArray *myclosetGlobalarray;
@property (strong, nonatomic) NSMutableArray *allLikesGlobalarray;

@property(strong, nonatomic) NSMutableArray *horseanimationArrray;

//CoreData
@property (strong, nonatomic) NSManagedObjectContext *managedcontext;
@property (strong, nonatomic) NSManagedObjectModel *managedobjectmodel;
@property (strong, nonatomic) NSPersistentStoreCoordinator *persistancestore;
@property(strong, nonatomic) NSMutableArray *coreDataContactsAray;
@property(strong, nonatomic) NSMutableArray *newlyAddedContactsAray;

// navigation font
+(UIFont *)navtitlefont;

// hedding font
+(UIFont *)heddingfont :(int)fontsize;


//@property (strong, nonatomic) UIImage *cartglobalimage;


+(UILabel *)lablefitToWidth:(UILabel *)mylabel :(float)maxwidth;


-(NSMutableArray *)parselistingitem :(id)respdict :(BOOL)islikesarray :(NSMutableArray *)likesarray :(BOOL)ismylikes;


#pragma mark amazone
//********Amazon Server********//



@property (nonatomic, retain) AmazonS3Client *s3Clientobject;

-(void)configureAmazoneservics;

-(NSMutableArray *)postimagestoamazone :(BOOL)isbulk  photosaray:(NSMutableArray *)myphotosarray :(UIImage *)myimage;

//// amazone \\\\\\\

-(void)createmyscrollview :(UIScrollView *)itemscrollview :(NSMutableArray *)itemsarray :(int)x :(int)y :(id)targetviewcont;

+(void)assaignitemimages :(CGRect)rect :(NSMutableArray *)itemsarray :(UIScrollView *)itemscrollview;

-(void)likeandunlikestatus :(NSMutableArray *)araytoprocess :(NSMutableArray *)likesarray;

-(NSMutableArray *)sortmyarray :(NSString *)sortfield :(NSMutableArray *)mutablearray;


-(void)likeresponce:(NSDictionary *)likerespdict;




-(void)parsedataforCategory :(NSDictionary*)respdict;

-(void)animatemyview:(UIView *)myview;


-(void)sucessresponceforFollowandunfollow:(NSDictionary *)maindict;


-(void)horseanimation :(UIImageView *)animationv;

+(void)cropimage:(UIViewController *)mycont :(UIImage*)myimage;



-(void)uniquecharacters:(NSMutableArray *)myarray keyvalue:(NSString *)mykey :(id)condid ;

//Krish
-(void)dealWithPhoneBookContacts;
-(void)getContactsFromPhone;
-(void)insertingContactsinCoreData:(NSMutableArray *)mainArray;
-(void)getContactsfromCoreData;
-(void)validateNewlyAddedContactsInAddressbook:(NSMutableArray *)emailarray compareWith:(NSMutableArray *)coreDataArray;


@end
