//
//  NewListingViewController.h
//  MarkidPlace
//
//  Created by stellentmac1 on 5/21/14.
//  Copyright (c) 2014 stellentmac1. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import "AppDelegate.h"
#import "SuggestionsViewController.h"
#import "RegistrationRepository.h"
#import "LookupModelobject.h"
#import "HomeViewController.h"
#import "FeePolicyViewController.h"
#import "Reachability.h"
#import "JSON.h"

//#import "NSData+Base64.h"
#import "ASIFormDataRequest.h"

#import "NewListingCustomcell.h"
#import "Newmodelobject.h"
#import "HelperProtocol.h"
#import "ProfileRepository.h"

@class RegistrationRepository;
@class AppDelegate;
@class SuggestionsViewController;
@class ProfileRepository;
@interface NewListingViewController : UIViewController<UITextFieldDelegate,UITableViewDataSource,UITableViewDelegate,UIImagePickerControllerDelegate,HelperProtocol,UIAlertViewDelegate,UITextViewDelegate,UIActionSheetDelegate,ASIHTTPRequestDelegate>
{
    CGRect screenSize;
   // BOOL issize;
   // BOOL iscondition;
    BOOL iscategory;
  //  BOOL issubCategory;
    
    BOOL isupsc;
    BOOL ismetting;
    BOOL sizeBool;
   // BOOL iscanmetting;
 //   BOOL isbrand;
 //   BOOL isavailable;
    

  //  BOOL isforsale;
    
    AppDelegate *appdelegate;
    BOOL isgalleryapear;
    UITextField *currentTextfield;
  //  AppDelegate *appdelegate;
    
    UIImageView *currentimageview;
   
    int imgtag;
    
    RegistrationRepository *registrationRep;

   // NSMutableArray *catogoryarray;
   // NSMutableArray *sizearray;
   // NSMutableArray *conditionarray;
   // NSMutableArray *brandarray;
   
    NSString *itemidstr;
    
    NSString *catogeryotherstr;
    BOOL isothercat;
    ProfileRepository *profilerepo;
    UILabel *actiondatelabel;
    NSArray *lbsArray;
    NSArray *ozArray;
    NSMutableData *respdata;
    NSString *boundary;
    NSData *imageData;
    
    IBOutlet UIImageView *categoryImageView;
    IBOutlet UIImageView *itemnameImageView;
    
    IBOutlet UIImageView *subCategoryImgView;
    IBOutlet UITextField *subCategoryTextField;
    IBOutlet UILabel *subCategory_StatLbl;
    IBOutlet UIButton *subCategoryBtn;
    int phototag;
       NSString *parentid;
    NSMutableArray *genderarray;
    NSMutableArray *weightArray;
    NSString*catidstr;
}



@property (strong, nonatomic) IBOutlet UILabel *lbslbl;
@property (strong, nonatomic) IBOutlet UILabel *ozlbl;
@property (strong, nonatomic) IBOutlet UILabel *lbsweightlbl;
@property (strong, nonatomic) IBOutlet UILabel *ozweightlbl;
@property (strong, nonatomic) IBOutlet UILabel *weightlbl;
@property (strong, nonatomic) IBOutlet UIImageView *createimageview;
@property (nonatomic,readwrite) BOOL isupdate;

@property(nonatomic,strong) id<HelperProtocol>delegate;
@property(nonatomic,strong) NSMutableArray *photosarray;
@property(nonatomic,strong) ItemsModelobject *selecteditemobj;


@property (nonatomic,strong) IBOutlet UITableView *dropdownview;

@property (nonatomic,strong) IBOutlet UIButton *cancelbtn;
@property (nonatomic,strong) IBOutlet UINavigationBar *navbar;
@property (nonatomic,strong) IBOutlet UIScrollView *listingScrollView;
@property (nonatomic,strong) IBOutlet UIImageView *mettingimgview;
@property (nonatomic,strong) IBOutlet UIImageView *upscimgview;
@property (nonatomic,strong) IBOutlet UIImageView *canMettingimgview;
@property (nonatomic,strong) IBOutlet UILabel *ListingLabel;
@property (nonatomic,strong) IBOutlet UILabel *photosLabel;
@property (nonatomic,strong) IBOutlet UILabel *detailsLabel;
@property (nonatomic,strong) IBOutlet UITextField *categorytxtfeld;
@property (nonatomic,strong) IBOutlet UILabel *categoryNameLabel;
@property (nonatomic,strong) IBOutlet UILabel *sizeLabel;
@property (nonatomic,strong) IBOutlet UILabel *sizeNameLabel;
@property (nonatomic,strong) IBOutlet UILabel *brandLabel;
@property (nonatomic,strong) IBOutlet UILabel *brandNameLabel;
@property (nonatomic,strong) IBOutlet UILabel *conditionLabel;
@property (nonatomic,strong) IBOutlet UILabel *conditionNameLabel;
@property (nonatomic,strong) IBOutlet UILabel *descrptionLabel;
@property (nonatomic,strong) IBOutlet UILabel *descrptionNameLabel;
@property (nonatomic,strong) IBOutlet UILabel *shippingLabel;
@property (nonatomic,strong) IBOutlet UILabel *meetingLabel;
@property (nonatomic,strong) IBOutlet UILabel *canmeetLabel;
@property (nonatomic,strong) IBOutlet UILabel *markidLabel;
@property (nonatomic,strong) IBOutlet UILabel *priceLabel;
@property (nonatomic,strong) IBOutlet UILabel *sellLabel;
@property (nonatomic,strong) IBOutlet UILabel *originalLabel;
@property (nonatomic,strong) IBOutlet UILabel *listpriceLabel;
@property (nonatomic,strong) IBOutlet UILabel *earningsLabel;
@property (nonatomic,strong) IBOutlet UILabel *quicktipsLabel;
@property (nonatomic,strong) IBOutlet UIButton *categoryBtn;
@property (nonatomic,strong) IBOutlet UIButton *sizeBtn;
@property (nonatomic,strong) IBOutlet UIButton *conditionBtn;

@property (nonatomic,strong)  NSString *weight;
@property (nonatomic,strong)  NSString *getweightstr;

@property (nonatomic,strong) IBOutlet UIButton *upscBtn;
@property (nonatomic,strong) IBOutlet UIButton *mettingBtn;
@property (nonatomic,strong) IBOutlet UIButton *canMettingBtn;
@property (nonatomic,strong) SuggestionsViewController *suggestionsVC;

-(IBAction)backAction:(id)sender;
-(IBAction)categoryBtnAction:(id)sender;
-(IBAction)subCategoryBtnAction:(id)sender;

-(IBAction)sizeBtnAction:(id)sender;
-(IBAction)conditionBtnAction:(id)sender;

-(IBAction)upscAction:(id)sender;
-(IBAction)mettingBtnAction:(id)sender;
-(IBAction)canMettingBtnAction:(id)sender;
-(IBAction)homeBtnAction:(id)sender;

-(IBAction)camraBtnAction:(id)sender;

-(IBAction)DoneBtnAction:(id)sender;
-(IBAction)cancelBtnAction:(id)sender;



@property(nonatomic,strong) IBOutlet   UIImageView *listingfirstimgview;
@property(nonatomic,strong) IBOutlet   UIScrollView *photosScrollview;
@property (nonatomic,strong) IBOutlet UILabel *poptitlelable;


@property (nonatomic,strong) IBOutlet UITextField *originalpriceTF;
@property (nonatomic,strong) IBOutlet UITextField *listpriceTF;
@property (nonatomic,strong) IBOutlet UITextField *earningpriceTF;
@property (nonatomic,strong) IBOutlet UITextView *descriptionTF;
@property (nonatomic,strong) IBOutlet UITextField *brandTF;
@property (nonatomic,strong) IBOutlet UITextField *itemnameTF;

@property (nonatomic,strong) IBOutlet UITextField *quantityTF;

@property (nonatomic,strong) IBOutlet UITextField *weightTF;
@property (nonatomic,strong) IBOutlet UITextField *startdateTF;
@property (nonatomic,strong) IBOutlet UITextField *enddateTF;


@property (nonatomic,strong) IBOutlet UILabel *itemnameLabel;
@property (nonatomic,strong) IBOutlet UILabel *availableLabel;
@property (nonatomic,strong) IBOutlet UILabel *startdateLabel;
@property (nonatomic,strong) IBOutlet UILabel *enddateLabel;


-(IBAction)submitBtnAction:(id)sender;
-(IBAction)selectdateAction:(id)sender;
@property(nonatomic,strong) NSDate *startDate,*endDate;
@property(nonatomic,strong) UIDatePicker *datePicker;
@property(nonatomic,strong) UIActionSheet *actionSheet;

-(IBAction)quicktipsbtnaction:(id)sender;
-(IBAction)quicktipsbtnunderaction:(id)sender;
/// connection
-(IBAction)ActionBtnAction:(id)sender;


@property (nonatomic, strong) NSURLConnection *con;
@property (nonatomic, strong) NSMutableData *mdata;
@property(nonatomic,retain)NSArray *successarray;
@property (nonatomic,strong) IBOutlet UIView *popview;


@property (nonatomic,strong) IBOutlet NewListingCustomcell *nlcustomcell;
@property (nonatomic,strong) IBOutlet UIView *earningview;
@property (nonatomic,strong) IBOutlet UITableView *categorytableview;

-(IBAction)categorybtncellaction:(id)sender;
@property(nonatomic,strong)NSMutableArray *selltablearray;
-(IBAction)weightBtnAction:(id)sender;

@end
