//
//  Item.h
//  MarkidPlace
//
//  Created by Varma Bhupatiraju on 5/31/14.
//  Copyright (c) 2014 stellentmac1. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Item : NSObject

@property(nonatomic) NSString *userItemPictureIdStr;
@property(nonatomic) NSString *userItemIdStr;
@property(nonatomic) NSString *pictureCdnUrlStr;
@property(nonatomic) NSString *createdDateStr;

@end
