//
//  FriendsObject.h
//  MarkidPlace
//
//  Created by stellent on 03/06/14.
//  Copyright (c) 2014 stellentmac1. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FriendsObject : NSObject
@property(nonatomic,strong)NSString *string_follwerPicUrl;
@property(nonatomic,strong)NSString *string_follwerName;
@property(nonatomic,strong)NSString *string_follwerUserName;

@property(nonatomic,strong)NSString *useridstr;
@property (nonatomic,readwrite)BOOL isstatus;

@end
