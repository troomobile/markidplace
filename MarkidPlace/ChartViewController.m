//
//  ChartViewController.m
//  MarkidPlace
//
//  Created by Varma Bhupatiraju on 11/06/14.
//  Copyright (c) 2014 stellentmac1. All rights reserved.
//

#import "ChartViewController.h"
#import "ChartModelObject.h"
@interface ChartViewController ()

@end

@implementation ChartViewController
@synthesize navbar,chartscrollview,chartsearchbar,itemsarray,sendtextfield,cameraview,duplicatearray,titlbl,titlestr;

@synthesize selleruseridstr,camimageview;


@synthesize isloginseller,profilepicstr;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
#pragma mark view methods
- (void)viewDidLoad
{
    [super viewDidLoad];
    //   appdelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
   
    screenSize = [[UIScreen mainScreen]bounds];
    duplicatearray=[[NSMutableArray alloc]init];
    
    itemsarray =[[NSMutableArray alloc]init];
    
    [chartsearchbar setShowsCancelButton:NO animated:YES];
    chartsearchbar.placeholder=@"Search";
    x=15;
    y=15;
    
    
    self.navbar.frame = CGRectMake(0, 0, 320, 64);
    [self.navbar setBackgroundImage:[UIImage imageNamed:@"navbar.png"]
                      forBarMetrics:UIBarMetricsDefault];
    
    
    self.titlbl.font =[AppDelegate navtitlefont];
    self.titlbl.text=titlestr;
    
    
    loginuserpick=[myappDelegate getStringFromPlist:@"profilePicture"];
    
    registrationRep=[[RegistrationRepository alloc]init];
    registrationRep.delegate=self;
    [myappDelegate startspinner:self.view];
    [self getDataWithDelay];
}
-(void)viewWillAppear:(BOOL)animated
{
    
    
}
#pragma mark tableviewdelegate
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return itemsarray.count;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    //return  mainbackgroundview.frame.size.height+10;
    return 1234;
 }



- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}
-(void)viewDidDisappear:(BOOL)animated
{
    
    [UIViewController cancelPreviousPerformRequestsWithTarget:self ];
    
    [registrationRep cancelrequest];
    
    [UIViewController cancelPreviousPerformRequestsWithTarget:self ];
    
    [registrationRep cancelrequest];
}

//here using getdatawithdelaymethod from viewdidload
-(void)getDataWithDelay
{
    
    NSDictionary *mydic;
    // if (isloginseller)
    {
        // mydic=[[NSDictionary alloc]initWithObjectsAndKeys:[NSNumber numberWithInt:0],@"count",myappDelegate.useridstring,@"SellerUserId",selleruseridstr,@"BuyerUserId", nil];
    }
    // else
    {
        NSLog(@"%@",selleruseridstr);
        NSLog(@"%@",myappDelegate.useridstring);
        mydic=[[NSDictionary alloc]initWithObjectsAndKeys:[NSNumber numberWithInt:0],@"count",myappDelegate.useridstring,@"SellerUserId",selleruseridstr,@"BuyerUserId", nil];
    }
    
    [registrationRep postwithNSurl:mydic :@"Gethistory" :@"GET" :NO];
    
}
//here using  getdatawithdelay method

#pragma mark Button Actions
-(void)postdatawithdelay :(NSString *)messagestring
{
    
    NSMutableDictionary *postdictionary=[[NSMutableDictionary alloc]init];
    if (![myappDelegate isnullorempty:myselectedimage] && [myappDelegate isnullorempty:messagestring])
    {
        
        NSMutableArray *myimgarray=[[NSMutableArray alloc]initWithObjects:myselectedimage, nil];
        
        NSMutableArray *urlsarray=[myappDelegate postimagestoamazone:YES  photosaray:myimgarray :nil];
        
        NSString *finalstr=  [[urlsarray valueForKey:@"description"] componentsJoinedByString:@","];
        NSLog(@"check the image posting url ==%@===",finalstr);
        
        [postdictionary setObject:finalstr forKey:@"Image"];
        [postdictionary setObject:@"" forKey:@"Message"];
    }
    else if ([myappDelegate isnullorempty:myselectedimage] && ![myappDelegate isnullorempty:messagestring])
    {
        [postdictionary setObject:messagestring forKey:@"Message"];
        [postdictionary setObject:@"" forKey:@"Image"];
    }
    else if (![myappDelegate isnullorempty:myselectedimage] && ![myappDelegate isnullorempty:messagestring])
    {
        NSMutableArray *myimgarray=[[NSMutableArray alloc]initWithObjects:myselectedimage, nil];
        
        NSMutableArray *urlsarray=[myappDelegate postimagestoamazone:YES  photosaray:myimgarray :nil];
        
        NSString *finalstr=  [[urlsarray valueForKey:@"description"] componentsJoinedByString:@","];
        NSLog(@"check the iamge posting url ==%@===",finalstr);
        
        [postdictionary setObject:finalstr forKey:@"Image"];
        [postdictionary setObject:messagestring forKey:@"Message"];
        
    }
    else
    {
        [AppDelegate alertmethod:appname :@"Unable to post message"];
        return;
    }
      {
        [postdictionary setObject:myappDelegate.useridstring forKey:@"UserId"];
        [postdictionary setObject:myappDelegate.useridstring forKey:@"ConversationUserId"];
        NSLog(@"the messanger userid is %@",selleruseridstr);
        [postdictionary setObject:selleruseridstr forKey:@"MessengerUserId"];
    }
    
    
    //[postdictionary setObject:@"" forKey:@"BuyerUserId"];
    
    // [registrationRep getdata:postdictionary :@"Postchatting" :@"POST"];
    [registrationRep postwithNSurl:postdictionary :@"Postchatting" :@"POST" :YES];
    
    sendtextfield.text=@"";
    camimageview.image=[UIImage imageNamed:@"camerac.png"];
    myselectedimage=nil;
}



//here button is performs navigation bar backButtonAction
-(IBAction)backAction:(id)sender;
{
    
    [self.navigationController popViewControllerAnimated:YES];
    
}
//here button is performs pop the sugession viewcontroller
-(IBAction)homeBtnAction:(id)sender;
{
    NSArray *arr=self.navigationController.viewControllers;
    for (UIViewController *childvc in arr) {
        
        if([childvc isKindOfClass:[SuggestionsViewController class]])
        {
            SuggestionsViewController *subvc=(SuggestionsViewController *)childvc;
            subvc.ismyfeed=NO;
            [self.navigationController popToViewController:subvc animated:YES];
        }
    }
    
    
}
//here button is performs CamerButtonAction
-(IBAction)CameraBtnAction:(id)sender;
{
    
    [sendtextfield resignFirstResponder];
    CGRect frame=self.view.frame;
    frame.origin.y=0;
    self.view.frame=frame;
    [UIView commitAnimations];
    
    if ([AppDelegate imagecompare:[UIImage imageNamed:@"camerac.png"] isEqualTo:camimageview.image])
    {
        UIActionSheet *action = [[UIActionSheet alloc]initWithTitle:@"Choose Photo" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Camera",@"Gallery", nil];
        [action showInView:self.view];
    }
    else{
        UIActionSheet *action = [[UIActionSheet alloc]initWithTitle:@"Choose Photo" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Camera",@"Gallery",@"Delete", nil];
        [action showInView:self.view];
    }
    
    
    
}
//here button is performs SendButtonAction it is charting purpose, chart another person
-(IBAction)SendBtnAction:(id)sender;
{
    
    
    if (sendtextfield.text.length==0 && [AppDelegate imagecompare:[UIImage imageNamed:@"camerac.png"] isEqualTo:camimageview.image])
    {
        
        
    }
    else
    {
        [UIViewController cancelPreviousPerformRequestsWithTarget:self ];
        
        [sendtextfield resignFirstResponder];
        
        
        [myappDelegate startspinner:self.view];
        
        methodStart = [NSDate date];
        
        [self performSelector:@selector(postdatawithdelay:) withObject:sendtextfield.text afterDelay:0.01];
        
    }
    
    
}
//action sheet performs choose gallery or camera
-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    UIImagePickerController *picker = [[UIImagePickerController alloc]init];
    if(buttonIndex==0)
    {
        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
        picker.delegate =self;
        picker.wantsFullScreenLayout = YES;
        picker.allowsEditing = YES;
        [self presentViewController:picker animated:YES completion:nil];
    }
    else if(buttonIndex==1)
    {
        picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        picker.delegate = self;
        picker.wantsFullScreenLayout = YES;
        picker.allowsEditing = YES;
        [self presentViewController:picker animated:YES completion:nil];
    }
    else if (buttonIndex==2)
    {
        camimageview.image=[UIImage imageNamed:@"camerac.png"];
        myselectedimage=nil;
    }
    else
    {
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
//this scrollview using chating purpose
-(void)createScrollView
{
    dispatch_async(dispatch_get_main_queue(), ^{

    for (UIView *selfv in chartscrollview.subviews)
    {
        [selfv removeFromSuperview];
    }
    y=10;
    x=10;
    for (int i=0; i<itemsarray.count; i++)
    {
        
        ChartModelObject *itemobj=[itemsarray objectAtIndex:i];
        
        if ([myappDelegate isnullorempty:itemobj.messageStr])
        {
            itemobj.messageStr=@"";
        }
        
        
        NSAttributedString *attributedText =
        [[NSAttributedString alloc]
         initWithString:itemobj.messageStr attributes:@{NSFontAttributeName:[AppDelegate heddingfont:18] }];
        
        CGRect rect = [attributedText boundingRectWithSize:(CGSize){220, CGFLOAT_MAX} options:NSStringDrawingUsesLineFragmentOrigin    context:nil];
        
        CGSize lblsize = rect.size;
        
        UILabel *displable=[[UILabel alloc]initWithFrame:CGRectMake(7, 0, lblsize.width, lblsize.height)];
        
        
        displable.attributedText=attributedText;
        displable.numberOfLines=lblsize.height/20;
        
        displable.backgroundColor=[UIColor clearColor];
        
        UIImageView *dispimgview=[[UIImageView alloc]initWithFrame:CGRectMake(7, 7, 220, 200)];
        
        int width=235;
        int height=displable.frame.size.height;
        int lblyaxis=dispimgview.frame.size.height;
        
        
        
        if (![myappDelegate isnullorempty:itemobj.messageStr] && ![myappDelegate isnullorempty:itemobj.imageurl])
        {
            
            CGRect lblrect=displable.frame;
            
            
            lblrect.origin.y=lblyaxis+8;
            
            
            if (itemobj.isuser)
            {
                lblrect.origin.x=width-(lblrect.size.width+4);
            }
            
            displable.frame=lblrect;
            
            NSURL *imgurl=[NSURL URLWithString:itemobj.imageurl];
            [dispimgview setImageWithURL:imgurl             placeholderImage:[UIImage imageNamed:@"noimage.png"] options:SDWebImageHighPriority];
            height=height+215;
            
            
        }
        else if ([myappDelegate isnullorempty:itemobj.messageStr] && ![myappDelegate isnullorempty:itemobj.imageurl])
        {
            NSLog(@"we have image ----");
            height=height+200;
            // dispimgview.image=[UIImage imageNamed:@"item3.png"];
            NSURL *imgurl=[NSURL URLWithString:itemobj.imageurl];
            [dispimgview setImageWithURL:imgurl             placeholderImage:[UIImage imageNamed:@"noimage.png"] options:SDWebImageHighPriority];
            
        }
        else if (![myappDelegate isnullorempty:itemobj.messageStr] && [myappDelegate isnullorempty:itemobj.imageurl])
        {
            
            if (itemobj.isuser)
            {
                CGRect lblrect=displable.frame;
                
                lblrect.origin.x=4;
                
                displable.frame=lblrect;
            }
            
            width=displable.frame.size.width+8;
            
       
        }
        
        
        UIView *contentview=[[UIView alloc]initWithFrame:CGRectMake(35, 0, width, height)];
        
        
        [self applycorners:6 :contentview ];
        
        
        
        [contentview addSubview:dispimgview];
        
        [contentview addSubview:displable];
        
        
        
        
        UIView *mainbackgroundview=[[UIView alloc]initWithFrame:CGRectMake(x, y, 300, height)];
        
        
        [mainbackgroundview addSubview:contentview];
        mainbackgroundview.backgroundColor=[UIColor clearColor];
        UIImageView *userimageviw;
        
        UIImageView *tailimgv;
        
        displable.font=[AppDelegate heddingfont:18];
        if (itemobj.isuser==NO)
        {
            userimageviw=[[UIImageView alloc]initWithFrame:CGRectMake(0, mainbackgroundview.frame.size.height-30, 30, 30)];
            
            if ([myappDelegate isnullorempty:profilepicstr])
            {
                userimageviw.image=[UIImage imageNamed:@"noimage_friend.png"];
            }
            else{
                NSURL *imgurl33=[NSURL URLWithString:profilepicstr];
                [userimageviw setImageWithURL:imgurl33             placeholderImage:[UIImage imageNamed:@"noimage_friend.png"] options:SDWebImageHighPriority];
            }
            
            tailimgv=[[UIImageView alloc]initWithFrame:CGRectMake(25, mainbackgroundview.frame.size.height-20, 19, 20)];
            
            
            tailimgv.image=[UIImage imageNamed:@"chatleft.png"];
            
            contentview.backgroundColor=[UIColor colorWithRed:237.0/255.0 green:237.0/255.0  blue:237.0/255.0  alpha:1.0];
            
            displable.textColor=[UIColor colorWithRed:12.0/255.0 green:183.0/255.0  blue:170.0/255.0  alpha:1.0];
            
                     timelabel =[[UILabel alloc]initWithFrame:CGRectMake(54, contentview.frame.size.height+contentview.frame.origin.y+1, 31, 20)];

            timelabel.textColor=[UIColor colorWithRed:12.0/255.0 green:183.0/255.0  blue:170.0/255.0  alpha:1.0];
            timelabel.font=[AppDelegate heddingfont:10];
            itemobj.timevalueStr=[NSDate postedTimeAgoWithPostedDateString: itemobj.createdatechartstr];
            timelabel.text=itemobj.timevalueStr;
         if([timelabel.text isEqualToString:@"0s"])
            {
                timelabel.text=@"3s";
            }
            
            timeimg=[[UIImageView alloc]initWithFrame:CGRectMake(42, contentview.frame.size.height+contentview.frame.origin.y+5, 10, 10)];
            timeimg.image=[UIImage imageNamed:@"timer.png"];
            
            //  timeimg.backgroundColor=[UIColor blackColor];
        }
        else
        {
            userimageviw=[[UIImageView alloc]initWithFrame:CGRectMake(mainbackgroundview.frame.size.width-30, mainbackgroundview.frame.size.height-30, 30, 30)];
            
            NSURL *imgurl33=[NSURL URLWithString:loginuserpick];
            [userimageviw setImageWithURL:imgurl33             placeholderImage:[UIImage imageNamed:@"noimage_friend.png"] options:SDWebImageHighPriority];
            tailimgv=[[UIImageView alloc]initWithFrame:CGRectMake(mainbackgroundview.frame.size.width-44, mainbackgroundview.frame.size.height-20, 19, 20)];
            tailimgv.image=[UIImage imageNamed:@"chatright.png"];
            
            contentview.backgroundColor=  [UIColor colorWithRed:12.0/255.0 green:183.0/255.0  blue:170.0/255.0  alpha:1.0];
            
            
            int xval=(mainbackgroundview.frame.size.width-contentview.frame.size.width)-35;
            
            CGRect contentfrm=contentview.frame;
            contentfrm.origin.x=xval;
            contentview.frame=contentfrm;
            
            displable.textColor=[UIColor whiteColor];

                       timelabel =[[UILabel alloc]initWithFrame:CGRectMake(250, contentview.frame.size.height+contentview.frame.origin.y+1, 31, 20)];
          
            timelabel.textColor=[UIColor colorWithRed:12.0/255.0 green:183.0/255.0  blue:170.0/255.0  alpha:1.0];
            methodFinish = [NSDate date];
            NSTimeInterval executionTime= [methodFinish timeIntervalSinceDate:methodStart];
           // NSLog(@"executionTime = %f", executionTime);
            timelabel.font=[AppDelegate heddingfont:10];
            itemobj.timevalueStr=[NSDate postedTimeAgoWithPostedDateString: itemobj.createdatechartstr];
            timelabel.text=itemobj.timevalueStr;
      
            if([timelabel.text isEqualToString:@"0s"])
            {
                timelabel.text=@"3s";
            }
            
            timeimg=[[UIImageView alloc]initWithFrame:CGRectMake(238, contentview.frame.size.height+contentview.frame.origin.y+5, 10, 10)];
            timeimg.image=[UIImage imageNamed:@"timer.png"];
            //  timeimg.backgroundColor=[UIColor blackColor];

            
        }
        [mainbackgroundview addSubview:tailimgv];
        [self applycorners:(userimageviw.frame.size.width/2) :userimageviw ];
        [mainbackgroundview addSubview:userimageviw];
        [mainbackgroundview bringSubviewToFront:contentview];
        
   //     [chartscrollview addSubview:mainbackgroundview];
        //modified by ajay
        
          [mainbackgroundview addSubview:timeimg];
         [mainbackgroundview addSubview:timelabel];
        [mainbackgroundview bringSubviewToFront:timeimg];
        [mainbackgroundview bringSubviewToFront:timelabel];
        
         [chartscrollview addSubview:mainbackgroundview];
        
        
        
        ///end
        y=y+mainbackgroundview.frame.size.height+30;//20
        

        
    }
    
    [chartscrollview setContentSize:CGSizeMake(320, y+20)];
    //chartscrollview scrollRectToVisible:cgrec animated:<#(BOOL)#>
    if (y>chartscrollview.frame.size.height)
    {
        y=y-100;
    }
    
    [chartscrollview scrollRectToVisible:CGRectMake(0, 0, chartscrollview.contentSize.width, chartscrollview.contentSize.height) animated:NO];
    });
}



-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingImage:(UIImage *)image editingInfo:(NSDictionary *)editingInfo
{
    
    myselectedimage= image;
    camimageview.image=image;
    
    [picker dismissModalViewControllerAnimated:YES];
}

#pragma mark textfielddelegatemethods
-(void)textFieldDidBeginEditing:(UITextField *)textField

{
    if (screenSize.size.height == 568)
    {
        
        if (textField==sendtextfield)
        {
           
            CGRect camviewfrm=cameraview.frame;
            
            
            camviewfrm.origin.y=self.view.frame.size.height-256;
            NSLog(@"%f",camviewfrm.origin.y);
            cameraview.frame=camviewfrm;
            
            CGRect camviewfrmtemp=chartscrollview.frame;
            
            camviewfrmtemp.size.height=200;
            chartscrollview.frame=camviewfrmtemp;
            
            CGPoint bottomOffset = CGPointMake(0, self.chartscrollview.contentSize.height - self.chartscrollview.bounds.size.height);
            [self.chartscrollview setContentOffset:bottomOffset animated:YES];
            
        }
    }
    else
    {
        if (textField==sendtextfield)
        {
            CGRect camviewfrm=cameraview.frame;
            
            camviewfrm.origin.y=self.view.frame.size.height-256;
            NSLog(@"%f",camviewfrm.origin.y);
            cameraview.frame=camviewfrm;
            
            
            CGRect camviewfrmtemp=chartscrollview.frame;
            
            camviewfrmtemp.size.height=148;
            chartscrollview.frame=camviewfrmtemp;
            
            CGPoint bottomOffset = CGPointMake(0, self.chartscrollview.contentSize.height - self.chartscrollview.bounds.size.height);
            [self.chartscrollview setContentOffset:bottomOffset animated:YES];
        }
        
    }
    
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    if (screenSize.size.height == 568)
    {
        
        
        if (textField==sendtextfield)
        {
            CGRect camviewfrm=cameraview.frame;
            
            camviewfrm.origin.y=self.view.frame.size.height-45;
            NSLog(@"%f",camviewfrm.origin.y);
            cameraview.frame=camviewfrm;
            cameraview.frame=camviewfrm;
            
            CGRect camviewfrmtemp=chartscrollview.frame;
            NSLog(@"%f",self.view.frame.origin.y);
            camviewfrmtemp.size.height=410;
            chartscrollview.frame=camviewfrmtemp;
        }
    }
    else
    {
        
        if (textField==sendtextfield)
        {
            CGRect camviewfrm=cameraview.frame;
            
            camviewfrm.origin.y=self.view.frame.size.height-45;
            
            cameraview.frame=camviewfrm;
            
            CGRect camviewfrmtemp=chartscrollview.frame;
            NSLog(@"%f",self.view.frame.origin.y);
            camviewfrmtemp.size.height=322;
            chartscrollview.frame=camviewfrmtemp;
        }
        
        
    }
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    
    [textField resignFirstResponder];
    return YES;
}
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    if([text isEqualToString:@"\n"])
    {
        [textView resignFirstResponder];
        return NO;
    }
    return YES;
}
#pragma mark searchbarDelegates

-(void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    [chartsearchbar setShowsCancelButton:NO animated:YES];
    chartsearchbar.text=@"";
    [chartsearchbar resignFirstResponder];
    
    [itemsarray removeAllObjects];
    
    [itemsarray addObjectsFromArray:[duplicatearray mutableCopy]];
    [self createScrollView];
    
}


-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    // [searchBar resignFirstResponder];
    
    if (searchBar.text.length>2)
    {
        
        [itemsarray removeAllObjects];
        
        for (int i=0; i<[duplicatearray count]; i++)
        {
            ChartModelObject *itemobj=[duplicatearray objectAtIndex:i];
            NSLog(@"duplicatearray ....%@",duplicatearray );
            NSLog(@"messstr......%@",itemobj.messageStr);
            
            NSString *caseinsesearch=[searchBar.text lowercaseString];
            
            NSString *namecase=[itemobj.messageStr lowercaseString];
            if([namecase hasPrefix:caseinsesearch]  )
                
            {
                
                NSLog(@"duplicatearray....%@",duplicatearray);
                [itemsarray addObject:itemobj];
                NSLog(@"itemsarray.....%@",itemsarray);
                
            }
            
        }
        [self createScrollView];
    }
    else
    {
        [AppDelegate alertmethod:appname :@"Please enter at leaset 3 characters to search" ];
    }
    
    
    
}
-(void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar
{
    [duplicatearray removeAllObjects];
    [duplicatearray addObjectsFromArray:[itemsarray mutableCopy]];
    [itemsarray removeAllObjects];
    
    [searchBar setShowsCancelButton:YES animated:YES];
}


-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    
}


-(void)successresponceListing :(id)respdict :(NSString *)paramname;
{
    
    if ([paramname isEqualToString:@"Gethistory"])
    {
        
        if ([respdict isKindOfClass:[NSArray class]])
        {
            [AppDelegate alertmethod:appname :@"Some error."];
            [self.navigationController popViewControllerAnimated:YES];
            return;
        }
        
        NSDictionary *tempdict=(NSDictionary *)respdict;
        
        
        NSArray *temparray=[tempdict objectForKey:@"listnotifications"];
        
        if ([temparray isKindOfClass:[NSArray class]])
        {
            NSArray *resparray=(NSArray *)temparray;
            
            [itemsarray removeAllObjects];
            for (int i=0; i<resparray.count; i++)
            {
                
                NSDictionary *maindict=resparray[i];
                
                
                
                ChartModelObject *chartmodobj=[[ChartModelObject alloc]init];
                
                
                NSString *postid=[maindict valueForKey:@"conversationUserId"];
                
                if ([[NSString stringWithFormat:@"%@",myappDelegate.useridstring] isEqualToString:[NSString stringWithFormat:@"%@",postid]])
                    
                {
                    chartmodobj.isuser=YES;
                  //  timelabel.text=[NSDate postedTimeAgoWithPostedDateString: chartmodobj.createdatechartstr];

                }
                else
                {
                    chartmodobj.isuser=NO;
                   // timelabel.text=[NSDate postedTimeAgoWithPostedDateString: chartmodobj.createdatechartstr];

                }
                /////////////////////
                
              /*
                cell.timelbl.text=[NSDate postedTimeAgoWithPostedDateString:feedsObj.activitydateString];
                
                
                
                int length=cell.timelbl.text.length;
                NSLog(@"the length is %d",length);
                if(length==2)
                {
                    CGRect timerframe=cell.timerimg.frame;
                    timerframe.origin.x=286;
                    cell.timerimg.frame=timerframe;
                    
                }
                else if (length==3)
                {
                    CGRect timerframe=cell.timerimg.frame;
                    timerframe.origin.x=279;
                    cell.timerimg.frame=timerframe;
                }*/
                
                
                ////////////////////
                
                NSString *messagestr=[maindict valueForKey:@"message"];
                NSString *imageurlstr=[maindict valueForKey:@"image"];
               
                chartmodobj.createdatechartstr=[maindict valueForKey:@"createdDate"];
                NSLog(@"rhe created date  is %@",[maindict valueForKey:@"createdDate"]);
               

                if (![myappDelegate isnullorempty:messagestr] && [myappDelegate isnullorempty:imageurlstr])
                {
                    chartmodobj.istext=YES;
                    chartmodobj.messageStr=messagestr;
                }
                else if ([myappDelegate isnullorempty:messagestr] && ![myappDelegate isnullorempty:imageurlstr])
                {
                    chartmodobj.istext=NO;
                    chartmodobj.imageurl=imageurlstr;
                    
                }
                else if (![myappDelegate isnullorempty:messagestr] && ![myappDelegate isnullorempty:imageurlstr])
                {
                    chartmodobj.messageStr=messagestr;
                    chartmodobj.imageurl=imageurlstr;
                }
                [itemsarray addObject:chartmodobj];
            }
        }
      
        [self createScrollView];
        [myappDelegate stopspinner:self.view];
        
        
    }
    else if ([paramname isEqualToString:@"Postchatting"])
    {
       
        
        if ([respdict isKindOfClass:[NSDictionary class]])
        {
            
            NSDictionary *mydict=(NSDictionary *)respdict;
            
            if ( [[mydict valueForKey:@"status"] isEqualToString:@"Notification Added Successfully"])
            {
                ChartModelObject *chartmodobj=[[ChartModelObject alloc]init];
                
                
                {
                    chartmodobj.isuser=YES;
                   //  timelabel.text=[NSDate postedTimeAgoWithPostedDateString: chartmodobj.createdatechartstr];
                }
                
                {
                    
                }
                
                NSString *messagestr=[mydict valueForKey:@"message"];
                NSString *imageurlstr=[mydict valueForKey:@"image"];
                
                if (![myappDelegate isnullorempty:messagestr] && [myappDelegate isnullorempty:imageurlstr])
                {
                    chartmodobj.istext=YES;
                    chartmodobj.messageStr=messagestr;
                }
                else if ([myappDelegate isnullorempty:messagestr] && ![myappDelegate isnullorempty:imageurlstr])
                {
                    chartmodobj.istext=NO;
                    chartmodobj.imageurl=imageurlstr;
                    
                }
                else if (![myappDelegate isnullorempty:messagestr] && ![myappDelegate isnullorempty:imageurlstr])
                {
                    chartmodobj.messageStr=messagestr;
                    chartmodobj.imageurl=imageurlstr;
                }
                
                
                [itemsarray addObject:chartmodobj];
                
            }
            else
            {
                
            }
         
            [self createScrollView];
        }
        
        [myappDelegate stopspinner:self.view];
    }
    
    [UIViewController cancelPreviousPerformRequestsWithTarget:self ];
    [self performSelector:@selector(getDataWithDelay) withObject:nil afterDelay:10];
    
}
//*****this method using error messages******
-(void)errorMessage:(NSString *)message
{
    [myappDelegate stopspinner:self.view];
    [AppDelegate alertmethod:appname :message];
    
}

-(void)applycorners:(int)cornervalue :(UIView *)myview
{
    if (cornervalue==0)
    {
        cornervalue=25;
    }
    CALayer *imageLayer = myview.layer;
    [imageLayer setCornerRadius:cornervalue];
    
    [imageLayer setMasksToBounds:YES];
}
-(void)internetConnection
{
    UIAlertView *Internetalert=[[UIAlertView alloc]initWithTitle:appname message:@"Internet connection not available" delegate:self cancelButtonTitle:nil otherButtonTitles:@"Ok", nil];
    [Internetalert show];
    [APPDELEGATE stopspinner:self.view];
    
}
/*
-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    for (int i=0; i<itemsarray.count; i++)
    {
        ChartModelObject *itemobj=[itemsarray objectAtIndex:i];
        itemobj.timevalueStr=[NSDate postedTimeAgoWithPostedDateString: itemobj.createdatechartstr];
        if([itemobj.timevalueStr isEqualToString:@"0s"])
        {
            itemobj.timevalueStr=@"1s";
        }
        
    }
   [self createScrollView];
}
  */
@end
