//
//  FollowersTableViewCell.h
//  MarkidPlace
//
//  Created by stellent on 03/06/14.
//  Copyright (c) 2014 stellentmac1. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FollowersTableViewCell : UITableViewCell
@property(nonatomic,strong)IBOutlet UIImageView *imgview_follower;
@property(nonatomic,strong)IBOutlet UILabel *lbl_follower;
@property(nonatomic,strong)IBOutlet UIButton *button_follower;


@end
