//
//  ListingDetailViewController.h
//  MarkidPlace
//
//  Created by stellentmac1 on 5/21/14.
//  Copyright (c) 2014 stellentmac1. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ListPageControl.h"
#import "AppDelegate.h"
#import <AddressBook/AddressBook.h>
#import <AddressBookUI/AddressBookUI.h>
#import <MessageUI/MessageUI.h>
#import "FollowViewController.h"
#import "ItemsModelobject.h"
#import "RegistrationRepository.h"
#import "HelperProtocol.h"
#import <Pinterest/Pinterest.h>
#import "MessageViewController.h"
#import "ListingDetailsCustomcell.h"
#import "ProfileRepository.h"
#import <Social/Social.h>
#import <Accounts/Accounts.h>
#import "ASIFormDataRequest.h"

@class AppDelegate;
@class RegistrationRepository;
@class ProfileRepository;

@interface ListingDetailViewController : UIViewController<UIScrollViewDelegate,MFMailComposeViewControllerDelegate,MFMessageComposeViewControllerDelegate,HelperProtocol,UIGestureRecognizerDelegate,UIActionSheetDelegate,UIAlertViewDelegate>
{
     AppDelegate *appdelegate;
    CGRect screenSize;
    BOOL pageControlBeingUsed;
    int oldpage;
  ProfileRepository *profilerepo;
    NSString *markitemidstr;
     RegistrationRepository *registrationRep;
     Pinterest*  _pinterest;
    float zoomintval;
    UIActionSheet *action;
     NSString *acessToken;
    ASIFormDataRequest  *asiRequest;
    
    NSString *imgurlstr;
    NSString *descrpstr;
    NSString *namestr;
}

@property(nonatomic,strong) NSString *nameString;
@property(nonatomic,strong) NSString *usernameString;
@property(nonatomic,strong) NSString *imageString;




@property(nonatomic,strong) ACAccount *fbaccount;
@property (nonatomic,strong) ACAccountStore *fbaccountStore;
@property (nonatomic,strong) NSString *facebookID;
@property (nonatomic,readwrite) BOOL iscompare;

@property (nonatomic,strong) NSString *condstring;
@property (nonatomic,strong) NSString *notifystr;
@property (nonatomic,strong) NSString *exchangestring;
@property(nonatomic,strong) ItemsModelobject *sltobj;
@property(nonatomic,strong)NSMutableArray *listingmainarray;
@property(nonatomic,strong)NSMutableArray *editinglistingarray;

@property (nonatomic,readwrite) int senderIndex;
@property (nonatomic,strong) IBOutlet UINavigationBar *navbar;
@property (nonatomic,strong) IBOutlet UIScrollView *listingscrollview;
@property (nonatomic,strong) IBOutlet UIImageView *itemImageView;

@property (nonatomic,strong) IBOutlet UILabel *listingDetailsLabel;
@property (nonatomic,strong) IBOutlet UILabel *itemTitleLabel;
@property (nonatomic,strong) IBOutlet UILabel *itemPriceLabel;
@property (nonatomic,strong) IBOutlet UILabel *oldPriceLabel;
@property (nonatomic,strong) IBOutlet UILabel *shippingLabel;
@property (nonatomic,strong) IBOutlet UILabel *shippingValueLabel;
@property (nonatomic,strong) IBOutlet UILabel *categoryLabel;
@property (nonatomic,strong) IBOutlet UILabel *categoryNameLabel;
@property (nonatomic,strong) IBOutlet UILabel *sizeLabel;
@property (nonatomic,strong) IBOutlet UILabel *sizeNameLabel;
@property (nonatomic,strong) IBOutlet UILabel *brandLabel;
@property (nonatomic,strong) IBOutlet UILabel *brandNameLabel;
@property (nonatomic,strong) IBOutlet UILabel *conditionLabel;
@property (nonatomic,strong) IBOutlet UILabel *conditionNameLabel;
@property (nonatomic,strong) IBOutlet UILabel *descrptionLabel;
@property (nonatomic,strong) IBOutlet UILabel *descrptionNameLabel;
@property (nonatomic,strong) IBOutlet UIImageView *detailsImageView;
@property (nonatomic,strong) IBOutlet UIView *fbSubView;
@property (nonatomic,strong) IBOutlet UIImageView *fbImgView;
@property (nonatomic,strong) IBOutlet UITextView *fbtextView;
@property (nonatomic,strong) IBOutlet UIImageView *shareimgview;



@property (nonatomic,strong) IBOutlet UIImageView *nextarrowimg;
@property (nonatomic,strong) IBOutlet UIImageView *prevarrowimg;


@property (nonatomic,strong) IBOutlet UIImageView *nextpoparrowimg;
@property (nonatomic,strong) IBOutlet UIImageView *prevpoparrowimg;

@property (nonatomic,strong) IBOutlet UILabel *sellernamelabl;
@property (nonatomic,strong) IBOutlet UILabel *usernamelbl;

@property (nonatomic,strong) IBOutlet UIImageView *likeimgv;

@property (nonatomic,strong) IBOutlet UIButton *userbtn;
@property (nonatomic,strong) IBOutlet UIButton *exportbutton;

@property (nonatomic,strong) IBOutlet UIButton *editbutton;
@property (nonatomic,strong) IBOutlet UIButton *likebtn;

@property (nonatomic, retain) IBOutlet UIScrollView *pagescrollView;
@property (nonatomic, retain) IBOutlet ListPageControl *pageControl;

@property(nonatomic,strong)NSMutableArray *firstaaray;
@property(nonatomic,strong)NSMutableArray *secondarray;
@property (nonatomic, retain) IBOutlet ListingDetailsCustomcell *listingdetails;
@property (nonatomic, retain) IBOutlet UITableView *listingdetailstblview;
@property (nonatomic, retain) IBOutlet  UIView *nextview;

@property (nonatomic, retain) IBOutlet  UIView *editview;

-(IBAction)backAction:(id)sender;
-(IBAction)addCartAction:(id)sender;
-(IBAction)pageControllerAction;
-(IBAction)detailsOnAction:(id)sender;
-(IBAction)detailsOffAction:(id)sender;

-(IBAction)exportbtnaction:(id)sender;

-(IBAction)editbuttonaction:(id)sender;

-(IBAction)likebuttonaction:(id)sender;


-(IBAction)nextandprevbuttonaction:(id)sender;
//@property(nonatomic,strong) UIActionSheet *actionSheetref;

@property (nonatomic, retain) IBOutlet  UIView *addtocartview;
@property (nonatomic, retain) IBOutlet  UIView *detailview;
@property(nonatomic,strong) ACAccount *account;
@property (nonatomic,strong) ACAccountStore *accountStore;




@property(nonatomic,strong) IBOutlet UIView  *shippingview;

@property(nonatomic,strong) IBOutlet UILabel *shippingdatalbl;
@property(nonatomic,strong) IBOutlet UILabel *shippingnamelbl;


#pragma mark zoom functionality

-(IBAction)zoombuttonaction:(id)sender;
-(IBAction)zoomInbuttonaction:(id)sender;
-(IBAction)zoomoutbuttonaction:(id)sender;
-(IBAction)cancelbtnaction:(id)sender;

-(IBAction)popoverbtnaction:(id)sender;
@property (nonatomic, retain) IBOutlet UIScrollView *zoomscrollView;
@property (nonatomic, retain) IBOutlet  UIView *popview;                                                                                      @property (nonatomic,strong) UIImageView *zoomimgview;
@property (nonatomic,strong)UIImage*zoomimage;
@property (nonatomic,strong)IBOutlet UIView *zoomNextPrevView;

//Krish
@property(nonatomic,strong)IBOutlet UIImageView *addtoCartImgView;
@property(nonatomic,strong)IBOutlet UIButton *addtoCartButton;

@end
