 //
//  SuggestionsViewController.m
//  MarkidPlace
//
//  Created by stellentmac1 on 5/21/14.
//  Copyright (c) 2014 stellentmac1. All rights reserved.
//

#import "SuggestionsViewController.h"
#import "ProfileViewController.h"

@interface SuggestionsViewController ()

@end

@implementation SuggestionsViewController
{
    NSString *currentPositon;
    NSString *totalrecords;
    NSString *totalrecordsfromDB;
    
    NSString *totalrecords_myfeed;
    NSString *totalrecordsfromDB_myfeed;
    
    BOOL isLoadingMoreRecords;
    CGPoint currentscrollPosition;
}
@synthesize navbar,itemLabel1,itemLabel2,itempriceLabel1,itempriceLabel2,oldpriceLabel1,oldpriceLabel2,myAccountLabel,myClosetLabel,myfeedLabel,mywishlistLabel,shopallLabel,sellLabel,cartcountLabel,delegate;
@synthesize menuBtn,menuview;
@synthesize itemscrollview,myfeedImageview,mySuggestionsImageview,filterImageview,notificationcountlabel,notificationview;


@synthesize cartview,loaddatabool,loadfeeddatabool,rateRef,animationimgv,menubtnview,ismyfeed,isItFirstTime;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
#pragma mark View methods
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    currentPositon=@"";
    totalrecords=@"0";
    totalrecords_myfeed=@"0";
    totalrecordsfromDB=@"0";
    self.notificationcountlabel.font=[UIFont fontWithName:@"Muli" size:9];
    [myappDelegate.suggestonsGlobalarray removeAllObjects];
    [myappDelegate.myfeedsGlobalarray removeAllObjects];
    currentscrollPosition=CGPointMake(0,0);
    [myappDelegate.allLikesGlobalarray removeAllObjects];
    [myappDelegate.mylikesGlobalarray removeAllObjects];
    
    ismyfeed=NO;
    
    self.navbar.frame = CGRectMake(0, 0, 320, 64);
    [self.navbar setBackgroundImage:[UIImage imageNamed:@"navbar.png"] forBarMetrics:UIBarMetricsDefault];
    
    itemsarray=[NSMutableArray new];
    
    self.itemLabel1.font = [UIFont fontWithName:@"Muli" size:13];
    self.itemLabel2.font = [UIFont fontWithName:@"Muli" size:13];
    self.itempriceLabel1.font = [UIFont fontWithName:@"Muli" size:13];
    self.itempriceLabel1.font = [UIFont fontWithName:@"Muli" size:13];
    self.itempriceLabel2.font = [UIFont fontWithName:@"Muli" size:13];
    self.oldpriceLabel1.font = [UIFont fontWithName:@"Muli" size:13];
    self.oldpriceLabel2.font = [UIFont fontWithName:@"Muli" size:13];
    self.myAccountLabel.font = [UIFont fontWithName:@"Muli" size:15];
    self.myClosetLabel.font = [UIFont fontWithName:@"Muli" size:15];
    self.myfeedLabel.font = [UIFont fontWithName:@"Muli" size:15];
    self.mywishlistLabel.font = [UIFont fontWithName:@"Muli" size:15];
    self.shopallLabel.font = [UIFont fontWithName:@"Muli" size:15];
    self.sellLabel.font = [UIFont fontWithName:@"Muli" size:15];
    self.cartcountLabel.font = [UIFont fontWithName:@"Muli" size:11];
    
    
    refreshControl = [[ODRefreshControl alloc] initInScrollView:self.itemscrollview];

    refreshControl.tintColor=[UIColor colorWithRed:12.0/255.0 green:183.0/255.0  blue:170.0/255.0  alpha:1.0];
    
    //[UIColor colorWithRed:((float)57/255.0f)   green:((float)59/255.0f)  blue:((float)120/255.0f)  alpha:1.0];
    [refreshControl addTarget:self action:@selector(dropViewDidBeginRefreshing:) forControlEvents:UIControlEventValueChanged];
    refreshControl.activityIndicatorViewColor=[UIColor colorWithRed:12.0/255.0 green:183.0/255.0  blue:170.0/255.0  alpha:1.0];
  

    if (myappDelegate.isiphone5)
    {
        menubtnview.frame=CGRectMake(131,470, 58, 59);
    }
    else
    {
        menubtnview.frame=CGRectMake(131,355, 58, 59);
    }
    
    [self.view addSubview:menuview];
    [self.view addSubview:menubtnview];
    
    [myappDelegate horseanimation:animationimgv];
    

    itemscrollview.contentSize=CGSizeMake(itemscrollview.frame.size.width,itemscrollview.frame.size.height+100);
    
   myappDelegate.useridstring=[myappDelegate getStringFromPlist:@"userid"];

     prevlikedarray =[[NSMutableArray alloc]init];
  }
-(void)viewDidDisappear:(BOOL)animated
{/*
    for (ASIHTTPRequest *req in ASIHTTPRequest.sharedQueue.operations)
    {
        [req cancel];
        [req setDelegate:nil];
    }
  */
//    [queue setDelegate:nil];
}
-(void)viewWillDisappear:(BOOL)animated
{
    for (ASIHTTPRequest *req in ASIHTTPRequest.sharedQueue.operations)
    {
       // [req cancel];
       // [req setDelegate:nil];
    }
      itemscrollview.hidden=YES;
   // [queue setDelegate:nil];
  
}
-(void)viewWillAppear:(BOOL)animated
{
  //  dispatch_async( dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
    
     self.notificationcountlabel.font=[UIFont fontWithName:@"Muli" size:11];
    notificationcountlabel.text=[APPDELEGATE getStringFromPlist:@"notificationcount"];
    NSLog(@"notificationcountlabel.text....%@",notificationcountlabel.text);
    NSLog(@"cartGlobalarray...%@",myappDelegate.cartGlobalarray);
    if (myappDelegate.cartGlobalarray.count>0)
    {
        cartcountLabel.text=[NSString stringWithFormat:@"%lu",(unsigned long)APPDELEGATE.cartGlobalarray.count];
        NSLog(@"cartcountLabel.text....%@",cartcountLabel.text);
        cartview.hidden=NO;
    }
    else
    {
        cartview.hidden=YES;
    }
    
    
    registrationRep=[[RegistrationRepository alloc ]init];
    
    registrationRep.delegate=self;
    
    
    if (isItFirstTime)
    {
       //edited by srinadh, no needto call lookup data every time, call on becomeactive in appdelegate  [self performSelector:@selector(getdropdowndata) withObject:nil afterDelay:0.001];
        //Added by srinadh
        dispatch_async( dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
            
            dispatch_async( dispatch_get_main_queue(), ^{
                
                
                  [self mySuggestionsButtonTapped:nil] ;
            });
        });
      
    
        dispatch_async( dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
            
            //temp [registrationRep getdata:nil :@"Myfeeds" :@"GET" withcount:@"0"];
            dispatch_async( dispatch_get_main_queue(), ^{
                // Add code here to update the UI/send notifications based on the
                // results of the background processing
                
            });
        });
       

    }
    else
    {
        if (ismyfeed)
        {
             [self myFeedButtonTapped:nil] ;
        }
        else{
            [self mySuggestionsButtonTapped:nil] ;
        }
    }
    //});
    //  [ self myFeedButtonTapped:nil];
    
    
    [self iRateMethod];
            
}
- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (void)dropViewDidBeginRefreshing:(ODRefreshControl *)refresh
{
    [refreshControl beginRefreshing ];
     [self setmenudown];
    if (ismyfeed)
    {
        [myappDelegate.myfeedsGlobalarray removeAllObjects];
        
        [self myFeedButtonTapped:nil];
    }
    else
    {
        [myappDelegate.suggestonsGlobalarray removeAllObjects];
        totalrecords=@"0";
        [self mySuggestionsButtonTapped:nil];
    }
}


-(void)getdropdowndata
{
    registrationRep=[[RegistrationRepository alloc ]init];
    
    registrationRep.delegate=self;
    
    if (myappDelegate.catogoryGlobalarray.count<1 || myappDelegate.conditionGlobalarray.count<1 || myappDelegate.sizeGlobalarray.count<1 || myappDelegate.brandGlobalarray.count<1)
    {
        //[myappDelegate startspinner:self.view];
       // [ registrationRep getdata:nil :@"getcatogery" :@"GET" withcount:@"0"];
    }

}

-(void)iRateMethod
{
    
    rateRef=[[iRate alloc]init];
    rateRef.delegate=self;
    rateRef.promptAtLaunch = YES;
    [iRate sharedInstance].onlyPromptIfLatestVersion = NO;
    [iRate sharedInstance].previewMode = YES;
    [iRate sharedInstance].daysUntilPrompt = 1;
    
   }

#pragma mark Button Action
-(IBAction)backAction:(id)sender
{
     [self setmenudown];
    [self.navigationController popViewControllerAnimated:YES];
}

// This Action performs push to the Filter ViewController.
-(IBAction)filterButtonTapped:(id)sender
{
    [self setmenudown];
    
    if ([sender tag]==330)
    {
        if (myappDelegate.cartGlobalarray.count>0)
        {
            // ff
            
            CartViewController *cartVC;
            if ([ myappDelegate isiphone5])
            {
                cartVC = [[CartViewController alloc]initWithNibName:@"CartViewController" bundle:nil];
            }
            else
            {
                cartVC = [[CartViewController alloc]initWithNibName:@"CartViewController_iphone4" bundle:nil];
            }
            
            ItemsModelobject *sltobj=[myappDelegate.cartGlobalarray objectAtIndex:0];
            cartVC.itemsModelObj=sltobj;
            
            
            [self.navigationController pushViewController:cartVC animated:YES];
            
        }
        else{
            
            [AppDelegate alertmethod:appname :@"Your cart is empty. Please add listing to cart."];
        }
        
    }
    else{
        if (myappDelegate.isiphone5)
        {
            FilterViewController *filterVC=[[FilterViewController alloc] initWithNibName:@"FilterViewController" bundle:nil];
            [self.navigationController pushViewController:filterVC animated:YES];
        }
        else{
            FilterViewController *filterVC=[[FilterViewController alloc] initWithNibName:@"FilterViewController_iphone4" bundle:nil];
            [self.navigationController pushViewController:filterVC animated:YES];
        }
    }
    
}

// This Action performs push to the Feed ViewController.
-(IBAction)myFeedButtonTapped:(id)sender
{
    ismyfeed=YES;
    isLoadingMoreRecords=NO;
    [self setmenudown];
    itemscrollview.hidden=YES;
    myfeedImageview.image=[UIImage imageNamed:@"myfeedbtn_on.png"];
    mySuggestionsImageview.image=[UIImage imageNamed:@"mysuggestionbtn@2x"];
    filterImageview.image=[UIImage imageNamed:@"filterbtn_off@2x.png"];
    
    menuBtn.hidden=NO;
    
    menuview.hidden=NO;
    currentPositon=@"myfeed";
    if (myappDelegate.myfeedsGlobalarray.count<1 || loadfeeddatabool==YES)
    {
        //loaddatabool=NO;
        [myappDelegate startspinner:self.view ];
        
       // [registrationRep getdata:nil :@"getallLikes" :@"GET" withcount:@"0"];
        
        [registrationRep getdata:nil :@"Myfeeds" :@"GET" withcount:@"0"];

    }
    else
    {
        [self createscrollview];
        
       // [myappDelegate.myfeedsGlobalarray removeAllObjects];
        
        
        
       // [myappDelegate startspinner:self.view ];
        
      //  [registrationRep getdata:nil :@"Myfeeds" :@"GET" withcount:@"0"];

        
    }
    
}

// This Action performs push to the Suggestion ViewController.
-(IBAction)mySuggestionsButtonTapped:(id)sender
{
    dispatch_async( dispatch_get_main_queue(), ^{
    [self setmenudown];
        isLoadingMoreRecords=NO;
     ismyfeed=NO;
    
    myfeedImageview.image=[UIImage imageNamed:@"myfeedbtn_off.png"];
    mySuggestionsImageview.image=[UIImage imageNamed:@"mysuggestions@2x"];
    filterImageview.image=[UIImage imageNamed:@"filterbtn_off@2x.png"];
    menuBtn.hidden=NO;
    menuview.hidden=NO;
    itemscrollview.hidden=YES;
    currentPositon=@"suggestions";
    if (APPDELEGATE.suggestonsGlobalarray.count<1 || loaddatabool==YES)
    {
        if (loaddatabool==YES)
        {
            loadfeeddatabool=YES;
        }
        loaddatabool=NO;
        
        //edited by srinadh
        
        [APPDELEGATE startspinner:self.view];
       // [registrationRep getdata:nil :@"getallLikes" :@"GET" withcount:@"0"];
        
        [registrationRep getdata:nil :@"getListing" :@"GET" withcount:@"0"];
        
        
    }
    else
    {
       
        [self createscrollview];
        
        
    }
       });
}

// This Action performs the Animation of MenuView.

-(IBAction)menuBtnAction:(id)sender
{
    //notificationcountlabel.text=[APPDELEGATE getStringFromPlist:@"notificationcount"];
    //NSLog(@"notificationcountlabel.text....%@",notificationcountlabel.text);
   // notificationcountlabel.text=@"99";.
    int value=[[APPDELEGATE getStringFromPlist:@"notificationcount"] intValue];
    
    NSLog(@"the count is %d",value);
    
    if (value>9)
    {
        notificationcountlabel.text=@"9+";
        notificationview.hidden=NO;
    }
    else if (value==0)
    {
        notificationview.hidden=YES;
    }
    else
    {
        notificationcountlabel.text=[APPDELEGATE getStringFromPlist:@"notificationcount"];
        notificationview.hidden=NO;
    }

   
    [animationimgv stopAnimating];
    
    //animationimgv.image=[UIImage imageNamed:@"hours_0022_Layer-1.png"];
    
    [animationimgv startAnimating];
    
   
    
    
    
    CGRect menu=menuview.frame;
    CGRect menuButtonframe=menubtnview.frame;
    
    
    
    if (myappDelegate.isiphone5)
    {
        if(ismenu==YES)
        {
            ismenu=NO;
            
            menu.origin.y=600;
            menuButtonframe.origin.y=470;
            
        }
        else
        {
            //menuButtonframe=CGRectMake(131, 250, 58, 59);
            
            ismenu=YES;
            
            //menuButtonframe.origin.y=menubtnview.frame.origin.y-250;
            menuButtonframe.origin.y=250;
            menu.origin.y=280;
            
        }
        
        self.view.userInteractionEnabled=YES;
        
    }
    else
    {
        if(ismenu==YES)
        {
            
            ismenu=NO;
            
            menuButtonframe.origin.y=355;
            
            
            menu.origin.y=492;
            
        }
        else
        {
            menuButtonframe.origin.y= 160;
            
            ismenu=YES;
            menu.origin.y=190;
        }
    }
    
    [UIView animateWithDuration:0.5 animations:^
     {
         
         menuview.frame=menu;
         menubtnview.frame=menuButtonframe;
         
         // [self runSpinAnimationOnView:self.menuBtn duration:1.0 rotations:1.0 repeat:0];
         
     }
                     completion:^(BOOL finished){
                         
                         //[animationimgv stopAnimating];
                         
                         // animationimgv.image=[UIImage imageNamed:@"hours_0022_Layer-1.png"];
                     }];
    
    self.view.userInteractionEnabled=YES;
    
   dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 1.0 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
        [animationimgv stopAnimating];
      //
       // animationimgv.image=[UIImage imageNamed:@"hours_0022_Layer-1.png"];
    });
    
    
}

/*-(IBAction)menuBtnAction:(id)sender
{
          CGRect menu;
        CGRect menuButtonframe;

        if (myappDelegate.isiphone5)
        {
            if(ismenu==YES)
            {
                ismenu=NO;
                menuButtonframe=CGRectMake(131,470, 58, 59);
                menu=menuview.frame;
                menu.origin.y=menuview.frame.origin.y+600;
            }
            else
            {
                menuButtonframe=CGRectMake(131, 250, 58, 59);
                ismenu=YES;
                menu=menuview.frame;
                menu.origin.y=280;
            }
        }
        else
        {
            if(ismenu==YES)
            {

                ismenu=NO;

                menuButtonframe=CGRectMake(131,355, 58, 59);
                menu=menuview.frame;
                menu.origin.y=menuview.frame.origin.y+482;

            }
            else
            {
                menuButtonframe=CGRectMake(131, 160, 58, 59);
                ismenu=YES;
                menu=menuview.frame;
                menu.origin.y=190;
            }
        }

        [UIView animateWithDuration:0.5 animations:^{

           menuview.frame=menu;
            menuBtn.frame=menuButtonframe;
            
        } completion:^(BOOL finished) {
            
        }];
        
}

*/

//Action performs  the button Actions in the menuview(dropdownview).
-(IBAction)menuItemTapped:(id)sender
{
    
    
     [self setmenudown];
    switch ([sender tag]) {
        case 1:
        {
            BOOL isVCExist=NO;
            NSArray *arr=self.navigationController.viewControllers;
            for (UIViewController *childvc in arr) {
                
                if([childvc isKindOfClass:[ProfileViewController class]])
                {
                    isVCExist=YES;
                    UIViewController *subvc=(ProfileViewController *)childvc;
                    [self.navigationController popToViewController:subvc animated:YES];
                }
            }
            
            if(!isVCExist)
            {
                ProfileViewController *profileVC;
                if (myappDelegate.isiphone5
                    )
                {
                    profileVC = [[ProfileViewController alloc]initWithNibName:@"ProfileViewController" bundle:nil];
                }
                else
                {
                    profileVC = [[ProfileViewController alloc]initWithNibName:@"ProfileViewController_iphone4" bundle:nil];
                }
                [self.navigationController pushViewController:profileVC animated:YES];
            }
        }
            break;
        case 2:
        {
            BOOL isVCExist=NO;
            NSArray *arr=self.navigationController.viewControllers;
            for (UIViewController *childvc in arr) {
                
                if([childvc isKindOfClass:[MyClosetViewController class]])
                {
                    isVCExist=YES;
                    UIViewController *subvc=(MyClosetViewController *)childvc;
                    [self.navigationController popToViewController:subvc animated:YES];
                }
            }
            
            if(!isVCExist)
            {
                MyClosetViewController *myclosetVC;
                if (myappDelegate.isiphone5)
                {
                    myclosetVC = [[MyClosetViewController alloc]initWithNibName:@"MyClosetViewController" bundle:nil];
                }
                else
                {
                    myclosetVC = [[MyClosetViewController alloc]initWithNibName:@"MyClosetViewController_iphone4" bundle:nil];
                }
                [self.navigationController pushViewController:myclosetVC animated:YES];
            }
        }
            break;
            
        case 3:
        {
            BOOL isVCExist=NO;
            NSArray *arr=self.navigationController.viewControllers;
            for (UIViewController *childvc in arr) {
                
                if([childvc isKindOfClass:[MyFeedViewController class]])
                {
                    isVCExist=YES;
                    UIViewController *subvc=(MyFeedViewController *)childvc;
                    [self.navigationController popToViewController:subvc animated:YES];
                }
            }
            
            if(!isVCExist)
            {
                
                MyFeedViewController *myfeedVC;
                if (myappDelegate.isiphone5)
                {
                    myfeedVC = [[MyFeedViewController alloc]initWithNibName:@"MyFeedViewController" bundle:nil];
                }
                else
                {
                    myfeedVC = [[MyFeedViewController alloc]initWithNibName:@"MyFeedViewController_iphone4" bundle:nil];
                }
                [self.navigationController pushViewController:myfeedVC animated:YES];
            }
        }
            break;
            
        case 4:
        {
            BOOL isVCExist=NO;
            NSArray *arr=self.navigationController.viewControllers;
            for (UIViewController *childvc in arr) {
                
                if([childvc isKindOfClass:[MyWishlistViewController class]])
                {
                    isVCExist=YES;
                    UIViewController *subvc=(MyWishlistViewController *)childvc;
                    [self.navigationController popToViewController:subvc animated:YES];
                }
            }
            
            if(!isVCExist)
            {
                
                MyWishlistViewController *mywishlistVC;
                if (myappDelegate.isiphone5)
                {
                    mywishlistVC = [[MyWishlistViewController alloc]initWithNibName:@"MyWishlistViewController" bundle:nil];
                }
                else
                {
                    mywishlistVC = [[MyWishlistViewController alloc]initWithNibName:@"MyWishlistViewController_iphone4" bundle:nil];
                }
                [self.navigationController pushViewController:mywishlistVC animated:YES];
            }
        }
            break;
            
            
        case 5:
        {
            [self mySuggestionsButtonTapped:self];
        }
            break;
        case 6:
        {
            BOOL isVCExist=NO;
            NSArray *arr=self.navigationController.viewControllers;
            for (UIViewController *childvc in arr) {
                
                if([childvc isKindOfClass:[NewListingViewController class]])
                {
                    isVCExist=YES;
                    UIViewController *subvc=(NewListingViewController *)childvc;
                    [self.navigationController popToViewController:subvc animated:YES];
                }
            }
            
            if(!isVCExist)
            {
                if (![[APPDELEGATE getStringFromPlist:@"paypalstr"] isEqualToString:@""] && [APPDELEGATE getStringFromPlist:@"paypalstr"].length>3)
                {
                    
                    NewListingViewController *mywishlistVC;
                    if (myappDelegate.isiphone5)
                    {
                        mywishlistVC = [[NewListingViewController alloc]initWithNibName:@"NewListingViewController" bundle:nil];
                    }
                    else
                    {
                        mywishlistVC = [[NewListingViewController alloc]initWithNibName:@"NewListingViewController_iphone4" bundle:nil];
                    }
                    
                    [self.navigationController pushViewController:mywishlistVC animated:YES];
                    
                }
                else
                {
                    [AppDelegate alertmethod:@"Warning" :@"PayPal Email Address is required for Creating A Listing, Please enter your PayPal Email Address in your profile."] ;
                }
                
            }
        }
            break;
            
        default:
            break;
    }
}

-(IBAction)myAccountAction:(id)sender
{
     [self setmenudown];
    ProfileViewController *profileVC;
    if ([myappDelegate isiphone5])
    {
        profileVC = [[ProfileViewController alloc]initWithNibName:@"ProfileViewController" bundle:nil];
    }
    else
    {
        profileVC = [[ProfileViewController alloc]initWithNibName:@"ProfileViewController_iphone4" bundle:nil];
    }
    [self.navigationController pushViewController:profileVC animated:YES];
}
-(IBAction)listDetailsAction:(id)sender
{
     [self setmenudown];
    ListingDetailViewController *listingDetailVC;
    if ([myappDelegate isiphone5])
    {
        listingDetailVC = [[ListingDetailViewController alloc]initWithNibName:@"ListingDetailViewController" bundle:nil];
    }
    else
    {
        listingDetailVC = [[ListingDetailViewController alloc]initWithNibName:@"ListingDetailViewController_iphone4" bundle:nil];
    }
    
    if (ismyfeed)
    {
        listingDetailVC.listingmainarray=myappDelegate.myfeedsGlobalarray;
    }
    else
    {
        listingDetailVC.listingmainarray=myappDelegate.suggestonsGlobalarray;
    }
    listingDetailVC.senderIndex=[sender tag];
    listingDetailVC.condstring=@"";
    [self.navigationController pushViewController:listingDetailVC animated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)successresponceListing :(id)respdict :(NSString *)paramname;
{    dispatch_async( dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        dispatch_async( dispatch_get_main_queue(), ^{
   
     if ([paramname isEqualToString:@"LikeItem"])
     {
        NSLog(@"resp dict %@",respdict);
        
        if ([respdict isKindOfClass:[NSDictionary class]])
        {
            NSDictionary *mydict=(NSDictionary *)respdict;
            
            NSString *messagestr=[mydict valueForKey:@"message"];
            if ([messagestr isEqualToString:@"Wishlist Saved Successfully"] || [messagestr isEqualToString:@"Wishlist Delted Successfully"])
            {
                ItemsModelobject *itemobj;
                
                if (ismyfeed)
                {
                    itemobj=[myappDelegate.myfeedsGlobalarray objectAtIndex:likeindex];
                }
                else
                {
                    itemobj=[myappDelegate.suggestonsGlobalarray objectAtIndex:likeindex];
                }

                
                if ([messagestr isEqualToString:@"Wishlist Saved Successfully"])
                {
                    itemobj.likedbool=YES;
                    
                    NSString *likedid=[NSString stringWithFormat:@"%@", itemobj.userlistingidstr];
                    [myappDelegate.allLikesGlobalarray addObject:likedid];
                }
                else
                {
                     itemobj.likedbool=NO;
                    
                    for (int i=0; i<myappDelegate.allLikesGlobalarray.count; i++)
                    {
                        NSString *listidstr=[myappDelegate.allLikesGlobalarray objectAtIndex:i];
                        if ([[NSString stringWithFormat:@"%@",listidstr] isEqualToString:[NSString stringWithFormat:@"%@",itemobj.userlistingidstr]])
                        {
                            [myappDelegate.allLikesGlobalarray removeObjectAtIndex:i];
                            break;
                        }
                    }
                }
                
                [myappDelegate.mylikesGlobalarray removeAllObjects];
                BOOL breakbool=NO;
                for (int i=0; i<itemscrollview.subviews.count; i++)
                {
                    UIView *myv=[itemscrollview.subviews objectAtIndex:i];
                    
                    if (myv.tag==likeindex)
                    {
                        if ([myv isKindOfClass:[UIView class]])
                        {
                            for (UIView *temv in myv.subviews)
                            {
                                if ([temv isKindOfClass:[UIButton class]])
                                {
                                    UIButton *likebtn=(UIButton *)temv;
                                    if (likebtn.frame.size.width<20)
                                    {
                                        if (itemobj.likedbool==YES)
                                        {
                                            
                                            
                                            [likebtn setImage:[UIImage imageNamed:@"like_on.png"] forState:UIControlStateNormal];
                                                                                   }
                                        else{
                                            [likebtn setImage:[UIImage imageNamed:@"like_off.png"] forState:UIControlStateNormal];
                                            

                                        }
                                        
                                        breakbool=YES;
                                        break;
                                    }
                                }
                            }
                            
                            if (breakbool==YES)
                            {
                                break;
                            }
                        }
                        
                        
                    }
                }
                
            }
            
            
        }
      //  loaddatabool=YES;
        
         [APPDELEGATE stopspinner:self.view];
    }
    else if ([paramname isEqualToString:@"getallLikes"])
    {
        NSLog(@" check teh dictionay  %@ ",respdict);
        [myappDelegate.allLikesGlobalarray removeAllObjects];
      //
        if ([respdict isKindOfClass:[NSArray class]])
        {
            NSArray *resparray=(NSArray *)respdict;
            
            for (int i=0; i<resparray.count; i++)
            {
                NSDictionary *subdict=[resparray objectAtIndex:i];
                
                NSString *likedid=[NSString stringWithFormat:@"%@", [subdict objectForKey:@"userListingID"]];
                
                NSLog(@"check the string %@",likedid);
                [myappDelegate.allLikesGlobalarray addObject:likedid];
            }
        }
        
        prevlikedarray=myappDelegate.allLikesGlobalarray;
        if (ismyfeed==YES)
        {
           // loadfeeddatabool=NO;
           // [registrationRep getdata:nil :@"Myfeeds" :@"GET" withcount:@"0"];
        }
        else
        {
          // [registrationRep getdata:nil :@"getListing" :@"GET" withcount:@"0"];
        }
        
        loaddatabool=NO;
        
    }
    else if ([paramname isEqualToString:@"getListing"])
    {
        [myappDelegate.suggestonsGlobalarray removeAllObjects];
        
        NSArray *resparry=(NSArray *)respdict;
        if (resparry.count>0)
        {
            NSMutableDictionary *dict=(NSMutableDictionary*)[resparry objectAtIndex:0];
            if([dict objectForKey:@"startCount"])
            {
                totalrecords=[NSString stringWithFormat:@"%@",[dict objectForKey:@"startCount"]];
                totalrecordsfromDB=[NSString stringWithFormat:@"%@",[dict objectForKey:@"totalCount"]];
            }
        }
        [myappDelegate.suggestonsGlobalarray addObjectsFromArray:[myappDelegate parselistingitem:respdict :YES :prevlikedarray :NO]];
        if ([currentPositon isEqualToString:@"suggestions"])
        {
           [self createscrollview];
        }
        isLoadingMoreRecords=NO;
         [APPDELEGATE stopspinner:self.view];
        
        [refreshControl endRefreshing];
    }
    else if ([paramname isEqualToString:@"getListingMore"])
    {
        //[myappDelegate.suggestonsGlobalarray removeAllObjects];
        
        NSArray *resparry=(NSArray *)respdict;
        if (resparry.count>0)
        {
            NSMutableDictionary *dict=(NSMutableDictionary*)[resparry objectAtIndex:0];
            if([dict objectForKey:@"startCount"])
            {
                totalrecords=[NSString stringWithFormat:@"%@",[dict objectForKey:@"startCount"]];
                totalrecordsfromDB=[NSString stringWithFormat:@"%@",[dict objectForKey:@"totalCount"]];

            }
        }
        [myappDelegate.suggestonsGlobalarray addObjectsFromArray:[myappDelegate parselistingitem:respdict :YES :prevlikedarray :NO]];
        //Krish
        if ([currentPositon isEqualToString:@"suggestions"])
        {
            [self createscrollview];
            int count=[totalrecords intValue];
            count=count/2;
            NSLog(@"totarecords is %@ and count is %d",totalrecords,count);
            if(totalrecords<totalrecordsfromDB)
            {
             [itemscrollview setContentOffset:CGPointMake(itemscrollview.contentOffset.x,(count-1)*200) animated:NO];
             NSLog(@"curr ----- %f",currentscrollPosition.y);
                isLoadingMoreRecords=NO;
            }
            else
            {
                isLoadingMoreRecords=YES;
                // [itemscrollview setContentOffset:CGPointMake(itemscrollview.contentOffset.x,itemscrollview.contentOffset.y) animated:NO];
            }
            
   
        }
       //isLoadingMoreRecords=NO;
        [APPDELEGATE stopspinner:self.view];
        
        [refreshControl endRefreshing];
    }
    else if ([paramname isEqualToString:@"Myfeeds"])
    {
        [myappDelegate.myfeedsGlobalarray removeAllObjects];
        
        [myappDelegate.myfeedsGlobalarray addObjectsFromArray:[myappDelegate parselistingitem:respdict :YES :prevlikedarray :NO]];
        
        if ([currentPositon isEqualToString:@"myfeed"])
        {
           [self createscrollview];
        }
        
        [myappDelegate stopspinner:self.view];
        [refreshControl endRefreshing];
    }
    else if([paramname isEqualToString:@"getcatogery"])
    {
        
        
        [myappDelegate.catogoryGlobalarray removeAllObjects];
        
        
        [myappDelegate parsedataforCategory:respdict ];
        
        [myappDelegate stopspinner:self.view ];
        NSLog(@"check the catogory %@",respdict);
        
        if (myappDelegate.suggestonsGlobalarray.count<1 && myappDelegate.myclosetGlobalarray.count<1 && myappDelegate.myfeedsGlobalarray.count<1 && myappDelegate.mylikesGlobalarray.count<1)
        {
            [self mySuggestionsButtonTapped:nil];
        }
    }
        });
    });
    
}
// Loading the scroll view
-(void)createscrollview
{
    
    for (UIView *selfv in itemscrollview.subviews)
    {
        if ([selfv isKindOfClass:[ODRefreshControl class]])
        {
            
        }
        else{
        [selfv removeFromSuperview];
        }
    }
    int x=15;
    int y=5;
    
  //  NSLog(@" items array count %d",itemsarray.count);
    
    
    if (ismyfeed)
    {
        itemsarray=myappDelegate.myfeedsGlobalarray;
    }
    else
    {
        itemsarray=myappDelegate.suggestonsGlobalarray;
    }
    
    [myappDelegate createmyscrollview:itemscrollview :itemsarray :x :y :self];
    
   //  [itemscrollview setContentOffset:CGPointMake(0, 200) animated:NO];
    
     [itemscrollview setContentOffset:CGPointMake(0, 0) animated:NO];
   
    if (itemsarray.count<=10)
        
    {
        
        [myappDelegate animatemyview:itemscrollview ];
        
        
      //  [self animateview];
    }
    
}


- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    CGPoint offset = scrollView.contentOffset;
    CGRect bounds = scrollView.bounds;
    CGSize size = scrollView.contentSize;
    UIEdgeInsets inset = scrollView.contentInset;
    float y = offset.y + bounds.size.height - inset.bottom;
    float h = size.height;
    
    float reload_distance = krecordscount;
    if (!isLoadingMoreRecords)
    {
        currentscrollPosition=scrollView.contentOffset;
    }
    
    NSLog(@"-------------- y is %f",y);
    NSLog(@"-------------- h is %f",h);
    NSLog(@"-------------- h + reload_distance is %f",h + reload_distance);
    NSLog(@"-------------- h + reload_distance is %f",h + reload_distance-220);
    
    if(y > (h + reload_distance)-220)
    {
        //Call the Method to load More Data...
        NSLog(@"scrollview content size is %f",scrollView.contentOffset.y);
        NSLog(@"currentscrollPosition content size is %f",currentscrollPosition.y);
        if (!isLoadingMoreRecords)
        {
           isLoadingMoreRecords=YES;
            int count=[totalrecords intValue]+krecordscount;
            totalrecords=[NSString stringWithFormat:@"%d",count];
            int totatlcount=[totalrecordsfromDB intValue] + krecordscount;
            
            NSLog(@"totatlcount---------> %d",totatlcount);
            NSLog(@"count---------> %d",count);
            
            if (count<totatlcount)
            {
                // isLoadingMoreRecords=YES;
                if ([currentPositon isEqualToString:@"suggestions"])
                {
                    UIActivityIndicatorView *spinner=(UIActivityIndicatorView*)[scrollView viewWithTag:4821]; // spinner
                    UILabel *loadmore=(UILabel*)[scrollView viewWithTag:8421];// loadmore label
                    
                    NSLog(@"spinner is %@",spinner);
                    NSLog(@"loadmore is %@",loadmore);
                    
                    //[APPDELEGATE startspinner:self.view];
                    if (spinner)
                    {
                        [spinner startAnimating];
                        
                    }
                    if (loadmore)
                    {
                        loadmore.hidden=NO;
                    }
                    [registrationRep getdata:nil :@"getListingMore" :@"GET" withcount:totalrecords];
                    
                }
                else if([currentPositon isEqualToString:@"myfeed"])
                {
                    /* UIActivityIndicatorView *spinner=(UIActivityIndicatorView*)[scrollView viewWithTag:4821]; // spinner
                     UILabel *loadmore=(UILabel*)[scrollView viewWithTag:8421];// loadmore label
                     
                     NSLog(@"spinner is %@",spinner);
                     NSLog(@"loadmore is %@",loadmore);
                     
                     //[APPDELEGATE startspinner:self.view];
                     if (spinner)
                     {
                     [spinner startAnimating];
                     
                     }
                     if (loadmore)
                     {
                     loadmore.hidden=NO;
                     }
                     [registrationRep getdata:nil :@"getListingMore" :@"GET" withcount:totalrecords];
                     */
                }
                
            }
            
            if (count==totatlcount)
            {
                UIActivityIndicatorView *spinner=(UIActivityIndicatorView*)[scrollView viewWithTag:4821]; // spinner
                UILabel *loadmore=(UILabel*)[scrollView viewWithTag:8421];// loadmore label
                
                NSLog(@"spinner is %@",spinner);
                NSLog(@"loadmore is %@",loadmore);
                
                //[APPDELEGATE startspinner:self.view];
                if (spinner)
                {
                    spinner.hidden=YES;
                    
                }
                if (loadmore)
                {
                    loadmore.hidden=YES;
                }
            }
            
        }
        
    }
}
-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    [AppDelegate assaignitemimages:CGRectMake(0, scrollView.contentOffset.y,scrollView.frame.size.width , scrollView.frame.size.height) :itemsarray :itemscrollview];
    
    NSLog(@"heck the content offset %@",NSStringFromCGPoint(scrollView.contentOffset));
   
}

// Animation for the Menu button.
-(void)setmenudown
{
    CGRect menu=menuview.frame;
    CGRect menuButtonframe=menubtnview.frame;
    if (myappDelegate.isiphone5)
    {
        if(ismenu==YES)
        {
            ismenu=NO;
            // menuBtn.frame=CGRectMake(131,470, 58, 59);
            menuButtonframe=CGRectMake(131,470, 58, 59);
            
            menu.origin.y=menuview.frame.origin.y+600;
            // menuview.frame=menu;
           //  NO;
        }
        // YES;
    }
    else
    {
        if(ismenu==YES)
        {
            
            
            ismenu=NO;
            
            // menuBtn.frame=CGRectMake(131,355, 58, 59);
            menuButtonframe=CGRectMake(131,355, 58, 59);
           
            menu.origin.y=menuview.frame.origin.y+482;
            // menuview.frame=menu;
           //  NO;
            
        }
    }
    [UIView animateWithDuration:0.5 animations:^{
        
        //        CGRect rect= usersTableview.frame;
        //        rect.origin.y=64;
        //        usersTableview.frame=rect;
        
        
        menuview.frame=menu;
        menubtnview.frame=menuButtonframe;
        
    } completion:^(BOOL finished) {
        
    }];
    
   //  YES;
    
}

-(IBAction)likebuttonaction:(id)sender
{
    dispatch_async( dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH,0), ^{
        dispatch_async(dispatch_get_main_queue(), ^{

    [self setmenudown];
    
    // [myappDelegate startspinner:self.view];
    
    
    
    likeindex=[sender tag];
    
    
     ItemsModelobject *itemobj=[itemsarray objectAtIndex:[sender tag]];
    
    if ([myappDelegate isnullorempty:myset])
    {
          myset=[[NSMutableSet alloc]init];
    }

    
    
    [myset addObject:itemobj.userlistingidstr];
    
    
    NSLog(@"check my set %@",myset);
    
    [myappDelegate.allLikesGlobalarray objectEnumerator];
    
    
    if (itemobj.likedbool==YES)
    {
        itemobj.likedbool=NO;
        [myappDelegate.allLikesGlobalarray removeObject:[NSString stringWithFormat:@"%@",itemobj.userlistingidstr ]];
    }
    else
    {
        [myappDelegate.allLikesGlobalarray addObject:[NSString stringWithFormat:@"%@",itemobj.userlistingidstr ]];
        itemobj.likedbool=YES;
    }
    
    
   // UIView *myv=[itemscrollview viewWithTag:[sender tag]];
    
    //NSLog(@"myv subviews %@ ",myv.subviews);
    BOOL breakbool=NO;
    for (int i=0; i<itemscrollview.subviews.count; i++)
    {
        UIView *myv=[itemscrollview.subviews objectAtIndex:i];
        
        if (myv.tag==likeindex)
        {
            if ([myv isKindOfClass:[UIView class]])
            {
                for (UIView *temv in myv.subviews)
                {
                    if ([temv isKindOfClass:[UIButton class]])
                    {
                        UIButton *likebtn=(UIButton *)temv;
                        if (likebtn.frame.size.width==23)
                        {
                            if (itemobj.likedbool==YES)
                            {
                                
                                
                                [likebtn setImage:[UIImage imageNamed:@"like_on.png"] forState:UIControlStateNormal];
                            }
                            else{
                                [likebtn setImage:[UIImage imageNamed:@"like_off.png"] forState:UIControlStateNormal];
                                
                                
                            }
                            
                            breakbool=YES;
                            break;
                        }
                    }
                }
                
                if (breakbool==YES)
                {
                    break;
                }
            }
        }
    }

    NSString *userid=[APPDELEGATE getStringFromPlist:@"userid"];
    NSLog(@"the appid is %@",userid);

    
    NSDictionary *likedict=[[NSDictionary alloc]initWithObjectsAndKeys:userid,@"UserID",itemobj.userlistingidstr,@"UserListingID", nil];
    
    NSLog(@"chekc the like dict tag of sender %d-=-=-=%@",likeindex, likedict);
    registrationRep.delegate=self;
    [registrationRep getdata:likedict :@"LikeItem" :@"POST" withcount:@"0"];
    
    [myappDelegate.mylikesGlobalarray removeAllObjects];
    });
    });
}

//Animation of  menu.
-(void)animateview
{
    //itemscrollview.frame=CGRectMake(0, 250, 320, 504);
    [UIView animateWithDuration:0.2 animations:^{
        CGRect height=itemscrollview.frame;
        height.origin.y=itemscrollview.frame.origin.y-195;
        itemscrollview.frame=height;
        [self zoombtn];
    }];
    
    
    if (myappDelegate.isiphone5)
    {
        itemscrollview.frame=CGRectMake(0, 64, 320, 504);
    }
    else{
        itemscrollview.frame=CGRectMake(0, 64, 320, 416);
    }
    
}
-(void)zoombtn
{
    CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"transform.scale"];
    // Set the initial and the final values
    [animation setFromValue:[NSNumber numberWithFloat:0.4f]];
    [animation setToValue:[NSNumber numberWithFloat:1.00f]];
    
    // Set duration
    [animation setDuration:0.4f];
    
    // Set animation to be consistent on completion
    [animation setRemovedOnCompletion:NO];
    [animation setFillMode:kCAFillModeForwards];
    
    // Add animation to the view's layer
    [[itemscrollview layer] addAnimation:animation forKey:@"scale"];
}
// This action performs push to the NotificationViewController.
-(IBAction)notificationclicked:(id)sender
{
    NotificationViewController *notificationVC;
    
    if(myappDelegate.isiphone5==NO)
    {
        notificationVC=[[NotificationViewController alloc]initWithNibName:@"NotificationViewController_iphone4" bundle:nil];
    }
    else if([[UIScreen mainScreen] bounds].size.height == 568)
    {
        notificationVC=[[NotificationViewController alloc]initWithNibName:@"NotificationViewController" bundle:nil];
        
    }
    NSString *countstr=@"0";
    [APPDELEGATE insertStringInPlist:@"notificationcount" value:countstr];
    [self.navigationController pushViewController:notificationVC animated:YES];
}
-(void)internetConnection
{
     dispatch_async(dispatch_get_main_queue(), ^{
    UIAlertView *Internetalert=[[UIAlertView alloc]initWithTitle:appname message:@"Internet connection not available" delegate:self cancelButtonTitle:nil otherButtonTitles:@"Ok", nil];
          [APPDELEGATE stopspinner:self.view];
         self.view .userInteractionEnabled=YES;
    [Internetalert show];
     });
       NSLog(@"******* Suggestions*********");
}


@end
