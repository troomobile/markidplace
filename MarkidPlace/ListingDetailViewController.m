//
//  ListingDetailViewController.m
//  MarkidPlace
//
//  Created by stellentmac1 on 5/21/14.
//  Copyright (c) 2014 stellentmac1. All rights reserved.
//[itemobj.itemStatus isEqualToString:@"Sold"]

#import "ListingDetailViewController.h"
#import "CartViewController.h"
#import <FacebookSDK/FacebookSDK.h>
#import "ChartViewController.h"
#import "Listingobject.h"
#import "UserLists.h"

@interface ListingDetailViewController ()

@end

@implementation ListingDetailViewController
@synthesize navbar,listingscrollview,itemImageView,itemPriceLabel,itemTitleLabel,listingdetailstblview,exportbutton,shareimgview,fbSubView,fbImgView,facebookID;
@synthesize shippingLabel,shippingValueLabel,categoryLabel,categoryNameLabel,editview ,iscompare;
@synthesize sizeLabel,sizeNameLabel,brandLabel,brandNameLabel,conditionLabel,conditionNameLabel,descrptionLabel,descrptionNameLabel,oldPriceLabel,fbtextView,accountStore,account,fbaccount,fbaccountStore,addtoCartImgView,addtoCartButton;

@synthesize senderIndex,firstaaray,secondarray;
@synthesize pageControl,pagescrollView,detailsImageView;

@synthesize  listingmainarray,likeimgv,notifystr;
@synthesize sltobj;
@synthesize userbtn,editbutton,condstring,nextview,sellernamelabl,addtocartview,detailview,usernamelbl,exchangestring;

@synthesize zoomscrollView,popview,zoomimgview,zoomimage,zoomNextPrevView;

@synthesize nextpoparrowimg,prevpoparrowimg,likebtn;


@synthesize shippingview,shippingdatalbl,shippingnamelbl;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        listingscrollview.contentOffset=CGPointMake(0, 0);
        // Custom initialization
       
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
     ;
      appdelegate= (AppDelegate *)[[UIApplication sharedApplication]delegate];
    //appdelegate.islikedbool=NO;
    self.navbar.frame = CGRectMake(0, 0, 320, 64);
    listingdetailstblview.layer.borderWidth = 1;
    listingdetailstblview.layer.cornerRadius=1.0f;
   // listingdetailstblview.layer.borderColor = [[UIColor colorWithRed:12.0/255.0 green:174.0/255.0 blue:180.0/255.0 alpha:1.0] CGColor];
    listingdetailstblview.layer.borderColor =[[UIColor whiteColor ] CGColor];
    [self.navbar setBackgroundImage:[UIImage imageNamed:@"navbar.png"] forBarMetrics:UIBarMetricsDefault];
    //CGRect blockviewframe=editview.frame;
   // blockviewframe.origin.y+=400.0f;
    //editview.frame=blockviewframe;
    
    firstaaray=[[NSMutableArray alloc]initWithObjects:@"Edit Listing",@"Delete Listing",@"Mark as SOLD",@"Cancel", nil];
    secondarray=[[NSMutableArray alloc]init];
    
    if( [condstring isEqualToString:@"Closet"]&& [exchangestring isEqualToString:@"profile"])
    {
        likebtn.hidden=YES;
        likeimgv.hidden=YES;
    }
    else
    {
        likebtn.hidden=NO;
        likeimgv.hidden=NO;
    }
        for (int i=0;i<[firstaaray count];i++)
        {
            Listingobject *obj;
            obj=[[Listingobject alloc]init];
            obj.editstr=[firstaaray objectAtIndex:i];
            NSLog(@"name passing....%@", obj.editstr);
            [secondarray addObject:obj];
            // NSLog(@"secondarray is....%@",[secondarray description]);
            
        }

    [self.view addSubview:editview];
    CGRect popfrm=editview.frame;
    popfrm.origin.x=0;
    popfrm.origin.y=595;
    editview.frame=popfrm;
    
    screenSize = [[UIScreen mainScreen]bounds];
    listingscrollview.contentOffset=CGPointMake(0, 0);
  //  _pinterest = [[Pinterest alloc] initWithClientId:@"1234" urlSchemeSuffix:@"prod"];
    [self listingDetail];
    
    CATransition *transition = [CATransition animation];
    transition.duration = 0.0;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype =kCATransitionFromRight;
    transition.delegate = self;
    [listingscrollview.layer addAnimation:transition forKey:@"anim1"];
    [listingscrollview.layer removeAnimationForKey:@"anim1"];


     UITapGestureRecognizer  *swiperight = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(addCartAction:)];
      swiperight.delegate=self;
     [addtocartview addGestureRecognizer:swiperight];
   //  [self performSelector:@selector(addCartAction) withObject:nil afterDelay:0.1];
     //swiperight.direction =UISwipeGestureRecognizerDirectionDown;
    if([[appdelegate getStringFromPlist:@"Twitter"]isEqualToString:@"false"]&&[[appdelegate getStringFromPlist:@"fb"]isEqualToString:@"false"])
    {
        exportbutton.hidden=YES;
        shareimgview.hidden=YES;
    }
   else if([[appdelegate getStringFromPlist:@"fb"]isEqualToString:@"true"]&&[[appdelegate getStringFromPlist:@"Twitter"]isEqualToString:@"false"])
    {
        exportbutton.hidden=NO;
        shareimgview.hidden=NO;
    }
    else if([[appdelegate getStringFromPlist:@"Twitter"]isEqualToString:@"true"]&&[[appdelegate getStringFromPlist:@"fb"]isEqualToString:@"false"])
    {
        exportbutton.hidden=NO;
        shareimgview.hidden=NO;
    }
    else if([[appdelegate getStringFromPlist:@"Twitter"]isEqualToString:@"true"]&&[[appdelegate getStringFromPlist:@"fb"]isEqualToString:@"true"])
    {
        exportbutton.hidden=NO;
        shareimgview.hidden=NO;
    }
  }

-(void)viewWillAppear:(BOOL)animated
{
    
        CGRect blockviewframe=editview.frame;
        blockviewframe.origin.y+=590.0f;
        editview.frame=blockviewframe;
    
    
    [myappDelegate likeandunlikestatus:myappDelegate.suggestonsGlobalarray  :myappDelegate.allLikesGlobalarray ];
    [myappDelegate likeandunlikestatus:myappDelegate.myfeedsGlobalarray  :myappDelegate.allLikesGlobalarray ];
    [myappDelegate likeandunlikestatus:myappDelegate.myclosetGlobalarray  :myappDelegate.allLikesGlobalarray ];
    [myappDelegate likeandunlikestatus:myappDelegate.mylikesGlobalarray  :myappDelegate.allLikesGlobalarray ];
    
    if ([condstring isEqualToString:@"New"])
    {
       
        [myappDelegate startspinner:self.view];
        NSMutableDictionary *paramdict=[[NSMutableDictionary alloc]init];
        
        [paramdict setObject:sltobj.userlistingidstr forKey:@"UserListingId"];
        likebtn.hidden=YES;
        likeimgv.hidden=YES;
         [registrationRep getdata:paramdict :@"ListingDetail" :@"GET" withcount:@"0"];
    }
    
    
    // [self assaignviewdata];
   
   // [listingscrollview setContentOffset:CGPointMake(0, 0) animated:NO];
    [self performSelector:@selector(assaignviewdata) withObject:nil afterDelay:0.1];
   
}



- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

//Setting the Fonts to labels
-(void)listingDetail
{
    if (myappDelegate.isiphone5)
    {
        [listingscrollview setContentSize:CGSizeMake(listingscrollview.frame.size.width, 700)];
    }
    else
    {
        [listingscrollview setContentSize:CGSizeMake(listingscrollview.frame.size.width, 730)];
    }
    
    _pinterest=[[Pinterest alloc]initWithClientId:@"1438258" urlSchemeSuffix:@"prod"];
    
    self.listingDetailsLabel.font =[AppDelegate navtitlefont];
    
    self.itemTitleLabel.font = [UIFont fontWithName:@"Muli" size:15];
    self.itemPriceLabel.font = [UIFont fontWithName:@"Muli" size:13];
    self.oldPriceLabel.font = [UIFont fontWithName:@"Muli" size:13];
    self.shippingLabel.font = [UIFont fontWithName:@"Muli" size:13];
    self.shippingValueLabel.font = [UIFont fontWithName:@"Muli" size:13];
    sellernamelabl.font = [UIFont fontWithName:@"Muli" size:13];
    self.categoryLabel.font = [UIFont fontWithName:@"Muli" size:15];
    self.categoryNameLabel.font = [UIFont fontWithName:@"Muli" size:15];
    self.sizeLabel.font = [UIFont fontWithName:@"Muli" size:15];
    self.sizeNameLabel.font = [UIFont fontWithName:@"Muli" size:15];
    self.brandLabel.font = [UIFont fontWithName:@"Muli" size:15];
    self.brandNameLabel.font = [UIFont fontWithName:@"Muli" size:15];
    self.conditionLabel.font = [UIFont fontWithName:@"Muli" size:15];
    self.conditionNameLabel.font = [UIFont fontWithName:@"Muli" size:15];
    self.descrptionLabel.font = [UIFont fontWithName:@"Muli" size:15];
    self.descrptionNameLabel.font = [UIFont fontWithName:@"Muli" size:15];
    
    self.shippingnamelbl.font = [UIFont fontWithName:@"Muli" size:15];
    self.shippingdatalbl.font = [UIFont fontWithName:@"Muli" size:15];
    
    
    
    listingscrollview.contentOffset=CGPointMake(0, 0);
    
    zoomNextPrevView.layer.borderWidth=2.0f;
    zoomNextPrevView.layer.borderColor=[[UIColor colorWithRed:12.0/255.0 green:174.0/255.0 blue:180.0/255.0 alpha:0.5]CGColor];
    zoomNextPrevView.layer.cornerRadius=5.0f;
    zoomNextPrevView.clipsToBounds=YES;
    registrationRep=[[RegistrationRepository alloc ]init];
    
    registrationRep.delegate=self;
    
    if (listingmainarray.count==1)
    {
        self.prevarrowimg.alpha=0.2;
        self.nextarrowimg.alpha=0.2;
        
        self.nextview.userInteractionEnabled=NO;
    }
    /*
    if (senderIndex+1 >= listingmainarray.count)
    {
        self.nextarrowimg.alpha=0.2;
    }
    else if (senderIndex <=0)
    {
        
        self.prevarrowimg.alpha=0.2;
    }
    else
    {
        self.nextarrowimg.alpha=1.0 ;
        self.prevarrowimg.alpha=1.0;
    }*/
}

//This method is used for assigning the data to labels.
-(void)assaignviewdata
{
    
    NSLog(@"listingmainarray...%@",listingmainarray);
    NSLog(@"senderIndex....%d",senderIndex);
  // [listingmainarray addObject:sltobj];//
     NSLog(@"listingmainarray...%@",listingmainarray);//
    sltobj=[listingmainarray objectAtIndex:senderIndex];
    NSLog(@"weightstr is ...%@",sltobj.weightstring);

    markitemidstr=sltobj.itemidstr;
    NSLog(@"the mark as sold id is ==>>>>>%@",sltobj.itemidstr);
    
    NSLog(@"senderindex...%d,sltobj.likedbool=%d",senderIndex,sltobj.likedbool);
      NSLog(@"categoryid is %@",sltobj.catogryIdstr);
    
    if ([condstring isEqualToString:@"New"] || [condstring isEqualToString:@"Closet"] || [[NSString stringWithFormat:@"%@",myappDelegate.useridstring] isEqualToString:[NSString stringWithFormat:@"%@",sltobj.useridstr]])
    {
        
        if (([[NSString stringWithFormat:@"%@",myappDelegate.useridstring] isEqualToString:[NSString stringWithFormat:@"%@",sltobj.useridstr]]) && (![condstring isEqualToString:@"New"] && ![condstring isEqualToString:@"Closet"]&&![condstring isEqualToString:@"filterresult"]) )
        {
            editbutton.hidden=YES;
            userbtn.hidden=NO;
            nextview.hidden=NO;
            likebtn.hidden=NO;
            likeimgv.hidden=NO;
            editbutton.titleLabel.font=[UIFont fontWithName:@"Muli" size:14];
            
            CGRect cartfrm=addtocartview.frame;
            
            CGRect detailfrm=detailview.frame;
            detailfrm.origin.y=cartfrm.origin.y;
            detailview.frame=detailfrm;
            
            addtocartview.hidden=YES;
            self.listingDetailsLabel.text=@"Listing Details";
            if (iscompare==YES)
            {
                userbtn.hidden=YES;
                nextview.hidden=YES;
                self.listingDetailsLabel.text=@"Listing Details";
                editbutton.hidden=YES;
                likebtn.hidden=YES;
                likeimgv.hidden=YES;
                
            }


        }
        else
        {
            if (iscompare==YES)
            {
                userbtn.hidden=YES;
                nextview.hidden=YES;
                self.listingDetailsLabel.text=@"Listing Details";
                editbutton.hidden=YES;
                likebtn.hidden=YES;
                likeimgv.hidden=YES;

            }
            else
            {
            userbtn.hidden=YES;
            nextview.hidden=YES;
            self.listingDetailsLabel.font =[AppDelegate navtitlefont];
            self.listingDetailsLabel.text=@"Edit Listing Details";
            NSLog(@"self.listingDetailsLabel.text...%@",self.listingDetailsLabel.text);
            editbutton.hidden=NO;
            likebtn.hidden=YES;
            likeimgv.hidden=YES;
            }
           if([condstring isEqualToString:@"filterresult"])
            {
                 self.listingDetailsLabel.text=@"Listing Details";
                
                editbutton.hidden=YES;
                likebtn.hidden=NO;
                likeimgv.hidden=NO;

            }
          
            CGRect cartfrm=addtocartview.frame;
            
            CGRect detailfrm=detailview.frame;
            detailfrm.origin.y=cartfrm.origin.y;
            detailview.frame=detailfrm;
            
            addtocartview.hidden=YES;
        }
                   }
    /*else if([condstring isEqualToString:@"filterresult"])
    {
        NSLog(@"the sltobj.useridstr is %@",sltobj.useridstr);
        NSLog(@"the myappDelegate.useridstring is %@",myappDelegate.useridstring);
        
        if([[NSString stringWithFormat:@"%@",myappDelegate.useridstring] isEqualToString:[NSString stringWithFormat:@"%@",sltobj.useridstr]])
        {
        nextview.hidden=YES;
        editbutton.hidden=YES;
        }
        else
        {
            nextview.hidden=YES;
            editbutton.hidden=YES;
        }
    }*/
    else
    {
        editbutton.hidden=YES;
        
        userbtn.hidden=NO;
         if([condstring isEqualToString:@"filterresult"])
         {
             nextview.hidden=YES;
             editbutton.hidden=YES;
             likebtn.hidden=NO;
             likeimgv.hidden=NO;

             
         }
        else
        {
            nextview.hidden=NO;
            editbutton.hidden=YES;
            likebtn.hidden=NO;
            likeimgv.hidden=NO;

        }
         self.listingDetailsLabel.text=@"Listing Details";
        editbutton.titleLabel.font=[UIFont fontWithName:@"Muli" size:14];
        
        addtocartview.hidden=NO;
        
        if([sltobj.itemStatus isEqualToString:@"Sold"])
        {
           // addtoCartButton.hidden=YES;
            addtoCartImgView.image=[UIImage imageNamed:@"itemsold.png"];
        }
        else
        {
           // addtoCartButton.hidden=NO;
            //addtoCartImgView.image=[UIImage imageNamed:@"AddToCart@2x.png"];
            [addtoCartImgView setImage:[UIImage imageNamed:@"AddToCart.png"]];
        }

        
        CGRect cartfrm=addtocartview.frame;
        
        CGRect detailfrm=detailview.frame;
        detailfrm.origin.y=cartfrm.origin.y+cartfrm.size.height+5;
        detailview.frame=detailfrm;
        
    }

     NSLog(@"check the userid %@ --- ",sltobj.useridstr);
    NSLog(@"number of pictures %lu",(unsigned long)sltobj.picturearray.count);
    if (sltobj.picturearray.count<1)
    {
        // for (int i = 0; i < sltobj.picturearray.count; i++)
        {
            CGRect frame;
            frame.origin.x = self.pagescrollView.frame.size.width * 0;
            frame.origin.y = 0;
            frame.size = self.pagescrollView.frame.size;
            
            
            UIImageView* imgView = [[UIImageView alloc] init];
            imgView.image=[UIImage imageNamed:@"noimage.png"  ];
            
            imgView.frame = frame;
            
            [pagescrollView addSubview:imgView];
        }
        
        [pagescrollView setContentSize:CGSizeMake(self.pagescrollView.frame.size.width, self.pagescrollView.frame.size.height)];
        self.pageControl.numberOfPages = 1;
    }
    else
    {
        CGRect scrollfrm=pagescrollView.frame;
        scrollfrm.origin.x=320;
        for (UIView *temv in pagescrollView.subviews)
        {
            [temv removeFromSuperview];
        }
        
        for (int i = 0; i < sltobj.picturearray.count; i++)
        {
            CGRect frame;
            frame.origin.x = self.pagescrollView.frame.size.width * i;
            frame.origin.y = 0;
            frame.size = self.pagescrollView.frame.size;
            
             UIImageView* imgView = [[UIImageView alloc] init];
            
            imgView.tag=i;
            id photoid=[sltobj.picturearray objectAtIndex:i];
            if ([photoid isKindOfClass:[UIImage class]] )
            {
                
                
                imgView.image=[sltobj.picturearray objectAtIndex:i];
            }
            else  if ([photoid isKindOfClass:[NSString class]] )
            {
            NSString *urlstr=[sltobj.picturearray objectAtIndex:i];
            NSURL *picurl=[NSURL URLWithString:urlstr];
            
           
            
            [imgView setImageWithURL:picurl             placeholderImage:[UIImage imageNamed:@"noimage.png"] options:SDWebImageCacheMemoryOnly];
            }
            else{
                imgView.image= [UIImage imageNamed:@"noimage.png"];
            }
            
            imgView.frame = frame;
            
            [pagescrollView addSubview:imgView];
        }
        [pagescrollView setContentSize:CGSizeMake(sltobj.picturearray.count*self.pagescrollView.frame.size.width, self.pagescrollView.frame.size.height)];
        self.pageControl.numberOfPages = sltobj.picturearray.count;
        
    }
   
     [pagescrollView setContentOffset:CGPointMake(0, 0)];
    pageControl.frame= CGRectMake(8, pageControl.frame.origin.y, 16*sltobj.picturearray.count, pageControl.frame.size.height);

    if (sltobj.likedbool==YES)
    // if(sltobj.likeStatus==YES)
    {
        likeimgv.image=[UIImage imageNamed:@"like_on.png"];//like_on.png
        
        
    }
    else
    {
        likeimgv.image=[UIImage imageNamed:@"like_off.png"];//like_off.png
        
    }
    
    
    //azhar
//    NSLog(@"sltobj.userlistingidstr ...%@",sltobj.userlistingidstr);
//     NSLog(@"sltobj.itemidstr ...%@",sltobj.itemidstr);
//    if ([sltobj.userlistingidstr isEqualToString:sltobj.itemidstr])
//    {
//        likeimgv.image=[UIImage imageNamed:@"like_on.png"];
//    }
//    //azhar
    if([APPDELEGATE isnullorempty:sltobj.itemNamestr])
    {
   
    }
    else
    {
        itemTitleLabel.text=sltobj.itemNamestr; 
    }
    NSLog(@"itemTitleLabel.text....%@",itemTitleLabel.text);
    itemPriceLabel.text= [NSString stringWithFormat:@"$%@", sltobj.salecostcoststr];
    oldPriceLabel.text=[NSString stringWithFormat:@"$%@",sltobj.listingcoststr];
    
    NSDictionary* attributes = @{  NSStrikethroughStyleAttributeName: [NSNumber numberWithInt:NSUnderlineStyleSingle]   };
    
    NSAttributedString* attrText = [[NSAttributedString alloc] initWithString:oldPriceLabel.text attributes:attributes];
    oldPriceLabel.attributedText=attrText;
    
    
    //oldPriceLabel.attributedText = attrString;
    CGSize maximumLabelSize = CGSizeMake(187,CGFLOAT_MAX);
    CGSize requiredSize = [itemPriceLabel sizeThatFits:maximumLabelSize];
    CGRect labelFrame = itemPriceLabel.frame;
    labelFrame.size.width = requiredSize.width;
    itemPriceLabel.frame = labelFrame;
    
    CGRect oldpfrm=oldPriceLabel.frame;
    oldpfrm.origin.x= itemPriceLabel.frame.size.width+itemPriceLabel.frame.origin.x+10;
    oldPriceLabel.frame=oldpfrm;
    
    
    categoryNameLabel.text=sltobj.catogrystr;
    sizeNameLabel.text=sltobj.sizestr;
    brandNameLabel.text=sltobj.brandstr;
    conditionNameLabel.text=sltobj.conditionstr;
    if([APPDELEGATE isnullorempty:sltobj.descriptionstr])
    {
        
    }
    else
    {
    descrptionNameLabel.text=sltobj.descriptionstr;
    }
    

    shippingLabel.text=[NSString stringWithFormat:@"%@, %@",sltobj.citystring,sltobj.statestr];
    shippingValueLabel.text=@"";
    
 
    
    
    if ([myappDelegate isnullorempty:sltobj.namestr])
    {
      //  usernamelbl.text=  @"@--";//sltobj.usernamestring;
        }
    else
    {
        
        NSDictionary *underlineAttribute = @{NSUnderlineStyleAttributeName: @(NSUnderlineStyleSingle)};
        usernamelbl.attributedText = [[NSAttributedString alloc] initWithString:sltobj.namestr  attributes:underlineAttribute];
       // usernamelbl.text=[NSString stringWithFormat:@"@%@",sltobj.usernamestring];
    }
    
    
    CGSize maswidsize = CGSizeMake(137,CGFLOAT_MAX);
    CGSize requiredSize2 = [usernamelbl sizeThatFits:maswidsize];
    CGRect labelFrame12 = usernamelbl.frame;
    labelFrame12.size.width = requiredSize2.width+5;
    if (labelFrame12.size.width>135)
    {
        labelFrame12.size.width=135;
    }
    
    usernamelbl.frame = labelFrame12;
    
   /*
    usernamelbl.layer.borderWidth=2.0f;
    usernamelbl.layer.cornerRadius=8.0f;
    usernamelbl.backgroundColor=[UIColor colorWithRed:12.0/255.0 green:174.0/255.0 blue:180.0/255.0 alpha:0.5] ;
    usernamelbl.layer.borderColor = [[UIColor colorWithRed:12.0/255.0 green:174.0/255.0 blue:180.0/255.0 alpha:0.5] CGColor];
   // usernamelbl.layer.bounds=YES;
    //[userlayer setCornerRadius:0.5];
    //usernamelbl.layer=userlayer;
    */
    
    oldpage = 0;
    self.pageControl.currentPage = 0;
    // detail lable height
    NSAttributedString *attributedText =
    [[NSAttributedString alloc]
     initWithString:descrptionNameLabel.text attributes:@{NSFontAttributeName:descrptionNameLabel.font }];
    
    CGRect rect = [attributedText boundingRectWithSize:(CGSize){180, 120} options:NSStringDrawingUsesLineFragmentOrigin    context:nil];
        CGRect desclblfrm=descrptionNameLabel.frame;
    desclblfrm.size.height=rect.size.height;
    descrptionNameLabel.frame=desclblfrm;
    
    NSLog(@"number of lines %f",desclblfrm.size.height/18);
    descrptionNameLabel.numberOfLines= (int)(desclblfrm.size.height/18)  ;
    
    CGRect shippfrm=shippingview.frame;
    
    shippfrm.origin.y=descrptionNameLabel.frame.origin.y+descrptionNameLabel.frame.size.height+5;
    
    shippingview.frame=shippfrm;
    
    
    
    CGRect detailrect=detailview.frame;
    
    detailrect.size.height=shippfrm.size.height+shippfrm.origin.y+10;
    
    detailview.frame=detailrect;
    if (sltobj.inpersonbool == YES)
    {
        shippingnamelbl.text=@"Meeting in person";
    }
    else{
         shippingnamelbl.text=@"USPS";
    }
    
    listingscrollview.contentOffset=CGPointMake(0, 0);
    
      // sltobj.weightstring
    
//    listingscrollview.scrollsToTop=YES;
//    
//    [listingscrollview scrollRectToVisible:CGRectMake(0, 0, listingscrollview.frame.size.width, listingscrollview.frame.size.height) animated:YES];
//    [listingscrollview scrollRectToVisible:CGRectMake(0, 0, listingscrollview.frame.size.width, listingscrollview.frame.size.height) animated:YES];
    //registrationRep
    
    //Krish
    //Where i am a service by passing waight of item to get shipping price from a service
    [registrationRep getweightnewlisting:sltobj.weightstring];
}


//Krish
//Where i am getting shipping price from a service
-(void)successresponceweight:(NSDictionary *)responseDict
{
    
    dispatch_async(dispatch_get_main_queue(), ^{
        NSLog(@"response....%@",responseDict);
        sltobj.shippingChargesStr=[NSString stringWithFormat:@"%@",[[responseDict valueForKey:@"shippingPrice"] stringValue]];
        
    });
    
}



//performs back button Action
-(IBAction)backAction:(id)sender
{
    if ([condstring isEqual:@"New"] )
    {
        BOOL isVCExist=YES;
        NSArray *arr=self.navigationController.viewControllers;
        NSLog(@"check the viewconts %@ ",arr);
        for (int i=[arr count]-1;i>=0; i-- )
        {
            UIViewController *childvc=arr[i];
            
            NSLog(@"classname %@ ",childvc);
            if([childvc isKindOfClass:[ProfileViewController class]] )
            {
                isVCExist=NO;
                ProfileViewController *profilevc=(ProfileViewController *)childvc;
                [self.navigationController popToViewController:profilevc animated:YES];
                break;
            }
            else if([childvc isKindOfClass:[MyFeedViewController class]] )
            {
                  isVCExist=NO;
                MyFeedViewController *profilevc=(MyFeedViewController *)childvc;
                [self.navigationController popToViewController:profilevc animated:YES];
                break;
            }
            else if([childvc isKindOfClass:[MyWishlistViewController class]] )
            {
                  isVCExist=NO;
                MyWishlistViewController *profilevc=(MyWishlistViewController *)childvc;
                [self.navigationController popToViewController:profilevc animated:YES];
                break;
            }
            else if([childvc isKindOfClass:[MyClosetViewController class]] )
            {
                  isVCExist=NO;
                MyClosetViewController *profilevc=(MyClosetViewController *)childvc;
                [self.navigationController popToViewController:profilevc animated:YES];
                break;
            }
            else if([childvc isKindOfClass:[SuggestionsViewController class]] )
            {
                  isVCExist=NO;
                SuggestionsViewController *profilevc=(SuggestionsViewController *)childvc;
                [self.navigationController popToViewController:profilevc animated:YES];
                break;
            }
            else if([childvc isKindOfClass:[NotificationViewController class]] )
            {
                isVCExist=NO;
                NotificationViewController *profilevc=(NotificationViewController *)childvc;
                [self.navigationController popToViewController:profilevc animated:YES];
                break;
            }
  
        }
        
        if (isVCExist==YES)
        {
            [self.navigationController popViewControllerAnimated:YES];
        }

    }
    else
    {
    [self.navigationController popViewControllerAnimated:YES];
    }
}
//performs Add cart button Action
-(IBAction)addCartAction:(id)sender
{
    if([sltobj.itemStatus isEqualToString:@"Sold"])
    {
    }
    else
    {
        
    
    CartViewController *cartVC;
    if (myappDelegate.isiphone5)
    {
        cartVC = [[CartViewController alloc]initWithNibName:@"CartViewController" bundle:nil];
    }
    else
    {
        cartVC = [[CartViewController alloc]initWithNibName:@"CartViewController_iphone4" bundle:nil];
    }
    cartVC.itemsModelObj=sltobj;
    //////////////////////////
    
  
    
    ///////////////////
   /* for (int i=0; i<pagescrollView.subviews.count; i++)
    {
        UIView *temv=[pagescrollView.subviews objectAtIndex:i];
        
        if ([temv isKindOfClass:[UIImageView class]])
        {
            if (i==oldpage)
            {
                UIImageView *myingv=(UIImageView *)temv;
                
                appdelegate.cartglobalimage=myingv.image;
                [cartVC.cartArray addObject:myingv.image];
                break;
            }
        }
        
    } */
   /*
    if (appdelegate.cartGlobalarray.count>0)
    {
       // [appdelegate.cartGlobalarray removeAllObjects];
        BOOL addobjbool=YES;
        for (int i=0; i<appdelegate.cartGlobalarray.count; i++)
        {
            
            ItemsModelobject *itemobj=[appdelegate.cartGlobalarray objectAtIndex:i];
            
            if ([[NSString stringWithFormat:@"%@",itemobj.itemidstr ] isEqualToString:[NSString stringWithFormat:@"%@",sltobj.itemidstr]] && [[NSString stringWithFormat:@"%@",itemobj.userlistingidstr] isEqualToString:[NSString stringWithFormat:@"%@",sltobj.userlistingidstr]])
            {
                addobjbool=NO;
                break;
            }
        }
        
        
        if (addobjbool==YES)
        {
            [appdelegate.cartGlobalarray addObject:sltobj];
        }
    }
    else
    {
        [appdelegate.cartGlobalarray addObject:sltobj];
    }
    
   */
    [myappDelegate.cartGlobalarray removeAllObjects];//ajay
       [myappDelegate.cartGlobalarray addObject:sltobj];
    //   if (appdelegate.cartGlobalarray.count>0)
    {
        //   [appdelegate.cartGlobalarray removeAllObjects];
        //    [appdelegate.cartGlobalarray addObject:sltobj];
    }
    //else//
    {
        //
    }
    
    [self.navigationController pushViewController:cartVC animated:YES];
    }
}

-(IBAction)detailsOnAction:(id)sender
{
    self.detailsImageView.image = [UIImage imageNamed:@"Details.png"];
}
//performs the Action push to MessageViewController
-(IBAction)detailsOffAction:(id)sender
{
    if ([[NSString stringWithFormat:@"%@",myappDelegate.useridstring] isEqualToString:[NSString stringWithFormat:@"%@",sltobj.useridstr]])
    {
        MessageViewController *controller_Msg;
        if (myappDelegate.isiphone5)
        {
            controller_Msg=[[MessageViewController alloc]initWithNibName:@"MessageViewController" bundle:nil];
        }
        
        else
        {
            controller_Msg=[[MessageViewController alloc]initWithNibName:@"MessageViewController_iphone4" bundle:nil];
        }
        
        [self.navigationController pushViewController:controller_Msg animated:YES];
    }
    else
    {
        ChartViewController *chart_VC;
        if (myappDelegate.isiphone5)
        {
            chart_VC=[[ChartViewController alloc]initWithNibName:@"ChartViewController" bundle:nil];
           
        }
        
        else
            
        {
            chart_VC=[[ChartViewController alloc]initWithNibName:@"ChartViewController_iphone4" bundle:nil];
        }

        chart_VC.titlestr=sltobj.namestr;
        chart_VC.selleruseridstr=sltobj.useridstr;
        chart_VC.profilepicstr=sltobj.profilepicstr;
        [self.navigationController pushViewController:chart_VC animated:YES];
    }
   }

// show the Current page
-(IBAction)pageControllerAction
{
    CGRect frame;
    frame.origin.x = self.pagescrollView.frame.size.width * self.pageControl.currentPage;
    frame.origin.y = 0;
    frame.size = self.pagescrollView.frame.size;
    self.pageControl.currentPage = self.pageControl.currentPage;
    [self.pagescrollView scrollRectToVisible:frame animated:YES];
    pageControlBeingUsed = YES;
}

#pragma mark - scrollview delegate methods

- (void)scrollViewDidScroll:(UIScrollView *)sender
{
    if (!pageControlBeingUsed && sender==pagescrollView)
    {
        // Switch the indicator when more than 50% of the previous/next page is visible
        CGFloat pageWidth = self.pagescrollView.frame.size.width;
        int page = floor((self.pagescrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
        
        //NSLog(@"page ===%d",page);
        
        if (page != oldpage)
        {
            oldpage = page;
            self.pageControl.currentPage = page;
        }
    }
}
- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    pageControlBeingUsed = NO;
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    pageControlBeingUsed = NO;
    
    if (scrollView==pagescrollView)
    {
       
        id imageobj=[sltobj.picturearray objectAtIndex:self.pageControl.currentPage];
        
        if ([imageobj isKindOfClass:[UIImage class]])
        {
            
        }
        else if([imageobj isKindOfClass:[NSString class]] )
        {
            
            for (UIImageView *temv in pagescrollView.subviews )
            {
                if (CGRectIntersectsRect(temv.frame, CGRectMake(self.pageControl.currentPage*320, 0,scrollView.frame.size.width , scrollView.frame.size.height)) && [temv isKindOfClass:[UIImageView class]])
                {
                    if (pageControl.currentPage <sltobj.picturearray.count)
                    {
                        NSString *urlstr=[sltobj.picturearray objectAtIndex:pageControl.currentPage];
                        NSURL *picurl=[NSURL URLWithString:urlstr];
                        [temv setImageWithURL:picurl             placeholderImage:[UIImage imageNamed:@"noimage.png"] options:SDWebImageHighPriority];
                        
                        NSLog(@"load data with priority ===== %ld",(long)pageControl.currentPage);
                    }
                   
                }
            }
            
            
        }
    
    }
    else if (scrollView==listingscrollview)
    {
        CGPoint scrolviewpoint=scrollView.contentOffset;
        NSLog(@"check hte lof for %@",NSStringFromCGPoint(scrolviewpoint));
        
        if (scrolviewpoint.y<1)
        {
            listingscrollview.contentOffset=CGPointMake(0, 0);
           // [scrollView setContentOffset:CGPointMake(0, 0) animated:YES];
        }
    }
    
}



- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView
{
    
    if (zoomscrollView==scrollView)
    {
        NSLog(@"viewForZoomingInScrollView me");
        
        for (UIImageView *vi in scrollView.subviews )
        {
            return vi;
        }
        
        return nil;
    }
    else{
        return scrollView;
    }
   
}

-(BOOL)scrollViewShouldScrollToTop:(UIScrollView *)scrollView
{return YES;
//    if (listingscrollview==scrollView)
//    {
//        return YES;
//    }
//    else
//        return NO;
}


-(void)scrollViewDidEndZooming:(UIScrollView *)scrollView withView:(UIView *)view atScale:(float)scale
{
    // NSLog(@"am in getting called scrollViewDidEndZooming  check the view %@ and the scale %lf  ",view,scale);
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark email sending

// This method is used for sending the emails.
-(void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    [self becomeFirstResponder];
    [myappDelegate.naviCon dismissViewControllerAnimated:YES completion:nil];
    
    //[self dismissModalViewControllerAnimated:YES];
    if(result==MFMailComposeResultCancelled)
    {
        // NSLog(@"yes im cancled");
    }
    else if (result==MFMailComposeResultSent)
    {
        UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"Thank You" message:@"Your mail has been sent successfully." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        //[alert setTag:15];
        [alert show];
        
        
        /// send data to web service
        
        
    }
    
    else if (result == MFMailComposeResultSaved)
    {
        
    }
}

// This method is used for sending the Messages.
- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result
{
	switch (result) {
		case MessageComposeResultCancelled:
        {
            
            
			break;
        }
		case MessageComposeResultFailed:
        {
			UIAlertView *alert = [[UIAlertView alloc] initWithTitle:appname message:@"Unknown Error" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert setTag:15];
			[alert show];
			
			break;
        }
		case MessageComposeResultSent:
        {
            // UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"Thank You" message:@"Your message has been sent successfully." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
            //[alert setTag:15];
            //[alert show];
            
            // send data to webservice
            
            
            
            
			break;
        }
		default:
			break;
	}
    
    
	[myappDelegate.naviCon dismissViewControllerAnimated:YES completion:nil];
}

// performs Action Showing the ActionSheet
-(IBAction)exportbtnaction:(id)sender
{
    if ([sender tag]==570)
    {
        if([[appdelegate getStringFromPlist:@"fb"]isEqualToString:@"true"]&&[[appdelegate getStringFromPlist:@"Twitter"]isEqualToString:@"false"])
        {
            action = [[UIActionSheet alloc]initWithTitle:@"" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Facebook", nil];
        }
        else if([[appdelegate getStringFromPlist:@"Twitter"]isEqualToString:@"true"]&&[[appdelegate getStringFromPlist:@"fb"]isEqualToString:@"false"])
        {
            action = [[UIActionSheet alloc]initWithTitle:@"" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Twitter", nil];
        }
        else if([[appdelegate getStringFromPlist:@"Twitter"]isEqualToString:@"true"]&&[[appdelegate getStringFromPlist:@"fb"]isEqualToString:@"true"])
        {
            action = [[UIActionSheet alloc]initWithTitle:@"" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Facebook",@"Twitter", nil];
        }
        
      //  action = [[UIActionSheet alloc]initWithTitle:@"" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Facebook",@"Pinterest", nil];
        action.tag=570;
        [action showInView:self.view];
    }
    else if([sender tag]==571)
    {
        
        if ([[NSString stringWithFormat:@"%@",myappDelegate.useridstring]  isEqualToString:[NSString stringWithFormat:@"%@",sltobj.useridstr]] )
        {
            
            BOOL isVCExist=NO;
            NSArray *arr=self.navigationController.viewControllers;
            for (UIViewController *childvc in arr) {
                
                if([childvc isKindOfClass:[ProfileViewController class]])
                {
                    isVCExist=YES;
                    UIViewController *subvc=(ProfileViewController *)childvc;
                    [self.navigationController popToViewController:subvc animated:YES];
                    break;
                }
            }
            
            if(!isVCExist)
            {
                ProfileViewController *profileVC;
                if (myappDelegate.isiphone5
                    )
                {
                    profileVC = [[ProfileViewController alloc]initWithNibName:@"ProfileViewController" bundle:nil];
                }
                else
                {
                    profileVC = [[ProfileViewController alloc]initWithNibName:@"ProfileViewController_iphone4" bundle:nil];
                }
             
                [self.navigationController pushViewController:profileVC animated:YES];
            }

            
            
//            ProfileViewController *profivc;
//            if (appdelegate.isiphone5)
//            {
//                profivc = [[ProfileViewController alloc]initWithNibName:@"ProfileViewController" bundle:nil];
//            }
//            else
//            {
//                profivc = [[ProfileViewController alloc]initWithNibName:@"ProfileViewController_iphone4" bundle:nil];
//            }
//            
//           // followVC.sltobj=sltobj;
//            
//            [self.navigationController pushViewController:profivc animated:YES];
        }
        else{
            BOOL isVCExist=NO;
            NSArray *arr=self.navigationController.viewControllers;
            for (UIViewController *childvc in arr) {
                
                if([childvc isKindOfClass:[FollowViewController class]])
                {
                    isVCExist=YES;
                    FollowViewController *subvc=(FollowViewController *)childvc;
                    subvc.listdataarray=listingmainarray;
                    [self.navigationController popToViewController:subvc animated:YES];
                    break;
                }
            }
            
            if(!isVCExist)
            {
                FollowViewController *followVC;
                if (myappDelegate.isiphone5)
                {
                    followVC = [[FollowViewController alloc]initWithNibName:@"FollowViewController" bundle:nil];
                }
                else
                {
                    followVC = [[FollowViewController alloc]initWithNibName:@"FollowViewController_iphone4" bundle:nil];
                }
                
                followVC.sltobj=sltobj;
                followVC.notifystr=@"detail";
                followVC.listdataarray=listingmainarray;
                followVC.imFronVC = @"ListingDetailViewController";
                [self.navigationController pushViewController:followVC animated:YES];
            }

            
            /*
    FollowViewController *followVC;
    if (myappDelegate.isiphone5)
    {
        followVC = [[FollowViewController alloc]initWithNibName:@"FollowViewController" bundle:nil];
    }
    else
    {
        followVC = [[FollowViewController alloc]initWithNibName:@"FollowViewController_iphone4" bundle:nil];
    }
    
    followVC.sltobj=sltobj;
    
    [self.navigationController pushViewController:followVC animated:YES];
              */
        }
             
            
    }
}
-(void)popViewMethod :(BOOL)isshow
{
    CGRect popfrm=editview.frame;
    popfrm.origin.x=0;
    if (isshow)
    {
        // [self.view addSubview:popview];
        popfrm.origin.y=0;
        
        
        CATransition *transition = [CATransition animation];
        transition.duration = 0.3;
        transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
        transition.type = kCATransitionPush;
        transition.subtype =kCATransitionFromTop;
        transition.delegate = self;
        [editview.layer addAnimation:transition forKey:nil];
    }
    else
    {
        popfrm.origin.y=605;
        CATransition *transition = [CATransition animation];
        transition.duration = 0.3;
        transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
        transition.type = kCATransitionPush;
        transition.subtype =kCATransitionFromBottom;
        transition.delegate = self;
        [editview.layer addAnimation:transition forKey:nil];
    }
    
    //[UIView beginAnimations: @"anim" context: nil];
    //[UIView setAnimationBeginsFromCurrentState: YES];
    // [UIView setAnimationDuration:0.3];
    editview.frame=popfrm;
    // [UIView commitAnimations];
    [self.view bringSubviewToFront:editview];
    
    
}
// performs Action Showing the ActionSheet
-(IBAction)editbuttonaction:(id)sender
{
    
     [self popViewMethod:YES];
    
    //UIActionSheet *action;
    
//    firstaaray=[[NSMutableArray alloc]initWithObjects:@"EditListing",@"Delete Listing",@"Mark as Sold",@"cancel", nil];
//    
//    secondarray=[[NSMutableArray alloc]init];
//    for (int i=0;i<[firstaaray count];i++)
//    {
//        Listingobject *obj;
//        obj=[[Listingobject alloc]init];
//        obj.editstr=[firstaaray objectAtIndex:i];
//        NSLog(@"name passing....%@", obj.editstr);
//        [secondarray addObject:obj];
//        // NSLog(@"secondarray is....%@",[secondarray description]);
//        
//    }
//    [UIView animateWithDuration:0.2
//                     animations:^{
//                         // animations go here
//                         
//                        CGRect blockviewframe=editview.frame;
//                         blockviewframe.origin.y=400.0f;
//                         editview.frame=blockviewframe;
//                         
//                         [self.view addSubview:editview];
//                         //[self.view bringSubviewToFront:editview];
//                         [listingdetailstblview reloadData];
//                    }];
//
    
    
    
   // [self.view bringSubviewToFront:editview];
    //[self.view addSubview:editview];
//    
//    if (sltobj.forsalebool==YES)
//    {
//              action = [[UIActionSheet alloc]initWithTitle:@"" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Edit Listing",@"Delete Listing",@"Mark as SOLD", nil];
//    }
//    else{
//       //  action = [[UIActionSheet alloc]initWithTitle:@"" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Edit Listing",@"Mark as For Sale",@"Delete Listing", nil];
//         action = [[UIActionSheet alloc]initWithTitle:@"" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Edit Listing",@"Delete Listing",@"Mark as SOLD", nil];
//    }
//  
//    [action showInView:self.view];
    
    
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 36.0;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return secondarray.count;
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return  1;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellidentifier=@"cell";
    ListingDetailsCustomcell *cell=(ListingDetailsCustomcell *)[listingdetailstblview dequeueReusableCellWithIdentifier:cellidentifier];
    if (cell==nil)
    {
        [[NSBundle mainBundle]loadNibNamed:@"ListingDetailsCustomcell" owner:self options:nil];
        cell=self.listingdetails;
        
    }
    Listingobject *bobj=[secondarray objectAtIndex:indexPath.row];
    cell.editlbl.text=bobj.editstr;
    cell.editlbl.font = [UIFont fontWithName:@"Muli" size:16];
    cell.editlbl.textColor=[UIColor whiteColor];
    //[UIColor colorWithRed:131.0/255.0 green:131.0/255.0 blue:131.0/255.0 alpha:1.0];
    cell.textLabel.backgroundColor=[UIColor clearColor];
    cell.backgroundColor=[UIColor clearColor];
    cell.contentView.backgroundColor=[UIColor clearColor];
    listingdetailstblview.backgroundColor=[UIColor clearColor];
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    return  cell;
    
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
       if (indexPath.row==0)
    {
        [UIView animateWithDuration:0.3
                         animations:^{
                             // animations go here
                             CGRect blockviewframe=editview.frame;
                             blockviewframe.origin.y+=590.0f;
                             editview.frame=blockviewframe;
                         }];

        
        BOOL pushbool=YES;
        NSArray *arr=self.navigationController.viewControllers;
        
        for (UIViewController *childvc in arr)
        {
            
            if([childvc isKindOfClass:[NewListingViewController class]])
            {
                pushbool=NO;
                NewListingViewController *subvc=(NewListingViewController *)childvc;
                subvc.isupdate=YES;
                subvc.selecteditemobj=sltobj;
                [self.navigationController popToViewController:subvc animated:YES];
                break;
            }
        }
        
        if (pushbool)
        {
            
            NSString *pushnib;
            if (myappDelegate.isiphone5)
            {
                pushnib=@"NewListingViewController";
            }
            else{
                pushnib=@"NewListingViewController_iphone4";
            }
            
            NewListingViewController *newlist=[[NewListingViewController alloc]initWithNibName:pushnib bundle:nil];
            newlist.isupdate=YES;
            newlist.selecteditemobj=sltobj;
          // newlist.ListingLabel=@"Edit Listing";
          //  NSLog(@"newlist.ListingLabel....%@",newlist.ListingLabel.text);
            
            NSLog(@"the main weight is%@",sltobj.weightstring);
            newlist.weight=sltobj.weightstring;
            [self.navigationController pushViewController:newlist animated:YES];
        }
    }
    if (indexPath.row==1)
    {
        [UIView animateWithDuration:0.3
                         animations:^{
                             // animations go here
                             CGRect blockviewframe=editview.frame;
                             blockviewframe.origin.y+=590.0f;
                             editview.frame=blockviewframe;
                         }];

        {
            [myappDelegate startspinner:self.view];
            registrationRep.delegate=self;
            
            NSString *itemidstr=[NSString stringWithFormat:@"%@",sltobj.itemidstr];
            
            NSDictionary *likedict=[[NSDictionary alloc]initWithObjectsAndKeys: itemidstr ,@"UserItemId", nil];
            // NSMutableDictionary *likedict=[[NSMutableDictionary alloc]init];
            
            //initWithObjectsAndKeys: [itemidstr integerValue],@"UserItemId", nil];
            //[likedict setValue:[NSNumber numberWithInt:<#(int)#> ] ]  forKey:@"UserItemId"];
            
            [registrationRep getdata:likedict :@"Deletelisting23" :@"DELETE" withcount:@"0"];
            
        }

    }
    if (indexPath.row==2)
    {
        [UIView animateWithDuration:0.3
                         animations:^{
                             // animations go here
                             CGRect blockviewframe=editview.frame;
                             blockviewframe.origin.y+=590.0f;
                             editview.frame=blockviewframe;
                         }];

       // swdf
       // [AppDelegate alertmethod:appname :@"Mark as SOLD is under implementaion" ];
      
        profilerepo=[[ProfileRepository alloc]init];
       profilerepo.delegate=self;
        [APPDELEGATE startspinner:self.view];
       [profilerepo markassold:markitemidstr];
        
        
       
      //UIAlertView *soldalert=[[UIAlertView alloc]initWithTitle:appname message:@"Mark as SOLD is under implementaion" delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        //soldalert.tag=123;
       //soldalert show];
    }
    
    if (indexPath.row==3)
    {
        [UIView animateWithDuration:0.3
                        animations:^{
                             // animations go here
                             CGRect blockviewframe=editview.frame;
                             blockviewframe.origin.y+=590.0f;
                             editview.frame=blockviewframe;
                         }];
    }
}

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
     NSLog(@"sltobj .picturesarry %@ ",sltobj.picturearray );
    if (actionSheet.tag==570)
    {
        //FaceBook ButtonAction
        
        if([[appdelegate getStringFromPlist:@"fb"]isEqualToString:@"true"]&&[[appdelegate getStringFromPlist:@"Twitter"]isEqualToString:@"false"])
        {
            action = [[UIActionSheet alloc]initWithTitle:@"" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Facebook", nil];
            if(buttonIndex==0)
            {
               // [self facebookposting];
                [self facebookButtonAction:self];
            // [self faceBookButtonPressedNewHere:nil];
            }
        }
        else if([[appdelegate getStringFromPlist:@"Twitter"]isEqualToString:@"true"]&&[[appdelegate getStringFromPlist:@"fb"]isEqualToString:@"false"])
        {
            if(buttonIndex==0)
            {
                [self twitterposting];
            }
            
        }
        else if([[appdelegate getStringFromPlist:@"Twitter"]isEqualToString:@"true"]&&[[appdelegate getStringFromPlist:@"fb"]isEqualToString:@"true"])
        {
            if(buttonIndex==0)
            {
               // [self facebookposting];
               // [self facebookButtonAction:self];
                 [self facebookButtonAction:self];
                
            }
            else if (buttonIndex == 1)
            {
                   [self twitterposting];
            }
        }
    }
    
    
    //////////////////////////////////////////////
    else
    {
        if(buttonIndex==0)
        {
            
            BOOL pushbool=YES;
            NSArray *arr=self.navigationController.viewControllers;
            
            for (UIViewController *childvc in arr)
            {
                
                if([childvc isKindOfClass:[NewListingViewController class]])
                {
                     pushbool=NO;
                    NewListingViewController *subvc=(NewListingViewController *)childvc;
                    subvc.isupdate=YES;
                    subvc.selecteditemobj=sltobj;
                    [self.navigationController popToViewController:subvc animated:YES];
                    break;
                }
            }

            if (pushbool)
            {
                
                NSString *pushnib;
                if (myappDelegate.isiphone5)
                {
                  pushnib=@"NewListingViewController";
                }
                else{
                    pushnib=@"NewListingViewController_iphone4";
                }
                
                NewListingViewController *newlist=[[NewListingViewController alloc]initWithNibName:pushnib bundle:nil];
                newlist.isupdate=YES;
                newlist.selecteditemobj=sltobj;
                
                [self.navigationController pushViewController:newlist animated:YES];
            }
        
         
            
        }
        else if (buttonIndex == 1)
        {
            [myappDelegate startspinner:self.view];
            registrationRep.delegate=self;
            
            NSString *itemidstr=[NSString stringWithFormat:@"%@",sltobj.itemidstr];
            
            NSDictionary *likedict=[[NSDictionary alloc]initWithObjectsAndKeys: itemidstr ,@"UserItemId", nil];
            // NSMutableDictionary *likedict=[[NSMutableDictionary alloc]init];
            
            //initWithObjectsAndKeys: [itemidstr integerValue],@"UserItemId", nil];
            //[likedict setValue:[NSNumber numberWithInt:<#(int)#> ] ]  forKey:@"UserItemId"];
            
            [registrationRep getdata:likedict :@"Deletelisting23" :@"DELETE" withcount:@"0"];
            
               }
        else if (buttonIndex == 2)
        {
            [AppDelegate alertmethod:appname :@"Sold is under implementaion" ];
           
        }
    }
}
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(alertView.tag==123)
    {
        if(buttonIndex==0)
        {
            [self.navigationController popViewControllerAnimated:YES];
        }
    }
    if(alertView.tag==15)
    {
        if(buttonIndex==0)
        {
            [self.navigationController popViewControllerAnimated:YES];
        }
    }
}

//performs to show the like button Action
-(IBAction)likebuttonaction:(id)sender
{
    
    
    NSLog(@"sltobj.likedbool.....%d",sltobj.likedbool);
   // NSLog(@"sltobj.likeStatus....%d",sltobj.likeStatus);
      if (sltobj.likedbool==YES)
    //if(sltobj.likeStatus==YES)
    {
        [myappDelegate.allLikesGlobalarray removeObject:[NSString stringWithFormat:@"%@",sltobj.userlistingidstr ]];
        sltobj.likedbool=NO;
       // sltobj.likeStatus=NO;
       
         likeimgv.image=[UIImage imageNamed:@"like_off.png"];
        
    }
    else
    {
        [myappDelegate.allLikesGlobalarray addObject:[NSString stringWithFormat:@"%@",sltobj.userlistingidstr ]];
        sltobj.likedbool=YES;
        
           likeimgv.image=[UIImage imageNamed:@"like_on.png"];
        
    }
   
    
    //appdelegate.islikedbool=sltobj.likedbool;
    
    

   // [appdelegate startspinner:self.view];
   // likeindex=[sender tag];
  //  ItemsModelobject *itemobj=[itemsarray objectAtIndex:[sender tag]];
    NSString *userid=[APPDELEGATE getStringFromPlist:@"userid"];

    NSDictionary *likedict=[[NSDictionary alloc]initWithObjectsAndKeys:userid,@"UserID",sltobj.userlistingidstr,@"UserListingID", nil];
    
   // NSLog(@"chekc the like dict tag of sender %d-=-=-=%@",likeindex, likedict);
    registrationRep.delegate=self;
    [registrationRep getdata:likedict :@"LikeItem" :@"POST" withcount:@"0"];
    
    [myappDelegate.mylikesGlobalarray removeAllObjects];
    if ([condstring isEqualToString:@"Wishlist"])
    {
        //[myappDelegate.mylikesGlobalarray removeAllObjects];
         [self performSelector:@selector(likeBackAction) withObject:nil afterDelay:0.5];
        //[self.navigationController popViewControllerAnimated:YES];
    }
    
}
// Success response from Listing
-(void)successresponceListing :(id)respdict :(NSString *)paramname;
{
    
    NSLog(@"respdict %@ ",respdict);
    
    
    if ([paramname isEqualToString:@"LikeItem"])
    {
        NSLog(@"resp dict %@",respdict);
        
        if ([respdict isKindOfClass:[NSDictionary class]])
        {
            NSDictionary *mydict=(NSDictionary *)respdict;
            
            NSString *messagestr=[mydict valueForKey:@"message"];
            if ([messagestr isEqualToString:@"Wishlist Saved Successfully"] || [messagestr isEqualToString:@"Wishlist Delted Successfully"])
            {
                
               /// ItemsModelobject *itemobj=[itemsarray objectAtIndex:likeindex];
                
                if ([messagestr isEqualToString:@"Wishlist Saved Successfully"])
                {
                    
                    [myappDelegate.allLikesGlobalarray addObject:[NSString stringWithFormat:@"%@",[mydict objectForKey:@"userListingID"] ]];
                    
                    sltobj.likedbool=YES;
                   // sltobj.likeStatus=YES;
                    
                    likeimgv.image=[UIImage imageNamed:@"like_on.png"];//@"like_on.png"
                }
                else
                {
                    sltobj.likedbool=NO;
                    
                   // sltobj.likeStatus=NO;
                     likeimgv.image=[UIImage imageNamed:@"like_off.png"];//@"like_off.png"
                    
                     [myappDelegate.allLikesGlobalarray removeObject:[NSString stringWithFormat:@"%@",[mydict objectForKey:@"userListingID"] ]];
                }
                
                
                
              
            }
        }
        [myappDelegate.mylikesGlobalarray removeAllObjects];
      //  appdelegate.suggestionviewcont.loaddatabool=YES;
        [myappDelegate stopspinner:self.view];
    }
    else if ([paramname isEqualToString:@"Notforsale"])
    {
        
        if ([respdict isKindOfClass:[NSDictionary class]])
        {
            NSDictionary *mydict=(NSDictionary *)respdict;
            
            if ([[mydict valueForKey:@"message"] isEqualToString:@"User Item updated Successfully"] || [[mydict valueForKey:@"message"] isEqualToString:@"Update Forsale Sucessfully"])
            {
                
                
                [self.navigationController popViewControllerAnimated:YES];
            }
            else
            {
                [ AppDelegate alertmethod:appname :@"Unable to update item" ];
            }
        }
        [myappDelegate stopspinner:self.view];
    }
    else if ([paramname isEqualToString:@"Deletelisting23"])
    {
        
        if ([respdict isKindOfClass:[NSDictionary class]])
        {
            NSDictionary *mydict=(NSDictionary *)respdict;
            
            if ([[mydict valueForKey:@"message"] isEqualToString:@"UserItem Deleted Successfully"])
            {
                [myappDelegate.myclosetGlobalarray removeAllObjects];
                
                [self.navigationController popViewControllerAnimated:YES];
            }
            else{
               [ AppDelegate alertmethod:appname :@"Unable to delete item" ];
            }
        }
             [myappDelegate stopspinner:self.view];
    }
    else if ([paramname isEqualToString:@"ListingDetail"])
    {
        if ([respdict isKindOfClass:[NSArray class]])
        {
            NSArray *resparry=(NSArray *)respdict;
            
            NSLog(@"check the data getListing---%@ ",resparry);
            for (int i=0; i<resparry.count; i++)
            {
                
                NSDictionary *maindict=[resparry objectAtIndex:i];
                
               // ItemsModelobject *itemsmodobj=[[ItemsModelobject alloc]init];
                sltobj.itemNamestr=[maindict objectForKey:@"itemName"];
                itemTitleLabel.text=[maindict objectForKey:@"itemName"];
                sltobj.itemidstr=[maindict objectForKey:@"unitPrice"];
                sltobj.listingcoststr=[maindict objectForKey:@"unitPrice"];
                sltobj.salecostcoststr=[maindict objectForKey:@"listPrice"];
                if ([maindict objectForKey:@"status"])
                {
                    sltobj.itemStatus=[maindict objectForKey:@"status"];
                    
                }
               // sltobj.likeStatus=[[maindict valueForKey:@"likeStatus"] boolValue];
                
           // NSLog(@"sltobj.likeStatus...%d",sltobj.likeStatus);
                sltobj.brandstr=[maindict objectForKey:@"brand"];
                sltobj.conditionstr=[maindict objectForKey:@"condition"];
                
                if ( [myappDelegate isnullorempty:[maindict objectForKey:@"otherCategory"] ])
                {
                    sltobj.catogrystr=[maindict objectForKey:@"categoryName"];
                }
                else
                {
                    sltobj.catogrystr=[maindict objectForKey:@"otherCategory"];
                    sltobj.catogryIdstr=[maindict objectForKey:@"categoryId"];
                    NSLog(@"the userlisting cat id is %@",[maindict objectForKey:@"categoryId"]);
                    NSLog(@"the userlisting cat id is %@",sltobj.catogryIdstr);
                }
               // sltobj.catogrystr=[maindict objectForKey:@"categoryName"];
                sltobj.sizestr=[maindict objectForKey:@"size"];
                descrptionNameLabel.text=[maindict objectForKey:@"description"];
                sltobj.descriptionstr=[maindict objectForKey:@"description"];
                sltobj.usernamestring=[maindict objectForKey:@"userName"];
                sltobj.namestr=[maindict objectForKey:@"name"];
                
                sltobj.useridstr=[maindict objectForKey:@"userID"];
                
                sltobj.userlistingidstr=[NSString stringWithFormat:@"%@",[maindict valueForKey:@"userListingId"]];
                
                sltobj.itemidstr=[maindict objectForKey:@"userItemId"];
                sltobj.weightstring=[NSString stringWithFormat:@"%@",[maindict objectForKey:@"weight"]];
                NSLog(@"sltobj.weightstring....%@",sltobj.weightstring);
                sltobj.genderstring=[maindict valueForKey:@"gender"];
                
                if ([myappDelegate isnullorempty:sltobj.genderstring])
                {
                    sltobj.genderstring=@"";
                }
                
                sltobj.uspsbool=[[maindict valueForKey:@"usps"] boolValue];
                
                sltobj.inpersonbool=[[maindict valueForKey:@"inPerson"] boolValue];
                NSLog(@"sltobj.inpersonbool----------> %d",sltobj.inpersonbool);
                
                sltobj.profilepicstr=[maindict valueForKey:@"profilepic"];
                sltobj.citystring=[maindict valueForKey:@"city"];
                sltobj.statestr=[maindict valueForKey:@"state"];
                
                NSLog(@"check this %@=-=-%@",sltobj.statestr,[maindict valueForKey:@"state"]);
                
                if ([myappDelegate isnullorempty:sltobj.citystring])
                {
                    sltobj.citystring=@"";
                }
                
                if ([myappDelegate isnullorempty:sltobj.statestr])
                {
                    sltobj.statestr=@"";
                }
                
                NSArray *itempickarray=[maindict objectForKey:@"itemPictures"];
               
                
                sltobj.picturearray=[NSMutableArray new];
                
                for (int j=0; j<itempickarray.count; j++)
                {
                    NSDictionary *picdict=[itempickarray objectAtIndex:j];
                    
                    
                    NSString *urlsrt=[picdict valueForKey:@"pictureCdnUrl"];
                    urlsrt=[urlsrt stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
                    // %20 NSURL *picutl=[NSURL URLWithString:[picdict valueForKey:@"pictureCdnUrl"]];
                    
                    [sltobj.picturearray addObject:urlsrt];
                }
                
               // sltobj=itemsmodobj;
               /* for (int lk=0; lk<prevlikedarray.count; lk++)
                {
                    NSString *prevlikedstr=[prevlikedarray objectAtIndex:lk];
                    
                    if ([prevlikedstr isEqualToString:itemsmodobj.userlistingidstr ])
                    {
                        itemsmodobj.likedbool=YES;
                        break;
                    }
                    
                } */
                
                
                //[itemsarray addObject:itemsmodobj];
                /*itemsmodobj.itemNamestr=[maindict objectForKey:@""];
                 itemsmodobj.itemNamestr=[maindict objectForKey:@""];
                 itemsmodobj.itemNamestr=[maindict objectForKey:@""];
                 itemsmodobj.itemNamestr=[maindict objectForKey:@""];
                 itemsmodobj.itemNamestr=[maindict objectForKey:@""];
                 itemsmodobj.itemNamestr=[maindict objectForKey:@""];
                 itemsmodobj.itemNamestr=[maindict objectForKey:@""];
                 itemsmodobj.itemNamestr=[maindict objectForKey:@""];
                 */
                
            }
        }
        
        [self assaignviewdata];
        [myappDelegate stopspinner:self.view];
    }

}

//performs Next and previous button Action.
-(IBAction)nextandprevbuttonaction:(id)sender
{
     [listingscrollview setContentOffset:CGPointMake(0, 0) animated:NO];
    if ([sender tag]==10)
    {
        if (listingmainarray.count > senderIndex+1)
        {
            self.nextarrowimg.alpha=1.0;
            senderIndex++;
              [self performSelector:@selector(assaignviewdata) withObject:nil afterDelay:0.1];
           
        }
        else
        {
            self.nextarrowimg.alpha=1.0;
            senderIndex=0;
           [self performSelector:@selector(assaignviewdata) withObject:nil afterDelay:0.1];
            
        }
        CATransition *transition = [CATransition animation];
        transition.duration = 0.5;
        transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
        transition.type = kCATransitionPush;
        transition.subtype =kCATransitionFromLeft;
        transition.delegate = self;
        [listingscrollview.layer addAnimation:transition forKey:@"anim22"];
      
        
       [listingscrollview.layer removeAnimationForKey:@"anim22"];
      
    }
    else
    {
        if ( senderIndex>0 )
        {
            self.prevarrowimg.alpha=1.0;
            senderIndex--;
            [self performSelector:@selector(assaignviewdata) withObject:nil afterDelay:0.1];
           
        }
        else
        {
            self.prevarrowimg.alpha=1.0;
            senderIndex=listingmainarray.count-1;
            [self performSelector:@selector(assaignviewdata) withObject:nil afterDelay:0.1];
           
        }
        CATransition *transition = [CATransition animation];
        transition.duration = 0.5;
        transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
        transition.type = kCATransitionPush;
        transition.subtype =kCATransitionFromRight;
        transition.delegate = self;
        [listingscrollview.layer addAnimation:transition forKey:@"anim1"];
         [listingscrollview.layer removeAnimationForKey:@"anim1"];
        
    }
    
   
    /*
    if (senderIndex+1 >= listingmainarray.count)
    {
        self.nextarrowimg.alpha=0.2;
    }
   else if (senderIndex <=0)
    {
        
        self.prevarrowimg.alpha=0.2;
    }
   else
   {
       self.nextarrowimg.alpha=1.0 ;
       self.prevarrowimg.alpha=1.0;
   }*/

}


#pragma mark -
#pragma mark PinIt Method

// A function for parsing URL parameters.
- (NSDictionary*)parseURLParams:(NSString *)query {
    NSArray *pairs = [query componentsSeparatedByString:@"&"];
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    for (NSString *pair in pairs) {
        NSArray *kv = [pair componentsSeparatedByString:@"="];
        NSString *val =
        [kv[1] stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        params[kv[0]] = val;
    }
    return params;
}


#pragma mark zoom functonality

-(IBAction)zoombuttonaction:(id)sender
{
    
    for (UIView *subv in zoomscrollView.subviews)
    {
        [subv removeFromSuperview];
    }
    // self.view.userInteractionEnabled=NO;
    zoomintval=1.0f;
    //self.view.userInteractionEnabled=NO;
    zoomimgview=[[UIImageView alloc]init];
    
    if(sltobj.picturearray.count>0)
    {
        
        NSString *urlstr=[sltobj.picturearray objectAtIndex:pageControl.currentPage];
        NSURL *picurl=[NSURL URLWithString:urlstr];
        
        [zoomimgview setImageWithURL:picurl             placeholderImage:[UIImage imageNamed:@"noimage.png"] options:SDWebImageHighPriority];
        zoomimage= [sltobj.picturearray objectAtIndex:0];
    }
    zoomimgview.frame=CGRectMake(0, 0, zoomscrollView.frame.size.width, zoomscrollView.frame.size.height);
    
    [zoomimgview setContentMode:UIViewContentModeScaleAspectFit];
    zoomimgview.layer.masksToBounds=YES;
    
    [zoomscrollView addSubview:zoomimgview];
    
    popview.frame=CGRectMake(0, 0, 320, 568);
    [zoomscrollView setContentSize:CGSizeMake(zoomimgview.frame.size.width, zoomimgview.frame.size.height)];
    
    [zoomscrollView addSubview:zoomimgview];
    zoomscrollView.scrollEnabled=YES;
    zoomscrollView.delegate=self;
    zoomscrollView.maximumZoomScale=3.0f;
    zoomscrollView.minimumZoomScale=1.0f;
    [zoomscrollView setContentSize:CGSizeMake(zoomimgview.frame.size.width, zoomimgview.frame.size.height)];
    
    [zoomimgview bringSubviewToFront:zoomscrollView];
    
    [self.view addSubview:popview];
    
    
    CATransition *transition = [CATransition animation];
    transition.duration = 0.5;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype =kCATruncationStart;
    transition.delegate = self;
    [zoomimgview.layer addAnimation:transition forKey:nil];
    
    
    nextpoparrowimg.alpha=1.0;
    prevpoparrowimg.alpha=1.0;
    if (pageControl.currentPage+1 >=sltobj.picturearray.count)
    {
        nextpoparrowimg.alpha=0.5;
    }
    
    
    if (pageControl.currentPage-1<= 0)
    {
        prevpoparrowimg.alpha=0.5;
    }
    
}
// performs the action removing the subview.
-(IBAction)cancelbtnaction:(id)sender
{
    [popview removeFromSuperview];
    if([sender tag]==1000)
    {
         [fbSubView removeFromSuperview];
    }
    
}
// Performs the action previous  and nextbuttons in popview.
-(IBAction)popoverbtnaction:(id)sender
{
    if([sender tag]==999)
    {
        
        if(sltobj.picturearray.count>pageControl.currentPage+1)
        {
            
            [pagescrollView setContentOffset:CGPointMake((pageControl.currentPage+1)*320, 0)];
            
            NSString *urlstr=[sltobj.picturearray objectAtIndex:pageControl.currentPage];
            NSURL *picurl=[NSURL URLWithString:urlstr];
            
            [zoomimgview setImageWithURL:picurl             placeholderImage:[UIImage imageNamed:@"noimage.png"] options:SDWebImageCacheMemoryOnly];
            
            //zoomimage= [sltobj.picturearray objectAtIndex:pageControl.currentPage];
            [self zoombuttonaction:Nil];
            nextpoparrowimg.alpha=1.0;
        }

    }
   else if([sender tag]==888)
    {
        NSLog(@"pagecontrol %d",pageControl.currentPage);
        if(pageControl.currentPage-1>=0)
        {
            
            [pagescrollView setContentOffset:CGPointMake((pageControl.currentPage-1)*320, 0)];
            
            NSString *urlstr=[sltobj.picturearray objectAtIndex:pageControl.currentPage];
            NSURL *picurl=[NSURL URLWithString:urlstr];
            
            [zoomimgview setImageWithURL:picurl             placeholderImage:[UIImage imageNamed:@"noimage.png"] options:SDWebImageCacheMemoryOnly];
          //  zoomimage= [sltobj.picturearray objectAtIndex:pageControl.currentPage];
              [self zoombuttonaction:Nil];
            
            prevpoparrowimg.alpha=1.0;
            
        }
    }
    
    nextpoparrowimg.alpha=1.0;
    prevpoparrowimg.alpha=1.0;
    
    if (pageControl.currentPage+1>=sltobj.picturearray.count)
    {
        nextpoparrowimg.alpha=0.5;
    }
    
    
    if (pageControl.currentPage-1< 0)
    {
        prevpoparrowimg.alpha=0.5;
    }
    
 }
/*
-(void)facebookposting
{
    
    NSLog(@"sltobj .picturesarry %@ ",sltobj.picturearray );
    
    if (sltobj.picturearray.count>0)
    {
        id inputid=[sltobj.picturearray objectAtIndex:0];
        
        
        if ([inputid isKindOfClass:[NSString class]])
        {
            
            NSString *imgurlstr=[sltobj.picturearray objectAtIndex:0];
            
            NSString *descrpstr=sltobj.descriptionstr;
            
            
            NSMutableDictionary *params = [NSMutableDictionary dictionaryWithObjectsAndKeys:  sltobj.itemNamestr, @"name",  @"marKIDplace", @"caption", descrpstr , @"description",imgurlstr, @"picture",
                                           nil];
            
            // Show the feed dialog
            [FBWebDialogs presentFeedDialogModallyWithSession:nil
                                                   parameters:params
                                                      handler:^(FBWebDialogResult result, NSURL *resultURL, NSError *error) {
                                                          if (error) {
                                                              // An error occurred, we need to handle the error
                                                              // See: https://developers.facebook.com/docs/ios/errors
                                                              NSLog(@"Error publishing story: %@", error.description);
                                                          } else {
                                                              if (result == FBWebDialogResultDialogNotCompleted) {
                                                                  // User cancelled.
                                                                  NSLog(@"User cancelled.");
                                                              } else {
                                                                  // Handle the publish feed callback
                                                                  NSDictionary *urlParams = [self parseURLParams:[resultURL query]];
                                                                  
                                                                  if (![urlParams valueForKey:@"post_id"]) {
                                                                      // User cancelled.
                                                                      NSLog(@"User cancelled.");
                                                                      
                                                                  } else {
                                                                      // User clicked the Share button
                                                                      
                                                                      [AppDelegate alertmethod:appname :@"Success! Your item is now in marKIDplace" ];                       NSString *result = [NSString stringWithFormat: @"Posted story, id: %@", [urlParams valueForKey:@"post_id"]];
                                                                      NSLog(@"result %@", result);
                                                                  }
                                                              }
                                                          }
                                                      }];
        }
        else{
            [AppDelegate alertmethod:appname :@"unable to post on facebook. please try after some time"];
        }
        
        
        
    }
    else
    {
        [AppDelegate alertmethod:appname :@"unable to post on facebook. please try after some time"];
    }
 }
*/
-(IBAction)facebookButtonAction:(id)sender
{
    
    /*
     // [self showIndicator];
     if (isFacebook==NO)
     {
     [facebookButton setImage:[UIImage imageNamed:@"facebook_blue_Img.png"] forState:UIControlStateNormal];
     isFacebook=YES;
     
     }
     else
     {
     [facebookButton setImage:[UIImage imageNamed:@"facebook_Img.png"] forState:UIControlStateNormal];
     isFacebook=NO;
     
     }
     */
    dispatch_async(dispatch_get_main_queue(), ^{
    if (sltobj.picturearray.count>0)
    {
        id inputid=[sltobj.picturearray objectAtIndex:0];
        
        
        if ([inputid isKindOfClass:[NSString class]])
        {
            
            imgurlstr=[sltobj.picturearray objectAtIndex:0];
            
            descrpstr=sltobj.descriptionstr;
            namestr=sltobj.itemNamestr;
        }
    }
    
    if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook]) {
        
        SLComposeViewController *mySLComposerSheet = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
        
       
       // [mySLComposerSheet setInitialText:namestr];
     // NSString *sendText=@"<html><body><p>Check out my new listing on marKIDplace, THE app to buy and sell children's clothing, gear, and maternity wear.<a href=\"http://www.itunes.com\">#markidplace</a></p></body></html>";
       //NSString *sendText=@"Check out this awesome item I found on marKIDplace. Get it at : https://www.itunes.com";
         NSString *sendText=@"Check out my new listing on marKIDplace, THE app to buy and sell children's clothing and maternity wear. #markidplace";
        [mySLComposerSheet setInitialText:sendText];
       // UIImage *image = [UIImage imageWithData:appdelegate.capturedImgData];
        NSURL *imageURL = [NSURL URLWithString:imgurlstr];
       
        NSData *imageData = [NSData dataWithContentsOfURL:imageURL];
        UIImage *image = [UIImage imageWithData:imageData];
        
        [mySLComposerSheet addImage:image];
        
        //  [mySLComposerSheet addURL:[NSURL URLWithString:@"http://stackoverflow.com/questions/12503287/tutorial-for-slcomposeviewcontroller-sharing"]];
        
        [mySLComposerSheet setCompletionHandler:^(SLComposeViewControllerResult result)
         {
             
             switch (result)
             {
                 case SLComposeViewControllerResultCancelled:
                     // NSLog(@"Post Canceled");
                    // [facebookButton setImage:[UIImage imageNamed:@"facebook_Img.png"] forState:UIControlStateNormal];
                     break;
                 case SLComposeViewControllerResultDone:
                     // NSLog(@"Post Sucessful");
                     //[facebookButton setImage:[UIImage imageNamed:@"facebook_blue_Img.png"] forState:UIControlStateNormal];
                     break;
                     
                 default:
                     break;
             }
         }];
        
        [self presentViewController:mySLComposerSheet animated:YES completion:nil];
    }
    else
    {
       // [facebookButton setImage:[UIImage imageNamed:@"facebook_Img.png"] forState:UIControlStateNormal];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"No Facebook Accounts" message:@"For some reason, Facebook is not recognizing Markidplace. To fix, please tap your phone's Settings icon, scroll down to Facebook, where you should see your Facebook login credentials (if you don't, you need to log in) and there you should see a Markidplace icon also (if you don't, you need to refresh the permissions again). Toggle Markidplace to the \"ON\" position and you should be connected." delegate:self                                                       cancelButtonTitle:@"OK"  otherButtonTitles:nil];
       // alert.tag = 11;
        [alert show];
        
    }
    });
    
    
}

-(void)twitterposting
{
    if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter]) {
        
        SLComposeViewController *mySLComposerSheet = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
        NSString *imgurlstr;
        if (sltobj.picturearray.count>0)
        {
            id inputid=[sltobj.picturearray objectAtIndex:0];
            
            
            if ([inputid isKindOfClass:[NSString class]])
            {
                
                imgurlstr=[sltobj.picturearray objectAtIndex:0];
                
               // NSString *descrpstr=sltobj.descriptionstr;
                
            }
        }
        //NSString *messagetext=[NSString stringWithFormat:@"<html xmlns=\"http://www.w3.org/1999/xhtml\"><head> <meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" /><title>Untitled Document</title></head>  <body><a href=\"%@watch/f/%@\"> <img src=\"%@\" width=\"568\" height=\"568\"></a> </body> </html>",ShareUrl,popularobj.bloopid,mailimgUrl];
        
        //NSString *sendText=@"<html><body><p>Check out my new listing on marKIDplace, THE app to buy and sell children's clothing, gear, and maternity wear.<a href=\"http://www.itunes.com\">#markidplace</a></p></body></html>";
        
        //NSString *sendText=@"Check out this awesome item I found on marKIDplace. Get it at : https://www.itunes.com";
        
        NSString *sendText=@"Check out my new listing on marKIDplace, THE app to buy and sell children's clothing and maternity wear. #markidplace";
        //[mySLComposerSheet setInitialText:sltobj.itemNamestr];
        [mySLComposerSheet setInitialText:sendText];
        
       
        NSURL *url = [NSURL URLWithString:imgurlstr];
        NSData *data = [NSData dataWithContentsOfURL:url];
         UIImage *image = [UIImage imageWithData:data];
               
        
        [mySLComposerSheet addImage:image];
        
        //  [mySLComposerSheet addURL:[NSURL URLWithString:@"http://stackoverflow.com/questions/12503287/tutorial-for-slcomposeviewcontroller-sharing"]];
        
        [mySLComposerSheet setCompletionHandler:^(SLComposeViewControllerResult result)
         {
             
             switch (result)
             {
                 case SLComposeViewControllerResultCancelled:
                     //   NSLog(@"Post Canceled");
                     
                 ///    [twitterButton setImage:[UIImage imageNamed:@"twitter_Img.png"] forState:UIControlStateNormal];
                     break;
                 case SLComposeViewControllerResultDone:
                     // NSLog(@"Post Sucessful");
                   //  [twitterButton setImage:[UIImage imageNamed:@"twitter_blue_Img.png"] forState:UIControlStateNormal];
                   //  UIAlertView *sharealert = [[UIAlertView alloc] initWithTitle:@"Twitter" message:@"Successfully posted" delegate:nil                                                       cancelButtonTitle:@"OK" otherButtonTitles:nil];
                    // [sharealert show];
                     break;
                     
                 default:
                     break;
             }
         }];
        
        [self presentViewController:mySLComposerSheet animated:YES completion:nil];
    }
    else
    {
       // [twitterButton setImage:[UIImage imageNamed:@"twitter_Img.png"] forState:UIControlStateNormal];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"No Twitter Accounts" message:@"For some reason, Twitter is not recognizing marKIDplace. To fix, please tap your phone's Settings icon, scroll down to Twitter, where you should see your Twitter login credentials (if you don't, you need to log in) and there you should see a marKIDplace icon also (if you don't, you need to refresh the permissions again). Toggle marKIDplace to the \"ON\" position and you should be connected." delegate:self                                                       cancelButtonTitle:@"OK"  otherButtonTitles:nil];
        alert.tag = 12;
         [alert show];
        
    }
    
    
}
-(void)pinterestposting
{
    {
        // pinterest
        
        if (sltobj.picturearray.count>0)
        {
            id inputid=[sltobj.picturearray objectAtIndex:0];
            
            
            if ([inputid isKindOfClass:[NSString class]])
            {
                
                
                NSString *pinurl=[sltobj.picturearray objectAtIndex:0];
                
                
                [_pinterest createPinWithImageURL:[NSURL URLWithString:pinurl]
                                        sourceURL:[NSURL URLWithString:MainUrl]
                                      description:@"Pinning from Pin It Demo"];
            }
            else{
                [AppDelegate alertmethod:appname :@"unable to share on Pinterest. please try after some time"];
            }
        }
        else{
            [AppDelegate alertmethod:appname :@"unable to share on Pinterest. please try after some time"];
        }
    }
}
-(void)successResponseforsold:(NSDictionary *)responseDict
{
     dispatch_async(dispatch_get_main_queue(), ^{
    NSLog(@"the dict is %@",responseDict);
         [APPDELEGATE stopspinner:self.view];
    if([[responseDict valueForKey:@"message"] isEqualToString:@"Update Forsale Sucessfully"])
    {
        UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"Thank You" message:@"Your product has been processed as Sold item." delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK",nil];
        [alert setTag:15];
        [alert show];
    }
     });
}
-(void)internetConnection
{
    UIAlertView *Internetalert=[[UIAlertView alloc]initWithTitle:appname message:@"Internet connection not available" delegate:self cancelButtonTitle:nil otherButtonTitles:@"Ok", nil];
    [Internetalert show];
    [appdelegate stopspinner:self.view];
    
}

-(void)attemptRenewCredentials
{
    [self.fbaccountStore renewCredentialsForAccount:(ACAccount *)fbaccount completion:^(ACAccountCredentialRenewResult renewResult, NSError *error){
        if(!error)
        {
            switch (renewResult)
            {
                case ACAccountCredentialRenewResultRenewed:
                    [self facebookButtonAction:self];
                    break;
                case ACAccountCredentialRenewResultRejected:
                    [self attemptRenewCredentials];
                    break;
                case ACAccountCredentialRenewResultFailed:
                    [self attemptRenewCredentials];
                    break;
                default:
                    break;
            }
            
        }
        else
        {
            [self facebookButtonAction:self];
        }
    }];
}


-(void)facebookPosting
{
    NSArray *permissions =
    [NSArray arrayWithObjects:@"email",@"publish_actions", nil];
    NSDictionary * dict=[NSDictionary dictionaryWithObjectsAndKeys:@"770236303030542",ACFacebookAppIdKey,permissions,ACFacebookPermissionsKey,ACFacebookAudienceEveryone,ACFacebookAudienceKey, nil];
    
    fbaccountStore = [[ACAccountStore alloc]init];
    ACAccountType *accountType = [fbaccountStore accountTypeWithAccountTypeIdentifier:ACAccountTypeIdentifierFacebook];
    
    [fbaccountStore requestAccessToAccountsWithType:accountType options:dict completion:^(BOOL granted, NSError *error)
     {
         if (granted == YES)
         {
             
             NSArray *accountsArray = [fbaccountStore accountsWithAccountType:accountType];
             
             ////NSLog(@"accountsArray =====%@",accountsArray);
             
             if ([accountsArray count] > 0)
             {
                 fbaccount = [accountsArray objectAtIndex:0];
                 
                 //  NSLog(@"aaaaaaaaaaaa%@",fbaccount);
                 
                 ACAccountCredential *fbCredential = [fbaccount credential];
                 
                 acessToken = [NSString stringWithFormat:@"%@",fbCredential.oauthToken];
                 
                 // NSLog(@"acessToken   %@",acessToken);
                 
                 NSDictionary *parameters = @{@"access_token": acessToken};
                 
                 NSURL *requestURL = [NSURL URLWithString:@"https://graph.facebook.com/me"];
                 
                 SLRequest *postRequest = [SLRequest requestForServiceType:SLServiceTypeFacebook requestMethod:SLRequestMethodGET URL:requestURL parameters:parameters];
                 
                 postRequest.account = fbaccount;
                 
                 [postRequest performRequestWithHandler:
                  ^(NSData *responseData, NSHTTPURLResponse
                    *urlResponse, NSError *error)
                  {
                      NSDictionary *dataSourceDict= [NSJSONSerialization JSONObjectWithData:responseData options:NSJSONReadingMutableLeaves  error:&error];
                      
                      if([dataSourceDict objectForKey:@"error"]!=nil)
                      {
                          [self attemptRenewCredentials];
                      }
                      else
                      {
                          facebookID = dataSourceDict[@"id"];
                          
                          //  NSLog(@"facebookID %@",facebookID);
                          
                          
                          [self performSelector:@selector(shareInFB) withObject:nil];
                      }
                  }];
             }
         }
         else
         {
             UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"No Facebook Accounts" message:@"For some reason, Facebook is not recognizing Markidplace. To fix, please tap your phone's Settings icon, scroll down to Facebook, where you should see your Facebook login credentials (if you don't, you need to log in) and there you should see a Markidplace icon also (if you don't, you need to refresh the permissions again). Toggle Markidplace to the \"ON\" position and you should be connected." delegate:self                                                       cancelButtonTitle:@"OK"  otherButtonTitles:nil];
             alert.tag = 11;
             [alert show];
         }
     }];
    
    
    
    
}

-(void)shareInFB
{
    dispatch_async(dispatch_get_main_queue(), ^{
        
                // NSLog(@"image posting");
            NSURL *url = [NSURL URLWithString:@"https://graph.facebook.com/me/feed"];//feed
            
            asiRequest = [ASIFormDataRequest requestWithURL:url];
            
            [asiRequest setRequestMethod:@"POST"];
            
           // UIImage *fbImg=[UIImage imageWithData:appdelegate.capturedImgData];
        
        NSURL *imageURL = [NSURL URLWithString:imgurlstr];
        NSData *imageData = [NSData dataWithContentsOfURL:imageURL];
        UIImage *fbImg = [UIImage imageWithData:imageData];


        
            //  NSLog(@"fbImg ===%@",fbImg);
            //  [asiRequest setPostValue:@"video/quicktime" forKey:@"contentType"];
            
            [asiRequest setPostValue:UIImagePNGRepresentation(fbImg) forKey:@"picture"];
            [asiRequest setPostValue:descrpstr forKey:@"message"];
        // NSString *link =[NSString stringWithFormat:@"%@",@"https://www.google.co.in/?gfe_rd=cr&ei=sf-OU6rULoyBoAOk8YHADg"];
        
        //[asiRequest setPostValue:link forKey:@"link"];
        [asiRequest setPostValue:acessToken  forKey:@"access_token"];
        [asiRequest setPostValue:@"770236303030542"  forKey:@"app_id"];
        // [asiRequest setPostValue:@"testing" forKey:@"name"];
        // [asiRequest setPostValue:[NSString stringWithFormat:@"%@",@"https://s3-us-west-2.amazonaws.com/Markidplace-akiajjcm7taqsdtarnda/Markidplace06-03-2014-12-14-PM.jpeg"] forKey:@"picture"];
        
        [asiRequest setDelegate:self];
        [asiRequest startAsynchronous];
    });
    
}

-(void)requestFinished:(ASIHTTPRequest *)request
{
    dispatch_async(dispatch_get_main_queue(), ^{
        
        NSError *error1 = [request error];
        // NSLog(@"error is %@",[error1 description]);
        if (!error1)
        {
            NSString *response = [request responseString];
            NSMutableDictionary *mainDict = [response JSONValue];
            
            //  NSLog(@"main dict is %@",mainDict);
            
            
            if ([mainDict[@"id"] length]>0)
            {
                // NSLog(@"main dict is %@",mainDict[@"id"]);
            }
        }
    });
}
-(void)likeBackAction
{
    [self.navigationController popViewControllerAnimated:YES];
}
/*
/////Markidplace facebookcode
-(IBAction)facebookButtonAction:(id)sender
{
    UIButton *tempbtn=(UIButton *)sender;
    
    if([SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook])
    {
        self.fbSubView.frame=CGRectMake(0, 0, 320, 568);
        
        [self.view addSubview:fbSubView];
        NSString *imgurlstr;
        NSString *descrpstr;
        
        if (sltobj.picturearray.count>0)
        {
            id inputid=[sltobj.picturearray objectAtIndex:0];
            
            
            if ([inputid isKindOfClass:[NSString class]])
            {
                
               imgurlstr=[sltobj.picturearray objectAtIndex:0];
                
                descrpstr=sltobj.descriptionstr;
            }
        }
        

      //  NSString *str=[NSString stringWithFormat:@"%@/b/%@?fbrefresh=anything",MainUrl,popularobj.bloopid];
        
       // NSString *displayStr=[NSString stringWithFormat:@"%@",str];
        self.fbtextView.text=descrpstr;
        
      //  self.fbImgView.image=[UIImage imageWithData:fbImagedata];
      //  self.fbImgView.image=[UIImage imageNamed:[NSURL URLWithString:imgurlstr]];
        
       
        NSData *imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:imgurlstr]];
        self.fbImgView.image = [UIImage imageWithData:imageData];
        
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"No Facebook Accounts"message:@"For some reason, Facebook is not recognizing Markidplace. To fix, please tap your phone's Settings icon, scroll down to Facebook, where you should see your Facebook login credentials (if you don't, you need to log in) and there you should see a Markidplace icon also (if you don't, you need to refresh the permissions again). Toggle Markidplace to the \"ON\" position and you should be connected."
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        
        [alert show];
        
        
    }
    
    
    
}
- (void)uploadtofacebook
{
    
    // [self showIndicator];
    
    if (fbtextView.text.length == 0 || [fbtextView.text isEqualToString:@""])
    {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Markidplace" message:@"Please enter some text" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }
    else
    {
        NSString *imgurlstr;
        NSString *descrpstr;
        if (sltobj.picturearray.count>0)
        {
            id inputid=[sltobj.picturearray objectAtIndex:0];
            
            
            if ([inputid isKindOfClass:[NSString class]])
            {
                
                imgurlstr=[sltobj.picturearray objectAtIndex:0];
                
                descrpstr=sltobj.descriptionstr;
            }
        }

        NSURL *url = [NSURL URLWithString:@"https://graph.facebook.com/me/feed"];//photos
        asiRequest = [ASIFormDataRequest requestWithURL:url];
        
       // NSString *str=[NSString stringWithFormat:@"%@/b/%@?fbrefresh=anything",MainUrl,popularobj.bloopid];
        
        [asiRequest setRequestMethod:@"POST"];
        
        [asiRequest setPostValue:fbtextView.text forKey:@"message"];
        [asiRequest setPostValue:@"https://www.myappdemo.com/downloads/markidplace.php" forKey:@"link"];
        [asiRequest setPostValue:acessToken forKey:@"access_token"];
        [asiRequest setPostValue:descrpstr forKey:@"name"];
        
        [asiRequest setPostValue:[NSString stringWithFormat:@"%@",imgurlstr] forKey:@"picture"];
        
        [asiRequest setDelegate:self];
        [asiRequest startAsynchronous];
    }
    
    
    
}
-(void)attemptRenewCredentials
{
    [self.accountStore renewCredentialsForAccount:(ACAccount *)account completion:^(ACAccountCredentialRenewResult renewResult, NSError *error){
        if(!error)
        {
            switch (renewResult)
            {
                case ACAccountCredentialRenewResultRenewed:
                    //////NSLog(@"Good to go");
                    [self facebookButtonAction:self];
                    break;
                case ACAccountCredentialRenewResultRejected:
                    //////NSLog(@"User declined permission");
                    [self attemptRenewCredentials];
                    break;
                case ACAccountCredentialRenewResultFailed:
                    //////NSLog(@"non-user-initiated cancel, you may attempt to retry");
                    [self attemptRenewCredentials];
                    break;
                default:
                    break;
            }
            
        }
        else{
            //handle error
            //////NSLog(@"error from renew credentials%@",error);
            [self facebookButtonAction:self];
            // [self attemptRenewCredentials];
        }
    }];
}
//facebookpostmethod
-(IBAction)fbPostAction:(id)sender
{
    self.fbSubView.userInteractionEnabled=NO;
    self.view.userInteractionEnabled=NO;
    NSArray *permissions =
    [NSArray arrayWithObjects:@"email", @"read_stream",@"publish_stream", nil];
    NSDictionary * dict=[NSDictionary dictionaryWithObjectsAndKeys:@"770236303030542",ACFacebookAppIdKey,permissions,ACFacebookPermissionsKey,ACFacebookAudienceEveryone,ACFacebookAudienceKey, nil];
    
    accountStore = [[ACAccountStore alloc]init];
    ACAccountType *accountType = [accountStore accountTypeWithAccountTypeIdentifier:ACAccountTypeIdentifierFacebook];
    NSLog(@"the accountType%@",accountType);
    
    [accountStore requestAccessToAccountsWithType:accountType options:dict completion:^(BOOL granted, NSError *error)
     {
         if (granted == YES)
         {
             NSArray *accountsArray = [accountStore accountsWithAccountType:accountType];
             
             //////NSLog(@"accountsArray =====%@",accountsArray);
             
             if ([accountsArray count] > 0)
             {
                 NSArray *accountsArray = [accountStore accountsWithAccountType:accountType];
                 
                 //  //////NSLog(@"accountsArray =====%@",accountsArray);
                 
                 if ([accountsArray count] > 0)
                 {
                     account = [accountsArray objectAtIndex:0];
                     
                     
                     ACAccountCredential *fbCredential = [account credential];
                     
                     acessToken = [NSString stringWithFormat:@"%@",fbCredential.oauthToken];
                     
                     
                     NSDictionary *parameters = @{@"access_token": acessToken};
                     
                     NSURL *requestURL = [NSURL URLWithString:@"https://graph.facebook.com/me"];
                     
                     SLRequest *postRequest = [SLRequest requestForServiceType:SLServiceTypeFacebook requestMethod:SLRequestMethodGET URL:requestURL parameters:parameters];
                     
                     postRequest.account = account;
                     
                     [postRequest performRequestWithHandler:
                      ^(NSData *responseData, NSHTTPURLResponse
                        *urlResponse, NSError *error)
                      {
                          
                          //  //////NSLog(@"response ======%@",urlResponse);
                          
                          NSDictionary *dataSourceDict= [NSJSONSerialization JSONObjectWithData:responseData options:NSJSONReadingMutableLeaves  error:&error];
                          
                          //////NSLog(@"dataSourceDict =====%@",dataSourceDict);
                          
                          if([dataSourceDict objectForKey:@"error"]!=nil)
                          {
                              [self attemptRenewCredentials];
                          }
                          else
                          {
                              [self uploadtofacebook];
                              
                          }
                          
                      }];
                 }
             }
         }
         else
         {
             UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"No Facebook Accounts" message:@"For some reason, Facebook is not recognizing Markidplace. To fix, please tap your phone's Settings icon, scroll down to Facebook, where you should see your Facebook login credentials (if you don't, you need to log in) and there you should see a Markidplace icon also (if you don't, you need to refresh the permissions again). Toggle Markidplace to the \"ON\" position and you should be connected." delegate:self                                                       cancelButtonTitle:@"OK"  otherButtonTitles:nil];
             alert.tag = 11;
             self.fbSubView.userInteractionEnabled=YES;
             self.view.userInteractionEnabled=YES;
             [alert show];
         }
     }];
}
 */

@end
