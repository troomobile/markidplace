//
//  myAddressCustomcell.h
//  MarkidPlace
//
//  Created by Varma Bhupatiraju on 17/09/14.
//  Copyright (c) 2014 stellentmac1. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface myAddressCustomcell : UITableViewCell
@property(nonatomic,strong)IBOutlet UILabel *namelbl;
@property(nonatomic,strong)IBOutlet UIButton *updatebtn;
@property(nonatomic,strong)IBOutlet UITextField *addresstxtfield;
@property(nonatomic,strong) IBOutlet UIImageView *lineimg;
@end
