//
//  ChangePasswordViewController.h
//  MarkidPlace
//
//  Created by Varma Bhupatiraju on 11/09/14.
//  Copyright (c) 2014 stellentmac1. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ChangePasswordCustomCell.h"
#import "ProfileRepository.h"
@class AppDelegate;
@interface ChangePasswordViewController : UIViewController<UINavigationBarDelegate,UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate,HelperProtocol>
{
     AppDelegate*appdelegate;
    ProfileRepository *profileRepos;
    UITextField *Currenttextfield;
}
@property(nonatomic,strong)IBOutlet NSMutableArray *firstarray;
@property(nonatomic,strong)IBOutlet NSMutableArray *secondarry;
@property(nonatomic,strong)IBOutlet UITableView *firsttableview;
@property(nonatomic,strong)IBOutlet UILabel *Changepasswordtitlelbl;
@property(nonatomic,strong)IBOutlet ChangePasswordCustomCell *myChangePasswordCustomCell;
@property(nonatomic,strong) id<HelperProtocol>delegate;
@property (nonatomic,strong) IBOutlet UIButton *updatebtn;
@property (nonatomic,strong) IBOutlet UINavigationBar *navbar;

-(IBAction)backAction:(id)sender;
-(IBAction)homeBtnAction:(id)sender;
-(IBAction)updatebtnAction:(id)sender;
@end
