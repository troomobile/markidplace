//
//  BuyandSellViewController.h
//  MarkidPlace
//
//  Created by Stellent Software on 5/31/14.
//  Copyright (c) 2014 stellentmac1. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "RegistrationRepository.h"
#import "ItemsModelobject.h"
#import "HelperProtocol.h"
#import "BuyandSellTableViewCell.h"
#import "OrderModelObject.h"
#import "NotificationViewController.h"
#import "FAQViewController.h"
@class RegistrationRepository;
@class AppDelegate;

@interface BuyandSellViewController : UIViewController <UITableViewDelegate,UITableViewDataSource,HelperProtocol,UISearchBarDelegate>

{
    CGRect screenSize;
  //  AppDelegate *appdelegate;
    RegistrationRepository *registrationRep;
    
    
     IBOutlet UILabel *titlelbl;
    NSMutableArray *tablearray;

}

@property(nonatomic,readwrite)BOOL isbuyer;


@property (nonatomic,strong) IBOutlet UINavigationBar *navbar;
@property(nonatomic,strong)IBOutlet UITableView *buyandsellTbl;
@property(nonatomic,strong) NSString *buystring;
@property (nonatomic,strong) IBOutlet BuyandSellTableViewCell *buycell;
@property(nonatomic,strong) IBOutlet NSMutableArray *duplicatearray;
@property(nonatomic,strong)IBOutlet UISearchBar *mysearchbar;
-(IBAction)backbtnaction:(id)sender;
-(IBAction)homebtnaction:(id)sender;
-(IBAction)notificationclicked:(id)sender;



@end
