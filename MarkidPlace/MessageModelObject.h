//
//  MessageModelObject.h
//  messagetesting
//
//  Created by Varma Bhupatiraju on 09/06/14.
//  Copyright (c) 2014 Varma Bhupatiraju. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MessageModelObject : NSObject
@property(nonatomic,strong) NSString *namestring;
@property(nonatomic,strong) NSString *textstring;
@property(nonatomic,strong) NSString *imagestring;
@property(nonatomic,strong) NSString *timestring;

@end
