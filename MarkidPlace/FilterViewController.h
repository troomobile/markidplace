//
//  FilterViewController.h
//  MarkidPlace
//
//  Created by stellentmac1 on 5/21/14.
//  Copyright (c) 2014 stellentmac1. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import "HomeViewController.h"
#import "RegistrationRepository.h"
#import "AppDelegate.h"
#import "HelperProtocol.h"
#import "LookupModelobject.h"
#import "FilterCell.h"
#import "FilterObject.h"
#import "HelperProtocol.h"




@class RegistrationRepository;
@class AppDelegate;
@interface FilterViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,HelperProtocol,UISearchBarDelegate,NSURLConnectionDelegate,UITextFieldDelegate,CLLocationManagerDelegate>
{
    IBOutlet UILabel *titlelbl;
    IBOutlet UINavigationBar *navbar;
  //  IBOutlet UITextField *subCategoryTextField;
  //  AppDelegate *appdelegate;
    
       RegistrationRepository *registrationRep;
    BOOL issize;
    BOOL iscategory;
    BOOL isbrand;
     BOOL isprice;
    BOOL isdistance;
    BOOL isSearch;
    BOOL isApply;
    UITextField *currentTextField;
    int ival;
    
    NSString *sizestr;
    NSString *pricestr;
    NSString *genderstr;
    NSString *categorystr;
    NSString *brandstr;
    NSString *distancestr;
    NSString *latitudestr;
    NSString *statestr;
    NSString *longitudestr;
    NSMutableArray *listbrandArray;

    BOOL isdropdown;
    
    BOOL isothercat;
    NSURLConnection *conn;
    NSString *parentid;
    NSMutableArray *pricearray;
    CLLocationManager *locationManager;

    //NSMutableArray *genderarray;
}
@property (nonatomic,strong) NSMutableArray *filterarray;

@property (nonatomic,strong) IBOutlet FilterCell *filtercell;

@property (nonatomic,strong) IBOutlet UILabel *poptitlelable;
@property (nonatomic,strong) IBOutlet UILabel *genderlbl;
@property (nonatomic,strong) IBOutlet UIButton *gendercancelbtn;
@property (nonatomic,strong) IBOutlet UIButton *cancelbtn;

-(IBAction)cancelBtnAction:(id)sender;
@property (nonatomic,strong) IBOutlet UIView *popview;


@property(nonatomic,strong)NSMutableData *responseData;
@property(nonatomic,strong)NSMutableURLRequest *request;


@property (nonatomic,strong) IBOutlet UITableView *dropdownview;
@property (nonatomic,strong) IBOutlet UISearchBar *searchbar;


/////////
@property (nonatomic,strong) IBOutlet UITableView *filterlistview;
@property (nonatomic,strong) IBOutlet UIView *buttonview;
@property(nonatomic,strong) IBOutlet UIView  *genderpopview;
@property(nonatomic,strong) IBOutlet UITableView  *genderdropdownview;

-(IBAction)backAction:(id)sender;
-(IBAction)homeBtnAction:(id)sender;
-(IBAction)gendercancelbtnaction:(id)sender;
-(IBAction)filterBtnAction:(id)sender;

-(IBAction)applyBtnAction:(id)sender;

@end
