//
//  MySizesViewController.h
//  MarkidPlace
//
//  Created by stellent on 22/09/14.
//  Copyright (c) 2014 stellentmac1. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HelperProtocol.h"
#import "ODRefreshControl.h"
#import "AppDelegate.h"
@interface MySizesViewController : UIViewController <HelperProtocol,UIScrollViewDelegate>
{
    
 
    ODRefreshControl *refreshControl;
    NSMutableArray *sizesMArray;
   
    
    
    
}
@property (nonatomic,strong) IBOutlet UINavigationBar *navbar;
@property (nonatomic,strong) IBOutlet UILabel *titlelbl;
@property (nonatomic,strong) IBOutlet UILabel *mysizeslbl;
@property (nonatomic,strong) IBOutlet UIScrollView *myScrollView;
@property (nonatomic,strong) IBOutlet UIButton *updateButton;
-(IBAction)backbtnAction:(id)sender;
-(IBAction)sizesButton:(id)sender;
-(IBAction)updateButtonClicked:(id)sender;
-(IBAction)homebtnaction:(id)sender;
@end
