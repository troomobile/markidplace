//
//  AppDelegate.m
//  MarkidPlace
//
//  Created by stellentmac1 on 5/21/14.
//  Copyright (c) 2014 stellentmac1. All rights reserved.
//

#import "AppDelegate.h"


@implementation AppDelegate
@synthesize isiphone5,loginvc,naviCon;
@synthesize deviceTokenString,suggestionobj;
@synthesize customactivityObj,islistusers;
@synthesize catogoryGlobalarray,sizeGlobalarray,distanceGlobalarray,conditionGlobalarray,brandGlobalarray,cartGlobalarray,genderglobalaray;
@synthesize suggestionviewcont,emailArray,contactsArray,suggestionslistArray,Twitterfollowers;
@synthesize homeviewcont;

@synthesize useridstring,editprofilevc;

@synthesize s3Clientobject ;



@synthesize suggestonsGlobalarray,userlistArray;
@synthesize mylikesGlobalarray;
@synthesize myfeedsGlobalarray;
@synthesize myclosetGlobalarray;
@synthesize allLikesGlobalarray,horseanimationArrray,filterbrandGlobalarray;

@synthesize managedcontext,managedobjectmodel,persistancestore,coreDataContactsAray,newlyAddedContactsAray;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    [BugSenseController sharedControllerWithBugSenseAPIKey:@"27fb365a"];
    registrationRep=[[RegistrationRepository alloc]init];
    registrationRep.delegate=self;
      [NSThread sleepForTimeInterval:3.0];
    
customactivityObj=[[CustomActivity alloc]init];
    
  //  cartglobalimage=[UIImage new];
    
    suggestonsGlobalarray=[NSMutableArray new];
    mylikesGlobalarray=[NSMutableArray new];
    myfeedsGlobalarray=[NSMutableArray new];
    myclosetGlobalarray=[NSMutableArray new];
    allLikesGlobalarray=[NSMutableArray new];
    genderglobalaray=[NSMutableArray new];
  filterbrandGlobalarray=[NSMutableArray new];
    userlistArray=[NSMutableArray new];
    emailArray=[NSMutableArray new];
    contactsArray=[NSMutableArray new];
    suggestionslistArray=[NSMutableArray new];
    Twitterfollowers=[NSMutableArray new];

    NSString *string=[self getStringFromPlist:@"Twitter"];
    if([self isnullorempty: string])
    {
        [self insertStringInPlist:@"Twitter" value:@"true"];
    }
    NSString *fbstring=[self getStringFromPlist:@"fb"];
    if([self isnullorempty: fbstring])
    {
        [self insertStringInPlist:@"fb" value:@"true"];
    }
   
   
    // pay pal setup    sandobox
    /*
 [PayPalMobile initializeWithClientIdsForEnvironments:@{PayPalEnvironmentProduction : @"AV0iiRCHe4qktl9gILTyhy47lsJvGBjL_36oZo-YE9pnTb3trpJBlCVrMV8X",
                                                       PayPalEnvironmentSandbox : @"AWRNKhBi9RUoRmXoihRw7-jgh4E_C9G0bQUreXRY6AOdZHd5MKy5QXrvnwFF"}];
     */

    
// this is paypalEnvironment production account
 //[PayPalMobile initializeWithClientIdsForEnvironments:@{PayPalEnvironmentProduction : @"AV0iiRCHe4qktl9gILTyhy47lsJvGBjL_36oZo-YE9pnTb3trpJBlCVrMV8X",
                                                         //PayPalEnvironmentSandbox : @"ATl09BAdvedq9iaw0nExObllROimTx99FHO5Rof9EZT6TWSoqtxQpc7h3GyR"}];
   

    catogoryGlobalarray=[NSMutableArray new];
    conditionGlobalarray=[NSMutableArray new];
    sizeGlobalarray=[NSMutableArray new];
    brandGlobalarray=[NSMutableArray new];
    cartGlobalarray=[NSMutableArray new];
    distanceGlobalarray=[NSMutableArray new];
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    
    // Override point for customization after application launch.
    
    // amazone image post
#ifdef DEBUG
    [AmazonLogger verboseLogging];
#else
    [AmazonLogger turnLoggingOff];
#endif
    
    [AmazonErrorHandler shouldNotThrowExceptions];
    
    
    screenSize = [[UIScreen mainScreen]bounds];
//    
//    if (screenSize.size.height == 568)
//    {
//        registerVC = [[RegisterViewController alloc]initWithNibName:@"RegisterViewController" bundle:nil];
//    }
//    else
//    {
//        registerVC = [[RegisterViewController alloc]initWithNibName:@"RegisterViewController_iphone4" bundle:nil];
//    }
//    
   // self.naviCon = [[UINavigationController alloc]initWithRootViewController:registerVC];
    useridstring=[self getStringFromPlist:@"userid"];
    NSString *usernamestr=[self getStringFromPlist:usernamekey];
    
    NSString *realname=[self getStringFromPlist:@"realname"];
    
    if ([self isnullorempty:useridstring] && [self isnullorempty:usernamestr ] && [self isnullorempty:realname ])
    {
        if (screenSize.size.height == 568)
        {
            isiphone5=YES;
            loginvc = [[LoginViewController alloc]initWithNibName:@"LoginViewController" bundle:nil];
        }
        else
        {
            isiphone5=NO;
            loginvc = [[LoginViewController alloc]initWithNibName:@"LoginViewController_iphone4" bundle:nil];
        }
           self.naviCon = [[UINavigationController alloc]initWithRootViewController:loginvc];
    }
    else  if (![self isnullorempty:useridstring] && ![self isnullorempty:usernamestr ] &&  ![self isnullorempty:usernamestr ])
    {
        if (screenSize.size.height == 568)
        {
            isiphone5=YES;
            suggestionviewcont = [[SuggestionsViewController alloc]initWithNibName:@"SuggestionsViewController" bundle:nil];
            
        }
        else
        {
            isiphone5=NO;
            suggestionviewcont = [[SuggestionsViewController alloc]initWithNibName:@"SuggestionsViewController_iphone4" bundle:nil];
        }
            suggestionviewcont.isItFirstTime=YES;
           self.naviCon = [[UINavigationController alloc]initWithRootViewController:suggestionviewcont];
    }
    else  if ((![self isnullorempty:useridstring] && [self isnullorempty:usernamestr ]) || (![self isnullorempty:useridstring]&& [self isnullorempty:usernamestr ]))
    {
        if (screenSize.size.height == 568)
        {
            isiphone5=YES;
            editprofilevc = [[EditProfileViewController alloc]initWithNibName:@"EditProfileViewController" bundle:nil];
        }
        else
        {
            isiphone5=NO;
            editprofilevc = [[EditProfileViewController alloc]initWithNibName:@"EditProfileViewController_iphone4" bundle:nil];
        }
        editprofilevc.isnew=YES;
        self.naviCon = [[UINavigationController alloc]initWithRootViewController:editprofilevc];
    }

    dispatch_async(dispatch_get_main_queue(), ^{
        
 
        if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
            //NSLog(@"registerUserNotificationSettings in ios8.0==========================");
            UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:(UIRemoteNotificationTypeBadge
                                                                                                 |UIRemoteNotificationTypeSound
                                                                                                 |UIRemoteNotificationTypeAlert) categories:nil];
            [[UIApplication sharedApplication] registerUserNotificationSettings:settings];
        }
            
            //NSLog(@"registerUserNotificationSettings in ios7.0==========================");
            //********************** For Push Notification *********************
            [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
            [[UIApplication sharedApplication] registerForRemoteNotificationTypes:(UIRemoteNotificationTypeAlert | UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeSound)];
            //********************** For Push Notification *********************
      
        //********************** For Push Notification *********************
    });
    //Krish
    //[self dealWithPhoneBookContacts];
    self.naviCon.navigationBarHidden=YES;
    [self.window setRootViewController:self.naviCon];
    //self.window.rootViewController = self.naviCon;
    self.window.backgroundColor = [UIColor whiteColor];
    [self.window makeKeyAndVisible];
    return YES;
}


#pragma mark plist insert

// save to plist
-(void)insertStringInPlist:(NSString *)key value:(NSString *)value
{
    BOOL success;
    NSError *error;
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    NSString *filePath = [documentsDirectory stringByAppendingPathComponent:@"Data.plist"];
    success = [fileManager fileExistsAtPath:filePath];

    
    if (!success)
    {
        NSString *path = [[NSBundle mainBundle] pathForResource:@"Data" ofType:@"plist"];
        
        success = [fileManager copyItemAtPath:path toPath:filePath error:&error];
    }
    
    NSMutableDictionary* plistDict = [NSMutableDictionary dictionaryWithContentsOfFile:filePath];
    
    [plistDict setValue:value forKey:key];
    [plistDict writeToFile:filePath atomically: YES];
    
    //NSLog(@"the plist data is %@",[plistDict valueForKey:key]);
}

#pragma mark plist get
// get data from plist
-(NSString *)getStringFromPlist:(NSString *)key
{
    BOOL success;
    NSError *error;
    
    NSString *value;
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *filePath = [documentsDirectory stringByAppendingPathComponent:@"Data.plist"];
    
    success = [fileManager fileExistsAtPath:filePath];
    if (!success)
    {
        NSString *path = [[NSBundle mainBundle] pathForResource:@"Data" ofType:@"plist"];
        success = [fileManager copyItemAtPath:path toPath:filePath error:&error];
    }
    
    NSMutableDictionary* plistDict = [NSMutableDictionary dictionaryWithContentsOfFile:filePath];
    value=[plistDict valueForKey:key];
    
    //NSLog(@"value:=====%@",value);
    
    return value;
}

#pragma mark - Push Notification

-(void)application:(UIApplication *)app didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken
{

    NSString *dt = [[deviceToken description] stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"<>"]];
    dt = [dt stringByReplacingOccurrencesOfString:@" " withString:@""];
    self.deviceTokenString= dt;

     //NSLog(@"@@@@@@@@@@ device token is@@@@@@ %@",self.deviceTokenString);
 //testing
}
- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
{
    /*
    {
        aps =     {
            alert = "@shiv has favorited your item Test";
            badge = 1;
            sound = "sound.caf";
        };
    }
     */

UIApplicationState state = [application applicationState];
    
    //NSLog(@"userInfo-----------%@",userInfo);
    if (state == UIApplicationStateActive)
    {
        
        NSDictionary *theDict = [userInfo objectForKey:@"aps"];
        
        //  NSString *statusStr = [userInfo objectForKey:@"status"];
        
        //NSDictionary *alertDict = [theDict objectForKey:@"alert"];
        NSString *message=[theDict objectForKey:@"alert"];;
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:appname message:message delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
        
    }
    else
    {
    }

    /*
    UIApplicationState state = [application applicationState];
    if (state == UIApplicationStateActive)
    {
        NSString *message =@"";
        message = [userInfo objectForKey:appname];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:appname message:message delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
       // [alert show];
    }
    else
    {
        // Push Notification received in the background
    }
     */
 
}

#ifdef __IPHONE_8_0
- (void)application:(UIApplication *)application didRegisterUserNotificationSettings:(UIUserNotificationSettings *)notificationSettings
{
    //register to receive notifications
    [application registerForRemoteNotifications];
}

- (void)application:(UIApplication *)application handleActionWithIdentifier:(NSString *)identifier forRemoteNotification:(NSDictionary *)userInfo completionHandler:(void(^)())completionHandler
{
    //handle the actions
    if ([identifier isEqualToString:@"declineAction"]){
    }
    else if ([identifier isEqualToString:@"answerAction"]){
    }
}
#endif

- (void)application:(UIApplication *)app didFailToRegisterForRemoteNotificationsWithError:(NSError *)err
{
    //NSLog(@"Error in registration. Error: %@", err);

}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
       [timer invalidate];
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    NSString *useridStr=[self getStringFromPlist:@"userid"];
    
    if([self isnullorempty:useridStr])
    {
        
    }
    else
    {
        [self dealWithPhoneBookContacts];
    }
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    if([[self getStringFromPlist:@"userid"] intValue]>0)
    {
        timer=[NSTimer scheduledTimerWithTimeInterval:10 target:self selector:@selector(notificationcount11:) userInfo:nil repeats:YES];
    }
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];

    
    //Calling filter service here to update the
    
    
    
    registrationRep.delegate=self;
    [ registrationRep getdata:nil :@"getcatogery" :@"GET" withcount:@"0"];

    [FBAppCall handleDidBecomeActive];
    
}


- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication
         annotation:(id)annotation
{
    return [FBSession.activeSession handleOpenURL:url];
}

- (void)applicationWillTerminate:(UIApplication *)application
{
       [timer invalidate];
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}


// object null condition check
-(BOOL)isnullorempty:(id)sender
{
    if (sender ==nil || sender ==Nil || sender == NULL || [sender isKindOfClass:[NSNull class ]])
    {
        return YES;
    }
    else
    {
        if ([sender isKindOfClass:[NSString class]])
        {
            
            NSString *mysring=(NSString *)sender;
            
            if ([mysring isEqualToString:@"<null>" ] || [mysring isEqualToString:@"(null)" ] || [mysring isEqualToString:@"null" ] || mysring.length<1)
            {
                return YES;
            }
            else
            {
                
                mysring=[mysring stringByReplacingOccurrencesOfString:@" " withString:@""];
                
                if (mysring.length<1)
                {
                    return YES;
                }
                return NO;
            }
            
        }
        else
        {
            return NO;
        }
    }
    
    return NO;
    
}


// alert method to display
+(void)alertmethod :(NSString *)Titlestring :(NSString *)messagestring
{
    
    
    if (Titlestring ==nil || Titlestring == NULL || [Titlestring isKindOfClass:[NSNull class ]] || Titlestring.length<1)
    {
        Titlestring = appname;
    }
    
    if (messagestring ==nil || messagestring == NULL || [messagestring isKindOfClass:[NSNull class ]] || messagestring.length<1)
    {
        messagestring = @"Some error occor.";
    }
    
    UIAlertView *myalert=[[UIAlertView alloc]initWithTitle:Titlestring message:messagestring delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [myalert show];
}


// show spinner
-(void)startspinner :(UIView *)inview
{
        [customactivityObj stopactivity];
    inview.userInteractionEnabled=NO;
    [customactivityObj startactivity:inview];
}
// stop spinner
-(void)stopspinner :(UIView *)inview
{
    inview.userInteractionEnabled=YES;
    [customactivityObj stopactivity];
}


// generate unique id for amazone image post

+(NSString *)uniqueidstring
{
    
    
    NSUUID *uuid = [[NSUUID alloc] init];
    NSString *tempuniqstr=[uuid UUIDString];
    
   // NSLog(@"check it %@ ",tempuniqstr);
    tempuniqstr=[tempuniqstr lowercaseString];
    
   // NSLog(@"lowercasestr %@",tempuniqstr);
    
    tempuniqstr=[tempuniqstr stringByReplacingOccurrencesOfString:@"-" withString:@""];
   // NSLog(@"check the final str %@",tempuniqstr);
    
    return tempuniqstr;
    
    /// for random id
  /*  static NSString *letters = @"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXZY";
    static NSString *digits = @"0123456789";
    NSMutableString *passstr = [NSMutableString stringWithCapacity:8];
    for (NSUInteger i = 0; i < 2; i++) {
        uint32_t r;
        
        // Append 2 random letters:
        r = arc4random_uniform((uint32_t)[letters length]);
        [passstr appendFormat:@"%C", [letters characterAtIndex:r]];
        r = arc4random_uniform((uint32_t)[letters length]);
        [passstr appendFormat:@"%C", [letters characterAtIndex:r]];
        
        // Append 2 random digits:
        r = arc4random_uniform((uint32_t)[digits length]);
        [passstr appendFormat:@"%C", [digits characterAtIndex:r]];
        r = arc4random_uniform((uint32_t)[digits length]);
        [passstr appendFormat:@"%C", [digits characterAtIndex:r]];
        
    }
    
    return passstr;
   */
    
    
}

// font for nav title
+(UIFont *)navtitlefont{
    return [UIFont fontWithName:@"Monroe" size:18];
}
// font for heddings
+(UIFont *)heddingfont :(int)fontsize
{
    
    if (fontsize>0)
    {
        return  [UIFont fontWithName:@"Muli" size:fontsize];
    }
    else{
        return  [UIFont fontWithName:@"Muli" size:15];
    }

}

// screen size conditon
+(BOOL)isiphone5
{
    
 CGRect  iphonerect = [[UIScreen mainScreen]bounds];
    if (iphonerect.size.height == 568)
    {
        return YES;
    }
    else
    {
        return NO;
    }
    
}

// adjusting label size with width
+(UILabel *)lablefitToWidth:(UILabel *)mylabel :(float)maxwidth
{
    
    CGSize maswidsize = CGSizeMake(maxwidth,CGFLOAT_MAX);
    CGSize requiredSize2 = [mylabel sizeThatFits:maswidsize];
    CGRect labelFrame12 = mylabel.frame;
    labelFrame12.size.width = requiredSize2.width+5;
    mylabel.frame = labelFrame12;
    
    return mylabel;
}


+(void)cropimage:(id)mycont :(UIImage *)myimage
{
    RKCropImageController *cropController = [[RKCropImageController alloc] initWithImage:myimage];
    cropController.delegate = mycont;
    [mycont presentModalViewController:cropController animated:NO];
}


// parsing listing item
#pragma mark parse listing item
-(NSMutableArray *)parselistingitem :(id)respdict :(BOOL)islikesarray :(NSMutableArray *)likesarray :(BOOL)ismylikes
{
    
    //NSLog(@"check this array 1221212%@212122",likesarray);
    NSMutableArray *mylistingarray=[NSMutableArray new];
    
    if ([respdict isKindOfClass:[NSArray class]])
    {
        NSArray *resparry=(NSArray *)respdict;
        
        //NSLog(@"check the data getListing---%@ ",resparry);
        for (int i=0; i<resparry.count; i++)
        {
            
            NSDictionary *maindict=[resparry objectAtIndex:i];
            
            ItemsModelobject *itemsmodobj=[[ItemsModelobject alloc]init];
            itemsmodobj.itemNamestr=[maindict objectForKey:@"itemName"];
            
            itemsmodobj.itemidstr=[maindict objectForKey:@"unitPrice"];
            itemsmodobj.listingcoststr=[maindict objectForKey:@"unitPrice"];
            itemsmodobj.salecostcoststr=[maindict objectForKey:@"listPrice"];
            if ([maindict objectForKey:@"status"])
            {
                itemsmodobj.itemStatus=[maindict objectForKey:@"status"];

            }
            itemsmodobj.brandstr=[maindict objectForKey:@"brand"];
            itemsmodobj.conditionstr=[maindict objectForKey:@"condition"];
            
            
            if ( [self isnullorempty:[maindict objectForKey:@"otherCategory"] ])
            {
                itemsmodobj.catogrystr=[maindict objectForKey:@"categoryName"];
            }
            else
            {
                itemsmodobj.catogrystr=[maindict objectForKey:@"otherCategory"];
                itemsmodobj.catogryIdstr=[maindict objectForKey:@"categoryId"];
            }
            
            itemsmodobj.sizestr=[maindict objectForKey:@"size"];
            itemsmodobj.descriptionstr=[maindict objectForKey:@"description"];
            itemsmodobj.usernamestring=[maindict objectForKey:@"userName"];
            itemsmodobj.namestr=[maindict objectForKey:@"name"];
            
            itemsmodobj.useridstr=[maindict objectForKey:@"userID"];
            
            itemsmodobj.userlistingidstr=[NSString stringWithFormat:@"%@",[maindict valueForKey:@"userListingId"]];
            
            itemsmodobj.itemidstr=[maindict objectForKey:@"userItemId"];
            
            itemsmodobj.weightstring=[NSString stringWithFormat:@"%@",[maindict objectForKey:@"weight"]];
            
            itemsmodobj.genderstring=[maindict valueForKey:@"gender"];
            if ([self isnullorempty:itemsmodobj.genderstring])
            {
                itemsmodobj.genderstring=@"";
            }
            
            itemsmodobj.uspsbool=[[maindict valueForKey:@"usps"] boolValue];
            itemsmodobj.inpersonbool=[[maindict valueForKey:@"inPerson"] boolValue];
            //NSLog(@"itemsmodobj.inpersonbool ---------------> %d",itemsmodobj.inpersonbool);
            itemsmodobj.profilepicstr=[maindict valueForKey:@"profilepic"];
            itemsmodobj.citystring=[maindict valueForKey:@"city"];
            itemsmodobj.statestr=[maindict valueForKey:@"state"];
            
            if ([self isnullorempty:itemsmodobj.citystring])
            {
                itemsmodobj.citystring=@"";
            }
            
            if ([self isnullorempty:itemsmodobj.statestr])
            {
                itemsmodobj.statestr=@"";
            }
            
            
            NSArray *itempickarray=[maindict objectForKey:@"itemPictures"];
            itemsmodobj.picturearray=[NSMutableArray new];
            
            for (int j=0; j<itempickarray.count; j++)
            {
                NSDictionary *picdict=[itempickarray objectAtIndex:j];
                NSString *urlsrt=[picdict valueForKey:@"pictureCdnUrl"];
                urlsrt=[urlsrt stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
                // %20 NSURL *picutl=[NSURL URLWithString:[picdict valueForKey:@"pictureCdnUrl"]];
                
                [itemsmodobj.picturearray addObject:urlsrt];
            }
            
            if (islikesarray==YES && ismylikes==NO)
            {
                
            
            for (int lk=0; lk<likesarray.count; lk++)
            {
                NSString *prevlikedstr=[likesarray objectAtIndex:lk];
                NSString *listiddtr=itemsmodobj.userlistingidstr;
                if ([prevlikedstr isEqualToString:listiddtr ])
                {
                    itemsmodobj.likedbool=YES;
                    break;
                }
                
            }
            }
            else if (islikesarray == NO && ismylikes==YES)
            {
                itemsmodobj.likedbool=YES;
            }
            else
            {
                itemsmodobj.likedbool=NO;
            }
            
            [mylistingarray addObject:itemsmodobj];
         
            
        }
        
        
         return mylistingarray;
    }
    else
    {
        return nil;
    }
    
   
}

#pragma mark mamzone
-(void)configureAmazoneservics
{
    if([ACCESS_KEY_ID isEqualToString:@"AKIAJJCM7TAQSDTARNDA"]
       && self.s3Clientobject == nil)
    {
        // Initial the S3 Client.
        //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        // This sample App is for demonstration purposes only.
        // It is not secure to embed your credentials into source code.
        // DO NOT EMBED YOUR CREDENTIALS IN PRODUCTION APPS.
        // We offer two solutions for getting credentials to your mobile App.
        // Please read the following article to learn about Token Vending Machine:
        // * http://aws.amazon.com/articles/Mobile/4611615499399490
        // Or consider using web identity federation:
        // * http://aws.amazon.com/articles/Mobile/4617974389850313
        //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        s3Clientobject = [[AmazonS3Client alloc] initWithAccessKey:ACCESS_KEY_ID withSecretKey:SECRET_KEY] ;
        s3Clientobject.endpoint = [AmazonEndpoints s3Endpoint:US_WEST_2];
        
        // Create the picture bucket.
        S3CreateBucketRequest *createBucketRequest = [[S3CreateBucketRequest alloc] initWithName:PICTURE_BUCKET andRegion:[S3Region USWest2]] ;
        NSLog(@"check the request %@",createBucketRequest);
        S3CreateBucketResponse *createBucketResponse = [s3Clientobject createBucket:createBucketRequest];
        if(createBucketResponse.error != nil)
        {
            NSLog(@"Error: %@", createBucketResponse.error);
        }
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
    }
    else
    {
        // [self showAlertMessage:CREDENTIALS_ERROR_MESSAGE withTitle:CREDENTIALS_ERROR_TITLE];
        
    }

}

-(NSMutableArray *)postimagestoamazone :(BOOL)isbulk  photosaray:(NSMutableArray *)myphotosarray :(UIImage *)myimage
{
    NSMutableArray *mymutablearray=[NSMutableArray new];
    
    [self configureAmazoneservics];
    
    
  //  NSString *unkstr=[AppDelegate uniqueidstring];
    
    //   NSLog(@"check the uniqueidstr %@ and length %d",unkstr,unkstr.length);
    
    if (isbulk)
    {
        for (int i=0; i<myphotosarray.count; i++)
        {
            NSString *imagename=[AppDelegate uniqueidstring];
            
            NSString *videoNameString =  [NSString stringWithFormat:@"%@.jpeg",imagename];
            
            
            NSString *urlstring=[self prepareamazoneurl:videoNameString];
            
            [mymutablearray addObject:urlstring];
            
            S3PutObjectRequest *por = [[S3PutObjectRequest alloc] initWithKey:videoNameString   inBucket:PICTURE_BUCKET] ;
            NSLog(@"step 2");
            por.contentType = @"image/jpeg";
            NSLog(@"step 3");
            
            
            UIImage *imageThumb=myphotosarray[i];
            NSData   *thnmbData = UIImageJPEGRepresentation(imageThumb, 0.25);
            por.data        = thnmbData;
            NSLog(@"step 4");
            por.cannedACL   = [S3CannedACL publicRead];
            
            // Put the image data into the specified s3 bucket and object.
            S3PutObjectResponse *putObjectResponse = [self.s3Clientobject putObject:por];
            NSLog(@"step 5");
            NSLog(@"Image Response is %@",putObjectResponse);
            
            
            
        }
    }
    else
    {
        
        NSString *imagename=[AppDelegate uniqueidstring];
        
        NSString *videoNameString =  [NSString stringWithFormat:@"%@.jpeg",imagename];
        
        NSString *urlstring=[self prepareamazoneurl:videoNameString];
        
        [mymutablearray addObject:urlstring];
        
        S3PutObjectRequest *por = [[S3PutObjectRequest alloc] initWithKey:videoNameString   inBucket:PICTURE_BUCKET] ;
        NSLog(@"step 2");
        por.contentType = @"image/jpeg";
        NSLog(@"step 3");
        
        
        UIImage *imageThumb=myimage;
        NSData   *thnmbData = UIImageJPEGRepresentation(imageThumb, 0.25);
        por.data        = thnmbData;
        NSLog(@"step 4");
        por.cannedACL   = [S3CannedACL publicRead];
        
        // Put the image data into the specified s3 bucket and object.
        S3PutObjectResponse *putObjectResponse = [self.s3Clientobject putObject:por];
        NSLog(@"step 5");
        NSLog(@"Image Response is %@",putObjectResponse);
    }
      return mymutablearray;
}

-(NSString *)prepareamazoneurl :(NSString *)filename
{
    
    return [NSString stringWithFormat:@"http://d11lfnm7qwbkcd.cloudfront.net/%@",filename];
  // return [NSString stringWithFormat:@"https://s3-us-west-2.amazonaws.com/%@/%@",PICTURE_BUCKET,filename];
   
}

/////// amazone over \\\\\\\


// image compare
+(BOOL)imagecompare:(UIImage *)image1 isEqualTo:(UIImage *)image2
{
    NSData *data1 = UIImagePNGRepresentation(image1);
    NSData *data2 = UIImagePNGRepresentation(image2);
    
    return [data1 isEqual:data2];
}


#pragma mark creating scrollview for different views
-(void)createmyscrollview :(UIScrollView *)itemscrollview :(NSMutableArray *)itemsarray :(int)x :(int)y :(id )targetviewcont
{
    [self likeandunlikestatus:itemsarray  :self.allLikesGlobalarray ];
    
    for (int i=0; i<itemsarray.count; i++)
    {
        
        ItemsModelobject *itemobj=[itemsarray objectAtIndex:i];
        
        UIView *mainview=[[UIView alloc]initWithFrame:CGRectMake(x, y, 138, 174)];
        mainview .backgroundColor=[UIColor clearColor];
        mainview.tag=i;
        
        UIImageView *border=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, mainview.frame.size.width, mainview.frame.size.height)];
        border.image=[UIImage imageNamed:@"Createlistinglayer.png"];
        border.tag=i*200+200;
        [mainview addSubview:border];
        
        UIImageView *itemimgv=[[UIImageView alloc]initWithFrame:CGRectMake(1,1, 136, 118)];
        itemimgv.tag=i;
        if (itemobj.picturearray.count<1)
        {
            itemimgv.image=[UIImage imageNamed:@"noimage.png" ];
        }
        else
        {
            NSString *urlstr=[itemobj.picturearray objectAtIndex:0];
            NSURL *picurl=[NSURL URLWithString:urlstr];
            [itemimgv setImageWithURL:picurl             placeholderImage:[UIImage imageNamed:@"noimage.png"] options:SDWebImageCacheMemoryOnly];
            //itemimgv
        
        
        MDRadialProgressView *radialView3;
            
            CGRect progressfrm=itemimgv.frame;
            
            progressfrm.size.width=progressfrm.size.width-50;
            progressfrm.size.height=progressfrm.size.height-20;
            progressfrm.origin.x=progressfrm.origin.x+25;
            progressfrm.origin.y=progressfrm.origin.y+15;
            
            radialView3 = [self progressViewWithFrame:progressfrm];
            
            radialView3.tag=i+1;
            radialView3.progressTotal = 10;
            radialView3.progressCounter = 0;
            radialView3.theme.completedColor = [UIColor colorWithRed:12.0/255.0f green:174.0/255.0f blue:180.0/255.0f alpha:1.0f];
            radialView3.theme.incompletedColor = [UIColor lightGrayColor];
            radialView3.theme.thickness = 10;
            radialView3.theme.sliceDividerHidden = YES;
            radialView3.theme.labelColor=[UIColor colorWithRed:12.0/255.0f green:174.0/255.0f blue:180.0/255.0f alpha:1.0f];
            radialView3.theme.centerColor =[UIColor clearColor];
            radialView3.theme.font=[UIFont fontWithName:@"Muli" size:12];
            [mainview addSubview:radialView3];
        }
       
           [mainview addSubview:itemimgv];
        
        
        
        UILabel *titlelbl=[[UILabel alloc]initWithFrame:CGRectMake(3, 125, 132, 30)];
        titlelbl.text=[NSString stringWithFormat:@"%@",itemobj.itemNamestr];
        titlelbl.textAlignment=NSTextAlignmentCenter;
       // titlelbl.textColor=itemLabel1.textColor;
       // titlelbl.font=itemLabel1.font;
        titlelbl.textColor=[UIColor colorWithRed:12.0/255.0f green:174.0/255.0f blue:180.0/255.0f alpha:1.0f];
        titlelbl.font=[UIFont fontWithName:@"Muli" size:13];
        [mainview addSubview:titlelbl];
        
        
        UILabel *listpricelbl=[[UILabel alloc]initWithFrame:CGRectMake(0, 145, 68, 30)];
        //  NSLog(@"chec %@",itemobj.salecostcoststr);
        listpricelbl.text=[NSString stringWithFormat:@"$%@",itemobj.salecostcoststr];
        listpricelbl.textAlignment=NSTextAlignmentRight;
       // listpricelbl.font=itempriceLabel1.font;
       // listpricelbl.textColor=itempriceLabel1.textColor;
        
        listpricelbl.font=[UIFont fontWithName:@"Muli" size:13];
        listpricelbl.textColor=[UIColor colorWithRed:12.0/255.0f green:174.0/255.0f blue:180.0/255.0f alpha:1.0f];
        [mainview addSubview:listpricelbl];
        
        
        UILabel *origlbl=[[UILabel alloc]initWithFrame:CGRectMake(73, 145, 70, 30)];
        
        origlbl.textAlignment=NSTextAlignmentLeft;
        
        origlbl.text=[NSString stringWithFormat:@"$%@",itemobj.listingcoststr];
        
        NSDictionary* attributes = @{  NSStrikethroughStyleAttributeName: [NSNumber numberWithInt:NSUnderlineStyleSingle]   };
        
        NSAttributedString* attrText = [[NSAttributedString alloc] initWithString:origlbl.text attributes:attributes];
        origlbl.attributedText=attrText;
        // origlbl.text=itemobj.listingcoststr;
       // origlbl.font=oldpriceLabel1.font;
       // origlbl.textColor=oldpriceLabel1.textColor;
        origlbl.font=[UIFont fontWithName:@"Muli" size:13];;
        origlbl.textColor=[UIColor colorWithRed:131.0/255.0f green:131.0/255.0f blue:131.0/255.0f alpha:1.0f];
        [mainview addSubview:origlbl];
        
        
        UIButton *likebtn=[UIButton buttonWithType:UIButtonTypeCustom];
        
        likebtn.frame=CGRectMake(6, 6, 23, 23);
        if (itemobj.likedbool==YES)
        {
            [likebtn setImage:[UIImage imageNamed:@"like_on.png"] forState:UIControlStateNormal];
        }
        else{
            [likebtn setImage:[UIImage imageNamed:@"like_off.png"] forState:UIControlStateNormal];
        }
        
        if ([targetviewcont isKindOfClass:[MyWishlistViewController class ]] && itemobj.likedbool==NO)
        {
            break;
        }
        
        [likebtn addTarget:targetviewcont action:@selector(likebuttonaction:) forControlEvents:UIControlEventTouchUpInside];
        
        likebtn.tag=i;
        if ([targetviewcont isKindOfClass:[MyClosetViewController class ]] && itemobj.likedbool==NO)
        {
            
        }
        else
        {
        [mainview addSubview:likebtn];
        }
        //likebtn.backgroundColor=[UIColor redColor];
        
        
        UIButton *listdetailbtn=[UIButton buttonWithType:UIButtonTypeCustom];
        listdetailbtn.frame=CGRectMake(0, 28, mainview.frame.size.width, mainview.frame.size.height-25);
        [listdetailbtn addTarget:targetviewcont action:@selector(listDetailsAction:) forControlEvents:UIControlEventTouchUpInside];
        listdetailbtn.tag=i;
       // listdetailbtn.backgroundColor=[UIColor lightGrayColor];
        [mainview addSubview:listdetailbtn];
         NSLog(@"----sold is %@",itemobj.itemStatus);
        if ([itemobj.itemStatus isEqualToString:@"Sold"])
        {
          //  listdetailbtn.userInteractionEnabled=NO;
            
            UIImageView *soldOutImageView=[[UIImageView alloc]initWithFrame:CGRectMake(50,5,91,17.5)];
            [soldOutImageView setImage:[UIImage imageNamed:@"sold.png"]];
            soldOutImageView.tag=18000;
            [mainview addSubview:soldOutImageView];
            [mainview bringSubviewToFront:soldOutImageView];
        }
       
        
        if ((i+1)%2 == 0)
        {
            y=y+200;
            x=15;
        }
        else
        {
            x=167;
        }
        
        [itemscrollview addSubview:mainview];
    }
    
    
    
    if (y+220<itemscrollview.frame.size.height)
    {
        y=itemscrollview.frame.size.height-219;
    }
    
    [itemscrollview setContentSize:CGSizeMake(320, y+220)];
    itemscrollview.contentOffset=CGPointMake(0, 0);
    UIActivityIndicatorView *spinner = [[UIActivityIndicatorView alloc]
                                        initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    spinner.color=[UIColor colorWithRed:12.0/255.0 green:183.0/255.0 blue:174.0/255.0 alpha:1.0];
    //spinner.frame =CGRectMake(145,y+50,30,30);
    
    spinner.frame =CGRectMake(105,y+50,30,30);
    spinner.hidesWhenStopped = YES;
    spinner.tag=4821;
    [itemscrollview addSubview:spinner];
    //[spinner startAnimating];
    
    
    UILabel *loadmoreLabel=[[UILabel alloc]initWithFrame:CGRectMake(140,y+50,150,30)];
    loadmoreLabel.text=@"Loading...";
    loadmoreLabel.tag=8421;
    loadmoreLabel.textColor=[UIColor grayColor];
    [itemscrollview addSubview:loadmoreLabel];
    
    spinner.hidden=YES;
    loadmoreLabel.hidden=YES;
    itemscrollview.hidden=NO;
    
    
    CGSize tablecont=itemscrollview.contentSize;
    
    tablecont.height=tablecont.height+20;
    
    itemscrollview.contentSize=tablecont;
    
    
   
    
    [AppDelegate assaignitemimages:CGRectMake(0, 0, 320, itemscrollview.frame.size.height) :itemsarray :itemscrollview ];
}



/// progress bar with frame
- (MDRadialProgressView *)progressViewWithFrame:(CGRect)frame
{
	MDRadialProgressView *view = [[MDRadialProgressView alloc] initWithFrame:frame];
    
	// Only required in this demo to align vertically the progress views.
	//view.center = CGPointMake(self.view.center.x + 80, view.center.y);
	
	return view;
}

// loading images with priority
+(void)assaignitemimages :(CGRect)rect :(NSMutableArray *)itemsarray :(UIScrollView *)itemscrollview
{
    CGRect scrollviewrect=rect;
    
    // NSMutableArray *subviewsarray=[[NSMutableArray alloc]init];
    
    // getting subviews of scrollview
    for (UIView *temv in itemscrollview.subviews )
    {
        if (CGRectIntersectsRect(scrollviewrect, temv.frame)) // check ing for intersection
        {
            for (UIImageView *itemimgv in temv.subviews) // check ing for imageview
            {
                if (itemimgv.tag==temv.tag && [itemimgv isKindOfClass:[UIImageView class]])
                {
                    if (itemsarray.count>itemimgv.tag)
                    {
                        
                        ItemsModelobject *itemobj=[itemsarray objectAtIndex:itemimgv.tag];
                        
                        if (itemobj.picturearray.count<1)
                        {
                            itemimgv.image=[UIImage imageNamed:@"noimage.png" ];
                        }
                        else
                        {
                            
                            // NSLog(@"check the pic ocunt =%d",itemobj.picturearray.count);
                            NSString *urlstr=[itemobj.picturearray objectAtIndex:0];
                            //   NSLog(@"check teh str first %@",urlstr);
                            NSURL *picurl=[NSURL URLWithString:urlstr];
                            //  NSLog(@"now th e picture url %@",picurl);
                            //NSData *dataimg=[NSData dataWithContentsOfURL:picurl];
                            // itemimgv.image=[UIImage imageWithData:dataimg ];
                          //   [itemimgv setImageWithURL:picurl             placeholderImage:[UIImage imageNamed:@"noimage.png"] options:SDWebImageHighPriority];
                            
                           [itemimgv setImageWithURL:picurl placeholderImage:nil options:SDWebImageHighPriority progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                                
                                NSLog(@"check the received size %d and experted siize %d=-=-==-=-=-==-=-",receivedSize,expectedSize);
                                int prgressint=expectedSize-receivedSize;
                                
                                NSLog(@"chekc the progres -=-=- %d",prgressint);
                               
                              // dispatch_sync(dispatch_get_main_queue(), ^{
                                   UIView *progv=[temv viewWithTag:(temv.tag+1)];
                                   
                                   if ([progv isKindOfClass:[MDRadialProgressView class]])
                                   {
                                       MDRadialProgressView *myprogv=(MDRadialProgressView *)progv;
                                       
                                       myprogv.progressTotal=expectedSize;
                                       myprogv.progressCounter=receivedSize;
                                       
                                   }
                                   
                                   NSLog(@"check the tag %d and teh view %@ and class %@",progv.tag,progv,[progv class]);
                              // });
                               
                                
                                
                                
                            } completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType)
                            {
                                
                                  //NSLog(@"image completed  %@ ",cacheType);
                                
                              //  dispatch_sync(dispatch_get_main_queue(), ^{
                                    UIView *progv=[temv viewWithTag:(temv.tag+1)];
                                
                                if (image==NULL || image==nil)
                                {
                                    
                                }
                                
                                
                                    NSLog(@"Remove view %d and the view %d and image %@",progv.tag,cacheType,image);
                             //   });
                                
                              
                            }];
                           
                           /*
                            NSURL *imageURL = [NSURL URLWithString:urlstr];
                            if (imageURL == nil)
                                break;
                            
                CGImageSourceRef imageSourceRef =CGImageSourceCreateWithURL((__bridge CFURLRef)imageURL, NULL);
                            
                            if(imageSourceRef == NULL)
                                break;
                            
                            CFDictionaryRef props = CGImageSourceCopyPropertiesAtIndex(imageSourceRef, 0, NULL);
                            
                            CFRelease(imageSourceRef);
                            
                            NSLog(@"%@", (__bridge NSDictionary *)props);
                            
                            CFRelease(props);
                            */
                            
                            
                            
                           
                            
                             NSLog(@"load data with priority ===== %d",itemimgv.tag);
                            //itemimgv
                            
                        }
                        
                    }
                    
                    
                }
                else if (itemimgv.tag !=temv.tag && [itemimgv isKindOfClass:[UIImageView class]])
                {
                    // itemimgv.backgroundColor=[UIColor redColor];
                }
            }
            //[subviewsarray addObject:[NSNumber numberWithInt:temv.tag]];
        }
    }
    // NSLog(@"check teh views array count %d and check the data %@",subviewsarray.count,subviewsarray);
    
}


// Like and unlike items check in an array
-(void)likeandunlikestatus :(NSMutableArray *)araytoprocess :(NSMutableArray *)likesarray
{
    NSLog(@"check the likes ids -=-=-=%@-=-=-= ",likesarray);
    
    for (ItemsModelobject *itemsobj in araytoprocess)
    {
        BOOL invitebool=NO;
        
        for (int lk=0; lk<likesarray.count; lk++)
        {
            NSString *prevlikedstr=[likesarray objectAtIndex:lk];
            
            if ([prevlikedstr isEqualToString:itemsobj.userlistingidstr ])
            {
                itemsobj.likedbool=YES;
                invitebool=YES;
                break;
            }
            
        }
        if (invitebool==NO && itemsobj.likedbool==YES  )
        {
            itemsobj.likedbool=NO;
        }
        
        
    }
    
}


// sorting array for categories etc
-(NSMutableArray *)sortmyarray :(NSString *)sortfield :(NSMutableArray *)mutablearray
{
    NSSortDescriptor *sorterrrr = [[NSSortDescriptor alloc]initWithKey:sortfield
                                                             ascending:YES selector:@selector(localizedCaseInsensitiveCompare:)];
    
    [mutablearray sortUsingDescriptors:[NSArray arrayWithObject:sorterrrr]];
    
    return mutablearray;
}



#pragma mark Like responce


// handling like and unlike listing response
-(void)likeresponce:(NSDictionary *)likerespdict
{
    NSString *messagestr=[likerespdict valueForKey:@"message"];
    
    NSLog(@"Appdelegate Like responces %@ ",likerespdict);
   if ([messagestr isEqualToString:@"Wishlist Saved Successfully"])
    {
        NSString *likedid=[NSString stringWithFormat:@"%@",[likerespdict valueForKey:@"userListingID"]];
        [self.allLikesGlobalarray addObject:likedid];
    }
    else
    {
        NSString *likedid=[NSString stringWithFormat:@"%@",[likerespdict valueForKey:@"userListingID"]];
        for (int i=0; i<self.allLikesGlobalarray.count; i++)
        {
            NSString *listidstr=[self.allLikesGlobalarray objectAtIndex:i];
            
            if ([[NSString stringWithFormat:@"%@",listidstr] isEqualToString:[NSString stringWithFormat:@"%@",likedid]])
            {
                [self.allLikesGlobalarray removeObjectAtIndex:i];
                break;
            }
            
            
        }
    }
   
   
}

// parsing category data and other data regarding listing and filter
-(void)parsedataforCategory :(NSDictionary*)respdict
{
    
    
    [self.catogoryGlobalarray removeAllObjects];
    NSLog(@"check the catogory %@",respdict);
    
    if ([[respdict objectForKey:@"categories" ] isKindOfClass:[NSArray class]])
    {
        NSArray *myar=[respdict objectForKey:@"categories" ];
        for (int i=0; i<myar.count; i++)
        {
            NSDictionary *mydict=[myar objectAtIndex:i];
            
            LookupModelobject *lookobj=[[LookupModelobject alloc]init];
            
            lookobj.loolupid=[mydict valueForKey:@"categoryId"];
            lookobj.loolupValue=[mydict valueForKey:@"categoryName"];
            
          lookobj.parentcatid=[mydict valueForKey:@"parentCategoryId"];
            lookobj.loolupStatus=[mydict valueForKey:@"active"];
            NSMutableArray *categoryArray=[mydict valueForKey:@"listsubcategory"];
            lookobj.categoryDataArray=[NSMutableArray new];
            
            for (int j=0;j<categoryArray.count; j++)
            {
                LookupModelobject *lookUp_newobj=[[LookupModelobject alloc]init];
                lookUp_newobj.loolupid=[[categoryArray objectAtIndex:j] valueForKey:@"categoryId"];
                lookUp_newobj.loolupValue=[[categoryArray objectAtIndex:j]  valueForKey:@"categoryName"];
                lookUp_newobj.parentcatid=[[categoryArray objectAtIndex:j]  valueForKey:@"parentCategoryId"];
                lookUp_newobj.loolupStatus=[[categoryArray objectAtIndex:j]  valueForKey:@"active"];
                
                if ([lookUp_newobj.loolupValue isEqualToString:@"Other"])
                {
                    lookUp_newobj.isother=YES;
                }
                [lookobj.categoryDataArray addObject:lookUp_newobj];
            }
            
            
           // lookobj.categoryDataArray=[self sortmyarray:@"loolupValue" :lookobj.categoryDataArray];
            
            
            [self.catogoryGlobalarray addObject:lookobj];
        }
        
    }
    
   // self.catogoryGlobalarray=[self sortmyarray:@"loolupValue" :self.catogoryGlobalarray];
    
    [self.sizeGlobalarray removeAllObjects];
    //  NSLog(@"check the catogory %@",respdict);
    if ([[respdict objectForKey:@"lookupsBySize" ] isKindOfClass:[NSArray class]]) {
        NSArray *myar=[respdict objectForKey:@"lookupsBySize" ];
        for (int i=0; i<myar.count; i++)
        {
            NSDictionary *mydict=[myar objectAtIndex:i];
            
            LookupModelobject *lookobj=[[LookupModelobject alloc]init];
            
            lookobj.loolupName=@"lookupName";
            lookobj.loolupid=[mydict valueForKey:@"lookupID"];
            lookobj.loolupValue=[mydict valueForKey:@"lookupValue"];
            lookobj.loolupNumber=[mydict valueForKey:@"sequenceNumber"];
            lookobj.loolupStatus=[mydict valueForKey:@"status"];
            
            
            [self.sizeGlobalarray addObject:lookobj];
        }
        
    }
    
    // for sorting
   // self.sizeGlobalarray=[self sortmyarray:@"loolupValue" :self.sizeGlobalarray];
    
    [self.conditionGlobalarray removeAllObjects];
    //   NSLog(@"check the catogory %@",respdict);
    if ([[respdict objectForKey:@"lookupsByCondition" ] isKindOfClass:[NSArray class]]) {
        NSArray *myar=[respdict objectForKey:@"lookupsByCondition" ];
        for (int i=0; i<myar.count; i++)
        {
            NSDictionary *mydict=[myar objectAtIndex:i];
            
            LookupModelobject *lookobj=[[LookupModelobject alloc]init];
            
            lookobj.loolupName=@"lookupName";
            lookobj.loolupid=[mydict valueForKey:@"lookupID"];
            lookobj.loolupValue=[mydict valueForKey:@"lookupValue"];
            lookobj.loolupNumber=[mydict valueForKey:@"sequenceNumber"];
            lookobj.loolupStatus=[mydict valueForKey:@"status"];
            
            
            [self.conditionGlobalarray addObject:lookobj];
        }
        
    }
    
    //self.conditionGlobalarray=[self sortmyarray:@"loolupValue" :self.conditionGlobalarray];
    
    
    [self.brandGlobalarray removeAllObjects];
    [self.filterbrandGlobalarray removeAllObjects];
    //   NSLog(@"check the catogory %@",respdict);
    if ([[respdict objectForKey:@"lookupsByBrand" ] isKindOfClass:[NSArray class]]) {
        NSArray *myar=[respdict objectForKey:@"lookupsByBrand" ];
        for (int i=0; i<myar.count; i++)
        {
           
            NSDictionary *mydict=[myar objectAtIndex:i];
            
            LookupModelobject *lookobj=[[LookupModelobject alloc]init];
            
            lookobj.loolupName=@"lookupName";
            lookobj.loolupid=[mydict valueForKey:@"lookupID"];
            lookobj.loolupValue=[mydict valueForKey:@"lookupValue"];
            lookobj.loolupNumber=[mydict valueForKey:@"sequenceNumber"];
            lookobj.loolupStatus=[mydict valueForKey:@"status"];
            
            
             NSString *tempstr=[[lookobj.loolupValue substringToIndex:1] uppercaseString];
            
            if (self.filterbrandGlobalarray.count<1)
            {
                NSMutableDictionary *mymutdict=[NSMutableDictionary new];
                
                [mymutdict setObject:[NSMutableArray arrayWithObject:lookobj] forKey:tempstr];
                
                
                [self.filterbrandGlobalarray addObject:mymutdict];
                
            }
            else
            {
                BOOL adddictbool=YES;
                
                for (NSMutableDictionary *somedict in self.filterbrandGlobalarray )
                {
                    for (int j=0; j<somedict.allKeys.count; j++)
                    {
                     //   NSLog(@"check the ketemp %@  andddd %@",tempstr,somedict.allKeys);
                        if ([tempstr isEqualToString:[somedict.allKeys objectAtIndex:j]])
                        {
                            
                            NSMutableArray *dataarray=[somedict objectForKey:[somedict.allKeys objectAtIndex:j] ];
                            [dataarray addObject:lookobj];
                            
                            
                            dataarray=[self sortmyarray:@"loolupValue" :dataarray];
                            
                            
                            adddictbool=NO;
                            break;
                            
                            
                        }
                    }
                    
                }
                
                if (adddictbool==YES)
                {
                    NSMutableDictionary *mymutdict=[NSMutableDictionary new];
                    
                    [mymutdict setObject:[NSMutableArray arrayWithObject:lookobj] forKey:tempstr];
                    
                    
                    [self.filterbrandGlobalarray addObject:mymutdict];
                }
                
            }
            
           
            [self.brandGlobalarray addObject:lookobj];
        }
        
    }
    
    
    
  
   /// this is for sorting the tilter array
  
    NSArray *tempcopyarray=[self.filterbrandGlobalarray copy];
    
    
    NSMutableDictionary *sortdict=[NSMutableDictionary new];
    
    for (int i=0; i<tempcopyarray.count; i++)
    {
        NSDictionary *tempdict=[tempcopyarray objectAtIndex:i];
        [sortdict setObject:tempdict forKey:[tempdict.allKeys objectAtIndex:0]];
    }
    
    NSArray *sortedarray = [sortdict.allKeys sortedArrayUsingSelector:@selector(compare:)];
    
    [self.filterbrandGlobalarray removeAllObjects];
    for (int i=0; i<sortedarray.count; i++)
    {
        NSString *tempkey=[sortedarray objectAtIndex:i];
        
        [self.filterbrandGlobalarray addObject:[sortdict objectForKey:tempkey ]];
    }
    
    tempcopyarray=nil;
    
    
    //filterbrandGlobalarray sorting complete
     NSLog(@"check the count of the array %lu  and description %@",(unsigned long)brandGlobalarray.count,brandGlobalarray);
    self.brandGlobalarray=[self sortmyarray:@"loolupValue" :self.brandGlobalarray];
    
    [self.genderglobalaray removeAllObjects];
    //   NSLog(@"check the catogory %@",respdict);
    if ([[respdict objectForKey:@"lookupsByGender" ] isKindOfClass:[NSArray class]]) {
        NSArray *myar=[respdict objectForKey:@"lookupsByGender" ];
        for (int i=0; i<myar.count; i++)
        {
            NSDictionary *mydict=[myar objectAtIndex:i];
            
            LookupModelobject *lookobj=[[LookupModelobject alloc]init];
            
            lookobj.loolupName=@"lookupName";
            lookobj.loolupid=[mydict valueForKey:@"lookupID"];
            lookobj.loolupValue=[mydict valueForKey:@"lookupValue"];
            lookobj.loolupNumber=[mydict valueForKey:@"sequenceNumber"];
            lookobj.loolupStatus=[mydict valueForKey:@"status"];
            
            
            [self.genderglobalaray addObject:lookobj];
        }
        
    }
    
    //self.genderglobalaray=[self sortmyarray:@"loolupValue" :self.genderglobalaray];
    
    
    
    LookupModelobject *lookobj=[[LookupModelobject alloc]init];
    
    lookobj.loolupName=@"lookupName";
    lookobj.loolupid=0;
    lookobj.loolupValue=@"Other";
    lookobj.loolupNumber=@"0";
    lookobj.loolupStatus=@"0";
    lookobj.isother=YES;
    
    [self.conditionGlobalarray addObject:lookobj];


    LookupModelobject *lookobj22=[[LookupModelobject alloc]init];
    
    lookobj22.loolupName=@"lookupName";
    lookobj22.loolupid=0;
    lookobj22.loolupValue=@"Other";
    lookobj22.loolupNumber=@"0";
    lookobj22.loolupStatus=@"0";
    lookobj22.isother=YES;
    [self.sizeGlobalarray addObject:lookobj22];
    
    
    LookupModelobject *lookobj33=[[LookupModelobject alloc]init];
    
    lookobj33.loolupName=@"lookupName";
    lookobj33.loolupid=@"130";
    lookobj33.loolupValue=@"Other";
    lookobj33.loolupNumber=@"0";
    lookobj33.loolupStatus=@"0";
    lookobj33.isother=YES;
    [self.brandGlobalarray addObject:lookobj33];
    
    
    
  //  NSLog(@"check teh data count  ")
}
-(void)animatemyview:(UIView *)myview
{
    CATransition *transition = [CATransition animation];
    transition.duration = 0.5;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype =kCATransitionFromRight;
    transition.delegate = self;
    [myview.layer addAnimation:transition forKey:nil];
}

//// follow and unfolow

-(void)sucessresponceforFollowandunfollow:(NSDictionary *)maindict
{
    NSLog(@"check what is going out %@",maindict);
}

-(void)horseanimation :(UIImageView *)animationv;
{
    NSLog(@"thehorse animation in the suggestion");

    if ([self isnullorempty:horseanimationArrray])
    {
          horseanimationArrray=[NSMutableArray arrayWithObjects:[UIImage imageNamed:@"step1.png"],[UIImage imageNamed:@"step2.png"],[UIImage imageNamed:@"step3.png"],[UIImage imageNamed:@"step4.png"],[UIImage imageNamed:@"step5.png"],[UIImage imageNamed:@"step6.png"],[UIImage imageNamed:@"step7.png"],[UIImage imageNamed:@"step8.png"],[UIImage imageNamed:@"step9.png"],[UIImage imageNamed:@"step10.png"],[UIImage imageNamed:@"step11.png"],[UIImage imageNamed:@"step12.png"],[UIImage imageNamed:@"step13.png"], nil];
    }
    
    animationv.animationImages=horseanimationArrray;
    animationv.animationDuration=0.5;
    animationv.animationRepeatCount=2;
    
}

-(void)internetConnection
{
    UIAlertView *Internetalert=[[UIAlertView alloc]initWithTitle:appname message:@"Internet connection not available" delegate:self cancelButtonTitle:nil otherButtonTitles:@"Ok", nil];
    [Internetalert show];
    //[appdelegate stopspinner:self.view];
    
}
-(void)errorMessage:(NSString *)message
{
    if([message isEqualToString:@"message"])
    {
        
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:appname message:message delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }
}
-(void)successResponseforlistusers:(NSDictionary *)responseDict
{
    dispatch_async( dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        dispatch_async( dispatch_get_main_queue(), ^{
            
            NSLog(@"thelist main count is =======>>>>>>>>>><<<<<mainresponse dict is %@",responseDict);
            NSMutableArray *mainArray=[responseDict valueForKey:@"listusers"];
            [userlistArray removeAllObjects];
            for (int i=0; i<mainArray.count; i++)
            {
                NSMutableDictionary *maindict=[mainArray objectAtIndex:i];
                suggestionobj=[[SuggestionObject alloc]init];
                suggestionobj.useridstr=[maindict valueForKey:@"userId"];
                suggestionobj.emailstr=[maindict valueForKey:@"email"];
                suggestionobj.followcount=[maindict valueForKey:@"followersCount"];
                suggestionobj.namestr=[maindict valueForKey:@"name"];
                suggestionobj.usernamestr=[maindict valueForKey:@"userName"];
                suggestionobj.profilepicstr=[maindict valueForKey:@"profilepic"];
                suggestionobj.typestr=[maindict valueForKey:@"type"];
                suggestionobj.socialidstr=[maindict valueForKey:@"socialId"];
                [userlistArray addObject:suggestionobj];
            }
        });
    });
}
-(IBAction)notificationcount11:(id)sender
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, kNilOptions), ^{
        registrationRep.delegate=self;
        //By srinadh
       //[registrationRep getnotificationcount:[self getStringFromPlist:@"userid"]];
    });
    
}
-(void)successresponceListing :(id)respdict :(NSString *)paramname;
{    dispatch_async( dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
   
        
        if([paramname isEqualToString:@"getcatogery"])
        {
            
            
            [myappDelegate.catogoryGlobalarray removeAllObjects];
            
            
            [myappDelegate parsedataforCategory:respdict ];
            
            
            NSLog(@"check the catogory %@",respdict);
            
        }
});
    
}
-(void)successresponseofnotificationcount:(NSMutableDictionary *)responsedict
{
    if([[responsedict valueForKey:@"message"]isEqualToString:@"Notification Info Successfully"])
    {
            [self insertStringInPlist:@"notificationcount" value:[NSString stringWithFormat:@"%@",[responsedict objectForKey:@"notificationCount"]]];
    }
    else
    {
        NSLog(@"notificationcountlabel.text....%@",[responsedict valueForKey:@"message"]);
    }
    }



#pragma coredata stack
- (NSManagedObjectContext *) managedcontext
{
    
    if (managedcontext != nil)
    {
        return managedcontext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistancestore];
    if (coordinator != nil) {
        managedcontext = [[NSManagedObjectContext alloc] init];
        [managedcontext setPersistentStoreCoordinator: coordinator];
    }
    return managedcontext;
}
- (NSManagedObjectModel *)managedobjectmodel
{
    
    if (managedobjectmodel != nil)
    {
        return managedobjectmodel;
    }
    managedobjectmodel = [NSManagedObjectModel mergedModelFromBundles:nil];
    return managedobjectmodel;
}
- (NSPersistentStoreCoordinator *)persistancestore
{
    if (persistancestore != nil)
    {
        return persistancestore;
    }
    
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"ContactsInfo.sqlite"];
    
    NSError *error = nil;
    persistancestore = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedobjectmodel]];
    if (![persistancestore addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error]) {
        /*
         Replace this implementation with code to handle the error appropriately.
         
         abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development. If it is not possible to recover from the error, display an alert panel that instructs the user to quit the application by pressing the Home button.
         
         Typical reasons for an error here include:
         * The persistent store is not accessible
         * The schema for the persistent store is incompatible with current managed object model
         Check the error message to determine what the actual problem was.
         */
        ////NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return persistancestore;
}


- (NSURL *)applicationDocumentsDirectory
{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

//Inserting and Fetching and Validation or Comparision of contacts methods will organize here
-(void)dealWithPhoneBookContacts
{
    dispatch_async( dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        
        [self getContactsFromPhone];
        [self getContactsfromCoreData];
        
        if(self.coreDataContactsAray.count==0)
        {
            [self insertingContactsinCoreData:self.emailArray];
        }
        else
        {
            [self validateNewlyAddedContactsInAddressbook:self.emailArray compareWith:self.coreDataContactsAray];
            [self insertingContactsinCoreData:self.newlyAddedContactsAray];
            [self getContactsfromCoreData];
        }
    });
}
//This is the method to get the Contacts from Addressbook, It is already Implemented in InviteFriendsViewController so we are calling the method which is presented in that VC
-(void)getContactsFromPhone
{

    InviteFriendsViewController *invitefriendsVC=[[InviteFriendsViewController alloc]init];
   // [invitefriendsVC performSelectorOnMainThread:@selector(emailbutton:) withObject:nil waitUntilDone:YES];
    [invitefriendsVC performSelector:@selector(emailbutton:) withObject:nil];
    

}



//This is the method to Insert the contacts to core Data
-(void)insertingContactsinCoreData:(NSMutableArray *)mainArray
{
    NSManagedObjectContext *context=[self managedcontext];
    
    NSManagedObject *manageobj;
    
    for (int k=0; k<[mainArray count]; k++)
        //for (int k=minval; k<countval; k++)
    {
        manageobj=[NSEntityDescription insertNewObjectForEntityForName:@"PhoneContacts" inManagedObjectContext:context];
        
        
        Person*obj=[mainArray objectAtIndex:k];
        //if(obj.phonenumberArray.count>0)
        {
            if([APPDELEGATE isnullorempty:obj.fullName])
            {
                
            }
            else
            {
                //[manageobj setValue:@"1" forKey:@"userid"];
                //NSString*namestr=[obj.fullName lowercaseString];
                //saving the username in the coreata
                [manageobj setValue:obj.fullName forKey:@"username"];
                
            }
            //NSString*phonenumstr=[obj.phonenumberArray componentsJoinedByString:@","];
            [manageobj setValue:obj.homeEmail forKey:@"homemailid"];
            [manageobj setValue:obj.workEmail forKey:@"workmailid"];
            [manageobj setValue:obj.recordId forKey:@"recordid"];
        }
        
    }
    
    NSError *err;
    if(![context save:&err])
    {
        NSLog( @"Could not save in to core data");
    }
    else
    {
        NSLog(@"data saved in coredata");
        [self getContactsfromCoreData];
        
    }
    
}
//This method is to fetch the contacts from core data
-(void)getContactsfromCoreData
{
    NSManagedObjectContext *context=[(AppDelegate *)[[UIApplication sharedApplication]delegate]managedcontext];
    NSFetchRequest *request =[[NSFetchRequest alloc]init];
    NSEntityDescription *entity;
    entity =[NSEntityDescription entityForName:@"PhoneContacts" inManagedObjectContext:context];
    [request setEntity:entity];
    [request setReturnsDistinctResults:NO];
    NSError *error;
    NSArray *resposeArray=[context executeFetchRequest:request error:&error];
    self.coreDataContactsAray=[[NSMutableArray alloc]init];
    NSLog(@"the error is %@",error.description);
    NSLog(@"the getCoreDataContactsAray is %lu",(unsigned long)resposeArray.count);
    
    
    for (int i=0; i<resposeArray.count; i++)
    {
        PhoneContacts *coreDataobj=[resposeArray objectAtIndex:i];
        Person *personobj=[[Person alloc]init];
        personobj.recordId=coreDataobj.recordid;
        personobj.fullName=coreDataobj.username;
        personobj.homeEmail=coreDataobj.homemailid;
        personobj.workEmail=coreDataobj.workmailid;
        [self.coreDataContactsAray addObject:personobj];
        NSLog(@"coredata %@******%@",coreDataobj.username,coreDataobj.recordid);
        
    }
    
    //[self compareArraymethod];
    
}

//This method is to what are the new contacts added in address book from last synced
-(void)validateNewlyAddedContactsInAddressbook:(NSMutableArray *)emailarray compareWith:(NSMutableArray *)coreDataArray
{
    
    NSLog(@"emailarray count=%lu",(unsigned long)[emailarray count]);
     NSLog(@"coreDataArray count=%lu",(unsigned long)[coreDataArray count]);
    
    newlyAddedContactsAray =[[NSMutableArray alloc]init];
    //__block NSMutableArray *newData = [NSMutableArray new];
    
    [emailarray enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        __block Person *phonebookObj = (Person *)obj;
        NSLog(@"phonebookObj.recordId=%@",phonebookObj.recordId);
        NSLog(@"phonebookObj.recordId=%@",phonebookObj.fullName);
        
        __block BOOL isExisting = NO;
        [coreDataArray enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            Person *coreDataObj = (Person *)obj;
            NSLog(@"coreDataObj.recordId=%@",coreDataObj.recordId);
             NSLog(@"coreDataObj.recordId=%@",coreDataObj.fullName);
            //if(phonebookObj.recordId == coreDataObj.recordId)
            if([phonebookObj.recordId isEqualToString:coreDataObj.recordId])
            {
                isExisting = YES;
            }
        }];
        if(!isExisting)
        {
            NSLog(@"obj=%@",phonebookObj.recordId);
            [newlyAddedContactsAray addObject:phonebookObj];
            
        }
        
    }];
    NSLog(@"newlyAddedContactsAray count=%lu",(unsigned long)[newlyAddedContactsAray count]);
}

@end
