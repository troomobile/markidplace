//
//  MybrandsCustomCell.h
//  MarkidPlace
//
//  Created by Varma Bhupatiraju on 15/09/14.
//  Copyright (c) 2014 stellentmac1. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MybrandsCustomCell : UITableViewCell
@property(nonatomic,strong)IBOutlet UILabel *brandlbl;
@property(nonatomic,strong)IBOutlet UILabel *brandNamelbl;
@property(nonatomic,strong)IBOutlet UIButton *brandbtn;

@end
