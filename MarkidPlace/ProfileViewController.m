//
//  ProfileViewController.m
//  MarkidPlace
//
//  Created by stellentmac1 on 5/21/14.
//  Copyright (c) 2014 stellentmac1. All rights reserved.
//

#import "ProfileViewController.h"
#import "FollowersViewController.h"
#import "MessageViewController.h"
@interface ProfileViewController ()

@end

@implementation ProfileViewController
@synthesize usernameLabel,nameLabel,starLabel,itemsLabel,likesLabel,followersLabel,followingLabel,createListLabel,titleLabel,priceLabel,notificationcountLabel,notificationview,profilenotificationview,notificationcountuplabel,msgnotificationcountLabel;

@synthesize picturesarray;
@synthesize delegate;
@synthesize profileImageView,itemscrollview,msgnotificationview;


// menu

@synthesize itemLabel1,itempriceLabel1,oldpriceLabel1,myAccountLabel,myClosetLabel,myfeedLabel,mywishlistLabel,shopallLabel,sellLabel,cartcountLabel;
@synthesize menuBtn,menuview,cartview,animationimgv,menubtnview;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

#pragma mark View Methods.
- (void)viewDidLoad
{
    [super viewDidLoad];
    profilenotificationview.hidden=YES;
    msgnotificationview.hidden=YES;
    notificationview.hidden=YES;
   // myappDelegate=(AppDelegate *)[[UIApplication sharedApplication]delegate];
    // Do any additional setup after loading the view from its nib.
    
    screenSize = [[UIScreen mainScreen]bounds];
    _pinterest=[[Pinterest alloc]initWithClientId:@"1438258" urlSchemeSuffix:@"prod"];
    paypalAddress=@"";
    [myappDelegate horseanimation:animationimgv];
    
    [self profileMethod];
    
      }
-(void)viewWillAppear:(BOOL)animated
{
    [self setmenudown];
    registrationRep=[[RegistrationRepository alloc]init];

  //  [self notificationcount11:nil];
    profileRepo=[[ProfileRepository alloc]init];
    profileRepo.delegate=self;
    
    [profileRepo getProfileData:[myappDelegate getStringFromPlist:@"userid"]];
    

   // int value =[[APPDELEGATE getStringFromPlist:@"notificationcount"] intValue];
    
    
  
          //notificationcountLabel.text=[APPDELEGATE getStringFromPlist:@"notificationcount"];
        
    if (myappDelegate.cartGlobalarray.count>0)
    {
        cartcountLabel.text=[NSString stringWithFormat:@"%d",myappDelegate.cartGlobalarray.count];
        cartview.hidden=NO;
    }
    else
    {
        cartview.hidden=YES;
    }
    
     [myappDelegate startspinner:self.view];
       int value1 = [[APPDELEGATE getStringFromPlist:@"notificationcount"] intValue];
    if (value1>9)
    {
        notificationcountuplabel.text=@"9+";
        notificationcountLabel.text=@"9+";
        profilenotificationview.hidden=NO;
        notificationview.hidden=NO;

        
    }
    else if (value1==0)
    {
        profilenotificationview.hidden=YES;
        notificationview.hidden=YES;
        
    }
    else
    {
        profilenotificationview.hidden=NO;
        notificationview.hidden=NO;

        notificationcountuplabel.text=[NSString stringWithFormat:@"%@",[APPDELEGATE getStringFromPlist:@"notificationcount"]];
        notificationcountLabel.text=[NSString stringWithFormat:@"%@",[APPDELEGATE getStringFromPlist:@"notificationcount"]];
    }
      timer=[NSTimer scheduledTimerWithTimeInterval:5 target:self selector:@selector(notificationcount11:) userInfo:nil repeats:YES];
}
-(void)viewDidDisappear:(BOOL)animated
{
    [timer invalidate];
}
// setting the font to the labels.
-(void)profileMethod
{
    notificationcountuplabel.font=[UIFont fontWithName:@"Muli" size:11];
    msgnotificationcountLabel.font=[UIFont fontWithName:@"Muli" size:11];
    notificationcountuplabel.font=[UIFont fontWithName:@"Muli" size:11];

    
    self.itemsarray=[NSMutableArray new];
    self.picturesarray=[[NSMutableArray alloc]init];
    usernameLabel.font=[UIFont fontWithName:@"Muli" size:14];
    
    self.nameLabel.font = [UIFont fontWithName:@"Monroe" size:14];
    self.itemsLabel.font = [UIFont fontWithName:@"Muli" size:12];
    self.itemsCountLabel.font = [UIFont fontWithName:@"Muli" size:12];
    self.likesLabel.font = [UIFont fontWithName:@"Muli" size:12];
    self.likesCountLabel.font = [UIFont fontWithName:@"Muli" size:12];
    self.followersLabel.font = [UIFont fontWithName:@"Muli" size:12];
    self.followersCountLabel.font = [UIFont fontWithName:@"Muli" size:12];
    self.followingLabel.font = [UIFont fontWithName:@"Muli" size:12];
    self.followingCountLabel.font = [UIFont fontWithName:@"Muli" size:12];
    self.createListLabel.font = [UIFont fontWithName:@"Muli" size:14];
    self.titleLabel.font = [UIFont fontWithName:@"Muli" size:13];
    self.starLabel.font = [UIFont fontWithName:@"Muli" size:12];
    
    self.itemscrollview.contentSize=CGSizeMake(self.itemscrollview.frame.size.width,self.itemscrollview.frame.size.height+100);
    
    NSString *usernamestring=[myappDelegate getStringFromPlist:usernamekey ];
    if ([myappDelegate isnullorempty:usernamestring])
    {
        usernamestring=@"me";
    }
    
    NSString *namestring=[myappDelegate getStringFromPlist:@"realname" ];
    if ([myappDelegate isnullorempty:namestring])
    {
        namestring=@"";
    }
    
    nameLabel.text=namestring;
    usernameLabel.text=[NSString stringWithFormat:@"@%@",usernamestring];
    
    NSString *loginTypeString=[myappDelegate getStringFromPlist:@"type" ];
    
    if ([loginTypeString isEqualToString:@"General"])
    {
        
    }
    else
    {
        
    }
    
     NSString *imagestr=[myappDelegate getStringFromPlist:@"profilePicture" ];
    NSString *picstr;
    if ([myappDelegate isnullorempty:imagestr])
    {
        
    }
    else{
      picstr =[imagestr stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
        [profileImageView setImageWithURL:[NSURL URLWithString:picstr] placeholderImage:[UIImage imageNamed:@"noimage_friend.png"] options:SDWebImageHighPriority];
    }
    
    if([self Contains:@"twimg" on:picstr])
    {
        self.profileImageView.contentMode=UIViewContentModeCenter;
        self.profileImageView.backgroundColor=[UIColor whiteColor];
    }
    NSLog(@"@@@@@@@@Type of Login:%@",[myappDelegate getStringFromPlist:@"type" ]);
    
    CALayer *layer2 = profileImageView.layer;
    [layer2 setCornerRadius:39.0];
    [layer2 setBorderWidth:0];
    [layer2 setMasksToBounds:YES];
    
    self.itemLabel1.font = [UIFont fontWithName:@"Muli" size:13];
    self.itempriceLabel1.font = [UIFont fontWithName:@"Muli" size:13];
    self.itempriceLabel1.font = [UIFont fontWithName:@"Muli" size:13];
    self.oldpriceLabel1.font = [UIFont fontWithName:@"Muli" size:13];
    self.myAccountLabel.font = [UIFont fontWithName:@"Muli" size:15];
    self.myClosetLabel.font = [UIFont fontWithName:@"Muli" size:15];
    self.myfeedLabel.font = [UIFont fontWithName:@"Muli" size:15];
    self.mywishlistLabel.font = [UIFont fontWithName:@"Muli" size:15];
    self.shopallLabel.font = [UIFont fontWithName:@"Muli" size:15];
    self.sellLabel.font = [UIFont fontWithName:@"Muli" size:15];
    self.cartcountLabel.font = [UIFont fontWithName:@"Muli" size:11];
    
    //kishore sep24
    //self.profileImageView.contentMode=UIViewContentModeCenter;
    //self.profileImageView.backgroundColor=[UIColor whiteColor];
    //end
}
-(BOOL)Contains:(NSString *)StrSearchTerm on:(NSString *)StrText
{
    return  [StrText rangeOfString:StrSearchTerm options:NSCaseInsensitiveSearch].location==NSNotFound?FALSE:TRUE;
}
#pragma mark Button Action
// Action performs  Push to Message ViewController
-(IBAction)MsgAction:(id)sender;
{
    if ([sender tag]==550)
    {
        UIActionSheet *action;
        action = [[UIActionSheet alloc]initWithTitle:@"" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Facebook", nil];
        action.tag=560;
        [action showInView:self.view];
    }
    else{
    if (myappDelegate.isiphone5)
    {
        MessageViewController *controller_Msg=[[MessageViewController alloc]initWithNibName:@"MessageViewController" bundle:nil];
        [self.navigationController pushViewController:controller_Msg animated:YES];
    }
    
    else
    {
        MessageViewController *controller_Msg=[[MessageViewController alloc]initWithNibName:@"MessageViewController_iphone4" bundle:nil];
        [self.navigationController pushViewController:controller_Msg animated:YES];
        
    }
    }
}

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    if (actionSheet.tag==560)
    {
         NSString *profilestr=[myappDelegate getStringFromPlist:@"profilePicture"];
        //FaceBook ButtonAction
        if(buttonIndex==0)
        {
           
            NSMutableDictionary *params = [NSMutableDictionary dictionaryWithObjectsAndKeys:  nameLabel.text, @"name",  @"marKIDplace", @"caption", @"profile testing" , @"description",profilestr, @"picture",  nil];
            
            NSLog(@"userObjRef.userNameStr %@ ",nameLabel.text );
            
            
            NSLog(@"profilestr.....%@",profilestr);
            //  NSMutableDictionary *params = [NSMutableDictionary dictionaryWithObjectsAndKeys: usernamestr,@"userName",profilestr,@"profilePicture",nil];
            NSLog(@"params....%@",params);
            
            // Show the feed dialog
            [FBWebDialogs presentFeedDialogModallyWithSession:nil
                                                   parameters:params
                                                      handler:^(FBWebDialogResult result, NSURL *resultURL, NSError *error) {
                                                          if (error) {
                                                              // An error occurred, we need to handle the error
                                                              // See: https://developers.facebook.com/docs/ios/errors
                                                              NSLog(@"Error publishing story: %@", error.description);
                                                          } else {
                                                              if (result == FBWebDialogResultDialogNotCompleted) {
                                                                  // User cancelled.
                                                                  NSLog(@"User cancelled.");
                                                              } else {
                                                                  // Handle the publish feed callback
                                                                  NSDictionary *urlParams = [self parseURLParams:[resultURL query]];
                                                                  
                                                                  if (![urlParams valueForKey:@"post_id"]) {
                                                                      // User cancelled.
                                                                      NSLog(@"User cancelled.");
                                                                      
                                                                  } else {
                                                                      // User clicked the Share button
                                                                      
                                                                      [AppDelegate alertmethod:appname :@"Thanks for posting with marKIDplace." ];                       NSString *result = [NSString stringWithFormat: @"Posted story, id: %@", [urlParams valueForKey:@"post_id"]];
                                                                      NSLog(@"result %@", result);
                                                                  }
                                                              }
                                                          }
                                                      }];
            
            
            
        }
        /*
           else if (buttonIndex == 1)
        {
            // pinterest
                if ([profilestr isKindOfClass:[NSString class]])
                {
                    [_pinterest createPinWithImageURL:[NSURL URLWithString:profilestr]
                                            sourceURL:[NSURL URLWithString:MainUrl]
                                          description:@"Pinning from Pin It Demo"];
                }
                else{
                    [AppDelegate alertmethod:appname :@"unable to share on Pinterest. please try after some time"];
                }
           
            
        }
        */
    }
}






// A function for parsing URL parameters.
- (NSDictionary*)parseURLParams:(NSString *)query {
    NSArray *pairs = [query componentsSeparatedByString:@"&"];
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    for (NSString *pair in pairs) {
        NSArray *kv = [pair componentsSeparatedByString:@"="];
        NSString *val =
        [kv[1] stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        params[kv[0]] = val;
    }
    return params;
}


// Action performs  Push to FollowersViewController

-(IBAction)followersclicked:(id)sender
{
    if (![self.followersCountLabel.text isEqualToString:@"0"])
    {
        FollowersViewController *controller_follwer=[[FollowersViewController alloc]initWithNibName:@"FollowersViewController" bundle:nil];
        controller_follwer.isfollowers=YES;
        [self.navigationController pushViewController:controller_follwer animated:YES];
    }
    
   
}
// Action performs  Push to FollowersViewController
-(IBAction)followingclicked:(id)sender
{
    NSLog(@"==%@==",followingLabel.text);
    if (![self.followingCountLabel.text isEqualToString:@"0"])
    {
        FollowersViewController *controller_follwer=[[FollowersViewController alloc]initWithNibName:@"FollowersViewController" bundle:nil];
        controller_follwer.isfollowers=NO;
        [self.navigationController pushViewController:controller_follwer animated:YES];
    }
    
}
// Action performs  Push to SettingsViewController
-(IBAction)settingsButtonAction:(id)sender
{
    SettingsViewController *settingsVC;
    if (myappDelegate.isiphone5)
    {
        settingsVC = [[SettingsViewController alloc]initWithNibName:@"SettingsViewController" bundle:nil];
    }
    else
    {
        settingsVC = [[SettingsViewController alloc]initWithNibName:@"SettingsViewController_iphone4" bundle:nil];
    }
    [self.navigationController pushViewController:settingsVC animated:YES];
    
}

// Action performs  logout the user and takes to Login View controller
-(IBAction)logoutButtonAction:(id)sender
{
    LoginViewController *loginvc;
    if (myappDelegate.isiphone5)
    {
        loginvc = [[LoginViewController alloc]initWithNibName:@"LoginViewController" bundle:nil];
    }
    else
    {
        loginvc = [[LoginViewController alloc]initWithNibName:@"LoginViewController_iphone4" bundle:nil];
    }

    [myappDelegate insertStringInPlist:@"userid" value:@""];

    myappDelegate.naviCon = [[UINavigationController alloc]initWithRootViewController:loginvc];
    myappDelegate.naviCon.navigationBarHidden=YES;
    myappDelegate.window.rootViewController = myappDelegate.naviCon;

}

//  navigationBar backbutonAction
-(IBAction)backAction:(id)sender
{
    NSLog(@"self nav test %@",self.navigationController.viewControllers);
    [self.navigationController popViewControllerAnimated:YES];
}
-(IBAction)createListAction:(id)sender
{
    if (![paypalAddress isEqualToString:@""] && paypalAddress.length>3)
    {
        [self callNewListing];
    }
    else
    {
        [AppDelegate alertmethod:@"Warning" :@"PayPal Email Address is required for Creating A Listing, Please enter your PayPal Email Address in your profile."] ;
    }
}
-(void)callNewListing
{
    NewListingViewController *newListingVC;
    if (myappDelegate.isiphone5)
    {
        newListingVC = [[NewListingViewController alloc]initWithNibName:@"NewListingViewController" bundle:nil];
    }
    else
    {
        newListingVC = [[NewListingViewController alloc]initWithNibName:@"NewListingViewController_iphone4" bundle:nil];
    }
    [self.navigationController pushViewController:newListingVC animated:YES];
    
}
// Success response from Profile.
-(void)successResponseforProfile:(NSDictionary *)responseDict
{
    //dfde
   // NSLog(@"@@@@@Response Dictionary for Profile:%@",responseDict);
      dispatch_async( dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
    [self.itemsarray removeAllObjects];
    
    NSLog(@"For Profile Vc");
    NSLog(@"  ****  response ****  %@",responseDict);
    NSMutableDictionary *userdataDict=(NSMutableDictionary *)responseDict;
    Users *userObj=[[Users alloc] init];
    dispatch_async(dispatch_get_main_queue(), ^{

    userObj.address1String=[userdataDict objectForKey:@"address1"];
    userObj.address2String=[userdataDict objectForKey:@"address2"];
    userObj.cityString=[userdataDict objectForKey:@"city"];
    userObj.stateString=[userdataDict objectForKey:@"state"];
    userObj.zipcodeString=[userdataDict objectForKey:@"zipcode"];
    
    userObj.userIdStr=[userdataDict objectForKey:@"userId"];
    userObj.userNameStr= [userdataDict objectForKey:@"userName"];
    userObj.nameStr= [userdataDict objectForKey:@"name"];
    userObj.emailStr= [userdataDict objectForKey:@"email"];
    userObj.profilepicStr= [userdataDict objectForKey:@"profilepic"];
    
    NSLog(@"");
    userObj.passwordStr= [userdataDict objectForKey:@"password"];
    userObj.createdDateStr= [userdataDict objectForKey:@"createdDate"];
    userObj.loginUserIdStr= [userdataDict objectForKey:@"loginUserId"];
    userObj.itemsCountStr= [userdataDict objectForKey:@"itemsCount"];
    userObj.wishListCountStr= [userdataDict objectForKey:@"wishListCount"];
    userObj.followerCountStr= [userdataDict objectForKey:@"followerCount"];
    userObj.followingCountStr= [userdataDict objectForKey:@"followingCount"];
    userObj.isFollowStr= [userdataDict objectForKey:@"isFollow"];
    userObj.userListingArray=[[NSMutableArray alloc] init];
    userObj.paypalAddress=[userdataDict objectForKey:@"paypalEmailAddress"];
    
    userObj.notificationcountstr=[userdataDict objectForKey:@"notificationCount"];
     [APPDELEGATE insertStringInPlist:@"notificationcount" value:[NSString stringWithFormat:@"%@",[userdataDict objectForKey:@"notificationCount"]]];
    NSLog(@"the notification count is %@",[userdataDict objectForKey:@"notificationCount"]);
    userObj.messagecountstr=[userdataDict objectForKey:@"messageCount"];
    NSLog(@"the messageCount  is %@",[userdataDict objectForKey:@"messageCount"]);
    [APPDELEGATE insertStringInPlist:@"notificationcount" value:[NSString stringWithFormat:@"%@",[userdataDict objectForKey:@"notificationCount"]]];
         int notcount=[[userdataDict objectForKey:@"notificationCount"] intValue];
        
        if (notcount>9)
        {
            notificationview.hidden=NO;
            
            notificationcountuplabel.text=@"9+";
            
        }
        else if (notcount==0)
        {
            
            notificationview.hidden=YES;
            
        }
        else
        {
            notificationview.hidden=NO;
            
            notificationcountuplabel.text=[APPDELEGATE getStringFromPlist:@"notificationcount"];
        }

    int msgvalue=[userObj.messagecountstr intValue];
    if (msgvalue==0)
    {
        msgnotificationview.hidden=YES;
        //notificationview.hidden=YES;
    }
    else if(msgvalue>9)
    {
        msgnotificationcountLabel.text=@"9+";
        msgnotificationview.hidden=NO;
        //notificationcountuplabel.text=@"9+";
       // notificationview.hidden=NO;
    }
    else
    {
        msgnotificationview.hidden=NO;
        msgnotificationcountLabel.text=[NSString stringWithFormat:@"%@",userObj.messagecountstr];
      //  notificationview.hidden=NO;
      //  notificationcountuplabel.text=[NSString stringWithFormat:@"%@",userObj.messagecountstr];
    }
        

 
    NSArray *userArray=[userdataDict objectForKey:@"userListing"];
    NSLog(@"paypalEmailAddress %@",[userdataDict objectForKey:@"paypalEmailAddress"]);
    paypalAddress=[userdataDict objectForKey:@"paypalEmailAddress"];
    [APPDELEGATE insertStringInPlist:@"paypalstr" value:paypalAddress];
    [userObj.userListingArray addObjectsFromArray:[myappDelegate parselistingitem:userArray :NO :nil :NO]];
    
    userObj.usersWishArray=[NSMutableArray new];
    NSMutableArray *userWhistArray=[userdataDict objectForKey:@"userWishlist"];
    for (int i=0;i<userWhistArray.count;i++)
    {
        NSMutableArray *listArray=[[userWhistArray objectAtIndex:i] valueForKey:@"listUserListing"];
        for (int i=0;i<listArray.count;i++)
        {
            UserLists *listObj=[[UserLists alloc] init];
            listObj.userListingIdStr=[[listArray objectAtIndex:i] objectForKey:@"userListingId"];
            listObj.userItemIdStr=[[listArray objectAtIndex:i] objectForKey:@"userItemId"];
            listObj.categoryIdStr=[[listArray objectAtIndex:i] objectForKey:@"categoryId"];
            listObj.likestatus=[[[listArray objectAtIndex:i]objectForKey:@"likeStatus"]boolValue];
        NSLog(@"listObj.likestatus....%d",listObj.likestatus);
                                
            listObj.listPriceStr=[[listArray objectAtIndex:i] objectForKey:@"listPrice"];
            listObj.statusStr=[[listArray objectAtIndex:i] objectForKey:@"status"];
            //if ([[listArray objectAtIndex:i] objectForKey:@"status"])
            {
               // listObj.itemStatus=[[listArray objectAtIndex:i] objectForKey:@"status"];
                
            }
            listObj.inPersonStr=[[listArray objectAtIndex:i] objectForKey:@"inPerson"];
            listObj.uspsStr=[[listArray objectAtIndex:i] objectForKey:@"usps"];
            listObj.userIDStr=[[listArray objectAtIndex:i] objectForKey:@"userID"];
            listObj.unitPriceStr=[[listArray objectAtIndex:i] objectForKey:@"unitPrice"];
            listObj.categoryNameStr=[[listArray objectAtIndex:i] objectForKey:@"categoryName"];
            listObj.sizeIdStr=[[listArray objectAtIndex:i] objectForKey:@"sizeId"];
            listObj.sizeStr=[[listArray objectAtIndex:i] objectForKey:@"size"];
            listObj.brandIdStr=[[listArray objectAtIndex:i] objectForKey:@"brandId"];
            listObj.brandStr=[[listArray objectAtIndex:i] objectForKey:@"brand"];
            listObj.conditionIdStr=[[listArray objectAtIndex:i] objectForKey:@"conditionId"];
            listObj.conditionStr=[[listArray objectAtIndex:i] objectForKey:@"condition"];
            listObj.descriptionStr=[[listArray objectAtIndex:i] objectForKey:@"description"];
            listObj.itemNameStr=[[listArray objectAtIndex:i] objectForKey:@"itemName"];
            listObj.availableQuantityStr=[[listArray objectAtIndex:i] objectForKey:@"availableQuantity"];
            listObj.itemPicturesArray=[[NSMutableArray alloc] init];
            NSMutableArray *itemArray=[[listArray objectAtIndex:i] objectForKey:@"itemPictures"];
            for (int i=0;i<itemArray.count;i++)
            {
                Item *itemObj=[[Item alloc] init];
                
                itemObj.userItemPictureIdStr=[[itemArray objectAtIndex:i] objectForKey:@"userItemPictureId"];
                itemObj.userItemIdStr=[[itemArray objectAtIndex:i] objectForKey:@"userItemId"];
                itemObj.pictureCdnUrlStr=[[itemArray objectAtIndex:i] objectForKey:@"pictureCdnUrl"];
                itemObj.createdDateStr=[[itemArray objectAtIndex:i] objectForKey:@"createdDate"];
                
                [listObj.itemPicturesArray addObject:itemObj];
                
            }
            
            [userObj.usersWishArray addObject:listObj];
        }
    }
    [self.itemsarray addObject:userObj];
    
    Users *userObjRef=[self.itemsarray objectAtIndex:0];
    
    
    
    self.itemsCountLabel.text=[NSString stringWithFormat:@"%@",[responseDict valueForKey:@"itemsCount"]];
    
    if ([myappDelegate isnullorempty:self.itemsCountLabel.text]) {
        self.itemsCountLabel.text=@"0";
    }
    NSLog(@"userObjRef.wishListCountStr....%@",userObjRef.wishListCountStr);
    self.likesCountLabel.text=[NSString stringWithFormat:@"%@",userObjRef.wishListCountStr];
    if ([myappDelegate isnullorempty:self.likesCountLabel.text]) {
        self.likesCountLabel.text=@"0";
    }
    self.followersCountLabel.text=[NSString stringWithFormat:@"%@",[responseDict valueForKey:@"followerCount"]];
    
    if ([myappDelegate isnullorempty:self.followersCountLabel.text]) {
        self.followersCountLabel.text=@"0";
    }
    
    self.followingCountLabel.text=[NSString stringWithFormat:@"%@",[responseDict valueForKey:@"followingCount"]];
    
    if ([myappDelegate isnullorempty:self.followingCountLabel.text]) {
        self.followingCountLabel.text=@"0";
    }
    
    
    
    if([myappDelegate isnullorempty:userObjRef.userNameStr])
    {
        self.usernameLabel.text=@"";
    }
    else
    {
        usernameLabel.text=[NSString stringWithFormat:@"@%@",userObjRef.userNameStr];
    }
    
    nameLabel.text=userObjRef.nameStr;
    
    NSLog(@"--- user name %@",userObjRef.nameStr);
    if([myappDelegate isnullorempty:userObjRef.nameStr])
    {
        self.nameLabel.text=@"";
    }
    else
    {
        nameLabel.text=userObjRef.nameStr;
    }
    
           //notificationcountuplabel.text=[NSString stringWithFormat:@"%@",userObj.notificationcountstr];
    
        NSLog(@"notificationcountLabel.text.....%@",notificationcountLabel.text);
    
    NSString *picstr =[[responseDict valueForKey:@"profilepic"] stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
    
    [myappDelegate insertStringInPlist:@"profilePicture" value:picstr];
    if([self Contains:@"twimg" on:picstr])
    {
        self.profileImageView.contentMode=UIViewContentModeCenter;
        self.profileImageView.backgroundColor=[UIColor whiteColor];
    }

    [profileImageView setImageWithURL:[NSURL URLWithString:picstr] placeholderImage:[UIImage imageNamed:@"noimage_friend.png"] options:SDWebImageHighPriority];
    
    NSString *tempuserstr=[self.usernameLabel.text stringByReplacingOccurrencesOfString:@"@" withString:@""];
    
    [myappDelegate insertStringInPlist:usernamekey value:tempuserstr];
    
    //[appdelegate.userDetailsArray addObjectsFromArray:self.itemsarray];
    
    [self createscrollview];
    [myappDelegate stopspinner:self.view];
           });
      });
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

// Performs Actions push to  MyClosetViewController and BuyandSellViewController.
-(IBAction)sellerButtonAction:(id)sender
{
    if ([sender tag]== 10)
    {
        BOOL pushbool=YES;
        for (UIViewController *vcont in self
             .navigationController.viewControllers)
        {
            if ([vcont isKindOfClass:[MyClosetViewController class]])
            {
                pushbool=NO;
                MyClosetViewController *myclosv=(MyClosetViewController *)vcont;
                [self.navigationController popToViewController:myclosv animated:YES];
                break;
            }
        }
        if (pushbool)
        {
            
            MyClosetViewController *myclosetVC;
            if (myappDelegate.isiphone5)
            {
                myclosetVC = [[MyClosetViewController alloc]initWithNibName:@"MyClosetViewController" bundle:nil];
            }
            else
            {
                myclosetVC = [[MyClosetViewController alloc]initWithNibName:@"MyClosetViewController_iphone4" bundle:nil];
            }
            [self.navigationController pushViewController:myclosetVC animated:YES];
        }
       }
    
    else
    {
    if (myappDelegate.isiphone5)
    {
    BuyandSellViewController *sell=[[BuyandSellViewController alloc]initWithNibName:@"BuyandSellViewController" bundle:nil];
    
    if ([sender tag]==11)
    {
        sell.isbuyer=YES;
    }
    else{
        sell.isbuyer=NO;
    }
    
    [self.navigationController pushViewController:sell animated:YES];
    }
    
    else
    {
        
        BuyandSellViewController *sell=[[BuyandSellViewController alloc]initWithNibName:@"BuyandSellViewController_iphone4" bundle:nil];
        
        if ([sender tag]==11)
        {
            sell.isbuyer=YES;
        }
        else
        {
            sell.isbuyer=NO;
        }
        [self.navigationController pushViewController:sell animated:YES];
    }
    }
    
}

#pragma mark --- UIScrollView Method

-(void)createscrollview
{
    
    for (UIView *selfv in self.itemscrollview.subviews)
    {
        [selfv removeFromSuperview];
    }
    int x=15;
    int y=15;
    
    NSLog(@" items array count %d",self.itemsarray.count);
    
    Users *userObj=[self.itemsarray objectAtIndex:0];
    
    NSMutableArray *cartItemsArray=[[NSMutableArray alloc]initWithArray:userObj.userListingArray];
    
    UIView *mainview;

    
    for (int i=0; i<cartItemsArray.count+1; i++)
    {
        if (i==0)
        {
            mainview=[[UIView alloc]initWithFrame:CGRectMake(x, y, 138, 174)];
            mainview .backgroundColor=[UIColor clearColor];
            mainview.tag=0;
            
            UIButton *addButton=[[UIButton alloc]initWithFrame:CGRectMake(x, y, 138, 174)];
            addButton .backgroundColor=[UIColor clearColor];
            addButton.tag=0;
            [addButton addTarget:self action:@selector(createListAction:) forControlEvents:UIControlEventTouchUpInside];
            
            [mainview addSubview:addButton];
            
            UIImageView *border=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, mainview.frame.size.width, mainview.frame.size.height)];
            border.image=[UIImage imageNamed:@"Createlistinglayer.png"];
            
            [mainview addSubview:border];
            
            UIImageView *plusborder=[[UIImageView alloc]initWithFrame:CGRectMake(50, 30, 42, 42)];
            plusborder.image=[UIImage imageNamed:@"plusbtn.png"];
            
            [mainview addSubview:plusborder];
            
            UILabel *titlelbl=[[UILabel alloc]initWithFrame:CGRectMake(30, 100, 70, 45)];
            titlelbl.text=@"Create A Listing";
            titlelbl.numberOfLines=2;
            titlelbl.textAlignment=NSTextAlignmentCenter;
            titlelbl.textColor=[UIColor colorWithRed:12.0/255.0 green:174.0/255.0 blue:180.0/255.0 alpha:1.0];
            titlelbl.font=[UIFont fontWithName:@"Muli" size:17];
            
            [mainview addSubview:titlelbl];
            
            //plusbtn.png
            
            x=x+152;
        }
        else
        {
          
            ItemsModelobject *itemobj=[cartItemsArray objectAtIndex:i-1];
            NSLog(@"itemobj.likedbool...%d",itemobj.likedbool);
            
            UIView *mainview=[[UIView alloc]initWithFrame:CGRectMake(x, y, 138, 174)];
            mainview .backgroundColor=[UIColor clearColor];
            mainview.tag=i;
            
            UIImageView *border=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, mainview.frame.size.width, mainview.frame.size.height)];
            border.image=[UIImage imageNamed:@"Createlistinglayer.png"];
            
            [mainview addSubview:border];
            
            UIImageView *itemimgv=[[UIImageView alloc]initWithFrame:CGRectMake(1,1, 136, 118)];
            if (itemobj.picturearray.count<1)
            {
                itemimgv.image=[UIImage imageNamed:@"noimage.png" ];
            }
            else
            {
                NSLog(@"check the pic ocunt =%d",itemobj.picturearray.count);
                NSString *urlstr=[itemobj.picturearray objectAtIndex:0];
                NSLog(@"check teh str first %@",urlstr);
                NSURL *picurl=[NSURL URLWithString:urlstr];
                NSLog(@"now th e picture url %@",picurl);
                //NSData *dataimg=[NSData dataWithContentsOfURL:picurl];
                // itemimgv.image=[UIImage imageWithData:dataimg ];
                [itemimgv setImageWithURL:picurl             placeholderImage:[UIImage imageNamed:@"noimage.png"] options:SDWebImageCacheMemoryOnly];
                //itemimgv
            }
            [mainview addSubview:itemimgv];
            
            UILabel *titlelbl=[[UILabel alloc]initWithFrame:CGRectMake(0, 125, 138, 30)];
            titlelbl.text=[NSString stringWithFormat:@"%@",itemobj.itemNamestr];
            titlelbl.textAlignment=NSTextAlignmentCenter;
            titlelbl.textColor=[UIColor colorWithRed:12.0/255.0f green:174.0/255.0f blue:180.0/255.0f alpha:1.0f];
            titlelbl.font=[UIFont fontWithName:@"Muli" size:13];
            [mainview addSubview:titlelbl];
            
               UILabel *listpricelbl=[[UILabel alloc]initWithFrame:CGRectMake(2, 145, 138, 30)];
       //     NSLog(@"chec %@",itemobj.salecostcoststr);
            listpricelbl.text=[NSString stringWithFormat:@"$%@",itemobj.salecostcoststr];
            listpricelbl.textAlignment=NSTextAlignmentCenter;
            listpricelbl.font=[UIFont fontWithName:@"Muli" size:13];
           listpricelbl.textColor=[UIColor colorWithRed:12.0/255.0f green:174.0/255.0f blue:180.0/255.0f alpha:1.0f];
            [mainview addSubview:listpricelbl];
            
            
            UILabel *origlbl=[[UILabel alloc]initWithFrame:CGRectMake(73, 145, 70, 30)];
            
            origlbl.textAlignment=NSTextAlignmentLeft;
            
            origlbl.text=[NSString stringWithFormat:@"$%@",itemobj.listingcoststr];
            
            NSDictionary* attributes = @{  NSStrikethroughStyleAttributeName: [NSNumber numberWithInt:NSUnderlineStyleSingle]   };
            
            NSAttributedString* attrText = [[NSAttributedString alloc] initWithString:origlbl.text attributes:attributes];
            origlbl.attributedText=attrText;
            // origlbl.text=itemobj.listingcoststr;
            origlbl.font=[UIFont fontWithName:@"Muli" size:13];;
           origlbl.textColor=[UIColor colorWithRed:131.0/255.0f green:131.0/255.0f blue:131.0/255.0f alpha:1.0f];
            //[mainview addSubview:origlbl];
            
        
            UIButton *likebtn=[UIButton buttonWithType:UIButtonTypeCustom];
            
            likebtn.frame=CGRectMake(6, 6, 16, 16);
            if (itemobj.likedbool==YES)
            {
                [likebtn setImage:[UIImage imageNamed:@"like_on.png"] forState:UIControlStateNormal];//like_on.png
               
                
            }
            else
            {
                [likebtn setImage:[UIImage imageNamed:@"like_off.png"] forState:UIControlStateNormal];//like_off.png
               
                
            }
           
            
          //  [likebtn addTarget:self action:@selector(likebuttonaction:) forControlEvents:UIControlEventTouchUpInside];
            
            likebtn.tag=i;
           // [mainview addSubview:likebtn];
            //likebtn.backgroundColor=[UIColor redColor];
            
            
            UIButton *listdetailbtn=[UIButton buttonWithType:UIButtonTypeCustom];
            listdetailbtn.frame=CGRectMake(0, 28, mainview.frame.size.width, mainview.frame.size.height-25);
            [listdetailbtn addTarget:self action:@selector(listDetailsAction:) forControlEvents:UIControlEventTouchUpInside];
            listdetailbtn.tag=i;
            [mainview addSubview:listdetailbtn];
            NSLog(@"--Profile--sold is %@",itemobj.itemStatus);
            if ([itemobj.itemStatus isEqualToString:@"Sold"])
            {
                //listdetailbtn.userInteractionEnabled=NO;
                UIImageView *soldOutImageView=[[UIImageView alloc]initWithFrame:CGRectMake(50,5,91,17.5)];
                [soldOutImageView setImage:[UIImage imageNamed:@"sold.png"]];
                soldOutImageView.tag=18000;
                [mainview addSubview:soldOutImageView];
                [mainview bringSubviewToFront:soldOutImageView];
            }
            
//            if ((i+1)%2 == 0)
//            {
//                y=y+200;
//                x=15;
//            }
//            else
//            {
//                x=167;
//            }
            
            [itemscrollview addSubview:mainview];

            
            if ((i+1)%2 == 0)
            {
                y=y+200;
                x=15;
            }
            else
            {
                x=167;
            }
        }
        [self.itemscrollview addSubview:mainview];
    }
    
    if (y+220<itemscrollview.frame.size.height)
    {
        y=itemscrollview.frame.size.height-219;
    }
    [self.itemscrollview setContentSize:CGSizeMake(320, y+220)];
    
    
}

//performs Actions push to NotificationViewController.
-(IBAction)notificationclicked:(id)sender;
{
    NotificationViewController *notificationVC;
    
        if(myappDelegate.isiphone5==NO)
        {
            notificationVC=[[NotificationViewController alloc]initWithNibName:@"NotificationViewController_iphone4" bundle:nil];
        }
        else if([[UIScreen mainScreen] bounds].size.height == 568)
        {
            notificationVC=[[NotificationViewController alloc]initWithNibName:@"NotificationViewController" bundle:nil];
            
        }
     notificationVC.getlistarray=self.itemsarray;
    [APPDELEGATE insertStringInPlist:@"notificationcount" value:[NSString stringWithFormat:@"%d",0]];
    [self.navigationController pushViewController:notificationVC animated:YES];
}

//This Action is used in the Create Scrollview
-(IBAction)listDetailsAction:(id)sender
{
    //[self setmenudown];
    ListingDetailViewController *listingDetailVC;
    if ([AppDelegate isiphone5])
    {
        listingDetailVC = [[ListingDetailViewController alloc]initWithNibName:@"ListingDetailViewController" bundle:nil];
    }
    else
    {
        listingDetailVC = [[ListingDetailViewController alloc]initWithNibName:@"ListingDetailViewController_iphone4" bundle:nil];
    }
    
    Users *userObj=[self.itemsarray objectAtIndex:0];
    
    listingDetailVC.listingmainarray=userObj.userListingArray;
    listingDetailVC.senderIndex=[sender tag]-1;
 listingDetailVC.exchangestring=@"profile";
    listingDetailVC.condstring=@"Closet";
    [self.navigationController pushViewController:listingDetailVC animated:YES];
    
}


//Performs the Action animating the Menuview

-(IBAction)menuBtnAction:(id)sender
{
    notificationcountLabel.font=[UIFont fontWithName:@"Muli" size:11];
    [animationimgv stopAnimating];
    
    int notcount=[[APPDELEGATE getStringFromPlist:@"notificationcount"] intValue];
    
    if (notcount>9)
    {
        profilenotificationview.hidden=NO;
        notificationview.hidden=NO;
        
        notificationcountuplabel.text=@"9+";
         notificationcountLabel.text=@"9+";
        
    }
    else if (notcount==0)
    {
        profilenotificationview.hidden=YES;
        notificationview.hidden=YES;
        
    }
    else
    {
        notificationview.hidden=NO;
        profilenotificationview.hidden=NO;
        notificationcountuplabel.text=[APPDELEGATE getStringFromPlist:@"notificationcount"];
        notificationcountLabel.text=[APPDELEGATE getStringFromPlist:@"notificationcount"];

    }

    
    animationimgv.image=[UIImage imageNamed:@"hours_0022_Layer-1.png"];
    
    [animationimgv startAnimating];
    
    
    
    CGRect menu=menuview.frame;
    CGRect menuButtonframe=menubtnview.frame;
    
    
    
    if (myappDelegate.isiphone5)
    {
        if(ismenu==YES)
        {
            ismenu=NO;
            
            menu.origin.y=600;
            menuButtonframe.origin.y=470;
            
        }
        else
        {
            //menuButtonframe=CGRectMake(131, 250, 58, 59);
            
            ismenu=YES;
            
            //menuButtonframe.origin.y=menubtnview.frame.origin.y-250;
            menuButtonframe.origin.y=250;
            menu.origin.y=280;
            
        }
        
        self.view.userInteractionEnabled=YES;
        
    }
    else
    {
        if(ismenu==YES)
        {
            
            ismenu=NO;
            
            menuButtonframe=CGRectMake(131,355, 58, 59);
            
            
            menu.origin.y=menuview.frame.origin.y+482;
            
        }
        else
        {
            menuButtonframe=CGRectMake(131, 160, 58, 59);
            
            ismenu=YES;
            menu=menuview.frame;
            menu.origin.y=190;
        }
    }
    
    [UIView animateWithDuration:0.5 animations:^
     {
         
         menuview.frame=menu;
         menubtnview.frame=menuButtonframe;
         
         // [self runSpinAnimationOnView:self.menuBtn duration:1.0 rotations:1.0 repeat:0];
         
     }
                     completion:^(BOOL finished){
                         
                         //[animationimgv stopAnimating];
                         
                         // animationimgv.image=[UIImage imageNamed:@"hours_0022_Layer-1.png"];
                     }];
    
    self.view.userInteractionEnabled=YES;
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 1.0 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
        [animationimgv stopAnimating];
        
        animationimgv.image=[UIImage imageNamed:@"hours_0022_Layer-1.png"];
    });
    
    
}
/*
-(IBAction)menuBtnAction:(id)sender
{
    
    CGRect menu;
    CGRect menuButtonframe;
    
    if (myappDelegate.isiphone5)
    {
        if(ismenu==YES)
        {
            ismenu=NO;
            // menuBtn.frame=CGRectMake(131,470, 58, 59);
            menuButtonframe=CGRectMake(131,470, 58, 59);
            menu=menuview.frame;
            menu.origin.y=menuview.frame.origin.y+600;
            self.view.userInteractionEnabled=NO;
        }
        else
        {
            menuButtonframe=CGRectMake(131, 250, 58, 59);
            ismenu=YES;
            menu=menuview.frame;
            menu.origin.y=280;
        }
        self.view.userInteractionEnabled=YES;
        
    }
    else
    {
        if(ismenu==YES)
        {
            
            ismenu=NO;
            
            menuButtonframe=CGRectMake(131,355, 58, 59);
            menu=menuview.frame;
            menu.origin.y=menuview.frame.origin.y+482;
            // menuview.frame=menu;
            self.view.userInteractionEnabled=NO;
            
        }
        else
        {
            menuButtonframe=CGRectMake(131, 160, 58, 59);
            ismenu=YES;
            
            menu=menuview.frame;
            menu.origin.y=190;
        }
    }
    
    [UIView animateWithDuration:0.5 animations:^{
        
           menuview.frame=menu;
        menuBtn.frame=menuButtonframe;
        
    } completion:^(BOOL finished) {
        
    }];
    
    self.view.userInteractionEnabled=YES;
    
}

*/

// animation for menu button
-(void)setmenudown
{
    CGRect menu=menuview.frame;
    CGRect menuButtonframe=menubtnview.frame;
    if (myappDelegate.isiphone5)
    {
        if(ismenu==YES)
        {
            ismenu=NO;
            // menuBtn.frame=CGRectMake(131,470, 58, 59);
            menuButtonframe=CGRectMake(131,470, 58, 59);
            menu=menuview.frame;
            menu.origin.y=menuview.frame.origin.y+600;
            // menuview.frame=menu;
            self.view.userInteractionEnabled=NO;
        }
        self.view.userInteractionEnabled=YES;
    }
    else
    {
        if(ismenu==YES)
        {
            
            
            ismenu=NO;
            
            // menuBtn.frame=CGRectMake(131,355, 58, 59);
            menuButtonframe=CGRectMake(131,355, 58, 59);
            menu=menuview.frame;
            menu.origin.y=menuview.frame.origin.y+482;
            // menuview.frame=menu;
            self.view.userInteractionEnabled=NO;
            
        }
    }
    [UIView animateWithDuration:0.5 animations:^{
        
        //        CGRect rect= usersTableview.frame;
        //        rect.origin.y=64;
        //        usersTableview.frame=rect;
        
        
        menuview.frame=menu;
        menubtnview.frame=menuButtonframe;
        
    } completion:^(BOOL finished) {
        
    }];
    
    self.view.userInteractionEnabled=YES;
    
}

//performs the Action in the menu
-(IBAction)filterButtonTapped:(id)sender
{
    [self setmenudown];;
    
    if ([sender tag]==330)
    {
        if (myappDelegate.cartGlobalarray.count>0)
        {
            // ff
            CartViewController *cartVC;
            if (myappDelegate.isiphone5)
            {
                cartVC = [[CartViewController alloc]initWithNibName:@"CartViewController" bundle:nil];
            }
            else
            {
                cartVC = [[CartViewController alloc]initWithNibName:@"CartViewController_iphone4" bundle:nil];
            }
            
            ItemsModelobject *sltobj=[myappDelegate.cartGlobalarray objectAtIndex:0];
            cartVC.itemsModelObj=sltobj;
           // cartVC.cartArray=[[NSMutableArray alloc]init];
            
            //    [cartVC.cartArray addObject:appdelegate.cartglobalimage];
            
            [self.navigationController pushViewController:cartVC animated:YES];
            
        }
        else{
            
            [AppDelegate alertmethod:appname :@"Your cart is empty. Please add listing to cart."];
        }
        
    }
    else{
             if (myappDelegate.isiphone5)
        {
            FilterViewController *filterVC=[[FilterViewController alloc] initWithNibName:@"FilterViewController" bundle:nil];
            [self.navigationController pushViewController:filterVC animated:YES];
        }
        else{
            FilterViewController *filterVC=[[FilterViewController alloc] initWithNibName:@"FilterViewController_iphone4" bundle:nil];
            [self.navigationController pushViewController:filterVC animated:YES];
        }
    }
    
}

//performs the Action in the menu

-(IBAction)menuItemTapped:(id)sender
{
    [self setmenudown];
    switch ([sender tag]) {
        case 1:
        {
            
                   }
            break;
        case 2: // my coset
        {
            BOOL isVCExist=NO;
            NSArray *arr=self.navigationController.viewControllers;
            for (UIViewController *childvc in arr) {
                
                if([childvc isKindOfClass:[MyClosetViewController class]])
                {
                    isVCExist=YES;
                    UIViewController *subvc=(MyClosetViewController *)childvc;
                    [self.navigationController popToViewController:subvc animated:YES];
                }
            }
            
            if(!isVCExist)
            {
                MyClosetViewController *myclosetVC;
                if (myappDelegate.isiphone5)
                {
                    myclosetVC = [[MyClosetViewController alloc]initWithNibName:@"MyClosetViewController" bundle:nil];
                }
                else
                {
                    myclosetVC = [[MyClosetViewController alloc]initWithNibName:@"MyClosetViewController_iphone4" bundle:nil];
                }
                [self.navigationController pushViewController:myclosetVC animated:YES];
            }
        }
            break;
            
        case 3: // myfeed
        {
            BOOL isVCExist=NO;
            NSArray *arr=self.navigationController.viewControllers;
            for (UIViewController *childvc in arr) {
                
                if([childvc isKindOfClass:[MyFeedViewController class]])
                {
                    isVCExist=YES;
                    UIViewController *subvc=(MyFeedViewController *)childvc;
                    [self.navigationController popToViewController:subvc animated:YES];
                }
            }
            
            if(!isVCExist)
            {
                MyFeedViewController *profileVC;
                if (myappDelegate.isiphone5)
                {
                    profileVC = [[MyFeedViewController alloc]initWithNibName:@"MyFeedViewController" bundle:nil];
                }
                else
                {
                    profileVC = [[MyFeedViewController alloc]initWithNibName:@"MyFeedViewController_iphone4" bundle:nil];
                }
                [self.navigationController pushViewController:profileVC animated:YES];
            }

           // [self menuBtnAction:self];
        }
            break;
        case 4: // my wish list
        {
            BOOL isVCExist=NO;
            NSArray *arr=self.navigationController.viewControllers;
            for (UIViewController *childvc in arr) {
                
                if([childvc isKindOfClass:[MyWishlistViewController class]])
                {
                    isVCExist=YES;
                    UIViewController *subvc=(MyWishlistViewController *)childvc;
                    [self.navigationController popToViewController:subvc animated:YES];
                }
            }
            
            if(!isVCExist)
            {
                
                MyWishlistViewController *mywishlistVC;
                if (myappDelegate.isiphone5)
                {
                    mywishlistVC = [[MyWishlistViewController alloc]initWithNibName:@"MyWishlistViewController" bundle:nil];
                }
                else
                {
                    mywishlistVC = [[MyWishlistViewController alloc]initWithNibName:@"MyWishlistViewController_iphone4" bundle:nil];
                }
                [self.navigationController pushViewController:mywishlistVC animated:YES];
            }
        }
            break;
            
        case 5:
        {
            BOOL isVCExist=NO;
            NSArray *arr=self.navigationController.viewControllers;
            for (UIViewController *childvc in arr) {
                
                if([childvc isKindOfClass:[SuggestionsViewController class]])
                {
                    isVCExist=YES;
                    UIViewController *subvc=(SuggestionsViewController *)childvc;
                    [self.navigationController popToViewController:subvc animated:YES];
                }
            }
            
            if(!isVCExist)
            {
                
                if ([AppDelegate isiphone5])
                {
                    myappDelegate.suggestionviewcont = [[SuggestionsViewController alloc]initWithNibName:@"SuggestionsViewController" bundle:nil];
                }
                else
                {
                    myappDelegate.suggestionviewcont = [[SuggestionsViewController alloc]initWithNibName:@"SuggestionsViewController_iphone4" bundle:nil];
                }
                [self.navigationController pushViewController:myappDelegate.suggestionviewcont animated:YES];
                /* HomeViewController *homeVC;
                 if (appdelegate.isiphone5)
                 {
                 homeVC = [[HomeViewController alloc]initWithNibName:@"HomeViewController" bundle:nil];
                 }
                 else
                 {
                 homeVC = [[HomeViewController alloc]initWithNibName:@"HomeViewController_iphone4" bundle:nil];
                 }
                 [self.navigationController pushViewController:homeVC animated:YES];
                 */
            }
        }
            break;
        case 6:
        {
            BOOL isVCExist=NO;
            NSArray *arr=self.navigationController.viewControllers;
            for (UIViewController *childvc in arr) {
                
                if([childvc isKindOfClass:[NewListingViewController class]])
                {
                    isVCExist=YES;
                    UIViewController *subvc=(NewListingViewController *)childvc;
                    [self.navigationController popToViewController:subvc animated:YES];
                }
            }
            
            if(!isVCExist)
            {
                
                if (![[APPDELEGATE getStringFromPlist:@"paypalstr"] isEqualToString:@""] && [APPDELEGATE getStringFromPlist:@"paypalstr"].length>3)
                {
                    
                    NewListingViewController *mywishlistVC;
                    if (myappDelegate.isiphone5)
                    {
                        mywishlistVC = [[NewListingViewController alloc]initWithNibName:@"NewListingViewController" bundle:nil];
                    }
                    else
                    {
                        mywishlistVC = [[NewListingViewController alloc]initWithNibName:@"NewListingViewController_iphone4" bundle:nil];
                    }
                    
                    [self.navigationController pushViewController:mywishlistVC animated:YES];
                    
                }
                else
                {
                    [AppDelegate alertmethod:@"Warning" :@"PayPal Email Address is required for Creating A Listing, Please enter your PayPal Email Address in your profile."] ;
                }
                
            }
        }
            break;
            
        default:
            break;
    }
}
-(void)internetConnection
{
    UIAlertView *Internetalert=[[UIAlertView alloc]initWithTitle:appname message:@"Internet connection not available" delegate:self cancelButtonTitle:nil otherButtonTitles:@"Ok", nil];
    [Internetalert show];
    self.view .userInteractionEnabled=YES;
    [APPDELEGATE stopspinner:self.view];
    
}

-(void)errorMessage:(NSString *)message
{
    
    if([message isEqualToString:@"message"])
    {
        
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:appname message:message delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }
}
-(IBAction)notificationcount11:(id)sender
{
      //[myappDelegate stopspinner:self.view];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, kNilOptions), ^{
    registrationRep.delegate=self;
    [registrationRep getnotificationcount:[APPDELEGATE getStringFromPlist:@"userid"]];
       //[myappDelegate stopspinner:self.view];
    });
      [myappDelegate stopspinner:self.view];

}
-(void)successresponseofnotificationcount:(NSMutableDictionary *)responsedict
{
    NSLog(@"responsedict----------> %@",responsedict);
       [myappDelegate stopspinner:self.view];
    if([[responsedict valueForKey:@"message"]isEqualToString:@"Notification Info Successfully"])
    {
        dispatch_async(dispatch_get_main_queue(), ^{

        [APPDELEGATE insertStringInPlist:@"notificationcount" value:[NSString stringWithFormat:@"%@",[responsedict objectForKey:@"notificationCount"]]];

        int msgval=[[responsedict valueForKey:@"messageCount"] intValue];
        NSLog(@"the count is %d",msgval);
        
        
    int value1 = [[NSString stringWithFormat:@"%@",[responsedict objectForKey:@"notificationCount"]] intValue];
        if (value1>9)
        {
            notificationcountuplabel.text=@"9+";
            notificationcountLabel.text=@"9+";
            profilenotificationview.hidden=NO;
            notificationview.hidden=NO;
            
            
        }
        else if (value1==0)
        {
            profilenotificationview.hidden=YES;
            notificationview.hidden=YES;
            
        }
        else
        {
            profilenotificationview.hidden=NO;
            notificationview.hidden=NO;
            
            notificationcountuplabel.text=[NSString stringWithFormat:@"%@",[APPDELEGATE getStringFromPlist:@"notificationcount"]];
            notificationcountLabel.text=[NSString stringWithFormat:@"%@",[APPDELEGATE getStringFromPlist:@"notificationcount"]];
        }

        if (msgval>9)
        {
            msgnotificationview.hidden=NO;
            msgnotificationcountLabel.text=@"9+";
            
        }
        else if (msgval==0)
        {
            msgnotificationview.hidden=YES;

        }
        else
        {
            msgnotificationview.hidden=NO;

            msgnotificationcountLabel.text=[NSString stringWithFormat:@"%@",[responsedict valueForKey:@"messageCount"]];
        }

      });
        
      

       // [self performSelector:@selector(notificationcount11:) withObject:nil afterDelay:1];
   
            
         }
    else
    {
        NSLog(@"notificationcountlabel.text....%@",[responsedict valueForKey:@"message"]);
    }
 

    // [self performSelector:@selector(notificationcount11:) withObject:nil afterDelay:1];
  

   
}

@end
