//
//  InviteCustomCell.h
//  MarkidPlace
//
//  Created by Varma Bhupatiraju on 17/09/14.
//  Copyright (c) 2014 stellentmac1. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface InviteCustomCell : UITableViewCell
@property(nonatomic,strong) IBOutlet UILabel *invitelbl;
@property(nonatomic,strong) IBOutlet UIImageView *inviteimgview;
@end
