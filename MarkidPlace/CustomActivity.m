//
//  CustomActivity.m
//  MarkidPlace
//
//  Created by stellent on 27/05/14.
//  Copyright (c) 2014 stellentmac1. All rights reserved.
//

#import "CustomActivity.h"

@implementation CustomActivity

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        
        imageview_spinner=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 100, 100)];
        imageview_spinner.image=[UIImage imageNamed:@"markid_activity.png"];
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

-(void)startactivity:(UIView *)otherview
{
    imageview_spinner.center=otherview.center;
    [otherview addSubview:imageview_spinner];
    
	CABasicAnimation *fullRotation;
	fullRotation = [CABasicAnimation animationWithKeyPath:@"transform.rotation"];
	fullRotation.fromValue = [NSNumber numberWithFloat:0];
	fullRotation.toValue = [NSNumber numberWithFloat:((360 * M_PI) / 180)];
	fullRotation.duration = 3.0;
    fullRotation.repeatCount = MAXFLOAT;
	[imageview_spinner.layer addAnimation:fullRotation forKey:@"360"];
}
- (void)stopAllAnimations
{
	[imageview_spinner.layer removeAllAnimations];
    
};
-(void)stopactivity
{
    [self stopAllAnimations];
    [imageview_spinner removeFromSuperview];
}

@end
