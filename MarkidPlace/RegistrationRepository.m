//
//  RegistrationRep/Users/stellentsoftware/Documents/ismail/markidplace/MarkidPlace/MarkidPlace/AppDelegate.hository.m
//  WonUp
//
//  Created by stellentmac1 on 5/15/14.
//  Copyright (c) 2014 stellentmac1. All rights reserved.
//

#import "RegistrationRepository.h"

@implementation RegistrationRepository
@synthesize delegate;

@synthesize successarray,con,mdata,mutableurlrequest;


// posting data to webservice/// by register and login views
-(void)postAllDetails :(NSMutableDictionary *)allDetailsdict :(NSString *)type :(NSString *)method
{
   // myappDelegate=(AppDelegate *)[[UIApplication sharedApplication]delegate];
    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    if (networkStatus == NotReachable)
    {
        [delegate internetConnection];
        ////NSLog(@"There IS NO internet connection");
    }
    else
    {
    NSError *jsonSerializationError = nil;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:allDetailsdict options:NSJSONWritingPrettyPrinted error:&jsonSerializationError];
    
   // NSLog(@"*** allDetailsdict *** %@",allDetailsdict);
        
    if(!jsonSerializationError)
    {
        NSString *serJSON = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        NSLog(@"Serialized JSON: %@", serJSON);
    }
    else
    {
        NSLog(@"JSON Encoding Failed: %@", [jsonSerializationError localizedDescription]);
    }
    
    
    //http://wonup.testshell.net/api/Register/Signup?format=json  [POST]
    
    NSString *urlstr;
    if ([type isEqualToString:@"Registration"])
    {
        urlstr = [NSString stringWithFormat:@"%@Register/SignUp?formate=json",MainUrl];
    }
    else if ([type isEqualToString:@"Login"])
    {
       urlstr = [NSString stringWithFormat:@"%@Account/ValidateLogin?format=json",MainUrl];
    }
    else if ([type isEqualToString:@"UserUpdate"])
    {
        urlstr = [NSString stringWithFormat:@"%@Account/UpdateUserProfile?format=json",MainUrl];
    }
    
    NSURL *url = [NSURL URLWithString:urlstr];
    
    NSLog(@"postAllDetails url ----%@",url);
    
    mutableurlrequest = [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:6000.0];
    
   /* if ([type isEqualToString:@"UserUpdate"])
    {
        [request setHTTPMethod:@"PUT"];
    }
    else
    {
        [request setHTTPMethod:@"POST"];
    }
    */
    if ([myappDelegate isnullorempty:method] )
    {
        method=@"GET";
    }
    [mutableurlrequest setHTTPMethod:method];
    
    
    [mutableurlrequest setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [mutableurlrequest setHTTPBody:jsonData];
    
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    
    [NSURLConnection sendAsynchronousRequest:mutableurlrequest queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         if (error)
         {
             [delegate errorMessage:@"message"];
         }
         else
         {
             NSError *jsonParsingError = nil;
             
             NSDictionary *mainDict = [NSJSONSerialization JSONObjectWithData:data options:0 error:&jsonParsingError];
             
             if (jsonParsingError)
             {
                 NSLog(@"JSON ERROR: %@", [jsonParsingError localizedDescription]);
                 [delegate errorMessage:@"message"];
             }
             else
             {
                  [delegate successResponseforRegistration:mainDict];
                 /*
                 dispatch_sync(dispatch_get_main_queue(), ^{
                       [delegate successResponseforRegistration:mainDict];
                 });*/
               
             }
             
         }
         
     }];
    }
}
//new registration
-(void)newregistration :(NSDictionary *)regDict
{
    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    if (networkStatus == NotReachable)
    {
        [delegate internetConnection];
        ////NSLog(@"There IS NO internet connection");
    }
    else
    {
        NSError *jsonSerializationError = nil;
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:regDict options:NSJSONWritingPrettyPrinted error:&jsonSerializationError];
        
        // NSLog(@"*** allDetailsdict *** %@",allDetailsdict);
        
        if(!jsonSerializationError)
        {
            NSString *serJSON = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
            NSLog(@"Serialized JSON: %@", serJSON);
        }
        else
        {
            NSLog(@"JSON Encoding Failed: %@", [jsonSerializationError localizedDescription]);
        }
        
        //http://markidplace.testshell.net/api/RegisterUser?format=json
        
        NSString *urlstr = [NSString stringWithFormat:@"%@RegisterUser?format=json",MainUrl];
        
        NSURL *url = [NSURL URLWithString:urlstr];
        
        NSLog(@"newregistration url ----%@",url);
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:6000.0];
        
        [request setHTTPMethod:@"POST"];
        
        
        [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        [request setHTTPBody:jsonData];
        
        NSOperationQueue *queue = [[NSOperationQueue alloc] init];
        
        [NSURLConnection sendAsynchronousRequest:request queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
         {
             if (error)
             {
                 [delegate errorMessage:@"message"];
             }
             else
             {
                 NSError *jsonParsingError = nil;
                 
                 NSDictionary *responseDict = [NSJSONSerialization JSONObjectWithData:data options:0 error:&jsonParsingError];
                 
                 NSLog(@"**** responseDict **** %@",responseDict);
                 
                 if (jsonParsingError)
                 {
                     NSLog(@"JSON ERROR: %@", [jsonParsingError localizedDescription]);
                     [delegate errorMessage:@"message"];
                 }
                 else
                 {
                     //  [delegate successResponseforPaypal:mainDict];
                     if ([responseDict objectForKey:@"message"])
                     {
                         
                         [delegate successResponseNewRegistrastion:responseDict];
                         
                         
                     }
                     else
                         [delegate errorMessage:@"message"];
                 }
                 
             }
             
         }];
    }
    
    
    
}
//geeting weight
-(void)getweightnewlisting:(NSString*)weight
{
    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    if (networkStatus == NotReachable)
    {
        [delegate internetConnection];
        ////NSLog(@"There IS NO internet connection");
    }
    else
    {
 
    
    //NSString *urlstr = [NSString stringWithFormat:@"%@ShippingPrice/%@?format=json",MainUrl,weight];
        NSString *urlstr = [NSString stringWithFormat:@"%@ShippingPrice?Weight=%@&format=json",MainUrl,weight];

    NSURL *url = [NSURL URLWithString:urlstr];
    
    NSLog(@"getweightnewlisting url:%@",url);
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:3600.0];
    
    [request setHTTPMethod:@"GET"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    
    [NSURLConnection sendAsynchronousRequest:request queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         if (error)
         {
             [delegate errorMessage:@"message"];
         }
         else
         {
             NSError *jsonParsingError = nil;
             
             NSDictionary *responseDict=[NSJSONSerialization JSONObjectWithData:data options:0 error:&jsonParsingError];
             
             if (jsonParsingError)
             {
                 //  NSLog(@"JSON ERROR: %@", [jsonParsingError localizedDescription]);
                 [delegate errorMessage:@"message"];
             }
             else
             {
                 // NSLog(@"Users mainDict is %@",mainDict);
                 // [delegate successResponseforNotification:mainDict];
                 
                 NSLog(@"responseDict...%@",responseDict);
                 //[delegate successResponsegetbrandsettings:dic];
                 [delegate successresponceweight:responseDict];
             }
         }
     }];
    
    }

    
}





// posting and getting data from chart and messages
-(void)postwithNSurl :(NSDictionary *)allDetailsdict :(NSString *)type :(NSString *)method :(BOOL)postdict
{
    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    if (networkStatus == NotReachable)
    {
        [delegate internetConnection];
        ////NSLog(@"There IS NO internet connection");
    }
    else
    {
  
    NSURL  *url;
    if ([type isEqualToString:@"Postchatting"])
    {
        NSString *urlstr=[NSString stringWithFormat:@"%@PostNotification?format=json",MainUrl];
        
       url =[NSURL URLWithString:urlstr ];
    }
    else if ([type isEqualToString:@"GetChatting"])
    {
      //  GetNotification?LoginUserId=1&format=json
        //GetNotification?LoginUserId=%@&count=%@&format=json
        
        //GetNotification?UserId=2
        
        NSString *urlstr=[NSString stringWithFormat:@"%@GetNotification?UserId=%@&format=json",MainUrl,myappDelegate.useridstring ];
        url =[NSURL URLWithString:urlstr ];
    }
    else if ([type isEqualToString:@"Gethistory"])
    {
        //GetNotificationHistory?BuyerUserId=%@&&SellerUserId=%@&format=json
        
            NSString *urlstr=[NSString stringWithFormat:@"%@GetNotificationHistory?UserId=%@&MessangerUserId=%@&format=json",MainUrl,myappDelegate.useridstring,[allDetailsdict valueForKey:@"BuyerUserId"] ];
            url =[NSURL URLWithString:urlstr ];
        
    }
    
    
    NSError *jsonSerializationError = nil;
    NSData *jsonData;
    
    if (postdict)
    {
        jsonData = [NSJSONSerialization dataWithJSONObject:allDetailsdict options:NSJSONWritingPrettyPrinted error:&jsonSerializationError];
    }
    
   // NSLog(@"*** allDetailsdict *** %@ and json data %@",allDetailsdict,jsonData);
    
    if(!jsonSerializationError)
    {
       // NSString *serJSON = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        //NSLog(@"Serialized JSON: %@", serJSON);
        mutableurlrequest = [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:6000.0];
        NSLog(@"postwithNSurl url %@ and method %@",url,method);
        [mutableurlrequest setHTTPMethod:method];
        
        
        [mutableurlrequest setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        if (postdict)
        {
            [mutableurlrequest setHTTPBody:jsonData];
            
            NSLog(@"check the dict %@ ",allDetailsdict);
        }
        
        
        NSOperationQueue *queue = [[NSOperationQueue alloc] init];
        
        [NSURLConnection sendAsynchronousRequest:mutableurlrequest queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
         {
             if (error)
             {
                 dispatch_sync(dispatch_get_main_queue(), ^{
                     [delegate errorMessage:[error localizedDescription] ];
                 });
             }
             else
             {
                 NSError *jsonParsingError = nil;
                 
                 NSDictionary *mainDict = [NSJSONSerialization JSONObjectWithData:data options:0 error:&jsonParsingError];
                 
                 if (jsonParsingError)
                 {
                     NSLog(@"JSON ERROR: %@", [jsonParsingError localizedDescription]);
                     dispatch_sync(dispatch_get_main_queue(), ^{
                         [delegate errorMessage:[mainDict valueForKey:@"message"] ];
                     });
                     
                 }
                 else
                 {
                     dispatch_sync(dispatch_get_main_queue(), ^{
                         
                         [delegate successresponceListing:mainDict :type];
                     });
                 }
             }
             
         }];

    }
    else
    {
        [delegate errorMessage:[jsonSerializationError localizedDescription] ];
        NSLog(@"JSON Encoding Failed: %@", [jsonSerializationError localizedDescription]);
    }
    
    }

    
}

/// this method is used  from listed views to get and post data
/* buysell, chart , filter, followers, home, listing detail, message, closet, wishlist, feed, new listing, suggestions view*/



-(void)getdata :(NSDictionary *)allDetailsdict :(NSString *)type :(NSString *)method withcount:(NSString *)count
{
    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    NSLog(@"the status is %d",networkStatus);
    if (networkStatus == NotReachable)
    {
        [delegate internetConnection];
        
        ////NSLog(@"There IS NO internet connection");
    }
    else
   {
       
        
        NSURL *url;
        
       
           typestring=type;
            NSString *urlstr;
            AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
            manager.responseSerializer=[AFJSONResponseSerializer serializer];
           [manager.requestSerializer setTimeoutInterval:3000];
         if([type isEqualToString:@"getcatogery"])
         {
            //  url = [NSURL URLWithString:[NSString stringWithFormat:@"%@GetCategorys?format=json",MainUrl]];
            urlstr =[NSString stringWithFormat:@"%@Sell?format=json",MainUrl];
             NSLog(@"getcatogery  url is:======> %@",urlstr);

             [manager GET:urlstr parameters:nil success:^(NSURLSessionDataTask *task, id responseObject)
              {
                  NSLog(@"JSON: %@", responseObject);
                  NSMutableDictionary *dict=(NSMutableDictionary*)responseObject;
                  
                  [delegate successresponceListing:dict :typestring];

                 
                  
              }
                  failure:^(NSURLSessionDataTask *operation, NSError *error)
              {
                  //NSLog(@"Error: %@", error);
                  NSLog(@"failed error is %@",error);
                  // [self.delegate  successresponse:stringval];
                  UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error!" message:[error localizedDescription] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                  //alertView.tag=19;
                  [alertView show];
                  
              }];
            
         }
         else if([type isEqualToString:@"getallLikes"])
         {
             
             
             
             urlstr =[NSString stringWithFormat:@"%@UserWishlist?UserId=%@&format=json",MainUrl,myappDelegate.useridstring];
             NSLog(@"getallLikes url is:======> %@",urlstr);
             [manager GET:urlstr parameters:nil success:^(NSURLSessionDataTask *task, id responseObject)
              {
                  NSLog(@"JSON: %@", responseObject);
                  NSMutableDictionary *dict=(NSMutableDictionary*)responseObject;
                  
                  [delegate successresponceListing:dict :typestring];

                  
                  
              }
                  failure:^(NSURLSessionDataTask *operation, NSError *error)
              {
                  NSLog(@"failed error is %@",error);
                  UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error!" message:[error localizedDescription] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                  [alertView show];
                  
              }];
         }
         else if([type isEqualToString:@"Myfeeds"])
         {
             
            
             urlstr =[NSString stringWithFormat:@"%@MyFeed?UserId=%@&format=json",MainUrl,myappDelegate.useridstring];
             NSLog(@"getallLikes url is:======> %@",urlstr);
             [manager GET:urlstr parameters:nil success:^(NSURLSessionDataTask *task, id responseObject)
              {
                  NSLog(@"JSON: %@", responseObject);
                  NSMutableDictionary *dict=(NSMutableDictionary*)responseObject;
                  
                  NSString *type=typestring;
                  if ([responseObject isKindOfClass:[NSArray class]])
                  {
                      NSArray *arr=(NSArray*)responseObject;
                      if (arr.count>0)
                      {
                          NSDictionary *subdict=[arr objectAtIndex:0];
                          if ([subdict objectForKey:@"key"])
                          {
                              if ([[subdict objectForKey:@"key"] isEqualToString:@""])
                              {
                                  type=@"Myfeeds";
                              }
                          }
                      }
                  }
                  
                  [delegate successresponceListing:dict :typestring];
                  
                  
                  
              }
                  failure:^(NSURLSessionDataTask *operation, NSError *error)
              {
                  NSLog(@"failed error is %@",error);
                  UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error!" message:[error localizedDescription] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                  [alertView show];
                  
              }];
         }
         else if([type isEqualToString:@"getListing"])
         {
            
            //My suggestions
            
             urlstr =[NSString stringWithFormat:@"%@UserListing?UserId=%@&key=mysuggestions&startCount=0&format=json",MainUrl,myappDelegate.useridstring];
             NSLog(@"getallLikes url is:======> %@",urlstr);
             [manager GET:urlstr parameters:nil success:^(NSURLSessionDataTask *task, id responseObject)
              {
                  NSLog(@"JSON: %@ %@", responseObject,task);
                  
                  NSMutableDictionary *dict=(NSMutableDictionary*)responseObject;
                  NSString *type=typestring;
                  if ([responseObject isKindOfClass:[NSArray class]])
                  {
                      NSArray *arr=(NSArray*)responseObject;
                      if (arr.count>0)
                      {
                          NSDictionary *subdict=[arr objectAtIndex:0];
                          if ([subdict objectForKey:@"service"])
                          {
                              if ([[subdict objectForKey:@"service"] isEqualToString:@"mysuggestions"])
                              {
                                  type=@"getListing";
                              }
                          }
                      }
                  }
                  [delegate successresponceListing:dict :type];

                  
                  
              }
                  failure:^(NSURLSessionDataTask *operation, NSError *error)
              {
                  NSLog(@"failed error is %@",error);
                  UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error!" message:[error localizedDescription] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                  [alertView show];
                  
              }];
        }
         else if([type isEqualToString:@"getListingMore"])
         {
             
             //My suggestions
             
             urlstr =[NSString stringWithFormat:@"%@UserListing?UserId=%@&key=mysuggestions&startCount=%@&format=json",MainUrl,myappDelegate.useridstring,count];
             NSLog(@"getallLikes url is:======> %@",urlstr);
             [manager GET:urlstr parameters:nil success:^(NSURLSessionDataTask *task, id responseObject)
              {
                  NSLog(@"JSON: %@ %@", responseObject,task);
                  
                  NSMutableDictionary *dict=(NSMutableDictionary*)responseObject;
                  NSString *type=typestring;
                  if ([responseObject isKindOfClass:[NSArray class]])
                  {
                      NSArray *arr=(NSArray*)responseObject;
                      if (arr.count>0)
                      {
                          NSDictionary *subdict=[arr objectAtIndex:0];
                          if ([subdict objectForKey:@"service"])
                          {
                              if ([[subdict objectForKey:@"service"] isEqualToString:@"mysuggestions"])
                              {
                                  type=@"getListingMore";
                              }
                          }
                      }
                  }
                  [delegate successresponceListing:dict :type];
                  
                  
                  
              }
                  failure:^(NSURLSessionDataTask *operation, NSError *error)
              {
                  NSLog(@"failed error is %@",error);
                  UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error!" message:[error localizedDescription] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                  [alertView show];
                  
              }];
         }
        else
        {
            
            //New changes
            
            if([type isEqualToString:@"getsize"])
            {
                url = [NSURL URLWithString:[NSString stringWithFormat:@"%@GetLookups?LookupName=size&format=json",MainUrl]];
                formdatareq = [ASIFormDataRequest requestWithURL:url];
                
            }
            else if([type isEqualToString:@"getcondition"])
            {
                url = [NSURL URLWithString:[NSString stringWithFormat:@"%@GetLookups?LookupName=condition&format=json",MainUrl]];
                formdatareq = [ASIFormDataRequest requestWithURL:url];
                
            }
            else if([type isEqualToString:@"getbrand"])
            {
                url = [NSURL URLWithString:[NSString stringWithFormat:@"%@GetLookups?LookupName=brand&format=json",MainUrl]];
                formdatareq = [ASIFormDataRequest requestWithURL:url];
                
            }
            else if([type isEqualToString:@"LikeItem"])
            {
                url = [NSURL URLWithString:[NSString stringWithFormat:@"%@UserWishlist?format=json",MainUrl]];
                
                NSLog(@"like item url is.....%@",url);
                formdatareq = [ASIFormDataRequest requestWithURL:url];
                
                
                [ formdatareq setPostValue:[allDetailsdict valueForKey:@"UserID"] forKey:@"UserID"];
                
                [ formdatareq setPostValue:[allDetailsdict objectForKey:@"UserListingID" ] forKey:@"UserListingID"];
                
                NSLog(@"the log is %@",[allDetailsdict valueForKey:@"UserID"]);
                
                
                
            }
            else if([type isEqualToString:@"mywishlist"])
            {
                // http://markidplace.testshell.net/api/UserWishlist?UserId=2&format=json
                
                url = [NSURL URLWithString:[NSString stringWithFormat:@"%@UserWishlist?UserId=%@&format=json",MainUrl,myappDelegate.useridstring]];
                
                NSLog(@"getall likes url is:======> %@",url);
                formdatareq = [ASIFormDataRequest requestWithURL:url];
                
            }
            
            else if([type isEqualToString:@"MyCloset"])
            {
                // http://markidplace.testshell.net/api/UserWishlist?UserId=2&format=json
                //My closet
                url = [NSURL URLWithString:[NSString stringWithFormat:@"%@UserListing?UserId=%@&format=json",MainUrl,myappDelegate.useridstring]];
                
                NSLog(@"getall likes url is:======> %@",url);
                formdatareq = [ASIFormDataRequest requestWithURL:url];
                
            }
            
            
            else if([type isEqualToString:@"getbuyer"])
            {
                
                
                url = [NSURL URLWithString:[NSString stringWithFormat:@"%@order?UserID=%@&format=json",MainUrl,myappDelegate.useridstring]];
                
                NSLog(@"getall likes url is:======> %@",url);
                formdatareq = [ASIFormDataRequest requestWithURL:url];
                
            }
            else if([type isEqualToString:@"getseller"])
            {
                
                
                url = [NSURL URLWithString:[NSString stringWithFormat:@"%@order?UserID=%@&Key=seller&format=json",MainUrl,myappDelegate.useridstring]];
                
                NSLog(@"getall likes url is:======> %@",url);
                formdatareq = [ASIFormDataRequest requestWithURL:url];
            }
            else if([type isEqualToString:@"Deletelisting23"])
            {
                // UserItems?format=json
                // DeleteItems?format=json
                
                
                url = [NSURL URLWithString:[NSString stringWithFormat:@"%@DeleteItems?format=json",MainUrl]];
                
                // NSLog(@"getall likes url is:======> %@",url);
                // formdatareq = [ASIFormDataRequest requestWithURL:url];
                // [formdatareq setPostValue:[allDetailsdict valueForKey:@"UserItemId"] forKey:@"UserItemId"];
                
                
                SBJsonWriter *jsonWriter = [[SBJsonWriter alloc] init];
                NSString *jsonRequest = [jsonWriter stringWithObject:allDetailsdict];
                jsonRequest = [jsonRequest stringByReplacingOccurrencesOfString:@"<" withString:@""];
                NSLog(@"check the json request %@",jsonRequest);
                
                // NSURL *url;
                // url = [NSURL URLWithString:[NSString stringWithFormat:@"%@UserItems?format=json",MainUrl]];
                mutableurlrequest = [NSMutableURLRequest requestWithURL:url
                                                            cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:60.0];
                NSData *requestData = [NSData dataWithBytes:[jsonRequest UTF8String] length:[jsonRequest length]];
                
                [mutableurlrequest setHTTPMethod:method];
                NSString *boundary = @"14737809831466499882746641449";
                // NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
                [mutableurlrequest setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
                //[request setValue:appDelegate.dsmEmail forHTTPHeaderField:@"UserName"];
                //[request setValue:appDelegate.dsmpassword forHTTPHeaderField:@"Password"];
                [mutableurlrequest setValue:[NSString stringWithFormat:@"%d", [requestData length]] forHTTPHeaderField:@"Content-Length"];
                [mutableurlrequest setHTTPBody: requestData];
                
                con = [NSURLConnection connectionWithRequest:mutableurlrequest delegate:self];
                return;
                
            }
            else if([type isEqualToString:@"Notforsale"])
            {
                
                
                url = [NSURL URLWithString:[NSString stringWithFormat:@"%@UserListing/UpdateForSale?format=json",MainUrl]];
                SBJsonWriter *jsonWriter = [[SBJsonWriter alloc] init];
                NSString *jsonRequest = [jsonWriter stringWithObject:allDetailsdict];
                jsonRequest = [jsonRequest stringByReplacingOccurrencesOfString:@"<" withString:@""];
                NSLog(@"check the json request %@",jsonRequest);
                
                
                // NSURL *url;
                // url = [NSURL URLWithString:[NSString stringWithFormat:@"%@UserItems?format=json",MainUrl]];
                mutableurlrequest = [NSMutableURLRequest requestWithURL:url
                                                            cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:60.0];
                NSData *requestData = [NSData dataWithBytes:[jsonRequest UTF8String] length:[jsonRequest length]];
                [mutableurlrequest setHTTPMethod:method];
                NSString *boundary = @"14737809831466499882746641449";
                // NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
                [mutableurlrequest setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
                //[request setValue:appDelegate.dsmEmail forHTTPHeaderField:@"UserName"];
                //[request setValue:appDelegate.dsmpassword forHTTPHeaderField:@"Password"];
                [mutableurlrequest setValue:[NSString stringWithFormat:@"%d", [requestData length]] forHTTPHeaderField:@"Content-Length"];
                [mutableurlrequest setHTTPBody: requestData];
                
                con = [NSURLConnection connectionWithRequest:mutableurlrequest delegate:self];
                return;
                
                
                
                
            }
            else if([type isEqualToString:@"getfollowers"])
            {
                
                if([myappDelegate isnullorempty:allDetailsdict])
                {
                    url = [NSURL URLWithString:[NSString stringWithFormat:@"%@Account?UserId=%@&LoginUserId=%@&format=json",MainUrl,myappDelegate.useridstring,myappDelegate.useridstring]];
                    NSLog(@"myappDelegate.useridstring--------> %@",myappDelegate.useridstring);
                    
                    
                }
                else
                {
                    url = [NSURL URLWithString:[NSString stringWithFormat:@"%@Account?UserId=%@&LoginUserId=%@&format=json",MainUrl,[allDetailsdict objectForKey:@"sellerid"],myappDelegate.useridstring]];
                    NSLog(@"[allDetailsdict objectForKey:sellerid]--------> %@",[allDetailsdict objectForKey:@"sellerid"]);
                    NSLog(@"myappDelegate.useridstring--------> %@",myappDelegate.useridstring);
                    
                }
                
                
                
                
                // http://markidplace.testshell.net/api/Account?UserId=19&LoginUserId=29&format=json
                formdatareq = [ASIFormDataRequest requestWithURL:url];
                
                
                
            }
            else if([type isEqualToString:@"Registration"])
            {
                
                NSString *urlstr=[NSString stringWithFormat:@"%@Register/SignUp?formate=json",MainUrl];
                url =[NSURL URLWithString:urlstr ];
                
                // http://markidplace.testshell.net/api/Account?UserId=19&LoginUserId=29&format=json
                formdatareq = [ASIFormDataRequest requestWithURL:url];
                
                [ formdatareq setPostValue:[allDetailsdict valueForKey:@"UserName"] forKey:@"UserName"];
                
                [ formdatareq setPostValue:[allDetailsdict valueForKey:@"Email"] forKey:@"Email"];
                [ formdatareq setPostValue:[allDetailsdict valueForKey:@"Password"] forKey:@"Password"];
                [ formdatareq setPostValue:[allDetailsdict valueForKey:@"DeviceType"] forKey:@"DeviceType"];
                [ formdatareq setPostValue:[allDetailsdict valueForKey:@"DeviceToken"] forKey:@"DeviceToken"];
                [ formdatareq setPostValue:[allDetailsdict valueForKey:@"DeviceOs"] forKey:@"DeviceOs"];
                [ formdatareq setPostValue:[allDetailsdict valueForKey:@"DeviceId"] forKey:@"DeviceId"];
                [ formdatareq setPostValue:[allDetailsdict valueForKey:@"DeviceKeychain"] forKey:@"DeviceKeychain"];
                
                
                [ formdatareq setPostValue:[allDetailsdict valueForKey:@"Device"] forKey:@"Device"];
                
                
                
                
                NSData *imgdata=UIImageJPEGRepresentation([allDetailsdict objectForKey:@"Profilepic"], 0.25f);
                
                [formdatareq addData:imgdata withFileName:[NSString stringWithFormat:@"%@.jpeg",[AppDelegate uniqueidstring]] andContentType:@"image/jpeg" forKey:@"Profilepic"];
                
                
                
            }
            
            else if ([type isEqualToString:@"ListingDetail"])
            {
                
                NSString *urlstr=[NSString stringWithFormat:@"%@UserListing?UserListingId=%@&format=json",MainUrl,[allDetailsdict valueForKey:@"UserListingId"]];
                url =[NSURL URLWithString:urlstr ];
                formdatareq = [ASIFormDataRequest requestWithURL:url];
            }
            else if ([type isEqualToString:@"getNotifications"])
            {
                NSString *urlstr=[NSString stringWithFormat:@"%@NotificationLog?UserId=%@&StartCount=%@&format=json",MainUrl,myappDelegate.useridstring,count];
                //NSLog(@"urlstr")
                url =[NSURL URLWithString:urlstr ];
                formdatareq = [ASIFormDataRequest requestWithURL:url];
                //method=@"GET";
                
               /* [manager GET:urlstr parameters:nil success:^(NSURLSessionDataTask *task, id responseObject)
                 {
                     NSMutableDictionary *dict=(NSMutableDictionary*)responseObject;
                     NSLog(@"$$$$$JSON$$$$$: %@", dict);

                     [delegate successresponceListing:dict :typestring];
                     
                     
                     
                 }
                     failure:^(NSURLSessionDataTask *operation, NSError *error)
                 {
                     //NSLog(@"Error: %@", error);
                     NSLog(@"failed error is %@",error);
                     // [self.delegate  successresponse:stringval];
                     UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error!" message:[error localizedDescription] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                     //alertView.tag=19;
                     [alertView show];
                     
                 }];*/
            
                
            }
            
        
            

            if ([myappDelegate isnullorempty:method])
            {
                method=@"GET";
            }
            
            NSLog(@"getdata url %@ and method %@",url,method);
            
            [formdatareq setRequestMethod:method];
            
            
            [formdatareq addRequestHeader:@"Content-Type" value:@"application/json"];
            
            formdatareq.delegate=self;
            [formdatareq setTimeOutSeconds:3000];
            
            // NSLog(@"json request %@==== ",formdatareq.JSONRepresentation);
            
            [formdatareq startAsynchronous];
        }
      
        
    }
}
/*
-(void)getdata :(NSDictionary *)allDetailsdict :(NSString *)type :(NSString *)method withcount:(NSString *)count
{
    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    NSLog(@"the status is %d",networkStatus);
    if (networkStatus == NotReachable)
    {
        [delegate internetConnection];
        
        ////NSLog(@"There IS NO internet connection");
    }
    else
    {

  //  http://markidplace.testshell.net/api/UserListing?format=json   [Get]
    
    NSURL *url;
    
 
    //UserId
   // http://markidplace.testshell.net/api/UserItems?format=json
    typestring=type;
    if([type isEqualToString:@"getListing"])
    {
       // UserItems?format=json
       // UserListing?format=json
        //UserListing?UserId=2?format=json
        //My suggestions
       // api/UserListing?UserId=2&key=mysuggestions&format=json // new
        url = [NSURL URLWithString:[NSString stringWithFormat:@"%@UserListing?UserId=%@&key=mysuggestions&format=json",MainUrl,myappDelegate.useridstring]];

        NSLog(@"userlisting url is: %@",url);
        formdatareq = [ASIFormDataRequest requestWithURL:url];

    }
    else if([type isEqualToString:@"getcatogery"])
    {
      //  url = [NSURL URLWithString:[NSString stringWithFormat:@"%@GetCategorys?format=json",MainUrl]];
        url = [NSURL URLWithString:[NSString stringWithFormat:@"%@Sell?format=json",MainUrl]];
        formdatareq = [ASIFormDataRequest requestWithURL:url];

    }
    else if([type isEqualToString:@"getsize"])
    {
        url = [NSURL URLWithString:[NSString stringWithFormat:@"%@GetLookups?LookupName=size&format=json",MainUrl]];
        formdatareq = [ASIFormDataRequest requestWithURL:url];
        
    }
    else if([type isEqualToString:@"getcondition"])
    {
        url = [NSURL URLWithString:[NSString stringWithFormat:@"%@GetLookups?LookupName=condition&format=json",MainUrl]];
        formdatareq = [ASIFormDataRequest requestWithURL:url];
        
    }
    else if([type isEqualToString:@"getbrand"])
    {
        url = [NSURL URLWithString:[NSString stringWithFormat:@"%@GetLookups?LookupName=brand&format=json",MainUrl]];
        formdatareq = [ASIFormDataRequest requestWithURL:url];
        
    }
    else if([type isEqualToString:@"LikeItem"])
    {
        url = [NSURL URLWithString:[NSString stringWithFormat:@"%@UserWishlist?format=json",MainUrl]];
        
        NSLog(@"like item url is.....%@",url);
        formdatareq = [ASIFormDataRequest requestWithURL:url];
        
        
        [ formdatareq setPostValue:[allDetailsdict valueForKey:@"UserID"] forKey:@"UserID"];
        
        [ formdatareq setPostValue:[allDetailsdict objectForKey:@"UserListingID" ] forKey:@"UserListingID"];
        
        NSLog(@"the log is %@",[allDetailsdict valueForKey:@"UserID"]);
        
        
        
    }
    else if([type isEqualToString:@"getallLikes"])
    {
      
        
        url = [NSURL URLWithString:[NSString stringWithFormat:@"%@UserWishlist?UserId=%@&format=json",MainUrl,myappDelegate.useridstring]];
        
        NSLog(@"getall likes url is:======> %@",url);
        formdatareq = [ASIFormDataRequest requestWithURL:url];
        
    }
    
    
    else if([type isEqualToString:@"mywishlist"])
    {
        // http://markidplace.testshell.net/api/UserWishlist?UserId=2&format=json

        url = [NSURL URLWithString:[NSString stringWithFormat:@"%@UserWishlist?UserId=%@&format=json",MainUrl,myappDelegate.useridstring]];

        NSLog(@"getall likes url is:======> %@",url);
        formdatareq = [ASIFormDataRequest requestWithURL:url];
        
    }
    
    else if([type isEqualToString:@"MyCloset"])
    {
        // http://markidplace.testshell.net/api/UserWishlist?UserId=2&format=json
       //My closet
        url = [NSURL URLWithString:[NSString stringWithFormat:@"%@UserListing?UserId=%@&format=json",MainUrl,myappDelegate.useridstring]];
        
        NSLog(@"getall likes url is:======> %@",url);
        formdatareq = [ASIFormDataRequest requestWithURL:url];
        
    }
    else if([type isEqualToString:@"Myfeeds"])
    {
        
        url = [NSURL URLWithString:[NSString stringWithFormat:@"%@MyFeed?UserId=%@&format=json",MainUrl,myappDelegate.useridstring]];
        
        NSLog(@"getall likes url is:======> %@",url);
        formdatareq = [ASIFormDataRequest requestWithURL:url];
        
    }
    
    else if([type isEqualToString:@"getbuyer"])
    {
        
        
        url = [NSURL URLWithString:[NSString stringWithFormat:@"%@order?UserID=%@&format=json",MainUrl,myappDelegate.useridstring]];
        
        NSLog(@"getall likes url is:======> %@",url);
        formdatareq = [ASIFormDataRequest requestWithURL:url];
        
    }
    else if([type isEqualToString:@"getseller"])
    {
        
        
        url = [NSURL URLWithString:[NSString stringWithFormat:@"%@order?UserID=%@&Key=seller&format=json",MainUrl,myappDelegate.useridstring]];
        
        NSLog(@"getall likes url is:======> %@",url);
        formdatareq = [ASIFormDataRequest requestWithURL:url];
    }
    else if([type isEqualToString:@"Deletelisting23"])
    {
       // UserItems?format=json
        // DeleteItems?format=json
        
        
        url = [NSURL URLWithString:[NSString stringWithFormat:@"%@DeleteItems?format=json",MainUrl]];
        
       // NSLog(@"getall likes url is:======> %@",url);
       // formdatareq = [ASIFormDataRequest requestWithURL:url];
       // [formdatareq setPostValue:[allDetailsdict valueForKey:@"UserItemId"] forKey:@"UserItemId"];
        
        
        SBJsonWriter *jsonWriter = [[SBJsonWriter alloc] init];
        NSString *jsonRequest = [jsonWriter stringWithObject:allDetailsdict];
        jsonRequest = [jsonRequest stringByReplacingOccurrencesOfString:@"<" withString:@""];
        NSLog(@"check the json request %@",jsonRequest);
        
       // NSURL *url;
       // url = [NSURL URLWithString:[NSString stringWithFormat:@"%@UserItems?format=json",MainUrl]];
        mutableurlrequest = [NSMutableURLRequest requestWithURL:url
                                                               cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:60.0];
        NSData *requestData = [NSData dataWithBytes:[jsonRequest UTF8String] length:[jsonRequest length]];
        
        [mutableurlrequest setHTTPMethod:method];
        NSString *boundary = @"14737809831466499882746641449";
       // NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
        [mutableurlrequest setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        //[request setValue:appDelegate.dsmEmail forHTTPHeaderField:@"UserName"];
        //[request setValue:appDelegate.dsmpassword forHTTPHeaderField:@"Password"];
        [mutableurlrequest setValue:[NSString stringWithFormat:@"%d", [requestData length]] forHTTPHeaderField:@"Content-Length"];
        [mutableurlrequest setHTTPBody: requestData];
        
        con = [NSURLConnection connectionWithRequest:mutableurlrequest delegate:self];
        return;
        
    }
    else if([type isEqualToString:@"Notforsale"])
    {
        
        
        url = [NSURL URLWithString:[NSString stringWithFormat:@"%@UserListing/UpdateForSale?format=json",MainUrl]];
        SBJsonWriter *jsonWriter = [[SBJsonWriter alloc] init];
        NSString *jsonRequest = [jsonWriter stringWithObject:allDetailsdict];
        jsonRequest = [jsonRequest stringByReplacingOccurrencesOfString:@"<" withString:@""];
        NSLog(@"check the json request %@",jsonRequest);
        
        
        // NSURL *url;
        // url = [NSURL URLWithString:[NSString stringWithFormat:@"%@UserItems?format=json",MainUrl]];
        mutableurlrequest = [NSMutableURLRequest requestWithURL:url
                                                               cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:60.0];
        NSData *requestData = [NSData dataWithBytes:[jsonRequest UTF8String] length:[jsonRequest length]];
        [mutableurlrequest setHTTPMethod:method];
        NSString *boundary = @"14737809831466499882746641449";
        // NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
        [mutableurlrequest setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        //[request setValue:appDelegate.dsmEmail forHTTPHeaderField:@"UserName"];
        //[request setValue:appDelegate.dsmpassword forHTTPHeaderField:@"Password"];
        [mutableurlrequest setValue:[NSString stringWithFormat:@"%d", [requestData length]] forHTTPHeaderField:@"Content-Length"];
        [mutableurlrequest setHTTPBody: requestData];
        
        con = [NSURLConnection connectionWithRequest:mutableurlrequest delegate:self];
        return;
        
       
       
        
    }
    else if([type isEqualToString:@"getfollowers"])
    {
        
        if([myappDelegate isnullorempty:allDetailsdict])
        {
            url = [NSURL URLWithString:[NSString stringWithFormat:@"%@Account?UserId=%@&LoginUserId=%@&format=json",MainUrl,myappDelegate.useridstring,myappDelegate.useridstring]];
        }
        else
        {
            url = [NSURL URLWithString:[NSString stringWithFormat:@"%@Account?UserId=%@&LoginUserId=%@&format=json",MainUrl,[allDetailsdict objectForKey:@"sellerid"],myappDelegate.useridstring]];
        }
        
        
        
        
        // http://markidplace.testshell.net/api/Account?UserId=19&LoginUserId=29&format=json
        formdatareq = [ASIFormDataRequest requestWithURL:url];
        
        
        
    }
    else if([type isEqualToString:@"Registration"])
    {
        
        NSString *urlstr=[NSString stringWithFormat:@"%@Register/SignUp?formate=json",MainUrl];
        url =[NSURL URLWithString:urlstr ];
        
        // http://markidplace.testshell.net/api/Account?UserId=19&LoginUserId=29&format=json
        formdatareq = [ASIFormDataRequest requestWithURL:url];
        
        [ formdatareq setPostValue:[allDetailsdict valueForKey:@"UserName"] forKey:@"UserName"];
        
         [ formdatareq setPostValue:[allDetailsdict valueForKey:@"Email"] forKey:@"Email"];
         [ formdatareq setPostValue:[allDetailsdict valueForKey:@"Password"] forKey:@"Password"];
         [ formdatareq setPostValue:[allDetailsdict valueForKey:@"DeviceType"] forKey:@"DeviceType"];
         [ formdatareq setPostValue:[allDetailsdict valueForKey:@"DeviceToken"] forKey:@"DeviceToken"];
         [ formdatareq setPostValue:[allDetailsdict valueForKey:@"DeviceOs"] forKey:@"DeviceOs"];
         [ formdatareq setPostValue:[allDetailsdict valueForKey:@"DeviceId"] forKey:@"DeviceId"];
         [ formdatareq setPostValue:[allDetailsdict valueForKey:@"DeviceKeychain"] forKey:@"DeviceKeychain"];
        
        
        [ formdatareq setPostValue:[allDetailsdict valueForKey:@"Device"] forKey:@"Device"];
       
        
        
        
        NSData *imgdata=UIImageJPEGRepresentation([allDetailsdict objectForKey:@"Profilepic"], 0.25f);
        
        [formdatareq addData:imgdata withFileName:[NSString stringWithFormat:@"%@.jpeg",[AppDelegate uniqueidstring]] andContentType:@"image/jpeg" forKey:@"Profilepic"];
        
        
        
    }
    
    else if ([type isEqualToString:@"ListingDetail"])
    {
        
        NSString *urlstr=[NSString stringWithFormat:@"%@UserListing?UserListingId=%@&format=json",MainUrl,[allDetailsdict valueForKey:@"UserListingId"]];
        url =[NSURL URLWithString:urlstr ];
        formdatareq = [ASIFormDataRequest requestWithURL:url];
    }
    else if ([type isEqualToString:@"getNotifications"])
    {
        NSString *urlstr=[NSString stringWithFormat:@"%@NotificationLog?UserId=%@&format=json",MainUrl,myappDelegate.useridstring];
        url =[NSURL URLWithString:urlstr ];
        formdatareq = [ASIFormDataRequest requestWithURL:url];
    }
    


    


    
    if ([myappDelegate isnullorempty:method])
    {
        method=@"GET";
    }
    
    NSLog(@"getdata url %@ and method %@",url,method);
    
    [formdatareq setRequestMethod:method];
    
    
    [formdatareq addRequestHeader:@"Content-Type" value:@"application/json"];
    
    formdatareq.delegate=self;
    [formdatareq setTimeOutSeconds:3000];
    
   // NSLog(@"json request %@==== ",formdatareq.JSONRepresentation);
    
    [formdatareq startAsynchronous];

  }
}
*/
-(void)requestFinished:(ASIHTTPRequest *)request
{
   // NSLog(@"request....%@",request);
    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    if (networkStatus == NotReachable)
    {
        [delegate internetConnection];
        ////NSLog(@"There IS NO internet connection");
    }
    else
    {

      NSError *error = [request error];
    
    if(!error)
    {
        NSString *response = [request responseString];
        
         NSLog(@"response for ---- %@",response);
        
        
        NSDictionary *dict = [response JSONValue];
        
        if ([typestring isEqualToString:@"SubmitListine"])
        {
            [delegate successresponceListing:dict :typestring];
        }
        else if([typestring isEqualToString:@"getListing"])
        {
            [delegate successresponceListing:dict :typestring];
        }
        else if([typestring isEqualToString:@"getcatogery"] || [typestring isEqualToString:@"getsize"] ||[typestring isEqualToString:@"getcondition"]||[typestring isEqualToString:@"getbrand"])
        {
            [delegate successresponceListing:dict :typestring];
        }
       
        else if([typestring isEqualToString:@"LikeItem"])
        {
               [myappDelegate likeresponce:dict];
            // [delegate successresponceListing:dict :typestring];
        }
        else if([typestring isEqualToString:@"getallLikes"])
        {
            [delegate successresponceListing:dict :typestring];
        }
        
        else if([typestring isEqualToString:@"mywishlist"])
        {
            NSLog(@"mywish listlike clicked    =-=-=-=");
            
            [delegate successresponceListing:dict :typestring];
        }
        else if([typestring isEqualToString:@"MyCloset"] || [typestring isEqualToString:@"Myfeeds"] || [typestring isEqualToString:@"getbuyer"]|| [typestring isEqualToString:@"getseller"] || [typestring isEqualToString:@"Deletelisting23"]|| [typestring isEqualToString:@"Notforsale"] || [typestring isEqualToString:@"ListingDetail"] || [typestring isEqualToString:@"getNotifications"] )
        {
            [delegate successresponceListing:dict :typestring];
        }
        else if([typestring isEqualToString:@"getfollowers"])
        {
            [delegate successresponceListing:dict :typestring];
        }
        else if ([typestring isEqualToString:@"Registration"])
        {
            [delegate successResponseforRegistration:dict];
        }
        

        
    }
    else
    {
        
        NSLog(@"error desc %@ ",error.description);
        
    }
}
}
/// pay pal

-(void)postPaymentDetails :(NSMutableDictionary *)allDetailsdict
{
    
    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    if (networkStatus == NotReachable)
    {
        [delegate internetConnection];
        ////NSLog(@"There IS NO internet connection");
    }
    else
    {
   
    NSError *jsonSerializationError = nil;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:allDetailsdict options:NSJSONWritingPrettyPrinted error:&jsonSerializationError];
    
    if(!jsonSerializationError)
    {
        NSString *serJSON = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        NSLog(@"Serialized JSON: %@", serJSON);
    }
    else
    {
        NSLog(@"JSON Encoding Failed: %@", [jsonSerializationError localizedDescription]);
    }
    

    
    //NSString *urlstr = [NSString stringWithFormat:@"http://MarkidPlace.testshell.net/api/Order?format=json"];
    NSString *urlstr = [NSString stringWithFormat:@"%@Order?format=json",MainUrl];
    
    NSURL *url = [NSURL URLWithString:urlstr];
    
    NSLog(@"postPaymentDetails url ----%@",url);
    
    mutableurlrequest = [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:6000.0];
    
    [mutableurlrequest setHTTPMethod:@"POST"];
    
    
    [mutableurlrequest setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [mutableurlrequest setHTTPBody:jsonData];
    
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    
    [NSURLConnection sendAsynchronousRequest:mutableurlrequest queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         if (error)
         {
             [delegate errorMessage:@"message"];
         }
         else
         {
             NSError *jsonParsingError = nil;
             
             NSDictionary *mainDict = [NSJSONSerialization JSONObjectWithData:data options:0 error:&jsonParsingError];
             
             if (jsonParsingError)
             {
                 NSLog(@"JSON ERROR: %@", [jsonParsingError localizedDescription]);
                 [delegate errorMessage:@"message"];
             }
             else
             {
                 [delegate successResponseforPaypal:mainDict];
                 /*
                  dispatch_sync(dispatch_get_main_queue(), ^{
                  [delegate successResponseforRegistration:mainDict];
                  });*/
             }
         }
     }];
}
}







// this is for sagety to post data to services
-(void)successresponceListing :(id)respdict :(NSString *)paramname
{
    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    if (networkStatus == NotReachable)
    {
        [delegate internetConnection];
        ////NSLog(@"There IS NO internet connection");
    }
    else
    {

    
    if ([paramname isEqualToString:@"SubmitListine"])
    {
    
    }
}
}




// this is for safety in case we have to swith to nsurl from asiform data request
//// nsurl connection

#pragma mark - Data transpering to W/S
- (void) connection:(NSURLConnection *)connection didReceiveData:(NSData *)data1
{
    
    if (!mdata)
    {
        mdata = [[NSMutableData alloc] initWithData:data1];
    }
    else
    {
        [mdata appendData:data1];
    }
    
}
- (void) connectionDidFinishLoading:(NSURLConnection *)connection
{
    
	NSString *dataString=[[NSString alloc]initWithData:mdata encoding:NSUTF8StringEncoding];
	
    
	mdata = nil;
	con = nil;
    
	NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    //[setResponseDict addEntriesFromDictionary:[dataString JSONValue]];
    dict = [dataString JSONValue];
    NSLog(@"setResponseDict--%@",dict);
    
    
   // NSString *msgstring=[dict objectForKey:@"message"];
    
    if ([typestring isEqualToString:@"SubmitListine"])
    {
        [delegate successresponceListing:dict :typestring];
    }
    else if([typestring isEqualToString:@"getListing"])
    {
        [delegate successresponceListing:dict :typestring];
    }
    else if([typestring isEqualToString:@"getcatogery"] || [typestring isEqualToString:@"getsize"] ||[typestring isEqualToString:@"getcondition"]||[typestring isEqualToString:@"getbrand"])
    {
        [delegate successresponceListing:dict :typestring];
        
    }
    else if([typestring isEqualToString:@"getdistance"] )
    {
        // [delegate successResponseforDistance:dict :typestring];
        
    }
        else if([typestring isEqualToString:@"getallLikes"])
    {
        [delegate successresponceListing:dict :typestring];
    }
    
    else if([typestring isEqualToString:@"mywishlist"])
    {
        [delegate successresponceListing:dict :typestring];
    }
    else if([typestring isEqualToString:@"MyCloset"] || [typestring isEqualToString:@"Myfeeds"] || [typestring isEqualToString:@"getbuyer"]|| [typestring isEqualToString:@"getseller"] || [typestring isEqualToString:@"Deletelisting23"]|| [typestring isEqualToString:@"Notforsale"] || [typestring isEqualToString:@"ListingDetail"] || [typestring isEqualToString:@"getNotifications"])
    {
        [delegate successresponceListing:dict :typestring];
    }
    else if([typestring isEqualToString:@"getfollowers"])
    {
        [delegate successresponceListing:dict :typestring];
    }
    
    // [self.delegate  successresponse:stringval];
    
   // [appdelegate stopspinner:self.view];
    
}


- (void) connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    
   	con = nil;
    NSString *stringval=@"error occured";
    // [self.delegate  successresponse:stringval];
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Connection Error!" message:@"Coudn't process request." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
    //alertView.tag=19;
    [alertView show];
    
   // [appdelegate stopspinner:self.view];
    
}



-(void)cancelrequest
{
    

    [formdatareq cancel];
    [con cancel];
    [NSURLConnection cancelPreviousPerformRequestsWithTarget:mutableurlrequest];
        mutableurlrequest=nil;
    
}

-(void)getdistance:(NSString*)typestr
{
    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    if (networkStatus == NotReachable)
    {
        [delegate internetConnection];
        ////NSLog(@"There IS NO internet connection");
    }
    else
    {
        
        
        //  myappDelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
        //http://markidplace.testshell.net/api/Account?UserId=19&LoginUserId=19&format=json
        // http://markidplace.testshell.net/api/GetMyAddress?UserId=1&format=json
        NSString *urlstr;
        //urlstr = [NSString stringWithFormat:@"%@Account?UserId=%@&LoginUserId=%@&format=json",MainUrl,userid,myappDelegate.useridstring];
        urlstr = [NSString stringWithFormat:@"%@GetLookups?LookupName=Distance&format=json",MainUrl];
        
        NSURL *url = [NSURL URLWithString:urlstr];
        
        NSLog(@"getdistance url:%@",url);
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:3600.0];
        
        [request setHTTPMethod:@"GET"];
        [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        
        urlconnection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
        NSOperationQueue *queue = [[NSOperationQueue alloc] init];
        
        [NSURLConnection sendAsynchronousRequest:request queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
         {
             if (error)
             {
                 [delegate errorMessage:@"message"];
             }
             else
             {
                 NSError *jsonParsingError = nil;
                 
                 NSMutableArray *mainArray = [NSJSONSerialization JSONObjectWithData:data options:0 error:&jsonParsingError];
                 
                 if (jsonParsingError)
                 {
                     //  NSLog(@"JSON ERROR: %@", [jsonParsingError localizedDescription]);
                     [delegate errorMessage:@"message"];
                 }
                 else
                 {
                     // NSLog(@"Users mainDict is %@",mainDict);
                     [delegate successResponseforDistance:mainArray];
                     
                 }
             }
         }];
        
    }
}

-(void)getnotificationcount:(NSString*)userid
{
    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    if (networkStatus == NotReachable)
    {
        [delegate internetConnection];
    }
    else
    {
        
               NSString *urlstr = [NSString stringWithFormat:@"%@Account/NotificationInfo?UserId=%@&format=json",MainUrl,userid];
        
        NSURL *url = [NSURL URLWithString:urlstr];
        
        NSLog(@"getnotificationcount url %@",url);
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:3600.0];
        
        [request setHTTPMethod:@"GET"];
        [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        
        urlconnection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
        NSOperationQueue *queue = [[NSOperationQueue alloc] init];
        
        [NSURLConnection sendAsynchronousRequest:request queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
         {
             if (error)
             {
                 [delegate errorMessage:@"message"];
             }
             else
             {
                 NSError *jsonParsingError = nil;
                 
                 NSMutableDictionary *maindict = [NSJSONSerialization JSONObjectWithData:data options:0 error:&jsonParsingError];
                 
                 if (jsonParsingError)
                 {
                     //  NSLog(@"JSON ERROR: %@", [jsonParsingError localizedDescription]);
                     [delegate errorMessage:@"message"];
                 }
                 else
                 {
                     // NSLog(@"Users mainDict is %@",mainDict);
                     [delegate successresponseofnotificationcount:maindict];
                     
                 }
             }
         }];
        
    }
}
//verification of the seller and buyer address
-(void)sellerandbuyerVerification:(NSMutableDictionary*)addressdict
{
    
    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    if (networkStatus == NotReachable)
    {
        [delegate internetConnection];
        ////NSLog(@"There IS NO internet connection");
    }
    else
    {
        
        NSError *jsonSerializationError = nil;
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:addressdict options:NSJSONWritingPrettyPrinted error:&jsonSerializationError];
        
        if(!jsonSerializationError)
        {
            NSString *serJSON = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
            NSLog(@"sellerandbuyerVerification Serialized JSON: %@", serJSON);
        }
        else
        {
            NSLog(@"JSON Encoding Failed: %@", [jsonSerializationError localizedDescription]);
        }
        
        //http://wonup.testshell.net/api/Register/Signup?format=json  [POST]
        
        
        NSString *urlstr = [NSString stringWithFormat:@"%@Account/ShippingAddressValidation?format=json",MainUrl];
        
        NSURL *url = [NSURL URLWithString:urlstr];
        
        NSLog(@"sellerandbuyerVerification url ----%@",url);
        
        mutableurlrequest = [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:6000.0];
        
        [mutableurlrequest setHTTPMethod:@"POST"];
        
        
        [mutableurlrequest setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        [mutableurlrequest setHTTPBody:jsonData];
        
        NSOperationQueue *queue = [[NSOperationQueue alloc] init];
        
        [NSURLConnection sendAsynchronousRequest:mutableurlrequest queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
         {
             if (error)
             {
                 [delegate errorMessage:@"message"];
             }
             else
             {
                 NSError *jsonParsingError = nil;
                 
                 NSMutableDictionary *mainDict = [NSJSONSerialization JSONObjectWithData:data options:0 error:&jsonParsingError];
                 
                 if (jsonParsingError)
                 {
                     NSLog(@"JSON ERROR: %@", [jsonParsingError localizedDescription]);
                     [delegate errorMessage:@"message"];
                 }
                 else
                 {
                     [delegate successResponseforDetailVerification:mainDict];
                     /*
                      dispatch_sync(dispatch_get_main_queue(), ^{
                      [delegate successResponseforRegistration:mainDict];
                      });*/
                     
                 }
                 
             }
             
         }];
        
    }
}

//Krish
-(void)verificationOfPayPalPayment:(NSString *)orderid
{
    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    if (networkStatus == NotReachable)
    {
        [delegate internetConnection];
        ////NSLog(@"There IS NO internet connection");
    }
    else
    {
        NSString *urlstr;
        urlstr = [NSString stringWithFormat:@"%@OrderStatus?OrderId=%@&format=json",MainUrl,orderid];
        
        NSURL *url = [NSURL URLWithString:urlstr];
        
        NSLog(@"getdistance url:%@",url);
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:3600.0];
        
        [request setHTTPMethod:@"GET"];
        [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        
        //urlconnection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
        NSOperationQueue *queue = [[NSOperationQueue alloc] init];
        [NSURLConnection sendAsynchronousRequest:request queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
         {
             if (error)
             {
                 [delegate errorMessage:@"message"];
             }
             else
             {
                 NSError *jsonParsingError = nil;
                 
                 NSMutableDictionary *mainDict = [NSJSONSerialization JSONObjectWithData:data options:0 error:&jsonParsingError];
                 
                 if (jsonParsingError)
                 {
                     //  NSLog(@"JSON ERROR: %@", [jsonParsingError localizedDescription]);
                     [delegate errorMessage:@"message"];
                 }
                 else
                 {
                     // NSLog(@"Users mainDict is %@",mainDict);
                     [delegate successResponseforPaypalVerification:mainDict];
                     //successResponseforPaypalVerification
                 }
             }
         }];
    }
}



@end







//{CategoryId=3;EndDate="2014-05-3009:35:52+0000";InPerson=0;ListPrice=25000;StartDate="2014-05-3009:35:52+0000";Status=Posted;USPS=0;}

