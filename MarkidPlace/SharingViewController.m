//
//  SharingViewController.m
//  MarkidPlace
//
//  Created by Varma Bhupatiraju on 20/09/14.
//  Copyright (c) 2014 stellentmac1. All rights reserved.
//

#import "SharingViewController.h"
#import "SuggestionsViewController.h"
#import "Sharingmodelobject.h"
@interface SharingViewController ()

@end

@implementation SharingViewController
@synthesize firstarray,secondarray,sharingtblview,thirdarray,titlelbl;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
  appdelegate=(AppDelegate*)[[UIApplication sharedApplication]delegate];
    self.titlelbl.font = [UIFont fontWithName:@"Monroe" size:18];
    self.navbar.frame = CGRectMake(0, 0, 320, 64);
    [self.navbar setBackgroundImage:[UIImage imageNamed:@"navbar.png"] forBarMetrics:UIBarMetricsDefault];
    firstarray=[[NSMutableArray alloc]initWithObjects:@"Facebook",@"Twitter", nil];
    secondarray=[[NSMutableArray alloc]initWithObjects:@"Share your activity on your wall.", @"Share your activity via tweets",nil];
    thirdarray=[[NSMutableArray alloc]init];
    
    for (int i=0;i<[firstarray count];i++)
    {
        Sharingmodelobject *obj;
        obj=[[Sharingmodelobject alloc]init];
        obj.namestr=[firstarray objectAtIndex:i];
        obj.titlestr=[secondarray objectAtIndex:i];
        NSLog(@"name passing....%@",obj.namestr);
        [thirdarray addObject:obj];
        // NSLog(@"secondarray is....%@",[secondarray description]);
        
    }
    [sharingtblview reloadData];
    // Do any additional setup after loading the view from its nib.
    fb=YES;
    twitter=YES;
}



-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return  70;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return  thirdarray.count;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
}
-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellidentifier=@"cell";
    SharingTableViewCell *cell=(SharingTableViewCell *)[sharingtblview dequeueReusableCellWithIdentifier:cellidentifier];
    if (cell==nil)
    {
        [[NSBundle mainBundle]loadNibNamed:@"SharingTableViewCell" owner:self options:nil];
        cell=self.tableviewcell;
    }
    // cell.namelbl.font = [UIFont fontWithName:@"Muli" size:13];
    Sharingmodelobject *bobj=[thirdarray objectAtIndex:indexPath.row];
    
    NSLog(@"bobj.namestr...%@",bobj.namestr);
    cell.firstlbl.text=bobj.namestr;
    cell.secondlbl.text=bobj.titlestr;
    cell.secondlbl.font=[UIFont fontWithName:@"muli" size:12];
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
     NSLog(@"indexPath.row....%d",indexPath.row);
    cell.switchbtn.tag=indexPath.row;
    if(indexPath.row==0)
    {
        NSString *string=[appdelegate getStringFromPlist:@"fb"];
        fb = [string boolValue];
        isswitch=fb;
        NSLog(@"the fb value is %@",string);
    }
    else if(indexPath.row==1)
    {
        NSString *string=[appdelegate getStringFromPlist:@"Twitter"];
        twitter = [string boolValue];
        isswitch=twitter;
        NSLog(@"the twitter value is %@",string);
    }
    if (isswitch==YES)
    {
        [cell.switchbtn setImage:[UIImage imageNamed:@"switch_on.png"] forState:UIControlStateNormal];
    }
    else
    {
        [cell.switchbtn setImage:[UIImage imageNamed:@"switch_off.png"] forState:UIControlStateNormal];
    }
        return cell;
}

-(IBAction)switchbutton:(id)sender
{
    UIButton*switchbtn=(UIButton*)sender;
    NSString *statuskey;
    if([sender tag]==0)
    {
        if(fb==YES)
        {
            fb=NO;
            isswitch=YES;
            statuskey=@"false";
        }
        else
        {
            fb=YES;
            isswitch=NO;
             statuskey=@"true";
        }
        NSLog(@"facebook........%@",statuskey);
        [appdelegate insertStringInPlist:@"fb" value:statuskey];
    }
    if([sender tag]==1)
    {
        if(twitter==YES)
        {
            twitter=NO;
            isswitch=YES;
            statuskey=@"false";
        }
        else
        {
            twitter=YES;
            isswitch=NO;
          statuskey=@"true";
        }
         NSLog(@"twitter........%@",statuskey);
        [appdelegate insertStringInPlist:@"Twitter" value:statuskey];
        if (isswitch==YES)
        {
            isswitch=NO;
            [switchbtn setImage:[UIImage imageNamed:@"switch_off.png"] forState:UIControlStateNormal];
            NSLog(@"the sender tag<<<<<====%ld =>>>> is off",(long)[sender tag]);
        }
        else
        {
            isswitch=YES;
            [switchbtn setImage:[UIImage imageNamed:@"switch_on.png"] forState:UIControlStateNormal];
            NSLog(@"the sender tag<<<<<====%ld =>>>> is on",(long)[sender tag]);
        }
        
}
    [sharingtblview reloadData];
}
-(IBAction)backbtnaction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
    
}
- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}
-(IBAction)homebtnaction:(id)sender
{
    
    NSArray *arr=self.navigationController.viewControllers;
    for (UIViewController *childvc in arr) {
        
        if([childvc isKindOfClass:[SuggestionsViewController class]])
        {
            UIViewController *subvc=(SuggestionsViewController *)childvc;
            [self.navigationController popToViewController:subvc animated:YES];
        }
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)internetConnection
{
    UIAlertView *Internetalert=[[UIAlertView alloc]initWithTitle:appname message:@"Internet connection not available" delegate:self cancelButtonTitle:nil otherButtonTitles:@"Ok", nil];
    [Internetalert show];
    [appdelegate stopspinner:self.view];
    
}
@end
