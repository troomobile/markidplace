//
//  BalanceViewController.m
//  MarkidPlace
//
//  Created by stellent_mac_05 on 17/09/14.
//  Copyright (c) 2014 stellentmac1. All rights reserved.
//

#import "BalanceViewController.h"

@interface BalanceViewController ()

@end

@implementation BalanceViewController
@synthesize balancecell;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    appdelegate= (AppDelegate *)[[UIApplication sharedApplication]delegate];
    balanceArray=[NSArray new];
        [self.navbar setBackgroundImage:[UIImage imageNamed:@"navbar.png"] forBarMetrics:UIBarMetricsDefault];
    self.titlelbl.font = [UIFont fontWithName:@"Monroe" size:18];
    _navbar.frame = CGRectMake(0, 0, 320, 64);

}
//this action perfrorms pop the suggestion view controller
-(IBAction)homebtnaction:(id)sender
{
    NSArray *arr=self.navigationController.viewControllers;
    for (UIViewController *childvc in arr) {
        
        if([childvc isKindOfClass:[SuggestionsViewController class]])
        {
            SuggestionsViewController *subvc=(SuggestionsViewController *)childvc;
            subvc.ismyfeed=NO;
            [self.navigationController popToViewController:subvc animated:YES];
        }
       
    }
}
//this action perfrorms navigation back action
-(IBAction)backbtnAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
//To make the statusbar in the white color
- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}
#pragma mark Tableview Delegate
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 3;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50;
}
-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier=@"acell";
    
    BalanceCell *tbcell =(BalanceCell*)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (tbcell==nil)
    {
        [[NSBundle mainBundle] loadNibNamed:@"BalanceCell" owner:self options:nil];
        tbcell=balancecell;
        tbcell.selectionStyle=UITableViewCellSelectionStyleNone;
    }
    tbcell.balancelbl.font = [UIFont fontWithName:@"Muli" size:13];
    tbcell.balancecountlbl.font = [UIFont fontWithName:@"Muli" size:13];
    
    if(indexPath.row==0)
    {
        tbcell.balancelbl.text=@"Credits";
        tbcell.balancecountlbl.text=[NSString stringWithFormat:@"$%@",tbcell.balancecountlbl.text];
    }
    else if(indexPath.row==1)
    {
        tbcell.balancelbl.text=@"Pending";
        tbcell.balancecountlbl.text=[NSString stringWithFormat:@"$%@",tbcell.balancecountlbl.text];
    }
    else if (indexPath.row==2)
    {
           tbcell.balancelbl.text=@"Redeemable";
           tbcell.balancecountlbl.text=[NSString stringWithFormat:@"$%@",tbcell.balancecountlbl.text];
    }
       return tbcell;
}
-(void)internetConnection
{
    UIAlertView *Internetalert=[[UIAlertView alloc]initWithTitle:appname message:@"Internet connection not available" delegate:self cancelButtonTitle:nil otherButtonTitles:@"Ok", nil];
    [Internetalert show];
    [appdelegate stopspinner:self.view];
    
}
@end
