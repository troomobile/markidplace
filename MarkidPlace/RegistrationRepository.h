//
//  RegistrationRepository.h
//  WonUp
//
//  Created by stellentmac1 on 5/15/14.
//  Copyright (c) 2014 stellentmac1. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "HelperProtocol.h"
#import "AppDelegate.h"
#import "Reachability.h"
#import "ASIFormDataRequest.h"
#import "JSON.h"


@class AppDelegate;
@interface RegistrationRepository : NSObject<HelperProtocol,ASIHTTPRequestDelegate>
{
   // AppDelegate *appdelegate;
    id <HelperProtocol> delegate;
    
    ASIFormDataRequest *formdatareq;
    
    NSString *typestring;
    
    NSMutableData *respData;
    NSURLConnection *urlconnection;
}
@property(nonatomic,strong)id <HelperProtocol> delegate;

-(void)postAllDetails :(NSMutableDictionary *)allDetailsdict :(NSString *)type :(NSString *)method;

-(void)newregistration :(NSDictionary *)alldetailsdict;
-(void)getdata :(NSDictionary *)allDetailsdict :(NSString *)type :(NSString *)method withcount:(NSString*)count;

/// pay pal
-(void)postPaymentDetails :(NSMutableDictionary *)allDetailsdict;
-(void)getdistance:(NSString*)typestr;


//////

/// connection

@property (nonatomic, strong) NSURLConnection *con;
@property (nonatomic, strong)NSMutableURLRequest *mutableurlrequest;
@property (nonatomic, strong) NSMutableData *mdata;
@property(nonatomic,retain)NSArray *successarray;

-(void)postwithNSurl :(NSDictionary *)allDetailsdict :(NSString *)type :(NSString *)method :(BOOL)postdict;
-(void)getweightnewlisting:(NSString*)weight;
-(void)cancelrequest;
-(void)getnotificationcount:(NSString*)userid;
-(void)sellerandbuyerVerification:(NSMutableDictionary*)addressdict;
-(void)verificationOfPayPalPayment:(NSString *)orderid;

@end
