
//
//  DeleteaccountViewController.m
//  MarkidPlace
//
//  Created by Varma Bhupatiraju on 10/09/14.
//  Copyright (c) 2014 stellentmac1. All rights reserved.
//

#import "DeleteaccountViewController.h"
#import "SuggestionsViewController.h"
#import "LoginViewController.h"
@interface DeleteaccountViewController ()

@end

@implementation DeleteaccountViewController
@synthesize navbar,deletetitlelbl,dropdownbtn,dropdowntblview,dropdownlbl,firstarray,deletebtn;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
  //  CGRect blockviewframe=dropdowntblview.frame;
 //  blockviewframe.origin.y+=400.0f;
  //  dropdowntblview.frame=blockviewframe;
    dropdowntblview.hidden=YES;
    profileRepos=[[ProfileRepository alloc]init];
    profileRepos.delegate=self;
     appdelegate =[[UIApplication sharedApplication]delegate];
    self.navbar.frame = CGRectMake(0, 0, 320, 64);
     self.deletetitlelbl.font = [UIFont fontWithName:@"Monroe" size:18];
    self.welbl.font=[UIFont fontWithName:@"muli" size:16];
    self.dropdownlbl.font=[UIFont fontWithName:@"muli" size:15];
    [self.navbar setBackgroundImage:[UIImage imageNamed:@"navbar.png"] forBarMetrics:UIBarMetricsDefault];
    firstarray=[[NSMutableArray alloc]initWithObjects:@" Not for me ",@"I don't have the time",@"I'm not making any sales",@"I have multiple accounts",@"I had a bad experience",@"Other",nil];
    deletebtn.layer.borderWidth = 1.0;
    deletebtn.layer.cornerRadius=5.0f;
    deletebtn.backgroundColor=[UIColor whiteColor];
    //loginlabel.layer.borderColor =[[UIColor greenColor] CGColor];
    deletebtn.layer.borderColor=[[UIColor colorWithRed:12.0/255.0f green:183.0/255.0f blue:174.0/255.0f alpha:1.0f]CGColor];
    deletebtn.titleLabel.font=[UIFont fontWithName:@"Muli" size:12];
    deletebtn.hidden=YES;
    dropdowntblview.layer.borderWidth = 1;
    dropdowntblview.layer.cornerRadius=1.0f;
    
    dropdowntblview.layer.borderColor =[[UIColor lightGrayColor ] CGColor];
    // Do any additional setup after loading the view from its nib.
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return  44;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return  firstarray.count;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    dropdownlbl.text=[firstarray objectAtIndex:indexPath.row];
    
    
    [UIView animateWithDuration:0.3
                     animations:^{
                         // animations go here
                         
                         CGRect blockviewframe=dropdowntblview.frame;
                         blockviewframe.size.height=0.0f;
                         dropdowntblview.frame=blockviewframe;
                     }];
    deletebtn.hidden=NO;

    
}
-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *cellIdentifier = @"cell";
    
    UITableViewCell *cell =[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if(cell==nil)
    {
        cell=[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        
    }
    cell.textLabel.text=[firstarray objectAtIndex:indexPath.row];
    cell.textLabel.textColor=[UIColor lightGrayColor];
    cell.textLabel.font=[UIFont fontWithName:@"muli" size:13];
    cell.textLabel.textAlignment=NSTextAlignmentCenter;
    return cell;
}

-(IBAction)backAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
    
}

-(IBAction)deletebtnAction:(id)sender
{
    
UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"Are you sure want to delete ?"message:nil delegate:self
cancelButtonTitle:nil otherButtonTitles:@"No",@"Yes",nil];
    [alert show];
   
// [myappDelegate startspinner:self.view];
    // NSString *userid=[myappDelegate getStringFromPlist:@"userid"];
   // [profileRepos deleteUserAccountWithUserid:userid andReasonToDelete:dropdownlbl.text];
    
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
        if (buttonIndex == 1)
        {
             [myappDelegate startspinner:self.view];
             NSString *userid=[myappDelegate getStringFromPlist:@"userid"];
            [profileRepos deleteUserAccountWithUserid:userid andReasonToDelete:dropdownlbl.text];
            
        }
    
    else if (buttonIndex == 0)
    {
        [alertView dismissWithClickedButtonIndex:1 animated:TRUE];
    }
    
}
-(void)errorMessage:(NSString *)message
{
    
    [AppDelegate alertmethod:appname :@"Something going wrong,Please contact admin"];
    
}
-(void)successResponseforProfile:(NSDictionary *)responseDict
{
    
      //[myappDelegate stopspinner:self.view];
    NSLog(@"responsedict.....%@",responseDict);
    
    if ([[responseDict valueForKey:@"message"] isEqualToString:@"Account Deletion Successfully"])
    {
       //[AppDelegate alertmethod:appname :@"Your account has been deleted successfully, Thanks For using Markid place"];
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.001 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            
            UIAlertView *myalert=[[UIAlertView alloc]initWithTitle:@"Goodbye" message:@"Your account has been deleted successfully. Thanks for using MarKIDplace" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [myalert show];
            
            [myappDelegate insertStringInPlist:usernamekey value:@""];
            [myappDelegate insertStringInPlist:@"userid" value:@""];
            [myappDelegate insertStringInPlist:@"realname" value:@""];
            
            [myappDelegate.suggestonsGlobalarray removeAllObjects];
            [myappDelegate.myfeedsGlobalarray removeAllObjects];
            [myappDelegate.myclosetGlobalarray removeAllObjects];
            [myappDelegate.mylikesGlobalarray removeAllObjects];
            [myappDelegate.cartGlobalarray removeAllObjects];
            [myappDelegate.allLikesGlobalarray removeAllObjects];
            
           
            
            [myappDelegate stopspinner:self.view];
                    NSArray *arr=self.navigationController.viewControllers;
                    BOOL isloginVcfind=NO;
                    UIViewController *tempVC=[[UIViewController alloc]init];
                    for (UIViewController *childvc in arr)
                    {
            
                        if([childvc isKindOfClass:[LoginViewController class]])
                        {
                           tempVC=(LoginViewController *)childvc;
                            isloginVcfind=YES;
                            break;
                        }
                    }
                    if (isloginVcfind)
                    {
                        [self.navigationController popToViewController:tempVC animated:YES];
            
                    }
                    else
                    {
                        LoginViewController *loginVC=[[LoginViewController alloc]initWithNibName:@"LoginViewController" bundle:nil];
            
                       UINavigationController *navcontroller=[[UINavigationController alloc]initWithRootViewController:loginVC];
            
                        appdelegate.window.rootViewController = navcontroller;
                        [self.navigationController popToRootViewControllerAnimated:YES];
                    }
            });
                }
                else
                {
                    [myappDelegate stopspinner:self.view];
                      [AppDelegate alertmethod:appname :@"Something going wrong,Please contact admin"];
            
            
               }

        
        

    
    
}
-(IBAction)homeBtnAction:(id)sender;
{
    
    NSArray *arr=self.navigationController.viewControllers;
    for (UIViewController *childvc in arr)
    {
        
        if([childvc isKindOfClass:[SuggestionsViewController class]])
        {
            SuggestionsViewController *subvc=(SuggestionsViewController *)childvc;
            subvc.ismyfeed=NO;
            [self.navigationController popToViewController:subvc animated:YES];
        }
    }
    
}
-(IBAction)dropdownAction:(id)sender
{
    dropdowntblview.hidden=NO;
      [UIView animateWithDuration:0.2
                     animations:^{
                         // animations go here
    
                         CGRect blockviewframe=dropdowntblview.frame;
                         blockviewframe.size.height=270.0f;//227
                         dropdowntblview.frame=blockviewframe;
                    }];
    
    
   // [dropdowntblview reloadData];
    
}
   
-(void)internetConnection
{
    UIAlertView *Internetalert=[[UIAlertView alloc]initWithTitle:appname message:@"Internet connection not available" delegate:self cancelButtonTitle:nil otherButtonTitles:@"Ok", nil];
    [Internetalert show];
    [appdelegate stopspinner:self.view];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
