//
//  NewListingCustomcell.h
//  MarkidPlace
//
//  Created by Varma Bhupatiraju on 07/07/14.
//  Copyright (c) 2014 stellentmac1. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NewListingCustomcell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *categorylbl;
@property (strong, nonatomic) IBOutlet UILabel *categorynamelbl;
@property (strong, nonatomic) IBOutlet UIButton *catdropdownbutton;



@property(nonatomic,strong)IBOutlet UIImageView *dropdownimgv;


@property(nonatomic,strong)IBOutlet UITextField *othertextfield;




@end
