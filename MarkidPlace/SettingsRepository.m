//
//  SettingsRepository.m
//  MarkidPlace
//
//  Created by Varma Bhupatiraju on 11/09/14.
//  Copyright (c) 2014 stellentmac1. All rights reserved.
//

#import "SettingsRepository.h"

@implementation SettingsRepository
@synthesize delegate;
-(void)deleteUserAccountWithUserid:(NSString*)userid andReasonToDelete:(NSString*)reason
{
    
}

-(void)getListUsers:(NSString *)userid
{
    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
     NSLog(@"There  connection+===%id",networkStatus);
    if (networkStatus == NotReachable)
    {
        [delegate internetConnection];
        //NSLog(@"There IS NO internet connection");
    }
    else
    {
    NSString *urlstr;
    urlstr = [NSString stringWithFormat:@"%@Account/ListUsers?UserId=%@&format=json",MainUrl,userid];
    
    NSURL *url = [NSURL URLWithString:urlstr];
    
    NSLog(@"getListUsers url *** %@",url);
    
    NSLog(@"@@@@@@@@ URL at users list is:%@",urlstr);
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:3600.0];
    
    [request setHTTPMethod:@"GET"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    //urlconnection = [[NSURLConnection alloc] initWithRequest:request delegate:nil];
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    
    [NSURLConnection sendAsynchronousRequest:request queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         if (error)
         {
             [delegate errorMessage:@"message"];
         }
         else
         {
             NSError *jsonParsingError = nil;
             
             NSMutableDictionary *mainDict=[NSJSONSerialization JSONObjectWithData:data options:0 error:&jsonParsingError];
             if (jsonParsingError)
             {
            
                 [delegate errorMessage:@"message"];
             }
             else
             {
                 // NSLog(@"Users mainDict is %@",mainDict);
                 [APPDELEGATE successResponseforlistusers:mainDict];
                 
             }
         }
     }];
    }
}

@end
