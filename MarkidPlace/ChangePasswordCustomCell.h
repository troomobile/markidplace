//
//  ChangePasswordCustomCell.h
//  MarkidPlace
//
//  Created by Varma Bhupatiraju on 11/09/14.
//  Copyright (c) 2014 stellentmac1. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ChangePasswordCustomCell : UITableViewCell
@property(nonatomic,strong)IBOutlet UILabel *namelbl;
@property (nonatomic,strong) IBOutlet UITextField *changetextfield;
@end
