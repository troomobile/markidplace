//
//  Users.m
//  MarkidPlace
//
//  Created by Varma Bhupatiraju on 5/31/14.
//  Copyright (c) 2014 stellentmac1. All rights reserved.
//

#import "Users.h"

@implementation Users
@synthesize userIdStr,isFollow;
@synthesize userNameStr;
@synthesize emailStr;
@synthesize profilepicStr;
@synthesize passwordStr;
@synthesize createdDateStr;
@synthesize loginUserIdStr;
@synthesize itemsCountStr;
@synthesize wishListCountStr;
@synthesize followerCountStr;
@synthesize followingCountStr;
@synthesize isFollowStr;
@synthesize userListingArray;
@synthesize usersWishArray;
@synthesize nameStr;
@synthesize paypalAddress;
@synthesize address1String;
@synthesize address2String;
@synthesize cityString;
@synthesize stateString;
@synthesize zipcodeString,notificationcountstr,messagecountstr;

@end
