//
//  MySizesAndBrandsViewController.m
//  MarkidPlace
//
//  Created by Varma Bhupatiraju on 14/08/14.
//  Copyright (c) 2014 stellentmac1. All rights reserved.
//

#import "MySizesAndBrandsViewController.h"
#import "SizeModelObject.h"
#import "LookupModelobject.h"
#import "Newmodelobject.h"
#import "MySizecustomCell.h"
@interface MySizesAndBrandsViewController ()

@end

@implementation MySizesAndBrandsViewController
{
    
}
@synthesize titlelabel,listingScrollView,preelabel,dropdownview,sizecell,mysizesview,updatebtn,threelabel;
@synthesize bornlabel,zerolabel,sixlabel,twlabel,eightlabel,twotlabel,threetlabel,fourlabel,fivelabel,sixtwllabel,sevenlabel,eiglabel,ninelabel,tenlabel,elevlabel,twlevelabel,thretlabel,fourtlabel,xslabel,slabel,mlabel,llabel,xllabel,maternityxslabel,maternityslabel,maternityllabel,maternitymlabel,maternityxllabel,brandlabel,cancelbutton;
@synthesize popview,firstaary,secondarray,updateimagview,brandtableview,sizearray,poptitlelable,updatelbl;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
   
    
   poptitlelable.font= [UIFont fontWithName:@"Muli" size:15];
    self.brandlabel.font=[UIFont fontWithName:@"Muli" size:14];
    // Do any additional setup after loading the view from its nib.
    self.navbar.frame = CGRectMake(0, 0, 320, 64);
    [self.navbar setBackgroundImage:[UIImage imageNamed:@"navbar.png"] forBarMetrics:UIBarMetricsDefault];
    
    preelabel.layer.borderWidth = 1.0;
    preelabel.layer.cornerRadius=5.0f;
    preelabel.backgroundColor=[UIColor whiteColor];
    //loginlabel.layer.borderColor =[[UIColor greenColor] CGColor];
    preelabel.layer.borderColor=[[UIColor colorWithRed:12.0/255.0f green:183.0/255.0f blue:174.0/255.0f alpha:1.0f]CGColor];
    preelabel.font=[UIFont fontWithName:@"Muli" size:12];
    
    bornlabel.layer.borderWidth = 1.0;
    bornlabel.layer.cornerRadius=5.0f;
    bornlabel.backgroundColor=[UIColor whiteColor];
    //loginlabel.layer.borderColor =[[UIColor greenColor] CGColor];
    bornlabel.layer.borderColor=[[UIColor colorWithRed:12.0/255.0f green:183.0/255.0f blue:174.0/255.0f alpha:1.0f]CGColor];
    bornlabel.font=[UIFont fontWithName:@"Muli" size:12];

    zerolabel.layer.borderWidth = 1.0;
    zerolabel.layer.cornerRadius=5.0f;
    zerolabel.backgroundColor=[UIColor whiteColor];
    
    updatelbl.font=[UIFont fontWithName:@"Muli" size:12];
    updatelbl.backgroundColor=[UIColor whiteColor];
    updatelbl.layer.borderWidth = 1.0;
    //updatelbl.layer.cornerRadius=1.0f;
    updatelbl.layer.borderColor=[[UIColor colorWithRed:12.0/255.0f green:183.0/255.0f blue:174.0/255.0f alpha:1.0f]CGColor];
    
    zerolabel.layer.borderColor=[[UIColor colorWithRed:12.0/255.0f green:183.0/255.0f blue:174.0/255.0f alpha:1.0f]CGColor];
    zerolabel.font=[UIFont fontWithName:@"Muli" size:12];
    
       threelabel.layer.borderWidth = 1.0;
    threelabel.layer.cornerRadius=5.0f;
    threelabel.backgroundColor=[UIColor whiteColor];
    //loginlabel.layer.borderColor =[[UIColor greenColor] CGColor];
    threelabel.layer.borderColor=[[UIColor colorWithRed:12.0/255.0f green:183.0/255.0f blue:174.0/255.0f alpha:1.0f]CGColor];
    threelabel.font=[UIFont fontWithName:@"Muli" size:12];
    
    sixlabel.layer.borderWidth = 1.0;
    sixlabel.layer.cornerRadius=5.0f;
    sixlabel.backgroundColor=[UIColor whiteColor];
    //loginlabel.layer.borderColor =[[UIColor greenColor] CGColor];
    sixlabel.layer.borderColor=[[UIColor colorWithRed:12.0/255.0f green:183.0/255.0f blue:174.0/255.0f alpha:1.0f]CGColor];
    sixlabel.font=[UIFont fontWithName:@"Muli" size:12];
    
    
    twlabel.layer.borderWidth = 1.0;
    twlabel.layer.cornerRadius=5.0f;
    twlabel.backgroundColor=[UIColor whiteColor];
    //loginlabel.layer.borderColor =[[UIColor greenColor] CGColor];
    twlabel.layer.borderColor=[[UIColor colorWithRed:12.0/255.0f green:183.0/255.0f blue:174.0/255.0f alpha:1.0f]CGColor];
    twlabel.font=[UIFont fontWithName:@"Muli" size:12];
    
    
    twlabel.layer.borderWidth = 1.0;
    twlabel.layer.cornerRadius=5.0f;
    twlabel.backgroundColor=[UIColor whiteColor];
    //loginlabel.layer.borderColor =[[UIColor greenColor] CGColor];
    twlabel.layer.borderColor=[[UIColor colorWithRed:12.0/255.0f green:183.0/255.0f blue:174.0/255.0f alpha:1.0f]CGColor];
    twlabel.font=[UIFont fontWithName:@"Muli" size:12];
    
    eightlabel.layer.borderWidth = 1.0;
    eightlabel.layer.cornerRadius=5.0f;
    eightlabel.backgroundColor=[UIColor whiteColor];
    //loginlabel.layer.borderColor =[[UIColor greenColor] CGColor];
    eightlabel.layer.borderColor=[[UIColor colorWithRed:12.0/255.0f green:183.0/255.0f blue:174.0/255.0f alpha:1.0f]CGColor];
    eightlabel.font=[UIFont fontWithName:@"Muli" size:12];
    
    
    twotlabel.layer.borderWidth = 1.0;
    twotlabel.layer.cornerRadius=5.0f;
    twotlabel.backgroundColor=[UIColor whiteColor];
    //loginlabel.layer.borderColor =[[UIColor greenColor] CGColor];
    twotlabel.layer.borderColor=[[UIColor colorWithRed:12.0/255.0f green:183.0/255.0f blue:174.0/255.0f alpha:1.0f]CGColor];
    twotlabel.font=[UIFont fontWithName:@"Muli" size:12];
    
    threetlabel.layer.borderWidth = 1.0;
    threetlabel.layer.cornerRadius=5.0f;
    threetlabel.backgroundColor=[UIColor whiteColor];
    //loginlabel.layer.borderColor =[[UIColor greenColor] CGColor];
    threetlabel.layer.borderColor=[[UIColor colorWithRed:12.0/255.0f green:183.0/255.0f blue:174.0/255.0f alpha:1.0f]CGColor];
    threetlabel.font=[UIFont fontWithName:@"Muli" size:12];
    
    
    fourlabel.layer.borderWidth = 1.0;
    fourlabel.layer.cornerRadius=5.0f;
    fourlabel.backgroundColor=[UIColor whiteColor];
    //loginlabel.layer.borderColor =[[UIColor greenColor] CGColor];
    fourlabel.layer.borderColor=[[UIColor colorWithRed:12.0/255.0f green:183.0/255.0f blue:174.0/255.0f alpha:1.0f]CGColor];
    fourlabel.font=[UIFont fontWithName:@"Muli" size:12];
    
    
    
    fivelabel.layer.borderWidth = 1.0;
    fivelabel.layer.cornerRadius=5.0f;
    fivelabel.backgroundColor=[UIColor whiteColor];
    //loginlabel.layer.borderColor =[[UIColor greenColor] CGColor];
    fivelabel.layer.borderColor=[[UIColor colorWithRed:12.0/255.0f green:183.0/255.0f blue:174.0/255.0f alpha:1.0f]CGColor];
    fivelabel.font=[UIFont fontWithName:@"Muli" size:12];
    
    
    sixtwllabel.layer.borderWidth = 1.0;
    sixtwllabel.layer.cornerRadius=5.0f;
    sixtwllabel.backgroundColor=[UIColor whiteColor];
    //loginlabel.layer.borderColor =[[UIColor greenColor] CGColor];
    sixtwllabel.layer.borderColor=[[UIColor colorWithRed:12.0/255.0f green:183.0/255.0f blue:174.0/255.0f alpha:1.0f]CGColor];
    sixtwllabel.font=[UIFont fontWithName:@"Muli" size:12];
    
    
    sevenlabel.layer.borderWidth = 1.0;
    sevenlabel.layer.cornerRadius=5.0f;
    sevenlabel.backgroundColor=[UIColor whiteColor];
    //loginlabel.layer.borderColor =[[UIColor greenColor] CGColor];
    sevenlabel.layer.borderColor=[[UIColor colorWithRed:12.0/255.0f green:183.0/255.0f blue:174.0/255.0f alpha:1.0f]CGColor];
    sevenlabel.font=[UIFont fontWithName:@"Muli" size:12];
    
    
    eiglabel.layer.borderWidth = 1.0;
    eiglabel.layer.cornerRadius=5.0f;
    eiglabel.backgroundColor=[UIColor whiteColor];
    //loginlabel.layer.borderColor =[[UIColor greenColor] CGColor];
    eiglabel.layer.borderColor=[[UIColor colorWithRed:12.0/255.0f green:183.0/255.0f blue:174.0/255.0f alpha:1.0f]CGColor];
    eiglabel.font=[UIFont fontWithName:@"Muli" size:12];
    
    
    ninelabel.layer.borderWidth = 1.0;
    ninelabel.layer.cornerRadius=5.0f;
    ninelabel.backgroundColor=[UIColor whiteColor];
    //loginlabel.layer.borderColor =[[UIColor greenColor] CGColor];
    ninelabel.layer.borderColor=[[UIColor colorWithRed:12.0/255.0f green:183.0/255.0f blue:174.0/255.0f alpha:1.0f]CGColor];
    ninelabel.font=[UIFont fontWithName:@"Muli" size:12];
    
    tenlabel.layer.borderWidth = 1.0;
    tenlabel.layer.cornerRadius=5.0f;
    tenlabel.backgroundColor=[UIColor whiteColor];
    //loginlabel.layer.borderColor =[[UIColor greenColor] CGColor];
    tenlabel.layer.borderColor=[[UIColor colorWithRed:12.0/255.0f green:183.0/255.0f blue:174.0/255.0f alpha:1.0f]CGColor];
    tenlabel.font=[UIFont fontWithName:@"Muli" size:12];
    
    
    elevlabel.layer.borderWidth = 1.0;
    elevlabel.layer.cornerRadius=5.0f;
    elevlabel.backgroundColor=[UIColor whiteColor];
    //loginlabel.layer.borderColor =[[UIColor greenColor] CGColor];
    elevlabel.layer.borderColor=[[UIColor colorWithRed:12.0/255.0f green:183.0/255.0f blue:174.0/255.0f alpha:1.0f]CGColor];
    elevlabel.font=[UIFont fontWithName:@"Muli" size:12];
    
    
    twlevelabel.layer.borderWidth = 1.0;
    twlevelabel.layer.cornerRadius=5.0f;
    twlevelabel.backgroundColor=[UIColor whiteColor];
    //loginlabel.layer.borderColor =[[UIColor greenColor] CGColor];
    twlevelabel.layer.borderColor=[[UIColor colorWithRed:12.0/255.0f green:183.0/255.0f blue:174.0/255.0f alpha:1.0f]CGColor];
    twlevelabel.font=[UIFont fontWithName:@"Muli" size:12];
    
    
    
    
    thretlabel.layer.borderWidth = 1.0;
    thretlabel.layer.cornerRadius=5.0f;
    thretlabel.backgroundColor=[UIColor whiteColor];
    //loginlabel.layer.borderColor =[[UIColor greenColor] CGColor];
    thretlabel.layer.borderColor=[[UIColor colorWithRed:12.0/255.0f green:183.0/255.0f blue:174.0/255.0f alpha:1.0f]CGColor];
    thretlabel.font=[UIFont fontWithName:@"Muli" size:12];
    

    fourtlabel.layer.borderWidth = 1.0;
    fourtlabel.layer.cornerRadius=5.0f;
    fourtlabel.backgroundColor=[UIColor whiteColor];
    //loginlabel.layer.borderColor =[[UIColor greenColor] CGColor];
    fourtlabel.layer.borderColor=[[UIColor colorWithRed:12.0/255.0f green:183.0/255.0f blue:174.0/255.0f alpha:1.0f]CGColor];
    fourtlabel.font=[UIFont fontWithName:@"Muli" size:12];

    
    
    xslabel.layer.borderWidth = 1.0;
    xslabel.layer.cornerRadius=5.0f;
    xslabel.backgroundColor=[UIColor whiteColor];
    //loginlabel.layer.borderColor =[[UIColor greenColor] CGColor];
    xslabel.layer.borderColor=[[UIColor colorWithRed:12.0/255.0f green:183.0/255.0f blue:174.0/255.0f alpha:1.0f]CGColor];
    xslabel.font=[UIFont fontWithName:@"Muli" size:12];

    slabel.layer.borderWidth = 1.0;
    slabel.layer.cornerRadius=5.0f;
    slabel.backgroundColor=[UIColor whiteColor];
    //loginlabel.layer.borderColor =[[UIColor greenColor] CGColor];
    slabel.layer.borderColor=[[UIColor colorWithRed:12.0/255.0f green:183.0/255.0f blue:174.0/255.0f alpha:1.0f]CGColor];
    slabel.font=[UIFont fontWithName:@"Muli" size:12];
    
    mlabel.layer.borderWidth = 1.0;
    mlabel.layer.cornerRadius=5.0f;
    mlabel.backgroundColor=[UIColor whiteColor];
    //loginlabel.layer.borderColor =[[UIColor greenColor] CGColor];
    mlabel.layer.borderColor=[[UIColor colorWithRed:12.0/255.0f green:183.0/255.0f blue:174.0/255.0f alpha:1.0f]CGColor];
    mlabel.font=[UIFont fontWithName:@"Muli" size:12];
    
    llabel.layer.borderWidth = 1.0;
    llabel.layer.cornerRadius=5.0f;
    llabel.backgroundColor=[UIColor whiteColor];
    //loginlabel.layer.borderColor =[[UIColor greenColor] CGColor];
    llabel.layer.borderColor=[[UIColor colorWithRed:12.0/255.0f green:183.0/255.0f blue:174.0/255.0f alpha:1.0f]CGColor];
    llabel.font=[UIFont fontWithName:@"Muli" size:12];
    
    xllabel.layer.borderWidth = 1.0;
    xllabel.layer.cornerRadius=5.0f;
    xllabel.backgroundColor=[UIColor whiteColor];
    //loginlabel.layer.borderColor =[[UIColor greenColor] CGColor];
    xllabel.layer.borderColor=[[UIColor colorWithRed:12.0/255.0f green:183.0/255.0f blue:174.0/255.0f alpha:1.0f]CGColor];
    xllabel.font=[UIFont fontWithName:@"Muli" size:12];
    
    maternityxslabel.layer.borderWidth = 1.0;
    maternityxslabel.layer.cornerRadius=5.0f;
    maternityxslabel.backgroundColor=[UIColor whiteColor];
    //loginlabel.layer.borderColor =[[UIColor greenColor] CGColor];
    maternityxslabel.layer.borderColor=[[UIColor colorWithRed:12.0/255.0f green:183.0/255.0f blue:174.0/255.0f alpha:1.0f]CGColor];
    maternityxslabel.font=[UIFont fontWithName:@"Muli" size:10];
    
    maternityslabel.layer.borderWidth = 1.0;
    maternityslabel.layer.cornerRadius=5.0f;
    maternityslabel.backgroundColor=[UIColor whiteColor];
    //loginlabel.layer.borderColor =[[UIColor greenColor] CGColor];
    maternityslabel.layer.borderColor=[[UIColor colorWithRed:12.0/255.0f green:183.0/255.0f blue:174.0/255.0f alpha:1.0f]CGColor];
    maternityslabel.font=[UIFont fontWithName:@"Muli" size:10];
    
    maternitymlabel.layer.borderWidth = 1.0;
    maternitymlabel.layer.cornerRadius=5.0f;
    maternitymlabel.backgroundColor=[UIColor whiteColor];
    //loginlabel.layer.borderColor =[[UIColor greenColor] CGColor];
    maternitymlabel.layer.borderColor=[[UIColor colorWithRed:12.0/255.0f green:183.0/255.0f blue:174.0/255.0f alpha:1.0f]CGColor];
    maternitymlabel.font=[UIFont fontWithName:@"Muli" size:10];
    
    
    maternityllabel.layer.borderWidth = 1.0;
    maternityllabel.layer.cornerRadius=5.0f;
    maternityllabel.backgroundColor=[UIColor whiteColor];
    //loginlabel.layer.borderColor =[[UIColor greenColor] CGColor];
    maternityllabel.layer.borderColor=[[UIColor colorWithRed:12.0/255.0f green:183.0/255.0f blue:174.0/255.0f alpha:1.0f]CGColor];
    maternityllabel.font=[UIFont fontWithName:@"Muli" size:10];
    
    maternityxllabel.layer.borderWidth = 1.0;
    maternityxllabel.layer.cornerRadius=5.0f;
    maternityxllabel.backgroundColor=[UIColor whiteColor];
    //loginlabel.layer.borderColor =[[UIColor greenColor] CGColor];
    maternityxllabel.layer.borderColor=[[UIColor colorWithRed:12.0/255.0f green:183.0/255.0f blue:174.0/255.0f alpha:1.0f]CGColor];
    maternityxllabel.font=[UIFont fontWithName:@"Muli" size:10];
    
    
    
    dropdownview.layer.borderWidth = 1;
    dropdownview.layer.cornerRadius=1.0f;
    
    dropdownview.layer.borderColor =[[UIColor whiteColor ] CGColor];
    
       [listingScrollView addSubview:mysizesview];
    [listingScrollView setContentSize:CGSizeMake(320,850)];
    
    
//    firstaary =[[NSMutableArray alloc]initWithObjects:@"Brand",@"Brand",@"Brand",@"Brand",@"Brand",@"Brand",@"Brand",@"Brand",@"Brand",@"Brand",@"Brand",@"Brand",@"Brand",@"Brand",@"Brand",@"Brand",@"Brand",@"Brand",@"Brand", nil];
//    secondarray =[[NSMutableArray alloc]init];
//    
//    for (int i=0;i<[firstaary count];i++)
//    {
//        SizeModelObject *obj;
//        obj=[[SizeModelObject alloc]init];
//        obj.namestr=[firstaary objectAtIndex:i];
//        NSLog(@"name passing....%@",obj.namestr);
//        [secondarray addObject:obj];
//        // NSLog(@"secondarray is....%@",[secondarray description]);
//    }
//    NSLog(@"secondarray is....%@",[secondarray description]);
//    
    [self sizemethod];
    
}
-(void)sizemethod
{
    sizearray=[NSMutableArray new];
    for (int i=0;i<5 ; i++)
    {
        Newmodelobject *obj=[[Newmodelobject alloc]init];
        if (i==0)
        {
            obj.isother=NO;
            obj.cellnamestr=@"Category";
            obj.cellvaluestr=@"";
            obj.otherstr=@"";
        }
        else if (i==1)
        {
            obj.isother=NO;
            obj.cellnamestr=@"Size";
            obj.cellvaluestr=@"";
            obj.otherstr=@"";
        }
        else if (i==2)
        {
            obj.isother=NO;
            obj.cellnamestr=@"Condition";
            obj.cellvaluestr=@"";
            obj.otherstr=@"";
        }
        else if (i==3)
        {
            obj.isother=NO;
            obj.cellnamestr=@"Brand";
            obj.cellvaluestr=@"";
            obj.otherstr=@"";
        }
        
        else if (i==4)
        {
            obj.isother=NO;
            obj.cellnamestr=@"Gender";
            obj.cellvaluestr=@"";
            obj.otherstr=@"";
        }
        else if (i==5)
        {
            obj.isother=NO;
            obj.cellnamestr=@"Gender";
            obj.cellvaluestr=@"";
            obj.otherstr=@"";
        }

        [sizearray addObject:obj];
        
    }
}


-(void)popViewMethod :(BOOL)isshow
{
    CGRect popfrm=popview.frame;
    popfrm.origin.x=0;
    if (isshow)
    {
        // [self.view addSubview:popview];
        popfrm.origin.y=0;
        
        
        CATransition *transition = [CATransition animation];
        transition.duration = 0.3;
        transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
        transition.type = kCATransitionPush;
        transition.subtype =kCATransitionFromTop;
        transition.delegate = self;
        [popview.layer addAnimation:transition forKey:nil];
    }
    else
    {
        popfrm.origin.y=605;
        CATransition *transition = [CATransition animation];
        transition.duration = 0.3;
        transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
        transition.type = kCATransitionPush;
        transition.subtype =kCATransitionFromBottom;
        transition.delegate = self;
        [popview.layer addAnimation:transition forKey:nil];
    }
    
    //[UIView beginAnimations: @"anim" context: nil];
    //[UIView setAnimationBeginsFromCurrentState: YES];
    // [UIView setAnimationDuration:0.3];
    popview.frame=popfrm;
    // [UIView commitAnimations];
    [self.view bringSubviewToFront:popview];
    [self.view addSubview:popview];
    
}
-(IBAction)backAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
-(IBAction)cancelBtnAction:(id)sender
{
    [self popViewMethod:NO];
}
//this action performs pops suggession view controller
-(IBAction)homeBtnAction:(id)sender
{
    NSArray *arr=self.navigationController.viewControllers;
    for (UIViewController *childvc in arr)
    {
        
        if([childvc isKindOfClass:[SuggestionsViewController class]])
        {
            SuggestionsViewController *subvc=(SuggestionsViewController *)childvc;
            subvc.ismyfeed=NO;
            [self.navigationController popToViewController:subvc animated:YES];
        }
    }
    
}

-(IBAction)brandaction:(id)sender
{
    
    
    if([sender tag]==111)
    {
        poptitlelable.text=@"Category";
    }
    else if ([sender tag]==222)
    {
        poptitlelable.text=@"Size";
    }
    
    else if ([sender tag]==444)
    {
        poptitlelable.text=@"Brand";
    }
    else  if ([sender tag]==333)
    {
        poptitlelable.text=@"Condition";
    }
    else if ([sender tag]==777)
    {
        poptitlelable.text=@"Gender";
    }
    
    [self popViewMethod:YES];
   
    dropdownview.tag=[sender tag];
    
    [dropdownview reloadData];
    
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (tableView.tag==111)
    {
        return 30;
    }
    else
    {
        
        
        return 0;
    }

}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    
       if (tableView.tag==111)
    {
        UIView *sectionview=[[UIView alloc] initWithFrame:CGRectMake(0,0,320,30)];
        UILabel *lbl=[[UILabel alloc] initWithFrame:CGRectMake(5,0,320,25)];
        
        [sectionview addSubview:lbl];
        lbl.font = [UIFont fontWithName:@"Muli" size:17];
        
        LookupModelobject *lookobj=[myappDelegate.catogoryGlobalarray objectAtIndex:section];
        lbl.text=lookobj.loolupValue;
        lbl.textColor=[UIColor colorWithRed:(12/255.f) green:(180/255.f) blue:(174/255.f) alpha:1.0];
        sectionview.backgroundColor=[UIColor whiteColor];
        return sectionview;
    }
    return nil;

}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
      NSLog(@"dropdownview.tag...%d",dropdownview.tag);
     NSLog(@"indexpath.section....%d",indexPath.section);
    NSLog(@"indexpath.row....%d",indexPath.row);
     UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
      //dropdownview.separatorStyle=NO;
    cell.selectionStyle=NO;
    if(tableView.tag==222)
    {
        
        LookupModelobject *lookobj=[myappDelegate.sizeGlobalarray objectAtIndex: indexPath.row];
        
        if (lookobj.isCheckMark)
        {
            lookobj.isCheckMark=NO;
        }
        else
        {
            lookobj.isCheckMark=YES;
            
        }
         [dropdownview reloadData];
        
    }
        
    if (tableView.tag==111)
    {
        LookupModelobject *lookobj=[myappDelegate.catogoryGlobalarray objectAtIndex:indexPath.section];
        
        LookupModelobject *obj=[lookobj.categoryDataArray objectAtIndex:indexPath.row];
        
        if (obj.isCheckMark)
        {
            obj.isCheckMark=NO;
        }
        else
        {
            obj.isCheckMark=YES;

        }
            [dropdownview reloadData];
    }
    
    if (tableView.tag==333)
    {
        LookupModelobject *lookobj=[myappDelegate.conditionGlobalarray objectAtIndex: indexPath.row];
        if (lookobj.isCheckMark)
        {
            lookobj.isCheckMark=NO;
        }
        else
        {
            lookobj.isCheckMark=YES;
            
        }
        [dropdownview reloadData];
    }
    if (tableView.tag==777)
    {
        LookupModelobject *lookobj=[myappDelegate.genderglobalaray objectAtIndex: indexPath.row];
        if (lookobj.isCheckMark)
        {
            lookobj.isCheckMark=NO;
        }
        else
        {
            lookobj.isCheckMark=YES;
            
        }
        [dropdownview reloadData];
        
    }
    
    
    if (tableView.tag==444)
    {
       LookupModelobject *lookobj=[myappDelegate.brandGlobalarray objectAtIndex: indexPath.row];
      
        
        if (lookobj.isCheckMark)
        {
            lookobj.isCheckMark=NO;
        }
        else
        {
            lookobj.isCheckMark=YES;
            
        }
        [dropdownview reloadData];
    }

    else
    {
        NSLog(@"cell.accessoryType...%d",cell.accessoryType);
         NSLog(@"UITableViewCellAccessoryCheckmark...%d",UITableViewCellAccessoryCheckmark);
        if (cell.accessoryType == UITableViewCellAccessoryCheckmark)
        {
            cell.accessoryType = UITableViewCellAccessoryNone;
            cell.tintColor = [UIColor whiteColor];
        }
        else
        {
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
           cell.tintColor = [UIColor whiteColor];
        }
    }
    
    
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 40.0;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
      NSLog(@"myappDelegate.brandGlobalarray.counts...%d",myappDelegate.brandGlobalarray.count);
    if (tableView == dropdownview)
    {
        if (tableView.tag==111)
        {
            //lookobj.categoryDataArray
            LookupModelobject *lookobj=[myappDelegate.catogoryGlobalarray objectAtIndex:section];
            return lookobj.categoryDataArray.count;
        }
        else if (tableView.tag==222)
        {
            return myappDelegate.sizeGlobalarray.count;
        }
        else if (tableView.tag==333)
        {
            return myappDelegate.conditionGlobalarray.count;
        }
        else if (tableView.tag==444)
        {
          
            return myappDelegate.brandGlobalarray.count;
        }
        else if (tableView.tag==777)
        {
            return myappDelegate.genderglobalaray.count;
        }

        else
        {
            return 1;
        }
    }
    else
    {
        return sizearray.count;
    }
    
    
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if (tableView.tag==111)
    {
        return myappDelegate.catogoryGlobalarray.count;
    }
    else
        return 1;
}
-(void)viewWillAppear:(BOOL)animated
{
    [listingScrollView scrollsToTop];
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView==brandtableview)
    {
        
    static NSString *cellidentifier=@"cell";
    MySizecustomCell *cell=(MySizecustomCell *)[tableView dequeueReusableCellWithIdentifier:cellidentifier];
    if (cell==nil)
    {
        [[NSBundle mainBundle]loadNibNamed:@"MySizecustomCell" owner:self options:nil];
        cell=self.sizecell;
        
    }
        Newmodelobject *cellobj=[sizearray objectAtIndex:indexPath.row];
       
        cell.brandnamelabel.font=[UIFont fontWithName:@"Muli" size:15];
        NSLog(@"cellobj.cellnamestr.....%@",cellobj.cellnamestr);
        //cell.brandnamelabel.text=cellobj.cellnamestr;
        cell.brandlabel.text=cellobj.cellnamestr;
         NSLog(@" cell.brandnamelabel.text....%@", cell.brandnamelabel.text);
        //cell.brandnamelabel.text=cellobj.cellvaluestr;//
    cell.backgroundColor=[UIColor clearColor];
    cell.contentView.backgroundColor=[UIColor clearColor];
        cell.brandlabel.font=[UIFont fontWithName:@"Muli" size:15];
        if (indexPath.row==0)
        {
            cell.brandactiontag.tag=111;
            
        }
        else if(indexPath.row==1)
        {
            cell.brandactiontag.tag=222;
        }
        else if(indexPath.row==2)
        {
            cell.brandactiontag.tag=333;
        }
        else if(indexPath.row==3)
        {
             NSLog(@"myappDelegate.brandGlobalarray.counts...%d",myappDelegate.brandGlobalarray.count);
            cell.brandactiontag.tag=444;
        }
        else if(indexPath.row==4)
        {
            cell.brandactiontag.tag=777;
        }

        return  cell;
    }
    else if(tableView ==dropdownview)
    {
        
        UITableViewCell *tbcell=[tableView dequeueReusableCellWithIdentifier:@"cell"];
        if(tbcell==nil)
        {
            tbcell=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
        }
        tbcell.textLabel.font = [UIFont fontWithName:@"Muli" size:15];
        
        tbcell.textLabel.textColor=[UIColor whiteColor];//
        
        tbcell.textLabel.backgroundColor=[UIColor clearColor];
        tbcell.selectionStyle=UITableViewCellSelectionStyleNone;
        tbcell.contentView.backgroundColor=[UIColor clearColor];
        tbcell.backgroundColor=[UIColor clearColor];
        
        if (tableView.tag==111)
        {
            
            LookupModelobject *lookobj=[myappDelegate.catogoryGlobalarray objectAtIndex:indexPath.section];
            
            LookupModelobject *obj=[lookobj.categoryDataArray objectAtIndex:indexPath.row];
            
            tbcell.textLabel.text=obj.loolupValue;
            if (obj.isCheckMark)
            {
                tbcell.accessoryType=UITableViewCellAccessoryCheckmark;
                tbcell.tintColor = [UIColor whiteColor];
            }
            else
                tbcell.accessoryType=UITableViewCellAccessoryNone;
            
            NSLog(@"obj.loolupValue....%@",tbcell.textLabel.text);
            NSLog(@"access type %d",tbcell.accessoryType);
            return tbcell;
            
        }
        else if (tableView.tag==222)
        {
            LookupModelobject *lookobj=[myappDelegate.sizeGlobalarray objectAtIndex: indexPath.row];
            
            tbcell.textLabel.text=lookobj.loolupValue;
            if (lookobj.isCheckMark)
            {
                tbcell.accessoryType=UITableViewCellAccessoryCheckmark;
                tbcell.tintColor = [UIColor whiteColor];
            }
            else
                tbcell.accessoryType=UITableViewCellAccessoryNone;
            
            NSLog(@"obj.loolupValue....%@",tbcell.textLabel.text);
            NSLog(@"access type %d",tbcell.accessoryType);
            return tbcell;

            
            // tbcell.textLabel.textColor=[UIColor colorWithRed:131.0/255.0 green:131.0/255.0 blue:131.0/255.0 alpha:1.0];
            return tbcell;
        }
        else if (tableView.tag==333)
        {
            LookupModelobject *lookobj=[myappDelegate.conditionGlobalarray objectAtIndex: indexPath.row];
            
            tbcell.textLabel.text=lookobj.loolupValue;
            if (lookobj.isCheckMark)
            {
                tbcell.accessoryType=UITableViewCellAccessoryCheckmark;
                tbcell.tintColor = [UIColor whiteColor];
            }
            else
                tbcell.accessoryType=UITableViewCellAccessoryNone;
            
            
            NSLog(@"access type %d",tbcell.accessoryType);
            return tbcell;
            
            
        }
        else if (tableView.tag==444)
        {
            LookupModelobject *lookobj=[myappDelegate.brandGlobalarray objectAtIndex: indexPath.row];
            
            tbcell.textLabel.text=lookobj.loolupValue;
            
            if (lookobj.isCheckMark)
            {
                tbcell.accessoryType=UITableViewCellAccessoryCheckmark;
                tbcell.tintColor = [UIColor whiteColor];
            }
            else
                tbcell.accessoryType=UITableViewCellAccessoryNone;
            
           
            NSLog(@"access type %d",tbcell.accessoryType);
            return tbcell;
            
            
        }
        
        else if (tableView.tag==777)
        {
            LookupModelobject *lookobj=[myappDelegate.genderglobalaray objectAtIndex: indexPath.row];
            if (lookobj.isCheckMark)
            {
                tbcell.accessoryType=UITableViewCellAccessoryCheckmark;
                tbcell.tintColor = [UIColor whiteColor];
                NSLog(@"access type %d",tbcell.accessoryType);
            }
            else
                tbcell.accessoryType=UITableViewCellAccessoryNone;
            
            
            NSLog(@"access type %d",tbcell.accessoryType);
            tbcell.textLabel.text=lookobj.loolupValue;
            
            return tbcell;
            
        }
        
        
    }
    return nil;
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
