//
//  MessageCustomcell.h
//  messagetesting
//
//  Created by Varma Bhupatiraju on 09/06/14.
//  Copyright (c) 2014 Varma Bhupatiraju. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MessageCustomcell : UITableViewCell
@property(nonatomic,strong)IBOutlet UILabel *namelabel;
@property(nonatomic,strong)IBOutlet UILabel *textlabel;
@property(nonatomic,strong)IBOutlet UIImageView *firstimageview;
@property(nonatomic,strong)IBOutlet UILabel *timelabel;
@end
