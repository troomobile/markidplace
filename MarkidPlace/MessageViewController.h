//
//  MessageViewController.h
//  messagetesting
//
//  Created by Varma Bhupatiraju on 09/06/14.
//  Copyright (c) 2014 Varma Bhupatiraju. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "MessageModelobject.h"
#import "RegistrationRepository.h"
#import "HelperProtocol.h"
#import "ChartModelObject.h"
#import "ODRefreshControl.h"
#import "ProfileRepository.h"
#import "ChartViewController.h"

@class RegistrationRepository;
@class MessageCustomcell;
@class AppDelegate;
@interface MessageViewController : UIViewController<UITableViewDelegate,UITableViewDataSource,UISearchBarDelegate,HelperProtocol>
{
    CGRect screenSize;
  //  AppDelegate *appdelegate;
    
    NSMutableArray *duplicatearray;
    
     RegistrationRepository *registrationRep;
     ProfileRepository *profileRep;
     ODRefreshControl *refreshControl;
    
}
@property(nonatomic,strong)IBOutlet UITableView *msgtableview;
@property(nonatomic,strong) IBOutlet UINavigationBar *navbar;
@property(nonatomic,strong) IBOutlet  UILabel *ListingLabel;
@property(nonatomic,strong) IBOutlet  UISearchBar *msgsearchbar;

@property(nonatomic,strong) id<HelperProtocol>delegate;

@property(nonatomic,strong) NSMutableArray *itemsarray;
@property(nonatomic,strong)IBOutlet MessageCustomcell *mymsgcustomcell;

-(IBAction)backAction:(id)sender;
-(IBAction)homeBtnAction:(id)sender;


@end
