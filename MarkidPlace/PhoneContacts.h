//
//  PhoneContacts.h
//  MarkidPlace
//
//  Created by Stellent_Krish on 2/4/15.
//  Copyright (c) 2015 stellentmac1. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface PhoneContacts : NSManagedObject

@property (nonatomic, retain) NSString * username;
@property (nonatomic, retain) NSString * recordid;
@property (nonatomic, retain) NSString * homemailid;
@property (nonatomic, retain) NSString * workmailid;

@end
