//
//  ForgetPasswordViewController.h
//  MarkidPlace
//
//  Created by stellent_mac_05 on 03/07/14.
//  Copyright (c) 2014 stellentmac1. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "LoginViewController.h"
#import "ProfileRepository.h"
#import "HelperProtocol.h"


@class AppDelegate;
@interface ForgetPasswordViewController : UIViewController<UITextFieldDelegate,UIAlertViewDelegate,NSURLConnectionDelegate,HelperProtocol>
{

    AppDelegate*appdelegate;
IBOutlet UILabel *titlelbl;
IBOutlet UINavigationBar *navbar;
UITextField *currentTextField;
    id <HelperProtocol> delegate;
    ProfileRepository*profilerepo;


}

@property(nonatomic ,strong)UIButton *donebutton;
@property(nonatomic,strong)NSMutableData *responseData;
@property(nonatomic,strong)NSMutableURLRequest *request;
@property(nonatomic,strong) IBOutlet UIImageView*emailimgview;
@property(nonatomic,strong) IBOutlet UITextField *emailTf;

@property(nonatomic,strong) IBOutlet UILabel *donelbl;
@property(nonatomic,strong)NSDictionary*loginresponsedict;
@property(nonatomic,strong)NSString*loginstr;

-(IBAction)backbuttonAction:(id)sender;
-(IBAction)donebuttonAction:(id)sender;




@end
