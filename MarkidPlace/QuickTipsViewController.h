//
//  QuickTipsViewController.h
//  MarkidPlace
//
//  Created by Varma Bhupatiraju on 17/09/14.
//  Copyright (c) 2014 stellentmac1. All rights reserved.
//

#import <UIKit/UIKit.h>
@class AppDelegate;
@interface QuickTipsViewController : UIViewController<UIWebViewDelegate>
{
IBOutlet UIWebView *webview;
IBOutlet UIActivityIndicatorView *spinner;
AppDelegate *appdelegate;
}
@property(nonatomic,strong) NSString *htmlMainString;
@property(nonatomic,strong) NSString *titleString;

@property(nonatomic,strong)IBOutlet UIWebView *quicktipswebview;
@property(nonatomic,strong)IBOutlet UILabel *titlbl;
@property(nonatomic,strong)IBOutlet UIButton *homebtn;
@property(nonatomic,strong) NSString *presentstr;
@property (nonatomic,strong) IBOutlet UINavigationBar *navbar;
@property (nonatomic,strong) IBOutlet UIScrollView *quitScrollView;
-(IBAction)backBtn:(id)sender;
-(IBAction)homeBtnAction:(id)sender;
@end
