//
//  ProfileViewController.h
//  MarkidPlace
//
//  Created by stellentmac1 on 5/21/14.
//  Copyright (c) 2014 stellentmac1. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NewListingViewController.h"
#import "ListingDetailViewController.h"
#import "AppDelegate.h"
#import "LoginViewController.h"
#import "BuyandSellViewController.h"
#import "NotificationViewController.h"
#import "MyWishlistViewController.h"
#import "MyFeedViewController.h"
#import "HomeViewController.h"
#import "SuggestionsViewController.h"

#import "EditProfileViewController.h"

#import "SettingsViewController.h"
#import "ProfileRepository.h"

#import "HelperProtocol.h"
#import "UserLists.h"
#import "Users.h"
#import "Item.h"
#import <Pinterest/Pinterest.h>
#import "RegistrationRepository.h"
#import <FacebookSDK/FacebookSDK.h>



@class AppDelegate;
@class ProfileRepository;

@interface ProfileViewController : UIViewController<HelperProtocol,UIActionSheetDelegate>
{
  //  AppDelegate *appdelegate;
    CGRect screenSize;
    ProfileRepository *profileRepo;
    RegistrationRepository *registrationRep;
    id<HelperProtocol>delegate;
    
    Pinterest*  _pinterest;
    // for menu
     BOOL ismenu;
    IBOutlet UIImageView *myaccountImgView;
    IBOutlet UIImageView *myclosetImgView;
    IBOutlet UIImageView *myfeedImgView;
    IBOutlet UIImageView *mywishlistImgView;
    IBOutlet UIImageView *shopAllImgView;
    IBOutlet UIImageView *sellImgView;
    IBOutlet UIImageView *notificationimgview;
    
    NSTimer*timer;

    
    NSString *paypalAddress;
    NSString*str;
}
-(IBAction)notificationcount11:(id)sender;

@property (nonatomic,strong) IBOutlet UIView *notificationview;    @property (nonatomic,strong)IBOutlet UIView *profilenotificationview;
@property (nonatomic,strong)IBOutlet UIView *msgnotificationview;
@property (nonatomic,strong) IBOutlet UILabel *notificationcountLabel;
@property (nonatomic,strong) IBOutlet UILabel *msgnotificationcountLabel;

@property(nonatomic,strong)id <HelperProtocol> delegate;

@property (nonatomic,strong) IBOutlet UILabel *notificationcountuplabel;
@property (nonatomic,strong) IBOutlet UILabel *usernameLabel;
@property (nonatomic,strong) IBOutlet UILabel *nameLabel;
@property (nonatomic,strong) IBOutlet UILabel *starLabel;
@property (nonatomic,strong) IBOutlet UILabel *itemsLabel;
@property (nonatomic,strong) IBOutlet UILabel *itemsCountLabel;
@property (nonatomic,strong) IBOutlet UILabel *likesLabel;
@property (nonatomic,strong) IBOutlet UILabel *likesCountLabel;
@property (nonatomic,strong) IBOutlet UILabel *followersLabel;
@property (nonatomic,strong) IBOutlet UILabel *followersCountLabel;
@property (nonatomic,strong) IBOutlet UILabel *followingLabel;
@property (nonatomic,strong) IBOutlet UILabel *followingCountLabel;
@property (nonatomic,strong) IBOutlet UILabel *createListLabel;
@property (nonatomic,strong) IBOutlet UILabel *titleLabel;
@property (nonatomic,strong) IBOutlet UILabel *priceLabel;
@property (nonatomic,strong) IBOutlet UIImageView *profileImageView;

@property (nonatomic,strong) IBOutlet UIScrollView *itemscrollview;
@property(nonatomic,strong) NSMutableArray *itemsarray;
@property(nonatomic,strong) NSMutableArray *picturesarray;


-(IBAction)createListAction:(id)sender;
-(IBAction)listingDetailAction:(id)sender;
-(IBAction)backAction:(id)sender;
-(IBAction)logoutButtonAction:(id)sender;
-(IBAction)MsgAction:(id)sender;
-(void)createscrollview;

-(IBAction)sellerButtonAction:(id)sender;
-(IBAction)followersclicked:(id)sender;
-(IBAction)followingclicked:(id)sender;
-(IBAction)notificationclicked:(id)sender;


/// for menu


@property (nonatomic,strong) IBOutlet UILabel *itemLabel1;
@property (nonatomic,strong) IBOutlet UILabel *itempriceLabel1;
@property (nonatomic,strong) IBOutlet UILabel *oldpriceLabel1;
@property (nonatomic,strong) IBOutlet UILabel *myAccountLabel;
@property (nonatomic,strong) IBOutlet UILabel *myClosetLabel;
@property (nonatomic,strong) IBOutlet UILabel *myfeedLabel;
@property (nonatomic,strong) IBOutlet UILabel *mywishlistLabel;
@property (nonatomic,strong) IBOutlet UILabel *shopallLabel;
@property (nonatomic,strong) IBOutlet UILabel *sellLabel;
@property (nonatomic,strong) IBOutlet UILabel *cartcountLabel;
@property (nonatomic,strong) IBOutlet UIView *cartview;
@property (nonatomic,strong) IBOutlet UIView *menuview;
@property (nonatomic,strong) IBOutlet UIButton *menuBtn;

@property (nonatomic,strong) IBOutlet UIView *menubtnview;
@property (nonatomic,strong) IBOutlet UIImageView *animationimgv;

-(IBAction)filterButtonTapped:(id)sender;
-(IBAction)menuBtnAction:(id)sender;
-(IBAction)menuItemTapped:(id)sender;
@end
