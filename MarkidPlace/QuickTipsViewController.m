//
//  QuickTipsViewController.m
//  MarkidPlace
//
//  Created by Varma Bhupatiraju on 17/09/14.
//  Copyright (c) 2014 stellentmac1. All rights reserved.
//

#import "QuickTipsViewController.h"
#import "SuggestionsViewController.h"
@interface QuickTipsViewController ()

@end

@implementation QuickTipsViewController
@synthesize quitScrollView,presentstr,homebtn,quicktipswebview;
@synthesize navbar,htmlMainString,titleString,titlbl;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    appdelegate=(AppDelegate *)[[UIApplication sharedApplication]delegate];
    
    self.navbar.frame = CGRectMake(0, 0, 320, 64);
    [self.navbar setBackgroundImage:[UIImage imageNamed:@"navbar.png"] forBarMetrics:UIBarMetricsDefault];
    self.titlbl.font = [UIFont fontWithName:@"Monroe" size:18];
    
    
    NSString *htmlFile = [[NSBundle mainBundle] pathForResource:htmlMainString  ofType:@"html" inDirectory:nil];
    
    NSString* htmlString;
    
        htmlString = [NSString stringWithContentsOfFile:htmlFile encoding:NSUTF8StringEncoding error:nil];
        
    
    [quicktipswebview loadHTMLString:htmlString baseURL:nil];
    // Do any additional setup after loading the view from its nib.
    
    
    
}
-(IBAction)homeBtnAction:(id)sender
{
    
    NSArray *arr=self.navigationController.viewControllers;
    for (UIViewController *childvc in arr) {
        
        if([childvc isKindOfClass:[SuggestionsViewController class]])
        {
            SuggestionsViewController *subvc=(SuggestionsViewController *)childvc;
            subvc.ismyfeed=NO;
            [self.navigationController popToViewController:subvc animated:YES];
        }
    }
    
}
-(UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

-(IBAction)backBtnPressed:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)webViewDidStartLoad:(UIWebView *)webView
{
    // [spinner startAnimating];
    
    [appdelegate stopspinner:self.view];

   
}
-(void)webViewDidFinishLoad:(UIWebView *)webView
{

    if ([titleString isEqualToString:@"shipping-weights"])
    {
        [webView stringByEvaluatingJavaScriptFromString: @"window.location.hash='#shipingbasic'"];
        
    }
    //  [spinner stopAnimating];

}

-(void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    
    // NSLog(@"Error for WEBVIEW: %@", [error description]);
    [spinner stopAnimating];
}
- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
    if ([request.URL.scheme isEqualToString:@"inapp"]) {
        if ([request.URL.host isEqualToString:@"faq"]) {
            // do capture action
            
            NSLog(@"Faq is ");
            
            [self loadFAQ];
        }
        return NO;
    }
    return YES;
}
-(void)loadFAQ
{
    [appdelegate startspinner:self.view];
    
    titlbl.text=@"FAQ";
    NSString *htmlFile = [[NSBundle mainBundle] pathForResource:@"MKPFAQrevised"  ofType:@"html" inDirectory:nil];
    NSString* htmlString = [NSString stringWithContentsOfFile:htmlFile encoding:NSUTF8StringEncoding error:nil];
    
    [quicktipswebview loadHTMLString:htmlString baseURL:nil];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
