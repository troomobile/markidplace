//
//  FollowViewController.h
//  MarkidPlace
//
//  Created by stellent on 22/05/14.
//  Copyright (c) 2014 stellentmac1. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ProfileRepository.h"
#import "HelperProtocol.h"
#import "ItemsModelobject.h"
#import "AppDelegate.h"
#import "Users.h"
#import "RegistrationRepository.h"
#import "FollowersViewController.h"

@class ItemsModelobject;

@class ProfileRepository;

@class AppDelegate;
@interface FollowViewController : UIViewController<UIScrollViewDelegate,HelperProtocol>
{
    
 //   AppDelegate *appdelegate;

    CGRect screenSize;
    BOOL ismenu;
    int delaytime;
    
    IBOutlet UIButton *followbtn;
    IBOutlet UIImageView  *followImageView;
    IBOutlet UIImageView  *userimageview;
    
    NSString *followstring;
     NSString *usernamestr;
    
    
    Users *userObj;
    
     RegistrationRepository *registrationRep;
}


@property(nonatomic,strong) NSString *contactStr;
@property(nonatomic,strong) NSString *compareStr;
@property(nonatomic,strong) NSString *nameString;
@property(nonatomic,strong) NSString *usernameString;
@property(nonatomic,strong) NSString *imageString;
@property(nonatomic,strong) NSString *userId;
@property(nonatomic,strong) NSString *imFronVC;


@property(nonatomic,strong) NSString *useridstr;
@property(nonatomic,strong) NSString *notifystr;

@property(nonatomic,strong) ItemsModelobject *sltobj;
@property(nonatomic,strong) IBOutlet UIButton *followbtn;

@property(nonatomic) NSMutableArray *dataArray;
@property (nonatomic) ProfileRepository *profRepo;
@property id <HelperProtocol>  delegate;
@property (nonatomic,strong) IBOutlet UIView *menuview;
@property (nonatomic,strong) IBOutlet UIScrollView *followscrlview;
@property (nonatomic,strong) IBOutlet UILabel *usernameLabel;
@property (nonatomic,strong) IBOutlet UILabel *nameLabel;
@property (nonatomic,strong) IBOutlet UILabel *adressLabel;
@property (nonatomic,strong) IBOutlet UILabel *starLabel;
@property (nonatomic,strong) IBOutlet UILabel *itemsLabel;
@property (nonatomic,strong) IBOutlet UILabel *likesLabel;
@property (nonatomic,strong) IBOutlet UILabel *followersLabel;
@property (nonatomic,strong) IBOutlet UILabel *followingLabel;
@property (nonatomic,strong) IBOutlet UILabel *itemsCountLabel;
@property (nonatomic,strong) IBOutlet UILabel *likesCountLabel;
@property (nonatomic,strong) IBOutlet UILabel *followersCountLabel;
@property (nonatomic,strong) IBOutlet UILabel *followingCountLabel;
@property(nonatomic,strong) NSMutableArray *listdataarray;



///

@property (nonatomic,strong) IBOutlet UILabel *itemTitle1Label;
@property (nonatomic,strong) IBOutlet UILabel *itemPrice1Label;
@property (nonatomic,strong) IBOutlet UILabel *itemTitle2Label;
@property (nonatomic,strong) IBOutlet UILabel *itemPrice2Label;
@property (nonatomic,strong) IBOutlet UILabel *itemTitle3Label;
@property (nonatomic,strong) IBOutlet UILabel *itemPrice3Label;
@property (nonatomic,strong) IBOutlet UILabel *itemTitle4Label;
@property (nonatomic,strong) IBOutlet UILabel *itemPrice4Label;
@property (nonatomic,strong) IBOutlet UILabel *itemTitle5Label;
@property (nonatomic,strong) IBOutlet UILabel *itemPrice5Label;
@property (nonatomic,strong) IBOutlet UILabel *itemTitle6Label;
@property (nonatomic,strong) IBOutlet UILabel *itemPrice6Label;
@property (nonatomic,strong) IBOutlet UIButton *menuBtn;

-(IBAction)followAction:(id)sender;
-(IBAction)menuBtnAction:(id)sender;
-(IBAction)backAction:(id)sender;

-(IBAction)messagebuttonaction:(id)sender;


-(IBAction)getfollowersandfollwoing:(id)sender;
@end
