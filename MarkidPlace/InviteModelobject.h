//
//  InviteModelobject.h
//  MarkidPlace
//
//  Created by Varma Bhupatiraju on 17/09/14.
//  Copyright (c) 2014 stellentmac1. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface InviteModelobject : NSObject
@property(nonatomic,strong) NSString *namestring;
@property(nonatomic,strong) NSString *imgstring;
@property(nonatomic,strong) NSString *accountstr;
@property(nonatomic,strong) NSString *accountimgstr;
@end
