//
//  FeePolicyViewController.m
//  MarkidPlace
//
//  Created by Varma Bhupatiraju on 31/05/14.
//  Copyright (c) 2014 stellentmac1. All rights reserved.
//

#import "FeePolicyViewController.h"

@interface FeePolicyViewController ()

@end

@implementation FeePolicyViewController
@synthesize firstlabel,secondlabel,thirdlabel,fourthlabel,fifthlabel,sevenlabel,sixthlabel,eiglabel,viewtitlelbl;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
   // appdelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
   self.firstlabel.font = [UIFont fontWithName:@"Monroe Bold" size:22];
    //self.firstlabel.font = [UIFont fontWithName:@"Muli-Bold" size:15];
    self.secondlabel.font = [UIFont fontWithName:@"Muli" size:13];
    self.thirdlabel.font = [UIFont fontWithName:@"Muli" size:13];
    self.fourthlabel.font = [UIFont fontWithName:@"Muli" size:13];
    self.fifthlabel.font = [UIFont fontWithName:@"Muli" size:13];
    self.sixthlabel.font = [UIFont fontWithName:@"Muli" size:13];
    self.sevenlabel.font = [UIFont fontWithName:@"Muli" size:13];
    self.eiglabel.font = [UIFont fontWithName:@"Muli" size:13];
    viewtitlelbl.font=[AppDelegate navtitlefont];
    // Do any additional setup after loading the view from its nib.
}
- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)dismassbtnaction:(id)sender
{
    [myappDelegate.naviCon dismissViewControllerAnimated:YES completion:nil];
}
-(void)internetConnection
{
    UIAlertView *Internetalert=[[UIAlertView alloc]initWithTitle:appname message:@"Internet connection not available" delegate:self cancelButtonTitle:nil otherButtonTitles:@"Ok", nil];
    [Internetalert show];
    [APPDELEGATE stopspinner:self.view];
    
}
@end
