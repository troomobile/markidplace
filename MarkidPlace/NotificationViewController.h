//
//  NotificationViewController.h
//  MarkidPlace
//
//  Created by stellent_mac_05 on 10/06/14.
//  Copyright (c) 2014 stellentmac1. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "NotificationCell.h"

#import "RegistrationRepository.h"
#import "ODRefreshControl.h"
#import "NotificationMiodelObject.h"
#import "ProfileRepository.h"
#import "HelperProtocol.h"
@class  ProfileRepository;
@class RegistrationRepository;
@class AppDelegate;
@interface NotificationViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,HelperProtocol,UISearchBarDelegate>
{
    AppDelegate*appdelegate;
    
      ODRefreshControl *refreshControl;
    
     RegistrationRepository *registrationRep;
    ProfileRepository *profilerep;
    NSMutableArray *notificationarray;
    int start;
    
}
 @property (nonatomic,strong) NSMutableArray *getlistarray;
@property (nonatomic,strong) IBOutlet UINavigationBar *navbar;
@property (nonatomic,strong) IBOutlet UITableView *notificationlistview;
@property (nonatomic,strong) IBOutlet NotificationCell *notiicationcell;
@property (nonatomic,strong) IBOutlet UILabel *titlelbl;

-(IBAction)backbtnAction:(id)sender;

-(IBAction)homebtnaction:(id)sender;
-(void)loadMoreRecords;
@end
