//
//  ChartViewController.h
//  MarkidPlace
//
//  Created by Varma Bhupatiraju on 11/06/14.
//  Copyright (c) 2014 stellentmac1. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ChartViewController.h"
#import "AppDelegate.h"
#import "RegistrationRepository.h"
#import "HelperProtocol.h"
#import "NSDate+PostedTimeAgo.h"
#import "ItemsModelobject.h"

@class RegistrationRepository;
@class AppDelegate;
@interface ChartViewController : UIViewController<UIScrollViewDelegate,UISearchBarDelegate,UISearchDisplayDelegate,UIActionSheetDelegate,UIImagePickerControllerDelegate,UITextFieldDelegate,UITextViewDelegate,HelperProtocol>
{
    CGRect screenSize;
   // AppDelegate *appdelegate;
    
    RegistrationRepository *registrationRep;
    
    UIImage *myselectedimage;
    int x;
    int y;
    UILabel*timelabel;
    UIImageView*timeimg;
    NSString *loginuserpick;
    NSDate *methodStart;
    NSDate *methodFinish;
}

//@property(nonatomic,strong) ItemsModelobject *selectedobject;

@property (nonatomic,strong) IBOutlet UINavigationBar *navbar;
@property (nonatomic,strong) IBOutlet UIScrollView *chartscrollview;
@property (nonatomic,strong) IBOutlet UISearchBar *chartsearchbar;


@property(nonatomic,readwrite)BOOL isloginseller;
@property (nonatomic,strong)NSString *titlestr;
@property(nonatomic,strong) NSString *selleruseridstr;

@property(nonatomic,strong) NSString *profilepicstr;

@property (nonatomic,strong) IBOutlet UILabel *titlbl;



@property (nonatomic,strong) NSMutableArray *itemsarray;
@property (nonatomic,strong) IBOutlet UITextField *sendtextfield;
@property (nonatomic,strong) IBOutlet UIView *cameraview;

@property(nonatomic,strong)IBOutlet UIImageView *camimageview;




@property (nonatomic,strong) NSMutableArray *duplicatearray;

-(IBAction)backAction:(id)sender;
-(IBAction)homeBtnAction:(id)sender;
-(IBAction)CameraBtnAction:(id)sender;
-(IBAction)SendBtnAction:(id)sender;

-(IBAction)Send1BtnAction:(id)sender;
-(IBAction)rec1BtnAction:(id)sender;
-(IBAction)SenBtnAction:(id)sender;
-(IBAction)rectnAction:(id)sender;

@end
