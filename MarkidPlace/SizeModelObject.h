//
//  SizeModelObject.h
//  MarkidPlace
//
//  Created by Varma Bhupatiraju on 14/08/14.
//  Copyright (c) 2014 stellentmac1. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SizeModelObject : NSObject

@property(nonatomic,strong)NSString *sizeName;
@property(nonatomic,strong)NSString *sizeId;
@property(nonatomic,readwrite)BOOL isSelect;

@end
