//
//  CartViewCell.h
//  MarkidPlace
//
//  Created by stellent_mac_05 on 10/06/14.
//  Copyright (c) 2014 stellentmac1. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CartViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *orderfromlbl;
@property (strong, nonatomic) IBOutlet UILabel *usernamelbl;
@property (strong, nonatomic) IBOutlet UILabel *itemdetailslbl;
@property (strong, nonatomic) IBOutlet UILabel *itemnamelbl;
@property (strong, nonatomic) IBOutlet UILabel *sizelbl;
@property (strong, nonatomic) IBOutlet UILabel *sizennumlbl;
@property (strong, nonatomic) IBOutlet UILabel *itemcostlbl;
@property (strong, nonatomic) IBOutlet UILabel *purchasecostlbl;
@property (strong, nonatomic) IBOutlet UILabel *purchaselbl;
@property (strong, nonatomic) IBOutlet UILabel *chargeslbl;
@property (strong, nonatomic) IBOutlet UILabel *shipingoptionlbl;
@property (strong, nonatomic) IBOutlet UILabel *alternateadresslbl;
//@property (strong, nonatomic) IBOutlet UITextView *adresstxtview;
@property (strong, nonatomic) IBOutlet UIImageView *itemimgview;
@property (strong, nonatomic) IBOutlet UIButton *cancelbtn;
@property (strong, nonatomic) IBOutlet UIButton *checkoutbtn;
@property (strong, nonatomic) IBOutlet UIImageView *switchImageView;
@property (strong, nonatomic) IBOutlet UILabel *checkoutlbl;
@property (strong, nonatomic) IBOutlet UIButton *switchbtn;
@property (strong, nonatomic) IBOutlet UIImageView *textviewimageview;
@property (strong, nonatomic) IBOutlet UITextView *addresstextView;

@property (strong, nonatomic) IBOutlet UITextField *add1Tf;
@property (strong, nonatomic) IBOutlet UITextField *add2Tf;
@property (strong, nonatomic) IBOutlet UITextField *cityTf;
@property (strong, nonatomic) IBOutlet UITextField *stateTf;
@property (strong, nonatomic) IBOutlet UITextField *zipcodeTf;
@property (strong, nonatomic) IBOutlet UILabel *shippingcostlbl;
@property (strong, nonatomic) IBOutlet UILabel *shippingcostpricelbl;

@end
