//
//  InviteFriendsViewController.h
//  MarkidPlace
//
//  Created by stellent_mac_05 on 27/06/14.
//  Copyright (c) 2014 stellentmac1. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AddressBook/AddressBook.h>
#import <AddressBookUI/AddressBookUI.h>
#import <FacebookSDK/FacebookSDK.h>
#import <QuartzCore/QuartzCore.h>
#import "InvitefriendCell.h"
#import "Person.h"
#import "AppDelegate.h"
#import "ContactsCell.h"
#import <MessageUI/MessageUI.h>
#import "ASIHTTPRequest.h"
#import "Facebookobject.h"


@class AppDelegate;
@interface InviteFriendsViewController : UIViewController<UISearchBarDelegate,UITableViewDataSource,UITableViewDelegate,MFMailComposeViewControllerDelegate,MFMessageComposeViewControllerDelegate,ASIHTTPRequestDelegate>
{
    AppDelegate *appdelegate;
    CFIndex nPeople;
    CFArrayRef allPeople;
    BOOL issearching;
    int startvalue;
    int indexVal;
    BOOL isSMS;
    NSString *contactstr;
    NSString *savestr;
    Facebookobject *facebookobj;
    
    
    NSString *sepstr;
   

    NSMutableDictionary *facebookDict;
    ASIFormDataRequest*formRequest;

     NSMutableArray *searchresultarray;
    NSMutableArray *finalinvitearray;
    NSMutableArray *facebookarray;
    NSMutableArray *imagesArray;
    NSString *facebookID;
}
@property(nonatomic,strong)NSString *acessToken;
@property(nonatomic,strong)NSString *comparestr;
@property(nonatomic,strong) ACAccount *account;
@property (nonatomic,strong) ACAccountStore *accountStore;
@property(nonatomic,strong)NSMutableArray *defaultfriendsarray;
@property (strong, nonatomic) IBOutlet UILabel *titlelbl;
@property (strong, nonatomic) IBOutlet UISearchBar *searchbar;
@property (strong, nonatomic) IBOutlet UINavigationBar *navbar;
@property (strong, nonatomic) IBOutlet UITableView *friendslistview;
@property (strong, nonatomic) IBOutlet UIButton *Facebookbutton;
@property (strong, nonatomic) IBOutlet UIButton *emailbutton;
@property (strong, nonatomic) IBOutlet UIButton *smsbutton;
@property (strong, nonatomic) IBOutlet UIButton *sharebutton;

@property(nonatomic,strong)NSMutableArray *currentarray;
@property(nonatomic,strong) NSMutableArray *friendsArray;
//@property(nonatomic,strong) NSMutableArray *searchArray;
@property(nonatomic,strong) NSMutableArray *tempArray;

@property (strong, nonatomic) IBOutlet InvitefriendCell *friendscell;
@property (strong, nonatomic) IBOutlet ContactsCell *contactcell;
@property (strong, nonatomic) IBOutlet UIView *contactview;
@property (strong,nonatomic) IBOutlet UITableView *contactslistview;
@property (nonatomic, retain) ABPeoplePickerNavigationController *contacts;

-(IBAction)donebutton:(id)sender;
-(IBAction)backbutton:(id)sender;
-(IBAction)Facebookbutton:(id)sender;
-(IBAction)emailbutton:(id)sender;
-(IBAction)smsbutton:(id)sender;





@property(nonatomic,strong) NSMutableArray *collectionArray;

///
-(IBAction)sharebuttonaction:(id)sender;


@end
