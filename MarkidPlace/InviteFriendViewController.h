//
//  InviteFriendViewController.h
//  MarkidPlace
//
//  Created by Varma Bhupatiraju on 17/09/14.
//  Copyright (c) 2014 stellentmac1. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AddressBook/AddressBook.h>
#import <AddressBookUI/AddressBookUI.h>
#import <FacebookSDK/FacebookSDK.h>
#import <QuartzCore/QuartzCore.h>
#import "RegistrationRepository.h"
#import "Person.h"
#import "AppDelegate.h"
#import "ContactsCell.h"
#import <MessageUI/MessageUI.h>
#import "ASIHTTPRequest.h"
#import "Facebookobject.h"
#import "InviteCustomCell.h"
#import "InvitefriendCell.h"
@class AppDelegate;
@interface InviteFriendViewController : UIViewController<UISearchBarDelegate,UITableViewDataSource,UITableViewDelegate,MFMailComposeViewControllerDelegate,MFMessageComposeViewControllerDelegate,ASIHTTPRequestDelegate,HelperProtocol>
{
    MFMailComposeViewController *mailcomposer;
    
    RegistrationRepository *registrationRep;
     NSMutableDictionary *twitterDict;
    CFIndex nPeople;
    CFArrayRef allPeople;
    BOOL issearching;
    int startvalue;
    int indexVal;
    BOOL isSMS;
    NSString *contactstr;
    NSString *savestr;
    Facebookobject *facebookobj;
    
    
    NSString *sepstr;
    
    
    NSMutableDictionary *facebookDict;
    ASIFormDataRequest*formRequest;
    
    NSMutableArray *searchresultarray;
    NSMutableArray *finalinvitearray;
    NSMutableArray *facebookarray;
    NSMutableArray *imagesArray;
    NSString *facebookID;

    
}


@property(nonatomic,strong)NSString *acessToken;

@property(nonatomic,strong) ACAccount *account;
@property (nonatomic,strong) ACAccountStore *accountStore;
@property(nonatomic,strong)NSMutableArray *defaultfriendsarray;
@property (strong, nonatomic) IBOutlet UILabel *titlelbl;
@property (strong, nonatomic) IBOutlet UISearchBar *searchbar;
@property (strong, nonatomic) IBOutlet UINavigationBar *navbar;
@property (strong, nonatomic) IBOutlet UITableView *friendslistview;
@property (strong, nonatomic) IBOutlet UIButton *Facebookbutton;
@property (strong, nonatomic) IBOutlet UIButton *emailbutton;
@property (strong, nonatomic) IBOutlet UIButton *smsbutton;
@property (strong, nonatomic) IBOutlet UIButton *sharebutton;

@property(nonatomic,strong)NSMutableArray *currentarray;
@property(nonatomic,strong) NSMutableArray *friendsArray;
//@property(nonatomic,strong) NSMutableArray *searchArray;
@property(nonatomic,strong) NSMutableArray *tempArray;

@property (strong, nonatomic) IBOutlet InvitefriendCell *friendscell;
@property (strong, nonatomic) IBOutlet ContactsCell *contactcell;
@property (strong, nonatomic) IBOutlet UIView *contactview;
@property (strong,nonatomic) IBOutlet UITableView *contactslistview;
@property (nonatomic, retain) ABPeoplePickerNavigationController *contacts;

-(IBAction)donebutton:(id)sender;

-(IBAction)Facebookbutton:(id)sender;
-(IBAction)emailbutton:(id)sender;
-(IBAction)smsbutton:(id)sender;
-(IBAction)backAction:(id)sender;



@property(nonatomic,strong)IBOutlet InviteCustomCell  *myinvitecustomcell;
@property(nonatomic,strong) NSMutableArray *myfirstarray;
@property(nonatomic,strong) NSMutableArray *myimagearray;
@property(nonatomic,strong) NSMutableArray *Basicarray;
//@property(nonatomic,strong) IBOutlet UITableView *invitetblview;
@property(nonatomic,strong) NSMutableArray *collectionArray;
@property(nonatomic,strong) IBOutlet UILabel *invitelbl;
///
-(IBAction)sharebuttonaction:(id)sender;



@end
