//
//  SuggestionsViewController.h
//  MarkidPlace
//
//  Created by stellentmac1 on 5/21/14.
//  Copyright (c) 2014 stellentmac1. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "RegistrationRepository.h"
#import "ItemsModelobject.h"
#import "ODRefreshControl.h"
#import "NotificationViewController.h"
#import "iRate.h"
#import "SuggestionObject.h"
#import "HelperProtocol.h"

@class RegistrationRepository;
@class AppDelegate;
@interface SuggestionsViewController : UIViewController<UIScrollViewDelegate,HelperProtocol,iRateDelegate,HelperProtocol>
{
      //AppDelegate *appdelegate;
    CGRect screenSize;
    BOOL ismenu;
    int delaytime;

        RegistrationRepository *registrationRep;
    
    NSMutableArray *itemsarray;
    
    int likeindex;
    
    NSMutableArray *prevlikedarray;
    
    NSTimer*timer;
    
     ODRefreshControl *refreshControl;
    
    
    NSMutableSet *myset;
}
-(IBAction)notificationcount:(id)sender;


@property(nonatomic,strong)id <HelperProtocol> delegate;

@property (nonatomic,strong) iRate *rateRef;
@property (nonatomic,readwrite) BOOL isItFirstTime;

@property (nonatomic,readwrite)BOOL ismyfeed;
@property (nonatomic,readwrite)BOOL loadfeeddatabool;
@property (nonatomic,readwrite)BOOL loaddatabool;
@property (nonatomic,strong) IBOutlet UIImageView *myfeedImageview;
@property (nonatomic,strong) IBOutlet UIImageView *mySuggestionsImageview;
@property (nonatomic,strong) IBOutlet UIImageView *filterImageview;

@property (nonatomic,strong) IBOutlet UIScrollView *itemscrollview;
@property (nonatomic,strong) IBOutlet UINavigationBar *navbar;
@property (nonatomic,strong) IBOutlet UILabel *itemLabel1;
@property (nonatomic,strong) IBOutlet UILabel *notificationcountlabel;
@property (nonatomic,strong) IBOutlet UILabel *itemLabel2;
@property (nonatomic,strong) IBOutlet UILabel *itempriceLabel1;
@property (nonatomic,strong) IBOutlet UILabel *itempriceLabel2;
@property (nonatomic,strong) IBOutlet UILabel *oldpriceLabel1;
@property (nonatomic,strong) IBOutlet UILabel *oldpriceLabel2;
@property (nonatomic,strong) IBOutlet UILabel *myAccountLabel;
@property (nonatomic,strong) IBOutlet UILabel *myClosetLabel;
@property (nonatomic,strong) IBOutlet UILabel *myfeedLabel;
@property (nonatomic,strong) IBOutlet UILabel *mywishlistLabel;
@property (nonatomic,strong) IBOutlet UILabel *shopallLabel;
@property (nonatomic,strong) IBOutlet UILabel *sellLabel;
@property (nonatomic,strong) IBOutlet UILabel *cartcountLabel;


@property (nonatomic,strong) IBOutlet UIView *notificationview;
@property (nonatomic,strong) IBOutlet UIView *menubtnview;
@property (nonatomic,strong) IBOutlet UIImageView *animationimgv;

@property (nonatomic,strong) IBOutlet UIView *menuview;
@property (nonatomic,strong) IBOutlet UIButton *menuBtn;


@property (nonatomic,strong) IBOutlet UIView *cartview;



@property (nonatomic,strong) IBOutlet UIImageView *cartimage;
-(IBAction)myFeedButtonTapped:(id)sender;
-(IBAction)mySuggestionsButtonTapped:(id)sender;
-(IBAction)filterButtonTapped:(id)sender;

-(IBAction)notificationclicked:(id)sender;

-(IBAction)menuBtnAction:(id)sender;

-(IBAction)backAction:(id)sender;
-(IBAction)myAccountAction:(id)sender;
-(IBAction)listDetailsAction:(id)sender;

-(IBAction)menuItemTapped:(id)sender;
@end
