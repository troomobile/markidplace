//
//  FilterObject.h
//  MarkidPlace
//
//  Created by stellent_mac_05 on 04/07/14.
//  Copyright (c) 2014 stellentmac1. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FilterObject : NSObject

//@property(nonatomic,strong)NSString *categorystr;
//@property(nonatomic,strong)NSString *sizestr;
//@property(nonatomic,strong)NSString *genderstr;
//@property(nonatomic,strong)NSString *pricestr;
//@property(nonatomic,strong)NSString *brandstr;
//@property(nonatomic,strong)NSString *distancestr;

@property(nonatomic,strong)NSString *cellnamestr;
@property(nonatomic,strong)NSString *cellvaluestr;
@property(nonatomic,strong)NSString *otherstr;
@property(nonatomic,strong)NSString *parentcatid;
@property(nonatomic,readwrite)BOOL  isother;

@end
