//
//  OrderWebViewController.h
//  MarkidPlace
//
//  Created by Stellent_Krish on 2/9/15.
//  Copyright (c) 2015 stellentmac1. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "RegistrationRepository.h"
#import "BuyandSellViewController.h"
@interface OrderWebViewController : UIViewController<UIWebViewDelegate,HelperProtocol,UIAlertViewDelegate>
{

IBOutlet UIWebView *webview;
IBOutlet UIActivityIndicatorView *spinner;
AppDelegate *appdelegate;
    NSString *orderID;
    RegistrationRepository *regisRepository;
    id <HelperProtocol> delegate;
}
@property(nonatomic,strong) IBOutlet UINavigationBar *navbar;
@property(nonatomic,strong) IBOutlet UILabel *titlelbl;
@property(nonatomic,strong) NSString *htmlMainString;
@property(nonatomic,strong) NSString *orderID;
@property(nonatomic,strong) IBOutlet UIWebView *webview;
@property(nonatomic,strong) IBOutlet UIButton *doneBtn;
-(IBAction)backBtnPressed:(id)sender;
@end
