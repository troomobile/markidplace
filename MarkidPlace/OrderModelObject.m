//
//  OrderModelObject.m
//  MarkidPlace
//
//  Created by Stellent Software on 6/17/14.
//  Copyright (c) 2014 stellentmac1. All rights reserved.
//

#import "OrderModelObject.h"

@implementation OrderModelObject


@synthesize  address1Str;
@synthesize address2Str;
@synthesize buyerUseridstr;
@synthesize citystr;
@synthesize orderdatestr;
@synthesize orderidstr;
@synthesize ordernumberstr;
@synthesize orderstatusstr;
@synthesize quantitystr;
@synthesize sellernamestr;
@synthesize statestr;
@synthesize useridstr;
@synthesize userlistingstr;
@synthesize zipcodestr;
@synthesize trackingNumberStr;
@end
