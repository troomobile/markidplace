//
//  EditProfileViewController.h
//  MarkidPlace
//
//  Created by stellentmac1 on 5/31/14.
//  Copyright (c) 2014 stellentmac1. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "ProfileRepository.h"
#import "HelperProtocol.h"
#import "AppDelegate.h"
#import "Users.h"
#import "RKCropImageController.h"
#import "ProfileRepository.h"
#import "HelperProtocol.h"
#import "MySizesViewController.h"
#import "settingsmodelobject.h"
#import "FindFriendsViewController.h"

@class AppDelegate;
@class ProfileRepository;

@interface EditProfileViewController : UIViewController<UITextFieldDelegate,UINavigationControllerDelegate,UIImagePickerControllerDelegate,UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate,UIActionSheetDelegate,HelperProtocol,UIAlertViewDelegate,RKCropImageViewDelegate,HelperProtocol>
{
  //  AppDelegate *appdelegate;
    UITextField *currentTextField;
    ProfileRepository *profileRepos;
    NSString *pic_str;
    NSString *sepstr;
    BOOL isChange;
    BOOL isOther;
    NSString *otherstr;
    BOOL textchangebool;



    ProfileRepository *profilerepo;
    NSMutableArray *brandarray;

}

@property(nonatomic,strong) NSMutableArray *mybrandsarray;
@property(nonatomic,strong)IBOutlet UITableView *dropdowntblview;
@property(nonatomic,strong)IBOutlet UIView *popview;
@property (nonatomic,strong)NSArray *fieldArray;
@property(nonatomic,strong) IBOutlet UILabel *viewtitlelbl;
@property(nonatomic,strong) IBOutlet UILabel *paypallbl;
@property(nonatomic,strong) IBOutlet UIImageView *paypalImgView;
@property(nonatomic,strong) IBOutlet UITextField *paypaltxtField;

@property (nonatomic,readwrite) BOOL isnew;

@property(nonatomic,strong) id<HelperProtocol>delegate;
@property (nonatomic,strong) IBOutlet UINavigationBar *navbar;

@property(nonatomic,strong)IBOutlet UIImageView *profileImgView;


@property(nonatomic,strong) IBOutlet UITextField *userNameTxtFld;
@property(nonatomic,strong) IBOutlet UITextField *nameTextField;
@property(nonatomic,strong) IBOutlet UITextField *address1TextField;
@property(nonatomic,strong) IBOutlet UITextField *address2TextField;
@property(nonatomic,strong)IBOutlet UITextField *cityTextField;
@property(nonatomic,strong)IBOutlet UITextField *stateTextField;
@property(nonatomic,strong)IBOutlet UITextField *zipcodeTextField;
@property(nonatomic,strong)IBOutlet UILabel *userTitleNameLabel;
@property(nonatomic,strong)IBOutlet UILabel *userNameLabel;

@property(nonatomic,strong) IBOutlet UILabel *nameLabel;
@property(nonatomic,strong) IBOutlet UILabel *address1Label;
@property(nonatomic,strong) IBOutlet UILabel *address2Label;
@property(nonatomic,strong)IBOutlet UILabel *cityLabel;
@property(nonatomic,strong)IBOutlet UILabel *stateLabel;
@property(nonatomic,strong)IBOutlet UILabel *zipcodeLabel;
@property(nonatomic,strong)IBOutlet UIButton *backbtn;
@property(nonatomic,strong)IBOutlet UIButton *sizebtn;
@property(nonatomic,strong)IBOutlet UIButton *brandbtn;

@property(nonatomic,strong) IBOutlet UILabel *sizeandbrandlabel;

@property(nonatomic,strong)IBOutlet UIScrollView *bgscrollv;
-(IBAction)backAction:(id)sender;
-(IBAction)doneAction:(id)sender;
-(IBAction)mySizesAndBrands:(id)sender;
-(IBAction)MySizeBtnAction:(id)sender;
-(IBAction)MyBrandsBtnAction:(id)sender;
@end
