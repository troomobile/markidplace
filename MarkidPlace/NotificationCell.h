//
//  NotificationCell.h
//  MarkidPlace
//
//  Created by stellent_mac_05 on 10/06/14.
//  Copyright (c) 2014 stellentmac1. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NotificationCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *notificationlbl;
@property (strong, nonatomic) IBOutlet UILabel *notificationtimelbl;
@property (strong, nonatomic) IBOutlet UIImageView *userimgview;
@property (strong, nonatomic) IBOutlet UIImageView *shareimgview;
@property (strong, nonatomic) IBOutlet UIButton *switchbtn;
//@property (strong, nonatomic) IBOutlet UISwitch *switchbtn;



@end
