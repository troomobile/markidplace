//
//  MenuViewController.m
//  MarkidPlace
//
//  Created by stellentmac1 on 5/21/14.
//  Copyright (c) 2014 stellentmac1. All rights reserved.
//

#import "MenuViewController.h"

@interface MenuViewController ()

@end

@implementation MenuViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)internetConnection
{
    UIAlertView *Internetalert=[[UIAlertView alloc]initWithTitle:appname message:@"Internet connection not available" delegate:self cancelButtonTitle:nil otherButtonTitles:@"Ok", nil];
    [Internetalert show];
    [APPDELEGATE stopspinner:self.view];
    
}
@end
