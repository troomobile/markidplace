//
//  HelperProtocol.h
//  WonUp
//
//  Created by stellentmac1 on 5/15/14.
//  Copyright (c) 2014 stellentmac1. All rights reserved.
//

#ifndef WonUp_HelperProtocol_h
#define WonUp_HelperProtocol_h

#import "HelperProtocol.h"
@protocol HelperProtocol <NSObject>

@optional

-(void)errorMessage:(NSString *)message;
-(void)successResponseforRegistration:(NSDictionary *)responseDict;
-(void)successResponseforPostimgImageandVideo:(NSDictionary *)responseDict;
-(void)successResponseofSocialFeed:(NSMutableArray *)responseArray;
-(void)successResponseofinterests:(NSDictionary *)responseDict;
-(void)successResponseforProfile:(NSDictionary *)responseDict;

-(void)updateprofilesuccessprofile:(NSDictionary *)responseDict;

-(void)successresponceListing :(id)respdict :(NSString *)paramname;
-(void)successResponseforNotification:(NSDictionary *)responseDict;
-(void)updatesuccessResponse:(NSDictionary *)responseDict;
-(void)updatebrandsuccessResponse:(NSDictionary *)responseDict;
/// for pay pal
-(void)successResponseforPaypal:(NSDictionary *)responseDict;
-(void)successResponseforPaypalVerification:(NSDictionary *)responseDict;
-(void)successResponsegetbrandsettings:(NSMutableArray *)responsearray;
-(void)successResponseforDictionary:(NSDictionary *)responseDict;
-(void)successResponseforArray:(NSMutableArray *)responseArray;
-(void)successResponseforsold:(NSDictionary *)responseDict;
-(void)successresponceweight:(NSDictionary*)responseDict;
 -(void)successResponseforlistusers:(NSDictionary *)responseDict;
-(void) successResponseformessageRead:(NSDictionary*)mainDict;
 -(void)successResponseforsold:(NSDictionary *)responseDict;
-(void)internetConnection;
 -(void)successResponseforsizelist:(NSMutableArray *)responseArray;
-(void)successResponseNewRegistrastion:(NSDictionary *)responseDict;
-(void)successResponseforDistance:(NSMutableArray *)responseArray;
-(void)updatenotificationsuccessResponse:(NSDictionary *)responsedict;

-(void)successResponseforfilterBrandslist:(NSMutableArray *)responseArray;
-(void)successresponseofnotificationcount:(NSMutableDictionary *)responsedict;
-(void)successResponseforaddressVerification:(NSMutableDictionary *)responsedict;
-(void)successResponseforDetailVerification:(NSMutableDictionary *)responsedict;
-(void)successResponseforupdateemail:(NSMutableDictionary *)responsedict;


@end
#endif
