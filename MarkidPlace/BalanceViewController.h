//
//  BalanceViewController.h
//  MarkidPlace
//
//  Created by stellent_mac_05 on 17/09/14.
//  Copyright (c) 2014 stellentmac1. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "BalanceCell.h"


@class AppDelegate;
@class BalanceCell;
@interface BalanceViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>
{
    AppDelegate*appdelegate;
    NSArray *balanceArray;
}

@property (nonatomic,strong) IBOutlet UINavigationBar *navbar;
@property (nonatomic,strong) IBOutlet UILabel *titlelbl;
@property (nonatomic,strong) IBOutlet UITableView *balancelistview;
@property (nonatomic,strong) IBOutlet BalanceCell *balancecell;

-(IBAction)backbtnAction:(id)sender;

-(IBAction)homebtnaction:(id)sender;

@end
