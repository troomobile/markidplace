//
//  ProfileRepository.m
//  MarkidPlace
//
//  Created by stellentmac1 on 5/31/14.
//  Copyright (c) 2014 stellentmac1. All rights reserved.
//

#import "ProfileRepository.h"

@implementation ProfileRepository


@synthesize delegate;

-(void)getProfileData:(NSString*)userId
{
    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    if (networkStatus == NotReachable)
    {
        [delegate internetConnection];
        ////NSLog(@"There IS NO internet connection");
    }
    else
    {

    
     //  myappDelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    //http://markidplace.testshell.net/api/Account?UserId=19&LoginUserId=19&format=json

    NSString *urlstr;
    urlstr = [NSString stringWithFormat:@"%@Account?UserId=%@&LoginUserId=%@&format=json",MainUrl,userId,myappDelegate.useridstring];
    
    NSURL *url = [NSURL URLWithString:urlstr];
    
    NSLog(@"*** appdelegate.useridstring *** %@",myappDelegate.useridstring);
     NSLog(@"*** userId *** %@",userId);
    
    NSLog(@"getProfileData url %@",urlstr);
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:3600.0];
    
    [request setHTTPMethod:@"GET"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
     urlconnection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    
    
}
    
}
-(void)myaddressgetuserid:(NSString*)userid
{
    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    if (networkStatus == NotReachable)
    {
        [delegate internetConnection];
        ////NSLog(@"There IS NO internet connection");
    }
    else
    {

    
    
    NSString *urlstr;
    
     urlstr = [NSString stringWithFormat:@"%@GetMyAddress?UserId=%@&format=json",MainUrl,userid,myappDelegate.useridstring];
    
    NSURL *url = [NSURL URLWithString:urlstr];
    
    NSLog(@"*** appdelegate.useridstring *** %@",myappDelegate.useridstring);
    NSLog(@"*** userId *** %@",userid);
    
    NSLog(@"myaddressgetuserid url %@",urlstr);
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:3600.0];
    
    [request setHTTPMethod:@"GET"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    urlconnection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    
    [NSURLConnection sendAsynchronousRequest:request queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         if (error)
         {
             [delegate errorMessage:@"message"];
         }
         else
         {
             NSError *jsonParsingError = nil;
             
             NSMutableDictionary *mainDict = [NSJSONSerialization JSONObjectWithData:data options:0 error:&jsonParsingError];
             
             if (jsonParsingError)
             {
                 //  NSLog(@"JSON ERROR: %@", [jsonParsingError localizedDescription]);
                 [delegate errorMessage:@"message"];
             }
             else
             {
                 // NSLog(@"Users mainDict is %@",mainDict);
                 [delegate successResponseforProfile:mainDict];
               
             }
         }
     }];

    }
}

-(void)postProfileDetails :(NSMutableDictionary *)allDetailsdict : (NSData *)imgData
{
    //appdelegate=(AppDelegate *)[[UIApplication sharedApplication]delegate];
    
    
  //  myappDelegate=(AppDelegate *)[[UIApplication sharedApplication]delegate];
    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    
    if (networkStatus == NotReachable)
    {
        [delegate internetConnection];
        NSLog(@"There IS NO internet connection on postprofile");
    }
    else
    {

    
    NSError *jsonSerializationError = nil;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:allDetailsdict options:NSJSONWritingPrettyPrinted error:&jsonSerializationError];
    
    NSLog(@"*** allDetailsdict *** %@",allDetailsdict);
    
    if(!jsonSerializationError)
    {
        NSString *serJSON = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        NSLog(@"Serialized JSON: %@", serJSON);
    }
    else
    {
        NSLog(@"JSON Encoding Failed: %@", [jsonSerializationError localizedDescription]);
    }
    
    
    //http://wonup.testshell.net/api/Register/Signup?format=json  [POST]
    
    NSURL *url =[NSURL URLWithString:[NSString stringWithFormat:@"%@Account/UpdateUserProfile?format=json",MainUrl]];
    
    NSLog(@"postProfileDetails  url %@",url);
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:6000.0];
       [request setHTTPMethod:@"PUT"];
    
    
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    [request setValue:[NSString stringWithFormat:@"%d", [jsonData length]] forHTTPHeaderField:@"Content-Length"];
    
    [request setHTTPBody:jsonData];
    
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    
    [NSURLConnection sendAsynchronousRequest:request queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         if (error)
         {
             [delegate errorMessage:@"message"];
         }
         else
         {
             NSError *jsonParsingError = nil;
             
             NSDictionary *mainDict = [NSJSONSerialization JSONObjectWithData:data options:0 error:&jsonParsingError];
             
             if (jsonParsingError)
             {
                 NSLog(@"JSON ERROR: %@", [jsonParsingError localizedDescription]);
                 [delegate errorMessage:@"message"];
             }
             else
             {
                 //[delegate successResponseforRegistration:mainDict];
                 
                  dispatch_sync(dispatch_get_main_queue(), ^{
                      NSLog(@"data response %@ ",mainDict);
                       [delegate updateprofilesuccessprofile:mainDict];
                  //[delegate successResponseforRegistration:mainDict];
                  });
                 
             }
             
         }
         
     }];

    
    
    
  }
}
-(void)deleteUserAccountWithUserid:(NSString*)userid andReasonToDelete:(NSString*)reason
{
    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    if (networkStatus == NotReachable)
    {
        [delegate internetConnection];
        ////NSLog(@"There IS NO internet connection");
    }
    else
    {

    //  myappDelegate=(AppDelegate *)[[UIApplication sharedApplication]delegate];
    NSMutableDictionary *dic=[[NSMutableDictionary alloc]init];
    [dic setObject:userid forKey:@"UserId"];
    [dic setObject:reason forKey:@"Reason"];
    NSError *jsonSerializationError = nil;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dic options:NSJSONWritingPrettyPrinted error:&jsonSerializationError];
    
    
    if(!jsonSerializationError)
    {
        NSString *serJSON = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        NSLog(@"Serialized JSON: %@", serJSON);
    }
    else
    {
        NSLog(@"JSON Encoding Failed: %@", [jsonSerializationError localizedDescription]);
    }
    
    //http://wonup.testshell.net/api/Register/Signup?format=json  [POST]
   // http://markidplace.testshell.net/api/DeleteAccount?format=json
    
    NSString *urlstr = [NSString stringWithFormat:@"%@DeleteAccount?format=json",MainUrl];
    
    NSURL *url = [NSURL URLWithString:urlstr];
    
    NSLog(@"deleteUserAccountWithUserid url ----%@",url);
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:6000.0];
    
    [request setHTTPMethod:@"POST"];
    
    
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:jsonData];
    
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    
    [NSURLConnection sendAsynchronousRequest:request queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         if (error)
         {
             [delegate errorMessage:@"message"];
         }
         else
         {
             NSError *jsonParsingError = nil;
             
             NSDictionary *mainDict = [NSJSONSerialization JSONObjectWithData:data options:0 error:&jsonParsingError];
             
             NSLog(@"**** mainDict **** %@",mainDict);
             
             if (jsonParsingError)
             {
                 NSLog(@"JSON ERROR: %@", [jsonParsingError localizedDescription]);
                 [delegate errorMessage:@"message"];
             }
             else
             {
                 //  [delegate successResponseforPaypal:mainDict];
                 if ([mainDict objectForKey:@"message"])
                 {
                    
                   [delegate successResponseforProfile:mainDict];
                 }
                 else
                     [delegate errorMessage:@"message"];
             }
             
         }
         
     }];
    }
}

-(void)changepasswordUserid:(NSString*)UserId currentpassword:(NSString*)currentpassword newpassword:(NSString*)newpassword;
{
    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    if (networkStatus == NotReachable)
    {
        [delegate internetConnection];
        ////NSLog(@"There IS NO internet connection");
    }
    else
    {

    //  myappDelegate=(AppDelegate *)[[UIApplication sharedApplication]delegate];
    NSMutableDictionary *dic=[[NSMutableDictionary alloc]init];
    [dic setObject:UserId forKey:@"UserId"];
    [dic setObject:currentpassword forKey:@"CurrentPassword"];
    [dic setObject:newpassword forKey:@"NewPassword"];
    
    
    NSError *jsonSerializationError = nil;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dic options:NSJSONWritingPrettyPrinted error:&jsonSerializationError];
    
    
    if(!jsonSerializationError)
    {
        NSString *serJSON = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        NSLog(@"Serialized JSON: %@", serJSON);
    }
    else
    {
        NSLog(@"JSON Encoding Failed: %@", [jsonSerializationError localizedDescription]);
    }
    
    //http://wonup.testshell.net/api/Register/Signup?format=json  [POST]
    //http://markidplace.testshell.net/api/Account/ChangePassowrd?format=json

    
    NSString *urlstr = [NSString stringWithFormat:@"%@Account/ChangePassowrd?format=json",MainUrl];
    
    NSURL *url = [NSURL URLWithString:urlstr];
    
    NSLog(@"changepasswordUserid url ----%@",url);
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:6000.0];
    
    [request setHTTPMethod:@"POST"];
    
    
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:jsonData];
    
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    
    [NSURLConnection sendAsynchronousRequest:request queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         if (error)
         {
             [delegate errorMessage:@"message"];
         }
         else
         {
             NSError *jsonParsingError = nil;
             
             NSDictionary *mainDict = [NSJSONSerialization JSONObjectWithData:data options:0 error:&jsonParsingError];
             
             NSLog(@"**** mainDict **** %@",mainDict);
             
             if (jsonParsingError)
             {
                 NSLog(@"JSON ERROR: %@", [jsonParsingError localizedDescription]);
                 [delegate errorMessage:@"message"];
             }
             else
             {
                 //  [delegate successResponseforPaypal:mainDict];
                 if ([mainDict objectForKey:@"message"])
                 {
                     
                     [delegate successResponseforProfile:mainDict];
                 }
                 else
                     [delegate errorMessage:@"message"];
                 
                 
                 
                 
             }
             
         }
         
     }];
    }

}
-(void)updateaddress:(NSString*)userid:(NSMutableDictionary *)updatedic
{
    //appdelegate=(AppDelegate *)[[UIApplication sharedApplication]delegate];
    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    if (networkStatus == NotReachable)
    {
        [delegate internetConnection];
        ////NSLog(@"There IS NO internet connection");
    }
    else
    {

    
    //  myappDelegate=(AppDelegate *)[[UIApplication sharedApplication]delegate];
    
    NSError *jsonSerializationError = nil;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:updatedic options:NSJSONWritingPrettyPrinted error:&jsonSerializationError];
    
    NSLog(@"*** allDetailsdict *** %@",updatedic);
    
    if(!jsonSerializationError)
    {
        NSString *serJSON = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        NSLog(@"Serialized JSON: %@", serJSON);
    }
    else
    {
        NSLog(@"JSON Encoding Failed: %@", [jsonSerializationError localizedDescription]);
    }
    
    
    //http://wonup.testshell.net/api/Register/Signup?format=json  [POST]
    
    NSURL *url =[NSURL URLWithString:[NSString stringWithFormat:@"%@UpdateMyAddress?format=json",MainUrl]];
    
    NSLog(@"updateaddress url ----%@",url);
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:6000.0];
    [request setHTTPMethod:@"PUT"];
    
    
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    [request setValue:[NSString stringWithFormat:@"%d", [jsonData length]] forHTTPHeaderField:@"Content-Length"];
    
    [request setHTTPBody:jsonData];
    
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    
    [NSURLConnection sendAsynchronousRequest:request queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         if (error)
         {
             [delegate errorMessage:@"message"];
         }
         else
         {
             NSError *jsonParsingError = nil;
             
             NSDictionary *mainDict = [NSJSONSerialization JSONObjectWithData:data options:0 error:&jsonParsingError];
             
             if (jsonParsingError)
             {
                 NSLog(@"JSON ERROR: %@", [jsonParsingError localizedDescription]);
                 [delegate errorMessage:@"message"];
             }
             else
             {
                 //[delegate successResponseforRegistration:mainDict];
                 
                 dispatch_sync(dispatch_get_main_queue(), ^{
                     NSLog(@"data response %@ ",mainDict);
                     //[delegate updateprofilesuccessprofile:mainDict];
                     if ([mainDict objectForKey:@"message"])
                     {
                         
                         
                     [delegate updatesuccessResponse:mainDict];
                     }
                     //[delegate successResponseforRegistration:mainDict];
                 });
                 
             }
             
         }
         
     }];
    
   }
}
-(void)notificationbadge:(NSString*)notificationid 
{
    //appdelegate=(AppDelegate *)[[UIApplication sharedApplication]delegate];
    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    if (networkStatus == NotReachable)
    {
        [delegate internetConnection];
        ////NSLog(@"There IS NO internet connection");
    }
    else
    {
        
        
        //  myappDelegate=(AppDelegate *)[[UIApplication sharedApplication]delegate];
        NSMutableDictionary *updatedic=[[NSMutableDictionary alloc]init];
        [updatedic setObject:notificationid forKey:@"UserId"];
        
        
        NSError *jsonSerializationError = nil;
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:updatedic options:NSJSONWritingPrettyPrinted error:&jsonSerializationError];
        
        NSLog(@"*** allDetailsdict *** %@",updatedic);
        
        if(!jsonSerializationError)
        {
            NSString *serJSON = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
            NSLog(@"Serialized JSON: %@", serJSON);
        }
        else
        {
            NSLog(@"JSON Encoding Failed: %@", [jsonSerializationError localizedDescription]);
        }
        
        
        //http://wonup.testshell.net/api/Register/Signup?format=json  [POST]
       // http://markidplace.testshell.net/api/NotificationLog?UserId=56&StartCount=0&format=json
        //NotificationLog?format=json
        
        NSURL *url =[NSURL URLWithString:[NSString stringWithFormat:@"%@UpdateNotificationLog?format=json",MainUrl]];
        
        NSLog(@"notificationbadge url ----%@",url);
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:6000.0];
        [request setHTTPMethod:@"PUT"];
        
        
        [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        
        [request setValue:[NSString stringWithFormat:@"%d", [jsonData length]] forHTTPHeaderField:@"Content-Length"];
        
        [request setHTTPBody:jsonData];
        
        NSOperationQueue *queue = [[NSOperationQueue alloc] init];
        
        [NSURLConnection sendAsynchronousRequest:request queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
         {
             if (error)
             {
                 [delegate errorMessage:@"message"];
             }
             else
             {
                 NSError *jsonParsingError = nil;
                 
                 NSDictionary *mainDict = [NSJSONSerialization JSONObjectWithData:data options:0 error:&jsonParsingError];
                 
                 if (jsonParsingError)
                 {
                     NSLog(@"JSON ERROR: %@", [jsonParsingError localizedDescription]);
                     [delegate errorMessage:@"message"];
                 }
                 else
                 {
                     //[delegate successResponseforRegistration:mainDict];
                     
                     dispatch_sync(dispatch_get_main_queue(), ^{
                         NSLog(@"data response %@ ",mainDict);
                         //[delegate updateprofilesuccessprofile:mainDict];
                         if ([mainDict objectForKey:@"message"])
                         {
                             
                             
                             [delegate updatenotificationsuccessResponse:mainDict];
                         }
                         //[delegate successResponseforRegistration:mainDict];
                     });
                     
                 }
                 
             }
             
         }];
        
    }
}
-(void)updatebrand:(NSDictionary*)dic;
{
    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    if (networkStatus == NotReachable)
    {
        [delegate internetConnection];
        ////NSLog(@"There IS NO internet connection");
    }
    else
    {

    
    NSLog(@"**** dict ***** %@",dic);
    NSError *jsonSerializationError = nil;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dic options:NSJSONWritingPrettyPrinted error:&jsonSerializationError];
    
    if(!jsonSerializationError)
    {
        NSString *serJSON = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        NSLog(@"Serialized JSON: %@", serJSON);
    }
    else
    {
        NSLog(@"JSON Encoding Failed: %@", [jsonSerializationError localizedDescription]);
    }
    
    //http://wonup.testshell.net/api/Register/Signup?format=json  [POST]
    
    
    NSString *urlstr = [NSString stringWithFormat:@"%@MyBrands?format=json",MainUrl];
    
    NSURL *url = [NSURL URLWithString:urlstr];
    
    NSLog(@"notificationbadge url ----%@",url);
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:6000.0];
    
    [request setHTTPMethod:@"POST"];
    
    
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:jsonData];
    
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    
    [NSURLConnection sendAsynchronousRequest:request queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         if (error)
         {
             [delegate errorMessage:@"message"];
         }
         else
         {
             NSError *jsonParsingError = nil;
             
             NSDictionary *mainDict = [NSJSONSerialization JSONObjectWithData:data options:0 error:&jsonParsingError];
             
             NSLog(@"**** mainDict **** %@",mainDict);
             
             if (jsonParsingError)
             {
                 NSLog(@"JSON ERROR: %@", [jsonParsingError localizedDescription]);
                 [delegate errorMessage:@"message"];
             }
             else
             {
                 //  [delegate successResponseforPaypal:mainDict];
                 
                 double delayInSeconds =0.02;
                 
                 dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
                 dispatch_after(popTime, dispatch_get_main_queue(), ^(void)
                                {
                                    NSLog(@"check main dict %@",mainDict);
                                    // [delegate successResponseforRegistration:mainDict];
                                   // [myappDelegate sucessresponceforFollowandunfollow:mainDict];
                                    [delegate updatebrandsuccessResponse:mainDict];
                                    
                                });
                 
                 
             }
             
         }
         
     }];

    }
}
-(void)followOrUnfollow:(NSMutableDictionary *)dict
{
    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    if (networkStatus == NotReachable)
    {
        [delegate internetConnection];
        ////NSLog(@"There IS NO internet connection");
    }
    else
    {

  //  myappDelegate=(AppDelegate *)[[UIApplication sharedApplication]delegate];
    NSLog(@"**** dict ***** %@",dict);
    NSError *jsonSerializationError = nil;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dict options:NSJSONWritingPrettyPrinted error:&jsonSerializationError];
    
    if(!jsonSerializationError)
    {
        NSString *serJSON = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        NSLog(@"Serialized JSON: %@", serJSON);
    }
    else
    {
        NSLog(@"JSON Encoding Failed: %@", [jsonSerializationError localizedDescription]);
    }
    
    //http://wonup.testshell.net/api/Register/Signup?format=json  [POST]
    
    
    NSString *urlstr = [NSString stringWithFormat:@"%@MyFeed/Follow?format=json",MainUrl];
    
    NSURL *url = [NSURL URLWithString:urlstr];
    
    NSLog(@"followOrUnfollow url ----%@",url);
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:6000.0];
    
    [request setHTTPMethod:@"POST"];
    
    
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:jsonData];
    
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    
    [NSURLConnection sendAsynchronousRequest:request queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         if (error)
         {
             [delegate errorMessage:@"message"];
         }
         else
         {
             NSError *jsonParsingError = nil;
             
             NSDictionary *mainDict = [NSJSONSerialization JSONObjectWithData:data options:0 error:&jsonParsingError];
             
             NSLog(@"**** mainDict **** %@",mainDict);
             
             if (jsonParsingError)
             {
                 NSLog(@"JSON ERROR: %@", [jsonParsingError localizedDescription]);
                 [delegate errorMessage:@"message"];
             }
             else
             {
              //  [delegate successResponseforPaypal:mainDict];
                 
                 double delayInSeconds =0.02;
                 
                 dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
                 dispatch_after(popTime, dispatch_get_main_queue(), ^(void)
                                {
                                    NSLog(@"check main dict %@",mainDict);
                                   // [delegate successResponseforRegistration:mainDict];
                                    [myappDelegate sucessresponceforFollowandunfollow:mainDict];
                                });
                 
                 
             }
             
         }
         
     }];
}
}
- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    if (networkStatus == NotReachable)
    {
        [delegate internetConnection];
        ////NSLog(@"There IS NO internet connection");
    }
    else
    {

    respData = [[NSMutableData alloc] init];
    
    NSLog(@"@@@@@@@@@Data is:%@",response);
    }
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    if (networkStatus == NotReachable)
    {
        [delegate internetConnection];
        ////NSLog(@"There IS NO internet connection");
    }
    else
    {

    [respData appendData:data];
    }
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    if (networkStatus == NotReachable)
    {
        [delegate internetConnection];
        ////NSLog(@"There IS NO internet connection");
    }
    else
    {
    NSError *errInfo;
    
    NSDictionary *mainDict= [NSJSONSerialization JSONObjectWithData:respData options:kNilOptions error:&errInfo];
    
    NSLog(@"maindictionary is %@",mainDict);
    
    NSError *jsonParsingError = nil;
    id object = [NSJSONSerialization JSONObjectWithData:respData options:0 error:&jsonParsingError];
    
    if (jsonParsingError)
    {
        NSLog(@"JSON ERROR: %@", [jsonParsingError localizedDescription]);
    }
    else
    {
        NSLog(@"OBJECT: %@", [object class]);
    }
    
      [delegate successResponseforProfile:mainDict];
    }
}
-(void)requestFinished:(ASIHTTPRequest *)request
{
    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    if (networkStatus == NotReachable)
    {
        [delegate internetConnection];
        ////NSLog(@"There IS NO internet connection");
    }
    else
    {

    NSError *error = [request error];
    
    if(!error)
    {
        NSString *response  = [request responseString];
        
        NSDictionary *dict = [response JSONValue];
        
        NSLog(@"profile data ***** %@",dict);
        
        [delegate updateprofilesuccessprofile:dict];
    }
    }
}
///This code is for display the notificationsettings
-(void)getnotificationsettings:(NSString*)userid
{
    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    if (networkStatus == NotReachable)
    {
        [delegate internetConnection];
        ////NSLog(@"There IS NO internet connection");
    }
    else
    {

    
    NSString *urlstr = [NSString stringWithFormat:@"%@NotificationType?UserId=%@&format=json",MainUrl,userid];
    
    NSURL *url = [NSURL URLWithString:urlstr];
    
    NSLog(@"getnotificationsettings url:%@",url);
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:3600.0];
    
    [request setHTTPMethod:@"GET"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    
    [NSURLConnection sendAsynchronousRequest:request queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         if (error)
         {
             [delegate errorMessage:@"message"];
         }
         else
         {
             NSError *jsonParsingError = nil;
             
             NSMutableDictionary *mainDict = [NSJSONSerialization JSONObjectWithData:data options:0 error:&jsonParsingError];
             
             if (jsonParsingError)
             {
                 //  NSLog(@"JSON ERROR: %@", [jsonParsingError localizedDescription]);
                 [delegate errorMessage:@"message"];
             }
             else
             {
                 // NSLog(@"Users mainDict is %@",mainDict);
                 [delegate successResponseforNotification:mainDict];
             }
         }
     }];
    }
}

-(void)getbrandsettings:(NSString*)userid
{
    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    if (networkStatus == NotReachable)
    {
        [delegate internetConnection];
        ////NSLog(@"There IS NO internet connection");
    }
    else
    {
      NSString *urlstr = [NSString stringWithFormat:@"%@ListBrands?UserId=%@&format=json",MainUrl,userid];
    
    NSURL *url = [NSURL URLWithString:urlstr];
    
    NSLog(@" getbrandsettings url %@",url);
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:3600.0];
    
    [request setHTTPMethod:@"GET"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    
    [NSURLConnection sendAsynchronousRequest:request queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         if (error)
         {
             [delegate errorMessage:@"message"];
         }
         else
         {
             NSError *jsonParsingError = nil;
             
             NSMutableArray *responsearray=[NSJSONSerialization JSONObjectWithData:data options:0 error:&jsonParsingError];
             
             if (jsonParsingError)
             {
                 //  NSLog(@"JSON ERROR: %@", [jsonParsingError localizedDescription]);
                 [delegate errorMessage:@"message"];
             }
             else
             {
                 // NSLog(@"Users mainDict is %@",mainDict);
                // [delegate successResponseforNotification:mainDict];
                 
                 NSLog(@"responsearray...%@",responsearray);
                 [delegate successResponsegetbrandsettings:responsearray];
                 
             }
         }
     }];

    }
}
//This code is for changed in notification settings
-(void)notificationsettingschanges:(NSString*)loginid:(NSString *)mainkey:(NSString*)statuskey
{
    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    if (networkStatus == NotReachable)
    {
        [delegate internetConnection];
        ////NSLog(@"There IS NO internet connection");
    }
    else
    {

    NSError *jsonSerializationError = nil;
    // NSLog(@"----------> hash tags array is %@",HashTags);
    NSLog(@"the main stris %@",mainkey);
    NSLog(@"the statuskey stris %@",statuskey);
    
    NSMutableDictionary *postDict=[[NSMutableDictionary alloc]init];
    
    [postDict setValue:loginid forKey:@"UserId"];
    [postDict setValue:statuskey forKey:mainkey];
    [postDict setValue:mainkey forKey:@"Key"];
    
    NSLog(@" the post dictionary is %@",postDict);
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:postDict options:NSJSONWritingPrettyPrinted error:&jsonSerializationError];
    
    if(!jsonSerializationError)
    {
        NSString *serJSON = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        NSLog(@"Serialized JSON: %@", serJSON);
    }
    else
    {
    }
    
    NSString *urlstr;
    
    urlstr = [NSString stringWithFormat:@"%@NotificationType?format=json",MainUrl];
    NSLog(@"notificationsettingschanges url %@",urlstr);
    
    //  urlstr = [NSString stringWithFormat:@"%@Post?format=json",MainUrl];
    
    NSURL *url = [NSURL URLWithString:urlstr];
    
    NSLog(@"the url is %@",url);
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:3600.0];
    
    [request setHTTPMethod:@"POST"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:jsonData];
    
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    
    [NSURLConnection sendAsynchronousRequest:request queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         if (error)
         {
             [delegate errorMessage:@"message"];
         }
         else
         {
             NSError *jsonParsingError = nil;
             
             NSDictionary *mainDict = [NSJSONSerialization JSONObjectWithData:data options:0 error:&jsonParsingError];
             
             if (jsonParsingError)
             {
                 //  NSLog(@"JSON ERROR: %@", [jsonParsingError localizedDescription]);
                 [delegate errorMessage:@"message"];
             }
             else
             {
                 NSLog(@"mainDict is %@",mainDict);
                 
             }
             
         }
         
     }];
    }
}

-(void)requestFailed:(ASIHTTPRequest *)request
{
    NSLog(@" ***** request failed ******* %@",[request error]);
}
#pragma mark Sizes Get and Update Services
-(void)getSizesListByUserid:(NSString*)userid
{
    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    if (networkStatus == NotReachable)
    {
        [delegate internetConnection];
        ////NSLog(@"There IS NO internet connection");
    }
    else
    {

    
    NSString *urlstr;
    //urlstr = [NSString stringWithFormat:@"%@Account?UserId=%@&LoginUserId=%@&format=json",MainUrl,userid,myappDelegate.useridstring];
    urlstr = [NSString stringWithFormat:@"%@ListSizes?UserId=%@&format=json",MainUrl,userid];
    
    NSURL *url = [NSURL URLWithString:urlstr];
    
    NSLog(@"*** appdelegate.useridstring *** %@",myappDelegate.useridstring);
    NSLog(@"getSizesListByUserid url %@",url);
    
    NSLog(@"@@@@@@@@ URL at users list is:%@",urlstr);
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:3600.0];
    
    [request setHTTPMethod:@"GET"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    //urlconnection = [[NSURLConnection alloc] initWithRequest:request delegate:nil];
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    
    [NSURLConnection sendAsynchronousRequest:request queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         if (error)
         {
             [delegate errorMessage:@"message"];
         }
         else
         {
             NSError *jsonParsingError = nil;
             
             NSMutableArray *mainArray=[NSJSONSerialization JSONObjectWithData:data options:0 error:&jsonParsingError];
             if (jsonParsingError)
             {
                 //  NSLog(@"JSON ERROR: %@", [jsonParsingError localizedDescription]);
                 [delegate errorMessage:@"message"];
             }
             else
             {
                 // NSLog(@"Users mainDict is %@",mainDict);
                 
                 [delegate successResponseforArray:mainArray];
                 
             }
         }
     }];
    }
    
}
-(void)postSizesListByUserid:(NSString*)userid withSelectSizesString:(NSString *)sizesString
{
    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    if (networkStatus == NotReachable)
    {
        [delegate internetConnection];
        ////NSLog(@"There IS NO internet connection");
    }
    else
    {

    
    NSString *urlstr;
    //urlstr = [NSString stringWithFormat:@"%@Account?UserId=%@&LoginUserId=%@&format=json",MainUrl,userid,myappDelegate.useridstring];
    urlstr = [NSString stringWithFormat:@"%@MySizes?format=json",MainUrl];
    
    NSURL *url = [NSURL URLWithString:urlstr];
    
    NSLog(@"*** appdelegate.useridstring *** %@",myappDelegate.useridstring);
    NSLog(@"*** userId *** %@",userid);
    
    NSLog(@"postSizesListByUserid url:%@",urlstr);
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:3600.0];
    
    [request setHTTPMethod:@"POST"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    NSMutableDictionary *dict=[[NSMutableDictionary alloc]init];
    [dict setObject:userid forKey:@"UserId"];
    [dict setObject:sizesString forKey:@"Sizes"];
    NSLog(@"DICT IS %@",dict);
    NSError *jsonSerializationError = nil;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dict options:NSJSONWritingPrettyPrinted error:&jsonSerializationError];
    [request setHTTPBody:jsonData];
    
    //urlconnection = [[NSURLConnection alloc] initWithRequest:request delegate:nil];
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    
    [NSURLConnection sendAsynchronousRequest:request queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         if (error)
         {
             [delegate errorMessage:@"message"];
         }
         else
         {
             NSError *jsonParsingError = nil;
             
             NSMutableDictionary *mianDict=[NSJSONSerialization JSONObjectWithData:data options:0 error:&jsonParsingError];
             if (jsonParsingError)
             {
                 //  NSLog(@"JSON ERROR: %@", [jsonParsingError localizedDescription]);
                 [delegate errorMessage:@"message"];
             }
             else
             {
                 // NSLog(@"Users mainDict is %@",mainDict);
                 
                 [delegate successResponseforDictionary:mianDict];
                 
             }
         }
     }];
    
    }
}
-(void)getSuggestionFriendsListByUserid:(NSString*)userid
{
    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    if (networkStatus == NotReachable)
    {
        [delegate internetConnection];
        ////NSLog(@"There IS NO internet connection");
    }
    else
    {
    NSString *urlstr;
    //urlstr = [NSString stringWithFormat:@"%@Account?UserId=%@&LoginUserId=%@&format=json",MainUrl,userid,myappDelegate.useridstring];
    urlstr = [NSString stringWithFormat:@"%@Account/Suggestions?UserId=%@&format=json",MainUrl,userid];
    
    NSURL *url = [NSURL URLWithString:urlstr];
    
    NSLog(@"*** appdelegate.useridstring *** %@",myappDelegate.useridstring);
    NSLog(@"*** userId *** %@",userid);
    
    NSLog(@"getSuggestionFriendsListByUserid url: %@",urlstr);
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:3600.0];
    
    [request setHTTPMethod:@"GET"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    //urlconnection = [[NSURLConnection alloc] initWithRequest:request delegate:nil];
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    
    [NSURLConnection sendAsynchronousRequest:request queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         if (error)
         {
             [delegate errorMessage:@"message"];
         }
         else
         {
             NSError *jsonParsingError = nil;
             
             NSMutableDictionary *mainDict=[NSJSONSerialization JSONObjectWithData:data options:0 error:&jsonParsingError];
             if (jsonParsingError)
             {
                 //  NSLog(@"JSON ERROR: %@", [jsonParsingError localizedDescription]);
                 [delegate errorMessage:@"message"];
             }
             else
             {
                 // NSLog(@"Users mainDict is %@",mainDict);
                 [delegate successResponseforDictionary:mainDict];
             }
         }
     }];
    }
    
}
-(void)followUserswithDictionary:(NSMutableDictionary*)dict
{
    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    if (networkStatus == NotReachable)
    {
        [delegate internetConnection];
        ////NSLog(@"There IS NO internet connection");
    }
    else
    {

    //  myappDelegate=(AppDelegate *)[[UIApplication sharedApplication]delegate];
    NSLog(@"**** dict ***** %@",dict);
    NSError *jsonSerializationError = nil;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dict options:NSJSONWritingPrettyPrinted error:&jsonSerializationError];
    
    if(!jsonSerializationError)
    {
        NSString *serJSON = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        NSLog(@"Serialized JSON: %@", serJSON);
    }
    else
    {
        NSLog(@"JSON Encoding Failed: %@", [jsonSerializationError localizedDescription]);
    }
    
    //http://wonup.testshell.net/api/Register/Signup?format=json  [POST]
    
    
    NSString *urlstr = [NSString stringWithFormat:@"%@MyFeed/Follow?format=json",MainUrl];
    
    NSURL *url = [NSURL URLWithString:urlstr];
    
    NSLog(@"followUserswithDictionary url ----%@",url);
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:6000.0];
    
    [request setHTTPMethod:@"POST"];
    
    
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:jsonData];
    
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    
    [NSURLConnection sendAsynchronousRequest:request queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         if (error)
         {
             //[delegate errorMessage:@"message"];
         }
         else
         {
             NSError *jsonParsingError = nil;
             
             NSDictionary *mainDict = [NSJSONSerialization JSONObjectWithData:data options:0 error:&jsonParsingError];
             
             NSLog(@"**** mainDict **** %@",mainDict);
             
             if (jsonParsingError)
             {
                 NSLog(@"JSON ERROR: %@", [jsonParsingError localizedDescription]);
                 //[delegate errorMessage:@"message"];
             }
             else
             {
                 //  [delegate successResponseforPaypal:mainDict];
                 
                 double delayInSeconds =0.02;
                 
                 dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
                 dispatch_after(popTime, dispatch_get_main_queue(), ^(void)
                                {
                                    NSLog(@"check main dict %@",mainDict);
                                    // [delegate successResponseforRegistration:mainDict];
                                    // [myappDelegate sucessresponceforFollowandunfollow:mainDict];
                                });
                 
                 
             }
             
         }
         
     }];
    }
}
-(void)markassold:(NSString *)objectid
{
    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    if (networkStatus == NotReachable)
    {
        [delegate internetConnection];
        ////NSLog(@"There IS NO internet connection");
    }
    else
    {

    //  myappDelegate=(AppDelegate *)[[UIApplication sharedApplication]delegate];
    NSString *idstr=[NSString stringWithFormat:@"%@",objectid];

    NSMutableDictionary *dic=[[NSMutableDictionary alloc]init];
    [dic setObject:idstr forKey:@"UserListingId"];
    [dic setObject:@"false" forKey:@"Forsale"];
    NSError *jsonSerializationError = nil;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dic options:NSJSONWritingPrettyPrinted error:&jsonSerializationError];
    NSLog(@"the dic is %@",dic);
    
    
    if(!jsonSerializationError)
    {
        NSString *serJSON = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        NSLog(@"Serialized JSON: %@", serJSON);
    }
    else
    {
        NSLog(@"JSON Encoding Failed: %@", [jsonSerializationError localizedDescription]);
    }
    
    //http://wonup.testshell.net/api/Register/Signup?format=json  [POST]
    // http://markidplace.testshell.net/api/DeleteAccount?format=json
    
    NSString *urlstr = [NSString stringWithFormat:@"%@UserListing/UpdateForSale?format=json",MainUrl];
    
    NSURL *url = [NSURL URLWithString:urlstr];
    
    NSLog(@"markassold url ----%@",url);
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:6000.0];
    
    [request setHTTPMethod:@"PUT"];
    
    
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:jsonData];
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    
    [NSURLConnection sendAsynchronousRequest:request queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         NSLog(@"repsone %@",response);

         if (error)
         {
             [delegate errorMessage:@"message"];
         }
         else
         {
             NSError *jsonParsingError = nil;
             
             NSDictionary *mainDict = [NSJSONSerialization JSONObjectWithData:data options:0 error:&jsonParsingError];
             
             NSLog(@"**** mainDict **** %@",mainDict);
             NSLog(@"**** mainDict **** %@",[NSJSONSerialization JSONObjectWithData:data options:0 error:&jsonParsingError]);

             if (jsonParsingError)
             {
                 NSLog(@"JSON ERROR: %@", [jsonParsingError localizedDescription]);
                 [delegate errorMessage:@"message"];
             }
             else
             {
                 //  [delegate successResponseforPaypal:mainDict];
                 if ([mainDict objectForKey:@"message"])
                 {
                     
                     [delegate successResponseforsold:mainDict];
                 }
                 else
                     [delegate errorMessage:@"message"];
             }
             
         }
         
     }];

    }
   }
-(void)messageRead:(NSString*)useridfrm:(NSString*)msguserid
{
    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    if (networkStatus == NotReachable)
    {
        [delegate internetConnection];
        ////NSLog(@"There IS NO internet connection");
    }
    else
    {
        
        //  myappDelegate=(AppDelegate *)[[UIApplication sharedApplication]delegate];
        //NSString *useridstr=[NSString stringWithFormat:@"%@",userid];
        
        NSMutableDictionary *dic=[[NSMutableDictionary alloc]init];
        [dic setObject:msguserid forKey:@"UserId"];
        [dic setObject:useridfrm forKey:@"MessengerUserId"];
        NSError *jsonSerializationError = nil;
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dic options:NSJSONWritingPrettyPrinted error:&jsonSerializationError];
        NSLog(@"the dic is %@",dic);
        
        
        if(!jsonSerializationError)
        {
            NSString *serJSON = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
            NSLog(@"Serialized JSON: %@", serJSON);
        }
        else
        {
            NSLog(@"JSON Encoding Failed: %@", [jsonSerializationError localizedDescription]);
        }
        
        //http://wonup.testshell.net/api/Register/Signup?format=json  [POST]
        // http://markidplace.testshell.net/api/DeleteAccount?format=json
        
        NSString *urlstr = [NSString stringWithFormat:@"%@UpdateNotification?format=json",MainUrl];
        
        NSURL *url = [NSURL URLWithString:urlstr];
        
        NSLog(@"messageRead url ----%@",url);
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:6000.0];
        
        [request setHTTPMethod:@"PUT"];
        
        
        [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        [request setHTTPBody:jsonData];
        NSOperationQueue *queue = [[NSOperationQueue alloc] init];
        
        [NSURLConnection sendAsynchronousRequest:request queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
         {
             NSLog(@"repsone %@",response);
             
             if (error)
             {
                 [delegate errorMessage:@"message"];
             }
             else
             {
                 NSError *jsonParsingError = nil;
                 
                 NSDictionary *mainDict = [NSJSONSerialization JSONObjectWithData:data options:0 error:&jsonParsingError];
                 
                 NSLog(@"**** mainDict **** %@",mainDict);
                 NSLog(@"**** mainDict **** %@",[NSJSONSerialization JSONObjectWithData:data options:0 error:&jsonParsingError]);
                 
                 if (jsonParsingError)
                 {
                     NSLog(@"JSON ERROR: %@", [jsonParsingError localizedDescription]);
                     [delegate errorMessage:@"message"];
                 }
                 else
                 {
                     //  [delegate successResponseforPaypal:mainDict];
                     if ([mainDict objectForKey:@"message"])
                     {
                         
                        // [delegate successResponseforsold:mainDict];
                         [delegate successResponseformessageRead:mainDict];
                     }
                     else
                         [delegate errorMessage:@"message"];
                 }
                 
             }
             
         }];
        
    }
}
-(void)getsizelist:(NSString *)categoryid
{
    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    if (networkStatus == NotReachable)
    {
        [delegate internetConnection];
        ////NSLog(@"There IS NO internet connection");
    }
    else
    {
        
        NSString *urlstr;
        //urlstr = [NSString stringWithFormat:@"%@Account?UserId=%@&LoginUserId=%@&format=json",MainUrl,userid,myappDelegate.useridstring];
        urlstr = [NSString stringWithFormat:@"%@Account/AllSizes?CategoryId=%@&format=json",MainUrl,categoryid];
        
        NSURL *url = [NSURL URLWithString:urlstr];
        
     
        NSLog(@"*** userId *** %@",categoryid);
        
        NSLog(@"getsizelist url %@",urlstr);
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:3600.0];
        
        [request setHTTPMethod:@"GET"];
        [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        
        //urlconnection = [[NSURLConnection alloc] initWithRequest:request delegate:nil];
        NSOperationQueue *queue = [[NSOperationQueue alloc] init];
        
        [NSURLConnection sendAsynchronousRequest:request queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
         {
             if (error)
             {
                 [delegate errorMessage:@"message"];
             }
             else
             {
                 NSError *jsonParsingError = nil;
                 
                 NSMutableArray *mainArray=[NSJSONSerialization JSONObjectWithData:data options:0 error:&jsonParsingError];
                 if (jsonParsingError)
                 {
                     //  NSLog(@"JSON ERROR: %@", [jsonParsingError localizedDescription]);
                     [delegate errorMessage:@"message"];
                 }
                 else
                 {
                      NSLog(@"Users mainDict is %@",mainArray);
                     
                     [delegate successResponseforsizelist:mainArray];
                     
                 }
             }
         }];
    }
}
-(void)addressVerification:(NSMutableDictionary*)addressdict
{
    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    if (networkStatus == NotReachable)
    {
        [delegate internetConnection];
        ////NSLog(@"There IS NO internet connection");
    }
    else
    {
        NSError *jsonSerializationError = nil;
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:addressdict options:NSJSONWritingPrettyPrinted error:&jsonSerializationError];
        
        
        if(!jsonSerializationError)
        {
            NSString *serJSON = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
            NSLog(@"Serialized JSON: %@", serJSON);
        }
        else
        {
            NSLog(@"JSON Encoding Failed: %@", [jsonSerializationError localizedDescription]);
        }
        
        
        NSString *urlstr = [NSString stringWithFormat:@"%@Account/AddressValidation?format=json",MainUrl];
        
        NSURL *url = [NSURL URLWithString:urlstr];
        
        NSLog(@"addressVerification url ----%@",url);
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:6000.0];
        
        [request setHTTPMethod:@"POST"];
        
        
        [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        [request setHTTPBody:jsonData];
        
        NSOperationQueue *queue = [[NSOperationQueue alloc] init];
        
        [NSURLConnection sendAsynchronousRequest:request queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
         {
             if (error)
             {
                 [delegate errorMessage:@"message"];
             }
             else
             {
                 NSError *jsonParsingError = nil;
                 
                 NSMutableDictionary *mainDict = [NSJSONSerialization JSONObjectWithData:data options:0 error:&jsonParsingError];
                 
                 NSLog(@"**** mainDict **** %@",mainDict);
                 
                 if(![mainDict isKindOfClass:[NSNull class]])
                 {
                 if (jsonParsingError)
                 {
                     NSLog(@"JSON ERROR: %@", [jsonParsingError localizedDescription]);
                     [delegate errorMessage:@"message"];
                 }
                 else
                 {
                     //  [delegate successResponseforPaypal:mainDict];
                     if ([mainDict objectForKey:@"message"])
                     {
                         
                   [delegate successResponseforaddressVerification:mainDict];
                     }
                     else
                         [delegate errorMessage:@"message"];
                 }
                 }
                 
                 
             }
             
         }];
    }
}
-(void)emailupdate:(NSMutableDictionary*)addressdict
{
    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    if (networkStatus == NotReachable)
    {
        [delegate internetConnection];
        ////NSLog(@"There IS NO internet connection");
    }
    else
    {
        NSError *jsonSerializationError = nil;
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:addressdict options:NSJSONWritingPrettyPrinted error:&jsonSerializationError];
        
        
        if(!jsonSerializationError)
        {
            NSString *serJSON = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
            NSLog(@"Serialized JSON: %@", serJSON);
        }
        else
        {
            NSLog(@"JSON Encoding Failed: %@", [jsonSerializationError localizedDescription]);
        }
        
        
        NSString *urlstr = [NSString stringWithFormat:@"%@Account/UpdateEmail?format=json",MainUrl];
        
        NSURL *url = [NSURL URLWithString:urlstr];
        
        NSLog(@"emailupdate url ----%@",url);
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:6000.0];
        
        [request setHTTPMethod:@"POST"];
        
        
        [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        [request setHTTPBody:jsonData];
        
        NSOperationQueue *queue = [[NSOperationQueue alloc] init];
        
        [NSURLConnection sendAsynchronousRequest:request queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
         {
             if (error)
             {
                 [delegate errorMessage:@"message"];
             }
             else
             {
                 NSError *jsonParsingError = nil;
                 
                 NSMutableDictionary *mainDict = [NSJSONSerialization JSONObjectWithData:data options:0 error:&jsonParsingError];
                 
                 NSLog(@"**** mainDict **** %@",mainDict);
                 
                 if (jsonParsingError)
                 {
                     NSLog(@"JSON ERROR: %@", [jsonParsingError localizedDescription]);
                     [delegate errorMessage:@"message"];
                 }
                 else
                 {
                     //  [delegate successResponseforPaypal:mainDict];
                     if ([mainDict objectForKey:@"message"])
                     {
                         
                         [delegate successResponseforupdateemail:mainDict];
                     }
                     else
                         [delegate errorMessage:@"message"];
                 }
                 
             }
             
         }];
    }
}

@end
