//
//  CartViewCell.m
//  MarkidPlace
//
//  Created by stellent_mac_05 on 10/06/14.
//  Copyright (c) 2014 stellentmac1. All rights reserved.
//

#import "CartViewCell.h"

@implementation CartViewCell
@synthesize cancelbtn,checkoutbtn,orderfromlbl,usernamelbl,itemcostlbl,itemdetailslbl,itemimgview,itemnamelbl,sizelbl,sizennumlbl,purchasecostlbl,purchaselbl,chargeslbl,shipingoptionlbl,alternateadresslbl,switchImageView,textviewimageview;
@synthesize addresstextView;

@synthesize add1Tf;
@synthesize add2Tf;
@synthesize cityTf;
@synthesize stateTf;
@synthesize zipcodeTf,shippingcostlbl,shippingcostpricelbl;
- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
