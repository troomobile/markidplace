//
//  FollowViewController.m
//  MarkidPlace
//
//  Created by stellent on 22/05/14.
//  Copyright (c) 2014 stellentmac1. All rights reserved.
//

#import "FollowViewController.h"
#import "SuggestionsViewController.h"
#import "ListingDetailsCustomcell.h"
#import "Users.h"
#import "UserLists.h"
#import "Item.h"

@interface FollowViewController ()

@end

@implementation FollowViewController
{
    NSMutableArray *tempArray;
}
@synthesize usernameLabel,nameLabel,starLabel,itemsLabel,likesLabel,followersLabel,followingLabel,itemsCountLabel,likesCountLabel,followersCountLabel,followingCountLabel,listdataarray,useridstr,nameString,usernameString,imageString,compareStr,contactStr,userId;

@synthesize itemPrice1Label,itemPrice2Label,itemPrice3Label,itemPrice4Label,itemPrice5Label,itemTitle1Label,itemTitle2Label,itemTitle3Label,itemTitle4Label,itemTitle5Label,itemPrice6Label,itemTitle6Label,followscrlview,menuview,menuBtn,notifystr;

@synthesize dataArray,profRepo,delegate,imFronVC;

@synthesize sltobj,followbtn,adressLabel;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

#pragma mark View Methods

- (void)viewDidLoad
{
    [super viewDidLoad];
    NSLog(@"%@",listdataarray);
    tempArray=[[NSMutableArray alloc]initWithArray:listdataarray];
     NSLog(@"%@",tempArray);
    // Do any additional setup after loading the view from its nib.
    //  myappDelegate= (AppDelegate *)[[UIApplication sharedApplication]delegate];
    screenSize = [[UIScreen mainScreen]bounds];
    dataArray=[NSMutableArray new];
    


    profRepo=[[ProfileRepository alloc] init];
    profRepo.delegate=self;
    registrationRep=[[RegistrationRepository alloc ]init];
    registrationRep.delegate=self;
    [self followMethod];
   }

-(void)viewWillAppear:(BOOL)animated
{
    [myappDelegate startspinner:self.view];
    NSLog(@"check the listing object %@ and user idstr %@ ",sltobj.userlistingidstr,sltobj.useridstr);
    
    if([compareStr isEqualToString:@"compare"])
    {
    
        nameLabel.text = nameString;
        usernameLabel.text = usernameString;
        NSURL *picurl=[NSURL URLWithString:imageString];
        [userimageview setImageWithURL:picurl             placeholderImage:[UIImage imageNamed:@"noimage_friend.png"] options:SDWebImageCacheMemoryOnly];
    }
    
    else
    {
    usernameLabel.text=sltobj.usernamestring;
    nameLabel.text=sltobj.namestr;
   NSURL *picurl=[NSURL URLWithString:sltobj.profilepicstr];
   [userimageview setImageWithURL:picurl     placeholderImage:[UIImage imageNamed:@"noimage_friend.png"] options:SDWebImageCacheMemoryOnly];
    }
    
    if([notifystr isEqualToString:@"Notification"])
    {
       // notifystr=@"";
        [profRepo getProfileData:useridstr];

    }
    
    else if([contactStr isEqualToString:@"contacts"])
    {

        nameLabel.text = nameString;
        usernameLabel.text = usernameString;
        NSURL *picurl=[NSURL URLWithString:imageString];
        [userimageview setImageWithURL:picurl             placeholderImage:[UIImage imageNamed:@"noimage_friend.png"] options:SDWebImageCacheMemoryOnly];
        
         [profRepo getProfileData:userId];
                
    }
    
    else //if([APPDELEGATE isnullorempty: notifystr])
    {
        [profRepo getProfileData:sltobj.useridstr];

    }
    // [profRepo getProfileData:sltobj.useridstr];
    [myappDelegate likeandunlikestatus:myappDelegate.suggestonsGlobalarray  :myappDelegate.allLikesGlobalarray ];
    [myappDelegate likeandunlikestatus:myappDelegate.myfeedsGlobalarray  :myappDelegate.allLikesGlobalarray ];
    [myappDelegate likeandunlikestatus:myappDelegate.myclosetGlobalarray  :myappDelegate.allLikesGlobalarray ];
    [myappDelegate likeandunlikestatus:myappDelegate.mylikesGlobalarray  :myappDelegate.allLikesGlobalarray ];
}

// Applying the fonts to the labels in this method
-(void)followMethod
{
    userimageview.layer.cornerRadius=userimageview.frame.size.width/2;
    userimageview.layer.masksToBounds=YES;
    
    self.nameLabel.font = [UIFont fontWithName:@"Monroe" size:16];
    
    // self.usernameLabel.font = [UIFont fontWithName:@"Muli" size:16];
    self.adressLabel.font = [UIFont fontWithName:@"Muli" size:11];
    self.itemsLabel.font = [UIFont fontWithName:@"Muli" size:12];
    self.likesLabel.font = [UIFont fontWithName:@"Muli" size:12];
    self.followersLabel.font = [UIFont fontWithName:@"Muli" size:12];
    self.followingLabel.font = [UIFont fontWithName:@"Muli" size:12];
    self.itemsCountLabel.font = [UIFont fontWithName:@"Muli" size:12];
    self.likesCountLabel.font = [UIFont fontWithName:@"Muli" size:12];
    self.followingCountLabel.font = [UIFont fontWithName:@"Muli" size:12];
    self.followersCountLabel.font = [UIFont fontWithName:@"Muli" size:12];
    self.starLabel.font = [UIFont fontWithName:@"Muli" size:12];
    /////////////////////
    self.itemTitle1Label.font = [UIFont fontWithName:@"Muli" size:13];
    self.itemTitle2Label.font = [UIFont fontWithName:@"Muli" size:13];
    self.itemTitle3Label.font = [UIFont fontWithName:@"Muli" size:13];
    self.itemTitle4Label.font = [UIFont fontWithName:@"Muli" size:13];
    self.itemTitle5Label.font = [UIFont fontWithName:@"Muli" size:13];
    self.itemTitle6Label.font = [UIFont fontWithName:@"Muli" size:13];
    
    self.itemPrice1Label.font = [UIFont fontWithName:@"Muli" size:11];
    self.itemPrice2Label.font = [UIFont fontWithName:@"Muli" size:11];
    self.itemPrice3Label.font = [UIFont fontWithName:@"Muli" size:11];
    self.itemPrice4Label.font = [UIFont fontWithName:@"Muli" size:11];
    self.itemPrice5Label.font = [UIFont fontWithName:@"Muli" size:11];
    self.itemPrice6Label.font = [UIFont fontWithName:@"Muli" size:11];
    if (myappDelegate.isiphone5)
    {
        menuBtn.frame=CGRectMake(131,470, 58, 59);
    }
    else
    {
        menuBtn.frame=CGRectMake(131,355, 58, 59);
    }
    
    
    self.view.userInteractionEnabled=YES;
    
    //  [self.followscrlview setContentSize:CGSizeMake(320,680)];
    [self.view addSubview:menuview];
    [self.view addSubview:menuBtn];
    
    
    NSLog(@" user idstr item user id str= %@",sltobj.useridstr);

}

-(void)createscrollview
{
    
    for (UIView *selfv in followscrlview.subviews)
    {
        [selfv removeFromSuperview];
    }
    int x=15;
    int y=5;
    
    NSLog(@" dataArray count %d",dataArray.count);
    
   
    [myappDelegate createmyscrollview:followscrlview :userObj.userListingArray :x :y :self];
    
    [followscrlview setContentOffset:CGPointMake(0, 200) animated:NO];
    
    [followscrlview setContentOffset:CGPointMake(0, 0) animated:NO];

    /*
    Users *userObj=[dataArray objectAtIndex:0];
    NSLog(@"**** %@ **** " , userObj.userListingArray);
    for (int i=0; i< userObj.userListingArray.count; i++)
    {
        UserLists *userlistObj=[ userObj.userListingArray objectAtIndex:i];
        Item *itemObj;
        if (userlistObj.itemPicturesArray.count>0)
        {
            itemObj =[userlistObj.itemPicturesArray objectAtIndex:0];
        }
        
        
        UIView *mainview=[[UIView alloc]initWithFrame:CGRectMake(x, y, 138, 174)];
        mainview .backgroundColor=[UIColor clearColor];
        mainview.tag=i;
        
        UIImageView *border=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, mainview.frame.size.width, mainview.frame.size.height)];
        border.image=[UIImage imageNamed:@"Createlistinglayer.png"];
        
        [mainview addSubview:border];
        
        UIImageView *itemimgv=[[UIImageView alloc]initWithFrame:CGRectMake(1, 1, 136, 118)];
        
         NSLog(@"itemObj.pictureCdnUrlStr url %@",itemObj.pictureCdnUrlStr);
        
        if (userlistObj.itemPicturesArray.count<1)
        {
            itemimgv.image=[UIImage imageNamed:@"noimage_friend.png" ];
        }
        else{
            
            itemObj.pictureCdnUrlStr=[itemObj.pictureCdnUrlStr stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
            NSURL *picurl=[NSURL URLWithString:itemObj.pictureCdnUrlStr];
            NSLog(@"now th e picture url %@",picurl);
            [itemimgv setImageWithURL:picurl             placeholderImage:[UIImage imageNamed:@"noimage_friend.png"] options:SDWebImageCacheMemoryOnly];
            
        }
        
        [mainview addSubview:itemimgv];
        
        if ([myappDelegate isnullorempty:userlistObj.itemNameStr]) {
            userlistObj.itemNameStr=@"--";
        }
        
        UILabel *titlelbl=[[UILabel alloc]initWithFrame:CGRectMake(3, 125, 132, 30)];
        titlelbl.text=[NSString stringWithFormat:@"%@",userlistObj.itemNameStr];
        
        titlelbl.textAlignment=NSTextAlignmentCenter;
      titlelbl.textColor=[UIColor colorWithRed:(12/255.f) green:(174/255.f) blue:(180/255.f) alpha:1.0];
        titlelbl.font=[UIFont fontWithName:@"Muli" size:13];
        [mainview addSubview:titlelbl];
        
        
        UILabel *listpricelbl=[[UILabel alloc]initWithFrame:CGRectMake(0, 145, 70, 30)];
       // NSLog(@"chec %@",itemobj.salecostcoststr);
        listpricelbl.text=[NSString stringWithFormat:@"$%@",userlistObj.listPriceStr];
        listpricelbl.textAlignment=NSTextAlignmentRight;
        listpricelbl.textColor=
        [UIColor colorWithRed:(12/255.f) green:(174/255.f) blue:(180/255.f) alpha:1.0];
        listpricelbl.font=[UIFont fontWithName:@"Muli" size:11];
        [mainview addSubview:listpricelbl];
        //listpricelbl.backgroundColor=[UIColor redColor];
        
        UILabel *origlbl=[[UILabel alloc]initWithFrame:CGRectMake(71, 145, 68, 30)];
        
        origlbl.textAlignment=NSTextAlignmentLeft;
        
        origlbl.text=[NSString stringWithFormat:@"$%@",userlistObj.unitPriceStr];
        
        origlbl.textColor=[UIColor grayColor];
        origlbl.font=[UIFont fontWithName:@"Muli" size:11];
        
        NSDictionary* attributes = @{  NSStrikethroughStyleAttributeName: [NSNumber numberWithInt:NSUnderlineStyleSingle]   };
        
        NSAttributedString* attrText = [[NSAttributedString alloc] initWithString:origlbl.text attributes:attributes];
        origlbl.attributedText=attrText;
       [mainview addSubview:origlbl];
        
        
        UIButton *likebtn=[UIButton buttonWithType:UIButtonTypeCustom];
        
        [likebtn addTarget:self action:@selector(likebuttonaction:) forControlEvents:UIControlEventTouchUpInside];
        
        likebtn.tag=i;
        [mainview addSubview:likebtn];
        
        
        UIButton *listdetailbtn=[UIButton buttonWithType:UIButtonTypeCustom];
        listdetailbtn.frame=CGRectMake(0, 25, mainview.frame.size.width, mainview.frame.size.height-25);
        listdetailbtn.tag=i;
        [mainview addSubview:listdetailbtn];
        
        
        if ((i+1)%2 == 0)
        {
            y=y+200;
            x=15;
        }
        else
        {
            x=167;
        }
        
       [followscrlview addSubview:mainview];
    }
    
    [followscrlview setContentSize:CGSizeMake(320, y+250)];
     */
}

-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    [AppDelegate assaignitemimages:CGRectMake(0, scrollView.contentOffset.y,scrollView.frame.size.width , scrollView.frame.size.height) :userObj.userListingArray :followscrlview];
    
    NSLog(@"heck the content offset %@",NSStringFromCGPoint(scrollView.contentOffset));
    
}



// This Action Performs the make the status bar White.
- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

// Performs the follow button Action.
-(IBAction)followAction:(id)sender
{
     if (![[NSString stringWithFormat:@"%@",myappDelegate.useridstring]  isEqualToString:[NSString stringWithFormat:@"%@",userObj.userIdStr]] )
   // if (![myappDelegate.useridstring isEqualToString:sltobj.useridstr])
    {

   // [myappDelegate startspinner:self.view];
    NSMutableDictionary *dict=[[NSMutableDictionary alloc] init];
    [dict setValue:myappDelegate.useridstring forKey:@"UserId"];
        if([notifystr isEqualToString:@"Notification"])
        {
               [dict setValue:[NSString stringWithFormat:@"%@",useridstr] forKey:@"FollowerUserId"];
        }
        
        else if([contactStr isEqualToString:@"contacts"])
        {
            [dict setValue:[NSString stringWithFormat:@"%@",userId] forKey:@"FollowerUserId"];
        }
        
        else
        {
              [dict setValue:[NSString stringWithFormat:@"%@",sltobj.useridstr] forKey:@"FollowerUserId"];
        }
    // [dict setValue:[NSString stringWithFormat:@"%@",sltobj.useridstr] forKey:@"FollowerUserId"];
    
    [profRepo followOrUnfollow:dict];
    
    if ([userObj.isFollowStr isEqualToString:@"0"])
    {
        adressLabel.hidden=NO;
        followImageView.image=[UIImage imageNamed:@"Unfollow.png"];
        userObj.isFollowStr=@"1";
        
        int follcount=0;
        
        if (([followersCountLabel.text intValue] +1 ))
        {
            follcount=([followersCountLabel.text intValue] +1);
        }
        else
        {
            follcount=0;
        }
        followersCountLabel.text=[NSString stringWithFormat:@"%d",follcount];
        
    }
    else if ([userObj.isFollowStr  isEqualToString:@"1"])
    {
        int follcount=0;
        adressLabel.hidden=YES;
        if (([followersCountLabel.text intValue] -1 )>0)
        {
          //  follcount=([followingCountLabel.text intValue] -1);
            followersCountLabel.text=[NSString stringWithFormat:@"%d",[followersCountLabel.text intValue]-1];
        }
        else
        {
            follcount=0;
            followersCountLabel.text=[NSString stringWithFormat:@"%d",follcount];
        }
        
        
        followImageView.image=[UIImage imageNamed:@"Follow@2x.png"];
        userObj.isFollowStr=@"0";
    }
  
    
    [myappDelegate.myfeedsGlobalarray removeAllObjects];
   }
    else{
        [AppDelegate alertmethod:appname :@"You can not follow your self"];
    }
    
    
    
}

// navigation back button Action.
-(IBAction)backAction:(id)sender
{
    if ([imFronVC isEqualToString:@"ListingDetailViewController"])
    {
        for (UIViewController *vc in self.navigationController.viewControllers)
        {
            if ([vc isKindOfClass:[ListingDetailViewController class]])
            {
                ListingDetailViewController *lcVC=(ListingDetailViewController*)vc;
                NSLog(@"%@",lcVC.listingmainarray);
                if (lcVC.listingmainarray.count==0)
                {
                    lcVC.listingmainarray=tempArray;
                    
                }
                NSLog(@"%@",lcVC.listingmainarray);
                [self.navigationController popToViewController:lcVC animated:YES];
                return;
                
            }
            
        }
    }
    else if ([imFronVC isEqualToString:@"NotificationViewController"])
    {
        for (UIViewController *vc in self.navigationController.viewControllers)
        {
            if ([vc isKindOfClass:[NotificationViewController class]])
            {
                NotificationViewController *lcVC=(NotificationViewController*)vc;
               
                [self.navigationController popToViewController:lcVC animated:YES];
                return;
                
            }
            
        }
    }
    
    else if ([imFronVC isEqualToString:@"FindFriendsViewController"])
    {
        for (UIViewController *vc in self.navigationController.viewControllers)
        {
            if ([vc isKindOfClass:[FindFriendsViewController class]])
            {
                FindFriendsViewController *lcVC=(FindFriendsViewController*)vc;
                
                [self.navigationController popToViewController:lcVC animated:YES];
                return;
                
            }
            
        }
    }
    
    else
     [self.navigationController popViewControllerAnimated:YES];
}

//Action performs the Animation of the menu view.
-(IBAction)menuBtnAction:(id)sender
{
    
        if (myappDelegate.isiphone5)
        {
            if(ismenu==YES)
            {
                ismenu=NO;
                menuBtn.frame=CGRectMake(131,470, 58, 59);
                CGRect menu=menuview.frame;
                menu.origin.y=menuview.frame.origin.y+570;
                menuview.frame=menu;
                self.view.userInteractionEnabled=NO;
                
            }
            else
            {
                menuBtn.frame=CGRectMake(131, 245, 58, 59);
                ismenu=YES;
                
                CGRect menu=menuview.frame;
                menu.origin.y=280;
                menuview.frame=menu;
                
            }
            self.view.userInteractionEnabled=YES;
            
        }
        else
        {
            if(ismenu==YES)
            {
                
                ismenu=NO;
                
                menuBtn.frame=CGRectMake(131,350, 58, 59);
                CGRect menu=menuview.frame;
                menu.origin.y=menuview.frame.origin.y+482;
                menuview.frame=menu;
                self.view.userInteractionEnabled=NO;
                
            }
            else
            {
                menuBtn.frame=CGRectMake(131, 150, 58, 59);
                ismenu=YES;
                
                CGRect menu=menuview.frame;
                menu.origin.y=190;
                menuview.frame=menu;
                
                
            }
        }
        self.view.userInteractionEnabled=YES;
    
}

// Success Response from profile.
-(void)successResponseforProfile:(NSDictionary *)responseDict
{
    
    NSLog(@"For Profile Vc");
    NSLog(@"  ****  response ****  %@",responseDict);
    
    NSMutableDictionary *userdataDict=(NSMutableDictionary *)responseDict;
    
    userObj=[[Users alloc] init];
    
    userObj.userIdStr=[userdataDict objectForKey:@"userId"];
    userObj.userNameStr= [userdataDict objectForKey:@"userName"];
    userObj.nameStr= [userdataDict objectForKey:@"name"];
    usernamestr=[userdataDict objectForKey:@"name"];
    
    NSString *addrstr;
    NSString *citystr;
       if ([myappDelegate isnullorempty:[userdataDict objectForKey:@"address1"]])
    {
       //addrstr=@"--";
        addrstr=@"";
    }
       else{
           addrstr=[userdataDict objectForKey:@"address1"];
       }
    
    if ([myappDelegate isnullorempty:[userdataDict objectForKey:@"city"]])
    {
       // citystr=@"--";
        citystr=@"";
    }
    else
    {
        citystr=[userdataDict objectForKey:@"city"];
    }
    NSString *addrlbl=@"";
    if ([addrstr isEqualToString:@""] && [citystr isEqualToString:@""])
    {
        addrlbl=@"";
    }
    else
    {
        if (![addrstr isEqualToString:@""] && [citystr isEqualToString:@""])
        {
             addrlbl=[NSString stringWithFormat:@"%@",addrstr];
        }
        else if ([addrstr isEqualToString:@""] && ![citystr isEqualToString:@""])
        {
            addrlbl=[NSString stringWithFormat:@"%@",citystr];
        }
        else
          addrlbl=[NSString stringWithFormat:@"%@, %@",addrstr,citystr];
        
    }
    adressLabel.text=addrlbl;
    if ([myappDelegate isnullorempty:userObj.userNameStr])
    {
        userObj.userNameStr=@"- -";
        
    }
    
    if ([myappDelegate isnullorempty:userObj.nameStr])
    {
        userObj.nameStr=@"- -";
    }
    
    
    userObj.emailStr= [userdataDict objectForKey:@"email"];
    userObj.profilepicStr= [userdataDict objectForKey:@"profilepic"];
    
    
    NSURL *picurl=[NSURL URLWithString:userObj.profilepicStr];
     [userimageview setImageWithURL:picurl             placeholderImage:[UIImage imageNamed:@"noimage_friend.png"] options:SDWebImageCacheMemoryOnly];
    userObj.passwordStr= [userdataDict objectForKey:@"password"];
    userObj.createdDateStr= [userdataDict objectForKey:@"createdDate"];
    userObj.loginUserIdStr= [userdataDict objectForKey:@"loginUserId"];
    
    userObj.itemsCountStr= [userdataDict objectForKey:@"itemsCount"];
    
    if ([myappDelegate isnullorempty:userObj.itemsCountStr])
    {
        userObj.itemsCountStr=@"0";
    }
    itemsCountLabel.text=[NSString stringWithFormat:@"%@",userObj.itemsCountStr];
    userObj.wishListCountStr= [userdataDict objectForKey:@"wishListCount"];
    userObj.followerCountStr= [userdataDict objectForKey:@"followerCount"];
    
    if ([myappDelegate isnullorempty:userObj.followerCountStr])
    {
        userObj.followerCountStr=@"-";
    }
    followersCountLabel.text=[NSString stringWithFormat:@"%@",userObj.followerCountStr];
    
    userObj.followingCountStr= [userdataDict objectForKey:@"followingCount"];
    
    if ([myappDelegate isnullorempty:userObj.followingCountStr])
    {
        userObj.followingCountStr=@"-";
    }
    
    followingCountLabel.text=[NSString stringWithFormat:@"%@",userObj.followingCountStr];
    
    userObj.isFollowStr= [NSString stringWithFormat:@"%@",[userdataDict objectForKey:@"isFollow"]];
    
    if ([userObj.isFollowStr isEqualToString:@"0"])
    {
            followImageView.image=[UIImage imageNamed:@"Follow@2x.png"];
        
            adressLabel.hidden=YES;
        
    }
    else if ([userObj.isFollowStr isEqualToString:@"1"])
    {
              followImageView.image=[UIImage imageNamed:@"Unfollow.png"];
        if (addrlbl.length>2)
        {
            adressLabel.hidden=NO;
        }
        else
        {
            adressLabel.hidden=YES;
        }
    }
    
    
    userObj.userListingArray=[[NSMutableArray alloc] init];
    [userObj.userListingArray addObjectsFromArray:[myappDelegate parselistingitem:[userdataDict objectForKey:@"userListing"] :YES :myappDelegate.allLikesGlobalarray :NO]];
    
    [self createscrollview];
     
    

    // ismail edited
    
    /*
    userObj.userListingArray=[[NSMutableArray alloc] init];
    
    NSMutableArray *userArray=[userdataDict objectForKey:@"userListing"];
    for (int i=0;i<userArray.count;i++)
    {
        UserLists *listObj=[[UserLists alloc] init];
        listObj.userListingIdStr=[[userArray objectAtIndex:i] objectForKey:@"userListingId"];
        listObj.userItemIdStr=[[userArray objectAtIndex:i] objectForKey:@"userItemId"];
        listObj.categoryIdStr=[[userArray objectAtIndex:i] objectForKey:@"categoryId"];
        listObj.listPriceStr=[[userArray objectAtIndex:i] objectForKey:@"listPrice"];
        listObj.statusStr=[[userArray objectAtIndex:i] objectForKey:@"status"];
        listObj.inPersonStr=[[userArray objectAtIndex:i] objectForKey:@"inPerson"];
        listObj.uspsStr=[[userArray objectAtIndex:i] objectForKey:@"usps"];
        listObj.userIDStr=[[userArray objectAtIndex:i] objectForKey:@"userID"];
        listObj.unitPriceStr=[[userArray objectAtIndex:i] objectForKey:@"unitPrice"];
        listObj.categoryNameStr=[[userArray objectAtIndex:i] objectForKey:@"categoryName"];
        listObj.sizeIdStr=[[userArray objectAtIndex:i] objectForKey:@"sizeId"];
        listObj.sizeStr=[[userArray objectAtIndex:i] objectForKey:@"size"];
        listObj.brandIdStr=[[userArray objectAtIndex:i] objectForKey:@"brandId"];
        listObj.brandStr=[[userArray objectAtIndex:i] objectForKey:@"brand"];
        listObj.conditionIdStr=[[userArray objectAtIndex:i] objectForKey:@"conditionId"];
        listObj.conditionStr=[[userArray objectAtIndex:i] objectForKey:@"condition"];
        listObj.descriptionStr=[[userArray objectAtIndex:i] objectForKey:@"description"];
        listObj.itemNameStr=[[userArray objectAtIndex:i] objectForKey:@"itemName"];
        listObj.availableQuantityStr=[[userArray objectAtIndex:i] objectForKey:@"availableQuantity"];
        listObj.itemPicturesArray=[[NSMutableArray alloc] init];
        NSMutableArray *itemArray=[[userArray objectAtIndex:i] objectForKey:@"itemPictures"];
        for (int i=0;i<itemArray.count;i++)
        {
            Item *itemObj=[[Item alloc] init];
            
            itemObj.userItemPictureIdStr=[[itemArray objectAtIndex:i] objectForKey:@"userItemPictureId"];
            itemObj.userItemIdStr=[[itemArray objectAtIndex:i] objectForKey:@"userItemId"];
            itemObj.pictureCdnUrlStr=[[itemArray objectAtIndex:i] objectForKey:@"pictureCdnUrl"];
            itemObj.createdDateStr=[[itemArray objectAtIndex:i] objectForKey:@"createdDate"];
            
            [listObj.itemPicturesArray addObject:itemObj];
            
        }
        
        [userObj.userListingArray addObject:listObj];
       
    }
    */
     
     // ismail edited
   
  //
    
    
    userObj.usersWishArray=[NSMutableArray new];
    NSMutableArray *userWhistArray=[userdataDict objectForKey:@"userWishlist"];
    for (int i=0;i<userWhistArray.count;i++)
    {
        NSMutableArray *listArray=[[userWhistArray objectAtIndex:i] valueForKey:@"listUserListing"];
        for (int i=0;i<listArray.count;i++)
        {
            UserLists *listObj=[[UserLists alloc] init];
            listObj.userListingIdStr=[[listArray objectAtIndex:i] objectForKey:@"userListingId"];
            listObj.userItemIdStr=[[listArray objectAtIndex:i] objectForKey:@"userItemId"];
            listObj.categoryIdStr=[[listArray objectAtIndex:i] objectForKey:@"categoryId"];
            listObj.listPriceStr=[[listArray objectAtIndex:i] objectForKey:@"listPrice"];
            
            listObj.statusStr=[[listArray objectAtIndex:i] objectForKey:@"status"];
            listObj.inPersonStr=[[listArray objectAtIndex:i] objectForKey:@"inPerson"];
            listObj.uspsStr=[[listArray objectAtIndex:i] objectForKey:@"usps"];
            listObj.userIDStr=[[listArray objectAtIndex:i] objectForKey:@"userID"];
            listObj.unitPriceStr=[[listArray objectAtIndex:i] objectForKey:@"unitPrice"];
            listObj.categoryNameStr=[[listArray objectAtIndex:i] objectForKey:@"categoryName"];
            listObj.sizeIdStr=[[listArray objectAtIndex:i] objectForKey:@"sizeId"];
            listObj.sizeStr=[[listArray objectAtIndex:i] objectForKey:@"size"];
            listObj.brandIdStr=[[listArray objectAtIndex:i] objectForKey:@"brandId"];
            listObj.brandStr=[[listArray objectAtIndex:i] objectForKey:@"brand"];
            listObj.conditionIdStr=[[listArray objectAtIndex:i] objectForKey:@"conditionId"];
            listObj.conditionStr=[[listArray objectAtIndex:i] objectForKey:@"condition"];
            listObj.descriptionStr=[[listArray objectAtIndex:i] objectForKey:@"description"];
            listObj.itemNameStr=[[listArray objectAtIndex:i] objectForKey:@"itemName"];
            listObj.availableQuantityStr=[[listArray objectAtIndex:i] objectForKey:@"availableQuantity"];
            listObj.itemPicturesArray=[[NSMutableArray alloc] init];
            NSMutableArray *itemArray=[[listArray objectAtIndex:i] objectForKey:@"itemPictures"];
            for (int i=0;i<itemArray.count;i++)
            {
                Item *itemObj=[[Item alloc] init];
                
                itemObj.userItemPictureIdStr=[[itemArray objectAtIndex:i] objectForKey:@"userItemPictureId"];
                itemObj.userItemIdStr=[[itemArray objectAtIndex:i] objectForKey:@"userItemId"];
                itemObj.pictureCdnUrlStr=[[itemArray objectAtIndex:i] objectForKey:@"pictureCdnUrl"];
                itemObj.createdDateStr=[[itemArray objectAtIndex:i] objectForKey:@"createdDate"];
                
                [listObj.itemPicturesArray addObject:itemObj];
                
            }
            
            [userObj.usersWishArray addObject:listObj];
        }
    }
    [dataArray addObject:userObj];
    
    if ([myappDelegate isnullorempty:userObj.nameStr])
    {
        userObj.nameStr=@"-";
    }
    if ([myappDelegate isnullorempty:userObj.userNameStr])
    {
        userObj.userNameStr=@"-";
    }
    
    nameLabel.text=userObj.nameStr;
    usernameLabel.text=[NSString stringWithFormat:@"@%@",userObj.userNameStr];
    
    [self createscrollview];
    
      [myappDelegate stopspinner:self.view ];
    
}

// Success Response from Registration.
-(void)successResponseforRegistration:(NSDictionary *)responseDict
{
    NSLog(@"***** %@ *******",responseDict);
    NSString *followStr=[NSString stringWithFormat:@"%@", [responseDict  valueForKey:@"isFollow"]];
    if ([followStr isEqualToString:@"0"])
    {
           int follcount=0;
        
        if (([followingCountLabel.text intValue] -1 )>0)
        {
            follcount=([followingCountLabel.text intValue] -1);
        }
        else{
            follcount=0;
        }
            followImageView.image=[UIImage imageNamed:@"Follow@2x.png"];
    
    }
    else if ([followStr  isEqualToString:@"1"])
    {
          followImageView.image=[UIImage imageNamed:@"Unfollow.png"];
    }
    
    NSLog(@"check the listing object %@ ",sltobj.useridstr);
    [profRepo getProfileData:sltobj.useridstr];

    
}

-(void)errorMessage:(NSString *)message
{
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)messagebuttonaction:(id)sender
{
    ChartViewController *chart_VC;
    if (myappDelegate.isiphone5)
    {
        chart_VC=[[ChartViewController alloc]initWithNibName:@"ChartViewController" bundle:nil];
        
    }
    
    else
        
    {
        chart_VC=[[ChartViewController alloc]initWithNibName:@"ChartViewController_iphone4" bundle:nil];
    }
    
   
    //chart_VC.titlestr=sltobj.namestr;
    if([APPDELEGATE isnullorempty:sltobj.useridstr])
    {
        chart_VC.selleruseridstr=useridstr;
        chart_VC.titlestr=usernamestr;

    }
    else
    {
        chart_VC.selleruseridstr=sltobj.useridstr;
        chart_VC.titlestr=sltobj.namestr;


    }
    NSLog(@"chart_VC.selleruseridstr is  ----%@---",chart_VC.selleruseridstr);
   // chart_VC.selleruseridstr=sltobj.useridstr;
    chart_VC.profilepicstr=sltobj.profilepicstr;
    [self.navigationController pushViewController:chart_VC animated:YES];
    

}


-(IBAction)listDetailsAction:(id)sender
{
    
    ListingDetailViewController *listingDetailVC;
    if ([myappDelegate isiphone5])
    {
        listingDetailVC = [[ListingDetailViewController alloc]initWithNibName:@"ListingDetailViewController" bundle:nil];
    }
    else
    {
        listingDetailVC = [[ListingDetailViewController alloc]initWithNibName:@"ListingDetailViewController_iphone4" bundle:nil];
    }
    
    listingDetailVC.listingmainarray=userObj.userListingArray;
    
    
    listingDetailVC.senderIndex=[sender tag];
    listingDetailVC.condstring=@"";
    [self.navigationController pushViewController:listingDetailVC animated:YES];
}



-(IBAction)likebuttonaction:(id)sender
{
      // [myappDelegate startspinner:self.view];
    
    
    
        int    likeindex=[sender tag];
    
    
    ItemsModelobject *itemobj=[userObj.userListingArray objectAtIndex:[sender tag]];
    
    
    if (itemobj.likedbool==YES)
    {
        itemobj.likedbool=NO;
        
        [myappDelegate.allLikesGlobalarray removeObject:[NSString stringWithFormat:@"%@",itemobj.userlistingidstr ]];
        
    }
    else
    {
        [myappDelegate.allLikesGlobalarray addObject:[NSString stringWithFormat:@"%@",itemobj.userlistingidstr ]];
        itemobj.likedbool=YES;
    }
    
    
    // UIView *myv=[itemscrollview viewWithTag:[sender tag]];
    
    //NSLog(@"myv subviews %@ ",myv.subviews);
    BOOL breakbool=NO;
    for (int i=0; i<followscrlview.subviews.count; i++)
    {
        UIView *myv=[followscrlview.subviews objectAtIndex:i];
        
        if (myv.tag==likeindex)
        {
            if ([myv isKindOfClass:[UIView class]])
            {
                for (UIView *temv in myv.subviews)
                {
                    if ([temv isKindOfClass:[UIButton class]])
                    {
                        UIButton *likebtn=(UIButton *)temv;
                        if (likebtn.frame.size.width==23)
                        {
                            if (itemobj.likedbool==YES)
                            {
                                
                                
                                [likebtn setImage:[UIImage imageNamed:@"like_on.png"] forState:UIControlStateNormal];
                            }
                            else{
                                [likebtn setImage:[UIImage imageNamed:@"like_off.png"] forState:UIControlStateNormal];
                                
                                
                            }
                            
                            breakbool=YES;
                            break;
                        }
                    }
                }
                
                if (breakbool==YES)
                {
                    break;
                }
            }
            
            
        }
    }
    
    NSString *userid=[APPDELEGATE getStringFromPlist:@"userid"];
    
    NSDictionary *likedict=[[NSDictionary alloc]initWithObjectsAndKeys:userid,@"UserID",itemobj.userlistingidstr,@"UserListingID", nil];
    
   
    registrationRep.delegate=self;
    [registrationRep getdata:likedict :@"LikeItem" :@"POST" withcount:@"0"];
    
    [myappDelegate.mylikesGlobalarray removeAllObjects];
}

-(IBAction)getfollowersandfollwoing:(id)sender
{
    NSLog(@"user seller id %@",sltobj.useridstr);
    
        if ([[NSString stringWithFormat:@"%@",myappDelegate.useridstring]  isEqualToString:[NSString stringWithFormat:@"%@",userObj.userIdStr]] )
        {
            
            BOOL isVCExist=NO;
            NSArray *arr=self.navigationController.viewControllers;
            for (UIViewController *childvc in arr) {
                
                if([childvc isKindOfClass:[ProfileViewController class]])
                {
                    isVCExist=YES;
                    UIViewController *subvc=(ProfileViewController *)childvc;
                    [self.navigationController popToViewController:subvc animated:YES];
                    break;
                }
            }
            
            if(!isVCExist)
            {
                ProfileViewController *profileVC;
                if (myappDelegate.isiphone5
                    )
                {
                    profileVC = [[ProfileViewController alloc]initWithNibName:@"ProfileViewController" bundle:nil];
                }
                else
                {
                    profileVC = [[ProfileViewController alloc]initWithNibName:@"ProfileViewController_iphone4" bundle:nil];
                }
                
                [self.navigationController pushViewController:profileVC animated:YES];
            }
            
        }
        else
        {
            FollowersViewController *controller_follwer=[[FollowersViewController alloc]initWithNibName:@"FollowersViewController" bundle:nil];
            
            controller_follwer.issellerprofile=YES;
            if([notifystr isEqualToString:@"Notification"])
            {
                controller_follwer.selleruseridstr=useridstr;

            }
            
            else if([contactStr isEqualToString:@"contacts"])
            {
                controller_follwer.selleruseridstr=userId;
                
            }
            
            
            else //if([APPDELEGATE isnullorempty: notifystr])
            {
                controller_follwer.selleruseridstr=sltobj.useridstr;

            }
//            controller_follwer.selleruseridstr=sltobj.useridstr;
            
            
            
            if ([sender tag]==110)
            {
                if (![self.followersCountLabel.text isEqualToString:@"0"])
                {
                    
                    controller_follwer.isfollowers=YES;
                    
                    
                    [self.navigationController pushViewController:controller_follwer animated:YES];
                }
                
            }
            else{
                if (![self.followingCountLabel.text isEqualToString:@"0"])
                {
                    
                    controller_follwer.isfollowers=NO;
                    [self.navigationController pushViewController:controller_follwer animated:YES];
                }
            }

    }
    
   
    
}
-(void)internetConnection
{
    UIAlertView *Internetalert=[[UIAlertView alloc]initWithTitle:appname message:@"Internet connection not available" delegate:self cancelButtonTitle:nil otherButtonTitles:@"Ok", nil];
    [Internetalert show];
    [APPDELEGATE stopspinner:self.view];
    
}


@end
