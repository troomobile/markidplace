//
//  MySizesViewController.m
//  MarkidPlace
//
//  Created by stellent on 22/09/14.
//  Copyright (c) 2014 stellentmac1. All rights reserved.
//

#import "MySizesViewController.h"
#import "ProfileRepository.h"
#import "SizeModelObject.h"
#import "SuggestionsViewController.h"
@interface MySizesViewController ()
{
    ProfileRepository *profileRep;
}

@end

@implementation MySizesViewController
@synthesize navbar,titlelbl,mysizeslbl,myScrollView;
@synthesize updateButton;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
        //calling the repository
   
    profileRep=[[ProfileRepository alloc]init];
    profileRep.delegate=self;
    sizesMArray=[[NSMutableArray alloc]init];
      self.navbar.frame = CGRectMake(0, 0, 320, 64);
    [self.navbar setBackgroundImage:[UIImage imageNamed:@"navbar.png"] forBarMetrics:UIBarMetricsDefault];
    self.titlelbl.font = [UIFont fontWithName:@"Monroe" size:18];
    
    self.mysizeslbl.font = [UIFont fontWithName:@"Monroe" size:18];
    
}
-(void)viewWillAppear:(BOOL)animated
{
    
    updateButton.titleLabel.font=[UIFont fontWithName:@"Muli" size:16];
    updateButton.layer.cornerRadius=5;
    updateButton.layer.masksToBounds=YES;
    updateButton.layer.borderWidth=1;
    updateButton.layer.borderColor=[[UIColor colorWithRed:12.0/255.0 green:183.0/255.0 blue:174.0/255.0 alpha:1.0]CGColor];
    [updateButton setTitleColor:[UIColor colorWithRed:12.0/255.0 green:183.0/255.0 blue:174.0/255.0 alpha:1.0] forState:UIControlStateNormal];
    updateButton.hidden=YES;
    [self getSizesFromDB];
}
#pragma mark--Button Actions
//this action perfrorms navigation back action
-(IBAction)backbtnAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
//this action perfrorms pop the suggestion view controller

-(void)getSizesFromDB
{
    [APPDELEGATE startspinner:self.view];
    NSString *userid=[myappDelegate getStringFromPlist:@"userid"];
    [profileRep getSizesListByUserid:userid];
}
-(void)createScrollViewForSizes
{
    int xAxis=0;
    int yAxis=0;
   // myScrollView=nil;
    if ([APPDELEGATE isiphone5])
      myScrollView.frame=CGRectMake(20,100,280,395);
    else
        myScrollView.frame=CGRectMake(20,100,280,307);
    //For iphone5
    for (int i=0;i<sizesMArray.count;i++)
    {
        SizeModelObject *obj=[sizesMArray objectAtIndex:i];
        UIButton *sizesButton=[[UIButton alloc]initWithFrame:CGRectMake(xAxis,yAxis,75,27)];
        
        xAxis=xAxis+sizesButton.frame.size.width+27; // here is 20 gap between two rows.
        NSLog(@"xAxis....%d",xAxis);
        [sizesButton setTitle:obj.sizeName forState:UIControlStateNormal];
        
        
        if (xAxis>270)
        {
            xAxis=0;
            yAxis=yAxis+sizesButton.frame.size.height+10; //here 50 is gap between two columns.
            NSLog(@"yaxis....%d",yAxis);
            
        }
        
        if (obj.isSelect)
        {
            [sizesButton setBackgroundColor:[UIColor colorWithRed:12.0/255.0 green:183.0/255.0 blue:174.0/255.0 alpha:1.0]];
            [sizesButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            sizesButton.layer.borderWidth=1;
            sizesButton.layer.borderColor=[[UIColor colorWithRed:131.0/255.0 green:131.0/255.0 blue:131.0/255.0 alpha:1.0] CGColor];
            
        }
        else
        {
            [sizesButton setBackgroundColor:[UIColor whiteColor] ];
            [sizesButton setTitleColor:[UIColor colorWithRed:131.0/255.0 green:131.0/255.0 blue:131.0/255.0 alpha:1.0] forState:UIControlStateNormal];
            sizesButton.layer.borderWidth=1;
            sizesButton.layer.borderColor=[[UIColor colorWithRed:12.0/255.0 green:183.0/255.0 blue:174.0/255.0 alpha:1.0]CGColor];
        }
        sizesButton.layer.cornerRadius=5;
        sizesButton.layer.masksToBounds=YES;
        sizesButton.tag=i;
        sizesButton.titleLabel.font=[UIFont fontWithName:@"Muli" size:10.5];
        
        [sizesButton addTarget:self action:@selector(sizesButton:) forControlEvents:UIControlEventTouchUpInside];
        myScrollView.contentSize=CGSizeMake(myScrollView.frame.size.width,yAxis+sizesButton.frame.size.height);
        
        
        [myScrollView addSubview:sizesButton];
        
        
    }
    NSLog(@"%f",myScrollView.contentSize.height);
    if ([APPDELEGATE isiphone5])
    {
        if(myScrollView.contentSize.height<400)
            myScrollView.scrollEnabled=NO;
        else
            myScrollView.scrollEnabled=YES;
    }
    else
    {
        if(myScrollView.contentSize.height<310)
            myScrollView.scrollEnabled=NO;
        else
            myScrollView.scrollEnabled=YES;
    }
   
    [self.view addSubview:myScrollView];
    updateButton.hidden=NO;
    
}
#pragma  mark Buttons Actions
-(IBAction)sizesButton:(id)sender
{
   SizeModelObject *obj= [sizesMArray objectAtIndex:[sender tag]];
    if (obj.isSelect)
    {
        obj.isSelect=NO;
    }
    else
         obj.isSelect=YES;
    [self createScrollViewForSizes];
}
-(IBAction)updateButtonClicked:(id)sender
{
    NSPredicate *predicate=[NSPredicate predicateWithFormat:@"isSelect == 1"];
    NSArray *selectsizesarr=[sizesMArray filteredArrayUsingPredicate:predicate];
    
    NSString *selectestr=@"";
    for (SizeModelObject *obj in selectsizesarr)
    {
        if (obj.isSelect)
        {
            if ([selectestr isEqualToString:@""])
            {
                selectestr=obj.sizeId;
            }
            else
             selectestr=[NSString stringWithFormat:@"%@,%@",selectestr,obj.sizeId];
        }
    }
    
    //NSLog(@"stri s %@",selectestr);
    [APPDELEGATE startspinner:self.view];
    NSString *userid=[myappDelegate getStringFromPlist:@"userid"];
    [profileRep postSizesListByUserid:userid withSelectSizesString:selectestr];
}
-(IBAction)homebtnaction:(id)sender
{
    NSArray *arr=self.navigationController.viewControllers;
    for (UIViewController *childvc in arr) {
        
        if([childvc isKindOfClass:[SuggestionsViewController class]])
        {
            SuggestionsViewController *subvc=(SuggestionsViewController *)childvc;
            subvc.ismyfeed=NO;
            [self.navigationController popToViewController:subvc animated:YES];
        }
    }
    
}
#pragma mark API Call Responses
-(void)successResponseforArray:(NSMutableArray *)responseArray
{
    for (NSMutableDictionary *dict in responseArray)
    {
        SizeModelObject *obj=[[SizeModelObject alloc]init];
        obj.sizeName=[NSString stringWithFormat:@"%@",[dict objectForKey:@"lookupValue" ]];
        obj.sizeId=[NSString stringWithFormat:@"%@",[dict objectForKey:@"lookupID" ]];
        NSNumber *num=[dict objectForKey:@"isSelect"];
        obj.isSelect= [num boolValue];
        [sizesMArray addObject:obj];
    }
   // NSLog(@"sizem %@",sizesMArray);
     dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.001 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
         [self createScrollViewForSizes];
         [APPDELEGATE stopspinner:self.view];
     });
   
}
-(void)successResponseforDictionary:(NSDictionary *)responseDict
{
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.001 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        
        if ([[responseDict valueForKey:@"Message"] isEqualToString:@"User InActive State"])
        {
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Warning" message:@"Please try again after some time" delegate:nil cancelButtonTitle:nil otherButtonTitles:@"Ok    ", nil];
            [alert show];
        }
        else
        {
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Success" message:@"Saved!" delegate:self cancelButtonTitle:nil otherButtonTitles:@"Ok    ", nil];
            alert.tag=111;
            [alert show];
            
        }
        [APPDELEGATE stopspinner:self.view];
        //[self backbtnAction:nil];
    });
    
}
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag==111)
    {
        if (buttonIndex==0)
        {
            [self backbtnAction:nil];
        }
    }
    
}
-(void)errorMessage:(NSString *)message
{
     dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.001 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
    if ([message isEqualToString:@"message"])
    {
       
            
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:appname message:@"Something is wrong, Please contact Admin" delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
            [alert show];
       
        
    }
             [APPDELEGATE stopspinner:self.view];
     });
    
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)internetConnection
{
    UIAlertView *Internetalert=[[UIAlertView alloc]initWithTitle:appname message:@"Internet connection not available" delegate:self cancelButtonTitle:nil otherButtonTitles:@"Ok", nil];
    [Internetalert show];
    [APPDELEGATE stopspinner:self.view];
    
}


@end
