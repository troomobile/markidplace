//
//  RegisterViewController.m
//  MarkidPlace/Users/stellentsoftware/Documents/ismail/markidplace/MarkidPlace/MarkidPlace/RegistrationRepository.h
//
//  Created by stellentmac1 on 5/21/14.
//  Copyright (c) 2014 stellentmac1. All rights reserved.
//
#import"AppDelegate.h"
#import "LoginViewController.h"
#import "HomeViewController.h"
#import "RegisterViewController.h"

@interface LoginViewController ()

@end

@implementation LoginViewController
@synthesize navBar,emailTextField,passwordTextField,spinner;
@synthesize account,accountStore,Registerlabel,loginlabel,capturedImg,capturedImgData;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
#pragma mark ViewMethods

- (void)viewDidLoad
{
    [super viewDidLoad];
    //appdelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    // Do any additional setup after loading the view from its nib.
    screenSize = [[UIScreen mainScreen]bounds];
     //************//
    currenttextfield=[[UITextField alloc]init];
    UITapGestureRecognizer *singletap =
    [[UITapGestureRecognizer alloc]
     initWithTarget:self
     action:@selector(tapDetected:)];
    singletap.numberOfTapsRequired = 1;
    [self.view addGestureRecognizer:singletap];
    //*************//
    self.navBar.frame = CGRectMake(0, 0, 320, 64);
    [self.navBar setBackgroundImage:[UIImage imageNamed:@"navbar.png"] forBarMetrics:UIBarMetricsDefault];
    self.emailTextField.font = [UIFont fontWithName:@"Muli" size:18];
    self.passwordTextField.font = [UIFont fontWithName:@"Muli" size:18];
    
    
    loginlabel.layer.borderWidth = 1.0;
    loginlabel.layer.cornerRadius=5.0f;
    //loginlabel.layer.borderColor =[[UIColor greenColor] CGColor];
    loginlabel.layer.borderColor=[[UIColor colorWithRed:12.0/255.0f green:174.0/255.0f blue:180.0/255.0f alpha:1.0f]CGColor];
    Registerlabel.layer.borderWidth=1.0f;
    Registerlabel.layer.cornerRadius=5.0f;
    Registerlabel.layer.borderColor=[[UIColor colorWithRed:12.0/255.0f green:174.0/255.0f blue:180.0/255.0f alpha:1.0f]CGColor];
    registrationRep = [[RegistrationRepository alloc]init];
    registrationRep.delegate = self;
    self.navigationController.navigationBarHidden=YES;
    emailTextField.text=@"";
    passwordTextField.text=@"";
}
-(IBAction)tapDetected:(id)sender
{
     [self moveViewUp:NO];
    [currenttextfield resignFirstResponder];

}
- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

-(void)viewWillAppear:(BOOL)animated
{
    [myappDelegate.suggestonsGlobalarray removeAllObjects];
    [myappDelegate.myfeedsGlobalarray removeAllObjects];
    [myappDelegate.myfeedsGlobalarray removeAllObjects];
    [myappDelegate.allLikesGlobalarray removeAllObjects];
    [myappDelegate.mylikesGlobalarray removeAllObjects];
    
   
}
-(IBAction)twitterAction:(id)sender;
{
        [myappDelegate startspinner:self.view];
        //[self rotate360WithDuration:2 repeatCount:HUGE_VALF];
        if([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter])
        {
            if(!accountStore)
            {
                accountStore = [[ACAccountStore alloc] init];
            }
            else
            {
                
            }
            
            ACAccountType *accountType = [accountStore accountTypeWithAccountTypeIdentifier:ACAccountTypeIdentifierTwitter];
            // //NSLog(@"accont type is %@",accountType);
            [accountStore
             requestAccessToAccountsWithType:accountType
             options:NULL
             completion:^(BOOL granted, NSError *error) {
                 if (granted)
                 {
                     //NSLog(@"error is %@",error);
                     //  Step 2:  Create a request
                     NSArray *accountsArray = [accountStore accountsWithAccountType:accountType];
                     
                     //NSLog(@"accounts array count is %lu",(unsigned long)accountsArray.count);
                     if (accountsArray.count)
                     {
                         self.account = [accountsArray objectAtIndex:0];
                         
                         // NSString *userID = [[twitterAccount valueForKey:@"properties"] valueForKey:@"user_id"];
                         
                         NSURL *url = [NSURL URLWithString:@"https://api.twitter.com/1.1/users/show.json"];
                         NSLog(@"url is....%@",url);
                         // NSDictionary *params = @{@"screen_name" : account.username };
                         
                         NSDictionary *params = @{@"screen_name" : account.accountDescription
                                                  };
                         SLRequest *request =
                         [SLRequest requestForServiceType:SLServiceTypeTwitter
                                            requestMethod:SLRequestMethodGET
                                                      URL:url
                                               parameters:params];
                         
                         //  Attach an account to the request
                         [request setAccount:[accountsArray lastObject]];
                         
                         //  Step 3:  Execute the request
                         [request performRequestWithHandler:^(NSData *responseData,
                                                              NSHTTPURLResponse *urlResponse,
                                                              NSError *error) {
                             if (responseData)
                             {
                                 if (urlResponse.statusCode >= 200 && urlResponse.statusCode < 300)
                                 {
                                     [self performSelectorOnMainThread:@selector(twitterdetails:)
                                                            withObject:responseData waitUntilDone:YES];
                                 }
                                 else
                                 {
                                     //  [self performSelector:@selector(hideIndicator) withObject:nil afterDelay:0.1];
                                     
                                  //   [self performSelector:@selector(stopactivity) withObject:nil afterDelay:1];
                                     
                                     dispatch_async(dispatch_get_main_queue(), ^{
                                     NSString *errStr=@"There are no Twitter accounts configured. You can add a Twitter account in your phone's Settings.";
                                      [self alertViewMethod:errStr];
                                     });
                                     // //NSLog(@"The response status code is %d", urlResponse.statusCode);
                                 }
                             }
                         }];
                     }
                     else
                     {
                         // dispatch_async(dispatch_get_main_queue(), ^{
                         //  [self dismissError:@"Please set up your twitter account in iphone settings"];
                         
                         //  [spinner stopAnimating];
                         // [self performSelector:@selector(hideIndicator) withObject:nil afterDelay:0.1];
                         
                         NSString *errStr=@"There are no Twitter accounts configured. You can add a Twitter account in your phone's Settings.";
                         
                         dispatch_async(dispatch_get_main_queue(), ^{
                             
                             [self alertViewMethod:errStr];
                         });
                         
                         // });
                     }
                     
                 }
                 else
                 {
                     dispatch_async(dispatch_get_main_queue(), ^{
                         //  [self dismissError:@"Please set up your twitter account in iphone settings"];
                         //   [spinner stopAnimating];
                         //  [self performSelector:@selector(hideIndicator) withObject:nil afterDelay:0.1];
                         [myappDelegate stopspinner:self.view];
                         NSString *errStr=@"There are no Twitter accounts configured. You can add a Twitter account in your phone's Settings.";
                         
                         [self alertViewMethod:errStr];
                         
                     });
                 }
             }];
            
        }
        else
        {
            //   [spinner stopAnimating];
            //    [self performSelector:@selector(hideIndicator) withObject:nil afterDelay:0.1];
            
            NSString *errStr=@"There are no Twitter accounts configured. You can add a Twitter account in your phone's Settings.";
            dispatch_async(dispatch_get_main_queue(), ^{
                [myappDelegate stopspinner:self.view];
                [self alertViewMethod:errStr];
            });
        }
    
    
           //[AppDelegate alertmethod:appname :@"Twitter Under implementation,please try Facebook."];
    }
           

-(void)alertViewMethod:(NSString *)messageStr
{
    [myappDelegate stopspinner:self.view];
    // [spinner stopAnimating];
    //   [self performSelector:@selector(hideIndicator) withObject:nil afterDelay:0.1];
    
    //[self performSelector:@selector(stopactivity) withObject:nil afterDelay:1];
    
    UIAlertView *alertView=[[UIAlertView alloc]initWithTitle:appname message:messageStr delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alertView show];
}
-(void)twitterdetails:(NSData *)responseData
{
    
    //[spinner startAnimating];
    //spinner.hidden=NO;
    // self.view.userInteractionEnabled=NO;
   
    NSError* error = nil;
    NSDictionary* json = [NSJSONSerialization
                          JSONObjectWithData:responseData //1
                          options:NSJSONReadingAllowFragments
                          error:&error];
    
   
    NSLog(@"Twitter JSon is %@",json);
    NSString *name = [json objectForKey:@"name"];
    NSLog(@"Name is %@",name);
    NSString *scrnm = [json objectForKey:@"screen_name"];
    NSLog(@"screen_name is %@",scrnm);
    NSString *idStrin=[json objectForKey:@"id"];
    NSLog(@"idString is %@",idStrin);
    NSString *profileimage=[json objectForKey:@"profile_image_url"];
     NSLog(@"profileimage is %@",profileimage);
    twitterDict = [[NSMutableDictionary alloc]init];
    
    [twitterDict setValue:scrnm forKey:@"Username"];
    [twitterDict setValue:name forKey:@"Name"];
   
    [twitterDict setValue:@"" forKey:@"Email"];
   
    [twitterDict setValue:profileimage forKey:@"Profilepic"];
    [twitterDict setValue:idStrin forKey:@"SocialId"];
   
    [twitterDict setValue:@"Twitter" forKey:@"Type"];
    
    NSMutableDictionary *devicedict = [[NSMutableDictionary alloc]init];
    
    BOOL  isDeviceIphone=([[UIDevice currentDevice] userInterfaceIdiom]== UIUserInterfaceIdiomPhone);
    
    if (isDeviceIphone)
    {
        [devicedict setValue:@"iphone" forKey:@"DeviceType"];
    }
    else
    {
        [devicedict setValue:@"ipad" forKey:@"DeviceType"];
    }
    
    if (myappDelegate.deviceTokenString.length > 0)
    {
        [devicedict setValue:myappDelegate.deviceTokenString forKey:@"DeviceToken"];
    }
    else
    {
        myappDelegate.deviceTokenString=@"";
        //appdelegate.deviceTokenString=@"ba8d2205ca21d3aab0c170c485494747e470220f1aa42a2fae5a2e9178678469";
        
        [devicedict setValue:myappDelegate.deviceTokenString forKey:@"DeviceToken"];
    }
    
    float version = [[[UIDevice currentDevice] systemVersion] floatValue];
    NSLog(@"*** %f ***",version);
    
    [devicedict setValue:[NSString stringWithFormat:@"%f",version] forKey:@"DeviceOS"];
    
    [devicedict setValue:@"1" forKey:@"DeviceID"];
    [devicedict setValue:@"nothing" forKey:@"DeviceKeychain"];
    
    [twitterDict setValue:devicedict forKey:@"Device"];
    NSLog(@"twitterDict....%@",twitterDict);
    [registrationRep postAllDetails:twitterDict :@"Login" :@"POST"];
    
}

-(void)viewWillDisappear:(BOOL)animated
{

    [self moveViewUp:NO];
    [currenttextfield resignFirstResponder];
}

-(IBAction)backAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - textfield

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    currenttextfield=textField;
    [self moveViewUp:YES];

}
-(void)textFieldDidEndEditing:(UITextField *)textField
{
    [UIView beginAnimations: @"anim" context: nil];
    [UIView setAnimationBeginsFromCurrentState: YES];
    [UIView setAnimationDuration:0.2];
    
    CGRect frame=self.view.frame;
    
    frame.origin.y=0;
    
    self.view.frame=frame;
    [UIView commitAnimations];
    
}
                   /********Animation for the view*******/
-(void) moveViewUp:(BOOL)moveUP
{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.3];

    CGRect rect = self.view.frame;

    CGRect screenbounds=[[UIScreen mainScreen]bounds];

    if (moveUP)
    {
        if(screenbounds.size.height==568)
        {

            if(currenttextfield==self.emailTextField)
            {
               // rect.origin.y = -90;
            }
            else if(currenttextfield==self.passwordTextField)
            {

                rect.origin.y = -200;//-70
            }


        }
        else
        {

            if(currenttextfield==self.emailTextField)
            {

               // rect.origin.y = -90;
            }
            else if(currenttextfield==self.passwordTextField)
            {

                rect.origin.y = -100;
            }

        }
    }
    else
    {
        rect.origin.y =0;

    }
    self.view.frame = rect;

    [UIView commitAnimations];
    
}


-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self moveViewUp:NO];
    [textField resignFirstResponder];
    return YES;
}

// This method is for checking the validation of Email and password.
- (BOOL) emailvalidate
{
    NSString *email;
    NSString *password;
    password=self.passwordTextField.text;
    
    email=[self.emailTextField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    
    NSString *emailRegEx = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
	NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegEx];
    
    
    if(email==nil||[email isEqualToString:@""] || email.length==0 || [email isEqualToString:@" "])
    {
        //   [spinner stopAnimating];
        UIAlertView *alertview=[[UIAlertView alloc]initWithTitle:appname message:@"Email should not be empty." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alertview show];
        return NO;
	}
    else if(password==nil||[password isEqualToString:@""] || password.length==0 || [password isEqualToString:@" "])
    {
        //  [spinner stopAnimating];
        UIAlertView *alertview=[[UIAlertView alloc]initWithTitle:appname message:@"Enter a valid password." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alertview show];
        return NO;
    }
    else if ([emailTest evaluateWithObject:email] != YES)
    {
        UIAlertView *loginalert = [[UIAlertView alloc] initWithTitle:appname message:@"Email field not in proper format." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:Nil, nil];
        [loginalert show];
        return NO;
    }
    else if(password.length>=1 && password.length<=5)
    {
        UIAlertView *loginalert = [[UIAlertView alloc] initWithTitle:appname message:@"Password is too short." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:Nil, nil];
        [loginalert show];
        return NO;
    }
    else
    {
        return YES;
    }
}
#pragma mark ButtonAction

//
-(IBAction)loginAction:(id)sender
{
    
    [emailTextField resignFirstResponder];
    [passwordTextField resignFirstResponder];
    
    //[AppDelegate alertmethod:appname :@"Under implementation. Please try facebook" ];
    
    
    NSLog(@"check this %@",[emailTextField.text lowercaseString]);
    if ([self emailvalidate])
    {
                //[self.spinner startAnimating];
        [myappDelegate startspinner:self.view];
        self.view.userInteractionEnabled = NO;
        NSMutableDictionary *loginDict = [[NSMutableDictionary alloc]init];
        [loginDict setValue:[emailTextField.text lowercaseString]  forKey:@"UserName"];
        [loginDict setValue:passwordTextField.text forKey:@"Password"];
        [loginDict setValue:@"General" forKey:@"Type"];
        NSMutableDictionary *devicedict = [[NSMutableDictionary alloc]init];
        
        BOOL  isDeviceIphone=([[UIDevice currentDevice] userInterfaceIdiom]== UIUserInterfaceIdiomPhone);
        
        if (isDeviceIphone)
        {
            [devicedict setValue:@"iphone" forKey:@"DeviceType"];
        }
        else
        {
            [devicedict setValue:@"ipad" forKey:@"DeviceType"];
        }
        
        if (myappDelegate.deviceTokenString.length > 0)
        {
            [devicedict setValue:myappDelegate.deviceTokenString forKey:@"DeviceToken"];
        }
        else
        {
            // appdelegate.deviceTokenString=@"";
            //appdelegate.deviceTokenString=@"ba8d2205ca21d3aab0c170c485494747e470220f1aa42a2fae5a2e9178678469";
            
            [devicedict setValue:@"" forKey:@"DeviceToken"];
        }
        
        float version = [[[UIDevice currentDevice] systemVersion] floatValue];
        NSLog(@"*** %f ***",version);
        
        [devicedict setValue:[NSString stringWithFormat:@"%f",version] forKey:@"DeviceOs"];
        
        [devicedict setValue:@"1" forKey:@"DeviceId"];
        [devicedict setValue:@"nothing" forKey:@"DeviceKeychain"];
        NSLog(@"devicedict.....%@",devicedict);
        
        [loginDict setValue:devicedict forKey:@"Device"];
        

        
        
        [registrationRep postAllDetails:loginDict :@"Login" :@"POST"];
    }
    
}
// Performs Forget password ViewController.
-(IBAction)forgetaction:(id)sender
{
    ForgetPasswordViewController *forgetVC;
    if (myappDelegate.isiphone5)
    {
        forgetVC=[[ForgetPasswordViewController alloc]initWithNibName:@"ForgetPasswordViewController" bundle:nil];
        
    }
    else{
        forgetVC=[[ForgetPasswordViewController alloc]initWithNibName:@"ForgetPasswordViewController_4s" bundle:nil];
        
        
    }
    forgetVC.loginstr=@"forget";
    [self.navigationController pushViewController:forgetVC animated:YES];
    
}
// this Action performs the RegistrationViewController
-(IBAction)registerAction:(id)sender;
{
    RegisterViewController *Registervc;
    
    [emailTextField resignFirstResponder];
    [passwordTextField resignFirstResponder];
    
//    if ([self emailvalidate])
//    {
//          if (myappDelegate.isiphone5)
//    {
//        Registervc = [[RegisterViewController alloc]initWithNibName:@"RegisterViewController" bundle:nil];
//    }
//    else
//    {
//        Registervc = [[RegisterViewController alloc]initWithNibName:@"RegisterViewController_iphone4" bundle:nil];
//    }
//        Registervc.emailstr=[emailTextField.text lowercaseString];
//      Registervc.passwordstr=passwordTextField.text;
//    }
//       [self.navigationController pushViewController:Registervc animated:YES];
    if ([self emailvalidate])
    {
        
        NSMutableDictionary *regDict = [[NSMutableDictionary alloc]init];
        [regDict setValue:@"" forKey:@"Name"];
        [regDict setValue:[emailTextField.text lowercaseString] forKey:@"UserName"];
        [regDict setValue:[emailTextField.text lowercaseString] forKey:@"Email"];
        
        // [regDict setValue:profileImageView.image forKey:@"Profilepic"];
        
        
        // NSMutableArray *urlsarray=[myappDelegate postimagestoamazone:NO photosaray:nil :profileImageView.image];
        
        //NSString *finalstr=  [[urlsarray valueForKey:@"description"] componentsJoinedByString:@","];
        
        
        // [regDict setValue:finalstr forKey:@"Profilepic"];
        [regDict setValue:passwordTextField.text forKey:@"Password"];
        
        
        NSMutableDictionary *devicedict = [[NSMutableDictionary alloc]init];
        
        BOOL  isDeviceIphone=([[UIDevice currentDevice] userInterfaceIdiom]== UIUserInterfaceIdiomPhone);
        
        if (isDeviceIphone)
        {
            [devicedict setValue:@"iphone" forKey:@"DeviceType"];
        }
        else
        {
            [devicedict setValue:@"ipad" forKey:@"DeviceType"];
        }
        
        if (myappDelegate.deviceTokenString.length > 0)
        {
            [devicedict setValue:myappDelegate.deviceTokenString forKey:@"DeviceToken"];
        }
        else
        {
            // appdelegate.deviceTokenString=@"";
            //appdelegate.deviceTokenString=@"ba8d2205ca21d3aab0c170c485494747e470220f1aa42a2fae5a2e9178678469";
            
            [devicedict setValue:@"" forKey:@"DeviceToken"];
        }
        
        float version = [[[UIDevice currentDevice] systemVersion] floatValue];
        NSLog(@"*** %f ***",version);
        
        [devicedict setValue:[NSString stringWithFormat:@"%f",version] forKey:@"DeviceOs"];
        
        [devicedict setValue:@"1" forKey:@"DeviceId"];
        [devicedict setValue:@"nothing" forKey:@"DeviceKeychain"];
        
        [regDict setValue:devicedict forKey:@"Device"];
        
        //[registrationRep postAllDetails:regDict :@"Registration" :@"POST"];
        [APPDELEGATE startspinner:self.view];
        [registrationRep newregistration:regDict];
        
    }

}
//NewRegistrastion success response.
-(void)successResponseNewRegistrastion:(NSDictionary *)responseDict
{
    
    NSLog(@"responsedict.....%@",responseDict);
    dispatch_async(dispatch_get_main_queue(), ^{
        [APPDELEGATE stopspinner:self.view];
        if ([[responseDict valueForKey:@"message"] isEqualToString:@"User Already Existed"])
        {
            //[AppDelegate alertmethod:appname :@"Your account has been deleted successfully, Thanks For using Markid place"];
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.001 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                
                NSString *emailstr=[NSString stringWithFormat:@"This email address, %@, is already registered.",emailTextField.text];
                UIAlertView *alert = [[UIAlertView alloc]initWithTitle:appname message:emailstr delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                
                [alert show];
                //[myappDelegate stopspinner:self.view];
                
            });
            
        }
        else
        {
            
            
            RegisterViewController *registerVC;
            if (myappDelegate.isiphone5)
            {
                registerVC=[[RegisterViewController alloc]initWithNibName:@"RegisterViewController" bundle:nil];
            }
            else
            {
                registerVC=[[RegisterViewController alloc]initWithNibName:@"RegisterViewController_iphone4" bundle:nil];
                
            }
            
            registerVC.emailstr=emailTextField.text;
            registerVC.passwordstr=passwordTextField.text;
            [self.navigationController pushViewController:registerVC animated:YES];
            
            
        }
        
    });
    
    
}

// Success rResponse For the Registration.
-(void)successResponseforRegistration:(NSDictionary *)responseDict
{
    NSLog(@"responseDict ====%@",responseDict);
    
    //[self.spinner stopAnimating];
    
    
    dispatch_sync(dispatch_get_main_queue(), ^{
        
        [myappDelegate stopspinner :self.view];
        if ([responseDict[@"message"] isEqualToString:@"Invalid UserName or Password.."])
        {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:appname message:@"Invalid Email or Password." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
              [myappDelegate stopspinner:self.view];
            passwordTextField.text=@"";
        }
        else if ([responseDict[@"message"] isEqualToString:@"Validate User"])
        {
        if([myappDelegate isnullorempty:[responseDict valueForKey:@"email"]])
            {
                ForgetPasswordViewController *forgetVC;
                if (myappDelegate.isiphone5)
                {
                    forgetVC=[[ForgetPasswordViewController alloc]initWithNibName:@"ForgetPasswordViewController" bundle:nil];
                    
                }
                else{
                    forgetVC=[[ForgetPasswordViewController alloc]initWithNibName:@"ForgetPasswordViewController_4s" bundle:nil];
                    
                    
                }
           forgetVC.loginstr=@"Login";
                forgetVC.loginresponsedict=responseDict;
                [self.navigationController pushViewController:forgetVC animated:YES];
           }
            else
            {       
            [myappDelegate stopspinner:self.view];
            [myappDelegate insertStringInPlist:@"userid" value:responseDict[@"userId"]];
            [myappDelegate insertStringInPlist:@"email" value:responseDict[@"email"]];
            [myappDelegate insertStringInPlist:@"type" value:responseDict[@"type"]];
             [myappDelegate insertStringInPlist:@"realname" value:responseDict[@"name"]];
            
            [myappDelegate insertStringInPlist:@"profilePicture" value:responseDict[@"profilePicture"]];
            [APPDELEGATE insertStringInPlist:@"notificationcount" value:[NSString stringWithFormat:@"%@",responseDict[@"notificationCount"]]];
           myappDelegate.useridstring=responseDict[@"userId"];
             [APPDELEGATE insertStringInPlist:@"paypalstr" value:responseDict[@"paypalEmailAddress"]];
            if(APPDELEGATE.islistusers==YES)
            {
                APPDELEGATE.islistusers=NO;
                dispatch_async( dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                    [APPDELEGATE performSelector:@selector(getuserslist) withObject:nil afterDelay:0.001];
                });
            }
            
            else if ([myappDelegate isnullorempty:responseDict[@"userName"]] || [myappDelegate isnullorempty:responseDict[@"name"] ])
            {
                EditProfileViewController *editprofile;
                if (myappDelegate.isiphone5)
                {
                    editprofile=[[EditProfileViewController alloc]initWithNibName:@"EditProfileViewController" bundle:nil];
                }
                else
                {
                    editprofile=[[EditProfileViewController alloc]initWithNibName:@"EditProfileViewController_iphone4" bundle:nil];
                }
                editprofile.isnew=YES;
                [self.navigationController pushViewController:editprofile animated:YES];
                
            }
            else
            {
                NSString *teststr=responseDict[@"userName"];
                
                teststr=[teststr stringByReplacingOccurrencesOfString:@"@" withString:@""];
                
                [myappDelegate insertStringInPlist:usernamekey value:teststr];
            
            if ([AppDelegate isiphone5])
            {
                myappDelegate.suggestionviewcont = [[SuggestionsViewController alloc]initWithNibName:@"SuggestionsViewController" bundle:nil];
            }
            else
            {
                myappDelegate.suggestionviewcont = [[SuggestionsViewController alloc]initWithNibName:@"SuggestionsViewController_iphone4" bundle:nil];
          
            }
                myappDelegate.suggestionviewcont.isItFirstTime=YES;
            [self.navigationController pushViewController:myappDelegate.suggestionviewcont animated:YES];
                [myappDelegate dealWithPhoneBookContacts];
            }

            }
        }
        else if([responseDict[@"message"] isEqualToString:@"Invalid Email or UserName or Password.."])
        {
            [myappDelegate stopspinner:self.view];
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:appname message:@"Invalid Email or Password." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
            [myappDelegate stopspinner:self.view];
            passwordTextField.text=@"";
        }
        else
        {
              [myappDelegate stopspinner:self.view];
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:appname message:responseDict[@"message"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            passwordTextField.text=@"";
            [alert show];
        }
        
    });
    
}
//  this method shows error response for Login.
-(void)errorMessage:(NSString *)message
{
    NSLog(@"error msg %@",message);
    dispatch_sync(dispatch_get_main_queue(), ^{
        [myappDelegate stopspinner:self.view];
        [self.spinner stopAnimating];
        self.view.userInteractionEnabled = YES;
        
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:appname message:@"Login failed" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        passwordTextField.text=@"";
        
    });
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


/*
-(IBAction)facebookaction:(id)sender
{
    
     [AppDelegate alertmethod:appname :@"Under implementation. Please try login" ];
    
//       HomeViewController *suggetionsVC;
//       if (screenSize.size.height == 568)
//       {
//           suggetionsVC = [[HomeViewController alloc]initWithNibName:@"HomeViewController" bundle:nil];
//       }
//       else
//       {
//           suggetionsVC = [[HomeViewController alloc]initWithNibName:@"HomeViewController_iphone4" bundle:nil];
//       }
//       [self.navigationController pushViewController:suggetionsVC animated:YES];
}
*/

/*
-(IBAction)facebookaction:(id)sender
{
   // [self.spinner startAnimating];
    [appdelegate startspinner:self.view];
    self.view.userInteractionEnabled = NO;

    NSArray *permissions =
    [NSArray arrayWithObjects:@"email", @"user_photos",@"read_stream",@"profile", nil];
    NSDictionary * dict=[NSDictionary dictionaryWithObjectsAndKeys:@"770236303030542",ACFacebookAppIdKey,permissions,ACFacebookPermissionsKey,ACFacebookAudienceEveryone,ACFacebookAudienceKey, nil];

    accountStore = [[ACAccountStore alloc]init];
    ACAccountType *accountType = [accountStore accountTypeWithAccountTypeIdentifier:ACAccountTypeIdentifierFacebook];

    [accountStore requestAccessToAccountsWithType:accountType options:dict completion:^(BOOL granted, NSError *error)
     {
         if (granted == YES)
         {
             //  [self performSelector:@selector(startSpinner) withObject:nil afterDelay:0.1];

             NSArray *accountsArray = [accountStore accountsWithAccountType:accountType];

             ////NSLog(@"accountsArray =====%@",accountsArray);

             if ([accountsArray count] > 0)
             {
                 account = [accountsArray objectAtIndex:0];

                 //  ////NSLog(@"aaaaaaaaaaaa%@",Account);

                 ACAccountCredential *fbCredential = [account credential];

                 acessToken = [NSString stringWithFormat:@"%@",fbCredential.oauthToken];

                 // ////NSLog(@"acessToken   %@",acessToken);

                 NSDictionary *parameters = @{@"access_token": acessToken};

                 NSURL *requestURL = [NSURL URLWithString:@"https://graph.facebook.com/me"];

                 SLRequest *postRequest = [SLRequest requestForServiceType:SLServiceTypeFacebook requestMethod:SLRequestMethodGET URL:requestURL parameters:parameters];

                 postRequest.account = account;

                 [postRequest performRequestWithHandler:
                  ^(NSData *responseData, NSHTTPURLResponse
                    *urlResponse, NSError *error)
                  {

                      ////NSLog(@"response ======%@",urlResponse);

                      NSDictionary *dataSourceDict= [NSJSONSerialization JSONObjectWithData:responseData options:NSJSONReadingMutableLeaves  error:&error];

                      ////NSLog(@"dataSourceDict =====%@",dataSourceDict);

                      if([dataSourceDict objectForKey:@"error"]!=nil)
                      {
                          [self attemptRenewCredentials];
                      }
                      else
                      {
                          NSString *birthday = dataSourceDict[@"birthday"];

                          NSDate *datenow = [NSDate date];

                          NSDateFormatter *formatter = [[NSDateFormatter alloc] init];

                          [formatter setDateFormat:@"MM/dd/yyyy"];

                          NSDate *startDate = [formatter dateFromString:birthday];

                          NSCalendar *gregorianCalendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];

                          unsigned int unitFlags = NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit;
                          NSDateComponents *comps = [gregorianCalendar components:unitFlags fromDate:startDate  toDate:datenow  options:0];
                          int years = [comps year];
                          //int months = [comps month];
                          //int days = [comps day];
                          NSLog(@" check the dict %@",dataSourceDict);

                          NSMutableArray *fbparamsarray=[[NSMutableArray alloc]init];
                          facebookID = dataSourceDict[@"id"];
                          NSString *facebookname = dataSourceDict[@"name"];
                          [appdelegate insertStringInPlist:@"fbname" value:facebookname];
                          NSString *email = dataSourceDict[@"email"];
                          NSURL *pictureURL = [NSURL URLWithString:[NSString stringWithFormat:@"https://graph.facebook.com/%@/picture?type=large",facebookID]];
                          NSString *gender = dataSourceDict[@"gender"];
                          NSString *location = dataSourceDict[@"location"][@"name"];
                          NSString *age = [NSString stringWithFormat:@"%d",years];
                          if ([facebookname isEqualToString:@""] || facebookname.length == 0 || [facebookname isKindOfClass:[NSNull class]])
                          {
                              facebookname = @"";
                          }

                          if ([email isEqualToString:@""] || email.length == 0 || [email isKindOfClass:[NSNull class]])
                          {
                              email = @"";
                          }

                          if ([gender isEqualToString:@""] || gender.length == 0 || [gender isKindOfClass:[NSNull class]])
                          {
                              gender = @"";
                          }

                          if ([location isEqualToString:@""] || location.length == 0 || [location isKindOfClass:[NSNull class]])
                          {
                              location = @"";
                          }

                          if ([age isEqualToString:@""] || age.length == 0 || [age isKindOfClass:[NSNull class]])
                          {
                              age = @"";
                          }
                          [fbparamsarray addObject:facebookID];
                          [fbparamsarray addObject:facebookname];
                          [fbparamsarray addObject:pictureURL];
                          [fbparamsarray addObject:email];
                          [fbparamsarray addObject:gender];
                          [fbparamsarray addObject:location];
                          [fbparamsarray addObject:age];

                          [self postFBDetails:fbparamsarray];

                          NSLog(@"fbparamsarray ===%@ ===%lu",fbparamsarray,(unsigned long)fbparamsarray.count);
                      }


                  }];
             }
             else{
                 [appdelegate stopspinner:self.view];
             }
         }
         else
         {
              [appdelegate stopspinner:self.view];
             //[self.spinner stopAnimating];
             self.view.userInteractionEnabled = YES;

             UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"No Facebook Accounts" message:@"For some reason, Facebook is not recognizing MarkidPlace. To fix, please tap your phone's Settings icon, scroll down to Facebook, where you should see your Facebook login credentials (if you don't, you need to log in) and there you should see a MarkidPlace icon also (if you don't, you need to refresh the permissions again). Toggle MarkidPlace to the \"ON\" position and you should be connected." delegate:self cancelButtonTitle:@"OK"  otherButtonTitles:nil];
             alert.tag = 11;
             [alert show];
         }
     }];
}


-(void)postFBDetails:(NSMutableArray *)fbArray
{
    dispatch_sync(dispatch_get_main_queue(), ^{

        NSLog(@"@@@@@@@@@ fb details=====> %@",fbArray);
        facebookDict = [[NSMutableDictionary alloc]init];

        [facebookDict setValue:@"" forKey:@"Username"];
        [facebookDict setValue:[fbArray objectAtIndex:1] forKey:@"Name"];
        [facebookDict setValue:[fbArray objectAtIndex:3] forKey:@"Email"];
        NSString *profileURL=[NSString stringWithFormat:@"%@",[fbArray objectAtIndex:2]];
        [facebookDict setValue:profileURL forKey:@"Profilepic"];
        [facebookDict setValue:[fbArray objectAtIndex:0] forKey:@"SocialId"];
        //[facebookDict setValue:[fbArray objectAtIndex:0] forKey:@"FacebookId"];
        [facebookDict setValue:@"Facebook" forKey:@"Type"];

        NSMutableDictionary *devicedict = [[NSMutableDictionary alloc]init];

        BOOL  isDeviceIphone=([[UIDevice currentDevice] userInterfaceIdiom]== UIUserInterfaceIdiomPhone);

        if (isDeviceIphone)
        {
            [devicedict setValue:@"iphone" forKey:@"DeviceType"];
        }
        else
        {
            [devicedict setValue:@"ipad" forKey:@"DeviceType"];
        }

        if (appdelegate.deviceTokenString.length > 0)
        {
            [devicedict setValue:appdelegate.deviceTokenString forKey:@"DeviceToken"];
        }
        else
        {
            appdelegate.deviceTokenString=@"";
            //appdelegate.deviceTokenString=@"ba8d2205ca21d3aab0c170c485494747e470220f1aa42a2fae5a2e9178678469";

            [devicedict setValue:appdelegate.deviceTokenString forKey:@"DeviceToken"];
        }

        float version = [[[UIDevice currentDevice] systemVersion] floatValue];
        NSLog(@"*** %f ***",version);

        [devicedict setValue:[NSString stringWithFormat:@"%f",version] forKey:@"DeviceOS"];

        [devicedict setValue:@"1" forKey:@"DeviceID"];
        [devicedict setValue:@"nothing" forKey:@"DeviceKeychain"];

        [facebookDict setValue:devicedict forKey:@"Device"];

        [registrationRep postAllDetails:facebookDict :@"Login" :@"POST"];

    });

}


-(void)attemptRenewCredentials
{
    [self.accountStore renewCredentialsForAccount:(ACAccount *)account completion:^(ACAccountCredentialRenewResult renewResult, NSError *error){
        if(!error)
        {
            switch (renewResult) {
                case ACAccountCredentialRenewResultRenewed:
                    ////NSLog(@"Good to go");
                    [self facebookaction:self];
                    break;
                case ACAccountCredentialRenewResultRejected:
                    ////NSLog(@"User declined permission");
                    [self attemptRenewCredentials];
                    break;
                case ACAccountCredentialRenewResultFailed:
                    ////NSLog(@"non-user-initiated cancel, you may attempt to retry");
                    [self attemptRenewCredentials];
                    break;
                default:
                    break;
            }

        }
        else{
            //handle error
            ////NSLog(@"error from renew credentials%@",error);
            [self facebookaction:self];
            // [self attemptRenewCredentials];
        }
    }];
}
*/

-(IBAction)facebookaction:(id)sender
{
    
   emailTextField.text=@"";
    passwordTextField.text=@"";
       // [self.spinner startAnimating];
    [currenttextfield resignFirstResponder];
    [myappDelegate startspinner:self.view];
    self.view.userInteractionEnabled = NO;
    
    NSArray *permissions =
    [NSArray arrayWithObjects:@"email",@"user_photos",@"read_stream", nil];
    NSDictionary * dict=[NSDictionary dictionaryWithObjectsAndKeys:@"770236303030542",ACFacebookAppIdKey,permissions,ACFacebookPermissionsKey,ACFacebookAudienceEveryone,ACFacebookAudienceKey, nil];
    
    accountStore = [[ACAccountStore alloc]init];
    ACAccountType *accountType = [accountStore accountTypeWithAccountTypeIdentifier:ACAccountTypeIdentifierFacebook];

        [accountStore requestAccessToAccountsWithType:accountType options:dict completion:^(BOOL granted, NSError *error)
         {
             if (granted == YES)
             {
                 //  [self performSelector:@selector(startSpinner) withObject:nil afterDelay:0.1];
                 
                 NSLog(@"Grantedddddddddddd");
                 NSArray *accountsArray = [accountStore accountsWithAccountType:accountType];
                 
                 ////NSLog(@"accountsArray =====%@",accountsArray);
                 
                 if ([accountsArray count] > 0)
                 {
                     account = [accountsArray objectAtIndex:0];
                     
                     //  ////NSLog(@"aaaaaaaaaaaa%@",Account);
                     
                     ACAccountCredential *fbCredential = [account credential];
                     
                     acessToken = [NSString stringWithFormat:@"%@",fbCredential.oauthToken];
                     
                     // ////NSLog(@"acessToken   %@",acessToken);
                     
                     NSDictionary *parameters = @{@"access_token": acessToken};
                     
                     NSURL *requestURL = [NSURL URLWithString:@"https://graph.facebook.com/me"];
                     
                     SLRequest *postRequest = [SLRequest requestForServiceType:SLServiceTypeFacebook requestMethod:SLRequestMethodGET URL:requestURL parameters:parameters];
                     
                     postRequest.account = account;
                     
                     [postRequest performRequestWithHandler:
                      ^(NSData *responseData, NSHTTPURLResponse
                        *urlResponse, NSError *error)
                      {
                          
                          NSLog(@"response ======%@",urlResponse);
                          
                          NSDictionary *dataSourceDict= [NSJSONSerialization JSONObjectWithData:responseData options:NSJSONReadingMutableLeaves  error:&error];
                          
                          NSLog(@"dataSourceDict =====%@",dataSourceDict);
                          
                          if([dataSourceDict objectForKey:@"error"]!=nil)
                          {
                              [self attemptRenewCredentials];
                          }
                          else
                          {
                              NSString *birthday = dataSourceDict[@"birthday"];
                              
                              NSDate *datenow = [NSDate date];
                              
                              NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
                              
                              [formatter setDateFormat:@"MM/dd/yyyy"];
                              
                              NSDate *startDate = [formatter dateFromString:birthday];
                              
                              NSCalendar *gregorianCalendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
                              
                              unsigned int unitFlags = NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit;
                              NSDateComponents *comps = [gregorianCalendar components:unitFlags fromDate:startDate  toDate:datenow  options:0];
                              int years = [comps year];
                              //int months = [comps month];
                              //int days = [comps day];
                              NSLog(@" check the dict %@",dataSourceDict);
                              
                              NSMutableArray *fbparamsarray=[[NSMutableArray alloc]init];
                              facebookID = dataSourceDict[@"id"];
                              NSString *facebookname = dataSourceDict[@"name"];
                              [myappDelegate insertStringInPlist:@"fbname" value:facebookname];
                              NSString *email = dataSourceDict[@"email"];
                              NSURL *pictureURL = [NSURL URLWithString:[NSString stringWithFormat:@"https://graph.facebook.com/%@/picture?type=large",facebookID]];
                              NSString *gender = dataSourceDict[@"gender"];
                              NSString *location = dataSourceDict[@"location"][@"name"];
                              NSString *age = [NSString stringWithFormat:@"%d",years];
                              if ([facebookname isEqualToString:@""] || facebookname.length == 0 || [facebookname isKindOfClass:[NSNull class]])
                              {
                                  facebookname = @"";
                              }
                              
                              if ([email isEqualToString:@""] || email.length == 0 || [email isKindOfClass:[NSNull class]])
                              {
                                  email = @"";
                              }
                              
                              if ([gender isEqualToString:@""] || gender.length == 0 || [gender isKindOfClass:[NSNull class]])
                              {
                                  gender = @"";
                              }
                              
                              if ([location isEqualToString:@""] || location.length == 0 || [location isKindOfClass:[NSNull class]])
                              {
                                  location = @"";
                              }
                              
                              if ([age isEqualToString:@""] || age.length == 0 || [age isKindOfClass:[NSNull class]])
                              {
                                  age = @"";
                              }
                              [fbparamsarray addObject:facebookID];
                              [fbparamsarray addObject:facebookname];
                              [fbparamsarray addObject:pictureURL];
                              [fbparamsarray addObject:email];
                              [fbparamsarray addObject:gender];
                              [fbparamsarray addObject:location];
                              [fbparamsarray addObject:age];
                              
                              [self postFBDetails:fbparamsarray];
                              
                              NSLog(@"fbparamsarray ===%@ ===%lu",fbparamsarray,(unsigned long)fbparamsarray.count);
                          }
                          
                          
                      }];
                 }
                 else
                 {
                     [myappDelegate stopspinner:self.view];
                 }
             }
             else
             {
                 
                 NSLog(@"error in FB========> %@",error);
                 [myappDelegate stopspinner:self.view];
                 //[self.spinner stopAnimating];
                 self.view.userInteractionEnabled = YES;
                 
                 UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"No Facebook Accounts" message:@"For some reason, Facebook is not recognizing marKIDplace. To fix, please tap your phone's Settings icon, scroll down to Facebook, where you should see your Facebook login credentials (if you don't, you need to log in) and there you should see a marKIDplace icon also (if you don't, you need to refresh the permissions again). Toggle marKIDplace to the \"ON\" position and you should be connected." delegate:self cancelButtonTitle:@"OK"  otherButtonTitles:nil];
                 alert.tag = 11;
                 [alert show];
             }
         }];
        }

-(void)postFBDetails:(NSMutableArray *)fbArray
{
    dispatch_sync(dispatch_get_main_queue(), ^{
        
        NSLog(@"@@@@@@@@@ fb details=====> %@",fbArray);
        facebookDict = [[NSMutableDictionary alloc]init];
        
        [facebookDict setValue:@"" forKey:@"Username"];
        [facebookDict setValue:[fbArray objectAtIndex:1] forKey:@"Name"];
        if([self returningString:[NSString stringWithFormat:@"%@",[fbArray objectAtIndex:3]]].length!=0)
        {
            [facebookDict setValue:[fbArray objectAtIndex:3] forKey:@"Email"];
        }
        else
        {
            [facebookDict setValue:@"" forKey:@"Email"];
        }
        NSString *profileURL=[NSString stringWithFormat:@"%@",[fbArray objectAtIndex:2]];
        [facebookDict setValue:profileURL forKey:@"Profilepic"];
        [facebookDict setValue:[fbArray objectAtIndex:0] forKey:@"SocialId"];
        //[facebookDict setValue:[fbArray objectAtIndex:0] forKey:@"FacebookId"];
        [facebookDict setValue:@"Facebook" forKey:@"Type"];
        
        NSMutableDictionary *devicedict = [[NSMutableDictionary alloc]init];
        
        BOOL  isDeviceIphone=([[UIDevice currentDevice] userInterfaceIdiom]== UIUserInterfaceIdiomPhone);
        
        if (isDeviceIphone)
        {
            [devicedict setValue:@"iphone" forKey:@"DeviceType"];
        }
        else
        {
            [devicedict setValue:@"ipad" forKey:@"DeviceType"];
        }
        
        if (myappDelegate.deviceTokenString.length > 0)
        {
            [devicedict setValue:myappDelegate.deviceTokenString forKey:@"DeviceToken"];
        }
        else
        {
            myappDelegate.deviceTokenString=@"";
            //appdelegate.deviceTokenString=@"ba8d2205ca21d3aab0c170c485494747e470220f1aa42a2fae5a2e9178678469";
            
            [devicedict setValue:myappDelegate.deviceTokenString forKey:@"DeviceToken"];
        }
        
        float version = [[[UIDevice currentDevice] systemVersion] floatValue];
        NSLog(@"*** %f ***",version);
        
        [devicedict setValue:[NSString stringWithFormat:@"%f",version] forKey:@"DeviceOS"];
        
        [devicedict setValue:@"1" forKey:@"DeviceID"];
        [devicedict setValue:@"nothing" forKey:@"DeviceKeychain"];
        
        [facebookDict setValue:devicedict forKey:@"Device"];
        
        [registrationRep postAllDetails:facebookDict :@"Login" :@"POST"];
        
    });
    
}


-(void)attemptRenewCredentials
{
    [self.accountStore renewCredentialsForAccount:(ACAccount *)account completion:^(ACAccountCredentialRenewResult renewResult, NSError *error){
        if(!error)
        {
            switch (renewResult) {
                case ACAccountCredentialRenewResultRenewed:
                    ////NSLog(@"Good to go");
                    [self facebookaction:self];
                    break;
                case ACAccountCredentialRenewResultRejected:
                    ////NSLog(@"User declined permission");
                    [self attemptRenewCredentials];
                    break;
                case ACAccountCredentialRenewResultFailed:
                    ////NSLog(@"non-user-initiated cancel, you may attempt to retry");
                    [self attemptRenewCredentials];
                    break;
                default:
                    break;
            }
            
        }
        else{
            //handle error
            ////NSLog(@"error from renew credentials%@",error);
            [self facebookaction:self];
            // [self attemptRenewCredentials];
        }
    }];
}


-(NSString *)returningString:(NSString *)str
{
    if ([str isKindOfClass:[NSNull class]])
    {
        return @"";
    }
    else
    {
        if ([str isKindOfClass:[NSString class]]) {
            
            //if([[str lowercaseString] rangeOfString:@"null"].location !=NSNotFound)
            
            if([[str lowercaseString] isEqualToString:@"null"] || [[str lowercaseString] isEqualToString:@"(null)"] || [[str lowercaseString] isEqualToString:@"<null>"])
            {
                return @"";
            }
            else
            {
                return str;
            }
        }
        else
        {
            return @"";
        }
    }
}
-(void)successResponseforlistusers:(NSDictionary *)responseDict
{
    NSLog(@"thelist main count is =======>>>>>>>>>><<<<<mainresponse dict is %@",responseDict);
    NSMutableArray *mainArray=[responseDict valueForKey:@"listusers"];
    [APPDELEGATE.userlistArray removeAllObjects];
    for (int i=0; i<mainArray.count; i++)
    {
        NSMutableDictionary *maindict=[mainArray objectAtIndex:i];
        SuggestionObject *suggestionobj=[[SuggestionObject alloc]init];
        suggestionobj.useridstr=[maindict valueForKey:@"userId"];
        suggestionobj.emailstr=[maindict valueForKey:@"email"];
        suggestionobj.followcount=[maindict valueForKey:@"followersCount"];
        suggestionobj.namestr=[maindict valueForKey:@"name"];
        suggestionobj.usernamestr=[maindict valueForKey:@"userName"];
        suggestionobj.profilepicstr=[maindict valueForKey:@"profilepic"];
        suggestionobj.typestr=[maindict valueForKey:@"type"];
        suggestionobj.socialidstr=[maindict valueForKey:@"socialId"];
        [APPDELEGATE.userlistArray addObject:suggestionobj];
    }
}
-(void)internetConnection
{
    UIAlertView *Internetalert=[[UIAlertView alloc]initWithTitle:appname message:@"Internet connection not available" delegate:self cancelButtonTitle:nil otherButtonTitles:@"Ok", nil];
    [Internetalert show];
    self.view .userInteractionEnabled=YES;
    [APPDELEGATE stopspinner:self.view];
    
}
@end
