//
//  ItemsModelobject.h
//  MarkidPlace
//
//  Created by Stellent Software on 5/28/14.
//  Copyright (c) 2014 stellentmac1. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "OrderModelObject.h"

@interface ItemsModelobject : NSObject

@property (nonatomic,strong)NSString *itemNamestr;

@property (nonatomic,strong)NSString *sellnamestr;
@property (nonatomic,strong)NSString *itemnumstr;
@property (nonatomic,strong)NSString *listingcoststr;
@property (nonatomic,strong)NSString *salecostcoststr;
@property (nonatomic,strong)NSString *sizestr;
@property (nonatomic,strong)NSString *brandstr;
@property (nonatomic,strong)NSString *catogrystr;
//@property(nonatomic,readwrite)BOOL likeStatus;

@property (nonatomic,strong)NSString *catogryIdstr;

@property (nonatomic,strong)NSString *conditionstr;

@property (nonatomic,strong)NSString *descriptionstr;
@property (nonatomic,readwrite)BOOL   statusbool;
@property (nonatomic,strong)NSString *itemidstr;
@property (nonatomic,strong)NSString *userlistingidstr;
@property (nonatomic,strong)NSString *unitprisestr;

@property (nonatomic,strong)NSString *useridstr;

@property(nonatomic ,strong)NSMutableArray *picturearray;

@property (nonatomic,readwrite)BOOL   likedbool;

@property (nonatomic,strong)NSString *sellernamestr;

@property (nonatomic,readwrite)BOOL   forsalebool;

@property (nonatomic,strong)NSString *itemStatus;


@property (nonatomic,readwrite)BOOL   alteraddressbool;

@property (nonatomic,strong)NSString *orderstatusstr;

@property (nonatomic,strong)NSString *usernamestring;
@property(nonatomic,strong)NSString *namestr;



@property(nonatomic,strong)OrderModelObject *orderDetailsobj;


@property(nonatomic,strong)NSMutableDictionary *alternadeaddressdict;
/// profile address
@property (nonatomic,strong)NSString *citystring;
@property (nonatomic,strong)NSString *statestr;
@property (nonatomic,strong)NSString *profilepicstr;

// weight
@property (nonatomic,strong)NSString *weightstring;
@property (nonatomic,strong)NSString *genderstring;

@property (nonatomic,readwrite)BOOL   inpersonbool;
@property (nonatomic,readwrite)BOOL   uspsbool;

@property (nonatomic,strong)NSString *totalpricestr;

//Krish

@property (nonatomic,strong)NSString *shippingChargesStr;

@end
