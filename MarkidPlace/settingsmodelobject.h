//
//  settingsmodelobject.h
//  MarkidPlace
//
//  Created by Varma Bhupatiraju on 22/09/14.
//  Copyright (c) 2014 stellentmac1. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface settingsmodelobject : NSObject
@property(nonatomic,strong)NSString *namestr;
@property(nonatomic,strong)NSString *brandname;
@property(nonatomic,strong)NSString *brandid;
@property(nonatomic,readwrite)BOOL isCheckMark;
@end
