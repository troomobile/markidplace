//
//  MyAdressViewController.h
//  MarkidPlace
//
//  Created by Varma Bhupatiraju on 17/09/14.
//  Copyright (c) 2014 stellentmac1. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "myAddressCustomcell.h"
#import "ProfileRepository.h"
#import "myaddressmodelobject.h"
@class AppDelegate;

@interface MyAdressViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,UIScrollViewDelegate,UITextFieldDelegate,HelperProtocol>
{
 
    AppDelegate*appdelegate;
    UITextField *currentTextField;
    myaddressmodelobject *obj;
   
   ProfileRepository *profileRepos;
}

@property(nonatomic,strong) id<HelperProtocol>delegate;
@property(nonatomic,strong) UITableView *mytableview;
@property(nonatomic,strong) NSMutableArray *firstsectionarray;
@property(nonatomic,strong)NSMutableArray *secondsectionarray;
@property(nonatomic,strong)NSMutableArray *thirdsectionarray;
@property(nonatomic,strong)IBOutlet myAddressCustomcell *myaddcustomcell;
@property (nonatomic,strong) IBOutlet UINavigationBar *navbar;
@property(nonatomic,strong) IBOutlet UIButton *Updatebtn;
@property (nonatomic,strong) IBOutlet UITableView *myaddresstableview;
@property(nonatomic,strong) IBOutlet UITextField *mytextfield;
@property(nonatomic,strong) IBOutlet UILabel *titlelbl;
@property(nonatomic,strong) IBOutlet NSMutableArray *addressarray;
-(IBAction)backBtn:(id)sender;
-(IBAction)homeBtn:(id)sender;
-(IBAction)updatebuttonaction:(id)sender;
@end
