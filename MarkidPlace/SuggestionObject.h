//
//  SuggestionObject.h
//  MarkidPlace
//
//  Created by stellent_mac_05 on 26/09/14.
//  Copyright (c) 2014 stellentmac1. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SuggestionObject : NSObject

@property (nonatomic,strong)NSString *useridstr;
@property (nonatomic,strong)NSString *profilepicstr;
@property (nonatomic,strong)NSString *emailstr;
@property (nonatomic,strong)NSString *typestr;
@property (nonatomic,strong)NSString *usernamestr;
@property (nonatomic,strong)NSString *namestr;
@property (nonatomic,strong)NSString *followcount;

@property (nonatomic,strong)NSString *socialidstr;





@end
