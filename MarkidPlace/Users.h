//
//  Users.h
//  MarkidPlace
//
//  Created by Varma Bhupatiraju on 5/31/14.
//  Copyright (c) 2014 stellentmac1. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Users : NSObject
@property(nonatomic)  NSString *userIdStr;
@property(nonatomic)  NSString *userNameStr;
@property(nonatomic)  NSString *emailStr;
@property(nonatomic)  NSString *profilepicStr;
@property(nonatomic)  NSString *passwordStr;
@property(nonatomic)  NSString *createdDateStr;
@property(nonatomic)  NSString *loginUserIdStr;
@property(nonatomic)  NSString *itemsCountStr;
@property(nonatomic)  NSString *wishListCountStr;
@property(nonatomic)  NSString *followerCountStr;
@property(nonatomic)  NSString *followingCountStr;
@property(nonatomic)  NSString *isFollowStr;
@property(nonatomic)  NSMutableArray *userListingArray;
@property(nonatomic)  NSMutableArray *usersWishArray;
@property(nonatomic) NSString *nameStr;
@property(nonatomic) NSString *paypalAddress;
@property(nonatomic,strong) NSString *address1String;
@property(nonatomic,strong) NSString *address2String;
@property(nonatomic,strong) NSString *cityString;
@property(nonatomic,strong) NSString *stateString;
@property(nonatomic,strong) NSString *zipcodeString;
@property(nonatomic,strong) NSString *notificationcountstr;
@property(nonatomic,strong) NSString *messagecountstr;
@property(nonatomic,readwrite) BOOL isFollow;

@end
