//
//  OrderViewController.h
//  MarkidPlace
//
//  Created by Varma Bhupatiraju on 10/06/14.
//  Copyright (c) 2014 stellentmac1. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OrderViewController.h"
#import"AppDelegate.h"
#import "OrderModelObject.h"
@class AppDelegate;
@interface OrderViewController : UIViewController
{
    CGRect screenSize;
    CGFloat costval;
    CGFloat itemcostval;
    CGFloat shipcostval;

  //  AppDelegate *appdelegate;
}
@property (nonatomic,strong) IBOutlet UINavigationBar *navbar;
@property (nonatomic,strong) IBOutlet UIImageView *itemimgview;
@property (nonatomic,strong) IBOutlet UILabel *titlelbl;
@property (nonatomic,strong) IBOutlet UILabel *orderdatelbl;
@property (nonatomic,strong) IBOutlet UILabel *orderfrmlbl;
@property (nonatomic,strong) IBOutlet UILabel *termannlabl;
@property (nonatomic,strong) IBOutlet UILabel *itemdetailslbl;
@property (nonatomic,strong) IBOutlet UILabel *babyshoeslbl;
@property (nonatomic,strong) IBOutlet UILabel *sizelbl;
@property (nonatomic,strong) IBOutlet UILabel *dallerlbl;
@property (nonatomic,strong) IBOutlet UILabel *shippingcostlbl;

@property (strong, nonatomic) IBOutlet UILabel *dalleronelbl;

@property (strong, nonatomic) IBOutlet UILabel *purchasecostlbl;
@property (strong, nonatomic) IBOutlet UILabel *shippingchargeslbl;

@property (strong, nonatomic) IBOutlet UILabel *shipingoptionlbl;

@property (nonatomic,strong) IBOutlet UILabel *statuslabl;
@property (nonatomic,strong) IBOutlet UILabel *shippedlbl;
@property (nonatomic,strong) IBOutlet UILabel *shippedtolbl;
@property (nonatomic,strong) IBOutlet UILabel *anystreetlbl;
@property (nonatomic,strong) IBOutlet UILabel *anywherelbl;
@property (nonatomic,strong) IBOutlet UILabel *sizenumlbl;

@property (nonatomic,strong) IBOutlet UILabel *datelbl;
@property (nonatomic,strong) IBOutlet UILabel *addresslbl;
@property (nonatomic,strong)  ItemsModelobject *itemsobjmodel;

-(IBAction)backAction:(id)sender;
-(IBAction)homeBtnAction:(id)sender;
@end
