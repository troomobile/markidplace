//
//  NotificationMiodelObject.h
//  MarkidPlace
//
//  Created by Varma Bhupatiraju on 12/08/14.
//  Copyright (c) 2014 stellentmac1. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NotificationMiodelObject : NSObject

@property(nonatomic,strong)NSString *senderidstr;
@property(nonatomic,strong)NSString *useridstr;

@property(nonatomic,strong)NSString *itemidstr;
@property(nonatomic,strong)NSString *pushmsgstr;
@property(nonatomic,strong)NSString *imagestr;
@property(nonatomic,strong)NSString *notificationid;
@property(nonatomic,strong)NSString *countstr;
@property(nonatomic,strong)NSString *timestr;
@property(nonatomic,strong)NSString *notificationtype;

@property(nonatomic,strong)NSString *userNameStr;
@property(nonatomic,strong)NSString *nameStr;
@property(nonatomic,strong)NSString *profilepicStr;


@end
