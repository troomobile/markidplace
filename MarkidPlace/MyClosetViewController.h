//
//  SuggestionsViewController.h
//  MarkidPlace
//
//  Created by stellentmac1 on 5/21/14.
//  Copyright (c) 2014 stellentmac1. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "RegistrationRepository.h"
#import "ItemsModelobject.h"
#import "NewListingViewController.h"
#import "MyWishlistViewController.h"
#import "MyFeedViewController.h"
#import "HomeViewController.h"
#import "ODRefreshControl.h"
#import "NotificationViewController.h"

@class RegistrationRepository;
@class AppDelegate;
@interface MyClosetViewController : UIViewController<UIScrollViewDelegate,HelperProtocol>
{
     // AppDelegate *appdelegate;
    CGRect screenSize;
    BOOL ismenu;
    int delaytime;

        RegistrationRepository *registrationRep;
    
    NSMutableArray *itemsarray;
    
    int likeindex;
    
    NSMutableArray *prevlikedarray;

     IBOutlet UILabel *titlelbl;
    
    IBOutlet UIImageView *myaccountImgView;
    IBOutlet UIImageView *myclosetImgView;
    IBOutlet UIImageView *myfeedImgView;
    IBOutlet UIImageView *mywishlistImgView;
    IBOutlet UIImageView *shopAllImgView;
    IBOutlet UIImageView *sellImgView;
      ODRefreshControl *refreshControl;
    NSTimer*timer;

    
}
@property (nonatomic,strong) IBOutlet UIImageView *myfeedImageview;
@property (nonatomic,strong) IBOutlet UIImageView *mySuggestionsImageview;
@property (nonatomic,strong) IBOutlet UIImageView *filterImageview;

@property (nonatomic,strong) IBOutlet UIScrollView *itemscrollview;
@property (nonatomic,strong) IBOutlet UINavigationBar *navbar;
@property (nonatomic,strong) IBOutlet UILabel *itemLabel1;
@property (nonatomic,strong) IBOutlet UILabel *itemLabel2;
@property (nonatomic,strong) IBOutlet UILabel *itempriceLabel1;
@property (nonatomic,strong) IBOutlet UILabel *itempriceLabel2;
@property (nonatomic,strong) IBOutlet UILabel *oldpriceLabel1;
@property (nonatomic,strong) IBOutlet UILabel *oldpriceLabel2;
@property (nonatomic,strong) IBOutlet UILabel *myAccountLabel;
@property (nonatomic,strong) IBOutlet UILabel *myClosetLabel;
@property (nonatomic,strong) IBOutlet UILabel *myfeedLabel;
@property (nonatomic,strong) IBOutlet UILabel *mywishlistLabel;
@property (nonatomic,strong) IBOutlet UILabel *shopallLabel;
@property (nonatomic,strong) IBOutlet UILabel *sellLabel;
@property (nonatomic,strong) IBOutlet UILabel *cartcountLabel;
@property (nonatomic,strong) IBOutlet UILabel *notificationcountlabel;
@property (nonatomic,strong) IBOutlet UIView *menubtnview;
@property (nonatomic,strong) IBOutlet UIView *notificationview;
@property (nonatomic,strong) IBOutlet UIImageView *animationimgv;

@property (nonatomic,strong) IBOutlet UIView *menuview;
@property (nonatomic,strong) IBOutlet UIButton *menuBtn;

-(IBAction)myFeedButtonTapped:(id)sender;
-(IBAction)mySuggestionsButtonTapped:(id)sender;
-(IBAction)filterButtonTapped:(id)sender;

-(IBAction)menuBtnAction:(id)sender;

-(IBAction)backAction:(id)sender;
-(IBAction)homeBtnAction:(id)sender;

-(IBAction)myAccountAction:(id)sender;
-(IBAction)listDetailsAction:(id)sender;
-(IBAction)notificationcount:(id)sender;




@property (nonatomic,strong) IBOutlet UIView *cartview;

@end
