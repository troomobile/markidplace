//
//  FollowersViewController.h
//  MarkidPlace
//
//  Created by stellent on 03/06/14.
//  Copyright (c) 2014 stellentmac1. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FollowersTableViewCell.h"
#import "RegistrationRepository.h"
#import "AppDelegate.h"
#import "ProfileRepository.h"

@class ProfileRepository;
@interface FollowersViewController : UIViewController<HelperProtocol>
{
    RegistrationRepository *registrationRep;
  //  AppDelegate *appdelegate;
    
    
    NSMutableArray *followingArray;
}

@property(nonatomic,strong)NSString *selleruseridstr;

@property(nonatomic,readwrite) BOOL issellerprofile;

@property (nonatomic) ProfileRepository *profRepo;
@property(nonatomic,strong)IBOutlet FollowersTableViewCell *tblcell_followers;
@property(nonatomic,strong)NSMutableArray *array_followers;
@property(nonatomic,strong)IBOutlet UITableView *tblview_followers;
@property (nonatomic,strong) IBOutlet UINavigationBar *navbar;
@property(nonatomic,readwrite)BOOL isfollowers;

@property(nonatomic,strong) IBOutlet UILabel *viewtitlelbl;
-(IBAction)backAction:(id)sender;
-(IBAction)followAction:(id)sender;
@end
