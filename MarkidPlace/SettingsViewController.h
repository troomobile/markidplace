//
//  SettingsViewController.h
//  MarkidPlace
//
//  Created by stellentmac1 on 5/31/14.
//  Copyright (c) 2014 stellentmac1. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EditProfileViewController.h"
#import "AppDelegate.h"
#import "SettingsCell.h"
#import "InviteFriendViewController.h"
#import "BalanceViewController.h"
#import "NotificationSettingsViewController.h"
#import "ProfileRepository.h"
#import "HelperProtocol.h"
#import "FindFriendsViewController.h"
#import "SettingsRepository.h"
#import "AddFriendsViewController.h"

@class AppDelegate;
@class FindFriendsViewController;
@interface SettingsViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,UIAlertViewDelegate,UIScrollViewDelegate,UITextFieldDelegate,HelperProtocol>
{
    CGRect screenSize;
    ProfileRepository *profilerepo;
   AppDelegate*appdelegate;
}

@property(nonatomic,strong) IBOutlet UITableView *settingsTableView;
@property(nonatomic,strong) UIButton *loginbtn;
@property(nonatomic,strong) NSMutableArray *optionsArray;
@property(nonatomic,strong) NSMutableArray *editArray;
@property(nonatomic,strong) NSMutableArray *section3Array;
@property(nonatomic,strong) IBOutlet SettingsCell *settingscell;

@property (nonatomic,strong) IBOutlet UINavigationBar *navbar;
@property(nonatomic,strong) IBOutlet UILabel *viewtitlelbl;
@property (nonatomic,strong)id<HelperProtocol>delegate;

@property(nonatomic,strong) NSMutableArray *brandnamesArray;
@property(nonatomic,strong) NSMutableArray *zerosectionArray;
@property(nonatomic,strong) NSMutableArray *firstsectionArray;
@property(nonatomic,strong) NSMutableArray *secondsectionArray;
@property(nonatomic,strong) NSMutableArray *thirdsectionArray;
@property(nonatomic,strong) NSMutableArray *fourthsectionArray;
@property(nonatomic,strong) NSMutableArray *fivethsectionArray;
@property(nonatomic,strong) NSMutableArray *sixthsectionArray;
@property(nonatomic,strong)IBOutlet UIScrollView *myscrollview;
@property(nonatomic,strong)IBOutlet UIView *popview;
@property(nonatomic,strong)IBOutlet UITableView *dropdowntblview;
@property(nonatomic,strong) NSMutableArray *brandarray;
@property(nonatomic,strong) NSMutableArray *mybrandsarray;

@property(nonatomic,strong)IBOutlet UITextField *myaddresstextfiled;
-(IBAction)LogoutAction:(id)sender;
-(IBAction)DoneAction:(id)sender;
-(IBAction)CancelAction:(id)sender;
@end
