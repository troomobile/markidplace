//
//  BalanceCell.h
//  MarkidPlace
//
//  Created by stellent_mac_05 on 17/09/14.
//  Copyright (c) 2014 stellentmac1. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BalanceCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *balancelbl;
@property (strong, nonatomic) IBOutlet UILabel *balancecountlbl;

@end
