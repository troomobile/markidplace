//
//  Person.h
//  AddressBookDemo
//
//  Created by Arthur Knopper on 24/10/12.
//  Copyright (c) 2012 iOSCreator. All rights reserved.
//

#import <Foundation/Foundation.h>
//#import <Parse/Parse.h>

@interface Person : NSObject

@property (nonatomic, strong) NSString *firstName;
@property (nonatomic, strong) NSString *lastName;
@property (nonatomic, strong) NSString *fullName;
@property (nonatomic, strong) NSString *homeEmail;
@property (nonatomic, strong) NSString *workEmail;
@property (nonatomic, strong) NSString *internetEmail;

@property(nonatomic,strong)NSString *phonenumberstring;

@property (nonatomic, strong) NSString *userid;
//@property   (nonatomic, strong)PFObject *tvuserobj;

@property   (nonatomic, strong)NSMutableArray *contactEmailsArray;
@property   (nonatomic, strong)NSMutableArray *mutiplecontactsArray;
@property   (nonatomic, strong)NSMutableArray *contactphonenoArray;
@property(nonatomic,readwrite)BOOL invitenool;

@property (nonatomic, strong) NSString *contactstr;

@property (nonatomic, strong) NSString *recordId;

@end
