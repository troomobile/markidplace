//
//  MyAdressViewController.m
//  MarkidPlace
//
//  Created by Varma Bhupatiraju on 17/09/14.
//  Copyright (c) 2014 stellentmac1. All rights reserved.
//

#import "MyAdressViewController.h"
#import "SuggestionsViewController.h"
#import "myaddressmodelobject.h"
@interface MyAdressViewController ()

@end

@implementation MyAdressViewController
@synthesize mytableview,firstsectionarray,secondsectionarray,myaddcustomcell,navbar,thirdsectionarray,myaddresstableview,Updatebtn,titlelbl,addressarray;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}
-(IBAction)backBtn:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
    
}

-(IBAction)homeBtn:(id)sender
{
    
    NSArray *arr=self.navigationController.viewControllers;
    for (UIViewController *childvc in arr)
    {
        
        if([childvc isKindOfClass:[SuggestionsViewController class]])
        {
            SuggestionsViewController *subvc=(SuggestionsViewController *)childvc;
            subvc.ismyfeed=NO;
            [self.navigationController popToViewController:subvc animated:YES];
        }
    }
    
}


- (void)viewDidLoad
{
    [super viewDidLoad];
   [myappDelegate startspinner:self.view];
    addressarray=[[NSMutableArray alloc]init];
    profileRepos=[[ProfileRepository alloc]init];
    profileRepos.delegate=self;
   // appdelegate =(AppDelegate *)[[UIApplication sharedApplication]delegate];
    // appdelegate=(AppDelegate *)[[UIApplication sharedApplication]delegate];
    self.navbar.frame = CGRectMake(0, 0, 320, 64);
    [self.navbar setBackgroundImage:[UIImage imageNamed:@"navbar.png"] forBarMetrics:UIBarMetricsDefault];
      self.titlelbl.font = [UIFont fontWithName:@"Monroe" size:18];
    firstsectionarray=[[NSMutableArray alloc]initWithObjects:@"Address 1",@"Address 2",@"City",@"State",@"Zipcode" ,nil];
    secondsectionarray=[[NSMutableArray alloc]initWithObjects:@"Address 1",@"Address 2",@"City",@"State",@"Zipcode", nil];
    thirdsectionarray=[[NSMutableArray alloc]initWithObjects:@"Update", nil];
    NSString *userid=[myappDelegate getStringFromPlist:@"userid"];
    [profileRepos myaddressgetuserid:userid];
    //[myappDelegate stopspinner:self.view];
    [myaddresstableview reloadData];
 
    // Do any additional setup after loading the view from its nib.
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    //return 4;
 
    return 3;

}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if (section==0)
    {
        
        UIView *sectionview=[[UIView alloc] initWithFrame:CGRectMake(0,0,320,20)];
        //sectionview.backgroundColor=[UIColor whiteColor];
        
        UILabel *lbl=[[UILabel alloc] initWithFrame:CGRectMake(0,0,320,25)];
        
        [sectionview addSubview:lbl];
        lbl.font = [UIFont fontWithName:@"Muli" size:16];
        
    
        lbl.text=@"    Default Shipping Address";
        lbl.textColor=[UIColor colorWithRed:(131/255.f) green:(131/255.f) blue:(131/255.f) alpha:1.0];
        lbl.backgroundColor=[UIColor colorWithRed:(238/255.f) green:(238/255.f) blue:(238/255.f) alpha:1.0];
        return sectionview;
        
    }
    
    else if(section==1)
    {
        
        UIView *sectionview=[[UIView alloc] initWithFrame:CGRectMake(0,0,320,30)];
        //sectionview.backgroundColor=[UIColor whiteColor];
        
        UILabel *lbl=[[UILabel alloc] initWithFrame:CGRectMake(0,0,320,25)];
        
        [sectionview addSubview:lbl];
        lbl.font = [UIFont fontWithName:@"Muli" size:16];
        
        
        lbl.text=@"    Default Return Address";
        lbl.textColor=[UIColor colorWithRed:(131/255.f) green:(131/255.f) blue:(131/255.f) alpha:1.0];
        lbl.backgroundColor=[UIColor colorWithRed:(238/255.f) green:(238/255.f) blue:(238/255.f) alpha:1.0];
        return sectionview;
        
    }

    
    return nil;
}
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (scrollView==myaddresstableview)
    {
        CGFloat sectionHeaderHeight = 30;
        if (scrollView.contentOffset.y<=sectionHeaderHeight&&scrollView.contentOffset.y>=0) {
            scrollView.contentInset = UIEdgeInsetsMake(-scrollView.contentOffset.y, 0, 0, 0);
        }
        else if (scrollView.contentOffset.y>=sectionHeaderHeight)
        {
            scrollView.contentInset = UIEdgeInsetsMake(-sectionHeaderHeight, 0, 0, 0);
        }
    }
    else
    {
        CGFloat sectionHeaderHeight = 0;
        if (scrollView.contentOffset.y<=sectionHeaderHeight&&scrollView.contentOffset.y>=0) {
            scrollView.contentInset = UIEdgeInsetsMake(-scrollView.contentOffset.y, 0, 0, 0);
        }
        else if (scrollView.contentOffset.y>=sectionHeaderHeight)
        {
            scrollView.contentInset = UIEdgeInsetsMake(-sectionHeaderHeight, 0, 0, 0);
        }
        //       else if ((dropdownview.contentOffset.y <0))
        //        {
        //            [dropdownview setScrollEnabled:NO];
        //
        //        }
    }
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
   
    
        if (section==0)
        {
            return firstsectionarray.count;
        }
        else if (section==1)
        {
            return secondsectionarray.count;
        }
    else if (section==2)
        {
            return thirdsectionarray.count;
        
        }
        else
        {
            return 1;
        }
    
    
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    
        if(section==0)
        {
            return 20;
        }
        else
        {
            return 20;
        }
    
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
   
        static NSString *cellIdentifier=@"acell";
        myAddressCustomcell *tbcell =(myAddressCustomcell*)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        if (tbcell==nil)
        {
            [[NSBundle mainBundle] loadNibNamed:@"myAddressCustomcell" owner:self options:nil];
            tbcell=myaddcustomcell;
            tbcell.selectionStyle=UITableViewCellSelectionStyleNone;
        }
    
        tbcell.namelbl.font = [UIFont fontWithName:@"Muli" size:14];
     tbcell.addresstxtfield.font = [UIFont fontWithName:@"Muli" size:15];
    //tbcell.addresstxtfield.tag=(indexPath.section+1)*indexPath.row;
    tbcell.addresstxtfield.tag=(5*(indexPath.section))+indexPath.row;
  // obj=[addressarray objectAtIndex:0];
        if (indexPath.section==0)
        {
            tbcell.namelbl.text=[self.firstsectionarray objectAtIndex:indexPath.row];
            if (indexPath.row==0)
            {
                tbcell.addresstxtfield.text=obj.address1str;
                
            }
            else if (indexPath.row==1)
            {
               
                tbcell.addresstxtfield.text=obj.address2str;
                
            }
            else if (indexPath.row==2)
            {
                tbcell.addresstxtfield.text=obj.citystr;
                
            }
            else if (indexPath.row==3)
            {
               tbcell.addresstxtfield.text=obj.statestr;
                
            }
            else
            {
                 tbcell.addresstxtfield.text=obj.zipstr;
            }

          
            
        }
        else if (indexPath.section==1)
        {
            tbcell.namelbl.text=[self.secondsectionarray objectAtIndex:indexPath.row];
            tbcell.userInteractionEnabled=NO;
            
        }
    else if (indexPath.section==2)
    {
        tbcell.namelbl.hidden=YES;
        tbcell.lineimg.hidden=YES;
        tbcell.updatebtn.hidden=NO;
    
        tbcell.updatebtn.backgroundColor=[UIColor whiteColor];
        tbcell.updatebtn.titleLabel.font=[UIFont fontWithName:@"Muli" size:16];
        tbcell. updatebtn.layer.cornerRadius=5;
        tbcell. updatebtn.layer.masksToBounds=YES;
         tbcell.updatebtn.layer.borderWidth=1;
         tbcell.updatebtn.layer.borderColor=[[UIColor colorWithRed:12.0/255.0 green:183.0/255.0 blue:174.0/255.0 alpha:1.0]CGColor];
        [ tbcell.updatebtn setTitleColor:[UIColor colorWithRed:12.0/255.0 green:183.0/255.0 blue:174.0/255.0 alpha:1.0] forState:UIControlStateNormal];
        tbcell.backgroundColor=[UIColor clearColor];
        tbcell.updatebtn.titleLabel.font = [UIFont fontWithName:@"Muli" size:14];
        //tbcell.namelbl.text=[self.thirdsectionarray objectAtIndex:indexPath.row];
    }
    return tbcell;
}

-(IBAction)updatebuttonaction:(id)sender
{
    [myappDelegate startspinner:self.view];
    [currentTextField resignFirstResponder];
    //NSString *userid=[myappDelegate getStringFromPlist:@"userid"];
    NSIndexPath *indexpath1=[NSIndexPath indexPathForRow:0 inSection:0];
    NSIndexPath *indexpath2=[NSIndexPath indexPathForRow:1 inSection:0];
    NSIndexPath *indexpath3=[NSIndexPath indexPathForRow:2 inSection:0];
     NSIndexPath *indexpath4=[NSIndexPath indexPathForRow:3 inSection:0];
    NSIndexPath *indexpath5=[NSIndexPath indexPathForRow:4 inSection:0];
    
    myAddressCustomcell *myaddresscell1=(myAddressCustomcell *)[myaddresstableview cellForRowAtIndexPath:indexpath1];
    NSLog(@"myaddress1....%@",myaddresscell1.addresstxtfield.text);
    
    myAddressCustomcell *myaddresscell2=(myAddressCustomcell *)[myaddresstableview cellForRowAtIndexPath:indexpath2];
      NSLog(@"myaddress2.....%@",myaddresscell2.addresstxtfield.text);
     myAddressCustomcell *myaddresscell3=(myAddressCustomcell *)[myaddresstableview cellForRowAtIndexPath:indexpath3];
     NSLog(@"city.....%@",myaddresscell3.addresstxtfield.text);
     myAddressCustomcell *myaddresscell4=(myAddressCustomcell *)[myaddresstableview cellForRowAtIndexPath:indexpath4];
    NSLog(@"state.....%@",myaddresscell4.addresstxtfield.text);
     myAddressCustomcell *myaddresscell5=(myAddressCustomcell *)[myaddresstableview cellForRowAtIndexPath:indexpath5];
    NSLog(@"zipcode.....%@",myaddresscell5.addresstxtfield.text);
    
   
//    if ([myaddresscell5.addresstxtfield.text length]>5)
//    {
//       UIAlertView *myalert=[[UIAlertView alloc]initWithTitle:appname message:@"Below 5 charcaters only" delegate:nil cancelButtonTitle:@"ok" otherButtonTitles: nil];
//       [myalert show];
//    }
    //else
    //{
     NSString *userid=[myappDelegate getStringFromPlist:@"userid"];
    NSMutableDictionary *updic=[[NSMutableDictionary alloc]init];
    
    [updic setValue:userid forKey:@"UserId"];
    [updic setValue:myaddresscell1.addresstxtfield.text forKey:@"Address1"];
    [updic setValue:myaddresscell2.addresstxtfield.text forKey:@"Address2"];
    [updic setValue:myaddresscell3.addresstxtfield.text forKey:@"City"];
    [updic setValue:myaddresscell4.addresstxtfield.text forKey:@"State"];
    [updic setValue:myaddresscell5.addresstxtfield.text forKey:@"ZipCode"];
   
    [profileRepos updateaddress :userid :updic];
   // }
    
    
}
-(void)updatesuccessResponse:(NSDictionary *)responseDict;
{
    
    NSLog(@"response dict....%@",responseDict);
    if ([[responseDict valueForKey:@"message"] isEqualToString:@"Updated Successfully"])
    {
       //dispatch_async(dispatch_get_main_queue(), ^{
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.001 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
             [myappDelegate stopspinner:self.view];
            UIAlertView *myalert=[[UIAlertView alloc]initWithTitle:appname message:@"Updated Successfully, Thanks For using MarKIDplace" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [myalert show];
            
            
        
            
            
        });
        
    }

    
    
}
-(void)successResponseforProfile:(NSDictionary *)responseDict
{
    dispatch_async(dispatch_get_main_queue(), ^{
   
   [myappDelegate stopspinner:self.view];
    
       NSLog(@"responsedict....%@",responseDict);
    obj=[[myaddressmodelobject alloc]init];
    obj.address1str=[responseDict valueForKey:@"address1"];
     obj.address2str=[responseDict valueForKey:@"address2"];
     obj.citystr=[responseDict valueForKey:@"city"];
     obj.statestr=[responseDict valueForKey:@"state"];
     obj.zipstr=[responseDict valueForKey:@"zipcode"];
    [addressarray addObject:obj];
    
   
    [myaddresstableview reloadData];
    
      //[myappDelegate stopspinner:self.view];
     });
  //  "message":"User Valid" or
  //  "message":"User InActive State" or
   // "message":"Please Send Valid UserInformation"
    
    
    
}
-(void)dismissactivindic :(BOOL)showalert
{
    
    [myappDelegate stopspinner:self.view];
    [myappDelegate stopspinner:self.view];
    if (showalert)
    {
        [AppDelegate alertmethod:appname :@"No data to display" ];
    }
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
   /*
    BOOL didResign = [textField resignFirstResponder];
    if (!didResign) return NO;
    
    NSUInteger index = [self.firstsectionarray indexOfObject:textField];
    if (index == NSNotFound || index + 1 == firstsectionarray.count) return NO;
    
    id nexField = [firstsectionarray objectAtIndex:index + 1];
    textField = nexField;
    [nexField becomeFirstResponder];
    
    return NO;
   
    
   // NSLog(@"return");
   // [textField resignFirstResponder];
   //  myaddresstableview.contentOffset=CGPointMake(0, 0);
    // myaddresstableview.contentSize=CGSizeMake(320, 440);
   // return YES;
    */
    /*
    for (int i=0; i<5; i++)
    {
    NSIndexPath *indexpath1=[NSIndexPath indexPathForRow:i+1 inSection:0];
        
    myAddressCustomcell *myaddresscell2=(myAddressCustomcell *)[myaddresstableview cellForRowAtIndexPath:indexpath1];
        
        [myaddresscell2.addresstxtfield becomeFirstResponder];

         [myaddresstableview reloadData];
        
        
        
    }
    return NO;
    
    
 //
    
    
    // Try to find next responder
   
    /*
     UIResponder* nextResponder = [textField.superview viewWithTag:nextTag];
    if (nextResponder) {
        // Found next responder, so set it.
        [nextResponder becomeFirstResponder];
    }
    else {
        // Not found, so remove keyboard.
        [textField resignFirstResponder];
    }
    return YES;
   
    // We do not want UITextField to insert line-breaks.
     */
    id firstResponderCell = textField.superview.superview ;
    NSInteger firstResponderRow = [self.myaddresstableview indexPathForCell:firstResponderCell].row ;
    NSIndexPath* nextCellIndexPath = [NSIndexPath indexPathForRow:firstResponderRow+1 inSection:0] ;
    myAddressCustomcell* nextCell = (myAddressCustomcell*)[self.myaddresstableview cellForRowAtIndexPath:nextCellIndexPath];
    if(firstResponderRow+1==4)
    {
        nextCell.addresstxtfield.returnKeyType = UIReturnKeyDefault;
    }
    
    if (nextCell)
    {
        [nextCell.addresstxtfield becomeFirstResponder] ;
    }
    else
        nextCell.addresstxtfield.returnKeyType = UIReturnKeyDefault;
        [textField resignFirstResponder] ;
    return YES ;


}
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    
   
    return YES;
}
-(void)textFieldDidBeginEditing:(UITextField *)textField
{
  
    NSLog(@"textfiledtag...%d",textField.tag);
    myaddresstableview.contentSize=CGSizeMake(320, 1000);
    myaddresstableview.contentOffset=CGPointMake(0, textField.tag*44);
    
    //myaddresstableview setContentSize:CGSizeMake(320,23)
   // myaddresstableview.contentOffset=CGPointMake(0,180);
   

    
}
-(void)textFieldDidEndEditing:(UITextField *)textField
{
    
    myaddresstableview.contentOffset=CGPointMake(0, 0);
    if (textField.tag==0)
    {
        obj.address1str=textField.text;
    
    }
    else if (textField.tag==1)
    {
         obj.address2str=textField.text;
        
    }
    else if (textField.tag==2)
    {
        obj.citystr=textField.text;
        
    }
    else if (textField.tag==3)
    {
        obj.statestr=textField.text;
        
    }
    else
    {
        obj.zipstr=textField.text;
        
    }
    //[addressarray addObject:obj];
   // [myaddresstableview reloadData];
}
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
     NSIndexPath *indexpath5=[NSIndexPath indexPathForRow:4 inSection:0];
     myAddressCustomcell *myaddresscell5=(myAddressCustomcell *)[myaddresstableview cellForRowAtIndexPath:indexpath5];
    if (textField.tag==4)
    {
        
        NSCharacterSet *cs = [[NSCharacterSet characterSetWithCharactersInString:Numeric] invertedSet];
        
        NSString *filtered = [[string componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
        
        if ([string isEqualToString:filtered]==YES)
        {
            if([string isEqualToString:@""])
            {
                return YES;
            }
            if (myaddresscell5.addresstxtfield.text.length <= 5)
            {
                return YES;
            }
            else
            {
                return NO;
            }
        }
        else
        {
            return NO;
        }
        
        
    }
    
    return YES;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)internetConnection
{
    dispatch_async(dispatch_get_main_queue(), ^{
    UIAlertView *Internetalert=[[UIAlertView alloc]initWithTitle:appname message:@"Internet connection not available" delegate:self cancelButtonTitle:nil otherButtonTitles:@"Ok", nil];
    [Internetalert show];
    [APPDELEGATE stopspinner:self.view];
    });
    
}
@end
