//
//  OrderModelObject.h
//  MarkidPlace
//
//  Created by Stellent Software on 6/17/14.
//  Copyright (c) 2014 stellentmac1. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface OrderModelObject : NSObject


@property(nonatomic,strong) NSString *address1Str;
@property(nonatomic,strong) NSString *address2Str;
@property(nonatomic,strong) NSString *buyerUseridstr;
@property(nonatomic,strong) NSString *citystr;
@property(nonatomic,strong) NSString *orderdatestr;
@property(nonatomic,strong) NSString *orderidstr;
@property(nonatomic,strong) NSString *ordernumberstr;
@property(nonatomic,strong) NSString *orderstatusstr;
@property(nonatomic,strong) NSString *quantitystr;
@property(nonatomic,strong) NSString *sellernamestr;
@property(nonatomic,strong) NSString *statestr;
@property(nonatomic,strong) NSString *useridstr;
@property(nonatomic,strong) NSString *userlistingstr;
@property(nonatomic,strong) NSString *zipcodestr;
@property (nonatomic,strong)NSString *trackingNumberStr;
@end
