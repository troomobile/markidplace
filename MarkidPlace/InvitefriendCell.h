//
//  InvitefriendCell.h
//  MarkidPlace
//
//  Created by stellent_mac_05 on 27/06/14.
//  Copyright (c) 2014 stellentmac1. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface InvitefriendCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *namelbl;
@property (strong, nonatomic) IBOutlet UIImageView *contactimgview;
@property (strong, nonatomic) IBOutlet UIImageView *checkImageView;
@end
