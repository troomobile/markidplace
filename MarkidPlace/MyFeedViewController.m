//
//  SuggestionsViewController.m
//  MarkidPlace
//
//  Created by stellentmac1 on 5/21/14.
//  Copyright (c) 2014 stellentmac1. All rights reserved.
//

#import "MyFeedViewController.h"
#import "ProfileViewController.h"

@interface MyFeedViewController ()

@end

@implementation MyFeedViewController
@synthesize navbar,itemLabel1,itemLabel2,itempriceLabel1,itempriceLabel2,oldpriceLabel1,oldpriceLabel2,myAccountLabel,myClosetLabel,myfeedLabel,mywishlistLabel,shopallLabel,sellLabel,cartcountLabel,notificationcountlabel;
@synthesize menuBtn,menuview,notificationview;
@synthesize itemscrollview,myfeedImageview,mySuggestionsImageview,filterImageview,cartview,animationimgv,menubtnview;



- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
#pragma mark view methods
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
   // myappDelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    
    [myappDelegate horseanimation:animationimgv];
    self.navbar.frame = CGRectMake(0, 0, 320, 64);
    [self.navbar setBackgroundImage:[UIImage imageNamed:@"navbar.png"] forBarMetrics:UIBarMetricsDefault];
    
    itemsarray=[NSMutableArray new];
    [self myFeed];
    
}

-(void)myFeed
{
    self.itemLabel1.font = [UIFont fontWithName:@"Muli" size:13];
    self.itemLabel2.font = [UIFont fontWithName:@"Muli" size:13];
    self.itempriceLabel1.font = [UIFont fontWithName:@"Muli" size:13];
    self.itempriceLabel1.font = [UIFont fontWithName:@"Muli" size:13];
    self.itempriceLabel2.font = [UIFont fontWithName:@"Muli" size:13];
    self.oldpriceLabel1.font = [UIFont fontWithName:@"Muli" size:13];
    self.oldpriceLabel2.font = [UIFont fontWithName:@"Muli" size:13];
    self.myAccountLabel.font = [UIFont fontWithName:@"Muli" size:15];
    self.myClosetLabel.font = [UIFont fontWithName:@"Muli" size:15];
    self.myfeedLabel.font = [UIFont fontWithName:@"Muli" size:15];
    self.mywishlistLabel.font = [UIFont fontWithName:@"Muli" size:15];
    self.shopallLabel.font = [UIFont fontWithName:@"Muli" size:15];
    self.sellLabel.font = [UIFont fontWithName:@"Muli" size:15];
    self.cartcountLabel.font = [UIFont fontWithName:@"Muli" size:11];
    
    refreshControl = [[ODRefreshControl alloc] initInScrollView:self.itemscrollview];
    
    refreshControl.tintColor=[UIColor colorWithRed:12.0/255.0 green:183.0/255.0  blue:170.0/255.0  alpha:1.0];
    [refreshControl addTarget:self action:@selector(dropViewDidBeginRefreshing:) forControlEvents:UIControlEventValueChanged];
    refreshControl.activityIndicatorViewColor=[UIColor colorWithRed:12.0/255.0 green:183.0/255.0  blue:170.0/255.0  alpha:1.0];
    
    
    
    screenSize = [[UIScreen mainScreen]bounds];
    
    if (myappDelegate.isiphone5)
    {
        menubtnview.frame=CGRectMake(131,470, 58, 59);
    }
    else
    {
        menubtnview.frame=CGRectMake(131,355, 58, 59);
    }
    
    
    self.view.userInteractionEnabled=YES;
    
    [self.view addSubview:menuview];
    [self.view addSubview:menubtnview];
    
    itemscrollview.contentSize=CGSizeMake(itemscrollview.frame.size.width,itemscrollview.frame.size.height+100);
    
    myappDelegate.useridstring=[myappDelegate getStringFromPlist:@"userid"];
    
    titlelbl.font = [AppDelegate navtitlefont];
    
    
}


- (void)dropViewDidBeginRefreshing:(ODRefreshControl *)refresh
{
    [myappDelegate.myfeedsGlobalarray removeAllObjects];
    [refreshControl beginRefreshing ];
    [self setMenuDown];
    [self viewWillAppear:NO];
}
-(void)setMenuDown
{
    CGRect menu=menuview.frame;
    CGRect menuButtonframe=menubtnview.frame;
    if (myappDelegate.isiphone5)
    {
        if(ismenu==YES)
        {
            ismenu=NO;
            
            menuButtonframe=CGRectMake(131,470, 58, 59);
            menu=menuview.frame;
            menu.origin.y=menuview.frame.origin.y+600;
            
            self.view.userInteractionEnabled=NO;
        }
        self.view.userInteractionEnabled=YES;
    }
    else
    {
        if(ismenu==YES)
        {
            ismenu=NO;
            
            menuButtonframe=CGRectMake(131,355, 58, 59);
            menu=menuview.frame;
            menu.origin.y=menuview.frame.origin.y+482;
            self.view.userInteractionEnabled=NO;
        }
    }
    [UIView animateWithDuration:0.5 animations:^{
        
        
        menuview.frame=menu;
        menubtnview.frame=menuButtonframe;
        
    } completion:^(BOOL finished) {
        
    }];
    
    self.view.userInteractionEnabled=YES;
    
}
-(void)viewWillAppear:(BOOL)animated
{
   
    registrationRep=[[RegistrationRepository alloc ]init];
    
    registrationRep.delegate=self;
    int value =[[APPDELEGATE getStringFromPlist:@"notificationcount"] intValue];
    NSLog(@"value....%d",value);
    
    if (value>9)
    {
        notificationcountlabel.text=@"9+";
        
    }
    else if (value==0)
    {
        
        notificationview.hidden=YES;
        
    }
    else
    {
        
        notificationcountlabel.text=[APPDELEGATE getStringFromPlist:@"notificationcount"];
    }
    

    [myaccountImgView setImage:[UIImage imageNamed:@"myaccount_off.png"]];
    [myclosetImgView setImage:[UIImage imageNamed:@"mycloset_off.png"]];
    [myfeedImgView setImage:[UIImage imageNamed:@"myfeed_on@2x.png"]];
    [mywishlistImgView setImage:[UIImage imageNamed:@"mywishlist_off.png"]];
    [shopAllImgView setImage:[UIImage imageNamed:@"shopall_off.png"]];
    [sellImgView setImage:[UIImage imageNamed:@"sell_off.png"]];
    
    if (myappDelegate.cartGlobalarray.count>0)
    {
        cartcountLabel.text=[NSString stringWithFormat:@"%d",myappDelegate.cartGlobalarray.count];
        cartview.hidden=NO;
    }
    else
    {
        cartview.hidden=YES;
    }
    
    itemscrollview.hidden=YES;
   
    if (myappDelegate.myfeedsGlobalarray.count<1)
    {
        [myappDelegate startspinner:self.view];
      
        
        [registrationRep getdata:nil :@"getallLikes" :@"GET" withcount:@"0"];
    }
    else
    {
        [self createScrollView];
    }
    
}
-(void)viewDidDisappear:(BOOL)animated
{
    for (ASIHTTPRequest *req in ASIHTTPRequest.sharedQueue.operations)
    {
        [req cancel];
        [req setDelegate:nil];
    }

}
-(void)viewWillDisappear:(BOOL)animated
{/*
    for (ASIHTTPRequest *req in ASIHTTPRequest.sharedQueue.operations)
    {
        [req cancel];
        [req setDelegate:nil];
    }

    NSLog(@" viewWillDisappear:(BOOL)animated");
  */
    itemscrollview.hidden=YES;
    [timer invalidate];
}
#pragma mark Button Actions
//this action performs  navigation back action
-(IBAction)backAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
//here button is performs pop the sugession viewcontroller
-(IBAction)homeBtnAction:(id)sender
{
    NSArray *arr=self.navigationController.viewControllers;
    for (UIViewController *childvc in arr) {
        
        if([childvc isKindOfClass:[SuggestionsViewController class]])
        {
            SuggestionsViewController *subvc=(SuggestionsViewController *)childvc;
            subvc.ismyfeed=NO;
            [self.navigationController popToViewController:subvc animated:YES];
        }
    }
    
}
//this action performs the push the filterview controller
-(IBAction)filterButtonTapped:(id)sender
{
    [self setMenuDown];
    
    if ([sender tag]==330)
    {
        if (myappDelegate.cartGlobalarray.count>0)
        {
            // ff
            CartViewController *cartVC;
            if (myappDelegate.isiphone5)
            {
                cartVC = [[CartViewController alloc]initWithNibName:@"CartViewController" bundle:nil];
            }
            else
            {
                cartVC = [[CartViewController alloc]initWithNibName:@"CartViewController_iphone4" bundle:nil];
            }
            
            ItemsModelobject *sltobj=[myappDelegate.cartGlobalarray objectAtIndex:0];
            cartVC.itemsModelObj=sltobj;
            [self.navigationController pushViewController:cartVC animated:YES];
        }
        else{
            
            [AppDelegate alertmethod:appname :@"Your cart is empty. Please add listing to cart."];
        }
        
    }
    else{
        if (myappDelegate.isiphone5)
        {
            FilterViewController *filterVC=[[FilterViewController alloc] initWithNibName:@"FilterViewController" bundle:nil];
            [self.navigationController pushViewController:filterVC animated:YES];
        }
        else{
            FilterViewController *filterVC=[[FilterViewController alloc] initWithNibName:@"FilterViewController_iphone4" bundle:nil];
            [self.navigationController pushViewController:filterVC animated:YES];
        }
    }
    
}
/************Animation of scroll view********************/

-(void)animateView
{
    
    [UIView animateWithDuration:0.2 animations:^{
        CGRect height=itemscrollview.frame;
        height.origin.y=itemscrollview.frame.origin.y-195;
        [self zoomButton];
        itemscrollview.frame=height;
    }];
    if (myappDelegate.isiphone5)
    {
        itemscrollview.frame=CGRectMake(0, 64, 320, 504);
    }
    else{
        itemscrollview.frame=CGRectMake(0, 64, 320, 416);
    }
}
-(void)zoomButton
{
    CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"transform.scale"];
    // Set the initial and the final values
    [animation setFromValue:[NSNumber numberWithFloat:0.4f]];
    [animation setToValue:[NSNumber numberWithFloat:1.00f]];
    
    // Set duration
    [animation setDuration:0.4f];
    
    // Set animation to be consistent on completion
    [animation setRemovedOnCompletion:NO];
    [animation setFillMode:kCAFillModeForwards];
    
    // Add animation to the view's layer
    [[itemscrollview layer] addAnimation:animation forKey:@"scale"];
}
//this action performs the animation of the menu's
-(IBAction)menuBtnAction:(id)sender
{
   // notificationcountlabel.text=[APPDELEGATE getStringFromPlist:@"notificationcount"];
   // NSLog(@"notificationcountlabel.text....%@",notificationcountlabel.text);
    
    int value =[[APPDELEGATE getStringFromPlist:@"notificationcount"] intValue];
    NSLog(@"value....%d",value);
    if (value>9)
    {
        notificationcountlabel.text=@"9+";
        
    }
    else if (value==0)
    {
        
        notificationview.hidden=YES;
        
    }
    else
    {
        
        notificationcountlabel.text=[APPDELEGATE getStringFromPlist:@"notificationcount"];
    }
    [animationimgv stopAnimating];
    
    animationimgv.image=[UIImage imageNamed:@"hours_0022_Layer-1.png"];
    
    [animationimgv startAnimating];
    CGRect menu=menuview.frame;
    CGRect menuButtonframe=menubtnview.frame;
    if (myappDelegate.isiphone5)
    {
        if(ismenu==YES)
        {
            ismenu=NO;
            
            menu.origin.y=600;
            menuButtonframe.origin.y=470;
        }
        else
        {
            //menuButtonframe=CGRectMake(131, 250, 58, 59);
            
            ismenu=YES;
            
            //menuButtonframe.origin.y=menubtnview.frame.origin.y-250;
            menuButtonframe.origin.y=250;
            menu.origin.y=280;
        }
        
        self.view.userInteractionEnabled=YES;
    }
    else
    {
        if(ismenu==YES)
        {
            ismenu=NO;
            menuButtonframe=CGRectMake(131,355, 58, 59);
            menu.origin.y=menuview.frame.origin.y+482;
        }
        else
        {
            menuButtonframe=CGRectMake(131, 160, 58, 59);
            
            ismenu=YES;
            menu=menuview.frame;
            menu.origin.y=190;
        }
    }
    
    [UIView animateWithDuration:0.5 animations:^
     {
         
         menuview.frame=menu;
         menubtnview.frame=menuButtonframe;
         
         // [self runSpinAnimationOnView:self.menuBtn duration:1.0 rotations:1.0 repeat:0];
     }
                     completion:^(BOOL finished){
                         
                         //[animationimgv stopAnimating];
                         
                         // animationimgv.image=[UIImage imageNamed:@"hours_0022_Layer-1.png"];
                     }];
    
    self.view.userInteractionEnabled=YES;
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 1.0 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
        [animationimgv stopAnimating];
        
        animationimgv.image=[UIImage imageNamed:@"hours_0022_Layer-1.png"];
    });
    
    
}
/*-(IBAction)menuBtnAction:(id)sender
{
    
    CGRect menu;
    CGRect menuButtonframe;
    
    if (myappDelegate.isiphone5)
    {
        if(ismenu==YES)
        {
            ismenu=NO;
            
            menuButtonframe=CGRectMake(131,470, 58, 59);
            menu=menuview.frame;
            menu.origin.y=menuview.frame.origin.y+600;
            
            self.view.userInteractionEnabled=NO;
        }
        else
        {
            
            menuButtonframe=CGRectMake(131, 250, 58, 59);
            ismenu=YES;
            menu=menuview.frame;
            menu.origin.y=280;
            
        }
        self.view.userInteractionEnabled=YES;
        
    }
    else
    {
        if(ismenu==YES)
        {
            
            ismenu=NO;
            
            
            menuButtonframe=CGRectMake(131,355, 58, 59);
            menu=menuview.frame;
            menu.origin.y=menuview.frame.origin.y+482;
            
            self.view.userInteractionEnabled=NO;
            
        }
        else
        {
            
            menuButtonframe=CGRectMake(131, 160, 58, 59);
            ismenu=YES;
            
            menu=menuview.frame;
            menu.origin.y=190;
            
        }
    }
    
    [UIView animateWithDuration:0.5 animations:^{
        
        menuview.frame=menu;
        menuBtn.frame=menuButtonframe;
        
    } completion:^(BOOL finished) {
        
    }];
    
    self.view.userInteractionEnabled=YES;
    
}
 
 */
-(IBAction)likebuttonaction:(id)sender
{
     [myappDelegate.mylikesGlobalarray removeAllObjects];
    
    likeindex=[sender tag];
    ItemsModelobject *itemobj=[itemsarray objectAtIndex:[sender tag]];
    
    
    if (itemobj.likedbool==YES)
    {
        itemobj.likedbool=NO;
        
        [myappDelegate.allLikesGlobalarray removeObject:[NSString stringWithFormat:@"%@",itemobj.userlistingidstr ]];
        
    }
    else
    {
        [myappDelegate.allLikesGlobalarray addObject:[NSString stringWithFormat:@"%@",itemobj.userlistingidstr ]];
        itemobj.likedbool=YES;
    }
    
    
    BOOL breakbool=NO;
    for (int i=0; i<itemscrollview.subviews.count; i++)
    {
        UIView *myv=[itemscrollview.subviews objectAtIndex:i];
        
        if (myv.tag==likeindex)
        {
            if ([myv isKindOfClass:[UIView class]])
            {
                for (UIView *temv in myv.subviews)
                {
                    if ([temv isKindOfClass:[UIButton class]])
                    {
                        UIButton *likebtn=(UIButton *)temv;
                        if (likebtn.frame.size.width==23)
                        {
                            if (itemobj.likedbool==YES)
                            {
                                
                                
                                [likebtn setImage:[UIImage imageNamed:@"like_on.png"] forState:UIControlStateNormal];
                            }
                            else{
                                [likebtn setImage:[UIImage imageNamed:@"like_off.png"] forState:UIControlStateNormal];
                                
                                
                            }
                            
                            breakbool=YES;
                            break;
                        }
                    }
                }
                
                if (breakbool==YES)
                {
                    break;
                }
            }
            
            
        }
    }
    
    
    
    NSString *userid=[APPDELEGATE getStringFromPlist:@"userid"];

    
    NSDictionary *likedict=[[NSDictionary alloc]initWithObjectsAndKeys:userid,@"UserID",itemobj.userlistingidstr,@"UserListingID", nil];
    
    NSLog(@"chekc the like dict tag of sender %d-=-=-=%@",likeindex, likedict);
    registrationRep.delegate=self;
    [registrationRep getdata:likedict :@"LikeItem" :@"POST" withcount:@"0"];
   
    
}

//this action performs push the profile view controller
-(IBAction)myAccountAction:(id)sender
{
    [self setMenuDown];
    ProfileViewController *profileVC;
    if (myappDelegate.isiphone5)
    {
        profileVC = [[ProfileViewController alloc]initWithNibName:@"ProfileViewController" bundle:nil];
    }
    else
    {
        profileVC = [[ProfileViewController alloc]initWithNibName:@"ProfileViewController_iphone4" bundle:nil];
    }
    [self.navigationController pushViewController:profileVC animated:YES];
}

//this action performs  push the listviewcontroller
-(IBAction)listDetailsAction:(id)sender
{
    [self setMenuDown];
    ListingDetailViewController *listingDetailVC;
    if (myappDelegate.isiphone5)
    {
        listingDetailVC = [[ListingDetailViewController alloc]initWithNibName:@"ListingDetailViewController" bundle:nil];
    }
    else
    {
        listingDetailVC = [[ListingDetailViewController alloc]initWithNibName:@"ListingDetailViewController_iphone4" bundle:nil];
    }
    
    listingDetailVC.listingmainarray=itemsarray;
    listingDetailVC.senderIndex=[sender tag];
    listingDetailVC.condstring=@"";
    [self.navigationController pushViewController:listingDetailVC animated:YES];
}
//this view controller push the profile view controller
-(IBAction)menuItemTapped:(id)sender
{
    
    [self setMenuDown];
    switch ([sender tag]) {
        case 1:
        {
            BOOL isVCExist=NO;
            NSArray *arr=self.navigationController.viewControllers;
            for (UIViewController *childvc in arr) {
                
                if([childvc isKindOfClass:[ProfileViewController class]])
                {
                    isVCExist=YES;
                    UIViewController *subvc=(ProfileViewController *)childvc;
                    [self.navigationController popToViewController:subvc animated:YES];
                }
            }
            
            if(!isVCExist)
            {
                ProfileViewController *profileVC;
                if (myappDelegate.isiphone5)
                {
                    profileVC = [[ProfileViewController alloc]initWithNibName:@"ProfileViewController" bundle:nil];
                }
                else
                {
                    profileVC = [[ProfileViewController alloc]initWithNibName:@"ProfileViewController_iphone4" bundle:nil];
                }
                [self.navigationController pushViewController:profileVC animated:YES];
            }
        }
            break;
        case 2:
        {
            BOOL isVCExist=NO;
            NSArray *arr=self.navigationController.viewControllers;
            for (UIViewController *childvc in arr) {
                
                if([childvc isKindOfClass:[MyClosetViewController class]])
                {
                    isVCExist=YES;
                    UIViewController *subvc=(MyClosetViewController *)childvc;
                    [self.navigationController popToViewController:subvc animated:YES];
                }
            }
            
            if(!isVCExist)
            {
                MyClosetViewController *myclosetVC;
                if (myappDelegate.isiphone5)
                {
                    myclosetVC = [[MyClosetViewController alloc]initWithNibName:@"MyClosetViewController" bundle:nil];
                }
                else
                {
                    myclosetVC = [[MyClosetViewController alloc]initWithNibName:@"MyClosetViewController_iphone4" bundle:nil];
                }
                [self.navigationController pushViewController:myclosetVC animated:YES];
            }
        }
            break;
            
        case 3:
        {
            
            [self menuBtnAction:self];
        }
            break;
        case 4:
        {
            BOOL isVCExist=NO;
            NSArray *arr=self.navigationController.viewControllers;
            for (UIViewController *childvc in arr) {
                
                if([childvc isKindOfClass:[MyWishlistViewController class]])
                {
                    isVCExist=YES;
                    UIViewController *subvc=(MyWishlistViewController *)childvc;
                    [self.navigationController popToViewController:subvc animated:YES];
                }
            }
            
            if(!isVCExist)
            {
                
                MyWishlistViewController *mywishlistVC;
                if (myappDelegate.isiphone5)
                {
                    mywishlistVC = [[MyWishlistViewController alloc]initWithNibName:@"MyWishlistViewController" bundle:nil];
                }
                else
                {
                    mywishlistVC = [[MyWishlistViewController alloc]initWithNibName:@"MyWishlistViewController_iphone4" bundle:nil];
                }
                [self.navigationController pushViewController:mywishlistVC animated:YES];
            }
        }
            break;
            
        case 5:
        {
            BOOL isVCExist=NO;
            NSArray *arr=self.navigationController.viewControllers;
            for (UIViewController *childvc in arr) {
                
                if([childvc isKindOfClass:[SuggestionsViewController class]])
                {
                    isVCExist=YES;
                    UIViewController *subvc=(SuggestionsViewController *)childvc;
                    [self.navigationController popToViewController:subvc animated:YES];
                }
            }
            
            if(!isVCExist)
            {
                
                if ([AppDelegate isiphone5])
                {
                    myappDelegate.suggestionviewcont = [[SuggestionsViewController alloc]initWithNibName:@"SuggestionsViewController" bundle:nil];
                }
                else
                {
                    myappDelegate.suggestionviewcont = [[SuggestionsViewController alloc]initWithNibName:@"SuggestionsViewController_iphone4" bundle:nil];
                }
                [self.navigationController pushViewController:myappDelegate.suggestionviewcont animated:YES];
                
            }
        }
            break;
        case 6:
        {
            BOOL isVCExist=NO;
            NSArray *arr=self.navigationController.viewControllers;
            for (UIViewController *childvc in arr) {
                
                if([childvc isKindOfClass:[NewListingViewController class]])
                {
                    isVCExist=YES;
                    UIViewController *subvc=(NewListingViewController *)childvc;
                    [self.navigationController popToViewController:subvc animated:YES];
                }
            }
            
            if(!isVCExist)
            {
                
                if (![[APPDELEGATE getStringFromPlist:@"paypalstr"] isEqualToString:@""] && [APPDELEGATE getStringFromPlist:@"paypalstr"].length>3)
                {
                    
                    NewListingViewController *mywishlistVC;
                    if (myappDelegate.isiphone5)
                    {
                        mywishlistVC = [[NewListingViewController alloc]initWithNibName:@"NewListingViewController" bundle:nil];
                    }
                    else
                    {
                        mywishlistVC = [[NewListingViewController alloc]initWithNibName:@"NewListingViewController_iphone4" bundle:nil];
                    }
                    
                    [self.navigationController pushViewController:mywishlistVC animated:YES];
                    
                }
                else
                {
                    [AppDelegate alertmethod:@"Warning" :@"PayPal Email Address is required for Creating A Listing, Please enter your PayPal Email Address in your profile."] ;
                }
                
            }
        }
            break;
            
        default:
            break;
    }
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

-(void)successresponceListing :(id)respdict :(NSString *)paramname;
{
    
    
    
    if ([paramname isEqualToString:@"LikeItem"])
    {
        NSLog(@"resp dict %@",respdict);
        
        if ([respdict isKindOfClass:[NSDictionary class]])
        {
            NSDictionary *mydict=(NSDictionary *)respdict;
            
            NSString *messagestr=[mydict valueForKey:@"message"];
            if ([messagestr isEqualToString:@"Wishlist Saved Successfully"] || [messagestr isEqualToString:@"Wishlist Delted Successfully"])
            {
                
                ItemsModelobject *itemobj=[itemsarray objectAtIndex:likeindex];
                
                if ([messagestr isEqualToString:@"Wishlist Saved Successfully"])
                {
                    itemobj.likedbool=YES;
                    [myappDelegate.allLikesGlobalarray addObject:[NSString stringWithFormat:@"%@",[mydict objectForKey:@"userListingID"] ]];
                    
                }
                else{
                    itemobj.likedbool=NO;
                    [myappDelegate.allLikesGlobalarray removeObject:[NSString stringWithFormat:@"%@",[mydict objectForKey:@"userListingID"] ]];
                }
                
                [myappDelegate.mylikesGlobalarray removeAllObjects];
                BOOL breakbool=NO;
                for (int i=0; i<itemscrollview.subviews.count; i++)
                {
                    UIView *myv=[itemscrollview.subviews objectAtIndex:i];
                    
                    if (myv.tag==likeindex)
                    {
                        if ([myv isKindOfClass:[UIView class]])
                        {
                            for (UIView *temv in myv.subviews)
                            {
                                if ([temv isKindOfClass:[UIButton class]])
                                {
                                    UIButton *likebtn=(UIButton *)temv;
                                    if (likebtn.frame.size.width<20)
                                    {
                                        if (itemobj.likedbool==YES)
                                        {
                                            
                                            
                                            [likebtn setImage:[UIImage imageNamed:@"like_on.png"] forState:UIControlStateNormal];
                                        }
                                        else{
                                            [likebtn setImage:[UIImage imageNamed:@"like_off.png"] forState:UIControlStateNormal];
                                            
                                            
                                        }
                                        
                                        breakbool=YES;
                                        break;
                                    }
                                }
                            }
                            
                            if (breakbool==YES)
                            {
                                break;
                            }
                        }
                        
                        
                    }
                }
                
            }
            
            
        }
        myappDelegate.suggestionviewcont.loaddatabool=YES;
        
        [myappDelegate stopspinner:self.view];
    }
    else if ([paramname isEqualToString:@"getallLikes"])
    {
        NSLog(@" check teh dictionay  %@ ",respdict);
        prevlikedarray =[[NSMutableArray alloc]init];
        if ([respdict isKindOfClass:[NSArray class]])
        {
            NSArray *resparray=(NSArray *)respdict;
            
            for (int i=0; i<resparray.count; i++)
            {
                NSDictionary *subdict=[resparray objectAtIndex:i];
                
                NSString *likedid=[NSString stringWithFormat:@"%@", [subdict objectForKey:@"userListingID"]];
                
                NSLog(@"check the string %@",likedid);
                [prevlikedarray addObject:likedid];
            }
        }
        
        [registrationRep getdata:nil :@"Myfeeds" :@"GET" withcount:@"0"];
    }
    else if ([paramname isEqualToString:@"Myfeeds"])
    {
        [myappDelegate.myfeedsGlobalarray removeAllObjects];
        [myappDelegate.myfeedsGlobalarray addObjectsFromArray:[myappDelegate parselistingitem:respdict :YES :prevlikedarray :NO]];
        
        [self createScrollView];
        [myappDelegate stopspinner:self.view];
        [refreshControl endRefreshing];
    }
    
    
    
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

-(void)createScrollView
{
    
    for (UIView *selfv in itemscrollview.subviews)
    {
        if ([selfv isKindOfClass:[ODRefreshControl class]])
        {
            
        }
        else{
            [selfv removeFromSuperview];
        }
    }
    int x=15;
    int y=5;
    
    NSLog(@" items array count %d",itemsarray.count);
    
    itemsarray=myappDelegate.myfeedsGlobalarray;
    
    
    [myappDelegate createmyscrollview:itemscrollview :itemsarray :x :y :self];
    
    itemscrollview.hidden=NO;
    
    {
        
    }
    
    [itemscrollview setContentOffset:CGPointMake(0 , 0) animated:YES];
     [myappDelegate animatemyview:itemscrollview ];
   // [self animateView];
    
}

#pragma mark scrollview methods
-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    
}

-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    [AppDelegate assaignitemimages:CGRectMake(0, scrollView.contentOffset.y,scrollView.frame.size.width , scrollView.frame.size.height) :itemsarray :itemscrollview];
    
}



/********Notification button Action***********/
-(IBAction)notificationclicked:(id)sender
{
    
    NotificationViewController *notificationVC;
    
    if(myappDelegate.isiphone5==NO)
    {
        notificationVC=[[NotificationViewController alloc]initWithNibName:@"NotificationViewController_iphone4" bundle:nil];
    }
    else if([[UIScreen mainScreen] bounds].size.height == 568)
    {
        notificationVC=[[NotificationViewController alloc]initWithNibName:@"NotificationViewController" bundle:nil];
    }
    NSString *countstr=@"0";
    [APPDELEGATE insertStringInPlist:@"notificationcount" value:countstr];
    [self.navigationController pushViewController:notificationVC animated:YES];
}
-(void)internetConnection
{
    UIAlertView *Internetalert=[[UIAlertView alloc]initWithTitle:appname message:@"Internet connection not available" delegate:self cancelButtonTitle:nil otherButtonTitles:@"Ok", nil];
    [Internetalert show];
    [APPDELEGATE stopspinner:self.view];
    
}

@end
