//
//  MessageViewController.m
//  messagetesting
//
//  Created by Varma Bhupatiraju on 09/06/14.
//  Copyright (c) 2014 Varma Bhupatiraju. All rights reserved.
//

#import "MessageViewController.h"
#import"MessageModelObject.h"
#import"MessageCustomcell.h"
@interface MessageViewController ()

@end

@implementation MessageViewController
@synthesize msgtableview,navbar,ListingLabel,msgsearchbar;

@synthesize itemsarray;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
#pragma mark view methods
- (void)viewDidLoad
{
    [super viewDidLoad];
    //  myappDelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    
    itemsarray=[NSMutableArray new];
    screenSize = [[UIScreen mainScreen]bounds];
    
    self.navbar.frame = CGRectMake(0, 0, 320, 64);
    [self.navbar setBackgroundImage:[UIImage imageNamed:@"navbar.png"] forBarMetrics:UIBarMetricsDefault];
    self.ListingLabel.font = [UIFont fontWithName:@"Monroe" size:18];
    
    
    
    refreshControl = [[ODRefreshControl alloc] initInScrollView:self.msgtableview];
    
    refreshControl.tintColor=[UIColor colorWithRed:12.0/255.0 green:183.0/255.0  blue:170.0/255.0  alpha:1.0];
    
    //[UIColor colorWithRed:((float)57/255.0f)   green:((float)59/255.0f)  blue:((float)120/255.0f)  alpha:1.0];
    [refreshControl addTarget:self action:@selector(dropViewDidBeginRefreshing:) forControlEvents:UIControlEventValueChanged];
    refreshControl.activityIndicatorViewColor=[UIColor colorWithRed:12.0/255.0 green:183.0/255.0  blue:170.0/255.0  alpha:1.0];
    
    
    [msgsearchbar setShowsCancelButton:NO animated:YES];
    
    msgsearchbar.placeholder=@"Search";
    duplicatearray=[[NSMutableArray alloc]init];
    
    
    registrationRep=[[RegistrationRepository alloc]init];
    registrationRep.delegate=self;
    [self getdatawithdelay];
}


- (void)dropViewDidBeginRefreshing:(ODRefreshControl *)refresh
{
    [refreshControl beginRefreshing ];
    [self getdatawithdelay];
    
}

-(void)getdatawithdelay
{
    [myappDelegate startspinner:self.view];
    
    //[registrationRep getdata:nil :@"GetChatting" :@"GET"];
    
    NSDictionary *mydic=[[NSDictionary alloc]initWithObjectsAndKeys:[NSNumber numberWithInt:0],@"count", nil];
    [registrationRep postwithNSurl:mydic :@"GetChatting" :@"GET" :NO];
    
}

//*****this method using error messages******
-(void)errorMessage:(NSString *)message
{
    [myappDelegate stopspinner:self.view];
    [AppDelegate alertmethod:appname :message];
    
}
#pragma mark tableview Delegates
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 65;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return itemsarray.count;
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return  1;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellidentifier=@"cell";
    MessageCustomcell *cell=(MessageCustomcell *)[msgtableview dequeueReusableCellWithIdentifier:cellidentifier];
    if (cell==nil)
    {
        [[NSBundle mainBundle]loadNibNamed:@"MessageCustomcell" owner:self options:nil];
        cell=self.mymsgcustomcell;
        
    }
    ChartModelObject  *chartobj=[itemsarray objectAtIndex:indexPath.row];
    cell.namelabel.text=chartobj.namestring;
    cell.textlabel.text=chartobj.messageStr;
    cell.timelabel.text=@"";
    
    NSURL *imgurl=[NSURL URLWithString:chartobj.profilepic];
    NSLog(@"%@",imgurl);
    [ cell.firstimageview setImageWithURL:imgurl             placeholderImage:[UIImage imageNamed:@"noimage_friend.png"] options:SDWebImageCacheMemoryOnly];
    // cell.firstimageview.image=[UIImage imageNamed:bobj.imagestring];
    CALayer *photobglayer = cell.firstimageview.layer;
    [photobglayer setCornerRadius:25];
    [photobglayer setBorderWidth:0];
    [photobglayer setMasksToBounds:YES];
    cell.namelabel.font = [UIFont fontWithName:@"Muli" size:15];
    cell.textlabel.font = [UIFont fontWithName:@"Muli" size:14];
    cell.timelabel.font = [UIFont fontWithName:@"Muli" size:12];
    
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    return  cell;
    
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    [msgsearchbar resignFirstResponder];
    
    
    ChartModelObject *chartobj=[itemsarray objectAtIndex:indexPath.row];
    ChartViewController *chart_VC;
    if (myappDelegate.isiphone5)
    {
        chart_VC=[[ChartViewController alloc]initWithNibName:@"ChartViewController" bundle:nil];
    }
    else
        
    {
        chart_VC=[[ChartViewController alloc]initWithNibName:@"ChartViewController_iphone4" bundle:nil];
    }
    
    chart_VC.titlestr=chartobj.namestring;// [chartobj.profilerespdict valueForKey:@"userName"];
    profileRep=[[ProfileRepository alloc]init];
    profileRep.delegate=self;
    NSString *userid=[myappDelegate getStringFromPlist:@"userid"];
    
    NSLog(@"the userid is %@",userid);
    //[myappDelegate startspinner:self.view];
     NSLog(@"sellerserid %@",chartobj.conversationuseridstr);
    NSString *str=[NSString stringWithFormat:@"%@",chartobj.conversationuseridstr];
    [profileRep messageRead:userid :str];
    
    NSLog(@"sellerserid %@",chartobj.buyeridstr);
    chart_VC.selleruseridstr=chartobj.buyeridstr;
    chart_VC.isloginseller=YES;
    chart_VC.profilepicstr=chartobj.profilepic;
    
    [self.navigationController pushViewController:chart_VC animated:YES];
    
}
-(void) successResponseformessageRead:(NSDictionary*)mainDict
{
    dispatch_async(dispatch_get_main_queue(), ^{
        NSLog(@"the dict is message iss %@",mainDict);
        [APPDELEGATE stopspinner:self.view];
        if([[mainDict valueForKey:@"message"] isEqualToString:@"Notification Updated Successfully"])
        {
//            UIAlertView *alert=[[UIAlertView alloc] initWithTitle:appname message:@"Notification Updated Successfully" delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK",nil];
//           // [alert setTag:15];
//            [alert show];
            
            NSLog(@"message Notification Updated Successfully");
            
        }
    });
}
//navigation back actions
-(IBAction)backAction:(id)sender;
{
    
    [self.navigationController popViewControllerAnimated:YES];
    
}
//this action performs pop SuggestionsViewController
-(IBAction)homeBtnAction:(id)sender;
{
    NSArray *arr=self.navigationController.viewControllers;
    for (UIViewController *childvc in arr) {
        
        if([childvc isKindOfClass:[SuggestionsViewController class]])
        {
            SuggestionsViewController *subvc=(SuggestionsViewController *)childvc;
            subvc.ismyfeed=NO;
            [self.navigationController popToViewController:subvc animated:YES];
        }
    }
    
}
#pragma mark search bar delegates
-(void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    /*[msgsearchbar setShowsCancelButton:NO animated:YES];
    searchBar.text=@"";
    [searchBar resignFirstResponder];
    [itemsarray removeAllObjects];
    
    [itemsarray addObjectsFromArray:[duplicatearray mutableCopy]];
    
    [msgtableview reloadData];*/
    
    
    searchBar.text=@"";
    [searchBar resignFirstResponder];
    [itemsarray removeAllObjects];
    [itemsarray addObjectsFromArray:duplicatearray];
    [msgtableview reloadData];
    
}
-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    
    [searchBar setShowsCancelButton:NO animated:YES];
    if(searchBar.text.length == 0)
    {
        [itemsarray removeAllObjects];
        [itemsarray addObjectsFromArray:duplicatearray];
        [msgtableview reloadData];
        
    }
    else
    {
        [searchBar setShowsCancelButton:NO animated:YES];
        [itemsarray removeAllObjects];
       
        
        for(int i=0;i<[duplicatearray count];i++)
        {
            ChartModelObject*searchobj=[duplicatearray objectAtIndex:i];
            
            //     NSRange nameRange = [[[self.duplicatearray objectAtIndex:i] valueForKey:@"itemName"]  rangeOfString:searchBar.text options:(NSCaseInsensitiveSearch|NSDiacriticInsensitiveSearch)];
            NSRange nameRange = [searchobj.namestring rangeOfString:searchBar.text options:(NSCaseInsensitiveSearch|NSDiacriticInsensitiveSearch)];
            
            if(nameRange.location != NSNotFound)
            {
                [itemsarray addObject:[duplicatearray objectAtIndex:i]];
            }
           
        }
        
    }
    
    if([searchText length] == 0)
    {
        [searchBar performSelector: @selector(resignFirstResponder)
                        withObject: nil
                        afterDelay: 0.1];
        return;
    }
 
     [msgtableview reloadData];
    
    
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    
    if (searchBar.text.length>2)
    {
        
        [itemsarray removeAllObjects];
        
        for (int i=0; i<[duplicatearray count]; i++)
        {
            ChartModelObject *itemobj=[duplicatearray objectAtIndex:i];
            NSLog(@"duplicatearray ....%@",duplicatearray );
            NSLog(@"messstr......%@",itemobj.messageStr);
            
            NSString *caseinsesearch=[searchBar.text lowercaseString];
            
            NSString *namecase=[itemobj.namestring lowercaseString];
            if([namecase hasPrefix:caseinsesearch]  )
            {
                
                NSLog(@"duplicatearray....%@",duplicatearray);
                [itemsarray addObject:itemobj];
                NSLog(@"itemsarray.....%@",itemsarray);
                
            }
            
        }
        [msgtableview reloadData];
    }
    else
    {
        [AppDelegate alertmethod:appname :@"Please enter atleast 3 characters to search" ];
    }
    /*new
    [searchBar resignFirstResponder];
    [APPDELEGATE.suggestionslistArray removeAllObjects];
    for(int i=0;i<[self.duplicatearray count];i++)
    {
        SuggestionObject*searchobj=[self.duplicatearray objectAtIndex:i];
        
        NSRange nameRange = [searchobj.usernamestr rangeOfString:searchBar.text options:(NSCaseInsensitiveSearch|NSDiacriticInsensitiveSearch)];
        
        if(nameRange.location != NSNotFound)
        {
            [APPDELEGATE.suggestionslistArray addObject:[self.duplicatearray objectAtIndex:i]];
        }
    }
    [suggestionslistview reloadData];new */

    
}

-(void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar
{
    
        
  // [duplicatearray removeAllObjects];
  // [duplicatearray addObjectsFromArray:[itemsarray mutableCopy]];
   //[itemsarray removeAllObjects];
   //[msgsearchbar setShowsCancelButton:YES animated:YES];
   ////[msgtableview reloadData];
        
    if ([searchBar.text isEqualToString:@" "])
    {
        [itemsarray removeAllObjects];
        [itemsarray addObjectsFromArray:duplicatearray];
        [msgtableview reloadData];
    }
    
    
}
-(void)searchBarTextDidEndEditing:(UISearchBar *)searchBar
{
   /* [duplicatearray removeAllObjects];
    [duplicatearray addObjectsFromArray:[itemsarray mutableCopy]];
    [itemsarray removeAllObjects];
    [msgsearchbar setShowsCancelButton:YES animated:YES];*/
    
    if ([searchBar.text isEqualToString:@" "])
    {
        searchBar.text = @"";
        [itemsarray removeAllObjects];
        [itemsarray addObjectsFromArray:duplicatearray];
        [msgtableview reloadData];
        [msgsearchbar setShowsCancelButton:YES animated:YES];
    }
    
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
/////////////////////////////////azhar

- (BOOL)searchBar:(UISearchBar *)searchBar shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    if ([text isEqualToString:@" "])
    {
        return NO;
    }
    else
    {
        return YES;
    }
}



///////////////////azhar
-(void)successresponceListing :(id)respdict :(NSString *)paramname;
{
    [duplicatearray removeAllObjects];
    [itemsarray removeAllObjects];
    if ([paramname isEqualToString:@"GetChatting"])
    {
        NSLog(@"check the dict %@",respdict);
        NSDictionary *respconvdict=(NSDictionary *)respdict;
        
        
        NSArray *temparray=[respconvdict objectForKey:@"listnotifications"];
        
        if ([temparray isKindOfClass:[NSArray class]])
        {
            NSArray *resparray=(NSArray *)temparray;
            
            [itemsarray removeAllObjects];
            for (int i=0; i<resparray.count; i++)
            {
                
                NSDictionary *maindict=resparray[i];
                
                
                
                ChartModelObject *chartmodobj=[[ChartModelObject alloc]init];
                
                
                NSString *postid=[maindict valueForKey:@"messengerUserId"];
                
                chartmodobj.conversationuseridstr=[maindict valueForKey:@"conversationUserId"];
                
                
                if ([[NSString stringWithFormat:@"%@",myappDelegate.useridstring] isEqualToString:[NSString stringWithFormat:@"%@",postid]])
                    // if ((i+1)%2 !=0)
                {
                    chartmodobj.isuser=YES;
                    
                    
                }
                //else
                {
                    chartmodobj.isuser=NO;
                    
                    // chartmodobj.profilerespdict=profiledict;
                    NSLog(@"pic is %@",[maindict valueForKey:@"profilePic"]);
                    chartmodobj.profilepic=[NSString stringWithFormat:@"%@",[maindict valueForKey:@"profilePic"] ];
                    
                    
                    chartmodobj.namestring= [maindict valueForKey:@"name"];
                    // [itemsarray addObject:chartmodobj];
                    
                }
                
                NSString *messagestr=[maindict valueForKey:@"message"];
                NSString *imageurlstr=[maindict valueForKey:@"image"];
                
                if (![myappDelegate isnullorempty:messagestr] && [myappDelegate isnullorempty:imageurlstr])
                {
                    chartmodobj.istext=YES;
                    chartmodobj.messageStr=messagestr;
                }
                else if ([myappDelegate isnullorempty:messagestr] && ![myappDelegate isnullorempty:imageurlstr])
                {
                    chartmodobj.istext=NO;
                    chartmodobj.imageurl=imageurlstr;
                    
                }
                else if (![myappDelegate isnullorempty:messagestr] && ![myappDelegate isnullorempty:imageurlstr])
                {
                    chartmodobj.messageStr=messagestr;
                    chartmodobj.imageurl=imageurlstr;
                }
                
                //buyerUserId
                //sellerUserId
                NSString *buyeridTemp=[NSString stringWithFormat:@"%@",[maindict valueForKey:@"userId"] ];
                NSLog(@"%@",myappDelegate.useridstring);
                if ([[NSString stringWithFormat:@"%@",myappDelegate.useridstring] isEqualToString:buyeridTemp])
                {
                    NSString *temp=[maindict valueForKey:@"messengerUserId"];
                    chartmodobj.buyeridstr=temp;
                }
                else
                {
                    chartmodobj.buyeridstr=buyeridTemp;
                }
                
                
                //userDetails
                //buyerUserDetails
                //sellerUserDetails
                NSDictionary *profiledict=[maindict valueForKey:@"userDetails"];
                
                if ([myappDelegate isnullorempty:profiledict])
                {
                    //[itemsarray addObject:chartmodobj];
                    // break;
                }
                else
                {
                    
                    NSLog(@"main dict %@",maindict);
                    
                }
                
                [itemsarray addObject:chartmodobj];
                 [duplicatearray addObject:chartmodobj];
                
                
            }
            [msgtableview reloadData];
        }
        
        [myappDelegate stopspinner:self.view];
        
    }
    
    [refreshControl endRefreshing];
}
-(void)internetConnection
{
    UIAlertView *Internetalert=[[UIAlertView alloc]initWithTitle:appname message:@"Internet connection not available" delegate:self cancelButtonTitle:nil otherButtonTitles:@"Ok", nil];
    [Internetalert show];
    [APPDELEGATE stopspinner:self.view];
    
}
@end
