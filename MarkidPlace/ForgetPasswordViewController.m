//
//  ForgetPasswordViewController.m
//  MarkidPlace
//
//  Created by stellent_mac_05 on 03/07/14.
//  Copyright (c) 2014 stellentmac1. All rights reserved.
//

#import "ForgetPasswordViewController.h"

@interface ForgetPasswordViewController ()

@end

@implementation ForgetPasswordViewController
@synthesize emailTf,emailimgview,request,responseData,donebutton,donelbl,loginresponsedict,loginstr;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
#pragma mark view methods
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    navbar.frame = CGRectMake(0, 0, 320, 64);
    [navbar setBackgroundImage:[UIImage imageNamed:@"navbar.png"] forBarMetrics:UIBarMetricsDefault];
    
    
    titlelbl.font = [AppDelegate navtitlefont];
    if([loginstr isEqualToString:@"Login"])
    {
    titlelbl.text=@"Update Email";
    }
    else
    {
          titlelbl.text=@"Forgot Password";
    }
    self.emailTf.font = [UIFont fontWithName:@"Muli" size:18];
    
    
    donelbl.font=[UIFont fontWithName:@"Muli" size:18];
    appdelegate=(AppDelegate*)[[UIApplication sharedApplication]delegate];
    
}
- (BOOL) emailvalidate
{
    [emailTf resignFirstResponder];
    NSString *email;
    email=[self.emailTf.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    
    NSString *emailRegEx = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
	NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegEx];
    
    if(email==nil||[email isEqualToString:@""] || email.length==0 || [email isEqualToString:@" "])
    {
        //   [spinner stopAnimating];
        UIAlertView *alertview=[[UIAlertView alloc]initWithTitle:appname message:@"Email is required" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alertview show];
        return NO;
	}
    else if ([emailTest evaluateWithObject:email] != YES)
    {
        UIAlertView *loginalert = [[UIAlertView alloc] initWithTitle:appname message:@"Email field not in proper format." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:Nil, nil];
        [loginalert show];
        return NO;
    }
    if([loginstr isEqualToString:@"Login"])
    {
        [appdelegate startspinner:self.view];
        profilerepo=[[ProfileRepository alloc]init];
        profilerepo.delegate=self;
        NSMutableDictionary*dict=[[NSMutableDictionary alloc]init];
        [dict setValue:loginresponsedict[@"userId" ] forKey:@"UserId"];
         [dict setValue:emailTf.text forKey:@"Email"];
        [profilerepo emailupdate:dict];
        return YES;

    }
    else
    {
      [appdelegate startspinner:self.view];
        [self postEmail];
        return YES;
    }
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [emailTf resignFirstResponder];
    return YES;
}
#pragma mark Button Actions
//this action performs navigation  back action
-(IBAction)backbuttonAction:(id)sender
{
    [emailTf resignFirstResponder];
    
    [self.navigationController popViewControllerAnimated:YES];
    
}
//this action call to method
-(IBAction)donebuttonAction:(id)sender
{
    [self emailvalidate];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag==1234)
    {
        if (buttonIndex==0)
        {
            [self.navigationController popViewControllerAnimated:YES];
            
        }
    }
    else
    {
        emailTf.text=@"";
    }
    
}
-(void)postEmail
{
  
    NSString *urlstr=[NSString stringWithFormat:@"%@Account/ForgotPassword?Email=%@&format=json",MainUrl,emailTf.text];
    NSURL *url = [NSURL URLWithString: urlstr];
    
    request = [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:3600.0];
    
    [request setHTTPMethod:@"GET"];
    NSLog(@"url.......%@",url);
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    
    [NSURLConnection sendAsynchronousRequest:request queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         if (error)
         {
             NSLog(@"error is %@",error);
             //  [appdelegate stopspinner:self.view];
             
             [self dismissactivindic:NO];
         }
         else
         {
             NSError *jsonParsingError = nil;
             
             id respdict = [NSJSONSerialization JSONObjectWithData:data options:0 error:&jsonParsingError];
             
             NSLog(@"the main dict is %@",respdict);
             
             if (jsonParsingError)
             {
                 NSLog(@"--------------JSON ERROR: %@", [jsonParsingError localizedDescription]);
                 dispatch_sync(dispatch_get_main_queue(), ^{
                      [self dismissactivindic:YES];
                 });
                 
                 // [delegate errorMessage:@"message"];
             }
             else
             {
               //  NSString *message=[respdict valueForKey:@"message"];
                 if([[respdict valueForKey:@"message"]isEqualToString:@"Email and Password Sent to Email Successfully"])
                 {
                 dispatch_sync(dispatch_get_main_queue(), ^{
                     UIAlertView *success=[[UIAlertView alloc]initWithTitle:appname message:@"Request for the password change has been sent to your email." delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
                     success.tag=1234;
                     [success show];
                 });
                 }
                 else
                 {
                     dispatch_sync(dispatch_get_main_queue(), ^{
                         UIAlertView *success=[[UIAlertView alloc]initWithTitle:appname message:[respdict valueForKey:@"message"] delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
                         success.tag=1234;
                         [success show];
                     });
                 }
                
             }
              [appdelegate stopspinner:self.view];
         }
         
         [self dismissactivindic:NO];
         
     }];
}

-(void)dismissactivindic :(BOOL)showalert
{
    [appdelegate stopspinner:self.view];
    [appdelegate stopspinner:self.view];
    if (showalert)
    {
        [AppDelegate alertmethod:appname :@"No data to display" ];
    }
}
-(void)internetConnection
{
    UIAlertView *Internetalert=[[UIAlertView alloc]initWithTitle:appname message:@"Internet connection not available" delegate:self cancelButtonTitle:nil otherButtonTitles:@"Ok", nil];
    [Internetalert show];
    [appdelegate stopspinner:self.view];
    
}
//email entering
-(void)successResponseforupdateemail:(NSMutableDictionary *)responsedict
{
    
       dispatch_async( dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH,0), ^{
    NSLog(@"responsedict %@",responsedict);
    if([[responsedict valueForKey:@"message"] isEqualToString:@"Email Updated Successfully"])
    {
        
     [myappDelegate stopspinner:self.view];
     [myappDelegate insertStringInPlist:@"userid" value:loginresponsedict[@"userId"]];
     [myappDelegate insertStringInPlist:@"email" value:loginresponsedict[@"email"]];
     [myappDelegate insertStringInPlist:@"type" value:loginresponsedict[@"type"]];
     [myappDelegate insertStringInPlist:@"realname" value:loginresponsedict[@"name"]];
     
     [myappDelegate insertStringInPlist:@"profilePicture" value:loginresponsedict[@"profilePicture"]];
     [APPDELEGATE insertStringInPlist:@"notificationcount" value:[NSString stringWithFormat:@"%@",loginresponsedict[@"notificationCount"]]];
     myappDelegate.useridstring=loginresponsedict[@"userId"];
     [APPDELEGATE insertStringInPlist:@"paypalstr" value:loginresponsedict[@"paypalEmailAddress"]];
     if(APPDELEGATE.islistusers==YES)
     {
     APPDELEGATE.islistusers=NO;
     dispatch_async( dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
     [APPDELEGATE performSelector:@selector(getuserslist) withObject:nil afterDelay:0.001];
     });
     }
     
     else if ([myappDelegate isnullorempty:loginresponsedict[@"userName"]] || [myappDelegate isnullorempty:loginresponsedict[@"name"] ])
     {
     EditProfileViewController *editprofile;
     if (myappDelegate.isiphone5)
     {
     editprofile=[[EditProfileViewController alloc]initWithNibName:@"EditProfileViewController" bundle:nil];
     }
     else
     {
     editprofile=[[EditProfileViewController alloc]initWithNibName:@"EditProfileViewController_iphone4" bundle:nil];
     }
     editprofile.isnew=YES;
     [self.navigationController pushViewController:editprofile animated:YES];
     
     }
     else
     {
     NSString *teststr=loginresponsedict[@"userName"];
     
     teststr=[teststr stringByReplacingOccurrencesOfString:@"@" withString:@""];
     
     [myappDelegate insertStringInPlist:usernamekey value:teststr];
     
     if ([AppDelegate isiphone5])
     {
     myappDelegate.suggestionviewcont = [[SuggestionsViewController alloc]initWithNibName:@"SuggestionsViewController" bundle:nil];
     }
     else
     {
     myappDelegate.suggestionviewcont = [[SuggestionsViewController alloc]initWithNibName:@"SuggestionsViewController_iphone4" bundle:nil];
     
     }
     [self.navigationController pushViewController:myappDelegate.suggestionviewcont animated:YES];
     }
}
    else
    {
        UIAlertView *alertview=[[UIAlertView alloc]initWithTitle:appname message:[responsedict valueForKey:@"message"] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alertview show];
    }
    [myappDelegate stopspinner:self.view];
});

}
@end

