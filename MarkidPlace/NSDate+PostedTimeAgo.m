//
//  NSDate+PostedTimeAgo.m
//  Date-Class
//
//  Created by stellent on 05/09/14.
//  Copyright (c) 2014 stellent. All rights reserved.
//

#import "NSDate+PostedTimeAgo.h"

#define YEARS "y"
#define MONTHS "M"
#define WEEKS "w"
#define DAYS "d"
#define HOURS "h"
#define MINUTES "m"
#define SECONDS "s"


@implementation NSDate (PostedTimeAgo)

-(NSDate *) convertServerDateStringToNSDateType:(NSString *)dateString
{
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    //[dateFormat setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS"];
    if (dateString.length>21)
    {
        
       [dateFormat setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS"];
    }
    else
     [dateFormat setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss"];
    
    [dateFormat setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
    NSDate *date = [dateFormat dateFromString:dateString];
    
    if (date == nil || [date isKindOfClass:[NSNull class]])
    {
        return [NSDate date]; // if you passed an unexpected Date string formate it will be treated as current date.
        
    }
    return date;
}
+(NSString*) postedTimeAgoWithPostedDateString:(NSString *)postedDateString // We always get posted Date as string from server in the formate of UTC.
{
 
    
    NSDate *currentDate=[NSDate date];

    NSDate *postedDate=[currentDate convertServerDateStringToNSDateType:postedDateString];
    
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    
    NSDateComponents *components = [calendar components:NSYearCalendarUnit | NSMonthCalendarUnit | NSWeekCalendarUnit | NSDayCalendarUnit | NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit fromDate:postedDate toDate:[NSDate date] options:0];
    
    //*Get hours and minutes between Device date time and posted Date time*/
    int years=[components year];
    int months=[components month];
    int weeks=[components week];
    int days=[components day];
    int hours=[components hour];
    int minutes=[components minute];
    int seconds=[components second];
   //*Get hours and minutes between Device date time and posted Date time*/
    
    //*Check the datetime difference are past or future and convert those into past different *//
    
    if (years<0)
    {
        years=-(years);
    }
    if (months<0)
    {
        months=-(months);
    }
    if (weeks<0)
    {
        weeks=-(weeks);
    }
    if (days<0)
    {
        days=-(days);
    }
    if (hours<0)
    {
        hours=-(hours);
    }
    if (minutes<0)
    {
        minutes=-(minutes);
    }
    if (seconds<0)
    {
        seconds=-(seconds);
    }
    
   
    
    //*Check the highest priority difference, show the highest difference in the form of years,weeks,days,hours,minutes,seconds *//
    if (years>0)
    {
        NSString *yeardsDifference=[NSString stringWithFormat:@"%d%s",years,YEARS];
        return yeardsDifference;
    }
    /*else if(months>0) // You can use the months also for see how many months ago, but all famous social sites uses the weeks.
    {
        NSString *monthsDifference=[NSString stringWithFormat:@"%d%s",months,MONTHS];
        return monthsDifference;
    }
    */else if(weeks>0)
    {
        NSString *weeksDifference=[NSString stringWithFormat:@"%d%s",weeks,WEEKS];
        return weeksDifference;
    }
    else if(days>0)
    {
        NSString *daysDifference=[NSString stringWithFormat:@"%d%s",days,DAYS];
        return daysDifference;
    }
    else if(hours>0)
    {
        NSString *hoursDifference=[NSString stringWithFormat:@"%d%s",hours,HOURS];
        return hoursDifference;
    }
    else if(minutes>0)
    {
        NSString *minutsDifference=[NSString stringWithFormat:@"%d%s",minutes,MINUTES];
        return minutsDifference;
    }
    else
    {
        NSString *secondsDifference=[NSString stringWithFormat:@"%d%s",seconds,SECONDS];
        return secondsDifference;
    }
        
    
    
}

@end
