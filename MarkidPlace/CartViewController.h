//
//  CartViewController.h
//  MarkidPlace
//
//  Created by stellentmac1 on 5/21/14.
//  Copyright (c) 2014 stellentmac1. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

//PayPal

#import <MessageUI/MessageUI.h>
#import "RegistrationRepository.h"

#import "ItemsModelobject.h"
//PayPal
//#import "PayPalMobile.h"
#import "CartViewCell.h"
#import "ZZFlipsideViewController.h"
#import "BuyandSellViewController.h"
#import "OrderWebViewController.h"

@class AppDelegate;
@class ZZFlipsideViewController;
@class OrderWebViewController;
@interface CartViewController : UIViewController< ZZFlipsideViewControllerDelegate,UIPopoverControllerDelegate,UIScrollViewDelegate,HelperProtocol,UIAlertViewDelegate,UITableViewDataSource,UITableViewDelegate,UITextViewDelegate,UITextFieldDelegate>//Add Paypal Delegate
{
  //  AppDelegate *appdelegate;
    CGRect screenSize;
    BOOL isSwitchChange;
    UIImageView *switchImageView;
    UITextView *txtview;
  
    UITextField *currenttextfield;
    
    NSString *priceString;
    int selectedindex;
    CGFloat costval;
    CGFloat itemcostval;
    CGFloat shipcostval;
    NSString*selleridstr;
    //Paypal Properties
    

    
    //End of paypal
    
    RegistrationRepository *regisRepository;
    id <HelperProtocol> delegate;
}
@property(nonatomic,strong) ItemsModelobject *itemsModelObj;
@property(nonatomic,strong) NSString *cartIdString;
//Paypal
@property(nonatomic,strong)NSString *paypalTransIDString;
@property(nonatomic,strong)NSString *paypalStatusString;
@property(nonatomic,strong)NSString *paypalEmailIDString;

@property(nonatomic, strong, readwrite) NSString *environment;
@property(nonatomic, assign, readwrite) BOOL acceptCreditCards;
@property(nonatomic, strong, readwrite) NSString *resultText;

@property(nonatomic, strong, readwrite) IBOutlet UIButton *payNowButton;
@property(nonatomic, strong, readwrite) IBOutlet UIButton *payFutureButton;
@property(nonatomic, strong, readwrite) IBOutlet UIView *successView;

//@property(nonatomic, strong, readwrite) PayPalConfiguration *payPalConfig;
@property(nonatomic, strong, readwrite) UIPopoverController *flipsidePopoverController;

//Paypal


@property (nonatomic,strong) IBOutlet UIScrollView *cartScrollView;
@property (strong, nonatomic) IBOutlet UILabel *itemsdetaillbl;
@property (strong, nonatomic) IBOutlet UILabel *purchasetotallbl;
@property (strong, nonatomic) IBOutlet UILabel *shippinglbl;
@property (strong, nonatomic) IBOutlet UILabel *purchasecostlbl;
@property (strong, nonatomic) IBOutlet UILabel *markaslbl;
@property (strong, nonatomic) IBOutlet UIImageView *paypalbgimg;
@property (strong, nonatomic) IBOutlet UIImageView *itemImageView;
@property (strong, nonatomic) IBOutlet UIButton *paypalbtn;
@property (strong, nonatomic) IBOutlet UILabel *allpaymentslbl;
//@property (strong, nonatomic) NSMutableArray *cartArray;
//@property (strong, nonatomic) NSMutableArray *itemnamesArray;
//@property (strong, nonatomic) NSMutableArray *itempriceArray;

//- (IBAction)deletebtnAction:(id)sender;
//- (IBAction)paypalbtnAction:(id)sender;
- (IBAction)backbtnAction:(id)sender;
-(void)sendBuyerDetails; //paypal

@property (strong, nonatomic) IBOutlet UILabel *detailitemcostlbl;
@property (strong, nonatomic) IBOutlet UIImageView *detailitemimglbl;
@property (strong, nonatomic) IBOutlet UILabel *itemnamelbl;
/////////////changed
@property (strong, nonatomic)IBOutlet UITableView *cartlistview;
@property (strong, nonatomic)IBOutlet CartViewCell *cartcell;
@property (strong, nonatomic) NSMutableArray *itemsArray;
@property (nonatomic,strong) IBOutlet UINavigationBar *navbar;
@property (strong, nonatomic) IBOutlet UILabel *titlelbl;

-(IBAction)cancelbtnAction:(id)sender;

-(IBAction)checkoutbtnAction:(id)sender;
-(void)sendBuyerDetails;

@end
