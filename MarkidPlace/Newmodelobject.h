//
//  Newmodelobject.h
//  MarkidPlace
//
//  Created by Varma Bhupatiraju on 07/07/14.
//  Copyright (c) 2014 stellentmac1. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Newmodelobject : NSObject

@property(nonatomic,strong)NSString *cellnamestr;
@property(nonatomic,strong)NSString *cellvaluestr;
@property(nonatomic,strong)NSString *otherstr;

@property(nonatomic,readwrite)BOOL  isother;
@end
