//
//  FollowersViewController.m
//  MarkidPlace
//
//  Created by stellent on 03/06/14.
//  Copyright (c) 2014 stellentmac1. All rights reserved.
//

#import "FollowersViewController.h"
#import "FriendsObject.h"
#import <SDWebImage/SDImageCache.h>
#import <SDWebImage/UIImageView+WebCache.h>

@interface FollowersViewController ()

@end

@implementation FollowersViewController
@synthesize tblcell_followers;
@synthesize array_followers;
@synthesize tblview_followers;
@synthesize isfollowers;
@synthesize navbar;
@synthesize viewtitlelbl;
@synthesize profRepo,selleruseridstr,issellerprofile;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
#pragma mark--view methods
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
   // myappDelegate=(AppDelegate *)[[UIApplication sharedApplication]delegate];
    viewtitlelbl.font = [UIFont fontWithName:@"Muli" size:20];
    
    followingArray=[NSMutableArray new];
    if (isfollowers==NO) {
        self.viewtitlelbl.text=@"Following";
    }
    else{
        self.viewtitlelbl.text=@"Followers";
    }
    
    registrationRep=[[RegistrationRepository alloc ]init];
    
    registrationRep.delegate=self;
    
    self.array_followers=[[NSMutableArray alloc]init];
   
    
    profRepo=[[ProfileRepository alloc] init];
    profRepo.delegate=self;
    
    if (myappDelegate.isiphone5==NO)
    {
        CGRect viewrect=self.view.frame;
        
        viewrect.size.height=480;
        self.view.frame=viewrect;
        
        
        CGRect tblfrm=tblview_followers.frame;
        tblfrm.origin.y=65;
        tblfrm.size.height=480-66;
        tblview_followers.frame=tblfrm;
    }
}

-(void)viewWillAppear:(BOOL)animated{
    self.navigationController.navigationBar.hidden=YES;
    self.navbar.frame = CGRectMake(0, 0, 320, 64);
    [self.navbar setBackgroundImage:[UIImage imageNamed:@"navbar.png"] forBarMetrics:UIBarMetricsDefault];
    
    [myappDelegate startspinner:self.view];
    [self performSelector:@selector(getdata)];
}
-(void)getdata
{
 
  
    NSMutableDictionary *datadict;
    if (issellerprofile==YES)
    {
        datadict=[NSMutableDictionary new];
        [datadict setObject:selleruseridstr forKey:@"sellerid"];
       
    }
    NSLog(@"check the post disct %@",datadict);
       [ registrationRep getdata :datadict :@"getfollowers" :@"GET" withcount:@"0"];
   
    
}

#pragma mark--Button Actions
-(IBAction)backAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
    
}
- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}


//this Action is using
-(IBAction)followAction:(id)sender
{
    FriendsObject *obj_friends=[array_followers objectAtIndex:[sender tag]];
    
    NSLog(@"check the userid %@",obj_friends.useridstr);
     if (![[NSString stringWithFormat:@"%@",myappDelegate.useridstring]  isEqualToString:[NSString stringWithFormat:@"%@",obj_friends.useridstr]] )
   // if (![myappDelegate.useridstring isEqual:obj_friends.useridstr])
    {
        // [myappDelegate startspinner:self.view];
        NSMutableDictionary *dict=[[NSMutableDictionary alloc] init];
        [dict setValue:myappDelegate.useridstring forKey:@"UserId"];
        [dict setValue:[NSString stringWithFormat:@"%@",obj_friends.useridstr] forKey:@"FollowerUserId"];
        
        [profRepo followOrUnfollow:dict];
        
        if (isfollowers)
        {
          if(obj_friends.isstatus==YES)
          {
              obj_friends.isstatus=NO;
               [followingArray addObject:obj_friends.useridstr];
          }
            else
            {
                 obj_friends.isstatus=YES;
                [followingArray removeObject:obj_friends.useridstr];
                
            }
        }
        else
        {
            obj_friends.isstatus=YES;
            [array_followers removeObjectAtIndex:[sender tag]];
            
        }
        [tblview_followers reloadData];
        [myappDelegate.myfeedsGlobalarray removeAllObjects];
    }
    else{
        [AppDelegate alertmethod:appname :@"You can not follow your self"];
    }
  
}
-(void)successresponceListing :(id)respdict :(NSString *)paramname;
{
    [myappDelegate stopspinner:self.view];
    if ([paramname isEqualToString:@"getfollowers"])
    {
        NSMutableDictionary *dict_response=(NSMutableDictionary *)respdict;
        NSLog(@"followers are %@",dict_response);
        [self.array_followers removeAllObjects];
        
        if (isfollowers==NO)
        {
            NSArray *arr_following=[dict_response objectForKey:@"listfollowing"];
            
            
            for (int i=0; i<[arr_following count]; i++)
            {
                FriendsObject *obj_friends=[[FriendsObject alloc]init];
                NSDictionary *dict_followers=[arr_following objectAtIndex:i];
                obj_friends.string_follwerUserName=[dict_followers objectForKey:@"userName"];
                obj_friends.string_follwerName=[dict_followers objectForKey:@"name"];
                obj_friends.string_follwerPicUrl=[dict_followers objectForKey:@"profilepic"];
                obj_friends.string_follwerPicUrl=[obj_friends.string_follwerPicUrl stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
                obj_friends.useridstr=[NSString stringWithFormat:@"%@",[dict_followers objectForKey:@"userId"]];
            obj_friends.isstatus=[[dict_followers objectForKey:@"isFollow"] boolValue];
                NSLog(@"listfollowing--------obj_friends.isstatus--------->");
                NSLog(obj_friends.isstatus ? @"Yes" : @"No");
                [self.array_followers addObject:obj_friends];
                
            }
            
        }
        else
        {
            NSArray *arr_following=[dict_response objectForKey:@"listfollower"];
            
            
            for (int i=0; i<[arr_following count]; i++)
            {
                FriendsObject *obj_friends=[[FriendsObject alloc]init];
                NSDictionary *dict_followers=[arr_following objectAtIndex:i];
                obj_friends.string_follwerName=[dict_followers objectForKey:@"name"];
                obj_friends.string_follwerUserName=[dict_followers objectForKey:@"userName"];
                obj_friends.string_follwerPicUrl=[dict_followers objectForKey:@"profilepic"];
                obj_friends.string_follwerPicUrl=[obj_friends.string_follwerPicUrl stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
                obj_friends.useridstr=[NSString stringWithFormat:@"%@",[dict_followers objectForKey:@"userId"]];
                obj_friends.isstatus=[[dict_followers objectForKey:@"isFollow"] boolValue];
                NSLog(@"listfollower------obj_friends.isstatus--------->");
                NSLog(obj_friends.isstatus ? @"Yes" : @"No");
                [self.array_followers addObject:obj_friends];
                
            }
            
            
            [followingArray removeAllObjects];
            
            NSArray *follinftemparray=[dict_response objectForKey:@"listfollowing"];
            
            for (int i=0; i<[follinftemparray count]; i++)
            {
                NSDictionary *dict_followers=[follinftemparray objectAtIndex:i];
                
                NSString *useridstr =[NSString stringWithFormat:@"%@",[dict_followers objectForKey:@"userId"]];
                [followingArray addObject:useridstr];
                
            }
        }
        NSLog(@"followers array is %@",self.array_followers);
        [self.tblview_followers reloadData];
        
    }
    
}

-(void)successResponseforRegistration:(NSDictionary *)responseDict
{
    
    [myappDelegate startspinner:self.view];
    [self performSelector:@selector(getdata)];
    
}
#pragma mark--tableviewmethods
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return self.array_followers.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    return 60;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    static NSString *MyIdentifier= @"MyIdentifiertripscell";
    FollowersTableViewCell *tbcell= (FollowersTableViewCell *)[tableView dequeueReusableCellWithIdentifier:MyIdentifier];
    
    if (tbcell==nil)
    {
        [[NSBundle mainBundle]loadNibNamed:@"FollowersTableViewCell" owner:self options:nil];
        
        tbcell=tblcell_followers;
    }
    
    FriendsObject *obj_friends=[array_followers objectAtIndex:indexPath.row];
    
    CALayer *layer2 = tbcell.imgview_follower.layer;
    [layer2 setCornerRadius:25.0];
    [layer2 setBorderWidth:0];
    [layer2 setMasksToBounds:YES];
    
    NSURL *picurl=[NSURL URLWithString:obj_friends.string_follwerPicUrl];
    [tbcell.imgview_follower setImageWithURL:picurl  placeholderImage:[UIImage imageNamed:@"noimage_friend.png"] options:SDWebImageHighPriority];
    
    
    //if([APPDELEGATE isnullorempty:obj_friends.string_follwerName])
    if([APPDELEGATE isnullorempty:obj_friends.string_follwerUserName])
    {
        //obj_friends.string_follwerUserName
       // tbcell.lbl_follower.text=obj_friends.string_follwerUserName;
        tbcell.lbl_follower.text=obj_friends.string_follwerName;
    }
    else
    {
        
        tbcell.lbl_follower.text=obj_friends.string_follwerUserName;
    }
    
    tbcell.lbl_follower.font = [UIFont fontWithName:@"Muli" size:17];
    if (isfollowers==YES)
    {
        if(obj_friends.isstatus==YES)
        // tbcell.button_follower.hidden=YES;
            [tbcell.button_follower setImage:[UIImage imageNamed:@"unfollow_btn.png"] forState:UIControlStateNormal];

            
        else{
            [tbcell.button_follower setImage:[UIImage imageNamed:@"follow_btn.png"] forState:UIControlStateNormal];

        }
    }
    
    //This is for display  for the unfollow and follow
 /*   if(obj_friends.isstatus==YES)
    {
          [tbcell.button_follower setImage:[UIImage imageNamed:@"follow_btn.png"] forState:UIControlStateNormal];
    }
    else
    {
        [tbcell.button_follower setImage:[UIImage imageNamed:@"unfollow_btn.png"] forState:UIControlStateNormal];
    }
  */
    if (followingArray.count>0)
    {
        for (int i=0; i<followingArray.count; i++)
        {
            if ([[NSString stringWithFormat:@"%@",obj_friends.useridstr] isEqualToString:[followingArray objectAtIndex:i]])
            {
                
             //  tbcell.button_follower.hidden=YES;
                break;
            }
        }
    }
    
    tbcell.button_follower.tag=indexPath.row;
    
    if (issellerprofile)
    {
        tbcell.button_follower.hidden=YES;
    }
    
    tbcell.selectionStyle=UITableViewCellSelectionStyleNone;
    return tbcell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (issellerprofile)
    {
        //tbcell.button_follower.hidden=YES;
       // return;
    }
    FriendsObject *obj_friends=[array_followers objectAtIndex:indexPath.row];

    
    if (![[NSString stringWithFormat:@"%@",myappDelegate.useridstring]  isEqualToString:[NSString stringWithFormat:@"%@",obj_friends.useridstr]] )
    {
    
         ItemsModelobject *sltobj=[[ItemsModelobject alloc]init];
    
    
    sltobj.profilepicstr=obj_friends.string_follwerPicUrl;
    sltobj.namestr= obj_friends.string_follwerName;
    sltobj.usernamestring = obj_friends.string_follwerUserName;
    sltobj.useridstr=obj_friends.useridstr;
        
    
    FollowViewController *followVC;
    if (myappDelegate.isiphone5)
    {
        followVC = [[FollowViewController alloc]initWithNibName:@"FollowViewController" bundle:nil];
    }
    else
    {
        followVC = [[FollowViewController alloc]initWithNibName:@"FollowViewController_iphone4" bundle:nil];
    }
    
    followVC.sltobj=sltobj;
        followVC.notifystr=@"followers";
    
    [self.navigationController pushViewController:followVC animated:YES];
    }
    else
    {
        BOOL isVCExist=NO;
        NSArray *arr=self.navigationController.viewControllers;
        for (UIViewController *childvc in arr) {
            
            if([childvc isKindOfClass:[ProfileViewController class]])
            {
                isVCExist=YES;
                UIViewController *subvc=(ProfileViewController *)childvc;
                [self.navigationController popToViewController:subvc animated:YES];
                break;
            }
        }
        
        if(!isVCExist)
        {
            ProfileViewController *profileVC;
            if (myappDelegate.isiphone5
                )
            {
                profileVC = [[ProfileViewController alloc]initWithNibName:@"ProfileViewController" bundle:nil];
            }
            else
            {
                profileVC = [[ProfileViewController alloc]initWithNibName:@"ProfileViewController_iphone4" bundle:nil];
            }
            
            [self.navigationController pushViewController:profileVC animated:YES];
        }

    }
    

}
-(void)internetConnection
{
    UIAlertView *Internetalert=[[UIAlertView alloc]initWithTitle:appname message:@"Internet connection not available" delegate:self cancelButtonTitle:nil otherButtonTitles:@"Ok", nil];
    [Internetalert show];
    [APPDELEGATE stopspinner:self.view];
    
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
