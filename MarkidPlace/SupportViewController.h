//
//  SupportViewController.h
//  MarkidPlace
//
//  Created by Varma Bhupatiraju on 10/09/14.
//  Copyright (c) 2014 stellentmac1. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SupportcustomCell.h"
@class AppDelegate;
@interface SupportViewController : UIViewController<UINavigationBarDelegate,UITableViewDataSource,UITableViewDelegate>
{
    AppDelegate*appdelegate;
     NSMutableArray *finalinvitearray;
}
@property (nonatomic,strong) IBOutlet UINavigationBar *navbar;
@property(nonatomic,strong)IBOutlet NSMutableArray *firstarry;
@property(nonatomic,strong)IBOutlet NSMutableArray *secondarrray;
@property(nonatomic,strong)IBOutlet UITableView *firsttableview;
@property(nonatomic,strong)IBOutlet UILabel *supportlbl;
@property(nonatomic,strong)IBOutlet SupportcustomCell *mySupportcustomCell;
-(IBAction)backAction:(id)sender;
-(IBAction)homeBtnAction:(id)sender;
@end
