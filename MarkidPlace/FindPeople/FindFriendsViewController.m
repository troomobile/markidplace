//
//  FindFriendsViewController.m
//  MarkidPlace
//
//  Created by Varma Bhupatiraju on 24/09/14.
//  Copyright (c) 2014 stellentmac1. All rights reserved.
//

#import "FindFriendsViewController.h"
#import "InviteModelobject.h"
#import "SuggestionsViewController.h"
#import "ProfileRepository.h"
@interface FindFriendsViewController ()

@end

@implementation FindFriendsViewController
{
    ProfileRepository *profileRep;
}
@synthesize navbar,findtitlelbl,firstsectionarray,myimagearray,secondsectionarray,Basicarray,findpeopletblview,findfriendcell,nameimagearray,accounttblview,accountarray,suggestiontbllbl,duplicatearray,searchbar,imFronVC,backButton;
@synthesize friendPickerController = _friendPickerController;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.navbar.frame = CGRectMake(0, 0, 320, 64);
    [self.navbar setBackgroundImage:[UIImage imageNamed:@"navbar.png"] forBarMetrics:UIBarMetricsDefault];
    self.suggestiontbllbl.font=[UIFont fontWithName:@"muli" size:16];
    
    [[UINavigationBar appearance] setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
    self.findtitlelbl.font = [UIFont fontWithName:@"Monroe" size:18];
    [searchbar setShowsCancelButton:NO animated:YES];
    /*
    dispatch_async( dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
    [self  getTwitter_and_Phonebookcontacts];
    });
     */
    
    //    FBLoginView *loginView = [[FBLoginView alloc] init];
    //    loginView.center = self.view.center;
    //    [self.view addSubview:loginView];
    profileRep=[[ProfileRepository alloc]init];
    profileRep.delegate=self;
    
    firstsectionarray=[[NSMutableArray alloc]initWithObjects:@"Facebook",@"Twitter",@"Contacts", nil];
    myimagearray=[[NSMutableArray alloc]initWithObjects:@"fb_btn-1.png",@"twitter_btn-1.png",@"contact_btn.png", nil];
    //duplicatearray=[NSMutableArray new];
    //Modified by srinadh on 25th
    nameimagearray=[[NSMutableArray alloc]initWithObjects:@"pic.png",@"pic.png",@"pic.png",@"pic.png", nil];
    
    secondsectionarray  =[[NSMutableArray alloc]init];
    
    Basicarray=[[NSMutableArray alloc]init];
    accountarray=[[NSMutableArray alloc]init];
    //[self performSelectorOnMainThread:@selector(getuserslist) withObject:nil waitUntilDone:YES];
    
     /*
    dispatch_async( dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
       
        AddFriendsViewController *addfriendsVC=[[AddFriendsViewController alloc]init];
        addfriendsVC.typeString=@"settings";
        
        [addfriendsVC twitterBtnAction:nil];
       // [addfriendsVC  performSelector:@selector(twitterBtnAction:) withObject:nil afterDelay:0];
        
        InviteFriendsViewController *invitefriendsVC=[[InviteFriendsViewController alloc]init];
        // [invitefriendsVC  performSelector:@selector(emailbutton:) withObject:nil afterDelay:0];
        [invitefriendsVC performSelectorOnMainThread:@selector(emailbutton:) withObject:nil waitUntilDone:YES];
      
        
    });
     */
    
    
    _friendPickerController=[[FBFriendPickerViewController alloc]init];
    
    for (int i=0;i<[firstsectionarray count]; i++)
    {
        InviteModelobject *obj=[[InviteModelobject alloc]init];
        obj.namestring=[firstsectionarray objectAtIndex:i];
        obj.imgstring=[myimagearray objectAtIndex:i];
        
        
        [Basicarray addObject:obj];
        NSLog(@"Basicarray.... %@", [Basicarray description]);
    }
    for (int i=0;i<[secondsectionarray count]; i++)
    {
        InviteModelobject *obj=[[InviteModelobject alloc]init];
        obj.accountstr=[secondsectionarray objectAtIndex:i];
        obj.accountimgstr=[nameimagearray objectAtIndex:i];
       
    }
   
    [APPDELEGATE stopspinner:self.view];
    //[accounttblview reloadData];
    // [findpeopletblview reloadData];
    // Do any additional setup after loading the view from its nib.
}


-(void)viewWillAppear:(BOOL)animated
{
    [APPDELEGATE.suggestionslistArray removeAllObjects];
    duplicatearray=[NSMutableArray new];
     searchbar.text=@"";
    self.navigationController.navigationBar.hidden=YES;
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.01 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
     //dispatch_async( dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        [APPDELEGATE  startspinner:self.view];
        //[accountarray removeAllObjects];
        
        [self getSuggestionList];
      SettingsRepository  *settingrepo=[[SettingsRepository alloc]init];
        [settingrepo getListUsers:[myappDelegate getStringFromPlist:@"userid"]];
        [APPDELEGATE stopspinner:self.view];
    });
   // [APPDELEGATE stopspinner:self.view];

   
}
-(void)getTwitter_and_Phonebookcontacts
{
    
        
        InviteFriendsViewController *invitefriendsVC=[[InviteFriendsViewController alloc]init];
    
        [invitefriendsVC performSelectorOnMainThread:@selector(emailbutton:) withObject:nil waitUntilDone:YES];
   
}
- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return  50;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView==accounttblview)
    {
        return Basicarray.count;
        
    }
    else
    {
        return accountarray.count;
    }
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView==accounttblview)
        
    {
        static NSString *cellIdentifier=@"acell";
        
        FindFriendsTableViewCell *tbcell =(FindFriendsTableViewCell*)[findpeopletblview dequeueReusableCellWithIdentifier:cellIdentifier];
        if (tbcell==nil)
        {
            [[NSBundle mainBundle] loadNibNamed:@"FindFriendsTableViewCell" owner:self options:nil];
            tbcell=findfriendcell;
            tbcell.selectionStyle=UITableViewCellSelectionStyleNone;
        }
        tbcell.accountlbl.font = [UIFont fontWithName:@"Muli" size:15];
        tbcell.suggestionlbl.font = [UIFont fontWithName:@"Muli" size:16];
        
        tbcell.accountlbl.text=[self.firstsectionarray objectAtIndex:indexPath.row];
        tbcell.accountimgview.image=[UIImage imageNamed:[myimagearray objectAtIndex:indexPath.row]];
        tbcell.suggestionlbl.hidden=YES;
        tbcell.followbtn.hidden=YES;
        
        return tbcell;
        
        
        
    }
    else
    {
        
        static NSString *cellIdentifier=@"acell";
        
        FindFriendsTableViewCell *cell =(FindFriendsTableViewCell*)[accounttblview dequeueReusableCellWithIdentifier:cellIdentifier];
        if (cell==nil)
        {
            [[NSBundle mainBundle] loadNibNamed:@"FindFriendsTableViewCell" owner:self options:nil];
            cell=findfriendcell;
            cell.selectionStyle=UITableViewCellSelectionStyleNone;
        }
        cell.accountlbl.font = [UIFont fontWithName:@"Muli" size:16];
        cell.suggestionlbl.font = [UIFont fontWithName:@"Muli" size:15];
        cell.followbtn.layer.borderWidth=1;
        cell.followbtn.layer.borderColor=[[UIColor colorWithRed:12.0/255.0 green:183.0/255.0 blue:174.0/255.0 alpha:1.0] CGColor];
        
        
        if(accountarray.count>0)
        {
            Users *userObj=[accountarray objectAtIndex:indexPath.row];
            if(userObj.isFollow==NO)
            {
                [cell.followbtn setTitle:@"Follow" forState:UIControlStateNormal];
            }
            else
            {
                [cell.followbtn setTitle:@"UnFollow" forState:UIControlStateNormal];
            }
            
            [cell.followbtn.titleLabel setFont:[UIFont fontWithName:@"Muli" size:14]];
            
            cell.suggestionlbl.text=userObj.userNameStr;
            cell.followbtn.tag=indexPath.row;
            [cell.nameimgview setImageWithURL:[NSURL URLWithString:userObj.profilepicStr] placeholderImage:[UIImage imageNamed:@"noimage_friend.png"] ];
            
            [cell.nameimgview.layer setCornerRadius:cell.nameimgview.frame.size.height/2];
            [cell.nameimgview.layer setBorderWidth:0.0];
            [cell.nameimgview.layer setMasksToBounds:YES];
            
            
            
            cell.accountlbl.hidden=YES;
        }
        
        
        return cell;
    }
    
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    suggVC=[[SuggestionlistViewController alloc]initWithNibName:@"SuggestionlistViewController" bundle:nil];
    if(tableView==accounttblview)
    {
        if(indexPath.section==0)
        {
            if(indexPath.row==0)
            {
                //facebook
                //[AppDelegate alertmethod:appname :@"Ooops!  It appears that none of your friends are using marKIDplace. That makes us sad!  No worries, you can invite them by going to the “Invite Friends” section in Settings"];
                
                //   ShareSettingsViewController *shareVC=[[ShareSettingsViewController alloc]init];
                // [shareVC performSelector:@selector(facebookPressed) withObject:nil afterDelay:0.01];
                [self facebookPressed];
            }
            if(indexPath.row==1)
            {
                suggVC.typestring=@"twitter";
                [self.navigationController pushViewController:suggVC animated:YES];

                
            }
            
            if(indexPath.row==2)
            {
                
               
                suggVC.typestring=@"contacts";

                [self.navigationController pushViewController:suggVC animated:YES];

            }
        }
    }
    
    if(tableView==findpeopletblview)
        
    {
        
        
        if (accountarray.count>0)
        {
            FollowViewController *followVC;
            Users *userObj=[accountarray objectAtIndex:indexPath.row];
            if (![[NSString stringWithFormat:@"%@",myappDelegate.useridstring]  isEqualToString:[NSString stringWithFormat:@"%@",userObj.userIdStr]] )
            {
                
                
                followVC = [[FollowViewController alloc]initWithNibName:@"FollowViewController" bundle:nil];
                
                followVC.imageString = userObj.profilepicStr;
                followVC.usernameString = userObj.nameStr;
                followVC.nameString = userObj.userNameStr;
                followVC.userId = userObj.userIdStr;
                followVC.contactStr=@"contacts";
                followVC.imFronVC = @"FindFriendsViewController";
                
                [self.navigationController pushViewController:followVC animated:YES];
                
            }
        }
        
    }
    
}


-(void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    
    NSInteger currentOffset = scrollView.contentOffset.y;
    NSInteger maximumOffset = scrollView.contentSize.height- scrollView.frame.size.height;
    if (maximumOffset - currentOffset <= 0) {
        //index =index +1;
        // Call your link here
    }
}
-(IBAction)backbuttonaction:(id)sender
{
    
    if ([imFronVC isEqualToString:@"SettingsViewController"])
    {
        for (UIViewController *vc in self.navigationController.viewControllers)
        {
            if ([vc isKindOfClass:[SettingsViewController class]])
            {
                SettingsViewController *lcVC=(SettingsViewController*)vc;
                
                [self.navigationController popToViewController:lcVC animated:YES];
                return;
                
            }
            
        }
    }
    else
        [self.navigationController popViewControllerAnimated:YES];
}
-(IBAction)homebuttonaction:(id)sender
{
    
    
    BOOL isVCExist=NO;
    NSArray *arr=self.navigationController.viewControllers;
    for (UIViewController *childvc in arr) {
        
        if([childvc isKindOfClass:[SuggestionsViewController class]])
        {
            isVCExist=YES;
           
            SuggestionsViewController *subvc=(SuggestionsViewController *)childvc;
             subvc.ismyfeed=NO;
            [self.navigationController popToViewController:subvc animated:YES];
        }
    }
    
    if(!isVCExist)
    {
        SuggestionsViewController *sugge;
        if ([AppDelegate isiphone5])
        {
            sugge = [[SuggestionsViewController alloc]initWithNibName:@"SuggestionsViewController" bundle:nil];
        }
        else
        {
            sugge = [[SuggestionsViewController alloc]initWithNibName:@"SuggestionsViewController_iphone4" bundle:nil];
        }
        [self.navigationController pushViewController:sugge animated:YES];
    }

}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(IBAction)followButtonAction:(id)sender
{
    Users *userObj=[accountarray objectAtIndex:[sender tag]];
    if(userObj.isFollow==YES)
    {
        userObj.isFollow=NO;
    }
    else
    {
        userObj.isFollow=YES;
    }
    
    
    
    NSMutableDictionary *dict=[[NSMutableDictionary alloc] init];
    NSString *userid=[myappDelegate getStringFromPlist:@"userid"];
    [dict setValue:userid forKey:@"UserId"];
    
    
    
    [dict setValue:[NSString stringWithFormat:@"%@",userObj.userIdStr] forKey:@"FollowerUserId"];
    
    [profileRep followUserswithDictionary:dict];
    
    
    [findpeopletblview reloadData];
}
#pragma mark Get data
-(void)getSuggestionList
{
    [APPDELEGATE startspinner:self.view];
    NSString *userid=[myappDelegate getStringFromPlist:@"userid"];
    [profileRep getSuggestionFriendsListByUserid:userid];
    
}
#pragma mark Responses
-(void)successResponseforDictionary:(NSDictionary *)responseDict
{
    NSLog(@"thew response dict is %@",responseDict);
    [APPDELEGATE stopspinner:self.view];
    NSMutableArray *list=[[NSMutableArray alloc]init];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.001 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        
          [accountarray removeAllObjects];
        
        if ([responseDict objectForKey:@"listSuggestionUsers"])
        {
            NSMutableArray *userslist=[[NSMutableArray alloc]init];
            
            [userslist addObjectsFromArray: [responseDict objectForKey:@"listSuggestionUsers"]];
            for (NSDictionary *dict in userslist)
            {
                Users *userObj=[[Users alloc]init];
                
                if([APPDELEGATE isnullorempty:[dict objectForKey:@"userName"]])
                {
                    
                     //userObj.userNameStr=@"No Name";
                    if([APPDELEGATE isnullorempty:[dict objectForKey:@"name"]])
                    {
                        userObj.userNameStr=@"No Name";
                    }
                    else
                    {
                        userObj.userNameStr=[dict objectForKey:@"name"];
                    }
                }
                else
                {
                   userObj.userNameStr=[dict objectForKey:@"userName"];
                }
                if([APPDELEGATE isnullorempty:[dict objectForKey:@"name"]])
                {
                    userObj.nameStr=@" ";
                }
                else
                {
                    userObj.nameStr=[dict objectForKey:@"name"];
                }
                if([APPDELEGATE isnullorempty:[dict objectForKey:@"userId"]])
                {
                    userObj.userIdStr=@" ";
                }
                else
                {
                    userObj.userIdStr=[dict objectForKey:@"userId"];
                }

                if([APPDELEGATE isnullorempty:[dict objectForKey:@"followersCount"]])
                {
                    userObj.followerCountStr=@" ";
                }
                else
                {
                    userObj.followerCountStr=[dict objectForKey:@"followersCount"];
                }

                if([APPDELEGATE isnullorempty:[dict objectForKey:@"profilepic"]])
                {
                    userObj.profilepicStr=@" ";
                }
                else
                {
                    userObj.profilepicStr=[dict objectForKey:@"profilepic"];
                }
               

                
                
                userObj.isFollow=[[dict objectForKey:@"isFollow"] boolValue];
                
                
                [list addObject:userObj];
                [duplicatearray addObject:userObj];
            }
        }
        [accountarray addObjectsFromArray:list];
        [findpeopletblview reloadData];
        [APPDELEGATE stopspinner:self.view];
    });
    [APPDELEGATE stopspinner:self.view];
}
-(void)errorMessage:(NSString *)message
{
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.001 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        if ([message isEqualToString:@"message"])
        {
            
            
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:appname message:@"Something is wrong, Please contact Admin" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
            
            
        }
        [APPDELEGATE stopspinner:self.view];
    });
}

//facebook get friendslist



-(void)facebookPressed//:(id)sender //thkr1
{
    
    
    if (FBSession.activeSession.state == FBSessionStateOpen
        || FBSession.activeSession.state == FBSessionStateOpenTokenExtended) {
        
        NSLog(@"in if block");
        [self openfbpicker];
        
    } else {
        NSLog(@"in else block");
        // Open a session showing the user the login UI
        // You must ALWAYS ask for public_profile permissions when opening a session
        [FBSession openActiveSessionWithReadPermissions:@[@"public_profile",@"email"]
                                           allowLoginUI:YES
                                      completionHandler:
         ^(FBSession *session, FBSessionState state, NSError *error) {
             
             
             // Retrieve the app delegate
             if (!error && state == FBSessionStateOpen){
                 NSLog(@"Session opened");
                 // Show the user the logged-in UI
                 [self openfbpicker];
             }
             else
             {
                 NSLog(@"error is %@",[error localizedDescription]);
             }
         }];
    }
    
}


-(void)openfbpicker
{
   
    
    FBRequest* friendsRequest = [FBRequest requestForMyFriends];
    [friendsRequest startWithCompletionHandler: ^(FBRequestConnection *connection,
                                                  NSDictionary* result,
                                                  NSError *error) {
        NSArray* friends = [result objectForKey:@"data"];
        //NSLog(@"Found: %d friends and result is %@",(int) friends.count,result);
        
        NSMutableArray *fbarray=[[NSMutableArray alloc]init];
        for (NSDictionary<FBGraphUser>* friend in friends) {
            FollowersObject *follow=[[FollowersObject alloc]init];
           // NSLog(@"I have a friend named %@ with id %@", friend.name, friend.objectID);
            follow.screen_name=friend.name;
            follow.followerid=friend.objectID;
            follow.imageurl=[NSString stringWithFormat:@"https://graph.facebook.com/%@/picture?type=square",friend.objectID];
            [fbarray addObject:follow];
            
        }
        [self getfacebooksuggestions:fbarray];
    }];
}







- (void)facebookViewControllerDoneWasPressed:(id)sender
{
    //  NSMutableString *text = [[NSMutableString alloc] init];
    NSString* fid;
    NSString* fbUserName;
    
    // NSLog(@"Friends Arr-------> %d",[self.friendPickerController.selection count]);
    
    if([self.friendPickerController.selection count]>0){
        
        for (id<FBGraphUser> user in self.friendPickerController.selection)
        {
            // NSLog(@"\nuser=%@\n", user);
            fid = user.id;
            
            fbUserName = user.name;
            //NSLog(@"friend name is %@",fbUserName);
        }
        [self dismissViewControllerAnimated:YES completion:^{
            
        }];
        
        
    }
    else
    {
        
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:appname
                                                            message:@"Please select a friend."
                                                           delegate:self
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
        
        
        [alertView show];
    }
    
}
- (void)facebookViewControllerCancelWasPressed:(id)sender
{
    
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
}
- (void)viewDidUnload
{
    self.friendPickerController = nil;
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)fillTextBoxAndDismiss:(NSString *)text {
    self.selectedFriendsView.text = text;
    
    [self dismissModalViewControllerAnimated:YES];
}

- (NSDictionary*)parseURLParams:(NSString *)query
{
    NSArray *pairs = [query componentsSeparatedByString:@"&"];
    NSMutableDictionary *params2 = [[NSMutableDictionary alloc] init];
    for (NSString *pair in pairs) {
        NSArray *kv = [pair componentsSeparatedByString:@"="];
        NSString *val =
        [kv[1] stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        params2[kv[0]] = val;
    }
    return params2;
}
//////////////////


-(void)getfacebooksuggestions:(NSMutableArray *)fbfollowers
{
    [APPDELEGATE.suggestionslistArray removeAllObjects];
    NSLog(@"users list from the MKP is %@",APPDELEGATE.userlistArray);
    for (int i=0; i<APPDELEGATE.userlistArray.count; i++)
    {
        for (int k=0; k<fbfollowers.count; k++)
        {
            SuggestionObject *obj=[APPDELEGATE.userlistArray objectAtIndex:i];
            FollowersObject *follow=[fbfollowers objectAtIndex:k];
            if([obj.typestr isEqualToString:@"Facebook"])
            {
                if([obj.socialidstr isEqualToString:follow.followerid])
                {
                   // NSLog(@"the log of k value is %d",i);
                    [APPDELEGATE.suggestionslistArray addObject:obj];
                    break;
                }
            }
        }
        
    }
    // NSLog(@"APPDELEGATE.suggestionslistArray.count is %d",APPDELEGATE.suggestionslistArray.count);
    [self suggestionlistview];
    
}
// This method will handle ALL the session state changes in the app

// Show the user the logged-out UI



-(void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar
{
    [searchBar setShowsCancelButton:YES animated:YES];
    
    if ([searchBar.text isEqualToString:@" "])
    {
        [accountarray removeAllObjects];
        [accountarray addObjectsFromArray:self.duplicatearray];
        [findpeopletblview reloadData];
    }
    
}
-(void)searchBarTextDidEndEditing:(UISearchBar *)searchBar
{
    if ([searchBar.text isEqualToString:@" "])
    {
        searchBar.text = @"";
        [accountarray removeAllObjects];
        [accountarray addObjectsFromArray:self.duplicatearray];
        [findpeopletblview reloadData];
    }
    
}
-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    [searchBar setShowsCancelButton:NO animated:YES];
    if(searchBar.text.length == 0)
    {
        [searchBar setShowsCancelButton:YES animated:YES];
        
        [accountarray removeAllObjects];
        [accountarray addObjectsFromArray:self.duplicatearray];
        [findpeopletblview reloadData];
    }
    else
    {
        [searchBar setShowsCancelButton:YES animated:YES];
        [accountarray removeAllObjects];
       
        
        for(int i=0;i<[self.duplicatearray count];i++)
        {
            Users*searchobj=[self.duplicatearray objectAtIndex:i];
            
            //     NSRange nameRange = [[[self.duplicatearray objectAtIndex:i] valueForKey:@"itemName"]  rangeOfString:searchBar.text options:(NSCaseInsensitiveSearch|NSDiacriticInsensitiveSearch)];
            NSRange nameRange = [searchobj.userNameStr rangeOfString:searchBar.text options:(NSCaseInsensitiveSearch|NSDiacriticInsensitiveSearch)];
            
            if(nameRange.location != NSNotFound)
            {
                [accountarray addObject:[self.duplicatearray objectAtIndex:i]];
            }
        }
        [findpeopletblview reloadData];
    }
    
    if([searchText length] == 0)
    {
        [searchBar performSelector: @selector(resignFirstResponder)
                        withObject: nil
                        afterDelay: 0.1];
        return;
    }
    
}
- (BOOL)searchBar:(UISearchBar *)searchBar shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    if ([text isEqualToString:@" "])
    {
        return NO;
    }
    else
    {
        return YES;
    }
}

-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [searchBar resignFirstResponder];
    [accountarray removeAllObjects];
    for(int i=0;i<[self.duplicatearray count];i++)
    {
        Users*searchobj=[self.duplicatearray objectAtIndex:i];
        
        NSRange nameRange = [searchobj.userNameStr rangeOfString:searchBar.text options:(NSCaseInsensitiveSearch|NSDiacriticInsensitiveSearch)];
        
        if(nameRange.location != NSNotFound)
        {
            [accountarray addObject:[self.duplicatearray objectAtIndex:i]];
        }
    }
    [findpeopletblview reloadData];
}

-(void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    searchBar.text=@"";
    [searchBar setShowsCancelButton:NO animated:YES];
    
    [searchBar resignFirstResponder];
    [accountarray removeAllObjects];
    [accountarray addObjectsFromArray:duplicatearray];
    [findpeopletblview reloadData];
}
-(void)internetConnection
{
    UIAlertView *Internetalert=[[UIAlertView alloc]initWithTitle:appname message:@"Internet connection not available" delegate:self cancelButtonTitle:nil otherButtonTitles:@"Ok", nil];
    [Internetalert show];
    [APPDELEGATE stopspinner:self.view];
    
}
-(void)suggestionlistview
{
    suggVC=[[SuggestionlistViewController alloc]initWithNibName:@"SuggestionlistViewController" bundle:nil];
    if(APPDELEGATE.suggestionslistArray.count>0)
    {
        [self.navigationController pushViewController:suggVC animated:YES];
    }
    else
    {
        if([alertmsg isEqualToString:@"twitter"])
        {
            [AppDelegate alertmethod:appname :@"Ooops!  It appears that none of your friends are using marKIDplace. That makes us sad!  No worries, you can invite them by going to the “Invite Friends” section in Settings"];
        }
        else if([alertmsg isEqualToString:@"contacts"])
        {
            [AppDelegate alertmethod:appname :@"Ooops!  It appears that none of your friends are using marKIDplace. That makes us sad!  No worries, you can invite them by going to the “Invite Friends” section in Settings"];
        }
        else
        {
            [AppDelegate alertmethod:appname :@"Ooops!  It appears that none of your friends are using marKIDplace. That makes us sad!  No worries, you can invite them by going to the “Invite Friends” section in Settings"];
        }
    }
}
//getting the all users.

-(void)getuserslist
{
    SettingsRepository *settingrepo=[[SettingsRepository alloc]init];
    settingrepo.delegate=self;
    
            NSString *useridstring=[APPDELEGATE getStringFromPlist:@"userid"];
            if([APPDELEGATE isnullorempty:useridstring])
            {
                APPDELEGATE.islistusers=YES;
            }
            else
            {
                [settingrepo getListUsers:useridstring];
            }
}
@end
