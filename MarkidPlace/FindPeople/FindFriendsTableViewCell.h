//
//  FindFriendsTableViewCell.h
//  MarkidPlace
//
//  Created by Varma Bhupatiraju on 24/09/14.
//  Copyright (c) 2014 stellentmac1. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FindFriendsTableViewCell : UITableViewCell
@property(nonatomic,strong)IBOutlet UILabel *accountlbl;
@property(nonatomic,strong)IBOutlet UIImageView *accountimgview;
@property(nonatomic,strong)IBOutlet UILabel *suggestionlbl;
@property(nonatomic,strong)IBOutlet UIImageView *nameimgview;
@property(nonatomic,strong)IBOutlet UIButton *followbtn;


@end
