//
//  FindFriendsViewController.h
//  MarkidPlace
//
//  Created by Varma Bhupatiraju on 24/09/14.
//  Copyright (c) 2014 stellentmac1. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FindFriendsTableViewCell.h"
#import "HelperProtocol.h"
#import "SuggestionlistViewController.h"
#import <Accounts/Accounts.h>
#import <FacebookSDK/FacebookSDK.h>
#import <Social/Social.h>
#import "FollowersObject.h"


//#import "ShareSettingsViewController.h

@class SuggestionlistViewController;
@interface FindFriendsViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,HelperProtocol,FBFriendPickerDelegate,UISearchBarDelegate>
{
    NSString *friendnmeString;
     NSMutableDictionary *params;
    SuggestionlistViewController *suggVC;
      NSString*alertmsg;
     //AppDelegate*appdelegate;


}
@property (strong, nonatomic)  NSMutableArray *duplicatearray;
@property (strong, nonatomic) FBFriendPickerViewController *friendPickerController;
@property (strong, nonatomic) IBOutlet UINavigationBar *navbar;
@property (strong, nonatomic) IBOutlet UILabel *findtitlelbl;
@property (strong, nonatomic) IBOutlet NSMutableArray *firstsectionarray;
@property (strong, nonatomic) IBOutlet UIView *fbView;
@property (strong, nonatomic) IBOutlet UISearchBar *searchbar;
@property (strong, nonatomic)  NSMutableArray *
myimagearray;
@property (strong, nonatomic) IBOutlet NSMutableArray *
suggestionsArray;
@property(nonatomic,strong) NSString *imFronVC;
@property (strong, nonatomic) IBOutlet NSMutableArray *Basicarray;
@property (strong, nonatomic) IBOutlet NSMutableArray *secondsectionarray;
@property (strong, nonatomic) IBOutlet UITableView *findpeopletblview;
@property (strong, nonatomic) IBOutlet FindFriendsTableViewCell *findfriendcell;
@property (strong, nonatomic) IBOutlet NSMutableArray *nameimagearray;@property (strong, nonatomic) IBOutlet NSMutableArray *accountarray;
@property (strong, nonatomic) IBOutlet UITableView *accounttblview;
@property (strong, nonatomic) IBOutlet UILabel *suggestiontbllbl;
@property (strong, nonatomic) IBOutlet UITextView *selectedFriendsView;
@property (strong,nonatomic) IBOutlet UIButton *backButton;

- (void)fillTextBoxAndDismiss:(NSString *)text;

-(IBAction)backbuttonaction:(id)sender;
-(IBAction)homebuttonaction:(id)sender;
-(IBAction)followButtonAction:(id)sender;
@end
