//
//  SharingTableViewCell.h
//  MarkidPlace
//
//  Created by Varma Bhupatiraju on 20/09/14.
//  Copyright (c) 2014 stellentmac1. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SharingTableViewCell : UITableViewCell
@property(nonatomic,strong)IBOutlet UILabel *firstlbl;
@property(nonatomic,strong) IBOutlet UILabel *secondlbl;
@property(nonatomic,strong) IBOutlet UIButton *switchbtn;
@end
