//
//  UserLists.m
//  MarkidPlace
//
//  Created by Varma Bhupatiraju on 5/31/14.
//  Copyright (c) 2014 stellentmac1. All rights reserved.
//

#import "UserLists.h"

@implementation UserLists
@synthesize userListingIdStr;
@synthesize userItemIdStr;
@synthesize categoryIdStr;
@synthesize listPriceStr;
@synthesize statusStr;
@synthesize inPersonStr;
@synthesize uspsStr;
@synthesize userIDStr;
@synthesize unitPriceStr;
@synthesize categoryNameStr;
@synthesize sizeIdStr;
@synthesize sizeStr;
@synthesize brandIdStr;
@synthesize brandStr;
@synthesize conditionIdStr;
@synthesize conditionStr;
@synthesize descriptionStr;
@synthesize itemNameStr;
@synthesize availableQuantityStr;
@synthesize itemPicturesArray,likestatus;
//@synthesize itemStatus;


@end
