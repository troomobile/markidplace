//
//  NotificationSettingsViewController.h
//  MarkidPlace
//
//  Created by stellent_mac_05 on 17/09/14.
//  Copyright (c) 2014 stellentmac1. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "NotificationCell.h"
#import "ProfileRepository.h"


@class AppDelegate;
@interface NotificationSettingsViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,HelperProtocol>
{
    AppDelegate*appdelegate;
    BOOL isswitch;
    BOOL isFollow;
    BOOL islike;
    BOOL ismessage;
    BOOL islisting;
    BOOL isannouncements;
}
@property (nonatomic,strong) IBOutlet UINavigationBar *navbar;
@property (nonatomic,strong) IBOutlet UITableView *notificationlistview;
@property (nonatomic,strong) IBOutlet NotificationCell *notificationcell;
@property (nonatomic,strong) IBOutlet UILabel *titlelbl;
@property (nonatomic,strong) ProfileRepository *profilerepo;
@property (nonatomic,strong)id<HelperProtocol>delegate;


-(IBAction)backbtnAction:(id)sender;

-(IBAction)homebtnaction:(id)sender;

-(IBAction)switchBtnAction:(id)sender;


@end
