//
//  SharingViewController.h
//  MarkidPlace
//
//  Created by Varma Bhupatiraju on 20/09/14.
//  Copyright (c) 2014 stellentmac1. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SharingTableViewCell.h"
#import "AppDelegate.h"
@interface SharingViewController : UIViewController<UINavigationBarDelegate,UITableViewDelegate,UITableViewDataSource>
{
    BOOL fb;
    BOOL twitter;
    BOOL isswitch;
  //  int switchtag;
    AppDelegate*appdelegate;
}
@property (nonatomic,strong)IBOutlet UITableView  *sharingtblview;
@property (nonatomic,strong)IBOutlet UILabel  *titlelbl;
@property(nonatomic,strong)IBOutlet NSMutableArray *firstarray;
@property(nonatomic,strong)IBOutlet NSMutableArray *secondarray;
@property(nonatomic,strong)IBOutlet NSMutableArray *thirdarray;
@property (nonatomic,strong) IBOutlet UINavigationBar *navbar;
@property (nonatomic,strong) IBOutlet SharingTableViewCell *tableviewcell;
-(IBAction)backbtnaction:(id)sender;
-(IBAction)homebtnaction:(id)sender;
-(IBAction)switchbutton:(id)sender;
@end
