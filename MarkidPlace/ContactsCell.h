//
//  contactsCell.h
//  MarkidPlace
//
//  Created by stellent_mac_05 on 28/06/14.
//  Copyright (c) 2014 stellentmac1. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ContactsCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *contactlbl;
@property (strong, nonatomic) IBOutlet UIImageView *selectedImageView;

@end
