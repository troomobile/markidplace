//
//  MySizesAndBrandsViewController.h
//  MarkidPlace
//
//  Created by Varma Bhupatiraju on 14/08/14.
//  Copyright (c) 2014 stellentmac1. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SuggestionsViewController.h"
#import "MySizecustomCell.h"
@interface MySizesAndBrandsViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>
{
    AppDelegate *appdelegate;
}
@property(nonatomic,strong)IBOutlet UILabel *titlelabel;
@property(nonatomic,strong)IBOutlet UILabel *preelabel;
@property (nonatomic,strong) IBOutlet UINavigationBar *navbar;
@property (nonatomic,strong) IBOutlet UIScrollView *listingScrollView;

@property(nonatomic,strong)IBOutlet UITableView *dropdownview;
@property (nonatomic,strong) IBOutlet MySizecustomCell *sizecell;
@property (nonatomic,strong) IBOutlet UIView *mysizesview;
@property(nonatomic,strong)IBOutlet UILabel *bornlabel;
@property(nonatomic,strong)IBOutlet UILabel *zerolabel;
@property(nonatomic,strong)IBOutlet UILabel *threelabel;
@property(nonatomic,strong)IBOutlet UILabel *sixtwllabel;
@property(nonatomic,strong)IBOutlet UILabel *twlabel;
@property(nonatomic,strong)IBOutlet UILabel *eiglabel;
@property(nonatomic,strong)IBOutlet UILabel *twotlabel;
@property(nonatomic,strong)IBOutlet UILabel *threetlabel;
@property(nonatomic,strong)IBOutlet UILabel *fourlabel;
@property(nonatomic,strong)IBOutlet UILabel *fivelabel;
@property(nonatomic,strong)IBOutlet UILabel *sixlabel;
@property(nonatomic,strong)IBOutlet UILabel *sevenlabel;
@property(nonatomic,strong)IBOutlet UILabel *eightlabel;
@property(nonatomic,strong)IBOutlet UILabel *ninelabel;
@property(nonatomic,strong)IBOutlet UILabel *tenlabel;
@property(nonatomic,strong)IBOutlet UILabel *elevlabel;
@property(nonatomic,strong)IBOutlet UILabel *twlevelabel;
@property(nonatomic,strong)IBOutlet UILabel *thretlabel;
@property(nonatomic,strong)IBOutlet UILabel *fourtlabel;
@property(nonatomic,strong)IBOutlet UILabel *xslabel;
@property(nonatomic,strong)IBOutlet UILabel *slabel;
@property(nonatomic,strong)IBOutlet UILabel *mlabel;
@property(nonatomic,strong)IBOutlet UILabel *llabel;
@property(nonatomic,strong)IBOutlet UILabel *xllabel;
@property(nonatomic,strong)IBOutlet UILabel *maternityxslabel;
@property(nonatomic,strong)IBOutlet UILabel *maternityslabel;
@property(nonatomic,strong)IBOutlet UILabel *maternitymlabel;
@property(nonatomic,strong)IBOutlet UILabel *maternityllabel;
@property(nonatomic,strong)IBOutlet UILabel *maternityxllabel;
@property(nonatomic,strong)IBOutlet UIButton*updatebtn;
@property(nonatomic,strong)IBOutlet UILabel *updatelbl;
@property(nonatomic,strong)IBOutlet NSMutableArray *firstaary;
@property(nonatomic,strong)IBOutlet NSMutableArray *secondarray;
@property(nonatomic,strong)IBOutlet UILabel *brandlabel;
@property(nonatomic,strong)IBOutlet UIView *popview;
@property(nonatomic,strong)IBOutlet UIImageView *updateimagview;
@property(nonatomic,strong)IBOutlet UITableView *brandtableview;
@property(nonatomic,strong)IBOutlet NSMutableArray *sizearray;
@property(nonatomic,strong)IBOutlet UILabel *poptitlelable;
@property(nonatomic,strong)IBOutlet UIButton *cancelbutton;
-(IBAction)backAction:(id)sender;
-(IBAction)homeBtnAction:(id)sender;

-(IBAction)cancelBtnAction:(id)sender;
-(IBAction)brandaction:(id)sender;

@end

