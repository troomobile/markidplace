//
//  CustomActivity.h
//  MarkidPlace
//
//  Created by stellent on 27/05/14.
//  Copyright (c) 2014 stellentmac1. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomActivity : UIView
{
    UIImageView *imageview_spinner;
}
-(void)startactivity:(UIView *)otherview;
-(void)stopactivity;
@end
